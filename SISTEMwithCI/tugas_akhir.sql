-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 10, 2017 at 05:43 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tugas_akhir`
--

-- --------------------------------------------------------

--
-- Table structure for table `file_uploads_standar5`
--

CREATE TABLE `file_uploads_standar5` (
  `id` int(10) NOT NULL,
  `file_name` varchar(100) NOT NULL,
  `kebutuhan` varchar(50) NOT NULL,
  `deskripsi` varchar(50) NOT NULL,
  `type` varchar(30) NOT NULL,
  `size` int(11) NOT NULL,
  `date` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `file_uploads_standar5`
--

INSERT INTO `file_uploads_standar5` (`id`, `file_name`, `kebutuhan`, `deskripsi`, `type`, `size`, `date`) VALUES
(1, 'HTML.pdf', 'Pembelajaran', 'dasar html', 'application/pdf', 64245, '2017-02-27 01:27:52'),
(2, 'Panggilan-Trisakti.pdf', 'Suasana Akademik', 'trisakti', 'application/pdf', 11374, '2017-02-27 01:34:18'),
(3, 'DNS-Prinsip-Kerja-Beserta-Contohnya.pdf', 'Kurikulum', 'dns', 'application/pdf', 59439, '2017-02-27 01:34:44'),
(4, 'QUIZ2-int.pdf', 'Pembelajaran', 'ini file quiz broooooooooooooooooooooooooooooooooo', 'application/pdf', 24758, '2017-03-02 18:26:46'),
(6, 'ACHMAD-BAHRUR-ROZIfrmPROPOSAL-DESERTASI.pdf', 'Suasana Akademik', 'teori', 'application/pdf', 237931, '2017-03-16 22:48:39');

-- --------------------------------------------------------

--
-- Table structure for table `numberts`
--

CREATE TABLE `numberts` (
  `no` int(11) NOT NULL,
  `date_from` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `numberts`
--

INSERT INTO `numberts` (`no`, `date_from`) VALUES
(5, '2016-06-01'),
(6, '2017-10-10');

-- --------------------------------------------------------

--
-- Table structure for table `record_penilai`
--

CREATE TABLE `record_penilai` (
  `no` int(11) NOT NULL,
  `id_standar7` varchar(200) NOT NULL,
  `nama_penilai` varchar(500) NOT NULL,
  `bobot` float NOT NULL,
  `nilai` float NOT NULL,
  `date` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `record_penilai`
--

INSERT INTO `record_penilai` (`no`, `id_standar7`, `nama_penilai`, `bobot`, `nilai`, `date`) VALUES
(34, '7.3.1', 'adminbaru', 1.88, 7.52, '27-07-2017 02:12:30'),
(35, '7.3.2', 'adminbaru', 1.88, 7.52, '27-07-2017 02:12:35'),
(36, '7.2.1', 'adminbaru', 1.88, 7.52, '27-07-2017 02:12:45'),
(37, '7.2.2', 'adminbaru', 1.88, 3.76, '27-07-2017 02:12:52'),
(38, '7.1.1', 'adminbaru', 3.75, 15, '27-07-2017 02:13:17'),
(39, '7.1.2', 'adminbaru', 1.88, 7.52, '27-07-2017 02:13:20'),
(40, '7.1.3', 'adminbaru', 3.75, 15, '27-07-2017 02:13:23'),
(41, '7.1.4', 'adminbaru', 1.88, 7.52, '27-07-2017 02:13:33'),
(42, '7.1.2', 'Teguh', 1.88, 7.52, '27-07-2017 02:40:46'),
(43, '7.1.4', 'Teguh', 1.88, 7.52, '27-07-2017 02:40:49'),
(44, '7.1.4', 'Teguh', 1.88, 3.76, '27-07-2017 02:41:27'),
(45, '7.1.4', 'Teguh', 1.88, 7.52, '27-07-2017 02:41:45'),
(46, '7.1.4', 'adminbaru', 1.88, 5.64, '03-08-2017 10:31:50'),
(47, '7.1.1', 'aa', 3.75, 15, '07-08-2017 14:13:36'),
(48, '7.1.1', 'aa', 3.75, 15, '07-08-2017 14:13:40'),
(49, '7.1.1', 'aa', 3.75, 15, '07-08-2017 14:14:52');

-- --------------------------------------------------------

--
-- Table structure for table `report_version`
--

CREATE TABLE `report_version` (
  `no` int(11) NOT NULL,
  `file_names` varchar(1000) NOT NULL,
  `file_date` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `report_version`
--

INSERT INTO `report_version` (`no`, `file_names`, `file_date`) VALUES
(2, '1501858765_standar7.pdf', '2017-08-04 21:59:38'),
(3, '1501859095_standar7.pdf', '2017-08-04 22:05:11'),
(4, '1501860499_standar7.pdf', '2017-08-04 22:28:35'),
(5, '1501923579_standar7.pdf', '2017-08-05 15:59:57');

-- --------------------------------------------------------

--
-- Table structure for table `std5_52peninjauankurikulum`
--

CREATE TABLE `std5_52peninjauankurikulum` (
  `no` int(75) UNSIGNED NOT NULL,
  `no_mk` varchar(30) NOT NULL,
  `nama_mk` varchar(75) NOT NULL,
  `mk_baru_lama_hapus` varchar(20) NOT NULL,
  `perubahan_pada_silabus_sap` varchar(10) NOT NULL,
  `perubahan_pada_buku_ajar` varchar(10) NOT NULL,
  `alasan_peninjauan` varchar(333) NOT NULL,
  `atas_usulan_masukan_dari` varchar(100) NOT NULL,
  `berlaku_mulai_sem_th` varchar(100) NOT NULL,
  `excel_path` varchar(100) NOT NULL,
  `excelhtml_path` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `std5_52peninjauankurikulum`
--

INSERT INTO `std5_52peninjauankurikulum` (`no`, `no_mk`, `nama_mk`, `mk_baru_lama_hapus`, `perubahan_pada_silabus_sap`, `perubahan_pada_buku_ajar`, `alasan_peninjauan`, `atas_usulan_masukan_dari`, `berlaku_mulai_sem_th`, `excel_path`, `excelhtml_path`) VALUES
(1, 'DUH1A2', 'Manajemen Layanan', 'MK Baru', 'Ya', '', 'Lulusan Prodi Sistem Informasi harus mampu menjembatani antara aspek bisnis dan aspek teknis, sehingga harus ada mata kuliah yang memberikan pengetahuan kepada mahasiswa bagaimana suatu organisasi mengelola resources (people, technology) yang dimiliki melalui suatu proses yang berkesinambungan dan berorientasi pada kualitas untuk m', 'Reviewer Ir. Kridanto Surendro, MSc., PhD (Selasa, 10 Mei 2016)', 'Semester Ganjil Tahun Akademik 2017/2018 (Semester 7)', '', ''),
(2, 'ISH1B3', 'Sistem Enterprise', 'MK Baru', 'Ya', '', 'Diperlukan mata kuliah dasar pada Tingkat I untuk memperkenalkan bagaimana suatu enterprise menjalankan fungsi bisnisnya dengan mengacu pada rantai nilai Porter, memberikan pemahaman integrasi seluruh komponen sistem dan aplikasi sistem informasi dapat digunakan dalam memaksimalkan fungsi bisnisnya. Mata kuliah ini diperkuat dengan', 'Masukan dari Dosen Sistem Informasi pada Rapat Tinjauan Draft Kurikulum dan dari Reviewer Ir. Kridan', 'Semester Ganjil Tahun Akademik 2016/2017 (Semester 1)', '', ''),
(3, 'ISH1C3', 'Kepemimpinan dan Komunikasi Interpersonal', 'MK Baru', 'Ya', '', 'Diperoleh dari hasil survey AISINDO terhadap profesional Sistem Infomasi di Industri bahwa lulusan yang kompeten bukan hanya menguasai pengetahuan dan teknologi tetapi yang jauh lebih penting adalah softskill yang merupakan competitive behavior di dunia kerja. Untuk memenuhi tuntuan industri tersebut maka pada Tingkat I (Semester 1', 'Masukan dari Hasil Temu Nasional dengan pembicara Samiaji Sarosa, SE, MIS, PhD (Chapter Director of ', 'Semester Ganjil Tahun Akademik 2016/2017 (Semester 1)', '', ''),
(4, 'DUH1A2', 'Literasi TIK', 'MK Baru', 'Ya', '', 'Mata kuliah literasi TIK merupakan mata kuliah wajib Universitas Telkom (2 SKS) yang bertujuan untuk mencapai visi universitas dimana seluruh program studi harus memiliki mata kuliah yang memberikan wawasan teknologi informasi dan bagaimana menggunakan tools untuk mendapatkan, menggunakan informasi dan berkolaborasi di dunia maya.', 'Rapat Koordinasi Kurikulum Tingkat Universitas. Pada level program studi mata kuliah ekuivalen denga', 'Semester Ganjil Tahun Akademik 2016/2017 (Semester 1)', '', ''),
(5, 'ISH3H4', 'Rekayasa Perangkat Lunak Capstone Project', 'MK Baru', 'Ya', '', 'Mata kuliah Rekayasa Perangkat Lunak Casptone Project menggantikan mata kuliah Pengujian dan Implementasi pada Kurikulum 2012. Mata kuliah ini mempersiapkan mahasiswa untuk mampu mengaplikasikan teori pengembangan aplikasi perangkat lunak dan manajemen proyek untuk dapat menyelesaikan permasalahan yang ditemui di organisasi ataupun', 'Hasil review dari Tim Dosen Program Studi Sistem Informasi dan Rapat Tinjauan Fakultas terhadap Draf', 'Semester Genap Tahun Akademik 2016/2017 (Semester 6)', '', ''),
(6, 'FUG1B3', 'Fisika I', 'MK Hapus', 'Ya', '', 'Dalam IS2010 Curriculum dinyatakan bahwa kompetensi dasar Sistem Informasi dari  basic science diperkuat dari bidang matematika dan statistik. Untuk itu mata kuliah fisika sebanyak 8 SKS dihapus dan digantikan dengan mata kuliah IS Fundamental di Tingkat I seperti Kepemimpinan dan Komunikasi Interpersonal dan Mata Kuliah Manajemen ', 'Benchmarking terhadap struktur kurikulum Program Studi Sistem Informasi ITS (Institut Teknologi 10 N', 'Semester Ganjil dan GenapTahun Akademik 2016/2017', '', ''),
(7, 'FUG1C3', 'Fisika II', 'MK Hapus', 'Ya', '', '', '', '', '', ''),
(8, 'FUG1B1', 'Praktikum Fisika I', 'MK Hapus', 'Ya', '', '', '', '', '', ''),
(9, 'FUG1D1', 'Praktikum Fisika II', 'MK Hapus', 'Ya', '', '', '', '', '', ''),
(10, 'HUG1H2', 'Pengetahuan Lingkungan', 'MK Hapus', 'Ya', '', 'Mata kuliah Pengetahuan Lingkungan dihapus untuk mengakomodasi mata kuliah Sistem Enterprise yang merupakan mata kuliah baru sebanyak 3 SKS. Kompetensi yang diperoleh dari mata kuliah pengetahuan lingkungan diakomodasi dari mata kuliah Literasi TIK yang merupakan mata kuliah wajib dari Universitas.', 'Hasil review dari Tim Dosen Program Studi Sistem Informasi ', 'Semester Ganjil Tahun Akademik 2016/2017 (Semester 1)', '', ''),
(11, 'HUG2A2', 'Geladi', 'MK Hapus', 'Ya', '', 'Mata kuliah Geladi dihapus untuk mengakomodasi jumlah SKS mata kuliah Etika Profesi, Regulasi ICT, dan Budaya Internasional. Kompetensi mata kuliah Geladi untuk memberikan bekal keterampilan dasar di dunia kerja disebar dalam kompetensi mata kuliah yang bersifat Studi Kasus dan mata kuliah Rekayasa Perangkat Lunak yang bersifat Cap', 'Hasil review dari Tim Dosen Program Studi Sistem Informasi dan Evaluasi pelaksanaan Geladi oleh Dose', 'Semester Genap Tahun Akademik 2016/2017 (Semester 4)', '', ''),
(12, 'ISH2B3', 'Permodelan Bisnis Proses', 'MK Baru', 'Ya', '', 'Mata Kuliah Manajemen Proses Bisnis dibagi menjadi 2 Mata Kuliah yaitu Pemodelan Proses Bisnis dan Rekayasa Proses Bisnis. Hal ini dikarenakan Mata Kuliah Manajemen Proses Bisnis sebelumnya tidak mampu memberikan kompetensi maksimal dalam hal pemodelan dan bagaimana melakukan perbaikan proses bisnis yang relevan dalam pengembangan ', 'Hasil review dari Tim Dosen Program Studi Sistem Informasi dan Rapat Tinjauan Fakultas terhadap Draf', 'Semester Ganjil Tahun Akademik 2016/2017 (Semester 3)', '', ''),
(13, 'ISH2F3', 'Perancangan Interaksi Manusia dan Komputer', 'MK Lama', 'Ya', '', 'Pada Kurikulum 2012, mata kuliah Interaksi Manusia Komputer merupakan mata kuliah pilihan pada peminatan Content & System Development. Sesuai dengan hasil evaluasi kurikulum, maka mata kuliah tersebut merupakan mata kuliah wajib di Kurikulum 2016 karena merupakan salah satu penunjang pencapaian kompetensi utama yaitu kemampuan mera', 'Hasil review dari Tim Dosen Program Studi Sistem Informasi dan Rapat Tinjauan Fakultas terhadap Draf', 'Semester Genap Tahun Akademik 2016/2017 (Semester 6)', '', ''),
(14, 'ISH3J4', 'Pengembangan Aplikasi Bergerak', 'MK Lama', 'Ya', '', 'Pada Kurikulum 2012, mata kuliah Pengembangan Aplikasi Bergerak merupakan mata kuliah pilihan pada peminatan Content & System Development. Sesuai dengan hasil evaluasi kurikulum, maka mata kuliah tersebut merupakan mata kuliah wajib di Kurikulum 2016 karena perkembangan teknologi saat ini ke arah mobile application sangat cepat dan', 'Hasil review dari Tim Dosen Program Studi Sistem Informasi dan Rapat Tinjauan Fakultas terhadap Draf', 'Semester Genap Tahun Akademik 2016/2017 (Semester 8)', '', ''),
(15, 'ISH4K3', 'e-Business', 'MK Lama', 'Ya', '', 'Pada Kurikulum 2012, mata e-Business merupakan mata kuliah Wajib dan pada kurikulum 2016 merupakan mata kuliah pilihan pada peminatan Content & System Development (CSD). Mata kuliah ini bersifat spesifik pada kemampuan pengembangan aplikasi e-Bisnis dimana lulusan Sistem Informasi dengan peminatan CSD mampu membangun aplikasi, meng', 'Hasil review dari Tim Dosen Program Studi Sistem Informasi dan Rapat Tinjauan Fakultas terhadap Draf', 'Semester Genap Tahun Akademik 2016/2017', '', ''),
(16, 'ISH4M2', 'Pelatihan dan Sertifikasi', 'MK Lama', 'Ya', '', 'Pada Kurikulum 2012, mata kuliah Pelatihan dan Sertifikasi merupakan mata kuliah pilihan umum. Sesuai dengan hasil evaluasi kurikulum, maka mata kuliah tersebut merupakan mata kuliah wajib di Kurikulum 2016. Hal ini didasarkan untuk mempersiapkan lulusan yang mampu bersaing di kancah internasional (era MEA) yang memerlukan pengakua', 'Kompetensi Profesional SI menurut Survey AISINDO (UIN, 2 Maret 2016) dan Hasil Industrial Gathering ', 'Semester Ganjil Tahun Akademik 2016/2017 (Semester 7)', '', ''),
(17, 'ISH4E2', 'Etika Profesi, Regulasi ICT dan Budaya Internasional', 'MK Baru', 'Ya', '', 'Mata kuliah ini dipandang sangat perlu untuk mempersiapkan lulusan Sistem Informasi menjadi professional di bidangnya dengan memegang teguh etika yang berlaku, menghormati kepercayaan publik, menaati hukum yang berlaku, menunjukkan komitmen atas profesionalisme dan mengetahui bagaimana bekerja sama dengan anggota tim dengan latar k', 'Masukan dari Hasil Temu Nasional dengan pembicara Samiaji Sarosa, SE, MIS, PhD (Chapter Director of ', 'Semester Genap Tahun Akademik 2016/2017 (Semester 8)', '', ''),
(18, 'MUG1B4', 'Kalkulus II', 'MK Hapus', 'Ya', '', 'Tidak diperlukan Mata Kuliah Kalkulus Lanjutan dikarenakan mata kuliah yang menunjang kompetensi IS Fundamental dari Basic Scicence sudah cukup dengan adanya mata kuliah Kalkulus 1B, Matriks dan Ruang Vektor, dan Matematika Diskrit untuk memberikan dasar berpikir untuk kemampuan dalam pemrograman.', 'Hasil review dari Tim Dosen Program Studi Sistem Informasi dan Rapat Tinjauan Fakultas terhadap Draf', 'Semester Genap Tahun Akademik 2016/2017 (Semester 2)', '', ''),
(19, 'ISG4X3', 'Pilihan IV', 'MK Hapus', 'Ya', '', 'Dalam Kurikulum 2016 hanya diberikan 3 mata kuliah pilihan sesuai peminatan. Pilihan IV dihapuskan untuk mengakomodasi jumlah SKS mata kuliah Pelatihan dan Sertifkasi yang bersifat Wajib.', 'Hasil review dari Tim Dosen Program Studi Sistem Informasi', 'Semester Ganjil dan Genap Tahun Akademik 2016/2017 (Semester 7 & 8)', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `std5_56upayaperbaikanpembelajaran`
--

CREATE TABLE `std5_56upayaperbaikanpembelajaran` (
  `no` int(75) NOT NULL,
  `butir` varchar(100) NOT NULL,
  `upaya_perbaikan_tindakan` varchar(10000) NOT NULL,
  `upaya_perbaikan_hasil` varchar(10000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `std5_56upayaperbaikanpembelajaran`
--

INSERT INTO `std5_56upayaperbaikanpembelajaran` (`no`, `butir`, `upaya_perbaikan_tindakan`, `upaya_perbaikan_hasil`) VALUES
(1, 'Materi', 'Peninjauan silabus dan SAP untuk menyelaraskan dengan perkembangan pengetahuan', 'Tersampaikannya materi kuliah terbaru bagi mahasiswa pada setiap tahun akademik yang berjalan'),
(2, 'Materi', 'Pelaksanaan ToT (Training of Trainer) per matakuliah', 'Materi kuliah  dengan slide yang seragam untuk semua dosen dalam satu tim pengampu matakuliah.'),
(3, 'Materi', 'Pengayaan materi kuliah dengan materi yang dapat membantu dan menambahkan  pemahaman mahasiswa', 'Meningkatnya kompetensi dosen dan mahasiswa atas penguasaan  materi kuliah '),
(4, 'Metode Pembelajaran', 'Traning menulis buku bagi para dosen dari penerbit Andi', 'Terbitnya buku ajar untuk matakuliah tertentu yang ditulis oleh dosen pengampu'),
(5, 'Metode Pembelajaran', 'Mengadakan training tentang Student Centered Learning (SCL) untuk para dosen', 'Digunakannya metode problem based learnnig pada matakuliah enterpreunership'),
(6, 'Metode Pembelajaran', 'Penggunaan bahasa Inggris sebagai pengantar dalam kuliah dikelas tertentu', 'Adanya kelas internasional dengan pengantar kuliah berbahasa Inggris'),
(7, 'Metode Pembelajaran', 'Workshop elearning dan pembuatan konten elearning bagi dosen', 'Tersajikannya materi belajar dalam bentuk kuliah elearning untuk beberapa mata kuliah'),
(8, 'Metode Pembelajaran', 'Dilaksanakannya kegiatan penelitian tindakan kelas pada matakuliah Kalkulus ', 'Diperolehnya hibah metode ajar bagi dosen yang akhirnya dapat memilih metode yang tepat untuk kuliah Kalkulus'),
(9, 'Penggunaan Teknologi Pembelajaran', 'Menggunakan media seperti LCD projector, laptop dalam proses penyampaian materi', 'Web dan e-learning MK pada https://idea.telkomuniversity.ac.id/'),
(10, 'Penggunaan Teknologi Pembelajaran', 'Penggunaan komputer sebagai alat bantu simulasi', 'https://moodle.fakultasrekayasaindustri.org'),
(11, 'Penggunaan Teknologi Pembelajaran', 'Penggunaan web untuk download materi', 'telah membuka kesempatan belajar bagi mahasiswa yang lebih luas tanpa batasan waktu dan ruang kuliah. '),
(12, 'Penggunaan Teknologi Pembelajaran', 'Penggunaan e-learning', ''),
(13, 'Cara-cara evaluasi', 'Evaluasi hasil pembelajaran dilakukan melalui UTS, UAS, Quis dan Tugas besar dengan soal-soal yang seragam untuk setiap kelas paralel', 'Standar penilaian sama untuk setiap kelas paralel sehingga menciptakan tingkat kompetensi yang sama untuk setiap grade nilai'),
(14, 'Cara-cara evaluasi', 'Pemberlakuan SK tentang penentuan grade dalam proses penentuan nilai', 'Terjadinya keseragaman grade nilai yang diberikan oleh para dosen'),
(15, 'Cara-cara evaluasi', 'Evaluasi terhadap proses pembelajaran dilakukan dengan melakukan pengontrolan kesesuaian antara materi dan SAP', 'SOP pelaksanaan kegiatan administrasi dan prses perkuliahan  menjadi lebih lengkap, akuntabel dan teratur dengan adanya keterlibatan mahasiswa dalam hal kontrol penyampaian materi perkuliahan'),
(16, 'Cara-cara evaluasi', 'Evaluasi terhadap silabus dan SAP dilakukan dengan mekanisme tertentu', 'SOP pembelajaran menjadi lebih konsisten untuk dijalankan'),
(17, 'Cara-cara evaluasi', 'Penerapan ISO 9001:2008', ''),
(18, 'Pengenalan mahasiswa terhadap dunia kerja', 'Melakukan seminar-seminar kepada mahasiswa mengenai prospek dunia kerja', 'Beberapa mahasiswa menjadi brand ambasador seperti Tokopedia, Tech in Asia'),
(19, 'Pengenalan mahasiswa terhadap dunia kerja', 'Melalui MK Geladi (penjelasan detail pada buku panduan akademik)', 'Beberapa mahasiswa telah berhasil membuat aplikasi untuk solusi bagi perusahaan tempat melakukan KP dan Geladi'),
(20, 'Pengenalan mahasiswa terhadap dunia kerja', 'Melalui MK Kerja Praktek (penjelasan detail pada buku panduan kademik)', 'Mahasiswa dapat menyesuaikan kemampuan yang dimiliki dengan lapangan kerja apa yang dipilih'),
(21, 'Pengenalan mahasiswa terhadap dunia kerja', 'Melalui program Co-op (penjelasan detail pada buku panduan kademik)', 'Mahasiswa dapat mengikuti kegaiatan KP atau TA di perusahaan dari hasil kerjasama industri'),
(22, 'Pengenalan mahasiswa terhadap dunia kerja', 'Kerja sama dengan industri', '');

-- --------------------------------------------------------

--
-- Table structure for table `std5_511kompetensi`
--

CREATE TABLE `std5_511kompetensi` (
  `no` int(100) UNSIGNED NOT NULL,
  `kompetensi_utama` varchar(20000) NOT NULL,
  `kompetensi_pendukung` varchar(20000) NOT NULL,
  `kompetensi_lainnya` varchar(20000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `std5_511kompetensi`
--

INSERT INTO `std5_511kompetensi` (`no`, `kompetensi_utama`, `kompetensi_pendukung`, `kompetensi_lainnya`) VALUES
(1, '<p>Sebagaimana panduan dari KepMenDikNas No.045/U/2002, kompetensi utama adalah kompetensi yang bersifat dasar dan merupakan acuan baku minimal mutu penyelenggaraan program studi, dan ditetapkan oleh kalangan perguruan tinggi (program studi sejenis) bersama masyarakat profesi dan pengguna.</p>\n\n<p>Berdasarkan pertimbangan kebutuhan kompetensi utama lulusan, maka pada kurikulum 2012 Program Studi S1 Sistem Informasi Universitas Telkom ditetapkan <strong>kompetensi utama lulusan</strong> sebagai berikut:</p>\n\n<ol>\n	<li>Mempunyai kemampuan menerapkan pengetahuan dibidang komputasi, rekayasa, matematika dan statistika dalam menyelesaikan permasalahan dibidang sistem informasi.</li>\n	<li>Mempunyai kemampuan analisis untuk mengidentifikasi dan merumuskan kebutuhan data, informasi dan pengetahuan dari suatu organisasi (profit dan non-profit) atau komunitas secara koheren, detail, dan menyeluruh yang tidak terlepas dari strategi organisasi tersebut.</li>\n	<li>Mempunyai kemampuan untuk merancang, mengimplementasikan dan mengevaluasi sistem informasi yang terdiri dari beragam komponen yang saling terkait antar satu dan lainnya, seperti: infrastruktur teknologi informasi (perangkat keras dan perangkat lunak), struktur organisasi, proses &amp; prosedur dan sumber daya manusia untuk mencapai tujuan organisasi.</li>\n	<li>Mempunyai kemampuan komunikasi interpersonal dan bekerja dalam tim secara efektif untuk mencapai tujuan organisasi.</li>\n	<li>Memiliki kepribadian dan akhlak yang baik, profesional, bertanggung jawab, peka terhadap masalah sosial dan memiliki etika profesi.</li>\n	<li>Mampu menganalisis dampak lokal dan global dari solusi sistem informasi berkaitan dengan individu, organisasi dan masyarakat.</li>\n	<li>Mempunyai kemampuan untuk belajar secara mandiri seumur hidup, dan bersifat terbuka terhadap perkembangan yang ada.</li>\n	<li>Mampu memakai teknik-teknik, keterampilan dan perlengkapan rekayasa yang diperlukan didalam membangun sistem informasi.</li>\n	<li>Memiliki kemampuan untuk memahami proses bisnis dan manajemen yang mendukung untuk mengembangkan Sistem Informasi yang efisien, efektif dan adaptif.</li>\n</ol>\n\n<p>Kompetensi utama lulusan merupakan bekal untuk menjalankan peran yang dapat dilakukan setelah menjalani semua proses pembelajaran di Program Studi Sistem Informasi. Selaras dengan perkembangan masa depan bidang IT, maka pada kurikulum 2012 dirancang agar mahasiswa dapat menentukan kompetensi pilihan pada bidang-bidang seperti <em>Information System Developer, System Integrator, Database Specialist, IT Governance, Risk and Compliance Specialist dan Technopreuneur</em>. Kompetensi ini dihasilkan melalui serangkaian materi pembelajaran yang sesuai dengan matakuliah pilihan pada masing-masing peminatan mahasiswa.<strong> </strong>Profil khusus lulusan berdasarkan kurikulum 2012 dapat dilihat pada Tabel 5.2.</p>\n\n<p><strong>Tabel 5.2</strong><strong> Peran Pekerjaan Alumni Program Studi S1 Sistem Informasi (2012-2016)</strong></p>\n\n<table align="left" border="1" cellspacing="0">\n	<tbody>\n		<tr>\n			<td style="background-color:#a6a6a6; width:28.1pt">\n			<p>No</p>\n			</td>\n			<td style="background-color:#a6a6a6; width:120.5pt">\n			<p>Peran Lulusan</p>\n			</td>\n			<td style="background-color:#a6a6a6; width:265.9pt">\n			<p>Deskripsi Peran Lulusan</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:28.1pt">\n			<p><strong>1</strong></p>\n			</td>\n			<td style="width:120.5pt">\n			<p><strong><em>Information System Developer</em></strong></p>\n			</td>\n			<td style="width:265.9pt">\n			<p>Lulusan sistem informasi universitas telkom memiliki kemampuan untuk menganalisis, merancang, membuat, menguji, menerapkan dan mengevaluasi sistem informasi untuk memenuhi kebutuhan bisnis dalam lingkungan global yang kompetitif.</p>\n\n			<p><strong>Pilihan Karir:</strong></p>\n\n			<p><em>Programmer</em>, <em>Application Architect</em>, <em>Application Auditor</em>, <em>System</em><em> </em><em>/</em><em> </em><em>Business Analyst</em></p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:28.1pt">\n			<p><strong>2</strong></p>\n			</td>\n			<td style="width:120.5pt">\n			<p><strong><em>System Integrator</em></strong></p>\n			</td>\n			<td style="width:265.9pt">\n			<p>Lulusan sistem informasi universitas telkom memiliki kemampuan dalam menganalisis, memodelkan proses bisnis dan data, mengintegrasikan seluruh komponen sistem untuk menerapkan solusi berbasis sistem informasi guna membantu individu, kelompok dan organisasi dalam mencapai tujuannya.</p>\n\n			<p><strong>Pilihan Karir:</strong></p>\n\n			<p><em>Business</em><em> </em><em>Process</em><em> </em><em>Analyst</em>, <em>Enterprise</em><em> </em><em>Architect</em>, <em>ERP</em><em> </em><em>Specialist</em>, <em>IS</em><em> </em><em>Consultant</em>, <em>IS Project Manager</em></p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:28.1pt">\n			<p><strong>3</strong></p>\n			</td>\n			<td style="width:120.5pt">\n			<p><strong><em>Database Specialist</em></strong></p>\n			</td>\n			<td style="width:265.9pt">\n			<p>Lulusan sistem informasi universitas telkom memiliki kemampuan dalam menganalisis data, merancang, membangun, memelihara, dan mengintegrasikan basis data, dan memberikan rekomendasi untuk perbaikan sistem.</p>\n\n			<p><strong>Pilihan Karir:</strong></p>\n\n			<p><em>Database Designer</em>, <em>Database Programmer</em>, <em>Database</em><em> </em><em>Administrator</em>, <em>DataAnalyst</em></p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:28.1pt">\n			<p><strong>4</strong></p>\n			</td>\n			<td style="width:120.5pt">\n			<p><strong><em>IT Governance, Risk and Compliance Specialist</em></strong></p>\n			</td>\n			<td style="width:265.9pt">\n			<p>Lulusan sistem informasi universitas telkom memiliki kemampuan untuk mengukur kinerja pemanfaatan IT, melakukan identifikasi kemungkinan resiko dalam pemanfaatan IT, dan merancang rekomendasi untuk menyesuaikan penggunaan IT yang sesuai dengan kebutuhan bisnis organisasi.</p>\n\n			<p><strong>Pilihan Karir</strong><strong> </strong><strong>:</strong></p>\n\n			<p><em>IT &amp; Networking Administrator</em>, <em>System Administrator</em>, <em>Security</em><em> </em><em>Administator</em>, <em>IT</em><em> </em><em>Consultant</em><em> </em><em>and</em><em> </em><em>Compliance</em></p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:28.1pt">\n			<p><strong>5</strong></p>\n			</td>\n			<td style="width:120.5pt">\n			<p><strong><em>Technopreneur</em></strong></p>\n			</td>\n			<td style="width:265.9pt">\n			<p>Lulusan sistem informasi universitas telkom memiliki kemampuan untuk merancang dan mengembangkan bisnis berbasis inovasi dengan pemanfaatan IT sebagai solusi dalam penyelesaian masalah dimasyarakat.</p>\n			</td>\n		</tr>\n	</tbody>\n</table>\n\n<p>&nbsp;</p>\n', '<p>Dengan pertimbangan bahwa kompetensi pendukung ditujukan untuk membekali lulusan agar dapat sukses dalam kehidupan bermasyarakat dan berorganisasi. Oleh karena itu kompetensi pendukung diarahkan pada pengembangan diri <strong>(<em>personal-development</em>) yang lebih mengacu pada kemampuan <em>soft skill</em></strong>. Pada kurikulum 2012 Program Studi S1 sistem informasi universitas telkom, <strong>kompetensi pendukung lulusan</strong> ditetapkan sebagai berikut:</p>\n\n<ol>\n	<li>Mempunyai keahlian yang unggul dibidang pengembangan aplikasi perusahaan, manajemen sistem informasi, <em>digital marketing</em> atau manajemen proses bisnis perusahaan.</li>\n	<li>Memiliki pengalaman memanfaatkan perangkat lunak berbasis fungsi bisnis perusahaan.</li>\n	<li>Mempunyai pengetahuan bisnis dan penerapannya dalam dunia nyata.</li>\n</ol>\n\n<p>Penetatapan kompetensi pendukung tersebut didasarkan pada acuan KepMenDikNas No.045/U/2002 yang menyatakan bahwa, kompetensi pendukung dan kompetensi lainnya adalah kompetensi yang bersifat khusus dan gayut dengan kompetensi utama suatu program studi dan ditetapkan oleh institusi penyelenggara.</p>\n\n<p>Hal ini sejalan dengan pengolahan data oleh unit CDC yang menunjukkan bahwa industri/<em>stakeholder </em>dalam melakukan rekruitasi atau promosi sangat mempertimbangkan kompetensi <em>so</em><em>ft skill</em>. Dari hasil pengolahan data tersebut, unit CDC merekomendasikan aspek <em>soft skill </em>yang diperlukan, dapat dilihat pada Tabel 5.3.</p>\n\n<p><strong>Tabel 5. 3</strong><strong> Tingkat Urgensi 5 </strong><strong>Soft-Skill</strong><strong> Penting Berdasarkan Hasil Kepuasan Industri</strong></p>\n\n<table border="1" cellspacing="0" style="width:385.35pt">\n	<tbody>\n		<tr>\n			<td style="background-color:#bfbfbf; width:29.8pt">\n			<p>No.</p>\n			</td>\n			<td style="background-color:#bfbfbf; width:290.6pt">\n			<p>Soft Skill</p>\n			</td>\n			<td style="background-color:#bfbfbf; width:64.95pt">\n			<p>Persentase</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:29.8pt">\n			<p>1</p>\n			</td>\n			<td style="width:290.6pt">\n			<p>Integritas (Etika dan Moral)</p>\n			</td>\n			<td style="width:64.95pt">\n			<p>14%</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:29.8pt">\n			<p>2</p>\n			</td>\n			<td style="width:290.6pt">\n			<p>Keahlian Berdasarkan Bidang Ilmu (Kompetensi Utama)</p>\n			</td>\n			<td style="width:64.95pt">\n			<p>15%</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:29.8pt">\n			<p>3</p>\n			</td>\n			<td style="width:290.6pt">\n			<p>Bahasa Inggris atau Bahasa Asing Lainnya</p>\n			</td>\n			<td style="width:64.95pt">\n			<p>14%</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:29.8pt">\n			<p>4</p>\n			</td>\n			<td style="width:290.6pt">\n			<p>Penggunaan Teknologi Informasi</p>\n			</td>\n			<td style="width:64.95pt">\n			<p>15%</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:29.8pt">\n			<p>5</p>\n			</td>\n			<td style="width:290.6pt">\n			<p>Komunikasi</p>\n			</td>\n			<td style="width:64.95pt">\n			<p>14%</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:29.8pt">\n			<p>6</p>\n			</td>\n			<td style="width:290.6pt">\n			<p>Kerjasama Tim</p>\n			</td>\n			<td style="width:64.95pt">\n			<p>14%</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:29.8pt">\n			<p>7</p>\n			</td>\n			<td style="width:290.6pt">\n			<p>Pengembangan Tim</p>\n			</td>\n			<td style="width:64.95pt">\n			<p>14%</p>\n			</td>\n		</tr>\n	</tbody>\n</table>\n\n<p>(Sumber: CDC Universitas Telkom)</p>\n', '<p><strong>Kompetensi lainnya</strong> lulusan Program Studi S-1 Sistem Informasi universitas telkom ditetapkan sebagai berikut:</p>\n\n<ol>\n	<li>Mengerti dan memahami ajaran salah satu agama yang diakui secara resmi di Indonesia.</li>\n	<li>Menghayati nilai-nilai Pancasila dan sejarah perjuangan bangsa Indonesia dan Undang Undang Dasar 1945 dalam kehidupan bermasyarakat.</li>\n</ol>\n\n<p>Penetapan kompetensi lainnya didasarkan atas KepMenDikNas No.045/ U/2002 yang menyatakan bahwa, kompetensi pendukung dan kompetensi lainnya adalah kompetensi yang bersifat khusus dan gayut dengan kompetensi utama suatu program studi dan ditetapkan oleh institusi penyelenggara. Program Studi S-1 Sistem Informasi Universitas Telkom menetapkan kompetensi lainnya dengan pertimbangan bahwa setiap mahasiswa harus memiliki landasan keimanan dan landasan kenegaraan yang kuat sebagai bentuk tanggung jawab warga negara Republik Indonesia yang baik.</p>\n');

-- --------------------------------------------------------

--
-- Table structure for table `std5_513matkulpilihan`
--

CREATE TABLE `std5_513matkulpilihan` (
  `no` int(20) NOT NULL,
  `semester` varchar(75) NOT NULL,
  `kode_mk` varchar(25) NOT NULL,
  `nama_mk_pilihan` varchar(75) NOT NULL,
  `bobot_sks` varchar(25) NOT NULL,
  `bobot_tugas` varchar(25) NOT NULL,
  `unit_jur_fak_pengelola` varchar(100) NOT NULL,
  `kelompok_mk_pilihan` varchar(100) NOT NULL,
  `excel_path` varchar(255) NOT NULL,
  `excelhtml_path` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `std5_513matkulpilihan`
--

INSERT INTO `std5_513matkulpilihan` (`no`, `semester`, `kode_mk`, `nama_mk_pilihan`, `bobot_sks`, `bobot_tugas`, `unit_jur_fak_pengelola`, `kelompok_mk_pilihan`, `excel_path`, `excelhtml_path`) VALUES
(1, 'Ganjil/Genap', 'ISG4H3', 'Interaksi Manusia dan Komputer', '3', 'Adaa', 'Fakultas Rekayasa Industri', 'Application Development', 'uploads/dokumenpendukungstd5/std5_513matkulpilihan/datastandar5-5.1.3Mataku', 'uploads/dokumenpendukungstd5/std5_513matkulpilihan/datastandar5-5.1.3Mataku'),
(2, 'Ganjil/Genap', 'ISG4I3', 'Business Intelligence', '3', 'Ada', 'Fakultas Rekayasa Industri', 'Application Development', 'uploads/dokumenpendukungstd5/std5_513matkulpilihan/datastandar5-5.1.3Mataku', 'uploads/dokumenpendukungstd5/std5_513matkulpilihan/datastandar5-5.1.3Mataku'),
(3, 'Ganjil/Genap', 'ISG4J3', 'Manajemen Basis Data', '3', 'Ada', 'Fakultas Rekayasa Industri', 'Application Development', 'uploads/dokumenpendukungstd5/std5_513matkulpilihan/datastandar5-5.1.3Mataku', 'uploads/dokumenpendukungstd5/std5_513matkulpilihan/datastandar5-5.1.3Mataku'),
(4, 'Ganjil/Genap', 'ISG4K3', 'Mobile Application Development', '3', 'Ada', 'Fakultas Rekayasa Industri', 'Application Development', 'uploads/dokumenpendukungstd5/std5_513matkulpilihan/datastandar5-5.1.3Mataku', 'uploads/dokumenpendukungstd5/std5_513matkulpilihan/datastandar5-5.1.3Mataku'),
(5, 'Ganjil/Genap', 'ISG4L3', 'Enterprise Application Development', '3', 'Ada', 'Fakultas Rekayasa Industri', 'Application Development', 'uploads/dokumenpendukungstd5/std5_513matkulpilihan/datastandar5-5.1.3Mataku', 'uploads/dokumenpendukungstd5/std5_513matkulpilihan/datastandar5-5.1.3Mataku'),
(6, 'Ganjil/Genap', 'ISG4M3', 'Perencanaan Strategi Sistem Informasi', '3', 'Ada', 'Fakultas Rekayasa Industri', 'IS  Management', 'uploads/dokumenpendukungstd5/std5_513matkulpilihan/datastandar5-5.1.3Mataku', 'uploads/dokumenpendukungstd5/std5_513matkulpilihan/datastandar5-5.1.3Mataku'),
(7, 'Ganjil/Genap', 'ISG4N3', 'Manajemen Risiko dan Keamanan Informasi', '3', 'Ada', 'Fakultas Rekayasa Industri', 'IS  Management', 'uploads/dokumenpendukungstd5/std5_513matkulpilihan/datastandar5-5.1.3Mataku', 'uploads/dokumenpendukungstd5/std5_513matkulpilihan/datastandar5-5.1.3Mataku'),
(8, 'Ganjil/Genap', 'ISG4O3', 'Audit dan Kontrol Sistem Informasi', '3', 'Ada', 'Fakultas Rekayasa Industri', 'IS  Management', 'uploads/dokumenpendukungstd5/std5_513matkulpilihan/datastandar5-5.1.3Mataku', 'uploads/dokumenpendukungstd5/std5_513matkulpilihan/datastandar5-5.1.3Mataku'),
(9, 'Ganjil/Genap', 'ISG4P3', 'Enterprise Information Management', '3', 'Ada', 'Fakultas Rekayasa Industri', 'IS  Management', 'uploads/dokumenpendukungstd5/std5_513matkulpilihan/datastandar5-5.1.3Mataku', 'uploads/dokumenpendukungstd5/std5_513matkulpilihan/datastandar5-5.1.3Mataku'),
(10, 'Ganjil/Genap', 'ISG4Q3', 'E- Commerce', '3', 'Ada', 'Fakultas Rekayasa Industri', 'e-commerce', 'uploads/dokumenpendukungstd5/std5_513matkulpilihan/datastandar5-5.1.3Mataku', 'uploads/dokumenpendukungstd5/std5_513matkulpilihan/datastandar5-5.1.3Mataku'),
(11, 'Ganjil/Genap', 'ISG4R3', 'Digital Marketing', '3', 'Ada', 'Fakultas Rekayasa Industri', 'e-commerce', 'uploads/dokumenpendukungstd5/std5_513matkulpilihan/datastandar5-5.1.3Mataku', 'uploads/dokumenpendukungstd5/std5_513matkulpilihan/datastandar5-5.1.3Mataku'),
(12, 'Ganjil/Genap', 'IEG4R3', 'Customer Relationship Management', '3', 'Ada', 'Fakultas Rekayasa Industri', 'e-commerce', 'uploads/dokumenpendukungstd5/std5_513matkulpilihan/datastandar5-5.1.3Mataku', 'uploads/dokumenpendukungstd5/std5_513matkulpilihan/datastandar5-5.1.3Mataku'),
(13, 'Ganjil/Genap', 'IEG4S3', 'Marketing Research', '3', 'Ada', 'Fakultas Rekayasa Industri', 'e-commerce', 'uploads/dokumenpendukungstd5/std5_513matkulpilihan/datastandar5-5.1.3Mataku', 'uploads/dokumenpendukungstd5/std5_513matkulpilihan/datastandar5-5.1.3Mataku'),
(14, 'Ganjil/Genap', 'BUG2A3', 'Bahasa Inggris 3', '3', 'Ada', 'Fakultas Rekayasa Industri', 'Umum', 'uploads/dokumenpendukungstd5/std5_513matkulpilihan/datastandar5-5.1.3Mataku', 'uploads/dokumenpendukungstd5/std5_513matkulpilihan/datastandar5-5.1.3Mataku'),
(15, 'Ganjil/Genap', 'ISG4V3', 'Pelatihan & Sertifikasi Internasional', '3', 'Ada', 'Fakultas Rekayasa Industri', 'Umum', 'uploads/dokumenpendukungstd5/std5_513matkulpilihan/datastandar5-5.1.3Mataku', 'uploads/dokumenpendukungstd5/std5_513matkulpilihan/datastandar5-5.1.3Mataku'),
(16, 'Total SKS', '', '', '45', '', '', '', 'uploads/dokumenpendukungstd5/std5_513matkulpilihan/datastandar5-5.1.3Mataku', 'uploads/dokumenpendukungstd5/std5_513matkulpilihan/datastandar5-5.1.3Mataku');

-- --------------------------------------------------------

--
-- Table structure for table `std5_514substansipraktikum`
--

CREATE TABLE `std5_514substansipraktikum` (
  `no` int(75) NOT NULL,
  `nama_praktikum_praktek` varchar(75) NOT NULL,
  `isi_praktikum_praktek_judul_modul` varchar(255) NOT NULL,
  `isi_praktikum_praktek_jam_pelaksanaan` varchar(30) NOT NULL,
  `tempat_lokasi_praktikum_praktek` varchar(50) NOT NULL,
  `excel_path` varchar(255) NOT NULL,
  `excelhtml_path` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `std5_514substansipraktikum`
--

INSERT INTO `std5_514substansipraktikum` (`no`, `nama_praktikum_praktek`, `isi_praktikum_praktek_judul_modul`, `isi_praktikum_praktek_jam_pelaksanaan`, `tempat_lokasi_praktikum_praktek`, `excel_path`, `excelhtml_path`) VALUES
(1, 'Fisika 1', 'Modul 1 : Ketidakpastian pada Pengolahan Data', '3 jam', 'Laboratorium Fisika Dasar', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(2, 'Fisika 1', 'Modul 2 : Gerak    Lurus    Beraturan dan Berubah Beraturan', '3 jam', 'Laboratorium Fisika Dasar', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(3, 'Fisika 1', 'Modul 3 : Gerakan Melingkar Beraturan', '3 jam', 'Laboratorium Fisika Dasar', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(4, 'Fisika 1', 'Modul 4 : Gerak Osilasi dan Jatuh Bebas', '3 jam', 'Laboratorium Fisika Dasar', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(5, 'Fisika 1', 'Modul 5 : Resonansi   Gelombang Bunyi', '3 jam', 'Laboratorium Fisika Dasar', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(6, 'Fisika 1', 'Modul 6 : Superposisi     Getaran Harmonik', '3 jam', 'Laboratorium Fisika Dasar', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(7, 'Fisika 2', 'Modul 1 : Ketidakpastian pada Pengolahan Data', '3 jam', 'Laboratorium Fisika Dasar', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(8, 'Fisika 2', 'Modul 2 : Alat  Ukur  dan  Besaran Listrik', '3 jam', 'Laboratorium Fisika Dasar', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(9, 'Fisika 2', 'Modul 3 : Pengisian dan Pengosongan Muatan pada Kapasitor', '3 jam', 'Laboratorium Fisika Dasar', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(10, 'Fisika 2', 'Modul 4 : Jembatan Wheatston', '3 jam', 'Laboratorium Fisika Dasar', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(11, 'Fisika 2', 'Modul 5 : Induksi Magnet', '3 jam', 'Laboratorium Fisika Dasar', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(12, 'Praktikum Sistem Enterprise', 'Modul 1 : Introduction', '3 jam', 'Gedung Kuliah Umum Fakultas Rekayasa Industri Ruan', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(13, 'Praktikum Sistem Enterprise', 'Modul 2 : Navigation', '3 jam', 'Gedung Kuliah Umum Fakultas Rekayasa Industri Ruan', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(14, 'Praktikum Sistem Enterprise', 'Modul 3 : System Wide Concept', '3 jam', 'Gedung Kuliah Umum Fakultas Rekayasa Industri Ruan', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(15, 'Praktikum Sistem Enterprise', 'Modul 4 : Logistik', '3 jam', 'Gedung Kuliah Umum Fakultas Rekayasa Industri Ruan', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(16, 'Praktikum Sistem Enterprise', 'Modul 5 : Financials', '3 jam', 'Gedung Kuliah Umum Fakultas Rekayasa Industri Ruan', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(17, 'Praktikum Sistem Enterprise', 'Modul 6 : Human Capital Management', '3 jam', 'Gedung Kuliah Umum Fakultas Rekayasa Industri Ruan', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(18, 'Praktikum Sistem Enterprise', 'Modul 7 : mySAP Technology', '3 jam', 'Gedung Kuliah Umum Fakultas Rekayasa Industri Ruan', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(19, 'Praktikum Algoritma Pemograman', 'Modul 1 : Variabel dan Tipe Data', '2 Jam', 'Common Lab Gedung C Fakultas Rekayasa Industri lan', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(20, 'Praktikum Algoritma Pemograman', 'Modul 2 : If , Else, if else, Switch', '2 Jam', 'Common Lab Gedung C Fakultas Rekayasa Industri lan', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(21, 'Praktikum Algoritma Pemograman', 'Modul 3 : Looping', '2 Jam', 'Common Lab Gedung C Fakultas Rekayasa Industri lan', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(22, 'Praktikum Algoritma Pemograman', 'Modul 4 : Method dan Greenfoot', '2 Jam', 'Common Lab Gedung C Fakultas Rekayasa Industri lan', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(23, 'Praktikum Algoritma Pemograman', 'Modul 5 : File Processing', '2 Jam', 'Common Lab Gedung C Fakultas Rekayasa Industri lan', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(24, 'Praktikum Algoritma Pemograman', 'Modul 6 : Basic OOP', '2 Jam', 'Common Lab Gedung C Fakultas Rekayasa Industri lan', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(25, 'Dasar Jaringan + Praktikum', 'Modul 1 : Network Fundamental (Part 1) ', '2 Jam', 'Common Lab Gedung C Fakultas Rekayasa Industri lan', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(26, 'Dasar Jaringan + Praktikum', 'Modul 2 : Network Fundamental (Part 2)', '2 Jam', 'Common Lab Gedung C Fakultas Rekayasa Industri lan', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(27, 'Dasar Jaringan + Praktikum', 'Modul 3 : Cabling, Network Access & Ethernet', '2 Jam', 'Common Lab Gedung C Fakultas Rekayasa Industri lan', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(28, 'Dasar Jaringan + Praktikum', 'Modul 4 : GNS 3', '2 Jam', 'Common Lab Gedung C Fakultas Rekayasa Industri lan', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(29, 'Dasar Jaringan + Praktikum', 'Modul 5 : IPv4 & IPv 6', '2 Jam', 'Common Lab Gedung C Fakultas Rekayasa Industri lan', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(30, 'Dasar Jaringan + Praktikum', 'Modul 6 : Konfigurasi Server dan Telnet', '2 Jam', 'Common Lab Gedung C Fakultas Rekayasa Industri lan', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(31, 'Sistem Operasi + Praktikum', 'Modul 1 : Pengenalan Dasar Sistem Operasi, File Management, dan User Management', '2 Jam', 'Common Lab Gedung C Fakultas Rekayasa Industri lan', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(32, 'Sistem Operasi + Praktikum', 'Modul 2 : Vi Editor & Network Configuration', '2 Jam', 'Common Lab Gedung C Fakultas Rekayasa Industri lan', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(33, 'Sistem Operasi + Praktikum', 'Modul 3 : Remote Access (Telnet, SSH, dan Remote Desktop)', '2 Jam', 'Common Lab Gedung C Fakultas Rekayasa Industri lan', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(34, 'Sistem Operasi + Praktikum', 'Modul 4 : Web Server & FTP Server', '2 Jam', 'Common Lab Gedung C Fakultas Rekayasa Industri lan', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(35, 'Sistem Operasi + Praktikum', 'Modul 5 : DNS Server', '2 Jam', 'Common Lab Gedung C Fakultas Rekayasa Industri lan', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(36, 'Sistem Operasi + Praktikum', 'Modul 6 : Mail Server', '2 Jam', 'Common Lab Gedung C Fakultas Rekayasa Industri lan', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(37, 'Sistem Operasi + Praktikum', 'Modul 7 : Bash Scripting', '2 Jam', 'Common Lab Gedung C Fakultas Rekayasa Industri lan', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(38, 'Jaringan Komputer + Praktikum', 'Modul 1: Routing Konsep & Static Routing', '2 Jam', 'Common Lab Gedung C Fakultas Rekayasa Industri lan', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(39, 'Jaringan Komputer + Praktikum', 'Modul 2: RIPv2 & RIPng', '2 Jam', 'Common Lab Gedung C Fakultas Rekayasa Industri lan', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(40, 'Jaringan Komputer + Praktikum', 'Modul 3: OSPF', '2 Jam', 'Common Lab Gedung C Fakultas Rekayasa Industri lan', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(41, 'Jaringan Komputer + Praktikum', 'Modul 4 : VLAN, Inter VLAN, VTP', '2 Jam', 'Common Lab Gedung C Fakultas Rekayasa Industri lan', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(42, 'Jaringan Komputer + Praktikum', 'Modul 5 : ACL', '2 Jam', 'Common Lab Gedung C Fakultas Rekayasa Industri lan', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(43, 'Jaringan Komputer + Praktikum', 'Modul 6 : DHCP (Dynamic Host Configuration Protocol)', '2 Jam', 'Common Lab Gedung C Fakultas Rekayasa Industri lan', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(44, 'Jaringan Komputer + Praktikum', 'Modul 7 : NAT', '2 Jam', 'Common Lab Gedung C Fakultas Rekayasa Industri lan', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(45, 'Manajemen Proses Bisnis + Praktikum', 'Modul 1 : Analisis Proses Bisnis', '2 Jam', 'Common Lab Gedung C Fakultas Rekayasa Industri lan', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(46, 'Manajemen Proses Bisnis + Praktikum', 'Modul 2 : Pengenalan Mega dan BPNM', '2 Jam', 'Common Lab Gedung C Fakultas Rekayasa Industri lan', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(47, 'Manajemen Proses Bisnis + Praktikum', 'Modul 3 : Proses Bisnis Eksisting', '2 Jam', 'Common Lab Gedung C Fakultas Rekayasa Industri lan', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(48, 'Manajemen Proses Bisnis + Praktikum', 'Modul 4 : Proses Bisnis Improvement', '2 Jam', 'Common Lab Gedung C Fakultas Rekayasa Industri lan', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(49, 'Manajemen Proses Bisnis + Praktikum', 'Modul 5 : Proses Bisnis Tanrget', '2 Jam', 'Common Lab Gedung C Fakultas Rekayasa Industri lan', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(50, 'Manajemen Proses Bisnis + Praktikum', 'Modul 6 : BPM Teknologi dan Simulasi Proses Bisnis', '2 Jam', 'Common Lab Gedung C Fakultas Rekayasa Industri lan', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(51, 'Object Oriented Programming + Praktikum', 'Modul 1 : Class & Object', '2 Jam', 'Common Lab Gedung C Fakultas Rekayasa Industri lan', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(52, 'Object Oriented Programming + Praktikum', 'Modul 2 : Inheritance & Generalization', '2 Jam', 'Common Lab Gedung C Fakultas Rekayasa Industri lan', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(53, 'Object Oriented Programming + Praktikum', 'Modul 3 : Abstract, Interface, & Polymorphism', '2 Jam', 'Common Lab Gedung C Fakultas Rekayasa Industri lan', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(54, 'Object Oriented Programming + Praktikum', 'Modul 4 : Overriding & Overloading', '2 Jam', 'Common Lab Gedung C Fakultas Rekayasa Industri lan', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(55, 'Object Oriented Programming + Praktikum', 'Modul 5 : GUI & Event Handler', '2 Jam', 'Common Lab Gedung C Fakultas Rekayasa Industri lan', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(56, 'Object Oriented Programming + Praktikum', 'Modul 6 : IO Handling & exception', '2 Jam', 'Common Lab Gedung C Fakultas Rekayasa Industri lan', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(57, 'Struktur Data + Praktikum', 'Modul 1 : Array', '2 Jam', 'Common Lab Gedung C Fakultas Rekayasa Industri lan', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(58, 'Struktur Data + Praktikum', 'Modul 2 : Stack and Queue', '2 Jam', 'Common Lab Gedung C Fakultas Rekayasa Industri lan', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(59, 'Struktur Data + Praktikum', 'Modul 3 : Linked List dan Doble Lnked List', '2 Jam', 'Common Lab Gedung C Fakultas Rekayasa Industri lan', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(60, 'Struktur Data + Praktikum', 'Modul 4 : Tree', '2 Jam', 'Common Lab Gedung C Fakultas Rekayasa Industri lan', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(61, 'Struktur Data + Praktikum', 'Modul 5 : Graph & Weighted Graph', '2 Jam', 'Common Lab Gedung C Fakultas Rekayasa Industri lan', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(62, 'Struktur Data + Praktikum', 'Modul 6 : Heap', '2 Jam', 'Common Lab Gedung C Fakultas Rekayasa Industri lan', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(63, 'Struktur Data + Praktikum', 'Modul 7 : Sorting', '2 Jam', 'Common Lab Gedung C Fakultas Rekayasa Industri lan', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(64, 'Basis Data + Praktikum', 'Modul 1 : SQL & Data Definition Languange', '2 Jam', 'Common Lab Gedung C Fakultas Rekayasa Industri lan', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(65, 'Basis Data + Praktikum', 'Modul 2 : Data Manipulation Languange', '2 Jam', 'Common Lab Gedung C Fakultas Rekayasa Industri lan', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(66, 'Basis Data + Praktikum', 'Modul 3 : Nested SQL', '2 Jam', 'Common Lab Gedung C Fakultas Rekayasa Industri lan', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(67, 'Basis Data + Praktikum', 'Modul 4 : JOIN', '2 Jam', 'Common Lab Gedung C Fakultas Rekayasa Industri lan', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(68, 'Basis Data + Praktikum', 'Modul 5 : Stored Procedure', '2 Jam', 'Common Lab Gedung C Fakultas Rekayasa Industri lan', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(69, 'Basis Data + Praktikum', 'Modul 6 : Trigger', '2 Jam', 'Common Lab Gedung C Fakultas Rekayasa Industri lan', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(70, 'Analisis Perancangan Sistem Informasi + Praktikum', 'Modul 1 : Analisis Kebutuhan Sistem', '2 Jam', 'Common Lab Gedung C Fakultas Rekayasa Industri lan', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(71, 'Analisis Perancangan Sistem Informasi + Praktikum', 'Modul 2 : Activity Diagram', '2 Jam', 'Common Lab Gedung C Fakultas Rekayasa Industri lan', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(72, 'Analisis Perancangan Sistem Informasi + Praktikum', 'Modul 3 :  Class Diagram dan Activity Diagram', '2 Jam', 'Common Lab Gedung C Fakultas Rekayasa Industri lan', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(73, 'Analisis Perancangan Sistem Informasi + Praktikum', 'Modul 4 : Sequence Diagram', '2 Jam', 'Common Lab Gedung C Fakultas Rekayasa Industri lan', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(74, 'Analisis Perancangan Sistem Informasi + Praktikum', 'Modul 5 : Component Diagram', '2 Jam', 'Common Lab Gedung C Fakultas Rekayasa Industri lan', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(75, 'Analisis Perancangan Sistem Informasi + Praktikum', 'Modul 6 : State Transition Diagram', '2 Jam', 'Common Lab Gedung C Fakultas Rekayasa Industri lan', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(76, 'Enterprise Architecture + Praktikum', 'Modul 1 : Pengenalan Togaf', '2 Jam', 'Common Lab Gedung C Fakultas Rekayasa Industri lan', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(77, 'Enterprise Architecture + Praktikum', 'Modul 2 : Architecture Vision', '2 Jam', 'Common Lab Gedung C Fakultas Rekayasa Industri lan', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(78, 'Enterprise Architecture + Praktikum', 'Modul 3 : Arsitektur Model', '2 Jam', 'Common Lab Gedung C Fakultas Rekayasa Industri lan', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(79, 'Enterprise Architecture + Praktikum', 'Modul 4 : Information System Architecture :Data Architecture', '2 Jam', 'Common Lab Gedung C Fakultas Rekayasa Industri lan', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(80, 'Enterprise Architecture + Praktikum', 'Modul 5 : Information System Architecture : Application Architecture', '2 Jam', 'Common Lab Gedung C Fakultas Rekayasa Industri lan', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(81, 'Enterprise Architecture + Praktikum', 'Modul 6 : Technology Architecture', '2 Jam', 'Common Lab Gedung C Fakultas Rekayasa Industri lan', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(82, 'Web Application Development + Praktikum', 'Modul 1 : HTML & CSS', '2 Jam', 'Common Lab Gedung C Fakultas Rekayasa Industri lan', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(83, 'Web Application Development + Praktikum', 'Modul 2 : Java Script', '2 Jam', 'Common Lab Gedung C Fakultas Rekayasa Industri lan', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(84, 'Web Application Development + Praktikum', ' Modul 3: MVC', '2 Jam', 'Common Lab Gedung C Fakultas Rekayasa Industri lan', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(85, 'Web Application Development + Praktikum', ' Modul 4 : JSP', '2 Jam', 'Common Lab Gedung C Fakultas Rekayasa Industri lan', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(86, 'Manajemen Rantai Pasok + Praktikum', 'Modul 1 :  SCM 100 : Overview of Supply chain Planning', '2 Jam', 'Common Lab Gedung C Fakultas Rekayasa Industri lan', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(87, 'Manajemen Rantai Pasok + Praktikum', 'Modul 1 :  SCM 300 : Introduction', '2 Jam', 'Common Lab Gedung C Fakultas Rekayasa Industri lan', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(88, 'Manajemen Rantai Pasok + Praktikum', 'Modul 2 :  SCM 100 : Master Data', '2 Jam', 'Common Lab Gedung C Fakultas Rekayasa Industri lan', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(89, 'Manajemen Rantai Pasok + Praktikum', 'Modul 2  SCM 300 : Order-Controlled Production with Production Orders', '2 Jam', 'Common Lab Gedung C Fakultas Rekayasa Industri lan', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(90, 'Manajemen Rantai Pasok + Praktikum', 'Modul 3 :  SCM 100 : Demand Planning', '2 Jam', 'Common Lab Gedung C Fakultas Rekayasa Industri lan', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(91, 'Manajemen Rantai Pasok + Praktikum', 'Modul 3 :  SCM 300 : Order-Controlled Production with Process Orders', '2 Jam', 'Common Lab Gedung C Fakultas Rekayasa Industri lan', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(92, 'Manajemen Rantai Pasok + Praktikum', 'Modul 4 :  SCM 100 : Demand Management', '2 Jam', 'Common Lab Gedung C Fakultas Rekayasa Industri lan', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(93, 'Manajemen Rantai Pasok + Praktikum', 'Modul 4 :  SCM 300 : Repetitive Manufacturing', '2 Jam', 'Common Lab Gedung C Fakultas Rekayasa Industri lan', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(94, 'Manajemen Rantai Pasok + Praktikum', 'Modul 5 :  SCM 100 : Long-Term Planning', '2 Jam', 'Common Lab Gedung C Fakultas Rekayasa Industri lan', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(95, 'Manajemen Rantai Pasok + Praktikum', 'Modul 5 :  SCM 300 : KANBAN', '2 Jam', 'Common Lab Gedung C Fakultas Rekayasa Industri lan', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(96, 'Manajemen Rantai Pasok + Praktikum', 'Modul 6 :  SCM 100 : Material Requirements Planning', '2 Jam', 'Common Lab Gedung C Fakultas Rekayasa Industri lan', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(97, 'Manajemen Rantai Pasok + Praktikum', 'Modul 6 :  SCM 300 : Project-Oriented Production', '2 Jam', 'Common Lab Gedung C Fakultas Rekayasa Industri lan', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(98, 'Keamanan Sistem Informasi + Praktikum', 'Modul 1 : Windows Firewall', '2 Jam', 'Common Lab Gedung C Fakultas Rekayasa Industri lan', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(99, 'Keamanan Sistem Informasi + Praktikum', 'Modul 2 : Information Gathering & Report', '2 Jam', 'Common Lab Gedung C Fakultas Rekayasa Industri lan', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(100, 'Keamanan Sistem Informasi + Praktikum', 'Modul 3 : Web Security with DVWA', '2 Jam', 'Common Lab Gedung C Fakultas Rekayasa Industri lan', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(101, 'Keamanan Sistem Informasi + Praktikum', 'Modul 4 : CISCO Access Control List', '2 Jam', 'Common Lab Gedung C Fakultas Rekayasa Industri lan', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(102, 'Keamanan Sistem Informasi + Praktikum', 'Modul 5 : Proxy Server', '2 Jam', 'Common Lab Gedung C Fakultas Rekayasa Industri lan', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S'),
(103, 'Keamanan Sistem Informasi + Praktikum', 'Modul 6 : Penetration Testing Menggunakan Kali Linux, Nmap dan Metasploit', '2 Jam', 'Common Lab Gedung C Fakultas Rekayasa Industri lan', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S', 'uploads/dokumenpendukungstd5/std5_514substansipraktikum/datastandar5-5.1.4S');

-- --------------------------------------------------------

--
-- Table structure for table `std5_531mekanismepenyusunan`
--

CREATE TABLE `std5_531mekanismepenyusunan` (
  `no` int(5) NOT NULL,
  `mekanisme_penyusunan` varchar(65000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `std5_531mekanismepenyusunan`
--

INSERT INTO `std5_531mekanismepenyusunan` (`no`, `mekanisme_penyusunan`) VALUES
(1, '<p><strong>Mekanisme Penyusunan Materi Kuliah</strong></p>\n\n<p>aUntuk mencapai capaian pembelajaran yang telah didefinisikan maka diperlukan bahan kajian yang akan diajarkan untuk didistribusikan ke dalam bentuk mata kuliah. Bahan kajian diperoleh dari rekomendasi <em>Association of Information System</em> (AIS) dan <em>ACM IS 2010</em>, mata kuliah universitas, dan kebutuhan industri untuk menjawab tantangan 5&ndash;10 tahun mendatang. Tabel 6.1 di bawah ini menunjukkan perbandingan bahan kajian dari masing-masing referensi untuk menentukan bahan kajian bagi program studi S1 sistem informasi Universitas Telkom.</p>\n\n<p>&nbsp;</p>\n\n<p><strong>Tabel 5. </strong><strong>14 Pemetaan dan Perbandingan Bahan Kajian Dari Beberapa Referensi</strong></p>\n\n<p>&nbsp;</p>\n\n<p>Berdasarkan visi keilmuan dan analisis kebutuhan, pemetaan bahan kajian pembelajaran di program studi S1 sistem informasi ditentukan. Bahan kajian yang akan diajarkan pada mahasiswa dibentuk sebagai mata kuliah yang ditempuh selama 4 tahun masa studi dengan total bobot SKS adalah 145 SKS yang dapat dilihat pada Gambar 5.9 sebagai berikut.</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p><strong>Gambar 5. </strong><strong>9 Interlink Alur dalam Penyusunan Capaian Pembelajaran</strong></p>\n\n<p>Secara rutin dilakukan evaluasi terhadap silabus, SAP dan GBPP dalam perkuliahan sebagai bagian dari evaluasi kurikulum jangka pendek. Secara formal rapat kordinasi menjadi salah satu sarana untuk menjamin keseragaman pelaksanaan mata kuliah khususnya pada kelas paralel. Program studi memberikan arahan bahwa untuk setiap semester dilaksanakan sebanyak 4 (empat) kali rapat koordinasi seperti dijelaskan pada Gambar 5.10.</p>\n\n<p>&nbsp;</p>\n\n<p><strong>Gambar 5. </strong><strong>10</strong><strong> Pola Rapat Koordinasi Pengampuan Perkuliahan</strong></p>\n\n<p>&nbsp;</p>\n\n<p>Selain dalam bentuk formal maka proses evaluasi dan penyesuaian materi kuliah juga dilakukan oleh tim-dosen pengajar selama perkuliahan untuk membahas masalah yang ditemukan seiring berjalannya proses pengajaran, pencapaian materi, penyeragaman <em>slide kuliah</em>, soal kuis dan tugas dan hal lain yang penting seperti mekanisme pengumpulan tugas besar dan lain-lain. Proses evaluasi tersebut dipimpin oleh seorang kordinator mata kuliah yang ditetapkan oleh program studi.</p>\n\n<p><strong>Mekanisme Monitoring Perkuliahan</strong></p>\n\n<p>Mekanisme untuk memonitor perkuliahan, antara lain kehadiran dosen dan mahasiswa, serta materi perkuliahan ditunjukkan pada tabel berikut.</p>\n\n<p><strong>Tabel 5. </strong><strong>20</strong><strong> Mekanisme Kegiatan Monitoring Perkuliahan</strong></p>\n\n<table border="1" cellspacing="0" style="width:493px">\n	<tbody>\n		<tr>\n			<td style="background-color:#a0a0a0; width:29.4pt">\n			<p><strong>No</strong></p>\n			</td>\n			<td style="background-color:#a0a0a0; width:120.65pt">\n			<p><strong>Mekanisme</strong></p>\n			</td>\n			<td style="background-color:#a0a0a0; width:219.7pt">\n			<p><strong>Kegiatan</strong></p>\n			</td>\n		</tr>\n		<tr>\n			<td rowspan="5" style="width:29.4pt">\n			<p>1</p>\n			</td>\n			<td rowspan="5" style="width:120.65pt">\n			<p>Dosen :</p>\n\n			<p>Kehadiran dosen / proses tatap muka</p>\n			</td>\n			<td style="width:219.7pt">\n			<p>Adanya berita acara perkuliahan (BAP )</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:219.7pt">\n			<p>Adanya daftar presensi dosen dari Fakultas</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:219.7pt">\n			<p>RFID <em>reader </em>di setiap ruang perkuliahan dan di dekat ruang kerja dosen</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:219.7pt">\n			<p>Aplikasi pemantau kehadiran dosen yang terintegrasi dengan i-Gracias</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:219.7pt">\n			<p>Adanya kuisioner EDOM untuk mendapatkan <em>feedback</em> dari mahasiswa tentang kinerja dosen dikelas</p>\n			</td>\n		</tr>\n		<tr>\n			<td rowspan="8" style="width:29.4pt">\n			<p>2</p>\n			</td>\n			<td rowspan="8" style="width:120.65pt">\n			<p>Mahasiswa :</p>\n\n			<p>Proses monitoring Tatap Muka</p>\n			</td>\n			<td style="width:219.7pt">\n			<p>Daftar hadir kuliah</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:219.7pt">\n			<p>Kartu tanda mahasiswa (KTM) yang dilengkapi teknologi RFID untuk presensi mahasiswa</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:219.7pt">\n			<p>RFID <em>reader </em>disetiap ruang perkuliahan</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:219.7pt">\n			<p>Aplikasi pemantau kehadiran mahasiswa yang terintegrasi dengan i-Gracias</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:219.7pt">\n			<p>Mahasiswa diwajibkan hadir 75% dari tatap muka.</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:219.7pt">\n			<p>Adanya berita acara perkuliahan dan berita acara praktikum</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:219.7pt">\n			<p>Adanya laporan untuk mata kuliah lapangan seperti kerja praktek</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:219.7pt">\n			<p>Ada dosen yang bertugas memonitor pelaksanaan praktikum di laboratorium</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:29.4pt">\n			<p>3</p>\n			</td>\n			<td style="width:120.65pt">\n			<p>Materi perkuliahan</p>\n			</td>\n			<td style="width:219.7pt">\n			<p>Adanya pengecekan kesesuaian antara BAP dengan SAP</p>\n			</td>\n		</tr>\n	</tbody>\n</table>\n\n<p>&nbsp;</p>\n\n<p><strong>Penjelasan Mekanisme Monitoring Perkuliahan: </strong></p>\n\n<p><strong>Tatap Muka :</strong></p>\n\n<ol>\n	<li>Kegiatan Perkuliahan</li>\n</ol>\n\n<ol>\n	<li>Sesuai dengan jadwal perkuliahan yang dikeluarkan oleh institusi. Lamanya tatap muka dihitung berdasarkan Satuan Kredit Semester (SKS), dimana 1 SKS per 1 jam perkuliahan setara dengan 50 menit.</li>\n</ol>\n\n<p>Keterangan :</p>\n\n<p>Mata Kuliah 2 SKS disediakan 1 (satu) kali pertemuan setiap minggu dengan 14 minggu perkuliahan dengan ketentuan sebagai berikut :</p>\n\n<ul>\n	<li>Setiap pertemuan 2 SKS per 2 jam perkuliahan</li>\n	<li>Jumlah pertemuan satu semester 14 pertemuan (28 jam perkuliahan) + UTS + UAS</li>\n</ul>\n\n<ol>\n	<li>Mata Kuliah 3 SKS disediakan 2 (dua) kali pertemuan setiap minggu dengan 14 minggu perkuliahan, dengan ketentuan sebagi berikut :</li>\n</ol>\n\n<ul>\n	<li>Setiap pertemuan 2 SKS per 2 jam perkuliahan</li>\n	<li>Setiap minggu terdiri dari 3 SKS per 3 jam perkuliahan</li>\n	<li>Setiap minggu terdiri dari 1 SKS per 1 jam responsi</li>\n	<li>Jumlah pertemuan 100 persen perkuliahan satu semester adalah 42 jam perkuliahan</li>\n	<li>Jumlah pertemuan satu semester adalah 42 jam perkuliahan + responsi + UTS + UAS</li>\n</ul>\n\n<ol>\n	<li>Mata Kuliah 4 SKS disediakan 3 (tiga) kali pertemuan setiap minggu dengan 14 minggu perkuliahan, dengan ketentuan sebagai berikut :</li>\n</ol>\n\n<ul>\n	<li>Setiap pertemuan 2 SKS per 2 jam perkuliahan</li>\n	<li>Setiap minggu terdiri dari 4 SKS per 2 jam perkuliahan</li>\n	<li>Jumlah pertemuan 100 persen perkuliahan satu semester adalah 56 jam perkuliahan</li>\n	<li>Jumlah pertemuan satu semester adalah 56 jam perkuliahan + responsi + UTS + UAS</li>\n</ul>\n\n<ol>\n	<li>Satu semester dijadwalkan 14 minggu perkuliahan dengan target kehadiran dosen minimum setiap kelas mata kuliah minimal 96%. Target ini dituangkan dalam surat pernyataan kesanggupan dari setiap Dosen pada awal semester saat rapat koordinasi pengajaran Fakultas.</li>\n	<li>Setiap Dosen wajib mengisi Berita Acara Perkuliahan (BAP) yang disesuaikan dengan SAP dan mengisi Daftar Hadir Kuliah mahasiswa dan Dosen, yang isinya antara lain tanggal, hari, jam, kuliah ke, uraian materi, tanda tangan mahasiswa dan tanda tangan Dosen per minggu. BAP ini dijadikan sebagai dasar evaluasi kehadiran Dosen dan mahasiswa. Mulai semester Ganjil 2013/2014, mekanisme presensi perkuliahan menggunakan RFID <em>(Radio Frequency Identification)</em> yang terintegrasi dengan aplikasi i-Gracias.</li>\n	<li>Apabila ada Dosen yang belum melakasanakan kegiatan sesuai mekanisme tatap muka pertemuan perkuliahan maka pihak Program Studi melakukan tahapan sebagai berikut :</li>\n</ol>\n\n<ul>\n	<li>Teguran secara lisan kepada Dosen yang bersangkutan</li>\n	<li>Teguran tertulis melalui surat kepada Dosen yang bersangkutan</li>\n	<li>Penggantian Dosen oleh Dosen lain yang memegang mata kuliah yang sama atau Dosen yang mampu/dianggap mampu.</li>\n</ul>\n\n<ol>\n	<li>Mata Kuliah 3 SKS disediakan 1 (satu) kali pertemuan setiap minggu dengan 14 minggu perkuliahan, dengan ketentuan sebagai berikut: secara umum evaluasi kuliah dilakukan dengan mengadakan Ujian Tengah Semester (UTS), dan Ujian Akhir Semester (UAS) di samping tugas-tugas lainnya seperti halnya pekerjaan rumah, kuis, pembuatan tugas makalah dan presentasi serta diskusi dalam kelas.</li>\n	<li>Setiap mahasiswa diberikan tugas yang sesuai dengan mata kuliahnya serta bobot SKS, baik berupa pekerjaan rumah, makalah, ataupun presentasi. Setiap tugas dievaluasi dan hasilnya dikembalikan kepada mahasiswa.</li>\n	<li>Mahasiswa wajib menghadiri kuliah tatap muka minimal 75% dari jumlah kehadiran Dosen. Apabila tingkat kehadiran dibawah 75% maka mahasiswa yang bersangkutan tidak berhak mengikuti UTS atau UAS.</li>\n	<li>Dosen juga dievaluasi oleh Institusi mengenai Kegiatan proses Belajar Mengajar (KBM) yang telah berlangsung selama satu semester yang akan mempengaruhi Nilai Kinerja Individu (NKI).</li>\n	<li>Pada awal semester diadakan rapat koordinasi pengajaran untuk membahas rencana kegiatan akademik atau proses belajar mengajar pada semester yang akan dilalui dan di akhir semester melalui mekanisme penjaminan mutu dilakukan evaluasi menyeluruh tentang pelaksanaan kegiatan dengan proses kegiatan akademik tersebut.</li>\n</ol>\n\n<ol>\n	<li>Kegiatan Praktikum</li>\n</ol>\n\n<ol>\n	<li>Jadwal praktikum ditentukan oleh program studi atau masing-masing laboratorium dengan mengacu pada jadwal perkuliahan Program Studi.</li>\n	<li>Evaluasi terhadap modul praktikum dilakukan jika diperlukan perubahan dari sisi <em>content </em>disesuaikan dengan kebutuhan kompetensi.</li>\n	<li>Evaluasi terhadap pelaksanaan praktikum, merujuk kepada umpan balik proses pembelajaran.</li>\n	<li>Kegiatan praktikum dibantu oleh kelompok keahlian beserta mahasiswa yang berperan sebagai asisten lab.</li>\n</ol>\n\n<ol>\n	<li>Kegiatan Kerja Praktek (Penjelasan detail pada Buku Panduan Akademik)</li>\n</ol>\n\n<ol>\n	<li>Untuk memberikan pengalaman bekerja pada mahasiswa di dunia kerja atau industri sesuai dengan program studi mahasiswa.</li>\n	<li>Untuk kegiatan Kerja Praktek, mahasiswa diwajibkan untuk melakukan praktek di perusahaan atau institusi yang terkait dengan Program Studi Sistem Informasi dengan mengajukan permohonan surat Kerja Praktek ke institusi atau perusahaan yang dituju melalui institusi atau Fakultas.</li>\n	<li>Mahasiswa wajib melakukan proses bimbingan dengan pembimbing di lapangan dan pembimbing akademik yang ada di Program Studi (ditunjuk oleh Fakultas)</li>\n	<li>Menyerahkan hasil Kerja Praktek berupa Laporan Kerja Praktek ke Program Studi dan mempresentasikan Laporan Kerja Praktek kepada pembimbing akademik.</li>\n</ol>\n\n<ol>\n	<li>Kegiatan Co-op / Opsional (Penjelasan detail pada Buku Panduan Akademik)</li>\n</ol>\n\n<ol>\n	<li>Untuk kegiatan Co-op, mahasiswa tidak diwajibkan yakni kegiatan Co-op sebagai opsional bagi mahasiswa untuk meningkatkan kualitas lulusan dan mengembangkan jiwa profesional di bidang tertentu. Co-op dikembangkan dikarenakan keterbatasan institusi dalam memfasilitasi kebutuhan mahasiswa untuk mendapatkan pengalaman kerja di dunia nyata dan sebagai wujud terciptanya <em>link and match </em>antara teori dan praktek di dunia kerja.</li>\n	<li>Proses pendaftaran dikelola oleh program studi, dan proses seleksi dilakukan oleh perusahaan/industri.</li>\n	<li>Mahasiswa wajib menaati prosedur kerja pada perusahaan/industri yang ditempatinya.</li>\n	<li>Penilaian dilakukan secara terus menerus selama mahasiswa menjalani program oleh perusahaan/industri, kemudian dikonversikan nilainya oleh pembimbing akademik dan dimasukkan sebagai nilai mata kuliah pada transkrip akademik.</li>\n</ol>\n\n<p><strong>Indikator-indikator keberhasilan mekanisme monitoring perkuliahan antara lain : </strong></p>\n\n<ol>\n	<li>Meningkatnya kehadiran Dosen</li>\n	<li>Meningkatnya kehadiran mahasiswa</li>\n	<li>Meningkatnya kinerja Dosen dalam mengajar</li>\n	<li>Mahasiswa merasa puas dalam kegiatan belajar mengajar</li>\n	<li>Meningkatnya indeks prestasi mahasiswa</li>\n	<li>Meningkatnya pemahaman dan daya nalar mahasiswa</li>\n	<li>Meningkatnya pemahaman mahasiswa tentang dunia kerja, jika nantinya lulus diharapkan sudah siap bekerja</li>\n</ol>\n\n<p><strong>Sasaran Mutu Prodi S1 Sistem Informasi : </strong></p>\n\n<ol>\n	<li>Jumlah mahasiswa DO atau undur diri maksimal 5%</li>\n	<li>Prosentase mahasiswa lulus tepat waktu minimal 60%</li>\n	<li>Rata-rata tingkat kepuasan mahasiswa terhadap pengajaran minimal 80%</li>\n</ol>\n\n<p>Absensi kehadiran dengan menggunakan RFID memungkinkan pemantauan kehadiran dosen di kelas dilakukan dengan akurat dan cepat. Hasil dari pemindaian kartu pegawai dengan RFID diintegrasikan dengan sistem informasi dan dapat langsung dimonitor oleh ketua program studi melalui aplikasi iGracias.</p>\n\n<p>Gambar 5.11 dan Gambar 5.12 memperlihatkan contoh tampilan program pemantauan kehadiran dosen di menu iGracias (menggunakan <em>account </em>Kaprodi).</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p><strong>Gambar 5. </strong><strong>11 Tampak Depan Laporan Kehadiran Dosen</strong></p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p><strong>Gambar 5. </strong><strong>12 Contoh Snapshot dari Monitoring Kehadiran Dosen</strong></p>\n\n<p><strong>M</strong><strong>onitoring </strong><strong>k</strong><strong>e</strong><strong>hadiran Mahasiswa</strong></p>\n\n<p>Monitoring kehadiran untuk mahasiswa dilakukan dengan sistem informasi yang terintegrasi, yaitu menggunakan:</p>\n\n<ol>\n	<li>Kartu tanda mahasiswa (KTM) yang dilengkapi teknologi RFID untuk presensi mahasiswa.</li>\n	<li>RFID <em>reader </em>di setiap ruang kelas perkuliahan.</li>\n	<li>Jaringan terpusat.</li>\n	<li>Aplikasi pemantau kehadiran dosen yang terintegrasi dengan iGracias.</li>\n</ol>\n\n<p>Presensi dengan RFID memungkinkan pemantauan kehadiran mahasiswa di kelas dilakukan dengan akurat dan cepat. Hasil dari pemindaian KTM dengan RFID diintegrasikan dengan sistem informasi yang dapat langsung dimonitor oleh ketua Program Studi, dosen wali mahasiswa yang bersangkutan, dan orang tua mahasiswa melalui aplikasi i-Gracias.</p>\n\n<p>Monitoring kehadiran mahasiswa dilakukan dengan aplikasi seperti yang terdapat pada Gambar 5.13.</p>\n\n<p>&nbsp;</p>\n\n<p><strong>Gambar 5. </strong><strong>13 Contoh Snapshot Menu Monitoring Kehadiran Mahasiswa</strong></p>\n\n<p><strong>Monitoring Materi</strong><strong> Kuliah</strong></p>\n\n<p>Saat ini institusi sedang mengembangkan mekanisme pendataan dan <em>monitoring </em>materi perkuliahan yang diintegrasikan dengan aplikasi akademik i-Gracias. Contoh menu monitoring materi perkuliahan dapat dilihat pada Gambar 5.20.</p>\n\n<p>&nbsp;</p>\n\n<p><strong>Gambar 5. </strong><strong>14 Contoh Snapshot Materi Perkuliahan yang Telah Disampaikan Oleh Dosen Berdasarkan Hasil Rekapitulasi Program Studi</strong></p>\n\n<p>Selain menggunakan sistem informasi terintegrasi, proses <em>monitoring </em>materi kuliah juga dilakukan oleh dosen koordinator mata kuliah melalui mekanisme rapat koordinasi.</p>\n');

-- --------------------------------------------------------

--
-- Table structure for table `std5_532contohsoalujian`
--

CREATE TABLE `std5_532contohsoalujian` (
  `no` int(20) UNSIGNED NOT NULL,
  `contoh_soal_ujian` varchar(65000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `std5_532contohsoalujian`
--

INSERT INTO `std5_532contohsoalujian` (`no`, `contoh_soal_ujian`) VALUES
(1, '<p>Dalam menyusun soal ujian tengah semester (UTS), ujian akhir semester (UAS) maupun soal-soal latihan dalam perkualihan, program studi S1 sistem informasi menetapkan sebuah template soal ujian wajib berisikan:</p>\n\n<ol>\n	<li>Informasi mengenai mata kuliah: kode mata kuliah, nama mata kuliah, semester pelaksanaan ujian</li>\n	<li>Informasi mengenai pelaksanaan ujian: hari, tanggal, bulan, tahun, waktu pelaksanaan, dosen pengampu, sifat ujian, ruang ujian.</li>\n	<li>Informasi identitas mahasiswa: nama, NIM, kelas</li>\n	<li>Pakta integritas pelaksanaan ujian yang wajib disalin dan ditandatangani oleh mahasiswa.</li>\n	<li>Setiap pemaparan soal wajib menyampaikan bobot nilai dan kompetensi yang diukur oleh soal yang diberikan.</li>\n</ol>\n\n<p>Mata Kuliah yang digunakan sebagai contoh sebagai berikut:</p>\n\n<ol>\n	<li>Mata Kuliah Algoritma dan Pemograman (KUG1A3)</li>\n	<li>Mata Kuliah Analisis dan Perancangan Sistem Informasi (ISG3A4)</li>\n	<li>Mata Kuliah Sistem Basis Data (ISG2H4)</li>\n	<li>Mata Kuliah Tata Kelola dan Manajemen TI (ISG3L3)</li>\n	<li>Mata Kuliah Enterprise Architecture (ISG3B3)</li>\n</ol>\n');

-- --------------------------------------------------------

--
-- Table structure for table `std5_541daftardosenpembimbing`
--

CREATE TABLE `std5_541daftardosenpembimbing` (
  `no` int(75) NOT NULL,
  `nama_dosen_pembimbing_akademik` varchar(200) NOT NULL,
  `jumlah_mahasiswa_bimbingan` int(50) NOT NULL,
  `rata_rata_banyaknya_pertemuan_mhs_smt` int(50) NOT NULL,
  `excel_path` varchar(255) NOT NULL,
  `excelhtml_path` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `std5_541daftardosenpembimbing`
--

INSERT INTO `std5_541daftardosenpembimbing` (`no`, `nama_dosen_pembimbing_akademik`, `jumlah_mahasiswa_bimbingan`, `rata_rata_banyaknya_pertemuan_mhs_smt`, `excel_path`, `excelhtml_path`) VALUES
(0, 'Rata-rata Banyaknya Pertemuan per mahasiswa per semester', 4, 0, '', ''),
(1, 'Adityas Widjajarto', 19, 4, '', ''),
(2, 'Ahmad Musnansyah', 33, 4, '', ''),
(3, 'Albi Fitransyah', 20, 4, '', ''),
(4, 'Andika Bayu H.', 32, 4, '', ''),
(5, 'Ari Fajar Santoso', 30, 4, '', ''),
(6, 'Asti Amalia', 30, 4, '', ''),
(7, 'Basuki Rahmad', 26, 4, '', ''),
(8, 'Budi Santosa', 25, 4, '', ''),
(9, 'Christanto Triwibisono***', 15, 4, '', ''),
(10, 'Deden Witarsyah', 24, 4, '', ''),
(11, 'Faishal Mufied Al Anshary', 33, 4, '', ''),
(12, 'Farda Hasun***', 19, 4, '', ''),
(13, 'Ferdian', 28, 4, '', ''),
(14, 'Ilham Perdana***', 24, 4, '', ''),
(15, 'Irfan Darmawan***', 35, 4, '', ''),
(16, 'Judi Alhilman***', 25, 4, '', ''),
(17, 'Litasari Widyastuti Soewarsono', 13, 4, '', ''),
(18, 'Lukman Abdurrahman', 24, 4, '', ''),
(19, 'Luthfi Ramadani', 9, 4, '', ''),
(20, 'M. Teguh Kurniawan***', 24, 4, '', ''),
(21, 'Maria Dellarosawati Idawicaksakti', 22, 4, '', ''),
(22, 'Muhammad Azani Hasibuan***', 25, 4, '', ''),
(23, 'Murahartawaty***', 24, 4, '', ''),
(24, 'Nia Ambarsari***', 25, 4, '', ''),
(25, 'Nur Ichsan Utama', 20, 4, '', ''),
(26, 'Nopendri', 24, 4, '', ''),
(27, 'R. Wahjoe Witjaksono', 25, 4, '', ''),
(28, 'Rachmadita Andreswari***', 31, 4, '', ''),
(29, 'Rahmat Mulyana', 26, 4, '', ''),
(30, 'Rd. Rohmat Saedudin***', 15, 4, '', ''),
(31, 'Ridha Hanafi', 20, 4, '', ''),
(32, 'Riza Agustiansyah***', 33, 4, '', ''),
(33, 'Sari Wulandari', 22, 4, '', ''),
(34, 'Seno Adi Putra', 24, 4, '', ''),
(35, 'Soni Fajar Surya Gumilang***', 88, 4, '', ''),
(36, 'Taufik Nur Adi', 19, 4, '', ''),
(37, 'Tien Fabrianti Kusumasari***', 31, 4, '', ''),
(38, 'Ulka Chandini Pendit', 20, 4, '', ''),
(39, 'Umar Yunan Kurnia Septo Hediyanto', 28, 4, '', ''),
(40, 'Warih Puspitasari', 27, 4, '', ''),
(41, 'Yuli Adam Prasetyo***', 35, 4, '', ''),
(42, 'Total', 1072, 0, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `std5_542prosespembimbingan`
--

CREATE TABLE `std5_542prosespembimbingan` (
  `no` int(50) NOT NULL,
  `hal` varchar(75) NOT NULL,
  `penjelasan` varchar(5000) NOT NULL,
  `excel_path` varchar(255) NOT NULL,
  `excelhtml_path` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `std5_542prosespembimbingan`
--

INSERT INTO `std5_542prosespembimbingan` (`no`, `hal`, `penjelasan`, `excel_path`, `excelhtml_path`) VALUES
(1, 'Tujuan bimbingan aa', '1. Proses pembimbingan yang berkaitan dengan aspek akademik mahasiswa\na. Memberikan informasi terkait aturan dan prosedur akademik yang berlaku sesuai dengan tahapan akademik yang sedang ditempuh mahasiswa.\nb. Memberikan pengarahan dan pertimbangan kepada mahasiswa dalam penyusunan Rencana Studi setiap semester mengenai mata kuliah apa saja yang seharusnya diambil dan jumlah SKS untuk semester yang berjalan, dan memberikan masukan dalam memperbaiki pencapaian Indeks Prestasi Kumulatif (IPK) mahasiswa.\nc. Memonitor dan mengevaluasi perkembangan prestasi akademik mahasiswa untuk mengantisipasi sedini mungkin masalah studi yang dihadapi mahasiswa.\nd. Memberikan dorongan dan motivasi kepada mahasiswa untuk berprestasi baik di kegiatan akademik dan mampu menyelesaikan studinya tepat waktu.\ne. Memberikan laporan perkembangan akademik mahasiswa yang membutuhkan konsultasi langsung dengan ketua program studi.\n2. Proses pembimbingan yang berkaitan dengan aspek non-akademik mahasiswa\na. Membantu mahasiswa dalam mencarikan jalan penyelesaian terbaik terkait masalah personal yang berpengaruh langsung terhadap kemajuan studi mahasiswa.\nb. Memberikan motivasi, stimulus sehingga mahasiswa lebih semangat dalam belajar dan memiliki kemampuan membagi waktu sehingga dapat berprestasi di kegiatan ekstrakurikuler.\n', 'uploads/dokumenpendukungstd5/std5_542prosespembimbingan/datastandar5-5.4.2ProsesPembimbinganAkademikyangDiterapkanpadaProgramStudi.xlsx', 'uploads/dokumenpendukungstd5/std5_542prosespembimbingan/datastandar5-5.4.2ProsesPembimbinganAkademikyangDiterapkanpadaProgramStudi.xlsx.html'),
(2, 'Pelaksanaan Pembimbingan test', 'Proses pembimbingan akademik dilaksanakan setiap awal sampai dengan akhir semester. Proses pembimbingan dilakukan minimal 4 kali dalam 1 (satu) semester.\na. Pada semester I, pertemuan untuk pembimbingan akademik bersifat perkenalan dan mengumpulkan data pribadi mahasiswa, serta menjelaskan aturan-aturan akademik dan metode pelaksanaan perwalian yang disepakati oleh dosen wali dan mahasiswanya. \nb. Pembimbingan akademik yang dilakukan pada awal semester, dimana dosen wali memberikan pandangan mengenal Mata Kuliah yang akan diambil untuk memperbaiki Indeks Prestasi Kumulatif (IPK) dengan mempertimbangkan nilai D dan IPK semester sebelumnya.\nc. Setelah mahasiswa mengambil Mata Kuliah dalam Rencana Studi, maka dosen wali memberikan persetujan yang dilakukan secara online, dan dosen berhak membatasi jumlah beban SKS yang diambil disesuaikan dengan keterbatasan kemampuan yang dimiliki mahasiswa disertai dengan penjelasan dari keputusan tersebut.\nd. Dua minggu setelah perkuliahan, merupakan masa perwalian untuk melakukan Perubahan Rencana Studi (PRS). Proses PRS merupakan akhir dari proses Registrasi yang dilakukan secara online dan membutuhkan persetujuan dosen wali.\ne. Sepanjang semester, dosen wali mengikuti perkembangan studi setiap mahasiswa bimbingannya sehingga dapat diantisipasi sedini mungkin masalah dalam studi mereka. Dosen wali diharuskan untuk mengisi Lembar Kemajuan Studi (LKS) pada akhir semester dengan memberikan catatan akademik untuk dikirimkan ke orang tua mahasiswa.\nf. Memberikan konsultasi kepada mahasiswa bimbingannya yang menghadapi kesulitan dalam menyelesaikan studinya dan kalau memerlukan bimbingan dan konseling yang lebih intensif dapat meneruskannya kepada bimbingan dan konseling di Career Development Center (CDC). Dosen wali dapat memanfaatkan informasi Self Asessment yang diperoleh dari CDC untuk memberikan profil setiap anak wali mengenai kelebihan dan kebutuhan untuk mengembangkan potensi anak wali.\ng. Dosen wali memberikan saran dan pertimbangan bagi mahasiswa yang  pada proses pengunduran diri dari Program Studi Sistem Informasi.\nh. Memberikan persetujuan kepada mahasiswa dalam mengajukan permohonan sidang akademik yang menentukan status dan atau kelulusan tahap pendidikannya. \ni. Persetujuan rencana studi dan proses registrasi secara online tidak meniadakan hak mahasiswa untuk berkonsultasi secara langsung kepada Dosen Wali. Mahasiswa dapat melakukan pembimbingan diluar jadwal disesuaikan dengan kebutuhan mahasiswa.\n', 'uploads/dokumenpendukungstd5/std5_542prosespembimbingan/datastandar5-5.4.2ProsesPembimbinganAkademikyangDiterapkanpadaProgramStudi.xlsx', 'uploads/dokumenpendukungstd5/std5_542prosespembimbingan/datastandar5-5.4.2ProsesPembimbinganAkademikyangDiterapkanpadaProgramStudi.xlsx.html'),
(3, 'Masalah yang dibicarakan dalam pembimbingan', 'a. Rencana studi dan program belajar mahasiswa                                         \nb. Memberikan atau tidak memberikan persetujuan kepada mahasiswa dalam hal permohonan sidang akademik                                                                                          \nc. Memberikan motivasi dan rangsangan belajar kepada mahasiswa                                              \nd. Melakukan diskusi dalam hal yang menyangkut masalah mahasiswa baik aspek akademik maupun non-akademik\ne. Pengarahan Akademik untuk mahasiswa yang terancam Drop Out (DO) untuk memberikan peringatan terkait DO pada semester 4, serta memberikan dorongan motivasi kepada mahasiswa agar terhindar dari DO.\n', 'uploads/dokumenpendukungstd5/std5_542prosespembimbingan/datastandar5-5.4.2ProsesPembimbinganAkademikyangDiterapkanpadaProgramStudi.xlsx', 'uploads/dokumenpendukungstd5/std5_542prosespembimbingan/datastandar5-5.4.2ProsesPembimbinganAkademikyangDiterapkanpadaProgramStudi.xlsx.html'),
(4, 'Kesulitan dalam pembimbingan dan upaya untuk mengatasinya', 'a. Kesadaran mahasiswa untuk melakukan konsultasi dengan Dosen Wali untuk membicarakan masalah akademik yang dihadapi mahasiswa dengan pembimbing masih kurang. Hal ini disikapi dengan menetapkan aturan akademik yang mewajibkan mahasiswa untuk berkomunikasi dan berinteraksi dengan Dosen Wali pada saat persetujuan Rencana Studi dan pengiriman Lembar Kemajuan Studi (LKS) kepada orang tua sehingga terbangun komunikasi yang efektif antara mahasiswa, dosen wali, dan orang tua.\nb. Jika tidak ada prakarsa/minat dari mahasiswa yang bersangkutan untuk menemui pembimbing akademik untuk mengemukakan masalahnya, pembimbing akademik wajib mengambil prakarsa memanggil mahasiswa yang diperkirakan mempunyai masalah dengan tujuan menggali informasi yang diperlukan untuk kepentingan pembimbingan. \nc. Untuk meningkattkan lulus tepat waktu dibutuhkan peran dosen wali untuk memberikan perhatian yang intens kepada mahasiswa yang berada di semester 7 dan memberikan peringatan kepada mahasiswa yang masih belum lulus pada semester 9.\n', 'uploads/dokumenpendukungstd5/std5_542prosespembimbingan/datastandar5-5.4.2ProsesPembimbinganAkademikyangDiterapkanpadaProgramStudi.xlsx', 'uploads/dokumenpendukungstd5/std5_542prosespembimbingan/datastandar5-5.4.2ProsesPembimbinganAkademikyangDiterapkanpadaProgramStudi.xlsx.html'),
(5, 'Manfaat yang diperoleh mahasiswa dari pembimbingan', 'a. Dapat membantu mahasiswa dalam menyelesaikan berbagai permasalahan yang dihadapi baik dalam bidang akademik maupun non akademik.\nb. Dapat mempererat dan mendekatkan hubungan antara dosen dengan mahasiswa.\nc. Rencana studi mahasiswa lebih terarah dan relatif dapat menyelesaikan studi sesuai target.\n', 'uploads/dokumenpendukungstd5/std5_542prosespembimbingan/datastandar5-5.4.2ProsesPembimbinganAkademikyangDiterapkanpadaProgramStudi.xlsx', 'uploads/dokumenpendukungstd5/std5_542prosespembimbingan/datastandar5-5.4.2ProsesPembimbinganAkademikyangDiterapkanpadaProgramStudi.xlsx.html');

-- --------------------------------------------------------

--
-- Table structure for table `std5_551pelaksanaanpembimbingan`
--

CREATE TABLE `std5_551pelaksanaanpembimbingan` (
  `no` int(100) NOT NULL,
  `nama_dosen_pembimbing` varchar(255) NOT NULL,
  `jumlah_mahasiswa` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `std5_551pelaksanaanpembimbingan`
--

INSERT INTO `std5_551pelaksanaanpembimbingan` (`no`, `nama_dosen_pembimbing`, `jumlah_mahasiswa`) VALUES
(1, 'Adityas Widjajarto', 6),
(2, 'Ahmad Musnansyah', 6),
(3, 'Materi', 0),
(4, 'Ari Fajar Santoso', 4),
(5, 'Asti Amalia', 6),
(6, 'Basuki Rahmad', 5),
(7, 'Budi Santosa', 5),
(8, 'Deden Witarsyah', 5),
(9, 'Faishal Mufied Al Anshary', 8),
(10, 'Ferdian', 5),
(11, 'Ilham Perdana***', 2),
(12, 'Irfan Darmawan***', 9),
(13, 'Judi Alhilman***', 2),
(14, 'Lukman Abdurrahman', 5),
(15, 'Luthfi Ramadani', 5),
(16, 'Maria Dellarosawati Idawicaksakti', 5),
(17, 'Muhammad Azani Hasibuan***', 6),
(18, 'Nia Ambarsari***', 4),
(19, 'Nur Ichsan Utama', 5),
(20, 'Nopendri', 4),
(21, 'R. Wahjoe Witjaksono', 5),
(22, 'Rachmadita Andreswari***', 4),
(23, 'Rahmat Mulyana', 5),
(24, 'Rd. Rohmat Saedudin***', 4),
(25, 'Ridha Hanafi', 6),
(26, 'Riza Agustiansyah***', 6),
(27, 'Soni Fajar Surya Gumilang***', 9),
(28, 'Taufik Nur Adi', 3),
(29, 'Tien Fabrianti Kusumasari***', 5),
(30, 'Ulka Chandini Pendit', 2),
(31, 'Umar Yunan Kurnia Septo Hediyanto', 5),
(32, 'Warih Puspitasari', 2),
(33, 'Yuli Adam Prasetyo***', 9),
(34, 'Total', 167);

-- --------------------------------------------------------

--
-- Table structure for table `std5_552ratapenyelesaianta`
--

CREATE TABLE `std5_552ratapenyelesaianta` (
  `no` int(25) NOT NULL,
  `waktu_penyelesaian_skripsi` varchar(65000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `std5_552ratapenyelesaianta`
--

INSERT INTO `std5_552ratapenyelesaianta` (`no`, `waktu_penyelesaian_skripsi`) VALUES
(1, '<p>Rata&ndash;rata waktu penyelesaian tugas akhir/ skripsi oleh mahasiswa Sistem Informasi pada tiga tahun terakhir adalah 7 bulan atau setara 1,2 semester.</p>\n');

-- --------------------------------------------------------

--
-- Table structure for table `std5_571kebijakan`
--

CREATE TABLE `std5_571kebijakan` (
  `no` int(25) UNSIGNED NOT NULL,
  `kebijakan_suasana_akademik` varchar(65000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `std5_571kebijakan`
--

INSERT INTO `std5_571kebijakan` (`no`, `kebijakan_suasana_akademik`) VALUES
(1, '<p>aSetiap civitas akademik Universitas Telkom, khususnya pada program studi sistem informasi memiliki hak untuk merasakan suasana akademik yang telah tertulis dalam kebijakan ang terdiri atas otonomi keilmuan, kebebasan akademik dan kebebasan mimbar akademik.</p>\n\n<p>Untuk mendukung adanya suasana akademik yang kondusif, di keluarkanlah Surat Keputusan Rektor universitas Telkom No. KR 024/AKD27/WR1/2014 tentang aturan akademik Universitas Telkom terdapat bab VII yang menjelaskan pedoman kebebasan akademik, kebebasan mimbar akademik dan otonomi keilmuan dan bab VIII yang menjelaskan pedoman pengembangan suasana akademik. Kelengkapan penjelasan keputusan rektor mengenai suasana akademik akan dijelaskan sebagai berikut.</p>\n\n<p><strong>Tabel 5. </strong><strong>26 Bab VII Pedoman Kebebasan Akademik, Kebebasan Mimbar Akademik dan Otonomi Keilmuan</strong></p>\n\n<table border="1" cellspacing="0">\n	<tbody>\n		<tr>\n			<td colspan="2" style="width:462.1pt">\n			<p>Bab VII Pedoman Kebebasan Akademik, Kebebasan Mimbar Akademik dan Otonomi Keilmuan</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:133.0pt">\n			<p><strong>Kebebasan Akademik</strong></p>\n			</td>\n			<td style="width:329.1pt">\n			<p>Kebebasan Akademik adalah kebebasan yang dimiliki oleh seluruh civitas akademik untuk mendalami dan mengembangkan Ilmu Pengetahuan, Teknologi dan Seni secara bertanggung jawab melalui pelaksanaan Tridharma Perguruan Tinggi.</p>\n\n			<p>Pada Pasal 56 tentang Pelaksanaan Kebebasan akademik menjelaskan bahwa (1) Kebebasan akademik merupakan asas yang mendorong berlangsungnya proses-proses penelitian, debat, pembelajaran dan publikasi ilmiah yang tidak terbelenggu di perguruan tinggi. (2) Oleh karena itu Universitas Telkom memberikan kebebasan akademik kepada sivitas akademikanya. Berarti, Universitas Telkom mendukung kebebasan dosen dan mahasiswa untuk membuat pernyataan-pernyataan dalam pengajaran, melakukan investigasi dalam penelitian, dan penyebarluasan hasilnya melalui presentasi, peragaan dan publikasi karya ilmiah. (3) Sebagai konskuensinya, sivitas akademika Universitas Telkom dinilai berdasarkan kinerja profesional mereka, selama tidak melanggar kebijakan dan peraturan Universitas Telkom.</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:133.0pt">\n			<p><strong>Kebebasan Mimbar Akademik</strong></p>\n			</td>\n			<td style="width:329.1pt">\n			<p>Kebebasan Mimbar Akademik adalah wewenang yang secara terbatas hanya dimiliki oleh guru besar dan/atau dosen dan/atau cendekia yang memiliki reputasi, otoritas, dan wibawa ilmiah untuk menyatakan secara terbuka dan bertanggung jawab mengenai sesuatu yang berkenaan dengan rumpun ilmu dan cabang ilmunya.</p>\n\n			<p>Pada Pasal 57 tentag Pelaksanaan Kebebasan Mimbar Akademik menjelaskan bahwa (2) Kebebasan mimbar akademik diberikan kepada Guru Besar Universitas Telkom dalam hal:</p>\n\n			<ol>\n				<li>\n				<ol>\n					<li>melaksanakan tugas mentransformasikan ilmu pengetahuan, teknologi, manajemen dan/atau seni yang dikuasainya kepada mahasiswa dengan mewujudkan suasana belajar dan pembelajaran sehingga mahasiswa aktif mengembangkan potensiny</li>\n					<li>menjalankan tugas pokok dan fungsi sebagai ilmuwan yang sedang mengembangkan suatu cabang ilmu pengetahuan, teknologi, manajemen dan/atau seni melalui penalaran dan penelitian ilmiah serta menyebarluaskannya.</li>\n				</ol>\n				</li>\n				<li>menjalankan tugas secara perseorangan atau berkelompok ketika menulis buku ajar atau buku teks, baik yang diterbitkan maupun terunggah di dunia maya, dan/atau publikasi ilmiah sebagai salah satu sumber belajar dan untuk pengembangan budaya akademik serta pembudayaan kegiatan baca tulis bagi sivitas akademika.</li>\n			</ol>\n\n			<p>&nbsp;</p>\n\n			<p>Dan (3) Kebebasan mimbar akademik untuk membahas topik-topik yang berhubungan dengan bidang keahlian profesi di ruang kuliah, pada pertemuan profesi, atau melalui publikasi disertai dengan tanggung jawab untuk tidak mengajukan diri dalam bentuk pernyataan atau kegiatan yang bb3berkesan atau berdampak pengaruh mewakili atau berbicara atas nama Universitas Telkom, kecuali jika secara spesifik telah diberi mandat oleh Universitas Telkom.</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:133.0pt">\n			<p>Otonomi Keilmuan</p>\n			</td>\n			<td style="width:329.1pt">\n			<p>Otonomi keilmuan adalah otonomi Sivitas Akademika pada suatu cabang Ilmu Pengetahuan, Teknologi dan/atau Seni dalam menemukan, mengembangkan, mengungkap, dan/atau mempertahankan kebenaran ilmiah menurut kaidah, metode keilmuan, dan budaya akademik.</p>\n\n			<p>&nbsp;</p>\n\n			<p>Pada pasal 58 tentang Pelaksanaan Otonomi Keilmuan menjelaskan bahwa (2) Dalam pelaksanaan otonomi keilmuan civitas akademika mempertimbangkan kesesuaiannya, (3) memperhatikan koordinasi dan kemungkanan kolaborasi antar kelompok keilmuan lain di cabang ilmu pengetahuan, teknologi, manajemen dan/atau seni dan desain di lingkungan Universitas Telkom. (4) Civitas akademika dapat memanfaatkan keberadaan sumberdaya yang telah tersedia di lingkungan Universitas Telkom dan dapat pula memahami keterbatasannya, serta dapat memanfaatkan sumberdaya di luar Universitas Telkom dalam suatu kerangka kerjasama secara kelembagaan.</p>\n\n			<p>&nbsp;</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:133.0pt">\n			<p><strong>Budaya Akademik </strong></p>\n			</td>\n			<td style="width:329.1pt">\n			<p>Budaya akademik adalah seluruh sistem nilai, gagasan, norma, tindakan, dan karya yang bersumber dari Ilmu Pengetahuan, Teknologi dan Seni sesuai dengan asas Pendidikan Tinggi.</p>\n\n			<p>&nbsp;</p>\n\n			<p>Pada pasal 59 tentang Pengembangan Budaya Akademik menjelaskan bahwa (1) Pengembangan budaya akademik dilakukan melalui interaksi sosial yang tidak membedakan suku, agama, ras, antargolongan, gender, kedudukan sosial, tingkat kemampuan ekonomi, dan aliran politik serta madzhab pemikiran maka dari itu (3) Sivitas akademika berkewajiban memelihara dan mengembangkan budaya akademik dengan memperlakukan ilmu pengetahuan, teknologi, manajemen dan/atau seni sebagai proses dan produk serta sebagai amal dan paradigma moral.</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:133.0pt">\n			<p><strong>Kemitraan Dosen-Mahasiswa</strong></p>\n			</td>\n			<td style="width:329.1pt">\n			<p>Salah satu bentuk kemitraan dosen-mahasiswa dalam dunia akademik terjadi pada <em>Study Group</em>, Penelitan Dana Mandiri, dan Penelitian Dana Internal.Pada <em>study group </em>dan <em>research group </em>dosen dan mahasiswa berbagi pengetahuan terkait materi/ penelitian yang cakupannya lebih luas dari yang dipelajari secara formal di kegiatan perkuliahan. Pada skema Penelitian Dana Mandiri dan Penelitian Dana Internal dosen bertindak sebagai ketua peneliti dan mahasiswa S1 bertindak sebagai Asisten peneliti atau <em>programmer</em>. Selama <strong>3 tahun terakhir</strong> telah tercatat <strong>97 penelitian</strong> <strong>dosen</strong> yang <strong>melibatkan 246 mahasiswa</strong> prodi SI.</p>\n\n			<p>&nbsp;</p>\n			</td>\n		</tr>\n	</tbody>\n</table>\n\n<p>Adapun beberapa indikator yang menunjukan keberhasilan dari upaya-upaya tersebut di atas adalah :</p>\n\n<ol>\n	<li>Terjalinnya komunikasi yang baik antara Dosen dan mahasiswa.</li>\n	<li>Meningkatnya kepuasan mahasiswa.</li>\n	<li>Meningkatnya tanggung jawab Dosen untuk melaksanakan proses perkuliahan.</li>\n	<li>Meningkatnya kemampuan mahasiswa.</li>\n	<li>Adanya kecintaan terhadap kampus.</li>\n	<li>Meningkatnya kepekaan, daya nalar dan wawasan mahasiswa dan Dosen.</li>\n	<li>Meningkatnya kelancaran studi mahahasiswa.</li>\n	<li>Meningkatnya efektivitas proses pembelajaran di dalam dan di luar kelas.</li>\n</ol>\n');

-- --------------------------------------------------------

--
-- Table structure for table `std5_572ketersediaanprasarana`
--

CREATE TABLE `std5_572ketersediaanprasarana` (
  `no` int(25) UNSIGNED NOT NULL,
  `ketersediaan_dan_jenis_prasarana` varchar(65000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `std5_572ketersediaanprasarana`
--

INSERT INTO `std5_572ketersediaanprasarana` (`no`, `ketersediaan_dan_jenis_prasarana`) VALUES
(1, '<p>aInteraksi akademik antara sivitas akademika menjadi suatu faktor yang mempengaruhi harmonisasi dan kondisi kerja dalam lembaga pendidikan. Secara rutin, anggaran investasi dan anggaran operasional ditetapkan dalam Rencana Kerja Anggaran (RKA) untuk mendukung terciptanya interaksi akademik antara sivitas akademika. <strong>Rata-rata perbandingan anggaran investasi dan operasional dalam</strong> <strong>3 tahun terakhir berkisar pada angka 1:3.</strong></p>\n\n<p>Program Studi Sarjana Sistem Informasi telah menyediakan sarana dan prasarana untuk memfasilitasi terbangunnya interaksi akademik antara sivitas akademika yang baik diantaranya:</p>\n\n<p>1. Sarana Akademik</p>\n\n<p><strong>Tabel 5. </strong><strong>27 Sarana Akademik</strong></p>\n\n<table border="1" cellspacing="0" style="width:505px">\n	<tbody>\n		<tr>\n			<td style="background-color:#bfbfbf; width:28.5pt">\n			<p>No.</p>\n			</td>\n			<td style="background-color:#bfbfbf; width:94.9pt">\n			<p>Jenis Prasarana/Sarana</p>\n			</td>\n			<td style="background-color:#bfbfbf; width:255.15pt">\n			<p>Keterangan</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:28.5pt">\n			<p>1</p>\n			</td>\n			<td style="width:94.9pt">\n			<p>Ruang Kuliah</p>\n			</td>\n			<td style="width:255.15pt">\n			<p>23 ruang kuliah yang dipergunakan untuk Program Studi S1 Sistem Informasi</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:28.5pt">\n			<p>2</p>\n			</td>\n			<td style="width:94.9pt">\n			<p>Ruang Dosen</p>\n			</td>\n			<td style="width:255.15pt">\n			<p>45 ruang dosen dilengkapi dengan ruang diskusi dengan LED TV untuk paparan presentasi bahan yang di diskusikan serta printer untuk mencetakan naskah hasil diskusi</p>\n			</td>\n		</tr>\n		<tr>\n			<td rowspan="4" style="width:28.5pt">\n			<p>3</p>\n			</td>\n			<td rowspan="4" style="width:94.9pt">\n			<p>Ruang Kuliah Umum</p>\n			</td>\n			<td style="width:255.15pt">\n			<p>Gedung Serba Guna (GSG) yang berkapasitas &plusmn;500 orang.</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:255.15pt">\n			<p>Gedung K dengan Kapasitas &plusmn;300 orang.</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:255.15pt">\n			<p>Auditorium di Learning Center (LC) dengan kapasitas &plusmn;100 orang</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:255.15pt">\n			<p>Auditorium FIT, Auditorium FIK, Auditorium FEB yang dapat digunakan untuk kegiatan akademik di tingkat universitas.</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:28.5pt">\n			<p>4</p>\n			</td>\n			<td style="width:94.9pt">\n			<p>Ruang Rapat</p>\n			</td>\n			<td style="width:255.15pt">\n			<p>3 ruang rapat (Gedung Karang Lantai 1, 2, dan 3) dilengkapi dengan proyektor dan audio sistem</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:28.5pt">\n			<p>5</p>\n			</td>\n			<td style="width:94.9pt">\n			<p>Laboratorium</p>\n			</td>\n			<td style="width:255.15pt">\n			<p>Daftar Laboratorium : Lab BPAD, Lab Sisjar, Lab ERP, Lab Prodase. Digunakan untuk pengembangan keprofesian sesuai dengan kelompok keahlian yang ditetapkan. Serta Lab Fisika Dasar, Lab Common Lab, Lab Dasar Program Komputer</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:28.5pt">\n			<p>6</p>\n			</td>\n			<td style="width:94.9pt">\n			<p>Perpustakan</p>\n			</td>\n			<td style="width:255.15pt">\n			<p>1 Gedung Perpustakaan umum di Gedung Mentarawu lantai 5 yang disediakan oleh institusi (universitas) sebagai perpustakaan modern yang memberikan fasilitas kepada mahasiswa berupa referensi buku teks, majalah ilmiah, jurnal, dan prosiding.</p>\n			</td>\n		</tr>\n		<tr>\n			<td rowspan="2" style="width:28.5pt">\n			<p>7</p>\n			</td>\n			<td rowspan="2" style="width:94.9pt">\n			<p>Ruang Kegiatan Mahasiswa</p>\n			</td>\n			<td style="width:255.15pt">\n			<p>Student center yang disediakan oleh institusi (universitas) sebagai sarana mahasiswa untuk melakukan kegiatan ekstrakurikuler dalam organisasi kemahasiswaan dan unit kegiatan mahasiswa.</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:255.15pt">\n			<p>Ruang khusus untuk UKM dibawah pembinaan Fakultas (HMSI)</p>\n			</td>\n		</tr>\n		<tr>\n			<td rowspan="3" style="width:28.5pt">\n			<p>8</p>\n			</td>\n			<td rowspan="3" style="width:94.9pt">\n			<p>Akses Internet</p>\n			</td>\n			<td style="width:255.15pt">\n			<p>Penyediaan jaringan akses internet bekecepatan 3-9 Mbps.</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:255.15pt">\n			<p>Akses internet melalui kabel dan wifi pada seluruh area kampus</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:255.15pt">\n			<p>Mekanisme SSO (single-sign-on) untuk setiap civitas academica (dosen dan mahasiswa) untuk akses intenet dan akses sistem akademik i-Gracias (informasi akademik),i-Caring (media e-learning yang digunakan sebelum tahun 2015), dan IDEA (media e-learning yang digunakan setelah tahun 2015), dan lain-lain.</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:28.5pt">\n			<p>9</p>\n			</td>\n			<td style="width:94.9pt">\n			<p>Fasilitas Umum</p>\n			</td>\n			<td style="width:255.15pt">\n			<p>Ruang belajar terbuka : 6 ruang belajar terbuka untuk tempat berdiskusi mahasiswa yang berada di Gedung Karang</p>\n			</td>\n		</tr>\n	</tbody>\n</table>\n\n<p>&nbsp;</p>\n\n<p>2. Partisipasi Aktif Mahasiswa dalam Proses Mengajar</p>\n\n<table border="1" cellspacing="0" style="width:432px">\n	<tbody>\n		<tr>\n			<td style="background-color:#bfbfbf; width:28.5pt">\n			<p>No.</p>\n			</td>\n			<td style="background-color:#bfbfbf; width:219.35pt">\n			<p>Laboratorium</p>\n			</td>\n			<td style="background-color:#bfbfbf; width:76.1pt">\n			<p>Jumlah Asisten</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:28.5pt">\n			<p>1</p>\n			</td>\n			<td style="width:219.35pt">\n			<p>Business Process Analysis And Design Laboratory (BPAD)</p>\n			</td>\n			<td style="width:76.1pt">\n			<p>34</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:28.5pt">\n			<p>2</p>\n			</td>\n			<td style="width:219.35pt">\n			<p>ERP</p>\n			</td>\n			<td style="width:76.1pt">\n			<p>26</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:28.5pt">\n			<p>3</p>\n			</td>\n			<td style="width:219.35pt">\n			<p>PRODASE</p>\n			</td>\n			<td style="width:76.1pt">\n			<p>50</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:28.5pt">\n			<p>4</p>\n			</td>\n			<td style="width:219.35pt">\n			<p>Sistem Operasi Dan Jaringan Komputer</p>\n			</td>\n			<td style="width:76.1pt">\n			<p>26</p>\n			</td>\n		</tr>\n	</tbody>\n</table>\n\n<p>&nbsp;</p>\n\n<p>3. Kode Etik Mahasiswa dan Sanksi Akademik</p>\n\n<ol>\n	<li>Kode Etik adalah aturan yang dibuat oleh Universitas Telkom pada <strong>Surat Keputusan Rektor Universitas Telkom No:KR.069/ORG22/REK.0/2014</strong> yang berisi hak, kewajiban, norma, larangan, pelanggaran dan sanksi sebagai landasan untuk bersikap dan berperilaku selayaknya mahasiswa Universitas Telkom</li>\n	<li>Sanksi Akademik merupakan akibat hukum yang dikenakan kepada mahasiswa yang melanggar kode etik mahasiswa Universitas Telkom</li>\n	<li>Setiap mahasiswa di Universitas Telkom akan diberitahukan mengenai adanya kode etik di Unversitas Telkom saat ada perwalian pertama atau saat adanya kegiatan orientasi mahasiswa baru.</li>\n	<li>Larangan mahasiswa dikategorikan menjadi 3 beban, yaitu Ringan, Sedang dan Berat</li>\n	<li>Sanksi bagi mahasiswa yang melakukan pelanggaran terhadap kode etik dapat dikenakan sanksi moral atau sanksi lainnya</li>\n	<li>Jenis sanksi yang dibeikan kepada mahasiswa haruslah dilakukan pertimbangan terlebih dahulu</li>\n</ol>\n\n<p>4. Sarana Publikasi Karya Ilmiah dan Produk</p>\n\n<ol>\n	<li><a href="https://openlibrary.telkomuniversity.ac.id/">https://openlibrary.telkomuniversity.ac.id/</a></li>\n	<li><a href="http://journals.telkomuniversity.ac.id/index.php/jrsi">http://journals.telkomuniversity.ac.id/index.php/jrsi</a></li>\n	<li><a href="https://ijies.sie.telkomuniversity.ac.id/index.php/IJIES">https://ijies.sie.telkomuniversity.ac.id/index.php/IJIES</a></li>\n	<li><a href="https://icoiese.org">https://icoiese.org</a></li>\n	<li><a href="https://fsktm2.uthm.edu.my/scdm2016/upload">https://fsktm2.uthm.edu.my/scdm2016/upload</a></li>\n	<li>Perpustakaan Universitas Telkom</li>\n</ol>\n\n<p>5. Evaluasi Kinerja Program Sudi dan Fakultas</p>\n\n<ol>\n	<li><a href="https://properman.telkomuniversity.ac.id">https://properman.telkomuniversity.ac.id</a></li>\n</ol>\n\n<p>6. Sarana Sistem Informasi Akademik Utama</p>\n\n<ol>\n	<li>Sistem Informasi Akademik (i-Gracias): <a href="https://igracias.telkomuniversity.ac.id/">https://igracias.telkomuniversity.ac.id/</a></li>\n</ol>\n\n<p>7. Sarana Sistem Informasi Akademik Lainnya</p>\n\n<ol>\n	<li>Website Program Studi : <a href="http://bis.telkomuniversity.ac.id/web/">http://bis.telkomuniversity.ac.id/web/</a></li>\n	<li>Sistem Informasi Perpustakaan : <a href="https://openlibrary.telkomuniversity.ac.id/">https://openlibrary.telkomuniversity.ac.id/</a></li>\n	<li>Website CDC : <a href="https://career.telkomuniversity.ac.id/">https://career.telkomuniversity.ac.id/</a></li>\n	<li>Website PPM : <a href="http://ppm.telkomuniversity.ac.id/home/">http://ppm.telkomuniversity.ac.id/home/</a></li>\n</ol>\n\n<p>8. Fasilitas <em>International Class </em></p>\n\n<p>Terdapat 3 ruangan kelas yang merupakan 1 ruangan kelas angkatan 2014, 1 ruangan kelas angkatan 2015, 1 ruangan kelas angkatan 2016 yang berada di Gedung Tokong Nanas Telkom University dan memiliki fasilitas TV, galon, AC, proyektor, tempat sampah.</p>\n\n<ol>\n	<li>Sarana Umum Lingkungan Akademik Program Studi Sistem Informasi</li>\n</ol>\n\n<p>Sarana umum lingkungan akademik Program Studi Sistem Informasi adalah TEL-U Convention Center, Masjid (Masjid Syamsul Ulum, berkapasitas 3000 jamaah), Gedung asrama, kantin/ food court, minimarket, koperasi, kantor pos, bank, ATM, klinik, Joglo belajar outdoor, Danau Situ Tekno, area olah raga dan beberapa fasilitas penunjang lain.</p>\n\n<p>Suasana akademis merupakan suasana yang bersifat mendukung terciptanya lingkungan yang berbudaya akademis. Suasana ini dapat berbentuk penciptaan lingkungan fisik dan suasana yang dapat menjamin kenyamanan pelaksanaan budaya akademis. Selain itu suasana ini juga harus didukung dengan penciptaan komunitas individu dengan pemahaman yang baik terhadap budaya akademis serta adanya usaha serius dari setiap individu untuk mencapai karakter budaya akademis tersebut.</p>\n\n<p>Beberapa hal yang berhubungan dengan peningkatan kualitas suasana akademik menyangkut hal-hal sebagai berikut :</p>\n\n<ol>\n	<li>Lingkungan, Keindahan dan Keserasian Kampus;</li>\n	<li>Sarana Untuk Memelihara Interaksi Dosen-Mahasiswa dan untuk Menciptakan Iklim yang Mendorong Perkembangan Kegiatan Akademis/Profesional;</li>\n	<li>Mutu dan Kuantitas Interaksi Kegiatan Akademis Dosen, Mahasiswa dan Civitas Akademika lainnya;</li>\n	<li>Rancangan Menyeluruh untuk Mengembangkan Suasana Akademis yang Kondusif untuk Pembelajaran, Penelitian dan Pengabdian kepada Masyarakat;</li>\n	<li>Keikutsertaan Civitas Akademika Dalam Kegiatan Akademik (Seminar, Simposium, Diskusi, Eksebisi) di Kampus;</li>\n	<li>Pengembangan Kepribadian Ilmiah;</li>\n	<li>Pembagian Lisensi Software</li>\n</ol>\n\n<p><em>Software</em> atau perangkat lunak merupakan media pendukung yang digunakan mahasiswa pada praktikum untuk memperkaya kompetensi mahasiswa. <em>Software</em> yang digunakan merupakan <em>software</em> berlisensi asli namun penggunaannya hanya terbatas pada laboratorium. Untuk mendukung penggunaan <em>software</em> agar dapat digunakan secara luas oleh mahasiswa, program studi Sistem Informasi telah dibantu anggaran Universitas Telkom untuk membeli lisensi produk <em>software</em> Microsoft secara massal yang dapat digunakan dengan mengakses alamat situs <a href="https://e5.onthehub.com/WebStore/Security/Signin.aspx?ws=791253cf-7060-e411-9405-b8ca3a5db7a1&amp;vsro=8&amp;action=signout">https://e5.onthehub.com/WebStore/Security/Signin.aspx?ws=791253cf-7060-e411-9405-b8ca3a5db7a1&amp;vsro=8&amp;action=signout</a></p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>Mahasiswa harus terkoneksi ke jaringan server yang telah disediakan sehingga mahasiswa dapat menggunakan <em>software</em> secara legal.</p>\n\n<p>Berikut ini adalah daftar lisensi <em>software</em> yang telah digunakan :</p>\n\n<table border="1" cellspacing="0" style="width:372px">\n	<tbody>\n		<tr>\n			<td style="background-color:#bfbfbf; width:76.0pt">\n			<p>Laboratorium</p>\n			</td>\n			<td style="background-color:#bfbfbf; width:203.0pt">\n			<p>Software</p>\n			</td>\n		</tr>\n		<tr>\n			<td rowspan="7" style="width:76.0pt">\n			<p>Prodase</p>\n			</td>\n			<td style="width:203.0pt">\n			<p><em>Java Develpoment Kit</em></p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:203.0pt">\n			<p><em>NetBeans Java IDE</em></p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:203.0pt">\n			<p><em>Vmware Workstation</em></p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:203.0pt">\n			<p><em>Oracle Database Express Edition 11g</em></p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:203.0pt">\n			<p><em>XAMPP Web Server</em></p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:203.0pt">\n			<p><em>Android SDK</em></p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:203.0pt">\n			<p><em>Android Studio</em></p>\n			</td>\n		</tr>\n		<tr>\n			<td rowspan="6" style="width:76.0pt">\n			<p>Sisjar</p>\n			</td>\n			<td style="width:203.0pt">\n			<p><em>Cisco Packet Tracer</em></p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:203.0pt">\n			<p><em>Wireshark</em></p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:203.0pt">\n			<p><em>GNS3</em></p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:203.0pt">\n			<p><em>Vmware Workstation</em></p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:203.0pt">\n			<p><em>Hyper-V</em></p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:203.0pt">\n			<p><em>Putty</em></p>\n			</td>\n		</tr>\n		<tr>\n			<td rowspan="2" style="width:76.0pt">\n			<p>ERP</p>\n			</td>\n			<td style="width:203.0pt">\n			<p><em>SAP LOGON (SAP01 Fundamental, SCM 100 &amp; 300, HR050, AC010)</em></p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:203.0pt">\n			<p><em>Exam</em></p>\n			</td>\n		</tr>\n		<tr>\n			<td rowspan="3" style="width:76.0pt">\n			<p>BPAD</p>\n			</td>\n			<td style="width:203.0pt">\n			<p><em>Mega</em></p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:203.0pt">\n			<p><em>Power Designer</em></p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:203.0pt">\n			<p><em>Astah Professional</em></p>\n			</td>\n		</tr>\n	</tbody>\n</table>\n');

-- --------------------------------------------------------

--
-- Table structure for table `std5_573program`
--

CREATE TABLE `std5_573program` (
  `no` int(25) UNSIGNED NOT NULL,
  `program_dan_kegiatan` varchar(65000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `std5_573program`
--

INSERT INTO `std5_573program` (`no`, `program_dan_kegiatan`) VALUES
(1, '<p><strong>1. Program Penambahan Kompetensi Mahasiswa</strong></p>\n\n<p><strong>Tabel 5. </strong><strong>30 Program Penambahan Kompetensi Mahasiswa</strong></p>\n\n<table border="1" cellspacing="0" style="width:514px">\n	<tbody>\n		<tr>\n			<td style="background-color:#bfbfbf; width:41.5pt">\n			<p>Tahun</p>\n			</td>\n			<td style="background-color:#bfbfbf; width:96.05pt">\n			<p>Nama Kegiatan</p>\n			</td>\n			<td style="background-color:#bfbfbf; width:70.9pt">\n			<p>Tanggal Pelaksanaan</p>\n			</td>\n			<td style="background-color:#bfbfbf; width:49.6pt">\n			<p>Tempat</p>\n			</td>\n			<td style="background-color:#bfbfbf; width:56.7pt">\n			<p>Jumlah Peserta</p>\n			</td>\n			<td style="background-color:#bfbfbf; width:70.85pt">\n			<p>Keterangan</p>\n			</td>\n		</tr>\n		<tr>\n			<td rowspan="15" style="background-color:white; width:41.5pt">\n			<p>2014</p>\n			</td>\n			<td style="background-color:white; width:96.05pt">\n			<p>Workshop Internal International Office dengan Program Studi di Telkom University</p>\n			</td>\n			<td style="background-color:white; width:70.9pt">\n			<p>20 Januari 2014</p>\n			</td>\n			<td style="background-color:white; width:49.6pt">\n			<p>&nbsp;</p>\n			</td>\n			<td style="background-color:white; width:56.7pt">\n			<p>&nbsp;</p>\n			</td>\n			<td style="background-color:white; width:70.85pt">\n			<p>&nbsp;</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="background-color:white; width:96.05pt">\n			<p>Kunjungan Industri Program Studi S1 Sistem Informasi Departemen Rekayasa Industri Universitas Telkom 2014</p>\n			</td>\n			<td style="background-color:white; width:70.9pt">\n			<p>2 bulan sekali selama tahun 2014</p>\n			</td>\n			<td style="background-color:white; width:49.6pt">\n			<p>Jakarta</p>\n			</td>\n			<td style="background-color:white; width:56.7pt">\n			<p>40 orang (mahasiswa), 2 orang (dosen pembimbing)</p>\n			</td>\n			<td style="background-color:white; width:70.85pt">\n			<p>&nbsp;</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="background-color:white; width:96.05pt">\n			<p>Industrial Visit Goes To Microsoft Indonesia</p>\n			</td>\n			<td style="background-color:white; width:70.9pt">\n			<p>14 Maret 2014</p>\n			</td>\n			<td style="background-color:white; width:49.6pt">\n			<p>Kantor Microsoft Indonesia BEJ Tower 2 Lantai 18 SCBD Jakarta Selatan</p>\n			</td>\n			<td style="background-color:white; width:56.7pt">\n			<p>33 orang</p>\n			</td>\n			<td style="background-color:white; width:70.85pt">\n			<p>&nbsp;</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="background-color:white; width:96.05pt">\n			<p>Workshop Competition Center</p>\n			</td>\n			<td style="background-color:white; width:70.9pt">\n			<p>01 Juni 2014</p>\n			</td>\n			<td style="background-color:white; width:49.6pt">\n			<p>Laboratorium Prosman Lt. 2 Universitas Telkom</p>\n			</td>\n			<td style="background-color:white; width:56.7pt">\n			<p>&nbsp;</p>\n			</td>\n			<td style="background-color:white; width:70.85pt">\n			<p>&nbsp;</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="background-color:white; width:96.05pt">\n			<p>Kuliah Umum dan Penandatanganan Perjanjian Kerjasama Project Manajement Institute dan Telkom University</p>\n			</td>\n			<td style="background-color:white; width:70.9pt">\n			<p>27 September 2014</p>\n			</td>\n			<td style="background-color:white; width:49.6pt">\n			<p>Gedung K (Gedung Damar)</p>\n			</td>\n			<td style="background-color:white; width:56.7pt">\n			<p>&nbsp;</p>\n			</td>\n			<td style="background-color:white; width:70.85pt">\n			<p>&nbsp;</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="background-color:white; width:96.05pt">\n			<p>Seminar &quot;Smart, Modern and Religious&quot;</p>\n			</td>\n			<td style="background-color:white; width:70.9pt">\n			<p>3 Oktober 2014</p>\n			</td>\n			<td style="background-color:white; width:49.6pt">\n			<p>Aula Fakultas Industri Kreatif Lt. 4</p>\n			</td>\n			<td style="background-color:white; width:56.7pt">\n			<p>113 orang</p>\n			</td>\n			<td style="background-color:white; width:70.85pt">\n			<p>&nbsp;</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="background-color:white; width:96.05pt">\n			<p>IBM Bluemix Roadshow 2014</p>\n			</td>\n			<td style="background-color:white; width:70.9pt">\n			<p>09 Oktober 2014</p>\n			</td>\n			<td style="background-color:white; width:49.6pt">\n			<p>Gedung Multimedia LC Lantai 2</p>\n			</td>\n			<td style="background-color:white; width:56.7pt">\n			<p>&nbsp;</p>\n			</td>\n			<td style="background-color:white; width:70.85pt">\n			<p>&nbsp;</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="background-color:white; width:96.05pt">\n			<p>Class Room - Leader in You</p>\n			</td>\n			<td style="background-color:white; width:70.9pt">\n			<p>05 November 2014</p>\n			</td>\n			<td style="background-color:white; width:49.6pt">\n			<p>Telkom University Convention Hall</p>\n			</td>\n			<td style="background-color:white; width:56.7pt">\n			<p>46 orang</p>\n			</td>\n			<td style="background-color:white; width:70.85pt">\n			<p>&nbsp;</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="background-color:white; width:96.05pt">\n			<p>Class Room - Self Awareness</p>\n			</td>\n			<td style="background-color:white; width:70.9pt">\n			<p>7 November 2014, 12 November 2014, 21 November 2014</p>\n			</td>\n			<td style="background-color:white; width:49.6pt">\n			<p>Gedung H (GSG) Universitas Telkom</p>\n			</td>\n			<td style="background-color:white; width:56.7pt">\n			<p>120 orang</p>\n			</td>\n			<td style="background-color:white; width:70.85pt">\n			<p>&nbsp;</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="background-color:white; width:96.05pt">\n			<p>Seminar teknologi &quot;Grab the Opportunity of Technology&quot;.</p>\n			</td>\n			<td style="background-color:white; width:70.9pt">\n			<p>15 November 2014</p>\n			</td>\n			<td style="background-color:white; width:49.6pt">\n			<p>&nbsp;</p>\n			</td>\n			<td style="background-color:white; width:56.7pt">\n			<p>&nbsp;</p>\n			</td>\n			<td style="background-color:white; width:70.85pt">\n			<p>&nbsp;</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:96.05pt">\n			<p>Pelatihan <em>Basic Routing</em> <em>Config</em></p>\n			</td>\n			<td style="width:70.9pt">\n			<p>23 November 2014</p>\n			</td>\n			<td style="width:49.6pt">\n			<p>Central Lab B dan Central Lab C</p>\n			</td>\n			<td style="background-color:white; width:56.7pt">\n			<p>&nbsp;</p>\n			</td>\n			<td style="background-color:white; width:70.85pt">\n			<p>&nbsp;</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="background-color:white; width:96.05pt">\n			<p>Class Room - Study Skill</p>\n			</td>\n			<td style="background-color:white; width:70.9pt">\n			<p>3 dan 5 Desember 2014</p>\n			</td>\n			<td style="background-color:white; width:49.6pt">\n			<p>Gedung GSG Universitas Telkom</p>\n			</td>\n			<td style="background-color:white; width:56.7pt">\n			<p>43 orang</p>\n			</td>\n			<td style="background-color:white; width:70.85pt">\n			<p>&nbsp;</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="background-color:white; width:96.05pt">\n			<p>Seminar ISIM (How To Be a Network Engineer)</p>\n			</td>\n			<td style="background-color:white; width:70.9pt">\n			<p>06 Desember 2014</p>\n			</td>\n			<td style="background-color:white; width:49.6pt">\n			<p>Gedung P</p>\n			</td>\n			<td style="background-color:white; width:56.7pt">\n			<p>&nbsp;</p>\n			</td>\n			<td style="background-color:white; width:70.85pt">\n			<p>&nbsp;</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="background-color:white; width:96.05pt">\n			<p>Pelatihan SAP01 Fundamental SI</p>\n			</td>\n			<td style="width:70.9pt">\n			<p>&nbsp;</p>\n			</td>\n			<td style="width:49.6pt">\n			<p>&nbsp;</p>\n			</td>\n			<td style="width:56.7pt">\n			<p>&nbsp;</p>\n			</td>\n			<td style="width:70.85pt">\n			<p>&nbsp;</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="background-color:white; width:96.05pt">\n			<p>Healthy Days</p>\n			</td>\n			<td style="background-color:white; width:70.9pt">\n			<p>&nbsp;</p>\n			</td>\n			<td style="background-color:white; width:49.6pt">\n			<p>&nbsp;</p>\n			</td>\n			<td style="background-color:white; width:56.7pt">\n			<p>203 orang</p>\n			</td>\n			<td style="background-color:white; width:70.85pt">\n			<p>&nbsp;</p>\n			</td>\n		</tr>\n		<tr>\n			<td rowspan="16" style="background-color:white; width:41.5pt">\n			<p>2015</p>\n			</td>\n			<td style="background-color:white; width:96.05pt">\n			<p>Kuliah Umum Anti Plagiarism</p>\n			</td>\n			<td style="background-color:white; width:70.9pt">\n			<p>07 Januari 2015</p>\n			</td>\n			<td style="background-color:white; width:49.6pt">\n			<p>Ged. Karang Lt. 2 Universitas Telkom</p>\n			</td>\n			<td style="background-color:white; width:56.7pt">\n			<p>&nbsp;</p>\n			</td>\n			<td style="background-color:white; width:70.85pt">\n			<p>&nbsp;</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:96.05pt">\n			<p>Pelatihan <em>Routing</em> dan <em>Switching</em> (VLAN, InterVLAN, EIGRP, NAT)</p>\n			</td>\n			<td style="width:70.9pt">\n			<p>19 April 2015</p>\n			</td>\n			<td style="width:49.6pt">\n			<p>Central Lab A dan Central Lab B</p>\n			</td>\n			<td style="background-color:white; width:56.7pt">\n			<p>&nbsp;</p>\n			</td>\n			<td style="background-color:white; width:70.85pt">\n			<p>&nbsp;</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="background-color:white; width:96.05pt">\n			<p>Seminar The Backstage TechInAsia - Ziliun</p>\n			</td>\n			<td style="background-color:white; width:70.9pt">\n			<p>06 Mei 2015</p>\n			</td>\n			<td style="background-color:white; width:49.6pt">\n			<p>Aula Bandung Techno Park</p>\n			</td>\n			<td style="background-color:white; width:56.7pt">\n			<p>&nbsp;</p>\n			</td>\n			<td style="background-color:white; width:70.85pt">\n			<p>&nbsp;</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="background-color:white; width:96.05pt">\n			<p>Pelatihan Indonesia Android Academy</p>\n			</td>\n			<td style="background-color:white; width:70.9pt">\n			<p>29-30 Agustus 2015</p>\n			</td>\n			<td style="background-color:white; width:49.6pt">\n			<p>Central Lab c 103 FRI</p>\n			</td>\n			<td style="background-color:white; width:56.7pt">\n			<p>79 orang</p>\n			</td>\n			<td style="background-color:white; width:70.85pt">\n			<p>&nbsp;</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="background-color:white; width:96.05pt">\n			<p>Seminar Cisco Networking</p>\n			</td>\n			<td style="background-color:white; width:70.9pt">\n			<p>18 September 2015</p>\n			</td>\n			<td style="background-color:white; width:49.6pt">\n			<p>Gedung K (Gedung Damar)</p>\n			</td>\n			<td style="background-color:white; width:56.7pt">\n			<p>&nbsp;</p>\n			</td>\n			<td style="background-color:white; width:70.85pt">\n			<p>&nbsp;</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="background-color:white; width:96.05pt">\n			<p>Seminar CISCO <em>NETWORKING</em></p>\n			</td>\n			<td style="width:70.9pt">\n			<p>18 September 2015</p>\n			</td>\n			<td style="width:49.6pt">\n			<p>Gedung Damar</p>\n			</td>\n			<td style="background-color:white; width:56.7pt">\n			<p>&nbsp;</p>\n			</td>\n			<td style="background-color:white; width:70.85pt">\n			<p>&nbsp;</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:96.05pt">\n			<p>Industrial Visit Tokopedia</p>\n			</td>\n			<td style="background-color:white; width:70.9pt">\n			<p>02 Oktober 2015</p>\n			</td>\n			<td style="background-color:white; width:49.6pt">\n			<p>Tokopedia Office</p>\n			</td>\n			<td style="background-color:white; width:56.7pt">\n			<p>&nbsp;</p>\n			</td>\n			<td style="background-color:white; width:70.85pt">\n			<p>&nbsp;</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="background-color:white; width:96.05pt">\n			<p>Kuliah Umum Manajemen Proyek</p>\n			</td>\n			<td style="background-color:white; width:70.9pt">\n			<p>28 November 2015</p>\n			</td>\n			<td style="background-color:white; width:49.6pt">\n			<p>Gedung Pelampong (GSG)</p>\n			</td>\n			<td style="background-color:white; width:56.7pt">\n			<p>&nbsp;</p>\n			</td>\n			<td style="background-color:white; width:70.85pt">\n			<p>&nbsp;</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="background-color:white; width:96.05pt">\n			<p>Pelatihan ICT (<em>Information and Comunication Technoloagy</em>)</p>\n			</td>\n			<td style="background-color:white; width:70.9pt">\n			<p>29 November 2015</p>\n			</td>\n			<td style="background-color:white; width:49.6pt">\n			<p>Central Lab A FRI, Telkom University</p>\n			</td>\n			<td style="background-color:white; width:56.7pt">\n			<p>20 orang</p>\n			</td>\n			<td style="background-color:white; width:70.85pt">\n			<p>&nbsp;</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:96.05pt">\n			<p>Pelatihan <em>Basic Routing</em>, <em>Subnetting</em> &amp; Topologi Jaringan</p>\n			</td>\n			<td style="width:70.9pt">\n			<p>29 November 2015</p>\n			</td>\n			<td style="width:49.6pt">\n			<p>Central Lab B dan Central Lab C</p>\n			</td>\n			<td style="background-color:white; width:56.7pt">\n			<p>&nbsp;</p>\n			</td>\n			<td style="background-color:white; width:70.85pt">\n			<p>&nbsp;</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="background-color:white; width:96.05pt">\n			<p>Pelatihan Basic Study Skill</p>\n			</td>\n			<td style="background-color:white; width:70.9pt">\n			<p>&nbsp;</p>\n			</td>\n			<td style="background-color:white; width:49.6pt">\n			<p>&nbsp;</p>\n			</td>\n			<td style="background-color:white; width:56.7pt">\n			<p>&nbsp;</p>\n			</td>\n			<td style="background-color:white; width:70.85pt">\n			<p>&nbsp;</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="background-color:white; width:96.05pt">\n			<p>Talkshow Karir &quot;Find Your Golden Ticket to reach Your Brighter Future&quot;</p>\n			</td>\n			<td style="background-color:white; width:70.9pt">\n			<p>&nbsp;</p>\n			</td>\n			<td style="background-color:white; width:49.6pt">\n			<p>&nbsp;</p>\n			</td>\n			<td style="background-color:white; width:56.7pt">\n			<p>&nbsp;</p>\n			</td>\n			<td style="background-color:white; width:70.85pt">\n			<p>&nbsp;</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="background-color:white; width:96.05pt">\n			<p>Pelatihan Etika Pergaulan</p>\n			</td>\n			<td style="background-color:white; width:70.9pt">\n			<p>&nbsp;</p>\n			</td>\n			<td style="background-color:white; width:49.6pt">\n			<p>&nbsp;</p>\n			</td>\n			<td style="background-color:white; width:56.7pt">\n			<p>&nbsp;</p>\n			</td>\n			<td style="background-color:white; width:70.85pt">\n			<p>&nbsp;</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="background-color:white; width:96.05pt">\n			<p>Healthy Days</p>\n			</td>\n			<td style="background-color:white; width:70.9pt">\n			<p>&nbsp;</p>\n			</td>\n			<td style="background-color:white; width:49.6pt">\n			<p>&nbsp;</p>\n			</td>\n			<td style="background-color:white; width:56.7pt">\n			<p>&nbsp;</p>\n			</td>\n			<td style="background-color:white; width:70.85pt">\n			<p>&nbsp;</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="background-color:white; width:96.05pt">\n			<p>Pelatihan SAP01 Fundamental SI</p>\n			</td>\n			<td style="background-color:white; width:70.9pt">\n			<p>&nbsp;</p>\n			</td>\n			<td style="background-color:white; width:49.6pt">\n			<p>&nbsp;</p>\n			</td>\n			<td style="background-color:white; width:56.7pt">\n			<p>&nbsp;</p>\n			</td>\n			<td style="background-color:white; width:70.85pt">\n			<p>&nbsp;</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="background-color:white; width:96.05pt">\n			<p>Pelatihan Personal Goal Setting</p>\n			</td>\n			<td style="background-color:white; width:70.9pt">\n			<p>&nbsp;</p>\n			</td>\n			<td style="background-color:white; width:49.6pt">\n			<p>&nbsp;</p>\n			</td>\n			<td style="background-color:white; width:56.7pt">\n			<p>&nbsp;</p>\n			</td>\n			<td style="background-color:white; width:70.85pt">\n			<p>&nbsp;</p>\n			</td>\n		</tr>\n		<tr>\n			<td rowspan="15" style="background-color:white; width:41.5pt">\n			<p>2016</p>\n			</td>\n			<td style="background-color:white; width:96.05pt">\n			<p>Seminar Techin Asia</p>\n			</td>\n			<td style="background-color:white; width:70.9pt">\n			<p>28 Januari 2016</p>\n			</td>\n			<td style="background-color:white; width:49.6pt">\n			<p>Ruang Rapat FRI lantai 3</p>\n			</td>\n			<td style="background-color:white; width:56.7pt">\n			<p>33 orang</p>\n			</td>\n			<td style="background-color:white; width:70.85pt">\n			<p>&nbsp;</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="background-color:white; width:96.05pt">\n			<p>Workshop Program Kerja</p>\n			</td>\n			<td style="background-color:white; width:70.9pt">\n			<p>10 Maret 2016</p>\n			</td>\n			<td style="background-color:white; width:49.6pt">\n			<p>Gedung B 107</p>\n			</td>\n			<td style="background-color:white; width:56.7pt">\n			<p>20 orang</p>\n			</td>\n			<td style="background-color:white; width:70.85pt">\n			<p>&nbsp;</p>\n			</td>\n		</tr>\n		<tr>\n			<td rowspan="2" style="background-color:white; width:96.05pt">\n			<p>Seminar Nasional Sistem Informasi Menghadapi MEA</p>\n			</td>\n			<td rowspan="2" style="background-color:white; width:70.9pt">\n			<p>15 Maret 2016</p>\n			</td>\n			<td rowspan="2" style="background-color:white; width:49.6pt">\n			<p>Telkom Universty</p>\n			</td>\n			<td style="background-color:white; width:56.7pt">\n			<p>40 orang</p>\n			</td>\n			<td style="background-color:white; width:70.85pt">\n			<p>Peserta Luar Tel-U</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="background-color:white; width:56.7pt">\n			<p>38 orang</p>\n			</td>\n			<td style="background-color:white; width:70.85pt">\n			<p>Peserta Tel-U</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="background-color:white; width:96.05pt">\n			<p>Studi Banding STT Nusaputra</p>\n			</td>\n			<td style="background-color:white; width:70.9pt">\n			<p>23 Maret 2016</p>\n			</td>\n			<td style="background-color:white; width:49.6pt">\n			<p>Ruang Rapat FRI lantai 2</p>\n			</td>\n			<td style="background-color:white; width:56.7pt">\n			<p>36 orang</p>\n			</td>\n			<td style="background-color:white; width:70.85pt">\n			<p>&nbsp;</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="background-color:white; width:96.05pt">\n			<p>Sosialisasi PKM dengan Lab</p>\n			</td>\n			<td style="background-color:white; width:70.9pt">\n			<p>01 April 2016</p>\n			</td>\n			<td style="background-color:white; width:49.6pt">\n			<p>Gedung C200</p>\n			</td>\n			<td style="background-color:white; width:56.7pt">\n			<p>17 orang</p>\n			</td>\n			<td style="background-color:white; width:70.85pt">\n			<p>&nbsp;</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="background-color:white; width:96.05pt">\n			<p>Sosialisasi Tugas Akhir dan Sidang Periode Semester Genap 2015/2016</p>\n			</td>\n			<td style="background-color:white; width:70.9pt">\n			<p>07 April 2016</p>\n			</td>\n			<td style="background-color:white; width:49.6pt">\n			<p>Gedung K (Gedung Damar)</p>\n			</td>\n			<td style="background-color:white; width:56.7pt">\n			<p>160 orang</p>\n			</td>\n			<td style="background-color:white; width:70.85pt">\n			<p>&nbsp;</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:96.05pt">\n			<p>Pelatihan <em>Dynamic Routing</em>, VLAN dan ACL</p>\n			</td>\n			<td style="width:70.9pt">\n			<p>10 April 2016</p>\n			</td>\n			<td style="width:49.6pt">\n			<p>Central Lab B dan Central Lab C</p>\n			</td>\n			<td style="background-color:white; width:56.7pt">\n			<p>&nbsp;</p>\n			</td>\n			<td style="background-color:white; width:70.85pt">\n			<p>&nbsp;</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="background-color:white; width:96.05pt">\n			<p>Sosialisasi Kurikulum</p>\n			</td>\n			<td style="background-color:white; width:70.9pt">\n			<p>15 April 2016</p>\n			</td>\n			<td style="background-color:white; width:49.6pt">\n			<p>Gedung K (Gedung Damar)</p>\n			</td>\n			<td style="background-color:white; width:56.7pt">\n			<p>436 orang</p>\n			</td>\n			<td style="background-color:white; width:70.85pt">\n			<p>Angkatan 2013, 2014, 2015</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="background-color:white; width:96.05pt">\n			<p>Kuliah Umum Mata Kuliah Pelatihan dan Sertifikasi</p>\n			</td>\n			<td style="background-color:white; width:70.9pt">\n			<p>02 September 2016</p>\n			</td>\n			<td style="background-color:white; width:49.6pt">\n			<p>Gedung K (Gedung Damar)</p>\n			</td>\n			<td style="background-color:white; width:56.7pt">\n			<p>185 orang</p>\n			</td>\n			<td style="background-color:white; width:70.85pt">\n			<p>&nbsp;</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:96.05pt">\n			<p>Pelatihan Pengenalan <em>Cisco Packet Tracer</em> dan GNS3</p>\n			</td>\n			<td style="width:70.9pt">\n			<p>04 September 2016</p>\n			</td>\n			<td style="width:49.6pt">\n			<p>Central Lab A dan Central Lab B</p>\n			</td>\n			<td style="background-color:white; width:56.7pt">\n			<p>&nbsp;</p>\n			</td>\n			<td style="background-color:white; width:70.85pt">\n			<p>&nbsp;</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="background-color:white; width:96.05pt">\n			<p>Startup Day</p>\n			</td>\n			<td style="background-color:white; width:70.9pt">\n			<p>11 November 2016</p>\n			</td>\n			<td style="background-color:white; width:49.6pt">\n			<p>Gedung Learning Center</p>\n			</td>\n			<td style="background-color:white; width:56.7pt">\n			<p>60 orang</p>\n			</td>\n			<td style="background-color:white; width:70.85pt">\n			<p>&nbsp;</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="background-color:white; width:96.05pt">\n			<p>Seminar Techin Asia Jakarta</p>\n			</td>\n			<td style="background-color:white; width:70.9pt">\n			<p>16 November 2016</p>\n			</td>\n			<td style="background-color:white; width:49.6pt">\n			<p>Balai Kartini Jakarta</p>\n			</td>\n			<td style="background-color:white; width:56.7pt">\n			<p>&nbsp;</p>\n			</td>\n			<td style="background-color:white; width:70.85pt">\n			<p>&nbsp;</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="background-color:white; width:96.05pt">\n			<p>ISACA &ndash; Cybersecurity&rsquo;s Role in Enterprise Risk Management</p>\n			</td>\n			<td style="background-color:white; width:70.9pt">\n			<p>17 November 2016</p>\n			</td>\n			<td style="background-color:white; width:49.6pt">\n			<p>Aula FIT (Fakultas Ilmu Terapan)</p>\n			</td>\n			<td style="background-color:white; width:56.7pt">\n			<p>&nbsp;</p>\n			</td>\n			<td style="background-color:white; width:70.85pt">\n			<p>&nbsp;</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="background-color:white; width:96.05pt">\n			<p>Building Credibility</p>\n			</td>\n			<td style="background-color:white; width:70.9pt">\n			<p>&nbsp;</p>\n			</td>\n			<td style="background-color:white; width:49.6pt">\n			<p>&nbsp;</p>\n			</td>\n			<td style="background-color:white; width:56.7pt">\n			<p>&nbsp;</p>\n			</td>\n			<td style="background-color:white; width:70.85pt">\n			<p>&nbsp;</p>\n			</td>\n		</tr>\n	</tbody>\n</table>\n\n<p>&nbsp;</p>\n\n<p><strong>2. Program Kursus Bahasa Inggris</strong></p>\n\n<p>Kursus Bahasa Inggris digunakan untuk memperkaya kompetensi mahasiswa dengan bahasa Inggris. Program kursus bahasa Inggris telah dilaksanakan selama 3 (tiga) semester dari tahun 2012. Program ini dilaksanakan kepada mahasiswa tingkat 4 (empat) atau semester 7 (tujuh) dan 8 (delapan). Program kursus bahasa inggris dilaksanakan kepada 2 (dua) kategori, yaitu :</p>\n\n<p><strong>a. Program bahasa Inggris untuk mahasiswa</strong></p>\n\n<p>Kelas bahasa Inggris telah dibuka dan diperuntukkan bagi mahasiswa tingkat akhir yang dilaksanakan seminggu dua kali selama satu semester.</p>\n\n<p><strong>b. Program bahasa Inggris untuk dosen</strong></p>\n\n<p>Selain mahasiswa, dosen juga mendapatkan program kursus bahasa Inggris. Peningkatan kemampuan dosen diharapkan dapat mempersiapkan dosen-dosen untuk keperluan seperti kuliah lanjut, penelitian, dan komunikasi ilmiah lainnya.</p>\n\n<p><strong>3. Program Persiapan dan Pembimbingan Lomba</strong></p>\n\n<p>Bimbingan juga dilakukan pada mahasiswa yang akan mengikuti kompetisi dan lomba baik skala Nasional maupun Internasional, sehingga pencapaiannya dapat maksimal. Bimbingan terhadap kelompok mahasiswa dilakukan oleh dosen yang memiliki kompetensi didalamnya. Pembekalan dilakukan untuk keikutsertaan dalam lomba PKM pada setiap tahunnya dan beberapa lomba lainnya. Dari pembinaan lomba yang dilakukan dihasilkan 23 raihan kejuaraan lomba mahasiswa, seperti yang dijelaskan pada Standar 3.</p>\n\n<p><strong>4. Study Group di Laboratorium</strong></p>\n\n<p>Selain bernaung pada tingkat program studi dan keprofesian, Kelompok belajar atau Study Group juga berada di bawah Laboratorium untuk membahas topik-topik khusus yang ada dibawah bidang Laboratorium tertentu sehingga ilmu yang ada di Laboratorium semakin berkembang. Kelompok Belajar ini terdiri dari mahasiswa-mahasiswa yang diseleksi oleh masing-masing laboratorium. Berikut ini adalah daftar Kelompok Belajar yang ada di Laboratorium :</p>\n\n<p><strong>Tabel 5. </strong><strong>31 Kelompok Belajar yang ada di Laboratorium</strong></p>\n\n<table border="1" cellspacing="0" style="width:441px">\n	<tbody>\n		<tr>\n			<td style="background-color:#999999; width:161.0pt">\n			<p>Kelompok Belajar</p>\n			</td>\n			<td style="background-color:#999999; width:170.0pt">\n			<p>Laboratorium</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:161.0pt">\n			<p><em>Enterprise Package Solution</em></p>\n			</td>\n			<td style="width:170.0pt">\n			<p>Laboratorium <em>Enterprise Resource Planning</em></p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:161.0pt">\n			<p>BPAD <em>Laboratory Club</em> (BLC)</p>\n			</td>\n			<td style="width:170.0pt">\n			<p>Laboratorium <em>Business Process Analysis and Design</em></p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:161.0pt">\n			<p><em>Information System Networking</em> (IS.Net)</p>\n			</td>\n			<td rowspan="2" style="width:170.0pt">\n			<p>Laboratorium Sistem Operasi dan Jaringan Komputer</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:161.0pt">\n			<p><em>Information Security Club</em> (INSECLUB)</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:161.0pt">\n			<p>Prodase <em>Innovation Club</em> (PIC)</p>\n			</td>\n			<td style="width:170.0pt">\n			<p>Laboratorium <em>Programming</em> dan <em>Database</em></p>\n			</td>\n		</tr>\n	</tbody>\n</table>\n\n<p>&nbsp;</p>\n\n<p><strong>5. Program Orientasi Mahasiswa Baru (GENESIS)</strong></p>\n\n<p><strong>Tabel 5. </strong><strong>32 Program Orientasi Mahasiswa Baru (GENESIS)</strong></p>\n\n<table border="1" cellspacing="0" style="width:512px">\n	<thead>\n		<tr>\n			<td style="background-color:silver; width:35.2pt">\n			<p><strong>No.</strong></p>\n			</td>\n			<td style="background-color:silver; width:74.0pt">\n			<p><strong>Nama Kegiatan</strong></p>\n			</td>\n			<td style="background-color:silver; width:274.5pt">\n			<p><strong>Tujuan</strong></p>\n			</td>\n		</tr>\n	</thead>\n	<tbody>\n		<tr>\n			<td style="width:35.2pt">\n			<p>1</p>\n\n			<p>&nbsp;</p>\n			</td>\n			<td style="width:74.0pt">\n			<p>Pra Opening</p>\n\n			<p>&nbsp;</p>\n			</td>\n			<td style="width:274.5pt">\n			<ol>\n				<li>Pengenalan GENESIS dan &ldquo;apa itu GENESIS&rdquo; kepada para peserta GENESIS.</li>\n				<li>Pemberian info tentang persiapan yang diperlukan untuk melaksanakan kegiatan GENESIS.</li>\n				<li>Pembagian kelompok dalam kegiatan GENESIS.</li>\n			</ol>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:35.2pt">\n			<p>2</p>\n			</td>\n			<td style="width:74.0pt">\n			<p>Opening</p>\n			</td>\n			<td style="width:274.5pt">\n			<ol>\n				<li>Persiapan awal sebelum pembukaan GENESIS.</li>\n				<li>Perkenalan intern kelompok dan antar peserta GENESIS.</li>\n				<li>Memberitahukan batasan yang harus dipatuhi serta target yang harus dipenuhi oleh peserta GENESIS.</li>\n				<li>Memperkenalkan peserta GENESIS dengan kakak pendamping masing-masing kelompok.</li>\n			</ol>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:35.2pt">\n			<p>3</p>\n\n			<p>&nbsp;</p>\n			</td>\n			<td style="width:74.0pt">\n			<p><em>This Is Informsion System and Cyan</em></p>\n\n			<p>&nbsp;</p>\n			</td>\n			<td style="width:274.5pt">\n			<ol>\n				<li>Memperkenalkan peserta GENESIS dengan Himpunan dan Badan Legislatif yang ada di Fakultas Rekayasa Industri (sesuai jurusan yang diambil selama studi di Universitas Telkom).</li>\n				<li>Memperkenalkan peserta GENESIS dengan laboratorium yang ada di Fakultas Rekayasa Industri (sesuai jurusan yang diambil selama studi di Universitas Telkom).</li>\n				<li>Memberi gambaran tentang ilmu apa yang akan diperoleh dari masing-masing laboratorium.</li>\n				<li>Melakukan seminar tentang PKM untuk seluruh peserta GENESIS</li>\n			</ol>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:35.2pt">\n			<p>4</p>\n\n			<p>&nbsp;</p>\n			</td>\n			<td style="width:74.0pt">\n			<p><em>Cleaning Habbitable Space</em></p>\n			</td>\n			<td style="width:274.5pt">\n			<ol>\n				<li>\n				<ol>\n					<li>Peserta melakukan kegiatan bersih-bersih lingkungan di sekitar kampus.</li>\n					<li>Peserta mempresentasikan hasil PKM yang telah diberikan oleh panitia</li>\n				</ol>\n				</li>\n			</ol>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:35.2pt">\n			<p>5</p>\n\n			<p>&nbsp;</p>\n			</td>\n			<td style="width:74.0pt">\n			<p><em>Pride of High Altitude</em></p>\n\n			<p>&nbsp;</p>\n			</td>\n			<td style="width:274.5pt">\n			<ol>\n				<li>Memberikan motivasi kepada para peserta GENESIS</li>\n				<li>Mengubah mind set para peserta menjadi mindset yang lebih dewasa</li>\n			</ol>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:35.2pt">\n			<p>6</p>\n			</td>\n			<td style="width:74.0pt">\n			<p>Closing</p>\n			</td>\n			<td style="width:274.5pt">\n			<p>Penutupan secara resmi kegiatan GENESIS</p>\n			</td>\n		</tr>\n	</tbody>\n</table>\n');

-- --------------------------------------------------------

--
-- Table structure for table `std5_574interaksi`
--

CREATE TABLE `std5_574interaksi` (
  `no` int(25) UNSIGNED NOT NULL,
  `interaksi_akademik` varchar(65000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `std5_574interaksi`
--

INSERT INTO `std5_574interaksi` (`no`, `interaksi_akademik`) VALUES
(1, '<p><strong>1. Interaksi akademik antara Dosen-Mahasiswa</strong></p>\n\n<p>Interaksi antara Dosen dan Mahasiswa yang kondusif akan memberi pengaruh besar pada peningkatan kualitas suasana akademik dan mutu akademik. Interaksi ini harus terus ditingkatkan agar dapat meningkatkan kreatifitas Dosen dan Mahasiswa baik yang bersifat akademik maupun non akademik. Beberapa interaksi yang terjadi antara Dosen dan Mahasiswa adalah sebagai berikut :</p>\n\n<p><strong>a. Perwalian</strong></p>\n\n<p>Interaksi antara Dosen dan Mahasiswa yang dilakukan secara rutin adalah perwalian. Interaksi ini dilakukan minimal 4 (empat) kali per semester. Perwalian pertama dilakukan pada saat pengambilan matakuliah. Perwalian kedua dan ketiga dilakukan pada saat perkuliahan berlangsung. Perwalian terakhir dilakukan pada saat perkuliahan telah selesai. Beberapa topik yang dibahas pada saat perwalian adalah :</p>\n\n<ol>\n	<li>Perencanaan studi mahasiswa</li>\n	<li>Cuti akademik</li>\n	<li>Sidang akademik kenaikan tingkat</li>\n	<li>Bimbingan kerja praktek dan geladi</li>\n	<li>Permasalahan umum lain yang terkait dengan perbaikan proses pencapaian target perkuliahan.</li>\n</ol>\n\n<p><strong>b. Konsultasi Tugas Mahasiswa</strong></p>\n\n<p>Proses belajar mengajar tidak hanya dilakukan dikelas, namun Dosen juga menyediakan kesempatan mahasiswa untuk melanjutkan proses belajar juga dapat dilanjutkan diluar kelas formal dengan bentuk konsultasi mahasiswa dan dosen. Konsultasi ini dilakukan untuk mendiskusikan berbagai permasalahan kuliah baik tugas kuliah, materi kuliah, tugas besar, praktikum, dan lain-lain. Konsultasi ini dapat berupa responsi di kelas, konsultasi langsung di ruangan dosen, konsultasi di Laboratorium, dan bentuk-bentuk lainnya.</p>\n\n<p><strong>c. Bimbingan Tugas Akhir</strong></p>\n\n<p>Mahasiswa yang sudah mengambil topik Tugas Ahir diwajibkan untuk melakukan bimbingan dengan Dosen pembimbing sesuai dengan topik Tugas Akhir yang diambil. Bimbingan dilakukan secara berkelompok dan sudah terbagi dalam keprofesian masing-masing. Pada proses bimbingan mahasiswa mempresentasikan progress tugas akhirnya kepada Dosen dan Mahasiswa yang hadir pada forum bimbingan. Bimbingan Tugas Akhir secara personil juga dilakukan ketika dibutuhkan dan bersifat tentatif.</p>\n\n<p><strong>d. Kelompok Belajar dan Keprofesian</strong></p>\n\n<p>Interaksi mahasiswa dan dosen juga difasilitasi dengan adanya kelompok belajar dan keprofesian. Pada kelompok belajar dan keprofesian, mahasiswa dan dosen berinteraksi membahas beberapa topik-topik lintas mata kuliah menarik yang berkaitan dengan Sistem Informasi. Aktivitas keprofesian juga membahas tentang diskusi tentang paper, diskusi tentang buku, dan diskusi tentang isu-isu yang menarik saat ini. Berikut ini adalah daftar kelompok keahlian dan keprofesian yang ada.</p>\n\n<p>Kelompok Keahlian dan Keprofesian :</p>\n\n<p><strong>Tabel 5. </strong><strong>34 Kelompok Keahlian dan Keprofesian</strong></p>\n\n<table border="1" cellspacing="0" style="width:378.3pt">\n	<thead>\n		<tr>\n			<td style="background-color:#bfbfbf; width:28.5pt">\n			<p>No.</p>\n			</td>\n			<td style="background-color:#bfbfbf; width:155.8pt">\n			<p>Kelompok Keahlian</p>\n			</td>\n			<td style="background-color:#bfbfbf; width:194.0pt">\n			<p>Keprofesian</p>\n			</td>\n		</tr>\n	</thead>\n	<tbody>\n		<tr>\n			<td rowspan="4" style="width:28.5pt">\n			<p>1</p>\n			</td>\n			<td rowspan="4" style="width:155.8pt">\n			<p>Enterprise Solution Assurance</p>\n			</td>\n			<td style="width:194.0pt">\n			<p>Enterprise Packaged Solution</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:194.0pt">\n			<p>Enterprise Architecture Assurance,</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:194.0pt">\n			<p>Enterprise Infrastructure Management,</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:194.0pt">\n			<p>Information Technology Govermance Risk and Compliance</p>\n			</td>\n		</tr>\n		<tr>\n			<td rowspan="3" style="width:28.5pt">\n			<p>2</p>\n			</td>\n			<td rowspan="3" style="width:155.8pt">\n			<p>Enterprise System Development</p>\n			</td>\n			<td style="width:194.0pt">\n			<p>Enterprise Application Development</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:194.0pt">\n			<p>Enterprise Data Management</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:194.0pt">\n			<p>Technopreneurship</p>\n			</td>\n		</tr>\n	</tbody>\n</table>\n\n<p>&nbsp;</p>\n\n<p><strong>e. Pemanfaatan Media e-mail dan Media Sosial</strong></p>\n\n<p>Interaksi mahasiswa dan dosen juga difasilitasi dengan adanya e-mail dan mailing list. Berikut beberapa contoh daftar Mailing List sebagai sarana interaksi Dosen dengan mahasiswa.</p>\n\n<p>Jejaring sosial di dunia maya (blog, facebook, twitter, dsb)</p>\n\n<p><strong>Tabel 5. </strong><strong>35</strong><strong> Jejaring sosial di dunia maya (blog, facebook, twitter, dsb)</strong></p>\n\n<table border="1" cellspacing="0">\n	<tbody>\n		<tr>\n			<td style="background-color:#a6a6a6; width:34.8pt">\n			<p><strong>No</strong></p>\n			</td>\n			<td style="background-color:#a6a6a6; width:184.8pt">\n			<p><strong>Nama </strong></p>\n			</td>\n			<td style="background-color:#a6a6a6; width:157.5pt">\n			<p><strong>Keterangan</strong></p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:34.8pt">\n			<p>1</p>\n			</td>\n			<td style="width:184.8pt">\n			<p>PRODI SI</p>\n			</td>\n			<td style="width:157.5pt">\n			<p>Grup di Facebook untuk program studi Sistem Informasi</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:34.8pt">\n			<p>2</p>\n			</td>\n			<td style="width:184.8pt">\n			<p>ERP</p>\n			</td>\n			<td style="width:157.5pt">\n			<p>Grup di Facebook untuk kelompok keprofesian ERP</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:34.8pt">\n			<p>3</p>\n			</td>\n			<td style="width:184.8pt">\n			<p>Mata Kuliah Kelas SI</p>\n			</td>\n			<td style="width:157.5pt">\n			<p>Grup di Facebook untuk mata kuliah kelas program studi Sistem Informasi</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:34.8pt">\n			<p>4</p>\n			</td>\n			<td style="width:184.8pt">\n			<p>Mailing List Alumni Sistem Informasi</p>\n			</td>\n			<td style="width:157.5pt">\n			<p>Sebagai forum lulusan Sistem Informasi</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:34.8pt">\n			<p>5</p>\n			</td>\n			<td style="width:184.8pt">\n			<p>FRI</p>\n			</td>\n			<td style="width:157.5pt">\n			<p>Grup di Facebook untuk forum Fakultas Rekayasa Industri</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:34.8pt">\n			<p>6</p>\n			</td>\n			<td style="width:184.8pt">\n			<p>Line</p>\n			</td>\n			<td style="width:157.5pt">\n			<p>Line digunakan untuk melakukan interaksi secara online dapat berupa bimbingan online.</p>\n			</td>\n		</tr>\n	</tbody>\n</table>\n\n<p>&nbsp;</p>\n\n<p>Selain kegiatan di atas, masalah tata ruang, dimana ruang Dosen berada di laboratorium yang cukup luas dan representatif untuk melakukan konsultasi, sehingga memudahkan interaksi antara Dosen dan mahasiswa.</p>\n\n<p><strong>f. Penelitian Bersama antara Dosen dan Mahasiswa</strong></p>\n\n<p>Mahasiswa juga terlibat dalam penelitian yang sedang dilakukan oleh Dosen, yaitu penelitian dana internal atau penelitian data eksternal. Kegiatan tersebut dapat melatih kemampuan mahasiswa dalam pengerjaan penelitian baik dari sisi softskill dan hardskill serta pencarian pengalaman. Banyaknya mahasiswa Program Studi yang ikut serta dalam penelitian dosen adalah 140<strong> </strong>orang dalam kurun waktu 2013-2016.</p>\n\n<p><strong>g. Pengabdian Masyarakat antara Dosen dan Mahasiswa</strong></p>\n\n<p>Mahasiswa juga dilibatkan dalam program pengabdian masyarakat. Peran mahasiswa sangat penting, dimana mereka dilibatkan dalam proses persiapan, acara inti, dan pasca pelaksanaan. Mahasiswa membantu sebagai asisten dalam penyampaian materi yang bersifat teknis.</p>\n\n<p><strong>h. FRI acoustik</strong></p>\n\n<p>Ajang apresiasi bagi dosen dan mahasiswa dalam suasana yang informal yang diisi dengan lomba-lomba yang bersifat menghibur.</p>\n\n<p>&nbsp;</p>\n\n<p><strong>2. Interaksi akademik antara Mahasiswa-Mahasiswa</strong></p>\n\n<p>Interaksi akademik antara mahasiswa di fasilitasi oleh Program Studi dan Organisasi Kemahasiswaan. Interaksi akademik antara mahasiswa yang difasilitasi oleh Program Studi adalah :</p>\n\n<p><strong>a. Aktivitas Praktikum</strong></p>\n\n<p>Kegiatan praktikum dilakukan untuk mendukung aktivitas pengajaran yang diadakan pada matkuliah. Praktikum dilaksanakan setiap semester untuk matakuliah tertentu sehingga mahasiswa akan mendapatkan praktikum setiap semesternya kecuali semester terakhir.</p>\n\n<p>Proses praktikum dilaksanakan secara berkelompok sehingga terdapat interaksi antara mahasiswa. Interaksi antar mahasiswa dapat berupa pengerjaan tugas praktikum, aktivitas praktikum, dan presentasi.</p>\n\n<p><strong>b. Aktivitas Kelompok Belajar di Keprofesian dan Laboratorium</strong></p>\n\n<p>Interaksi antara mahasiswa juga dilaksanakan pada kelompok belajar yang dinaungi oleh keprofesian dan laboratorium. Kelompok belajar dibawah laboratorium dan keprofesian merupakan kelompok belajar yang membahas kajian di masing-masing bidang keprofesian dan laboratorium.</p>\n\n<p><strong>Tabel 5. </strong><strong>36 Kelompok Belajar di Keprofesian dan Laboratorium</strong></p>\n\n<table border="1" cellspacing="0" style="width:360.45pt">\n	<tbody>\n		<tr>\n			<td style="background-color:#a6a6a6; width:116.5pt">\n			<p>Kelompok Belajar</p>\n			</td>\n			<td style="background-color:#a6a6a6; width:113.4pt">\n			<p>Laboratorium</p>\n			</td>\n			<td style="background-color:#a6a6a6; width:130.55pt">\n			<p>Keprofesian</p>\n			</td>\n		</tr>\n		<tr>\n			<td rowspan="3" style="width:116.5pt">\n			<p><em>Enterprise Package Solution</em></p>\n			</td>\n			<td rowspan="3" style="width:113.4pt">\n			<p>Laboratorium <em>Enterprise Resource Planning</em></p>\n			</td>\n			<td style="width:130.55pt">\n			<p>Konfigurasi dan Implementasi ERP</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:130.55pt">\n			<p>Kustomisasi ERP</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:130.55pt">\n			<p>Adobsi ERP dan Manajemen Perubahan</p>\n			</td>\n		</tr>\n		<tr>\n			<td rowspan="3" style="width:116.5pt">\n			<p>BPAD <em>Laboratory Club</em> (BLC)</p>\n			</td>\n			<td rowspan="3" style="width:113.4pt">\n			<p>Laboratorium <em>Business Process Analysis and Design</em></p>\n			</td>\n			<td style="width:130.55pt">\n			<p><em>Enterprise Architecture</em></p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:130.55pt">\n			<p><em>Technopreneurship</em></p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:130.55pt">\n			<p><em>Information System Management</em></p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:116.5pt">\n			<p><em>Information System Networking</em> (IS.Net)</p>\n			</td>\n			<td rowspan="2" style="width:113.4pt">\n			<p>Laboratorium Sistem Operasi dan Jaringan Komputer</p>\n			</td>\n			<td rowspan="2" style="width:130.55pt">\n			<p><em>Enterprise Infrastructure</em> <em>Management</em></p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:116.5pt">\n			<p><em>Information Security Club</em> (INSECLUB)</p>\n			</td>\n		</tr>\n		<tr>\n			<td rowspan="3" style="width:116.5pt">\n			<p>Prodase <em>Innovation Club</em> (PIC)</p>\n			</td>\n			<td rowspan="3" style="width:113.4pt">\n			<p>Laboratorium <em>Programming</em> dan <em>Database</em></p>\n			</td>\n			<td style="width:130.55pt">\n			<p><em>Technopreneur</em></p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:130.55pt">\n			<p><em>Computer Software Development</em></p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:130.55pt">\n			<p><em>Enterprise Data Management</em></p>\n			</td>\n		</tr>\n	</tbody>\n</table>\n\n<p>&nbsp;</p>\n\n<p>Interaksi yang diwadahi tersebut diharapkan dapat menumbuhkan dan menambahkan <em>softskill</em>. Interaksi akademik antara mahasiswa yang difasilitasi oleh Organisasi Kemahasiswaan adalah :</p>\n\n<ul>\n	<li>Senat Mahasiswa</li>\n	<li>Himpunan Mahasiswa Sistem Informasi (HMSI)</li>\n	<li>Unit Kegiatan Mahasiswa (UKM), Contoh UKM Bidang Olahraga, UKM Bidang Kerohanian, UKM Bidang Kesenian dan Pendidikan/Penalaran, UKM Bidang Kegiatan Sosial, UKM Bidang Kewirausahaan dan sebagainya.</li>\n</ul>\n\n<p><strong>3. Interaksi akademik antara Dosen-Dosen</strong></p>\n\n<p>Interaksi antara dosen yang baik dapat menumbuhkan suasana akademik sehingga kualitas bahan akademik semakin lebih baik. Interaksi akademik antara dosen dapat berupa :</p>\n\n<p><strong>a. Rapat Sidang Akademik tingkat Fakultas</strong></p>\n\n<p>Rapat sidang akademik merupakan pertemuan yang dilakukan untuk mengesahkan kelulusan dari mahasiswa tingkat akhir yang telah menyelesaikan skripsi atau sidang tugas akhirnya. Rapat sidang akademik dilaksanakan sebulan sekali pada akhir bulan.</p>\n\n<p><strong>b. Rapat Dosen tingkat Program Studi dan Fakultas</strong></p>\n\n<p>Rapat dosen dilakukan secara rutin sekali dalam satu minggu. Rapat ini dilaksanakan untuk membahas masalah-masalah khusus terkait rencana progam studi dan fakultas. Selain itu, informasi-informasi tentang perubahan peraturan dan kebijakan baru di tingkat program studi juga di informasikan disini.</p>\n\n<p><strong>c. Rapat Koordinasi Pengampuan Matakuliah</strong></p>\n\n<p>Rapat koordniasi pengampuan matakuliah dialkukan secara rutin pada awal semester baru berlangsung. Rapat koordinasi pengampuan matakuliah melibatkan seluruh dosen setiap fakultas untuk membahas Satuan Acara Pengajaran, Silabus Matakuliah, dan Rencana Pengajaran Mingguan.</p>\n\n<p><strong>d. Rapat Koordinasi Perkuliahan Pararel</strong></p>\n\n<p>Rapat koordinasi matakuliah pararel setidaknya dilakukan oleh tim Dosen matakuliah sebanyak tiga kali, yaitu pada awal semester sebelum perkuliahan berlangsung, pertengahan semester untuk memastikan materi yang diberikan sudah seragam dan bersamaan untuk setiap kelas yang dipegang oleh masing-masing Dosen bersangkutan dan terakhir pada akhir semester yaitu untuk mengevaluasi keseragaman materi yang telah diberikan.</p>\n\n<p><strong>e. Interaksi antar dosen pada Keprofesian</strong></p>\n\n<p>Keprofesian yang telah berdiri akan dikelola oleh dosen-dosen. Tugas dosen dosen di dalam keprofesian adalah mengembangkan keprofesian baik secara penelitian ataupun hubungan dengan dunia industri / eksternal. Pada proses pengembangannya dosen-dosen ini akan saling berinteraksi baik dalam forum dan rapat.</p>\n');

-- --------------------------------------------------------

--
-- Table structure for table `std5_575pengembangan`
--

CREATE TABLE `std5_575pengembangan` (
  `no` int(25) UNSIGNED NOT NULL,
  `pengembangan_perilaku_kecendekiawanan` varchar(65000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `std5_575pengembangan`
--

INSERT INTO `std5_575pengembangan` (`no`, `pengembangan_perilaku_kecendekiawanan`) VALUES
(1, '<p>Secara umum, dampak perilaku kecendekiawanan ini membuat seluruh sivitas akademik Program Studi Sarjana Sistem Informasi untuk berpegang pada kejujuran akademik dalam setiap kehidupan kampus (hal ini tertuang dalam Buku Pedoman Akademik Universitas Telkom). Selain itu, perilaku kecendekiawanan juga meningkatkan motivasi Dosen untuk selalu berinovasi sesuai dengan bidang keiilmuannya. Kegiatan sosial yang dilakukan :</p>\n\n<p><strong>Tabel </strong><strong>5.20 Perilaku Kecendikiawanan</strong></p>\n\n<table border="1" cellspacing="0" style="width:386.7pt">\n	<tbody>\n		<tr>\n			<td colspan="3" style="background-color:#bfbfbf; width:386.7pt">\n			<p>Kegiatan penanggulangan kemiskinan</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:25.5pt">\n			<p>No</p>\n			</td>\n			<td style="width:314.45pt">\n			<p>Judul</p>\n			</td>\n			<td style="width:46.75pt">\n			<p>Tahun</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:25.5pt">\n			<p>1</p>\n			</td>\n			<td style="background-color:white; width:314.45pt">\n			<p>Kunjungan Panti Asuhan</p>\n			</td>\n			<td style="width:46.75pt">\n			<p>2014</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:25.5pt">\n			<p>2</p>\n			</td>\n			<td style="background-color:white; width:314.45pt">\n			<p>Tagana (Tanggap Bencana Dan Logistik)</p>\n			</td>\n			<td style="width:46.75pt">\n			<p>2014</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:25.5pt">\n			<p>3</p>\n			</td>\n			<td style="background-color:white; width:314.45pt">\n			<p>Kunjungan Panti Asuhan</p>\n			</td>\n			<td style="width:46.75pt">\n			<p>2015</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:25.5pt">\n			<p>4</p>\n			</td>\n			<td style="background-color:white; width:314.45pt">\n			<p>Banjir Dayeuh Kolot</p>\n			</td>\n			<td style="width:46.75pt">\n			<p>2016</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:25.5pt">\n			<p>5</p>\n			</td>\n			<td style="background-color:white; width:314.45pt">\n			<p>Banjir dan Tanah Longsor Garut</p>\n			</td>\n			<td style="width:46.75pt">\n			<p>2016</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:25.5pt">\n			<p>6</p>\n			</td>\n			<td style="background-color:white; width:314.45pt">\n			<p>Banjir Baleendah</p>\n			</td>\n			<td style="width:46.75pt">\n			<p>2016</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:25.5pt">\n			<p>7</p>\n			</td>\n			<td style="background-color:white; width:314.45pt">\n			<p>Donor Darah dan Sehat Bersama HMSI (Donasi)</p>\n			</td>\n			<td style="width:46.75pt">\n			<p>2016</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:25.5pt">\n			<p>8</p>\n			</td>\n			<td style="background-color:white; width:314.45pt">\n			<p>Hapus (HMSI Peduli Kasih)</p>\n			</td>\n			<td style="width:46.75pt">\n			<p>2016</p>\n			</td>\n		</tr>\n		<tr>\n			<td colspan="3" style="background-color:#bfbfbf; width:386.7pt">\n			<p>Pelestarian Lingkungan</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:25.5pt">\n			<p>No</p>\n			</td>\n			<td style="width:314.45pt">\n			<p>Judul</p>\n			</td>\n			<td style="width:46.75pt">\n			<p>Tahun</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:25.5pt">\n			<p>1</p>\n			</td>\n			<td style="background-color:white; width:314.45pt">\n			<p>Pelatihan &amp; Pemanfaatan Aplikasi Dalam Aktivitas Logistik Ritel Untuk Dinas Perumahan, Tata Kelola &amp; Kebersihan (Dispertasih) Kabupaten Bandung</p>\n			</td>\n			<td style="width:46.75pt">\n			<p>2014</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:25.5pt">\n			<p>2</p>\n			</td>\n			<td style="background-color:white; width:314.45pt">\n			<p>Pelatihan Penggunaan Aplikasi Transportation Management System (TMS) untuk Aktivitas Operational Pendistribusian Pengangkutan Sampah Bidang Kebersihan Dispertasih Kabupaten Bandung</p>\n			</td>\n			<td style="width:46.75pt">\n			<p>2016</p>\n			</td>\n		</tr>\n		<tr>\n			<td colspan="3" style="background-color:#bfbfbf; width:386.7pt">\n			<p>Peningkatan Kesejahteraan Masyarakat</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:25.5pt">\n			<p>No</p>\n			</td>\n			<td style="width:314.45pt">\n			<p>Judul</p>\n			</td>\n			<td style="width:46.75pt">\n			<p>Tahun</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:25.5pt">\n			<p>1</p>\n			</td>\n			<td style="background-color:white; width:314.45pt">\n			<p>Pendampingan Pengembangan Sistem Informasi Pengelolaan Magang di Lingkungan Pemerintah Kota Bandung</p>\n			</td>\n			<td style="width:46.75pt">\n			<p>2014</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:25.5pt">\n			<p>2</p>\n			</td>\n			<td style="background-color:white; width:314.45pt">\n			<p>Pendampingan Penyusunan Master Plan Dashboard E-Government Kabupaten Cirebon</p>\n			</td>\n			<td style="width:46.75pt">\n			<p>2014</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:25.5pt">\n			<p>3</p>\n			</td>\n			<td style="background-color:white; width:314.45pt">\n			<p>Sekolah Pendampingan SMKN 1 Lemahsugih Pelatihan Dasar Jaringan dan Dasar Pemrograman Untuk Meningkatkan Kemampuan Siswa</p>\n			</td>\n			<td style="width:46.75pt">\n			<p>2014</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:25.5pt">\n			<p>4</p>\n			</td>\n			<td style="background-color:white; width:314.45pt">\n			<p>Pengabdian Masyarakat Berbasis ICT (<em>Information Communication And Technology</em>)</p>\n			</td>\n			<td style="width:46.75pt">\n			<p>2015</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:25.5pt">\n			<p>5</p>\n			</td>\n			<td style="background-color:white; width:314.45pt">\n			<p>IBM Sekolah Binaan SMAN 3 Kuningan &amp; SMPN 5 Lemahsugih (Optimalisasi Penggunaan Tik Untuk Mendukung Proses Belajar Mengajar)</p>\n			</td>\n			<td style="width:46.75pt">\n			<p>2015</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:25.5pt">\n			<p>6</p>\n			</td>\n			<td style="background-color:white; width:314.45pt">\n			<p>Pelatihan Customer Relationship Management (CRM) dan Pemanfaatan Aplikasi Opensource Untuk Anggota Kadin Kota Bandung</p>\n			</td>\n			<td style="width:46.75pt">\n			<p>2015</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:25.5pt">\n			<p>7</p>\n			</td>\n			<td style="background-color:white; width:314.45pt">\n			<p>Pelatihan E-Learning System Di Pesantren Darussalam Garut</p>\n			</td>\n			<td style="width:46.75pt">\n			<p>2015</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:25.5pt">\n			<p>8</p>\n			</td>\n			<td style="background-color:white; width:314.45pt">\n			<p>Pelatihan IT Literasi Bagi Perangkat Desa dan Karang Taruna untuk Meningkatkan Kualitas Pengelolaan Administrasi dan Manajemen Kantor Desa, Desa Mangunjaya, Agrabinta, Kabupaten Cianjur, Jabar Selatan</p>\n			</td>\n			<td style="width:46.75pt">\n			<p>2015</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:25.5pt">\n			<p>9</p>\n			</td>\n			<td style="background-color:white; width:314.45pt">\n			<p>Pelatihan IT Literasi Bagi Perangkat Desa Untuk Meningkatkan Kualitas Pengelolaan Management Sdn Sinar Mulya, Ds. Mangun Jaya, Agra Binta, Kab. Cianjur</p>\n			</td>\n			<td style="width:46.75pt">\n			<p>2015</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:25.5pt">\n			<p>10</p>\n			</td>\n			<td style="background-color:white; width:314.45pt">\n			<p>Pelatihan IT Literasi Bagi Perangkat Desa Untuk Meningkatkan Kualitas Pengelolaan Management SMPN 2 Agrabinta, Desa Mangun Jaya, Agra Binta, Kabupaten Cianjur</p>\n			</td>\n			<td style="width:46.75pt">\n			<p>2015</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:25.5pt">\n			<p>11</p>\n			</td>\n			<td style="background-color:white; width:314.45pt">\n			<p>Pelatihan Komputer Berbasis Kompetensi Di Balai Latihan Tenaga Kerja Luar Negeri (BLTKLN)</p>\n			</td>\n			<td style="width:46.75pt">\n			<p>2015</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:25.5pt">\n			<p>12</p>\n			</td>\n			<td style="background-color:white; width:314.45pt">\n			<p>Pelatihan Pembuatan Rencana Induk Teknologi Informasi dan Pemanfaatan Aplikasi Microsoft Project untuk Staff Badan Kepegawaian Daerah Kota Bandung</p>\n			</td>\n			<td style="width:46.75pt">\n			<p>2015</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:25.5pt">\n			<p>13</p>\n			</td>\n			<td style="background-color:white; width:314.45pt">\n			<p>Pelatihan Wawasan Terhadap Perlindungan Hak Kekayaan Intelektual Untuk Guru-Guru SMK Se-Jawa Barat</p>\n			</td>\n			<td style="width:46.75pt">\n			<p>2015</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:25.5pt">\n			<p>14</p>\n			</td>\n			<td style="background-color:white; width:314.45pt">\n			<p>Pelatihan Teknologi Informasi Bagi Siswa Sekolah Dasar di Desa Pamalayan Kecamatan Cisewu Kabupaten Garut</p>\n			</td>\n			<td style="width:46.75pt">\n			<p>2015</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:25.5pt">\n			<p>15</p>\n			</td>\n			<td style="background-color:white; width:314.45pt">\n			<p>Perpustakaan Digital Sebagai Upaya Meningkatkan Minat Baca Siswa Sejak Dini di Sekolah Dasar Negeri Sukadana II</p>\n			</td>\n			<td style="width:46.75pt">\n			<p>2015</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:25.5pt">\n			<p>16</p>\n			</td>\n			<td style="background-color:white; width:314.45pt">\n			<p>E-Rapor (Sistem Aplikasi Rapor) Untuk Membantu Para Guru Dalam Pengisian Laporan Akademik Siswa di Sekolah Dasar Negeri Sukadana II</p>\n			</td>\n			<td style="width:46.75pt">\n			<p>2015</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:25.5pt">\n			<p>17</p>\n			</td>\n			<td style="background-color:white; width:314.45pt">\n			<p>Pelatihan Dan Sosialisasi Programming Code Untuk Mengembangkan Soft Skill Anak di SD-Baiturrahman Tasikmalaya</p>\n			</td>\n			<td style="width:46.75pt">\n			<p>2015</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:25.5pt">\n			<p>18</p>\n			</td>\n			<td style="background-color:white; width:314.45pt">\n			<p>Pelatihan Dasar Mikrotik dan Sertifikasi Internasional Untuk Guru SMK/SMU di Bandung</p>\n			</td>\n			<td style="width:46.75pt">\n			<p>2015</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:25.5pt">\n			<p>19</p>\n			</td>\n			<td style="background-color:white; width:314.45pt">\n			<p>Pelatihan IT Literasi Bagi Guru Untuk Meningkatkan Kualitas Manajemen SDN Cibungur 1, Desa Mangun Jaya, Agrabinta, Kabupaten Cianjur</p>\n			</td>\n			<td style="width:46.75pt">\n			<p>2015</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:25.5pt">\n			<p>20</p>\n			</td>\n			<td style="background-color:white; width:314.45pt">\n			<p>Pelatihan Pembuatan Website dan Jurnalisme Warga di Desa Banjaran Kab. Bandung</p>\n			</td>\n			<td style="width:46.75pt">\n			<p>2015</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:25.5pt">\n			<p>21</p>\n			</td>\n			<td style="background-color:white; width:314.45pt">\n			<p>Pengembangan dan Implementasi Sistem Manajemen Mutu Terpadu Berbasis ISO 9001 2008 Pada SMAN 14 Bandung</p>\n			</td>\n			<td style="width:46.75pt">\n			<p>2016</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:25.5pt">\n			<p>22</p>\n			</td>\n			<td style="background-color:white; width:314.45pt">\n			<p>Pelatihan Penggunaan Microsoft Power Point Untuk Pembuatan Materi E-Learning Di Pesantren Darussalam Garut</p>\n			</td>\n			<td style="width:46.75pt">\n			<p>2016</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:25.5pt">\n			<p>23</p>\n			</td>\n			<td style="background-color:white; width:314.45pt">\n			<p>Pemasangan Data Logger Pada PLTS dan PLTMH Di Kampung Buligir, Desa Parentas, Cigalontang, Kab. Tasikmalaya</p>\n			</td>\n			<td style="width:46.75pt">\n			<p>2016</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:25.5pt">\n			<p>24</p>\n			</td>\n			<td style="background-color:white; width:314.45pt">\n			<p>Pendampingan Program Desa Mandiri dengan IT dalam Mewujudkan Desa Peradaban di Jawa Barat</p>\n			</td>\n			<td style="width:46.75pt">\n			<p>2016</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:25.5pt">\n			<p>25</p>\n			</td>\n			<td style="background-color:white; width:314.45pt">\n			<p>Pengembangan Softskill &amp; Hard.Skill Siswa SMKN 1 Lemahsugih dan SMKN 1 Malausma Guna Meningkatkan Kornpetensi Lulusan dan Kesiapan Kerja Lulusan dalam Menghadapi Dunia Kerja</p>\n			</td>\n			<td style="width:46.75pt">\n			<p>2016</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:25.5pt">\n			<p>26</p>\n			</td>\n			<td style="background-color:white; width:314.45pt">\n			<p>Peningkatan Pemanfaatan Perpustakaan Digital di Sekolah Dasar Negeri Sukadana II</p>\n			</td>\n			<td style="width:46.75pt">\n			<p>2016</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:25.5pt">\n			<p>27</p>\n			</td>\n			<td style="background-color:white; width:314.45pt">\n			<p>Smart School - Peningkatan Knowledge Sharing Antara Sekolah Bandung Raya (Kota Bandung, Kabupaten Bandung, Kbb, Cimahi) Dengan Pemberdayaan Komunitas ICT di Sekolah</p>\n			</td>\n			<td style="width:46.75pt">\n			<p>2016</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:25.5pt">\n			<p>28</p>\n			</td>\n			<td style="background-color:white; width:314.45pt">\n			<p>Workshop Peranan SDM Sebagai Kunci Keberhasilan Implementasi E-Government di Kabupaten Bandung untuk Menuju Tata Kelola Pemerintah yang Lebih Baik</p>\n			</td>\n			<td style="width:46.75pt">\n			<p>2016</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:25.5pt">\n			<p>29</p>\n			</td>\n			<td style="background-color:white; width:314.45pt">\n			<p>Programming For Kids</p>\n			</td>\n			<td style="width:46.75pt">\n			<p>2016</p>\n			</td>\n		</tr>\n		<tr>\n			<td colspan="3" style="background-color:#bfbfbf; width:386.7pt">\n			<p>Kegiatan Penanggulangan masalah ekonomi, politik, sosial, budaya dan lingkungan lainnya</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:25.5pt">\n			<p>No</p>\n			</td>\n			<td style="width:314.45pt">\n			<p>Judul</p>\n			</td>\n			<td style="width:46.75pt">\n			<p>Tahun</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:25.5pt">\n			<p>1</p>\n			</td>\n			<td style="width:314.45pt">\n			<p>Pembuatan Sistem Informasi Manajemen Koperasi Simpan Pinjam Tatali Wargi Desa Padasuka Kecamatan Cimahi Tengah</p>\n			</td>\n			<td style="width:46.75pt">\n			<p>2014</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:25.5pt">\n			<p>2</p>\n			</td>\n			<td style="background-color:white; width:314.45pt">\n			<p>Pendampingan Pengembangan Desain dan Kemasan Produk Olahan Horti</p>\n			</td>\n			<td style="width:46.75pt">\n			<p>2014</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:25.5pt">\n			<p>3</p>\n			</td>\n			<td style="background-color:white; width:314.45pt">\n			<p>Pendampingan Penguatan Desain dan Kemasan Produk Olahan Horti, Dinas Pertanian Perkebunan &amp; Kehutanan</p>\n			</td>\n			<td style="width:46.75pt">\n			<p>2014</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:25.5pt">\n			<p>4</p>\n			</td>\n			<td style="background-color:white; width:314.45pt">\n			<p>Pendampingan Penguatan Usaha dan Penguatan Kelembagaan Dinas Pertanian Perkebunan &amp; Kehutanan</p>\n			</td>\n			<td style="width:46.75pt">\n			<p>2014</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:25.5pt">\n			<p>5</p>\n			</td>\n			<td style="background-color:white; width:314.45pt">\n			<p>Training Sosial Learning Dengan Menggunakan Edmodo Untuk Guru SMA di Kota Bandung</p>\n			</td>\n			<td style="width:46.75pt">\n			<p>2014</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:25.5pt">\n			<p>6</p>\n			</td>\n			<td style="background-color:white; width:314.45pt">\n			<p>Kegiatan Pendampingan Sistem Keuangan Aparatur Pemerintah Desa Sukapura</p>\n			</td>\n			<td style="width:46.75pt">\n			<p>2015</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:25.5pt">\n			<p>7</p>\n			</td>\n			<td style="background-color:white; width:314.45pt">\n			<p>Pelatihan Peningkatan Kemampuan Pengelolaan Logistik Ritel Untuk Usaha Kecil Dan Mengenah (UKM) Se-Kabupaten Tasikmalaya</p>\n			</td>\n			<td style="width:46.75pt">\n			<p>2015</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:25.5pt">\n			<p>8</p>\n			</td>\n			<td style="background-color:white; width:314.45pt">\n			<p>Pemetaan Usaha Mikro Kecil dan Menengah (UMKM) di Desa Dayeuh Kolot dan Sukapura</p>\n			</td>\n			<td style="width:46.75pt">\n			<p>2015</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:25.5pt">\n			<p>9</p>\n			</td>\n			<td style="background-color:white; width:314.45pt">\n			<p>Sosialisasi Proses Bisnis Pemenuhan Sertifikasi CPPB-IRT dan Halal Pada Usaha Kecil Menengah Pengolah Makanan</p>\n			</td>\n			<td style="width:46.75pt">\n			<p>2015</p>\n			</td>\n		</tr>\n		<tr>\n			<td style="width:25.5pt">\n			<p>10</p>\n			</td>\n			<td style="background-color:white; width:314.45pt">\n			<p>Workshop Komunikasi Pemasaran Menggunakan Social Media Bagi Pelaku UMKM di Desa Dayeuhkolot dan Sukapura Kabupaten Bandung</p>\n			</td>\n			<td style="width:46.75pt">\n			<p>2015</p>\n			</td>\n		</tr>\n	</tbody>\n</table>\n');

-- --------------------------------------------------------

--
-- Table structure for table `std5_5121jumlahsksps`
--

CREATE TABLE `std5_5121jumlahsksps` (
  `no` int(100) NOT NULL,
  `jenis_mata_kuliah` varchar(75) NOT NULL,
  `sks` varchar(100) NOT NULL,
  `keterangan` varchar(20000) NOT NULL,
  `excel_path` varchar(255) NOT NULL,
  `excelhtml_path` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `std5_5121jumlahsksps`
--

INSERT INTO `std5_5121jumlahsksps` (`no`, `jenis_mata_kuliah`, `sks`, `keterangan`, `excel_path`, `excelhtml_path`) VALUES
(1, 'Mata Kuliah Wajib', '133', 'Terdiri atas 46 mata kuliah yang dimulai dari semester 1 sampai semester 8 dan dibagi menjadi 4 jenis mata kuliah, yaitu : MK. Dasar Umum, Matematika & Statistik = 37 sks, Mata Kuliah Dasar Sistem Informasi =  76 sks, Mata Kuliah Strategi & Manajemen =  18 sks, Mata Kuliah Non-Sistem Informasi =  2 sks', '', ''),
(2, 'Mata Kuliah Pilihan', '12', 'Mata kuliah pilihan dikelompokkan ke dalam 5 (lima) kelompok peminatan yang dikelola oleh Kelompok Keahlian. Mata kuliah pilihan dengan bobot 12 sks wajib diambil dengan 4 mata kuliah pilihan. ', '', ''),
(3, 'Jumlah Total ', '145', '', '', ''),
(4, 'Mata Kuliah Khusus ', '4', 'Mata Kuliah Kerja Praktek (2 sks) dan mata kuliah geladi (2 sks) merupakan mata kuliah yang tidak diselenggarakan di dalam ruang kelas melainkan dilaksanakan di tempat geladi dan kerja praktek. ', '', ''),
(5, 'Mata Kuliah Co-op (optional)', '4', 'Dapat diambil pada semester 8. Program Co-op adalah kegiatan pendidikan bagi mahasiswa berupa bekerja di suatu perusahaan, industri, atau UKM selama 3 – 6 bulan (sesuai dengan kesepakatan antara perguruan tinggi dengan perusahaan, industri, UKM). Dalam program ini mahasiswa bekerja secara penuh (full time) dalam rangka kegiatan sebuah perusahaan, industri, atau UKM yang dapat berupa kegiatan dalam jalur produksi, manajemen, ataupun proyek khusus.', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `std5_5122strukturkurikulummk`
--

CREATE TABLE `std5_5122strukturkurikulummk` (
  `no` int(100) NOT NULL,
  `smt` varchar(20) NOT NULL,
  `kode_mk` varchar(20) NOT NULL,
  `nama_mata_kuliah` varchar(121) NOT NULL,
  `bobot_sks` int(10) NOT NULL,
  `sks_mk_dalam_kurikulum_inti` int(10) NOT NULL,
  `sks_mk_dalam_kurikulum_Institusional` int(10) NOT NULL,
  `bobot_tugas` varchar(20) NOT NULL,
  `kelengkapan_deskripsi` varchar(20) NOT NULL,
  `kelengkapan_silabus` varchar(20) NOT NULL,
  `kelengkapan_sap` varchar(20) NOT NULL,
  `unit_jur_fak_penyelenggara` varchar(50) NOT NULL,
  `excel_path` varchar(255) NOT NULL,
  `excelhtml_path` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `std5_5122strukturkurikulummk`
--

INSERT INTO `std5_5122strukturkurikulummk` (`no`, `smt`, `kode_mk`, `nama_mata_kuliah`, `bobot_sks`, `sks_mk_dalam_kurikulum_inti`, `sks_mk_dalam_kurikulum_Institusional`, `bobot_tugas`, `kelengkapan_deskripsi`, `kelengkapan_silabus`, `kelengkapan_sap`, `unit_jur_fak_penyelenggara`, `excel_path`, `excelhtml_path`) VALUES
(1, 'I', 'MUG1A4', 'Kalkulus I', 4, 0, 4, 'Ada', 'Ada', 'Ada', 'Ada', 'Prodi SI', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda'),
(2, 'I', 'FUG1A3', 'Fisika I', 3, 0, 3, 'Ada', 'Ada', 'Ada', 'Ada', 'Prodi SI', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda'),
(3, 'I', 'FUG1B1', 'Praktikum Fisika I', 1, 0, 1, 'Ada', 'Ada', 'Ada', 'Ada', 'Prodi SI', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda'),
(4, 'I', 'HUG1Q3', 'Pengantar Sistem Informasi', 3, 3, 0, 'Ada', 'Ada', 'Ada', 'Ada', 'Prodi SI', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda'),
(5, 'I', 'HUG1X2', 'Pendidikan Agama dan Etika', 2, 0, 2, 'Ada', 'Ada', 'Ada', 'Ada', 'Prodi SI', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda'),
(6, 'I', 'MUG1E3', 'Aljabar Linier', 3, 0, 3, 'Ada', 'Ada', 'Ada', 'Ada', 'PPDU', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda'),
(7, 'I', 'BUG1A2', 'Bahasa Indonesia', 2, 0, 2, 'Ada', 'Ada', 'Ada', 'Ada', 'PPDU', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda'),
(8, 'II', 'MUG1B4', 'Kalkulus II', 4, 0, 4, 'Ada', 'Ada', 'Ada', 'Ada', 'PPDU', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda'),
(9, 'II', 'FUG1C3', 'Fisika II', 3, 0, 3, 'Ada', 'Ada', 'Ada', 'Ada', 'PPDU', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda'),
(10, 'II', 'FUG1D1', 'Praktikum Fisika II', 1, 0, 1, 'Ada', 'Ada', 'Ada', 'Ada', 'PPDU', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda'),
(11, 'II', 'HUG1I2', 'Konsep Pengembangan Sains & Teknologi', 2, 0, 2, 'Ada', 'Ada', 'Ada', 'Ada', 'PPDU', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda'),
(12, 'II', 'KUG1A3', 'Algoritma dan Pemrograman', 3, 3, 0, 'Ada', 'Ada', 'Ada', 'Ada', 'Prodi SI', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda'),
(13, 'II', 'KUG1B1', 'Praktikum Algoritma dan Pemrograman', 1, 1, 0, 'Ada', 'Ada', 'Ada', 'Ada', 'Prodi SI', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda'),
(14, 'II', 'HUG1H2', 'Pengetahuan Lingkungan', 2, 0, 2, 'Ada', 'Ada', 'Ada', 'Ada', 'PPDU', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda'),
(15, 'II', 'BUG1D2', 'Bahasa Inggris I (Reading & Speaking)', 2, 0, 2, 'Ada', 'Ada', 'Ada', 'Ada', 'PPDU', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda'),
(16, 'III', 'MUG2D3', 'Probabilitas dan Statistik', 3, 3, 0, 'Ada', 'Ada', 'Ada', 'Ada', 'Prodi SI', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda'),
(17, 'III', 'MUG2A3', 'Matematika Diskret', 3, 3, 0, 'Ada', 'Ada', 'Ada', 'Ada', 'Prodi SI', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda'),
(18, 'III', 'ISG2A3', 'Perilaku Organisasi', 3, 3, 0, 'Ada', 'Ada', 'Ada', 'Ada', 'Prodi SI', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda'),
(19, 'III', 'ISG2B3', 'Sistem Operasi *', 3, 3, 0, 'Ada', 'Ada', 'Ada', 'Ada', 'Prodi SI', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda'),
(20, 'III', 'ISG2C4', 'Struktur Data dan Pemrograman Lanjut *', 4, 4, 0, 'Ada', 'Ada', 'Ada', 'Ada', 'Prodi SI', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda'),
(21, 'III', 'ISG2D4', 'Network & Telecommunication Fundamental*', 4, 4, 0, 'Ada', 'Ada', 'Ada', 'Ada', 'Prodi SI', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda'),
(22, 'IV', 'IEG2E3', 'Statistika Industri', 3, 3, 0, 'Ada', 'Ada', 'Ada', 'Ada', 'Prodi SI', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda'),
(23, 'IV', 'ISG2E4', 'Object oriented programming *', 4, 4, 0, 'Ada', 'Ada', 'Ada', 'Ada', 'Prodi SI', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda'),
(24, 'IV', 'BUG1E2', 'Bahasa Inggris II (Writing & Speaking)', 2, 0, 2, 'Ada', 'Ada', 'Ada', 'Ada', 'PPDU', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda'),
(25, 'IV', 'ISG2F3', 'Manajemen Proses Bisnis *', 3, 3, 0, 'Ada', 'Ada', 'Ada', 'Ada', 'Prodi SI', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda'),
(26, 'IV', 'ISG2G4', 'Jaringan Komputer *', 4, 4, 0, 'Ada', 'Ada', 'Ada', 'Ada', 'Prodi SI', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda'),
(27, 'IV', 'ISG2H4', 'Sistem Basis Data *', 4, 4, 0, 'Ada', 'Ada', 'Ada', 'Ada', 'Prodi SI', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda'),
(28, 'V', 'ISG3A4', 'Analisis & Perancangan SI *', 4, 4, 0, 'Ada', 'Ada', 'Ada', 'Ada', 'Prodi SI', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda'),
(29, 'V', 'ISG3B3', 'Enterprise Architecture *', 3, 3, 0, 'Ada', 'Ada', 'Ada', 'Ada', 'Prodi SI', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda'),
(30, 'V', 'ISG3C3', 'Web Application Development *', 3, 3, 0, 'Ada', 'Ada', 'Ada', 'Ada', 'Prodi SI', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda'),
(31, 'V', 'ISG3D3', 'E-Business', 3, 3, 0, 'Ada', 'Ada', 'Ada', 'Ada', 'Prodi SI', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda'),
(32, 'V', 'ISG3E3', 'Sistem Manajemen Logistik & Produksi', 3, 3, 0, 'Ada', 'Ada', 'Ada', 'Ada', 'Prodi SI', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda'),
(33, 'V', 'IEG2J3', 'Sistem Manajemen Pemasaran', 3, 3, 0, 'Ada', 'Ada', 'Ada', 'Ada', 'Prodi SI', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda'),
(34, 'VI', 'IEG3C2', 'Sistem Manajemen Sumber Daya Manusia', 3, 3, 0, 'Ada', 'Ada', 'Ada', 'Ada', 'Prodi SI', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda'),
(35, 'VI', 'ISG3H3', 'Enterprise Application Integration', 3, 3, 0, 'Ada', 'Ada', 'Ada', 'Ada', 'Prodi SI', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda'),
(36, 'VI', 'ISG3I4', 'Keamanan Sistem Informasi *', 4, 4, 0, 'Ada', 'Ada', 'Ada', 'Ada', 'Prodi SI', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda'),
(37, 'VI', 'ISG3J3', 'Testing dan Implementasi', 3, 3, 0, 'Ada', 'Ada', 'Ada', 'Ada', 'Prodi SI', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda'),
(38, 'VI', 'ISG3K3', 'Sistem Akuntansi & Manajemen Keuangan', 3, 3, 0, 'Ada', 'Ada', 'Ada', 'Ada', 'Prodi SI', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda'),
(39, 'VI', 'ISG3L3', 'Tata Kelola dan Manajemen TI', 3, 3, 0, 'Ada', 'Ada', 'Ada', 'Ada', 'Prodi SI', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda'),
(40, 'VII', 'ISG4A3', 'Tata Tulis & Komunikasi Ilmiah', 3, 3, 0, 'Ada', 'Ada', 'Ada', 'Ada', 'Prodi SI', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda'),
(41, 'VII', 'ISG4B3', 'Manajemen Proyek SI', 3, 3, 0, 'Ada', 'Ada', 'Ada', 'Ada', 'Prodi SI', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda'),
(42, 'VII', 'HUG1G2', 'Pancasila & Kewarganegaraan', 2, 0, 2, 'Ada', 'Ada', 'Ada', 'Ada', 'PPDU', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda'),
(43, 'VII', 'IEG423', 'Inovasi & Kewirausahaan', 3, 3, 0, 'Ada', 'Ada', 'Ada', 'Ada', 'Prodi SI', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda'),
(44, 'VII', 'HUG2A2', 'Geladi', 2, 2, 0, 'Ada', 'Ada', 'Ada', 'Ada', 'Prodi SI', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda'),
(45, 'VII', 'ISG4x3', 'Pilihan I', 3, 3, 0, 'Ada', 'Ada', 'Ada', 'Ada', 'Prodi SI', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda'),
(46, 'VII', 'ISG4x3', 'Pilihan II', 3, 3, 0, 'Ada', 'Ada', 'Ada', 'Ada', 'Prodi SI', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda'),
(47, 'VIII', 'ISG4C4', 'Tugas Akhir', 4, 4, 0, 'Ada', 'Ada', 'Ada', 'Ada', 'Prodi SI', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda'),
(48, 'VIII', 'ISG4W2', 'Kerja Praktek', 2, 2, 0, 'Ada', 'Ada', 'Ada', 'Ada', 'Prodi SI', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda'),
(49, 'VIII', 'ISG4x3', 'Pilihan III', 3, 3, 0, 'Ada', 'Ada', 'Ada', 'Ada', 'Prodi SI', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda'),
(50, 'VIII', 'ISG4x3', 'Pilihan IV', 3, 3, 0, 'Ada', 'Ada', 'Ada', 'Ada', 'Prodi SI', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda'),
(51, 'Total SKS', '', '', 145, 112, 33, '50', '50', '50', '50', '', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda', 'uploads/dokumenpendukungstd5/std5_5122strukturkurikulummk/datastandar5-5.1.2.2strukturkurikulumberda');

-- --------------------------------------------------------

--
-- Table structure for table `std5_aspekpenilaian`
--

CREATE TABLE `std5_aspekpenilaian` (
  `id` int(11) NOT NULL,
  `aspek_penilaian` varchar(1000) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `std5_aspekpenilaian`
--

INSERT INTO `std5_aspekpenilaian` (`id`, `aspek_penilaian`) VALUES
(50, 'Struktur kurikulum (harus memuat standar kompetensi lulusan yang terstruktur dalam kompetensi utama, pendukung dan lainnya).'),
(51, 'Orientasi dan kesesuaian kurikulum dengan visi dan misi PS.'),
(52, 'Kesesuaian mata kuliah dengan standar kompetensi.'),
(53, 'Persentase mata kuliah  yang dalam penentuan nilai akhirnya memberikan bobot pada tugas-tugas (PR atau makalah) ≥ 20%. Persentase mata kuliah  yang dalam penentuan nilai akhirnya memberikan bobot pada tugas-tugas (PR atau makalah) ≥ 20%. '),
(54, 'Matakuliah dilengkapi dengan deskripsi matakuliah, silabus dan SAP.'),
(55, 'Fleksibilitas mata kuliah pilihan.'),
(56, 'Substansi praktikum dan pelaksanaan praktikum.'),
(57, 'Pelaksanaan peninjauan kurikulum selama 5 tahun terakhir.'),
(58, 'Penyesuaian kurikulum dengan perkembangan Ipteks dan kebutuhan.'),
(59, 'Pelaksanaan pembelajaran memiliki mekanisme untuk memonitor, mengkaji, dan memperbaiki secara periodik kegiatan perkuliahan (kehadiran dosen dan mahasiswa), penyusunan materi perkuliahan, serta penilaian hasil belajar.'),
(60, 'Mekanisme penyusunan materi perkuliahan.'),
(61, 'Mutu soal ujian.'),
(62, 'Rata-rata banyaknya mahasiswa per dosen Pembimbing Akademik per semester.'),
(63, 'Pelaksanaan kegiatan pembimbingan akademik.'),
(64, 'Jumlah rata-rata pertemuan pembimbingan akademik per mahasiswa per semester.'),
(65, 'Efektivitas kegiatan pembimbingan akademik.'),
(66, 'Ketersediaan panduan, sosialisasi, dan penggunaan.'),
(67, 'Rata-rata mahasiswa per dosen pembimbing tugas akhir. '),
(68, 'Rata-rata jumlah pertemuan/pembimbingan selama penyelesaian TA.'),
(69, 'Kualifikasi akademik dosen pembimbing tugas akhir.'),
(70, 'Rata-rata waktu penyelesaian penulisan tugas akhir.'),
(71, 'Upaya perbaikan sistem pembelajaran yang telah dilakukan selama tiga tahun terakhir. '),
(72, 'Kebijakan tentang suasana akademik (otonomi keilmuan, kebebasan akademik, kebebasan mimbar akademik).'),
(73, 'Ketersediaan dan jenis prasarana, sarana dan dana yang memungkinkan terciptanya interaksi akademik antara sivitas akademika.'),
(74, 'Program dan kegiatan akademik untuk menciptakan suasana akademik (seminar, simposium, lokakarya, bedah buku, penelitian bersama dll).'),
(75, 'Interaksi akademik antara dosen-mahasiswa.'),
(76, 'Pengembangan perilaku kecendekiawanan.');

-- --------------------------------------------------------

--
-- Table structure for table `std5_filependukung`
--

CREATE TABLE `std5_filependukung` (
  `id` int(100) NOT NULL,
  `nama_file` varchar(255) NOT NULL,
  `poindata` varchar(100) NOT NULL,
  `uploaddate` int(100) UNSIGNED NOT NULL,
  `id_user` int(20) NOT NULL,
  `filepath` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `std5_filependukung`
--

INSERT INTO `std5_filependukung` (`id`, `nama_file`, `poindata`, `uploaddate`, `id_user`, `filepath`) VALUES
(0, '[1501300348][1495771253]tes.xlsx', 'std5_5111', 1501300348, 1, 'uploads/dokumenpendukungstd5/std5_5111/[1501300348][1495771253]tes.xlsx'),
(1, 'borangStandar5prodinew.docx', 'std5_5111', 1495652679, 2, 'uploads/dokumenpendukungstd5/std5_5111/borangStandar5prodinew.docx'),
(2, 'datastandar5-5.1.2.1jumlahsksprogramstudi.xlsx', 'std5_5111', 1495652680, 2, 'uploads/dokumenpendukungstd5/std5_5111/datastandar5-5.1.2.1jumlahsksprogramstudi.xlsx'),
(3, 'borangStandar5prodinew.docx', 'std5_5111', 1495652879, 2, 'uploads/dokumenpendukungstd5/std5_5111/borangStandar5prodinew.docx'),
(4, '[1495653006]borangStandar5prodinew.docx', 'std5_5111', 1495653006, 2, 'uploads/dokumenpendukungstd5/std5_5111/[1495653006]borangStandar5prodinew.docx'),
(5, '[1495653076]DaftarDosenWalidanMahasiswayangdiwalikan.xlsx.xlsx', 'std5_5111', 1495653076, 2, 'uploads/dokumenpendukungstd5/std5_5111/[1495653076]DaftarDosenWalidanMahasiswayangdiwalikan.xlsx.xlsx'),
(6, '[1495653141]tes.xlsx', 'std5_5111', 1495653141, 2, 'uploads/dokumenpendukungstd5/std5_5111/[1495653141]tes.xlsx'),
(7, '[1495690472]datastandar5-profillulusan.xlsx', 'std5_5112', 1495690472, 2, 'uploads/dokumenpendukungstd5/std5_5112/[1495690472]datastandar5-profillulusan.xlsx'),
(8, '[1495771253]tes.xlsx', 'std5_5111', 1495771253, 1, 'uploads/dokumenpendukungstd5/std5_5111/[1495771253]tes.xlsx');

-- --------------------------------------------------------

--
-- Table structure for table `std5_history_uploadfile`
--

CREATE TABLE `std5_history_uploadfile` (
  `id` int(255) NOT NULL,
  `nama_file` varchar(500) NOT NULL,
  `tabel_db` varchar(100) NOT NULL,
  `tanggal` int(100) UNSIGNED NOT NULL,
  `id_user` int(20) NOT NULL,
  `excel_path` varchar(255) NOT NULL,
  `excelhtml_path` varchar(255) NOT NULL,
  `docx_path` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `std5_history_uploadfile`
--

INSERT INTO `std5_history_uploadfile` (`id`, `nama_file`, `tabel_db`, `tanggal`, `id_user`, `excel_path`, `excelhtml_path`, `docx_path`) VALUES
(69, '[Versi0]datastandar5-5.5.1Pelaksanaanpembimbingan.xlsx', 'std5_551pelaksanaanpembimbingan', 1492333338, 2, 'uploads/dokumendatastd5/std5_551pelaksanaanpembimbingan/[Versi0]datastandar5-5.5.1Pelaksanaanpembimbingan.xlsx', 'uploads/dokumendatastd5/std5_551pelaksanaanpembimbingan/[Versi0]datastandar5-5.5.1Pelaksanaanpembimbingan.xlsx.html', ''),
(70, '[Versi1]datastandar5-5.5.1Pelaksanaanpembimbingan.xlsx', 'std5_551pelaksanaanpembimbingan', 1492337081, 2, 'uploads/dokumendatastd5/std5_551pelaksanaanpembimbingan/[Versi1]datastandar5-5.5.1Pelaksanaanpembimbingan.xlsx', 'uploads/dokumendatastd5/std5_551pelaksanaanpembimbingan/[Versi1]datastandar5-5.5.1Pelaksanaanpembimbingan.xlsx.html', ''),
(79, '[Versi0]datastandar5-5.6Upayaperbaikanpembelajaran.xlsx', 'std5_56upayaperbaikanpembelajaran', 1493046961, 2, 'uploads/dokumendatastd5/std5_56upayaperbaikanpembelajaran/[Versi0]datastandar5-5.6Upayaperbaikanpembelajaran.xlsx', 'uploads/dokumendatastd5/std5_56upayaperbaikanpembelajaran/[Versi0]datastandar5-5.6Upayaperbaikanpembelajaran.xlsx.html', ''),
(80, '[Versi2]datastandar5-5.6Upayaperbaikanpembelajaran.xlsx', 'std5_56upayaperbaikanpembelajaran', 1493047292, 2, 'uploads/dokumendatastd5/std5_56upayaperbaikanpembelajaran/[Versi2]datastandar5-5.6Upayaperbaikanpembelajaran.xlsx', 'uploads/dokumendatastd5/std5_56upayaperbaikanpembelajaran/[Versi2]datastandar5-5.6Upayaperbaikanpembelajaran.xlsx.html', ''),
(81, '[Versi3]datastandar5-5.6Upayaperbaikanpembelajaran.xlsx', 'std5_56upayaperbaikanpembelajaran', 1493047510, 2, 'uploads/dokumendatastd5/std5_56upayaperbaikanpembelajaran/[Versi3]datastandar5-5.6Upayaperbaikanpembelajaran.xlsx', 'uploads/dokumendatastd5/std5_56upayaperbaikanpembelajaran/[Versi3]datastandar5-5.6Upayaperbaikanpembelajaran.xlsx.html', ''),
(82, '[Versi0]datastandar5-5.1.2.1jumlahsksprogramstudi.xlsx', 'std5_5121jumlahsksps', 1493050871, 2, 'uploads/dokumendatastd5/std5_5121jumlahsksps/[Versi0]datastandar5-5.1.2.1jumlahsksprogramstudi.xlsx', 'uploads/dokumendatastd5/std5_5121jumlahsksps/[Versi0]datastandar5-5.1.2.1jumlahsksprogramstudi.xlsx.html', ''),
(83, '[Versi2]datastandar5-5.1.2.1jumlahsksprogramstudi.xlsx', 'std5_5121jumlahsksps', 1493743013, 2, 'uploads/dokumendatastd5/std5_5121jumlahsksps/[Versi2]datastandar5-5.1.2.1jumlahsksprogramstudi.xlsx', 'uploads/dokumendatastd5/std5_5121jumlahsksps/[Versi2]datastandar5-5.1.2.1jumlahsksprogramstudi.xlsx.html', ''),
(97, '[Versi3]datastandar5-5.1.2.1jumlahsksprogramstudi.xlsx', 'std5_5121jumlahsksps', 1493839483, 1, 'uploads/dokumendatastd5/std5_5121jumlahsksps/[Versi3]datastandar5-5.1.2.1jumlahsksprogramstudi.xlsx', 'uploads/dokumendatastd5/std5_5121jumlahsksps/[Versi3]datastandar5-5.1.2.1jumlahsksprogramstudi.xlsx.html', ''),
(98, '[Versi4]datastandar5-5.1.2.1jumlahsksprogramstudi.xlsx', 'std5_5121jumlahsksps', 1493839600, 1, 'uploads/dokumendatastd5/std5_5121jumlahsksps/[Versi4]datastandar5-5.1.2.1jumlahsksprogramstudi.xlsx', 'uploads/dokumendatastd5/std5_5121jumlahsksps/[Versi4]datastandar5-5.1.2.1jumlahsksprogramstudi.xlsx.html', ''),
(99, '[Versi0]datastandar5-5.2HasilPeninjauanKurikulum2012menjadiKurikulum2016terkaitPerubahanMataKuliah.xlsx', 'std5_52peninjauankurikulum', 1493839959, 1, 'uploads/dokumendatastd5/std5_52peninjauankurikulum/[Versi0]datastandar5-5.2HasilPeninjauanKurikulum2012menjadiKurikulum2016terkaitPerubahanMataKuliah.xlsx', 'uploads/dokumendatastd5/std5_52peninjauankurikulum/[Versi0]datastandar5-5.2HasilPeninjauanKurikulum2012menjadiKurikulum2016terkaitPerubahanMataKuliah.xlsx.html', ''),
(100, '[Versi2]datastandar5-5.4.1DaftarDosenPembimbingAkademikdanJumlahMahasiswayangDibimbing.xlsx', 'std5_541daftardosenpembimbing', 1493841046, 1, 'uploads/dokumendatastd5/std5_541daftardosenpembimbing/[Versi2]datastandar5-5.4.1DaftarDosenPembimbingAkademikdanJumlahMahasiswayangDibimbing.xlsx', 'uploads/dokumendatastd5/std5_541daftardosenpembimbing/[Versi2]datastandar5-5.4.1DaftarDosenPembimbingAkademikdanJumlahMahasiswayangDibimbing.xlsx.html', ''),
(101, '[Versi4]datastandar5-5.6Upayaperbaikanpembelajaran.xlsx', 'std5_56upayaperbaikanpembelajaran', 1493841517, 1, 'uploads/dokumendatastd5/std5_56upayaperbaikanpembelajaran/[Versi4]datastandar5-5.6Upayaperbaikanpembelajaran.xlsx', 'uploads/dokumendatastd5/std5_56upayaperbaikanpembelajaran/[Versi4]datastandar5-5.6Upayaperbaikanpembelajaran.xlsx.html', ''),
(102, '[Versi2]datastandar5-5.5.1Pelaksanaanpembimbingan.xlsx', 'std5_551pelaksanaanpembimbingan', 1495017137, 2, 'uploads/dokumendatastd5/std5_551pelaksanaanpembimbingan/[Versi2]datastandar5-5.5.1Pelaksanaanpembimbingan.xlsx', 'uploads/dokumendatastd5/std5_551pelaksanaanpembimbingan/[Versi2]datastandar5-5.5.1Pelaksanaanpembimbingan.xlsx.html', '');

-- --------------------------------------------------------

--
-- Table structure for table `std5_penilaian`
--

CREATE TABLE `std5_penilaian` (
  `id` int(25) NOT NULL,
  `butir_penilaian` varchar(50) NOT NULL,
  `aspek_penilaian` int(25) NOT NULL,
  `bobot` float NOT NULL,
  `nilai` float NOT NULL,
  `bobotxnilai` float NOT NULL,
  `catatan` varchar(1000) NOT NULL,
  `oleh` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `std5_penilaian`
--

INSERT INTO `std5_penilaian` (`id`, `butir_penilaian`, `aspek_penilaian`, `bobot`, `nilai`, `bobotxnilai`, `catatan`, `oleh`) VALUES
(50, '5.1.1.a', 50, 0.57, 4, 2.28, '', 1),
(51, '5.1.1.b', 51, 0.57, 4, 2.28, '', 1),
(52, '5.1.2.a', 52, 0.57, 3, 1.71, '', 2),
(53, '5.1.2.b', 53, 0.57, 4, 2.28, '', 2),
(54, '5.1.2.c', 54, 0.57, 4, 2.28, '', 2),
(55, '5.1.3', 55, 0.57, 4, 2.28, '', 1),
(56, '5.1.4', 56, 1.14, 3, 3.42, 'kontrol dari dosen (evidence)', 1),
(57, '5.2.a', 57, 0.57, 4, 2.28, '', 1),
(58, '5.2.b', 58, 0.57, 4, 2.28, '', 1),
(59, '5.3.1.a', 59, 1.14, 3.33333, 3.8, '', 1),
(60, '5.3.1.b', 60, 0.57, 4, 2.28, '', 1),
(61, '5.3.2', 61, 0.57, 2, 1.14, 'dibuat evidence nya', 1),
(62, '5.4.1.a', 62, 0.57, 3.38537, 1.92966, '', 1),
(63, '5.4.1.b', 63, 0.57, 3, 1.71, 'dibuat evidence dan dibuat aturan nya di update (untuk bimbingan dosen wali)', 1),
(64, '5.4.1.c', 64, 0.57, 4, 2.28, '', 1),
(65, '5.4.1.d', 65, 0.57, 4, 2.28, '', 1),
(66, '5.5.1.a', 66, 0.57, 2, 1.14, 'mesti dibuat panduan TA danbimbingan TA', 1),
(67, '5.5.1.b', 67, 0.57, 3.73485, 2.12886, '', 1),
(68, '5.5.1.c', 68, 0.57, 4, 2.28, '', 1),
(69, '5.5.1.d', 69, 1.14, 3, 3.42, 'berkaitan dengan SDM', 1),
(70, '5.5.2', 70, 1.14, 3.5, 3.99, '', 1),
(71, '5.6', 71, 0.57, 3, 1.71, '', 1),
(72, '5.7.1', 72, 0.57, 4, 2.28, '', 2),
(73, '5.7.2', 73, 1.14, 4, 4.56, '', 2),
(74, '5.7.3', 74, 1.14, 4, 4.56, '', 2),
(75, '5.7.4', 75, 0.57, 4, 2.28, '', 2),
(76, '5.7.5', 76, 0.57, 4, 2.28, '', 2);

-- --------------------------------------------------------

--
-- Table structure for table `std5_penilaian52`
--

CREATE TABLE `std5_penilaian52` (
  `id` int(10) NOT NULL,
  `bobot` double NOT NULL DEFAULT '0.57',
  `nilai52a` double NOT NULL,
  `nilai52b` double NOT NULL,
  `bobotxnilai52a` double NOT NULL,
  `bobotxnilai52b` double NOT NULL,
  `catatan52a` varchar(1000) DEFAULT NULL,
  `catatan52b` varchar(1000) DEFAULT NULL,
  `oleh` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `std5_penilaian52`
--

INSERT INTO `std5_penilaian52` (`id`, `bobot`, `nilai52a`, `nilai52b`, `bobotxnilai52a`, `bobotxnilai52b`, `catatan52a`, `catatan52b`, `oleh`) VALUES
(1, 0.57, 4, 4, 2.28, 2.28, '', '', 2);

-- --------------------------------------------------------

--
-- Table structure for table `std5_penilaian54`
--

CREATE TABLE `std5_penilaian54` (
  `id` int(10) NOT NULL,
  `bobot` double NOT NULL DEFAULT '0.57',
  `nilai541a` double NOT NULL,
  `nilai541b` double NOT NULL,
  `nilai541c` double NOT NULL,
  `nilai542` double NOT NULL,
  `bobotxnilai541a` double NOT NULL,
  `bobotxnilai541b` double NOT NULL,
  `bobotxnilai541c` double NOT NULL,
  `bobotxnilai542` double NOT NULL,
  `catatan541a` varchar(1000) NOT NULL,
  `catatan541b` varchar(1000) NOT NULL,
  `catatan541c` varchar(1000) NOT NULL,
  `catatan542` varchar(1000) NOT NULL,
  `oleh` int(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `std5_penilaian54`
--

INSERT INTO `std5_penilaian54` (`id`, `bobot`, `nilai541a`, `nilai541b`, `nilai541c`, `nilai542`, `bobotxnilai541a`, `bobotxnilai541b`, `bobotxnilai541c`, `bobotxnilai542`, `catatan541a`, `catatan541b`, `catatan541c`, `catatan542`, `oleh`) VALUES
(1, 0.57, 3.385365853658537, 3, 4, 4, 1.9296585365854, 1.71, 2.28, 2.28, '', 'dibuat evidence dan dibuat aturan nya di update (untuk bimbingan dosen wali)', '', '', 2);

-- --------------------------------------------------------

--
-- Table structure for table `std5_penilaian56`
--

CREATE TABLE `std5_penilaian56` (
  `id` int(10) NOT NULL,
  `bobot` double NOT NULL DEFAULT '0.57',
  `nilai56` double NOT NULL,
  `bobotxnilai56` double NOT NULL,
  `catatan56` varchar(1000) NOT NULL,
  `oleh` int(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `std5_penilaian56`
--

INSERT INTO `std5_penilaian56` (`id`, `bobot`, `nilai56`, `bobotxnilai56`, `catatan56`, `oleh`) VALUES
(1, 0.57, 3, 1.71, '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `std5_penilaian511`
--

CREATE TABLE `std5_penilaian511` (
  `id` int(10) NOT NULL,
  `bobot` double NOT NULL DEFAULT '0.57',
  `nilai511a` double NOT NULL,
  `nilai511b` double NOT NULL,
  `bobotxnilai511a` double NOT NULL,
  `bobotxnilai511b` double NOT NULL,
  `catatan511a` varchar(1000) DEFAULT NULL,
  `catatan511b` varchar(1000) DEFAULT NULL,
  `oleh` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `std5_penilaian511`
--

INSERT INTO `std5_penilaian511` (`id`, `bobot`, `nilai511a`, `nilai511b`, `bobotxnilai511a`, `bobotxnilai511b`, `catatan511a`, `catatan511b`, `oleh`) VALUES
(1, 0.57, 4, 4, 2.28, 2.28, '', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `std5_penilaian512`
--

CREATE TABLE `std5_penilaian512` (
  `id` int(10) NOT NULL,
  `bobot` double NOT NULL DEFAULT '0.57',
  `nilai512a` double NOT NULL,
  `nilai512b` double NOT NULL,
  `nilai512c` double NOT NULL,
  `bobotxnilai512a` double NOT NULL,
  `bobotxnilai512b` double NOT NULL,
  `bobotxnilai512c` double NOT NULL,
  `catatan512a` varchar(1000) DEFAULT NULL,
  `catatan512b` varchar(1000) DEFAULT NULL,
  `catatan512c` varchar(1000) DEFAULT NULL,
  `oleh` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `std5_penilaian512`
--

INSERT INTO `std5_penilaian512` (`id`, `bobot`, `nilai512a`, `nilai512b`, `nilai512c`, `bobotxnilai512a`, `bobotxnilai512b`, `bobotxnilai512c`, `catatan512a`, `catatan512b`, `catatan512c`, `oleh`) VALUES
(1, 0.57, 3, 4, 4, 1.71, 2.28, 2.28, '', '512b', '512c', 2);

-- --------------------------------------------------------

--
-- Table structure for table `std5_penilaian513`
--

CREATE TABLE `std5_penilaian513` (
  `id` int(10) NOT NULL,
  `bobot` double NOT NULL DEFAULT '0.57',
  `nilai513` double NOT NULL,
  `bobotxnilai513` double NOT NULL,
  `catatan513` varchar(1000) DEFAULT NULL,
  `oleh` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `std5_penilaian513`
--

INSERT INTO `std5_penilaian513` (`id`, `bobot`, `nilai513`, `bobotxnilai513`, `catatan513`, `oleh`) VALUES
(1, 0.57, 4, 2.28, '', 2);

-- --------------------------------------------------------

--
-- Table structure for table `std5_penilaian514`
--

CREATE TABLE `std5_penilaian514` (
  `id` int(10) NOT NULL,
  `bobot` double NOT NULL DEFAULT '1.14',
  `nilai514` double NOT NULL,
  `bobotxnilai514` double NOT NULL,
  `catatan514` varchar(1000) DEFAULT NULL,
  `oleh` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `std5_penilaian514`
--

INSERT INTO `std5_penilaian514` (`id`, `bobot`, `nilai514`, `bobotxnilai514`, `catatan514`, `oleh`) VALUES
(1, 1.14, 3, 3.42, 'kontrol dari dosen (evidence)', 1);

-- --------------------------------------------------------

--
-- Table structure for table `std5_penilaian531`
--

CREATE TABLE `std5_penilaian531` (
  `id` int(10) NOT NULL,
  `bagi531a` int(11) NOT NULL DEFAULT '3',
  `bobot531b` double NOT NULL DEFAULT '0.57',
  `nilai531a` double NOT NULL,
  `nilai531b` double NOT NULL,
  `nilai531a/bagi` double NOT NULL,
  `bobotxnilai531b` double NOT NULL,
  `catatan531a` varchar(1000) DEFAULT NULL,
  `catatan531b` varchar(1000) DEFAULT NULL,
  `oleh` int(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `std5_penilaian531`
--

INSERT INTO `std5_penilaian531` (`id`, `bagi531a`, `bobot531b`, `nilai531a`, `nilai531b`, `nilai531a/bagi`, `bobotxnilai531b`, `catatan531a`, `catatan531b`, `oleh`) VALUES
(1, 3, 0.57, 10, 4, 3.3333333333333, 2.28, '', '', 2);

-- --------------------------------------------------------

--
-- Table structure for table `std5_penilaian532`
--

CREATE TABLE `std5_penilaian532` (
  `id` int(11) NOT NULL,
  `bobot` double NOT NULL DEFAULT '0.57',
  `nilai532` double NOT NULL,
  `bobotxnilai532` double NOT NULL,
  `catatan532` varchar(1000) NOT NULL,
  `oleh` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `std5_penilaian532`
--

INSERT INTO `std5_penilaian532` (`id`, `bobot`, `nilai532`, `bobotxnilai532`, `catatan532`, `oleh`) VALUES
(1, 0.57, 2, 1.14, 'dibuat evidence nya', 2);

-- --------------------------------------------------------

--
-- Table structure for table `std5_penilaian551`
--

CREATE TABLE `std5_penilaian551` (
  `id` int(10) NOT NULL,
  `bobot551abc` double NOT NULL DEFAULT '0.57',
  `bobot551d` double NOT NULL DEFAULT '1.14',
  `nilai551a` double NOT NULL,
  `nilai551b` double NOT NULL,
  `nilai551c` double NOT NULL,
  `nilai551d` double NOT NULL,
  `bobotxnilai551a` double NOT NULL,
  `bobotxnilai551b` double NOT NULL,
  `bobotxnilai551c` double NOT NULL,
  `bobotxnilai551d` double NOT NULL,
  `catatan551a` varchar(1000) NOT NULL,
  `catatan551b` varchar(1000) NOT NULL,
  `catatan551c` varchar(1000) NOT NULL,
  `catatan551d` varchar(1000) NOT NULL,
  `oleh` int(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `std5_penilaian551`
--

INSERT INTO `std5_penilaian551` (`id`, `bobot551abc`, `bobot551d`, `nilai551a`, `nilai551b`, `nilai551c`, `nilai551d`, `bobotxnilai551a`, `bobotxnilai551b`, `bobotxnilai551c`, `bobotxnilai551d`, `catatan551a`, `catatan551b`, `catatan551c`, `catatan551d`, `oleh`) VALUES
(1, 0.57, 1.14, 2, 3.734848484848485, 4, 3, 1.14, 2.1288636363636, 2.28, 3.42, 'mesti dibuat panduan TA danbimbingan TA', '', '', 'berkaitan dengan SDM', 2);

-- --------------------------------------------------------

--
-- Table structure for table `std5_penilaian552`
--

CREATE TABLE `std5_penilaian552` (
  `id` int(11) NOT NULL,
  `bobot` double NOT NULL DEFAULT '1.14',
  `nilai552` double NOT NULL,
  `bobotxnilai552` double NOT NULL,
  `catatan552` varchar(1000) NOT NULL,
  `oleh` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `std5_penilaian552`
--

INSERT INTO `std5_penilaian552` (`id`, `bobot`, `nilai552`, `bobotxnilai552`, `catatan552`, `oleh`) VALUES
(1, 1.14, 3.5, 3.99, '', 2);

-- --------------------------------------------------------

--
-- Table structure for table `std5_peranpekerjaanalumni`
--

CREATE TABLE `std5_peranpekerjaanalumni` (
  `no` int(50) NOT NULL,
  `peran_lulusan` varchar(500) NOT NULL,
  `deskripsi_peran_lulusan` varchar(500) NOT NULL,
  `excel_path` varchar(255) NOT NULL,
  `excelhtml_path` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `std5_peranpekerjaanalumni`
--

INSERT INTO `std5_peranpekerjaanalumni` (`no`, `peran_lulusan`, `deskripsi_peran_lulusan`, `excel_path`, `excelhtml_path`) VALUES
(1, 'Information System Developer', 'Lulusan sistem informasi universitas telkom memiliki kemampuan untuk menganalisis, merancang, membuat, menguji, menerapkan dan mengevaluasi sistem informasi untuk memenuhi kebutuhan bisnis dalam lingkungan global yang kompetitif. Pilihan Karir: Programmer, Application Architect, Application Auditor, System / Business Analyst', 'uploads/dokumenpendukungstd5/datatabelstandar5.2-PeranPekerjaanAlumniProgramStudiS1SistemInformasi(2012-2016).xlsx', 'uploads/dokumenpendukungstd5/datatabelstandar5.2-PeranPekerjaanAlumniProgramStudiS1SistemInformasi(2012-2016).xlsx.html'),
(2, 'System Integrator', 'Lulusan sistem informasi universitas telkom memiliki kemampuan dalam menganalisis, memodelkan proses bisnis dan data, mengintegrasikan seluruh komponen sistem untuk menerapkan solusi berbasis sistem informasi guna membantu individu, kelompok dan organisasi dalam mencapai tujuannya. Pilihan Karir: Business Process Analyst, Enterprise Architect, ERP Specialist, IS Consultant, IS Project Manager', 'uploads/dokumenpendukungstd5/datatabelstandar5.2-PeranPekerjaanAlumniProgramStudiS1SistemInformasi(2012-2016).xlsx', 'uploads/dokumenpendukungstd5/datatabelstandar5.2-PeranPekerjaanAlumniProgramStudiS1SistemInformasi(2012-2016).xlsx.html'),
(3, 'Database Specialist', 'Lulusan sistem informasi universitas telkom memiliki kemampuan dalam menganalisis data, merancang, membangun, memelihara, dan mengintegrasikan basis data, dan memberikan rekomendasi untuk perbaikan sistem. Pilihan Karir: Database Designer, Database Programmer, Database Administrator, DataAnalyst', 'uploads/dokumenpendukungstd5/datatabelstandar5.2-PeranPekerjaanAlumniProgramStudiS1SistemInformasi(2012-2016).xlsx', 'uploads/dokumenpendukungstd5/datatabelstandar5.2-PeranPekerjaanAlumniProgramStudiS1SistemInformasi(2012-2016).xlsx.html'),
(4, 'IT Governance, Risk and Compliance Specialist', 'Lulusan sistem informasi universitas telkom memiliki kemampuan untuk mengukur kinerja pemanfaatan IT, melakukan identifikasi kemungkinan resiko dalam pemanfaatan IT, dan merancang rekomendasi untuk menyesuaikan penggunaan IT  yang sesuai dengan kebutuhan bisnis organisasi. Pilihan Karir: IT & Networking Administrator, System Administrator, Security Administator, IT Consultant and Compliance', 'uploads/dokumenpendukungstd5/datatabelstandar5.2-PeranPekerjaanAlumniProgramStudiS1SistemInformasi(2012-2016).xlsx', 'uploads/dokumenpendukungstd5/datatabelstandar5.2-PeranPekerjaanAlumniProgramStudiS1SistemInformasi(2012-2016).xlsx.html'),
(5, 'Technopreneur', 'Lulusan sistem informasi universitas telkom memiliki kemampuan untuk merancang dan mengembangkan bisnis berbasis inovasi dengan pemanfaatan IT sebagai solusi dalam penyelesaian masalah dimasyarakat.', 'uploads/dokumenpendukungstd5/datatabelstandar5.2-PeranPekerjaanAlumniProgramStudiS1SistemInformasi(2012-2016).xlsx', 'uploads/dokumenpendukungstd5/datatabelstandar5.2-PeranPekerjaanAlumniProgramStudiS1SistemInformasi(2012-2016).xlsx.html');

-- --------------------------------------------------------

--
-- Table structure for table `std5_profil_lulusan`
--

CREATE TABLE `std5_profil_lulusan` (
  `no` int(25) NOT NULL,
  `profil_kelulusan` varchar(100) NOT NULL,
  `keterangan` varchar(1000) NOT NULL,
  `excel_path` varchar(100) NOT NULL,
  `excelhtml_path` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `std5_profil_lulusan`
--

INSERT INTO `std5_profil_lulusan` (`no`, `profil_kelulusan`, `keterangan`, `excel_path`, `excelhtml_path`) VALUES
(1, 'Karyawan (ICT Worker)', 'Meniti karir dari level staf hingga tingkatan yang lebih tinggi (manajemen), baik diperusahaan, pemerintahan maulun organisasi lainnya.', 'uploads/dokumenpendukungstd5/datastandar5-profillulusan.xlsx', 'uploads/dokumenpendukungstd5/datastandar5-profillulusan.xlsx.html'),
(2, 'Profesional', 'Menjadi freelancer yang siap direkrut kapan saja oleh siapa saja dalam format pekerjaan berbasis proyek atau program.', 'uploads/dokumenpendukungstd5/datastandar5-profillulusan.xlsx', 'uploads/dokumenpendukungstd5/datastandar5-profillulusan.xlsx.html'),
(3, 'Wiraswasta (Entrepreneur)', 'Menggunakan kemampuan kreativitas dan inovasi yang dimiliki untuk membangun usaha mandiri atau menciptakan lapangan kerja bagi orang lain.', 'uploads/dokumenpendukungstd5/datastandar5-profillulusan.xlsx', 'uploads/dokumenpendukungstd5/datastandar5-profillulusan.xlsx.html'),
(4, 'Akademisi', 'Memfokuskan diri untuk menjadi pengajar, Dosen, atau peneliti diberbagai institusi pendidikan tinggi yang melahirkan sarjana-sarjana baru.', 'uploads/dokumenpendukungstd5/datastandar5-profillulusan.xlsx', 'uploads/dokumenpendukungstd5/datastandar5-profillulusan.xlsx.html');

-- --------------------------------------------------------

--
-- Table structure for table `std7_penilaian`
--

CREATE TABLE `std7_penilaian` (
  `id` varchar(50) NOT NULL,
  `aspek_penilaian` varchar(500) NOT NULL,
  `simulasi_nilai` float NOT NULL,
  `bobot` float NOT NULL,
  `nilai` float NOT NULL,
  `penilai_terakhir` varchar(300) NOT NULL,
  `date` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `std7_penilaian`
--

INSERT INTO `std7_penilaian` (`id`, `aspek_penilaian`, `simulasi_nilai`, `bobot`, `nilai`, `penilai_terakhir`, `date`) VALUES
('7.1.1', 'Jumlah penelitian yang dilakukan oleh dosen tetap yang bidang keahliannya sesuai dengan PS per tahun, selama 3 tahun', 4, 3.75, 15, 'aa', '07-08-2017 14:14:52'),
('7.1.2', 'Keterlibatan mahasiswa yang melakukan tugas akhir dalam penelitian dosen \r\n', 4, 1.88, 7.52, 'Teguh', '27-07-2017 02:40:46'),
('7.1.3', 'Jumlah artikel ilmiah yang dihasilkan oleh dosen tetap yang bidang keahliannya sesuai dengan PS per tahun, selama tiga tahun. \r\n', 4, 3.75, 15, 'adminbaru', '27-07-2017 02:13:23'),
('7.1.4', 'Karya-karya PS/institusi yang telah memperoleh perlindungan Hak atas Kekayaan Intelektual (HaKI) dalam tiga tahun terakhir.', 3, 1.88, 5.64, 'adminbaru', '03-08-2017 10:31:50'),
('7.2.1', 'Jumlah kegiatan Pengabdian kepada Masyarakat (PkM) yang dilakukan oleh dosen tetap yang bidang keahliannya sesuai dengan PS.', 4, 1.88, 7.52, 'adminbaru', '27-07-2017 02:12:45'),
('7.2.2', 'Keterlibatan mahasiswa dalam kegiatan pengabdian kepada masyarakat.', 2, 1.88, 3.76, 'adminbaru', '27-07-2017 02:12:52'),
('7.3.1', 'Kegiatan kerjasama dengan instansi di dalam negeri dalam tiga tahun terakhir.', 4, 1.88, 7.52, 'adminbaru', '27-07-2017 02:12:30'),
('7.3.2', 'Kegiatan kerjasama dengan instansi di luar negeri dalam tiga tahun terakhir.', 4, 1.88, 7.52, 'adminbaru', '27-07-2017 02:12:35');

-- --------------------------------------------------------

--
-- Table structure for table `tb_7_2_2_abdimas`
--

CREATE TABLE `tb_7_2_2_abdimas` (
  `no` int(10) NOT NULL,
  `id_user` int(11) NOT NULL,
  `text` varchar(3000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_7_2_2_abdimas`
--

INSERT INTO `tb_7_2_2_abdimas` (`no`, `id_user`, `text`) VALUES
(13, 0, '<p>Tingkat partisipasi mahasiswa pada kegiatan pelayanan/pengabdian masyarakat cukup tinggi, rata-rata pada setiap 1 (satu) kegiatan pengabdian masyarakat telah dilibatkan sekitar 3-4 orang mahasiswa. Meskipun beberapa pelayanan/pengabdian masyarakat masih ada yang belum melibatkan mahasiswa sama sekali. Namun secara umum, jumlah keterlibatan mahasiswa sudah cukup tinggi dan meningkat pada tahun 2015/2016, dimana dari sekitar 34 pelayanan/pengabdian masyarakat yang berlangsung selama 3 (tiga) tahun terkahir, sejumlah 92 mahasiswa dilibatkan dalam kegiatan tersebut. Dengan rincian, jumlah partisipasi mahasiswa yang semula terdapat 20 mahasiswa, menurun pada tahun 2014/2015 menjadi 9 mahasiswa. Namun jumlah tersebut meningkat tajam pada tahun 2015/2016 menjadi 63 mahasiswa. Dimana setiap mahasiswa terlibat aktif dengan menjadi penanggung jawab acara pelatihan jika bentuk pelayanan/pengabdian masyarakat yang berlangsung berupa jasa pelayanan, sebagai contoh sebagai ketua koordinator humas, acara, konsumsi pada kegiatan pelatihan yang berlangsung. Sementara bagi pelayanan/pengabdian masyarakat yang menghasilkan produk seperti aplikasi/alat, mahasiswa berperan aktif membantu dalam kegiatan pembangunan aplikasi/alat tersebut, sebagai contoh sebagai tim penanggung jawab pembangunan konten/fungsi pada aplikasi, melakukan pengujian, dan mendefinisikan kebutuhan.</p>\r\n'),
(14, 1, '<p>Test Tingkat partisipasi mahasiswa pada kegiatan pelayanan/pengabdian masyarakat cukup tinggi, rata-rata pada setiap 1 (satu) kegiatan pengabdian masyarakat telah dilibatkan sekitar 3-4 orang mahasiswa. Meskipun beberapa pelayanan/pengabdian masyarakat masih ada yang belum melibatkan mahasiswa sama sekali. Namun secara umum, jumlah keterlibatan mahasiswa sudah cukup tinggi dan meningkat pada tahun 2015/2016, dimana dari sekitar 34 pelayanan/pengabdian masyarakat yang berlangsung selama 3 (tiga) tahun terkahir, sejumlah 92 mahasiswa dilibatkan dalam kegiatan tersebut. Dengan rincian, jumlah partisipasi mahasiswa yang semula terdapat 20 mahasiswa, menurun pada tahun 2014/2015 menjadi 9 mahasiswa. Namun jumlah tersebut meningkat tajam pada tahun 2015/2016 menjadi 63 mahasiswa. Dimana setiap mahasiswa terlibat aktif dengan menjadi penanggung jawab acara pelatihan jika bentuk pelayanan/pengabdian masyarakat yang berlangsung berupa jasa pelayanan, sebagai contoh sebagai ketua koordinator humas, acara, konsumsi pada kegiatan pelatihan yang berlangsung. Sementara bagi pelayanan/pengabdian masyarakat yang menghasilkan produk seperti aplikasi/alat, mahasiswa berperan aktif membantu dalam kegiatan pembangunan aplikasi/alat tersebut, sebagai contoh sebagai tim penanggung jawab pembangunan konten/fungsi pada aplikasi, melakukan pengujian, dan mendefinisikan kebutuhan.</p>\r\n'),
(15, 1, '<p>Tingkat partisipasi mahasiswa pada kegiatan pelayanan/pengabdian masyarakat cukup tinggi, rata-rata pada setiap 1 (satu) kegiatan pengabdian masyarakat telah dilibatkan sekitar 2-3 orang mahasiswa. Meskipun beberapa pelayanan/pengabdian masyarakat masih ada yang belum melibatkan mahasiswa sama sekali. Namun secara umum, jumlah keterlibatan mahasiswa sudah cukup tinggi, dimana dari sekitar 29 pelayanan/pengabdian masyarakat yang berlangsung selama 3 (tiga) tahun terkahir, sejumlah 57 mahasiswa dilibatkan dalam kegiatan tersebut. Dengan rincian, jumlah partisipasi mahasiswa yang semula terdapat 20 mahasiswa, menurun pada tahun 2014/2015 menjadi 19 mahasiswa. Dan terdapat sedikit penurunan pada tahun 2015/2016 menjadi 18 mahasiswa. Dimana setiap mahasiswa terlibat aktif dengan menjadi penanggung jawab acara pelatihan jika bentuk pelayanan/pengabdian masyarakat yang berlangsung berupa jasa pelayanan, sebagai contoh sebagai ketua koordinator humas, acara, konsumsi pada kegiatan pelatihan yang berlangsung. Sementara bagi pelayanan/pengabdian masyarakat yang menghasilkan produk seperti aplikasi/alat, mahasiswa berperan aktif membantu dalam kegiatan pembangunan aplikasi/alat tersebut, sebagai contoh sebagai tim penanggung jawab pembangunan konten/fungsi pada aplikasi, melakukan pengujian, dan mendefinisikan kebutuhan.</p>\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `tb_abdimas`
--

CREATE TABLE `tb_abdimas` (
  `no` int(11) NOT NULL,
  `program_studi` varchar(100) DEFAULT NULL,
  `no_ts` varchar(30) DEFAULT NULL,
  `type` varchar(100) DEFAULT NULL,
  `judul` varchar(150) DEFAULT NULL,
  `lokasi` varchar(200) DEFAULT NULL,
  `mitra` varchar(200) DEFAULT NULL,
  `tahun` varchar(100) DEFAULT NULL,
  `nama_dosen_ketua` varchar(250) DEFAULT NULL,
  `nama_dosen_anggota` varchar(1000) DEFAULT NULL,
  `nama_anggotadosen2` varchar(500) DEFAULT NULL,
  `nama_anggotadosen3` varchar(500) DEFAULT NULL,
  `nama_anggotadosen4` varchar(500) DEFAULT NULL,
  `nama_anggotadosen5` varchar(500) DEFAULT NULL,
  `nama_mahasiswa` varchar(1000) DEFAULT NULL,
  `anggaran` int(11) DEFAULT NULL,
  `bukti_laporan_akhir` varchar(300) DEFAULT NULL,
  `keterangan` varchar(1000) DEFAULT NULL,
  `sumber_pembiayaan` varchar(300) DEFAULT NULL,
  `sts_valid` varchar(50) DEFAULT NULL,
  `file_name` varchar(300) DEFAULT NULL,
  `file_type` varchar(300) DEFAULT NULL,
  `file_date` varchar(300) DEFAULT NULL,
  `id_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_abdimas`
--

INSERT INTO `tb_abdimas` (`no`, `program_studi`, `no_ts`, `type`, `judul`, `lokasi`, `mitra`, `tahun`, `nama_dosen_ketua`, `nama_dosen_anggota`, `nama_anggotadosen2`, `nama_anggotadosen3`, `nama_anggotadosen4`, `nama_anggotadosen5`, `nama_mahasiswa`, `anggaran`, `bukti_laporan_akhir`, `keterangan`, `sumber_pembiayaan`, `sts_valid`, `file_name`, `file_type`, `file_date`, `id_user`) VALUES
(1, 'SISTEM INFORMASI', 'TS_2', 'PENGABDIAN MASYARAKAT DANA INTERNAL', 'Pelatihan Pembuatan Blog untuk Guru di kabupaten Bojongsoang dan Dayeuh Kolot', 'Telkom University', 'Guru SD dan SMP Bojongsoang', '05-07-1905', 'Riza Agustiansyah ', '', '', '', '', '', '', 10000000, 'CV - Riza Agustiansyah', 'valid', 'PT yang bersangkutan', 'valid', NULL, NULL, NULL, 1),
(2, 'SISTEM INFORMASI', 'TS_2', 'PENGABDIAN MASYARAKAT DANA INTERNAL', 'Pembuatan Konten E-Learning untuk Mobil Internet Keliling Milik Pemerintah Daerah Kabupaten Bandung', 'IT Telkom', 'BAPAPSI Kabupaten Bandung', 'des 2013', 'Amelia Kurniawati', 'Dr. Luciana Andrawina    Nia Ambarsari, S. Si., MT.     Seno Adi Putra              Warih Puspitasari               M. Teguh Kurniawan', NULL, NULL, NULL, NULL, 'Zulfikar Akbar                       M. Mulya Fuadi A.               Anggita Regia M.                 Dwi Ernani                              Ricky Triwanda P.                    Pargaulan Siagian', 7110000, 'proposal hard', 'valid', 'PT yang bersangkutan', 'valid', NULL, NULL, NULL, 1),
(3, 'SISTEM INFORMASI', 'TS_2', 'PENGABDIAN MASYARAKAT DANA INTERNAL', 'Pelatihan Pembuatan ICT Masterpan dan Pemanfaatan Aplikasi Microsoft Project untuk Staf Bapapsi Kabupaten Bandung', 'Telkom University', 'SKPD Kab. Bandung', 'juli 2013', 'Nia Ambarsari, SSi, MT', 'Deden Witarsyah,                   Ayis Nurwita,                            Murahartawaty', NULL, NULL, NULL, NULL, 'Fantri Ika Putri, Mohamad Zulfikar, Ismi Sudiro, Woro Ninditarini', 4127050, 'proposal hard', 'valid', 'PT yang bersangkutan', NULL, NULL, NULL, NULL, 1),
(4, 'SISTEM INFORMASI', 'TS_2', 'PENGABDIAN MASYARAKAT DANA INTERNAL TAHUN 2014', 'Meningkatkan Keterampilan Guru dalam Pemanfaatan ICT', NULL, NULL, '06-07-1905', 'Murahartawaty', NULL, NULL, NULL, NULL, NULL, NULL, 5000000, 'proposal hard', 'valid', 'PT yang bersangkutan', NULL, NULL, NULL, NULL, 1),
(5, 'SISTEM INFORMASI', 'TS_2', 'PENGABDIAN MASYARAKAT DANA INTERNAL TAHUN 2014', 'PEMBUATAN SISTEM INFORMASI MANAJEMEN KOPERASI SIMPAN PINJAM TATALI WARGI DESA PADASUKA KECAMATAN CIMAHI TENGAH', 'Telkom University', 'Koperasi Simpan Pinjam Tatali Wargi Desa Padasuka Kecamatan Cimahi Tengah', '09-09-2014', 'Nia Ambarsari, SSi, MT', 'Dr. Luciana Andrawina \nAmelia Kurniawati, ST, MT \nMurahhartawaty, ST, MT\nUmar Yunan, KSH, ST, MT \nM. Teguh Kurniawan, ST, MT ', '', '', '', '', 'Rayinda Pramuditya Soesanto\nM. Ilham Fauzan \nM. Agung Agriza  \nBangkit Riawan', 7432500, 'proposal hard', 'valid', 'PT yang bersangkutan', NULL, NULL, NULL, NULL, 1),
(6, 'SISTEM INFORMASI', 'TS_2', 'PENGABDIAN MASYARAKAT DANA INTERNAL TAHUN 2014', 'PELATIHAN PEMBUATAN KERTAS SENI BERBAHAN BAKU LIMBAH MENDONG DI TASIKMALAYA', NULL, 'Masyarakat/remaja putus sekolah (20 orang) Desa Kamulyan, Kec. Manonjaya Tasikmalaya', '16/9/2014', 'Rd. Rohmat Saedudin, ST, MT', 'Rosad Ma''ali El Hadi, Ir., MPd, MT\nHaris Rahmat, ST, MT\nJudi Ahilman, Drs., MSIE\nF. Tatas Dwi Atmaji, ST, Meng', NULL, NULL, NULL, NULL, NULL, 10000000, 'proposal hard', 'valid', 'PT yang bersangkutan', NULL, NULL, NULL, NULL, 1),
(7, 'SISTEM INFORMASI', 'TS_2', 'PENGABDIAN MASYARAKAT DANA INTERNAL TAHUN 2014', 'Pendampingan Pengembangan Sistem Informasi Pengelolaan Magang di Lingkungan Pemerintah Kota Bandung', 'BKD Kota Bandung', 'Pemerintah Kota Bandung', '11-05-2014', 'Deden Witarsyah S.T., M.Eng', 'Warih Puspita sari S.Psi.,Mpsi\nFaishal Mufied Al-Anshary S.Kom., M.sc', NULL, NULL, NULL, NULL, '1. Ainu Faisal Pambudy\n2. I Made Prawira Indrawan\n3. Renantia Indriani', 9980000, 'proposal hard', 'valid', 'PT yang bersangkutan', NULL, NULL, NULL, NULL, 1),
(8, 'SISTEM INFORMASI', 'TS_2', 'PENGABDIAN MASYARAKAT DANA INTERNAL TAHUN 2014', 'PENDAMPINGAN PENYUSUNAN MASTER PLAN DASHBOARD E-GOVERNMENT KABUPATEN CIREBON', 'Badan Perencanaan Pembangunan Daerah Cirebon', 'Badan Perencanaan Pembangunan Daerah Cirebon', '26/11/2014', 'Yuli Adam Prasetyo', 'Rachmadita Andeswari, S. Kom., M. Kom.    Faishal Mufied Al-Anshary S.Kom., M.sc', NULL, NULL, NULL, NULL, 'Ardhyan Zulfikar Malik\nViky Hermana Pratama\nRatih Cintya Lestari', 9425500, 'proposal hard', 'valid', 'PT yang bersangkutan', NULL, NULL, NULL, NULL, 1),
(9, 'SISTEM INFORMASI', 'TS_2', 'PENGABDIAN MASYARAKAT DANA INTERNAL TAHUN 2014', 'SEKOLAH PENDAMPINGAN SMKN 1 LEMAHSUGIH PELATIHAN DASAR JADINGAN DAN DASAR PEMROGRAMAN UNTUK MENINGKATKAN KEMAMPUAN SISWA', 'SMKN 1 Lemahsugih', 'Guru dan siswa SMKN 1 Lemahsugih/55 Orang', '26/11/2014', 'Mochamad Teguh Kurniawan, ST., MT.', 'Murahartawaty S.T.M.T\nRachmadita Andeswari, S. Kom., M. Kom.\nFaishal Mufied Al-Anshary S.Kom., M.sc\nUmar Yunan, KSH, ST, MT', NULL, NULL, NULL, NULL, NULL, 10000000, 'proposal hard', 'valid', 'PT yang bersangkutan', NULL, NULL, NULL, NULL, 1),
(10, 'SISTEM INFORMASI', 'TS_1', 'PKM (PENGABDIAN KEPADA MASYARAKAT) PERIODE 1 DANA INTERNAL TAHUN 2015', 'PELATIHAN CUSTOMER RELATIONSHIP MANAGEMENT (CRM) DAN PEMANFAATAN APLIKASI OPENSOURCE UNTUK ANGGOTA KADIN KOTA BANDUNG', NULL, NULL, '31/3/2015', 'R. Wahjoe Witjaksono', 'Nia Ambarsari \nMardiyanto Wiyogo', NULL, NULL, NULL, NULL, 'Dika Anugerah Pratama \nFajrul Alfian \nRika Rihana Ilmar \nM. Denis Syahputra N. \nShinta Sindi Nuryani ', 9950000, 'proposal hard', 'valid', 'PT yang bersangkutan', NULL, NULL, NULL, NULL, 1),
(11, 'SISTEM INFORMASI', 'TS_1', 'PKM PERIODE 2 DANA INTERNAL TAHUN 2015', 'OPTIMALISASI PEMANFAATAN ENERGI BERBASIS TENAGA ANGIN UNTUK MENDUKUNG SENTRA PRODUKSI USAHA KECIL DI DESA SINDANG KERTA  KECAMATAN CIPATUJAH  KABUPATE', NULL, NULL, '07-07-1905', 'Rd. Rohmat Sedudin', 'Dida Diah Damayanti\nRosad Ma''ali A.\nHaris Racmat\nWiyono\nMuhamad Iqbal', NULL, NULL, NULL, NULL, NULL, 50000000, 'proposal hard', 'valid', 'PT yang bersangkutan', NULL, NULL, NULL, NULL, 1),
(12, 'SISTEM INFORMASI', 'TS_1', 'PKM (PENGABDIAN KEPADA MASYARAKAT) PERIODE 1 DANA INTERNAL TAHUN 2015', 'PELATIHAN PEMBUATAN RENCANA INDUK TEKNOLOGI INFORMASI DAN PEMANFAATAN APLIKASI MICROSOFT PROJECT UNTUK STAFF BADAN KEPEGAWAIAN DAERAH KOTA BANDUNG', NULL, NULL, '31/3/2015', 'Deden Witarsyah', 'Muhammad Azani Hasibuan\nDida Diah Damayanti\nFaishal Mufied Al Anshary', NULL, NULL, NULL, NULL, 'Eva Novianti \nI Komang Jaka Aksara W. \nAlima Indriati \nTimbul Prawira Gultom', 9950000, 'proposal hard', 'valid', 'PT yang bersangkutan', NULL, NULL, NULL, NULL, 1),
(13, 'SISTEM INFORMASI', 'TS_1', 'PKM (PENGABDIAN KEPADA MASYARAKAT) PERIODE 1 DANA INTERNAL TAHUN 2015', 'PELATIHAN TEKNOLOGI INFORMASI BAGI SISWA SEKOLAH DASAR DI DESA PAMALAYAN KECAMATAN CISEWU KABUPATEN GARUT', NULL, NULL, '31/3/2015', 'Irfan Darmawan ', 'Soni Fajar Surya Gumilang \nNia Ambarsari\nAlbi Fitransyah\nRachmadita Andreswari', NULL, NULL, NULL, NULL, NULL, 10000000, 'proposal hard', 'valid', 'PT yang bersangkutan', NULL, NULL, NULL, NULL, 1),
(14, 'SISTEM INFORMASI', 'TS_1', 'PDI', 'Pengembangan Leraning Management System (LMS) Berbasis Blog Wordpress dan Workshop Pengembangan Konten untuk Guru Tingkat Sekolah Menengah Atas', 'SMK N 4 Bandung dan SD Bintang Madani', 'Guru SMK N 4 Bandung dan SD Bintang Madani', '31/3/2015', 'Yuli Adam Prasetyo', 'Nur Ichsan Utama                  Luthfi Ramadani', NULL, NULL, NULL, NULL, NULL, 10000000, 'proposal hard', NULL, 'PT yang bersangkutan', NULL, NULL, NULL, NULL, 1),
(15, 'SISTEM INFORMASI', 'TS_1', 'PKM PERIODE 2 DANA INTERNAL TAHUN 2015', 'Pelatihan IT Literasi Bagi Perangkat Desa Untuk Meningkatkan Kualitas Pengelolaan Management SDN Sinar Mulya, Ds. Mangun Jaya, Agra Binta, Kab. Cianju', NULL, NULL, '30/6/2015', 'R. Rohmat Saedudin', 'Meldi Rendra\nRino Andias Nugraha\nJudi Alhilman\nTatang Mulyana', NULL, NULL, NULL, NULL, 'Septi Kurniawan \nBurhan Hendra \nFariz Nurrahman \nInda Putri Heni \nAnggi Lesmana', 10000000, 'proposal hard', 'valid', 'PT yang bersangkutan', NULL, NULL, NULL, NULL, 1),
(16, 'SISTEM INFORMASI', 'TS_1', 'PKM PERIODE 2 DANA INTERNAL TAHUN 2015', 'Pelatihan IT Literasi Bagi Perangkat Desa Untuk Meningkatkan Kualitas Pengelolaan Management SMPN 2 Agrabinta, Desa Mangun Jaya, Agra Binta, Kabupaten', NULL, NULL, '30/6/2015', 'Budi Sulistyo ', 'Soni Fajar Surya Gumilang \nBudi Santosa\nFransiskus Tatas Dwi Atmaji\nAri Yanuar Ridwan', NULL, NULL, NULL, NULL, 'Naufal Al Hadid \nIndra Hari Mukti\nRoni Komarul H. \nIntan Putri H. \nAlfattah Azis', 10000000, 'proposal hard', 'valid', 'PT yang bersangkutan', NULL, NULL, NULL, NULL, 1),
(17, 'SISTEM INFORMASI', 'TS_1', 'PKM PERIODE 2 DANA INTERNAL TAHUN 2015', 'PELATIHAN KOMPUTER BERBASIS KOMPETENSI DI BALAI LATIHAN TENAGA KERJA LUAR NEGERI (BLTKLN)', NULL, NULL, '30/6/2015', 'Soni Fajar Surya Gumilang ', 'Taufik Nur Adi\nMuhammad Azani Hasibuan\nTien Fabrianti Kusumasari\nIrfan Darmawan', NULL, NULL, NULL, NULL, 'Nazwar Syamsu \nMukhlis Anugrah Pratama \nNaufal Afra Firdaus \nAminnur Ikhsan \nGanang Afif Rijazim ', 10000000, 'proposal hard', 'valid', 'PT yang bersangkutan', NULL, NULL, NULL, NULL, 1),
(18, 'SISTEM INFORMASI', 'TS_1', 'PKM PERIODE 2 DANA INTERNAL TAHUN 2015', 'PERPUSTAKAAN DIGITAL SEBAGAI UPAYA MENINGKATKAN MINAT BACA SISWA SEJAK DINI DI SEKOLAH DASAR NEGERI SUKADANA II', NULL, NULL, '30/6/2015', 'M. Teguh Kurniawan ', 'Amelia Kurniawati \nLuciana Andrawina', NULL, NULL, NULL, NULL, 'Algadilan S. \nMukhlis Anugrah P.\nAtika Elysia \nAries Arieffandy S.\nM. Thariq Januar', 9990000, 'proposal hard', 'valid', 'PT yang bersangkutan', NULL, NULL, NULL, NULL, 1),
(19, 'SISTEM INFORMASI', 'TS_1', 'PKM PERIODE 2 DANA INTERNAL TAHUN 2015', 'Pelatihan peningkatan pengelolaan Logistik Bisnis Ritel dan  Penggunaan Aplikasi e-Ritel untuk Usaha Kecil dan Mengenah (UKM) se-Kabupaten Tasikmalaya', NULL, NULL, '30/6/2015', 'Budi Santosa ', 'Ari Yanuar Ridwan \nTeddy Syafrizal', NULL, NULL, NULL, NULL, 'Erlangga Bayu Setyawan \nNia Novitasari', 4475000, 'proposal hard', 'valid', 'PT yang bersangkutan', NULL, NULL, NULL, NULL, 1),
(20, 'SISTEM INFORMASI', 'TS', 'PDI', 'Pelatihan Teknologi Informasi Bagi Siswa Sekolah Dasar di Kota Tasikmalaya', 'Jl. R.E. Martadinata no.93B, Cipedes Kota Tasikmalaya', 'Siswa SD dan Guru Kecamatan Cipedes', 'mei 16', 'Irfan Darmawan ', 'Warih Puspitasari, Rachmadita Andreswari ', NULL, NULL, NULL, NULL, NULL, 9980000, 'proposal hard', NULL, 'PT yang bersangkutan', NULL, NULL, NULL, NULL, 1),
(21, 'SISTEM INFORMASI', 'TS', 'PKM PERIODE 3 DANA INTERNAL TAHUN 2015', 'Pelatihan E-Learning System di Pesantren Darussalam Garut', NULL, NULL, '26/10/2015', 'Riza Agustiansyah ', 'Faishal Mufied Al Anshary\nAdityas Widjajarto \nR. Wahjoe Witjaksono ', NULL, NULL, NULL, NULL, 'Sukrina Herman \nIka Puspitasari\nAyu Cahyani Febryanti \nMuhammad Firson Destadiawan \nGita Riesta ', 8050000, 'proposal hard', 'valid', 'PT yang bersangkutan', NULL, NULL, NULL, NULL, 1),
(22, 'SISTEM INFORMASI', 'TS', 'PKM PERIODE 3 DANA INTERNAL TAHUN 2015', 'SOSIALISASI PEMANFAATAN BIOGAS LIMBAH MENDONG DAN PUPUK CAIR ORGANIK HASIL PROSES BIOGAS PADA WARGA DESA KAMULYAN KELURAHAN MANONJAYA KABUPATEN TASIKM', NULL, NULL, '26/10/2015', 'Rosad Ma''ali El Hadi ', 'Litasari Widyastuti Suwarsono \nAulia Fashanah Hadining\nNurdinintya Athari Supratman \nTien Fabrianti Kusumasari', NULL, NULL, NULL, NULL, 'Dhiah Arini \nDwiska Aini Nurrahma \nDamanhuri Nurul Huda\nMuhammad Habiburrohman \nAstri Desiana', 9000000, 'proposal hard', 'valid', 'PT yang bersangkutan', NULL, NULL, NULL, NULL, 1),
(23, 'SISTEM INFORMASI', 'TS', 'PERIODE 1 DANA INTERNAL 2016, PKM REGULER', 'e-RAPOR (SISTEM APLIKASI RAPOR) UNTUK MEMBANTU PARA GURU DALAM PENGISIAN LAPORAN AKADEMIK SISWA DI SEKOLAH DASAR NEGERI SUKADANA II', NULL, 'SDN Sukadana II', '30/3/2016', 'M. Teguh Kurniawan ', 'Murahartawaty, Adityas Widjajarto', NULL, NULL, NULL, NULL, 'Ibnu Caesar, Febryan Elfanuary Azie, Isnaini Hayati, Jihan Herdiyanti Syafira, Nolanda Indria', 10000000, 'proposal hard', 'valid', 'PT yang bersangkutan', NULL, NULL, NULL, NULL, 1),
(24, 'SISTEM INFORMASI', 'TS', 'PERIODE 1 DANA INTERNAL 2016, PKM REGULER', 'Pelatihan Pengelolaan Sampah Terpadu Untuk Masyarakat Desa Sukapura', NULL, 'Masyarakat Desa Sukapura Kecamatan Dayeuh Kolot', '30/3/2016', 'Ari Yanuar Ridwan ', 'Faishal Mufied Al Anshary, Teddy Syafrizal, Rosad Ma''ali El Hadi, Ir.,M.Pd.,MT', NULL, NULL, NULL, NULL, 'M Hilmy Syahrul Ramadhan, Wira Majid Pambudi   ', 8500000, 'proposal hard', 'valid', 'PT yang bersangkutan', NULL, NULL, NULL, NULL, 1),
(25, 'SISTEM INFORMASI', 'TS', 'PERIODE 1 DANA INTERNAL 2016, PKM REGULER', 'Pelatihan Penggunaan Microsoft Power Point untuk Pembuatan Materi E-Learning di Pesantren Darussalam Garut', NULL, 'Pesantren Darussalam Garut', '30/3/2016', 'Riza Agustiansyah ', 'Muhammad Azani Hasibuan, Albi Fitransyah, Faishal Mufied Al-Anshary S.Kom.,M.Kom.,M.Sc', NULL, NULL, NULL, NULL, 'Wisnu Adi Pratama, Nur Aulia Faridiyah Rafika Sari, Mhd. Yuanda Hanafi Lubis, Munifah Siti Fadhilah', 10000000, 'proposal hard', 'valid', 'PT yang bersangkutan', NULL, NULL, NULL, NULL, 1),
(26, 'SISTEM INFORMASI', 'TS', 'PERIODE 1 DANA INTERNAL 2016, PKM REGULER', 'Pemasangan Data Logger pada PLTS dan PLTMH di Kampung Buligir, Desa Parentas, Cigalontang, Kab. Tasikmalaya', 'Kampung Buligir, Desa Parentas, Cigalontang, Kab. Tasikmalaya', 'tokoh masyarakat, Ketua RT/RW, Kepala Diknas dan warga di sekitar lokasi (10 orang)', '30/3/2016', 'Tatang Mulyana ', 'Agus Kusnayat, Meldi Rendra, Ekki Kurniawan, Rd. Rohmat Saedudin ', NULL, NULL, NULL, NULL, 'Mohammad Tajudin, Muhammad Faris Izzuddin, Muhammad Ichsan Nurliyan', 8500000, 'proposal hard', 'valid', 'PT yang bersangkutan', NULL, NULL, NULL, NULL, 1),
(27, 'SISTEM INFORMASI', 'TS', 'PERIODE 1 DANA INTERNAL 2016, PKM REGULER', 'Pendampingan Program Desa Mandiri dengan IT dalam Mewujudkan Desa Peradaban di Jawa Barat', NULL, 'YAYASAN AL-HIKMAH KAUM SELATAN', '30/3/2016', 'Rd. Rohmat Saedudin ', 'Dida Diah Damayanti, Murahartawaty', NULL, NULL, NULL, NULL, 'Revi Fahreza Al Hazmi, Arief Rachman Hartadi, Ilham Farobi, Alfian Hidayat', 10000000, 'proposal hard', 'valid', 'PT yang bersangkutan', NULL, NULL, NULL, NULL, 1),
(28, 'SISTEM INFORMASI', 'TS', 'PERIODE 1 DANA INTERNAL 2016, PKM REGULER', 'PENINGKATAN PEMANFAATAN PERPUSTAKAAN DIGITAL DI SEKOLAH DASAR NEGERI SUKADANA II', NULL, 'SD Sukadana II Kec, Malausma Majalengka Jawa Barat', '30/3/2016', 'Luciana Andrawina ', 'Amelia Kurniawati, Umar Yunan Kurnia Septo Hediyanto', NULL, NULL, NULL, NULL, 'Ana Meilani Puspitasari, Risna Purwita Siwi, Nolanda Indria', 10000000, 'proposal hard', 'valid', 'PT yang bersangkutan', NULL, NULL, NULL, NULL, 1),
(29, 'SISTEM INFORMASI', 'TS', 'PERIODE 1 DANA INTERNAL 2016, PKM REGULER', 'Smart School - PENINGKATAN KNOWLEDGE SHARING ANTARA SEKOLAH BANDUNG RAYA (Kota Bandung, Kabupaten Bandung, KBB, Cimahi) DENGAN PEMBERDAYAAN KOMUNITAS ', '', 'KARISMA Salman ITB', '28/4/2016', 'Rio Aurachman ', 'Murni Dwi Astuti, Luthfi Ramadani', '', '', '', '', 'Jauhari Habibie ', 9000000, 'proposal hard', 'valid', 'PT yang bersangkutan', NULL, NULL, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_dosen`
--

CREATE TABLE `tb_dosen` (
  `no` int(11) NOT NULL,
  `nip` varchar(20) NOT NULL,
  `nama_dosen` varchar(100) NOT NULL,
  `tempat_lahir` varchar(100) DEFAULT NULL,
  `tanggal_lahir` varchar(100) DEFAULT NULL,
  `pendidikan_terakhir` varchar(100) DEFAULT NULL,
  `jabatan_akademik` varchar(100) DEFAULT NULL,
  `keterangan` varchar(100) DEFAULT NULL,
  `program_studi` varchar(100) NOT NULL,
  `bidang_keahlian_pdd` varchar(200) DEFAULT NULL,
  `status_dosen` varchar(100) DEFAULT NULL,
  `id_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_dosen`
--

INSERT INTO `tb_dosen` (`no`, `nip`, `nama_dosen`, `tempat_lahir`, `tanggal_lahir`, `pendidikan_terakhir`, `jabatan_akademik`, `keterangan`, `program_studi`, `bidang_keahlian_pdd`, `status_dosen`, `id_user`) VALUES
(1, '13711174-3', 'Adityas Widjajarto, S.T., M.T.', 'Malang', '10 Desember1971', 'Institut Teknologi Bandung', 'Tenaga Pengajar', '410127103', 'Sistem Informasi (FRI)', 'Teknik Elektro', 'Dosen_PS', 1),
(2, '16611949-3', 'Ahmad Musnansyah, Ir., MS', 'Cimahi', '21 Mei 1961', 'New Mexico State University', 'Tenaga Pengajar', '9904013879', 'Sistem Informasi (FRI)', 'Computer Science', 'Dosen_PS', 1),
(3, '13841204-3', 'ALBI FITRANSYAH, S.Si., M.T.', 'Bandung', '09 Juli 1984', 'Institut Teknologi Bandung', 'Tenaga Pengajar', '409078404', 'Sistem Informasi (FRI)', 'Informatika', 'Dosen_PS', 1),
(4, '14661272-1', 'Ari Fajar Santoso, IR, M.T.', 'Surakarta', '28 Desember 1966', 'Institut Teknologi Bandung', 'Asisten Ahli', '428126602', 'Sistem Informasi (FRI)', 'Informatika', 'Dosen_PS', 1),
(5, '16881899-3', 'Asti Amalia, B.MM., M.Sc', 'Bandung', '13 Maret 1988', 'Universiti Teknologi Malaysia', 'Tenaga Pengajar', '413038802', 'Sistem Informasi (FRI)', 'Teknologi', 'Dosen_PS', 1),
(6, '11760751-3', 'Dr. BASUKI RAHMAD CISA, CISM, CRISC', 'Kediri', '08 Maret 1976', 'Institut Teknologi Bandung', 'Tenaga Pengajar', '408037602', 'Sistem Informasi (FRI)', 'Teknik Elektro dan Informatika', 'Dosen_PS', 1),
(7, '14721282-1', 'DEDEN WITARSYAH, ST., M.ENG', 'Kota Bumi', '06 Juli 1972', 'Curtin University of Technology', 'Asisten Ahli', '206077202', 'Sistem Informasi (FRI)', 'Electrical and Computer Engineering', 'Dosen_PS', 1),
(8, '14901493-1', 'FAISHAL MUFIED AL ANSHARY, S.Kom., M.Kom., M.Sc', 'Bandung', '27 Desember 1990', 'Asian Institute of Technology', 'Tenaga Pengajar', '427129001', 'Sistem Informasi (FRI)', 'Information Management', 'Dosen_PS', 1),
(9, '14751398-1', 'Dr. IRFAN DARMAWAN, ST., M.T.', 'Tasikmalaya', '17 Agustus 1975', 'Institut Teknologi Bandung', 'Lektor Kepala', '410087501', 'Sistem Informasi (FRI)', 'Teknik Elektro dan Informatika', 'Dosen_PS', 1),
(10, '12631100-4', 'Ir. LUKMAN ABDURRAHMAN, MIS', 'Bandung', '06 Februari 1963', 'Claremont Graduate University', 'Tenaga Pengajar', '406026305', 'Sistem Informasi (FRI)', 'Information Systems', 'Dosen_PS', 1),
(11, '15891700-1', 'LUTHFI RAMADANI', 'Padang Panjang', '7 April 1989', 'Institut Teknologi Bandung', 'Tenaga Pengajar', '407048901', 'Sistem Informasi (FRI)', 'Informatika', 'Dosen_PS', 1),
(12, '13861140-1', 'M. TEGUH KURNIAWAN S.T., M.T.', 'Sukadana', '11  November  1986', 'Institut Teknologi Telkom', 'Asisten Ahli', '411118601', 'Sistem Informasi (FRI)', 'Teknik Informatika', 'Dosen_PS', 1),
(13, '14851400-1', 'MUHAMMAD AZANI HASIBUAN, S.Kom., M.T.I', 'Pekanbaru', '22 Juli 1985', 'Universitas Indonesia', 'Asisten Ahli', '422078501', 'Sistem Informasi (FRI)', 'Teknologi Informasi', 'Dosen_PS', 1),
(14, '05820324-1', 'MURAHARTAWATY, S.T., M.T.', 'Makassar', '23 Oktober 1982', 'Institut Teknologi Bandung', 'Asisten Ahli', '423108201', 'Sistem Informasi (FRI)', 'Informatika', 'Dosen_PS', 1),
(15, '14771516-1', 'NIA AMBARSARI, S.Si., M.T.', 'Majalengka', '27 Februari 1977', 'Institut Teknologi Bandung', 'Asisten Ahli', '427027701', 'Sistem Informasi (FRI)', 'Informatika', 'Dosen_PS', 1),
(16, '15831704-1', 'NUR ICHSAN UTAMA', 'Bandung ', '20 Agustus 1983', 'Institut Teknologi Bandung', 'Tenaga Pengajar', '420088302', 'Sistem Informasi (FRI)', 'Teknik Elektro', 'Dosen_PS', 1),
(17, '14891325-1', 'RACHMADITA ANDRESWARI, S.Kom., M.Kom', 'Jombang', '04 September 1989', 'Institut Teknologi Sepuluh Nopember', 'Asisten Ahli', '404098901', 'Sistem Informasi (FRI)', 'Sistem Informasi', 'Dosen_PS', 1),
(18, '10780752-3', 'RAHMAT MULYANA, S.T., M.T., M.B.A., PMP, CISA, CISM', 'Ciamis', '11 Oktober 1978', 'Institut Teknologi Bandung', 'Tenaga Pengajar', '411107802', 'Sistem Informasi (FRI)', 'Informatika', 'Dosen_PS', 1),
(19, '00760202-1', 'RD. ROHMAT SAEDUDIN, S.T., M.T.', 'Tasikmalaya', '28 April 1976', 'Institut Teknologi Bandung', 'Lektor', '428047601', 'Sistem Informasi (FRI)', 'Teknik Elektro', 'Dosen_PS', 1),
(20, '10760753-3', 'RIDHA HANAFI, S.T., M.T.', 'Bandung', '18 Februari 1976', 'Institut Teknologi Bandung', 'Tenaga Pengajar', '418027602', 'Sistem Informasi (FRI)', 'Teknik Elektro', 'Dosen_PS', 1),
(21, '08760468-1', 'RIZA AGUSTIANSYAH, S.T., M.Kom', 'Palembang', '27 Agustus 1976', 'Universitas Indonesia', 'Asisten Ahli', '327087604', 'Sistem Informasi (FRI)', 'Ilmu Komputer', 'Dosen_PS', 1),
(22, '10780592-1', 'SENO ADI PUTRA, S.Si., M.T.', 'Bandung', '12 Oktober 1978', 'Institut Teknologi Bandung', 'Asisten Ahli', '412107802', 'Sistem Informasi (FRI)', 'Teknik Elektro', 'Dosen_PS', 1),
(23, '14751603-3', 'SONI FAJAR SURYA GUMILANG, S.T., M.T.', 'Majalengka', '23 Agustus 1975', 'Institut Teknologi Bandung', 'Lektor', '423087502', 'Sistem Informasi (FRI)', 'Informatika', 'Dosen_PS', 1),
(24, '14831337-1', 'TAUFIK NUR ADI, S.Kom., M.T.', 'Bandung', '24 Juli 1983', 'Institut Teknologi Bandung', 'Tenaga Pengajar', '424078301', 'Sistem Informasi (FRI)', 'Informatika', 'Dosen_PS', 1),
(25, '14791411-1', 'Dr. TIEN FABRIANTI KUSUMASARI, S.T., M.T.', 'Wonogiri', '19 Februari 1979', 'Institut Teknologi Bandung', 'Asisten Ahli', '419027904', 'Sistem Informasi (FRI)', 'Teknik Elektro dan Informatika', 'Dosen_PS', 1),
(26, '14841340-1', 'UMAR YUNAN KURNIA SEPTO HEDIYANTO, S.T., M.T.', 'Pangkalan Susu', '12 September 1984', 'Institut Teknologi Bandung', 'Tenaga Pengajar', '412098401', 'Sistem Informasi (FRI)', 'Teknik Elektro', 'Dosen_PS', 1),
(27, '10790604-1', 'YULI ADAM PRASETYO, S.T., M.T.', 'Malang', '19 Mei 1979', 'Institut Teknologi Bandung', 'Lektor', '419057903', 'Sistem Informasi (FRI)', 'Teknik Elektro', 'Dosen_PS', 1),
(28, '14691518-1', 'R. WAHJOE WITJAKSONO, S.T., M.M.', 'Bandung', '21 Februari 1969', 'Sekolah Tinggi Ilmu Ekonomi IPWI', 'Tenaga Pengajar', '421026904', 'Sistem Informasi (FRI)', 'Manajemen', 'Dosen_PS', 1),
(29, '10850703-3', 'ANDIKA BAYU H., S.T. ,M.T.', 'Probolinggo', '05 Desember 1985', 'Institut Teknologi Bandung', 'Tenaga Pengajar', '405128502', 'Sistem Informasi (FRI)', 'Teknik dan Manajemen Industri', 'Dosen_Non_PS', 1),
(30, '02630289-1', 'CHRISTANTO TRIWIBISONO, Ir., M.M.', 'Bandung', '15 Juli 1963', 'Institut Teknologi Bandung', 'Asisten Ahli', '315076302', 'Sistem Informasi (FRI)', 'MBA Teknologi', 'Dosen_Non_PS', 1),
(31, '92670068-1', 'Ir. FARDA HASUN, M.Sc.', 'Yogyakarta', '12 Juli 1967', 'Anglia Polytechnic University', 'Lektor', '412076702', 'Sistem Informasi (FRI)', 'Telecommunications System Management', 'Dosen_Non_PS', 1),
(32, '10860704-3', 'FERDIAN, S.T., M.T.', 'Jakarta', '11 Februari 1986', 'Institut Teknologi Bandung', 'Tenaga Pengajar', '411028602', 'Sistem Informasi (FRI)', 'Teknik dan Manajemen Industri', 'Dosen_Non_PS', 1),
(33, '14561582-1', 'Dr. HUSNI AMANI', 'Palembang', '05 Maret 1956', 'Universitas Padjadjaran ', 'Lektor', '9904003161', 'Sistem Informasi (FRI)', 'Ilmu Ekonomi', 'Dosen_Non_PS', 1),
(34, '10740605-1', 'ILHAM PERDANA', 'Bandung', '6 Desember 1974', 'Institut Teknologi Bandung', 'Asisten Ahli', '6127401', 'Sistem Informasi (FRI)', 'Teknik dan Manajemen Industri', 'Dosen_Non_PS', 1),
(35, '03760308-1', 'LITASARI WIDYASTUTI SUWARSONO, S.Psi', 'Sukabumi', '18 Agustus 1976', 'Universitas Padjadjaran', 'Tenaga Pengajar', '418087603', 'Sistem Informasi (FRI)', 'Psikologi Profesi', 'Dosen_Non_PS', 1),
(36, '14841416-1', 'MARIA DELLAROSAWATI IDAWICAKSAKTI, S.T., M.B.A.', 'Bandung', '13 September 1984', 'Institut Teknologi Bandung', 'Tenaga Pengajar', '413098404', 'Sistem Informasi (FRI)', NULL, 'Dosen_Non_PS', 1),
(37, '14871575-1', 'MURNI DWI ASTUTI, S.T., M.T.', 'Belitang', '29 Oktober 1987', 'Institut Teknologi Bandung', 'Tenaga Pengajar', '429108701', 'Sistem Informasi (FRI)', 'Teknik dan Manajemen Industri', 'Dosen_Non_PS', 1),
(38, '15841678-3', 'NOPENDRI, S.Si., M.Si', 'Pasar Kembang', '17 November 1984', 'Institut Teknologi Bandung', 'Tenaga Pengajar', '417118406', 'Sistem Informasi (FRI)', 'Matematika', 'Dosen_Non_PS', 1),
(39, '15881711-1', 'SARI WULANDARI, S.T., M.T.', 'Bogor', '24 Februari 1988', 'Institut Teknologi Bandung', 'Asisten Ahli', '424028802', 'Sistem Informasi (FRI)', 'Teknik dan Manajemen Industri', 'Dosen_Non_PS', 1),
(40, '10820582-1', 'WARIH PUSPITASARI, S.Psi., M.Psi', 'Semarang ', '27 Januari 1982', 'Universitas Surabaya', 'Tenaga Pengajar', '427018204', 'Sistem Informasi (FRI)', 'Psikologi', 'Dosen_Non_PS', 1),
(41, '92660057-1', 'Dr. Ir. YATI ROHAYATI, M.T.', 'Tasikmalaya', '27 Desember 1966', 'Universitas Indonesia', 'Lektor', '427126601', 'Sistem Informasi (FRI)', 'Ilmu Manajemen', 'Dosen_Non_PS', 1),
(42, '14781476-1', 'ARI YANUAR RIDWAN, S.T., M.T.', 'TASIKMALAYA', '21 Januari 1978', NULL, NULL, '0428017801', 'Teknik Industri (FRI)', NULL, 'Dosen_PS', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_haki`
--

CREATE TABLE `tb_haki` (
  `id` int(20) NOT NULL,
  `karya` varchar(100) DEFAULT NULL,
  `jenis_haki` varchar(200) DEFAULT NULL,
  `tahun` varchar(200) DEFAULT NULL,
  `file_name` varchar(100) DEFAULT NULL,
  `file_type` varchar(100) DEFAULT NULL,
  `file_date` varchar(100) DEFAULT NULL,
  `id_user` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_haki`
--

INSERT INTO `tb_haki` (`id`, `karya`, `jenis_haki`, `tahun`, `file_name`, `file_type`, `file_date`, `id_user`) VALUES
(1, 'Mini Green Power Plant', NULL, '15 Juni 2015', NULL, NULL, NULL, '1'),
(2, 'Aplikasi Praktikum Online INDIGOLABS', NULL, '15 Juni 2015', NULL, NULL, NULL, '1'),
(3, 'Website PASARAMAI.COM', NULL, '15 Juni 2015', NULL, NULL, NULL, '1'),
(4, 'aa', 'Hak Paten', '2013-02-21', NULL, NULL, NULL, '1'),
(5, NULL, NULL, NULL, NULL, NULL, NULL, '1');

-- --------------------------------------------------------

--
-- Table structure for table `tb_kerjasama`
--

CREATE TABLE `tb_kerjasama` (
  `id` int(11) NOT NULL,
  `nama_instansi` varchar(225) DEFAULT NULL,
  `jenis_kegiatan` varchar(225) DEFAULT NULL,
  `mulai_kerjasama` varchar(200) DEFAULT NULL,
  `akhir_kerjasama` varchar(225) DEFAULT NULL,
  `instasi_negeri` varchar(225) DEFAULT NULL,
  `keterangan` varchar(225) DEFAULT NULL,
  `manfaat` varchar(1000) DEFAULT NULL,
  `file_name` varchar(300) DEFAULT NULL,
  `file_type` varchar(100) DEFAULT NULL,
  `file_date` datetime DEFAULT NULL,
  `id_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_kerjasama`
--

INSERT INTO `tb_kerjasama` (`id`, `nama_instansi`, `jenis_kegiatan`, `mulai_kerjasama`, `akhir_kerjasama`, `instasi_negeri`, `keterangan`, `manfaat`, `file_name`, `file_type`, `file_date`, `id_user`) VALUES
(1, 'CV Bakti Kuntari ', 'Kegiatan Mahasiswa dalam bidang Penelitian dan Pengembangan Keprofesian', '21 Oktober 2014', '21 Oktober 2017', 'Dalam Negeri', 'Prodi', 'Meningkatkan mutu pendidikan di Fakultas Rekayasa Industri Telkom University dengan melibatkan mahasiswa untuk secara langsung mengambil kasus pada industri CV Bakti Kuntari sebagai user dalam bidang penelitian.', NULL, NULL, NULL, 1),
(2, 'CV Nenggala ', 'Kerjasama kegiatan mahasiswa dalam pelaksanaan kuliah kerja lapangan dan magang kerja', '10 Desember 2014', '10 Desember 2017', 'Dalam Negeri', 'Prodi', 'Meningkatkan mutu pendidikan di Fakultas Rekayasa Industri Telkom University bersama CV Nenggala dengan berorientasi kepada user secara berkesinambungan', NULL, NULL, NULL, 1),
(3, 'CV Mendong K. Craft', 'Kerjasama bidang Kegiatan Pengabdian Masyarakat', '13 Oktober 2014', '13 Oktober 2017', 'Dalam Negeri', 'Prodi', 'Meningkatkan mutu pendidikan di Fakultas Rekayasa Industri Telkom University bersama CV Mendong K. Craft  dengan berorientasi kepada user secara berkesinambungan', NULL, NULL, NULL, 1),
(4, 'Project Management Institute Indonesia Chapter', 'Kerjasama untuk mendukung kegiatan Tri Dharma Perguruan Tinggi dan Keprofesian', '09 September 2014', '09 September 2018', 'Dalam Negeri', 'Prodi', 'Meningkatkan keanggotaan asosiasi PMI Global PMIIC serta pemegang sertifikat PMP/CAPM, dan meningkatkan kualitas SDM di bidang Manajemen Proyek', NULL, NULL, NULL, 1),
(5, 'CV Trapawana ', 'kegiatan mahasiswa di bidang pelaksanaan kokurikuler dan ekstra kurikuler', '10 Desember 2014', '10 Desember 2017', 'Dalam Negeri', 'Prodi', 'Meningkatkan mutu pendidikan di Fakultas Rekayasa Industri Telkom University bersama CV Trapawana dengan berorientasi kepada user secara berkesinambungan', NULL, NULL, NULL, 1),
(6, 'PT. Swasembada Media Bisnis ', 'Riset dan Penjurian dengan tema "Indonesia Best eMark Award 2016"', '07 April 2016', '31 September 2016', 'Dalam Negeri', 'Prodi', 'Mendapatkan hasil tahapan riset dan assesmen  dari Indoneisa Best eMark Award 2016 dalam bentuk laporan akhir berupa hardcopy dan softcopy', NULL, NULL, NULL, 1),
(7, 'Kementrian Riset dan Teknologi ', 'Pengembangan Kapasitas Sumber Daya IPTEK', '29 Maret 2014', '29 Maret 2017', 'Dalam Negeri', 'Prodi', 'Menunjang pelaksanaan tugas belajar mengajar dalam menghasilkan sumber daya manusia di Fakultas Rekayasa Industri yang berkualitas dengan  menyelenggarakan kegiatan riset bersama', NULL, NULL, NULL, 1),
(8, 'PT. Telekomunikasi Indonesia, Tbk ', 'Kerjasama Penyediaan dan Pengembangan SDM', '29 Maret 2014', '29 Maret 2017', 'Dalam Negeri', 'Prodi', 'Menciptakan sinergi dalam memenuhi kebutuhan pengembangan sumber daya manusia di Fakultas Rekayasa Industri', NULL, NULL, NULL, 1),
(9, 'Pemerintah Kota Bandung ', 'Pengembangan di Bidang Teknologi Informasi Komunikasi', '29 Maret 2014', '28 Maret 2017', 'Dalam Negeri', 'Prodi', 'Pendayagunaan potensi mahasiswa dan dosen Fakultas Rakayasa Industri melalui program/kegiatan konsultasi teknis, penelitian dan/atau pengkajian yang berkaitan dengan bidang ICT.\n- Pelaksanaan program praktek kerja lapangan (PKL)/magang kerja/KKNbagi mahasiswa Fakultas Rekayasa Industri di lingkungan Pemerintahan Kota.', NULL, NULL, NULL, 1),
(10, 'Pemerintah Provinsi Sumatera Selatan ', 'Kerjasama di Bidang Pendidikan, Pelatihan, dan Pembangunan Sumatera Selatan', '29 Maret 2014', '29 Maret 2017', 'Dalam Negeri', 'Prodi', 'Pelaksanaan program kerja praktek, riset, beasiswa, kerja industry, penyerapan industry, pemanfaatan sarana prasarana/infrastruktur dan pengabdian masyarakat bagi mahasiswa dan dosen Fakultas Rekayasa Industri.', NULL, NULL, NULL, 1),
(11, 'PT. POS Indonesia ', 'Kerjasama dalam bidang Pengembangan Teknologi Informasi', '14 November 2014', '14 November 2016', 'Dalam Negeri', 'Prodi', 'Pelaksanaan program penelitian dan atau pengabdian kepada masyarakat\n- Pemanfaatan layanan teknologi informasi\n- Pengembangan sumber daya yang dimiliki\n', NULL, NULL, NULL, 1),
(12, 'Universitas Udayana ', 'Kerjasama Pengembangan Institusi', '23 Oktober 2014', '23 Oktober 2019', 'Dalam Negeri', 'Prodi', 'Pembinaan dan pengembangan program studi\n- Pembinaan dan pengembangan Dosen dan Tenaga Kependidikan\n- Kegiatan bersana di bidang incubator bisnis, hasil riset dan inovasi\n- Knowledge sharing untuk bidang/topik penelitian tertentu\n- Kegiatan bersama di bidang pertukaran dosen dan mahasiswa\n- Kegiatan bersama di bidang akademik dan kemahasiswaan\n- Konferensi atau seminar dalam rangka diseminasi hasil penelitian\n', NULL, NULL, NULL, 1),
(13, 'PT. Murtila Promosindo ', 'Kerjasama Building Image', '13 Februari 2015', '13 Februari 2019', 'Dalam Negeri', 'Prodi', 'Pembuatan buku dan iklan satu tahun Telkom University', NULL, NULL, NULL, 1),
(14, 'Pemerintah Provinsi Jawa Barat ', 'Kerjasama di Bidang Pendidikan, Pelatihan, dan Pembangunan di Jawa Barat', '2 Januari 2015', '2 Januari 2016', 'Dalam Negeri', 'Prodi', 'Peningkatan kemampuan di bidang akademik untuk mahasiswa dan dosen di lingkungan Fakultas Rekayasa Industri', NULL, NULL, NULL, 1),
(15, 'PT. Industri Telekomunikasi Indonesia ', 'Kerjasama di Bidang Pengembangan Institusi', '28 Maret 2015', '28 Maret 2018', 'Dalam Negeri', 'Prodi', 'Menunjang pengembangan institusi dalam bentuk kerjasama pengembangan sumber daya manusia', NULL, NULL, NULL, 1),
(16, 'PT. Phimatic Otomasi Indonesia ', 'Kerjasama Kelembagaan', '1 Maret 2015', '31 Maret 2020', 'Dalam Negeri', 'Prodi', 'Pengembangan sumber daya manusia di Fakultas Rekayasa Industri, penelitian dana tau pengabdian kepada masyarakat\n- Layanan keahlian praktis oleh dosen tamu yang berasal dari dunia usaha\n- Pemberian beasiswa atau bantuan biaya pendidikan dana tau bantuan lain yang dianggap perlu \n', NULL, NULL, NULL, 1),
(17, 'PT. Christalenta Pratama ', 'Kerjasama Kelembagaan', '25 Maret 2015', '25 April 2019', 'Dalam Negeri', 'Prodi', 'Pengembangan sumber daya manusia di Fakultas Rekayasa Industri, penelitian dana tau pengabdian kepada masyarakat\n- Layanan keahlian praktis oleh dosen tamu yang berasal dari dunia usaha\n- Pemberian beasiswa atau bantuan biaya pendidikan dana tau bantuan lain yang dianggap perlu\n', NULL, NULL, NULL, 1),
(18, 'Badan Penyelenggara Jaminan Sosial Kesehatan ', 'Kerjasama Kelembagaan dalam Mendukung Peningkatan Kualitas Penyelenggaraan Program Jaminan Kesehatan', '1 Juni 2015', '30 Mei 2018', 'Dalam Negeri', 'Prodi', 'Pelaksanaan program praktek kerja lapangan (PKL)/magang kerja/KKN, penelitian, pengabdian masyarakat dan Tugas Akhir bagi mahasiswa Fakultas Rekayasa Industri Telkom University di BPJS', NULL, NULL, NULL, 1),
(19, 'PT. Mediatrac Sistem Komunikasi', 'Kerjasama di Bidang Pendidikan dan Penelitian', '20 April 2015', '20 April 2018', 'Dalam Negeri', 'Prodi', 'Pelaksanaan kerja praktek, penelitian, magang, penyerapan industry, pemanfaatan sarana prasarana/infrastruktur dan pengabdian masyarakat bagi mahasiswa dan dosen di Fakultas Rekayasa Industri untuk meningkatkan kemampuan di bidang akademik', NULL, NULL, NULL, 1),
(20, 'Fakultas Kedokteran Gigi Universitas Padjadjaran ', 'Kerjasama Kelembagaan', '7 Agustus 2015', '7 Agustus 2019', 'Dalam Negeri', 'Prodi', ' Pengembangan sumber daya manusia di Fakultas Rekayasa Industri, penelitian dan atau pengabdian kepada masyarakat\n- Layanan keahlian praktis oleh dosen tamu yang berasal dari dunia usaha\n- Pemberian beasiswa atau bantuan biaya pendidikan dana tau bantuan lain yang dianggap perlu\n', NULL, NULL, NULL, 1),
(21, 'Pemerintah Daerah Kabupaten Kutai Kartanegara ', 'Peningkatan dan Pengembangan Potensi Sumber Daya Daerah Kabupaten Kutai Kartanegara melalui Kerjasama Bidang Pendidikan, Penelitian dan Pengabdian kepada Masyarakat', '13 Juli  2015', '13 Juli  2018', 'Dalam Negeri', 'Prodi', 'Pertukaran informasi yang ditujukan untuk penyelenggaraan penelitian, seminar, lokakarya, dan pengabdian kepada masyarakat.', NULL, NULL, NULL, 1),
(22, 'Pemerintah Daerah Kabupaten Kutai Kartanegara ', 'Beasiswa Program Kerjasama Gerbang Raja Kabupaten Kutai Kartanegara', '27 Agustus 2015', '27 Agustus 2019', 'Dalam Negeri', 'Prodi', 'Menerima dana untuk pembiayaan program beasiswa kerjasama sesuai jadwal dan besaran yang telah di sepakati', NULL, NULL, NULL, 1),
(23, 'Pemerintah Kabupaten Bantul ', 'Peningkatan dan Pengembangan Potensi Sumber Daya Daerah Kabupaten Bantul melalui Kerjasama Bidang Pendidikan, Penelitian dan Pengabdian kepada Masyarakat', '28 Mei 2015', '28 Mei 2018', 'Dalam Negeri', 'Prodi', 'Pengembangan sumber daya manusia di Fakultas Rekayasa Industri, penelitian dan atau pengabdian kepada masyarakat ', NULL, NULL, NULL, 1),
(24, 'Pemerintah Kabupaten Bantul ', 'Pelaksanaan Program Geladi bagi Mahasiswa Universitas Telkom Bandung di Kabupaten Bantul', '29 Mei 2015', '29 Mei 2018', 'Dalam Negeri', 'Prodi', 'Pelaksanaan program praktek kerja lapangan (PKL)/magang kerja/KKN, penelitian, pengabdian masyarakat dan Tugas Akhir bagi mahasiswa Fakultas Rekayasa Industri  Telkom University di Pemerintah Kabupaten Bantul', NULL, NULL, NULL, 1),
(25, 'Pusat Pengembangan dan Pemberdayaan Pendidik dan Tenaga Kependidikan Bidang Mesin dan Teknik Industri', 'Kerjasama Kelembagaan', '5 Oktober 2015', '5 Oktober 2019', 'Dalam Negeri', 'Prodi', 'Pengembangan sumber daya manusia di Fakultas Rekayasa Industri, penelitian dan atau pengabdian kepada masyarakat\n- Layanan keahlian praktis oleh dosen tamu yang berasal dari dunia usaha\n- Pemberian beasiswa atau bantuan biaya pendidikan dan  atau bantuan lain yang dianggap perlu\n', NULL, NULL, NULL, 1),
(26, 'Institut Teknologi Bandung', 'Kerjasama Pengembangan Institusi', '13 Oktober 2015', '13 Oktober 2020', 'Dalam Negeri', 'Prodi', ' Pembinaan dan pengembangan program studi\n- Pembinaan dan pengembangan dosen dan tenaga kependidikan\n- Kegiatan bersama di bidang incubator bisnis, hasil riset dan inovasi\n- Knowledge sharing untuk bidang/topik penelitian tertentu\n- Kegiatan bersama di bidang pertukaran dosen, mahasiswa dan karyawan\n- Kegiatan bersama di bidang akademik dan kemahasiswaan\n', NULL, NULL, NULL, 1),
(27, 'PT. Aero Systems Indonesia - Garuda Group ', 'Kerjasama Penyaluran Lulusan dan Program Magang', '16 Desember 2015', '16 Desember 2020', 'Dalam Negeri', 'Prodi', 'Penyelenggaraan program pemagangan dan program penyaluran lulusan bagi mahasiswa Frakultas Rekayasa Industri Telkom University', NULL, NULL, NULL, 1),
(28, 'Sekolah Tinggi Teknologi Telematika Telkom ', 'Penyelenggaraan Pendidikan Jarak Jauh (PJJ)', '16 Desember 2015', '16 Desember 2016', 'Dalam Negeri', 'Prodi', 'Penyelenggaraan Unit Sumber Belajar Jarak Jauh (USBJJ) dan kuliah daring PDITT (Perkuliahan Dalam Jaringan Indonesia Terbuka dan Terpadu)', NULL, NULL, NULL, 1),
(29, 'PT. Netkrom Solusindo', 'Pengembangan Potensi Kerjasama', '28 Oktober 2015', '28 Oktober 2016', 'Dalam Negeri', 'Prodi', 'Pemanfaatan teknologi, sarana dan prasarana\n- Pengembangan potensi tenaga pengajar\n- Pengembangan potensi mahasiswa\n- Pengembangan kurikulum\n- Pemberian sertifikasi keahlian kepada peserta Training\n', NULL, NULL, NULL, 1),
(30, 'PT. Multipolar Technology, Tbk. ', 'Kerjasama di bidang Akademik dan Pendidikan', '02 November 2015', '02 November 2016', 'Dalam Negeri', 'Prodi', 'Pemanfaatan teknologi, sarana dan prasarana\n- Pengembangan pengajaran dan penelitian\n- Pengembangan potensi mahasiswa dan intership\n- Pengembangan kurikulum\n- Penyelenggaraan kegiatan lomba\n', NULL, NULL, NULL, 1),
(31, 'Perpustakaan Nasional Republik Indonesia ', 'Kerjasama Perpustakaan', '15 Februari 2016', '15 Februari 2021', 'Dalam Negeri', 'Prodi', 'Pengembangan sumber daya manusia bidang perpustakaan\n- Pertemuan ilmiah, penelitian dan publikasi bersama dalam bidang perpustakaan\n- Pertukaran data katalog perpustakaan\n- Pengembangan dan pemanfaatan bersama koleksi perpustakaan\n- Perhimpunan dan pelestarian Karya Cetak Karya Rekam  (KRKC)\n- Peluasan jaringan perpustakaan lingkup nasional dan internasional\n', NULL, NULL, NULL, 1),
(32, 'Kompas Gramedia ', 'Penyelenggaraan Program Rekrutmen, Magang (Internship), Pelaksanaan Workshop dan atau Kuliah Umum, Pengabdian Masyarakat, Penelitian, Pengembangan Kompetensi Mahasiswa, serta Kunjungan Industri', '02 April 2016', '02 April 2021', 'Dalam Negeri', 'Prodi', 'Penyelenggaraan program rekrutmen, magang, pelaksanaan workshop dana tau kuliah umum, pengabdian masyarakat, penelitian, pengembangan kompetensi mahasiswa, serta kunjungan industri', NULL, NULL, NULL, 1),
(33, 'PT. Telekomunikasi Indonesia, Tbk dan PT. Murtila Promosindo ', 'Penyelenggaraan Anja & Bandung ICT Expo 2016 : Digital Industries for Cultural and Natural Wisdom to Enhance Nation Competitivess', '1 Februari 2016', '24 Desember 2016', 'Dalam Negeri', 'Prodi', 'Telkom University adalah pemilik event Bandung ICT Expo 2016', NULL, NULL, NULL, 1),
(34, 'PT. Murtila Promosindo ', 'Penyelenggaraan Bandung Information and Communication Technology Expo 2015', '25 Februari 2015', '25 Februari 2016', 'Dalam Negeri', 'Prodi', 'Penyelenggaraan Bandung Information and Communication Technology Expo 2015', NULL, NULL, NULL, 1),
(35, 'PT. Citilink Indonesia ', 'Program loyalitas perusahaan bagi stakeholder internal Telkom University', '13 Agustus 2016', '13 Agustus 2018', 'Dalam Negeri', 'Prodi', 'Pembelian tiket dinas dan non dinas\n- Komunikasi pemasaran\n- Kerjasama Backlink Website\n- Rekrutmen magang (Internship)\n- Pelaksanaan workshop dan atau kuliah umum\n- Pengembangan SDM melalui Pengabdian Masyarakat, penelitian, Pengembangan Kompetensi mahasiswa, serta Kunjungan Industri', NULL, NULL, NULL, 1),
(36, 'Universitas Pertahanan', 'Kerjasama Tri Dharma Perguruan Tinggi', '29 September 2016', '29 September 2019', 'Dalam Negeri', 'Prodi', 'Pengembangan di bidang pendidikan dan pengajaran, penelitian dan pengkajian, teknologi, serta pengabdian masyarakat', NULL, NULL, NULL, 1),
(37, 'PT. Huawei Investment', 'Membangun Dasar Kerjasama dan Kolaborasi dalam hal Pendidikan, Pengembangan SDM, dan Penyaluran Lulusan', '19 Oktober 2016', '19 Oktober 2019', 'Dalam Negeri', 'Prodi', 'Pemberian beasiswa bagi mahasiswa\n- Pengembangan SDM dan fasilitas laboratorium', NULL, NULL, NULL, 1),
(38, 'PT. ZTE Indonesia', 'Kerjasama dalam hal Pengembangan SDM, Fasilitas Laboratorium dan Pemberian Beasiswa', '19 Oktober 2016', '19 Oktober 2019', 'Dalam Negeri', 'Prodi', 'Pemberian beasiswa bagi mahasiswa\n- Pengembangan SDM dan fasilitas laboratorium\n', NULL, NULL, NULL, 1),
(39, 'Forum Alumni Universitas Telkom', 'Kerjasama Pengembangan Institusi : Akademik dan Non Akademik Universitas Telkom', '21 Oktober 2016', 'tidak terbatas waktu', 'Dalam Negeri', 'Prodi', 'Pengembangan institusi', NULL, NULL, NULL, 1),
(40, 'PT. CTEH LAB EDWAR Teknologi ', 'Kerjasama di bidang Pengembangan dan Pemanfaatan Kapasitas Sumber Daya Ilmu Pengetahuan dan Teknologi serta Komersialisasi Produk Inovasi', '10 Agustus 2016', '10 Agustus 2021', 'Dalam Negeri', 'Prodi', 'Pelaksanaan program Kerja Praktek/magang, Tugas Akhir, dll\n- Penerbitan jurnal ilmiah bersama\n- Penyelenggaraan seminar bersama\n- Layanan keahlian praktis\n', NULL, NULL, NULL, 1),
(41, 'PT. Garuda Indonesia, Tbk ', 'Kerjasama Bidang Pendidikan dan Penjualan', '14 November2016', '14 November 2017', 'Dalam Negeri', 'Prodi', 'Pengembangan kegiatan program komunikasi pemasaran, blacklink website, tarif korporat, rekrutmen, maang, workshop/kuliah umum, pengabdian kepada masyarakat, penelitian, pengembangan kompetensi mahasiswa/gelada serta kunjungan industri', NULL, NULL, NULL, 1),
(42, 'SMKN 1 Malausma Majalengka ', 'Kerjasama Pelaksanaan Uji Kompetensi Siswa Kelas XII SMK', '23 Februari 2017', '23 Maret 2017', 'Dalam Negeri', 'Prodi', 'Pelaksanaan program penelitian dan pengabdian masyarakat bagi mahasiswa Fakultas Rekayasa Industri  Telkom University di SMKN 1 Malausma Majalengka', NULL, NULL, NULL, 1),
(43, 'Permata Bunda Syari''ah ', 'Kerjasama dalam melaksanakan penelitian bersama', '16 November 2013', '16 November 2017', 'Dalam Negeri', 'Prodi', 'Penelitian bersama yang berjudul, Sistem rekoam medis pada citra USG dan identifikasi kesuburan wanitaberdasarkan klasifikasi PCOS\n', NULL, NULL, NULL, 1),
(44, 'PT. Net Mediatama Televisi', 'Kerjasama dalam hal Magang Mahasiswa dan Rekruitmen serta Kegiatan Pendukung', '29 November 2014', '29 November 2016', 'Dalam Negeri', 'Prodi', 'Pelaksanaan program magang untuk meningkatkan kualitas mahasiswa Fakultas Rekayasa Industri\n- Pelaksanaan program rekruitasi pegawai NET bagi alumni', NULL, NULL, NULL, 1),
(45, 'Direktorat Jenderal Penyelenggaraan Pos dan Informatika', 'Kesepakatan Saling Mendukung dalam Bidang Pendidikan, Penelitian, dan Pengembangan SDM', '13 Juli 2015', '13 Juli 2018', 'Dalam Negeri', 'Prodi', 'Pelaksanaan program praktek kerja lapangan (PKL)/magang kerja/KKN, penelitian dan pengabdian masyarakat bagi mahasiswa Fakultas Rekayasa Industri  Telkom University di Direktorat Jenderal Penyelenggaraan Pos dan Informatika', NULL, NULL, NULL, 1),
(46, 'Asosiasi Kliring Interkoneksi Telekomunikasi ', 'Kerjasama dalam melaksanakan Pekerjaan Kajian Interkoneksi di Era Broadband', '24 Maret 2016', '10 Oktober 2016', 'Dalam Negeri', 'Prodi', 'Pelaksanaan Pekerjaan Kajian Interkoneksi di Era Broadband dengan baik', NULL, NULL, NULL, 1),
(47, 'PT. Telekomunikasi Indonesia, Tbk. ', 'Kerjasama Program-Program Penelitian', '18 Februari 2016', '18 Februari 2018', 'Dalam Negeri', 'Prodi', 'Mendapatkan akses data dan fasilitas penelitian yang dibutuhkan untuk mendukung kerjasama riset sesuai dengan ruang lingkup yang disepakati', NULL, NULL, NULL, 1),
(48, 'Fontys University of Applied Sciences', 'Program Double Degree', '11 Oktober 2013', NULL, 'Luar Negeri', 'Prodi', 'Meningkatkan kualitas lulusan program studi IT dan Engineering', NULL, NULL, NULL, 1),
(49, 'Kumoh National Institute of Technology ', 'Program Double Degree', '25 September 2015', '25 September 2018', 'Luar Negeri', 'Prodi', 'Meningkatkan kualitas lulusan program studi dari kedua institusi', NULL, NULL, NULL, 1),
(50, 'Universiti Utara Malaysia', 'Kerjasama Penelitian', '14 Maret 2016', '14 Maret 2018', 'Luar Negeri', 'Prodi', 'Meningkatkan jumlah hak kekayaan intelektual', NULL, NULL, NULL, 1),
(51, 'Universiti Telekom Sdn ', 'Pertukaran Dosen dan Peneliti, Pertukaran Mahasiswa, dan Kerjasama Akademik', '31 Agustus 2013', '31 Agustus 2016', 'Luar Negeri', 'Prodi', 'Pertukaran Dosen dan Peneliti, Pertukaran Mahasiswa, dan Kerjasama Akademik', NULL, NULL, NULL, 1),
(52, 'Busan University of Foreign Student ', 'Kerjasama di bidang pendidikan (exchange student, faculty members, joint research project, joint conference, exchange research material, dan program penelitian)', '10 Desember 2015', '10 Desember 2020', 'Luar Negeri', 'Prodi', 'Perkembangan riset dan pendidikan untuk meningkatkan profesionalisme secara internasional', NULL, NULL, NULL, 1),
(53, 'DAQRI Open Source Division, United States of America ', 'Kerjasama dalam bidang akademik dan pendidikan untuk kepentingan bersama', '15 Desember 2015', '5 tahun akademik', 'Luar Negeri', 'Prodi', 'Pengembangkan Smart Helmet untuk industry dan menjadikan Universitas Telkom sebagai centre of excellent di bidang Augmented Reality di Asia Tenggara', NULL, NULL, NULL, 1),
(54, 'Kyungdong University Korea', 'Kerjasama di Bidang Akademik', '2 Mei 2016', '2 Mei 2018', 'Luar Negeri', 'Prodi', 'Mengembangkan kegiatan pertukaran akademik antar mahasiswa dan dosen di kedua institusi serta adanya program gelar ganda', NULL, NULL, NULL, 1),
(55, 'FPT University, Socialist Republic of Vietnam', 'Academic and Student Exchange Co-Operation between Universitas Telkom dan FPT University', '22 Oktober 2015', '22 Oktober 2016', 'Luar Negeri', 'Prodi', 'Meningkatkan kualitas lulusan dengan adanya program pertukaran pelajar', NULL, NULL, NULL, 1),
(56, 'Universidade Nacional Timor Lorosa''e', 'Network Administrator and Management Training, Information Management System Development of UNTL Including E-Office also Attended Monitoring System', '10 Oktober 2014', '10 Oktober 2017', 'Luar Negeri', 'Prodi', 'Meningkatkan kualitas civitas Telkom University dengan program pelatihan di bagian jaringan', NULL, NULL, NULL, 1),
(57, 'Myongji University', 'Research, Education, Academic Exchange', '01 September 2014', '01 September 2019', 'Luar Negeri', 'Prodi', 'Meningkatkan kualitas riset dan knowledge sharing melalui exchange program', NULL, NULL, NULL, 1),
(58, 'MarkAny Inc. ', 'Penelitian dan Pengembangan', '15 Oktober 2015', '15 Februari 2015', 'Luar Negeri', 'Prodi', 'Mengembangkan software ACR berbasis audio', NULL, NULL, NULL, 1),
(59, 'Osaka International University', 'Academic Exchange', '18 Maret 2015', '18 Maret 2020', 'Luar Negeri', 'Prodi', 'Meningkatkan kualitas riset dan knowledge sharing melalui exchange program', NULL, NULL, NULL, 1),
(60, 'Telekomunikasi Indonesia Internasional ', 'Kerjasama Beasiswa Mahasiswa Magister Manajemen Program Blended Learning', '30 Januari 2017', '30 Januari 2019', 'Luar Negeri', 'Prodi', 'Menerima pembayaran biaya beasiswa yang telah disepakati', NULL, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_peldosen_ta`
--

CREATE TABLE `tb_peldosen_ta` (
  `no` int(11) NOT NULL,
  `id_peldos` varchar(100) NOT NULL,
  `prodi` varchar(100) DEFAULT NULL,
  `no_ts` varchar(1000) DEFAULT NULL,
  `judul_penelitian_dosen` varchar(500) DEFAULT NULL,
  `nama_dosenketua` varchar(200) DEFAULT NULL,
  `nama_dosenanggota` varchar(200) DEFAULT NULL,
  `tahun` varchar(300) DEFAULT NULL,
  `nama_mahasiswa` varchar(300) DEFAULT NULL,
  `judul_ta_mahasiswa` varchar(300) DEFAULT NULL,
  `bukti_laporan_akhir` varchar(200) DEFAULT NULL,
  `keterangan` varchar(200) DEFAULT NULL,
  `sts_valid` varchar(50) DEFAULT NULL,
  `id_user` int(11) NOT NULL,
  `file_name` varchar(1000) DEFAULT NULL,
  `file_type` varchar(500) DEFAULT NULL,
  `file_date` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_peldosen_ta`
--

INSERT INTO `tb_peldosen_ta` (`no`, `id_peldos`, `prodi`, `no_ts`, `judul_penelitian_dosen`, `nama_dosenketua`, `nama_dosenanggota`, `tahun`, `nama_mahasiswa`, `judul_ta_mahasiswa`, `bukti_laporan_akhir`, `keterangan`, `sts_valid`, `id_user`, `file_name`, `file_type`, `file_date`) VALUES
(1, 'PLD_9', 'SISTEM INFORMASI', 'TS_1', 'PENERAPAN SISTEM INFORMASI BERBASIS OPENERP DENGAN METODE RAPID APPLICATION DEVELOPMENT (STUDI KASUS: PT GENTA TRIKARYA)																						', 'ARI YANUAR RIDWAN', 'R. WAHJOE WITJAKSONO', '2015', 'MUHAMMAD SYAIFUL RAMADHAN', 'PENERAPAN SISTEM PURCHASE MANAGEMENT MENGGUNAKAN OPENERP DENGAN METODE RAPID APPLICATION DEVELOPMENT (STUDI KASUS: PT GENTA TRIKARYA)											', '', 'TA', 'valid', 1, NULL, NULL, NULL),
(2, 'PLD_9', 'SISTEM INFORMASI', 'TS_1', 'PENERAPAN SISTEM INFORMASI BERBASIS OPENERP DENGAN METODE RAPID APPLICATION DEVELOPMENT (STUDI KASUS: PT GENTA TRIKARYA)', 'ARI YANUAR RIDWAN', 'R. WAHJOE WITJAKSONO', '2015', 'FAJRUL ALFIAN', 'PENERAPAN SISTEM MANUFACTURING BERBASIS OPENERP DENGAN METODE RAPID APPLICATION DEVELOPMENT (STUDI KASUS: PT GENTA TRIKARYA)', NULL, 'TA', 'valid', 1, NULL, NULL, NULL),
(3, 'PLD_10', 'SISTEM INFORMASI', 'TS_1', 'PENERAPAN ERP MENGGUNAKAN OPEN ERP DENGAN METODE SURE STEP (STUDI KASUS : PT XYZ RETAIL FASHION)', 'DEDEN WITARSYAH', 'R. WAHJOE WITJAKSONO', '2015', 'MOHD RHEDO MARGEN', 'PENERAPAN ERP MODUL PAYROLL MENGGUNAKAN OPEN ERP DENGAN METODE SURE STEP (STUDI KASUS : PT XYZ RETAIL FASHION', NULL, 'TA', 'valid', 1, NULL, NULL, NULL),
(4, 'PLD_10', 'SISTEM INFORMASI', 'TS_1', 'PENERAPAN ERP MENGGUNAKAN OPEN ERP DENGAN METODE SURE STEP (STUDI KASUS : PT XYZ RETAIL FASHION)', 'DEDEN WITARSYAH', 'R. WAHJOE WITJAKSONO', '2015', 'IKHSAN YUDHA PRADANA', 'PENERAPAN ERP MODUL LOGISTICS MENGGUNAKAN OPENERP DENGAN METODE SURE STEP (STUDI KASUS: PT XYZ RETAIL FASHION)', NULL, 'TA', 'valid', 1, NULL, NULL, NULL),
(5, 'PLD_10', 'SISTEM INFORMASI', 'TS_1', 'PENERAPAN ERP MENGGUNAKAN OPEN ERP DENGAN METODE SURE STEP (STUDI KASUS : PT XYZ RETAIL FASHION)', 'DEDEN WITARSYAH', 'R. WAHJOE WITJAKSONO', '2015', 'ADELIA FEBIYANTI M.', 'PENERAPAN MODUL RECRUITMENT MENGGUNAKAN OPENERP DENGAN METODE SURE STEP (STUDI KASUS: PT XYZ RETAIL FASHION)', NULL, 'TA', 'valid', 1, NULL, NULL, NULL),
(6, 'PLD_11', 'SISTEM INFORMASI', 'TS_1', 'PERANCANGAN DAN PEMBANGUNAN KNOWLEDGE MANAGEMENT SYSTEM PADA MODUL PENELITIAN DAN PENGABDIAN MASYARAKAT DAN PENUNJANG MENGGUNAKAN FRAMEWORK CODEIGNITER DENGAN METODE ITERATIVE INCREMENTAL', 'LUCIANA ANDRAWINA', 'AHMAD MUSNANSYAH', '2015', 'NEVISIA PUSPA AYUDHANA', 'PERANCANGAN DAN PEMBANGUNAN KNOWLEDGE MANAGEMENT SYSTEM PADA MODUL PENGABDIAN MASYARAKAT DAN PENUNJANG MENGGUNAKAN FRAMEWORK CODEIGNITER DENGAN METODE ITERATIVE INCREMENTAL', NULL, 'TA', 'valid', 1, NULL, NULL, NULL),
(7, 'PLD_11', 'SISTEM INFORMASI', 'TS_1', 'PERANCANGAN DAN PEMBANGUNAN KNOWLEDGE MANAGEMENT SYSTEM PADA MODUL PENELITIAN DAN PENGABDIAN MASYARAKAT DAN PENUNJANG MENGGUNAKAN FRAMEWORK CODEIGNITER DENGAN METODE ITERATIVE INCREMENTAL', 'LUCIANA ANDRAWINA', 'AHMAD MUSNANSYAH', '2015', 'LIFFI NOFERIANTI', 'PERANCANGAN DAN PEMBANGUNAN KNOWLEDGE MANAGEMENT SYSTEM PADA MODUL PENELITIAN MENGGUNAKAN FRAMEWORK CODEIGNITER DENGAN METODE ITERATIVE INCREMENTAL', NULL, 'TA', 'valid', 1, NULL, NULL, NULL),
(8, 'PLD_12', 'SISTEM INFORMASI', 'TS_1', 'PERANCANGAN KNOWLEDGE MANAGEMENT SYSTEM UNTUK PENGELOLAAN SELEKSI MAHASISWA BARU DI UNIVERSITAS TELKOM DENGAN METODE ITERATIVE INCREMENTAL', 'LUCIANA ANDRAWINA', 'FAISHAL MUFIED AL-ANSHARY', '2015', 'PUTU PUSPITHA SARASWATI', 'PERANCANGAN KNOWLEDGE MANAGEMENT SYSTEM UNTUK SELEKSI MAHASISWA BARU DI UNIVERSITAS TELKOM MENGGUNAKAN APLIKASI MOBILE BERBASIS ANDROID DENGAN METODE ITERATIVE INCREMENTAL', NULL, 'TA', 'valid', 1, NULL, NULL, NULL),
(9, 'PLD_12', 'SISTEM INFORMASI', 'TS_1', 'PERANCANGAN KNOWLEDGE MANAGEMENT SYSTEM UNTUK PENGELOLAAN SELEKSI MAHASISWA BARU DI UNIVERSITAS TELKOM DENGAN METODE ITERATIVE INCREMENTAL', 'LUCIANA ANDRAWINA', 'FAISHAL MUFIED AL-ANSHARY', '2015', 'INEZ SEKARAYU NAWANGWULAN', 'PERANCANGAN KNOWLEDGE MANAGEMENT SYSTEM UNTUK MENGELOLA DATA SELEKSI MAHASISWA BARU DI UNIVERSITAS TELKOM DENGAN METODE ITERATIVE INCREMENTAL', NULL, 'TA', 'valid', 1, NULL, NULL, NULL),
(10, 'PLD_12', 'SISTEM INFORMASI', 'TS_1', 'PERANCANGAN KNOWLEDGE MANAGEMENT SYSTEM UNTUK PENGELOLAAN SELEKSI MAHASISWA BARU DI UNIVERSITAS TELKOM DENGAN METODE ITERATIVE INCREMENTAL', 'LUCIANA ANDRAWINA', 'FAISHAL MUFIED AL-ANSHARY', '2015', 'SOFIA FARIDAH', 'PERANCANGAN KNOWLEDGE MANAGEMENT SYSTEM UNTUK MENGELOLA KNOWLEDGE SHARING PADA SELEKSI MAHASISWA BARU DI UNIVERSITAS TELKOM DENGAN METODE ITERATIVE INCREMENTAL', NULL, 'TA', 'valid', 1, NULL, NULL, NULL),
(11, 'PLD_13', 'SISTEM INFORMASI', 'TS_1', 'PENERAPAN SISTEM INFORMASI BERBASIS OPENERP DENGAN METODE SURESTEP (STUDI KASUS: UMKM KONVEKSI RAJUTAN)', 'IRFAN DARMAWAN', 'R. WAHJOE WITJAKSONO', '2015', 'IRENA ARSYKA DEWI', 'PENERAPAN SISTEM MANUFACTURING PADA UMKM KONVEKSI RAJUTAN BERBASIS OPENERP DENGAN METODE SURE STEP', NULL, 'TA', 'valid', 1, NULL, NULL, NULL),
(12, 'PLD_13', 'SISTEM INFORMASI', 'TS_1', 'PENERAPAN SISTEM INFORMASI BERBASIS OPENERP DENGAN METODE SURESTEP (STUDI KASUS: UMKM KONVEKSI RAJUTAN)', 'IRFAN DARMAWAN', 'R. WAHJOE WITJAKSONO', '2015', 'MOHAMAD IQBAL', 'PENERAPAN SISTEM PURCHASE MANAGEMENT DAN WAREHOUSE MANAGEMENT PADA UMKM KONVEKSI RAJUTAN BERBASIS OPENERP DENGAN METODE SURESTEP', NULL, 'TA', 'valid', 1, NULL, NULL, NULL),
(13, 'PLD_13', 'SISTEM INFORMASI', 'TS_1', 'PENERAPAN SISTEM INFORMASI BERBASIS OPENERP DENGAN METODE SURESTEP (STUDI KASUS: UMKM KONVEKSI RAJUTAN)', 'IRFAN DARMAWAN', 'R. WAHJOE WITJAKSONO', '2015', 'PRADITO SETIADI', 'PENERAPAN SISTEM SALES MANAGEMENT DAN WAREHOUSE MANAGEMENT BERBASIS OPENERP DENGAN METODE SURESTEP (STUDI KASUS: UMKM KONVEKSI RAJUTAN)', NULL, 'TA', 'valid', 1, NULL, NULL, NULL),
(14, 'PLD_14', 'SISTEM INFORMASI', 'TS_1', 'PERANCANGAN ENTERPRISE ARCHITECTURE E-COMMERCE DI PT XYZ MENGGUNAKAN FRAMEWORK TOGAF ADM', 'IRFAN DARMAWAN', 'BASUKI RAHMAD', '2015', 'RINI SETYANINGSIH', 'PERANCANGAN ENTERPRISE ARCHITECTURE E-COMMERCE PADA BAGIAN PAYMENT DI PT XYZ MENGGUNAKAN FRAMEWORK TOGAF ADM', NULL, 'TA', 'valid', 1, NULL, NULL, NULL),
(15, 'PLD_14', 'SISTEM INFORMASI', 'TS_1', 'PERANCANGAN ENTERPRISE ARCHITECTURE E-COMMERCE DI PT XYZ MENGGUNAKAN FRAMEWORK TOGAF ADM', 'IRFAN DARMAWAN', 'BASUKI RAHMAD', '2015', 'Ajeng Citra Rizkyanur', 'PERANCANGAN ENTERPRISE ARCHITECTURE E-COMMERCE PADA BAGIAN MANAJEMEN HUBUNGAN PELANGGAN DI PT XYZ MENGGUNAKAN FRAMEWORK TOGAF ADM', NULL, 'TA', 'valid', 1, NULL, NULL, NULL),
(16, 'PLD_14', 'SISTEM INFORMASI', 'TS_1', 'PERANCANGAN ENTERPRISE ARCHITECTURE E-COMMERCE DI PT XYZ MENGGUNAKAN FRAMEWORK TOGAF ADM', 'IRFAN DARMAWAN', 'BASUKI RAHMAD', '2015', 'SRI HARYANI BR. MANJUNTAK', 'PERANCANGAN ENTERPRISE ARCHITECTURE E-COMMERCE PADA BAGIAN E-MARKETPLACE DI PT XYZ MENGGUNAKAN FRAMEWORK TOGAF ADM', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(17, 'PLD_14', 'SISTEM INFORMASI', 'TS_1', 'PERANCANGAN ENTERPRISE ARCHITECTURE E-COMMERCE DI PT XYZ MENGGUNAKAN FRAMEWORK TOGAF ADM', 'IRFAN DARMAWAN', 'BASUKI RAHMAD', '2015', 'PUTRI MYKE WAHYUNI', 'PERANCANGAN ENTERPRISE ARCHITECTURE E-COMMERCE PADA BAGIAN MANAJEMEN PRODUK DAN PEMASOK DI PT XYZ MENGGUNAKAN FRAMEWORK TOGAF ADM', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(18, 'PLD_15', 'SISTEM INFORMASI', 'TS_1', 'PERANCANGAN PRIVATE CLOUD COMPUTING PADA FAKULTAS REKAYASA INDUSTRI UNIVERSITAS TELKOM', 'TEGUH KURNIAWAN', 'ADITYAS WIDJAJARTO', '2015', 'DECKY RADITAMA MEGANTARA', 'PERANCANGAN PRIVATE CLOUD COMPUTING DENGAN PENDEKATAN SOFTWARE AS A SERVICE PADA FAKULTAS REKAYASA INDUSTRI UNIVERSITAS TELKOM', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(19, 'PLD_15', 'SISTEM INFORMASI', 'TS_1', 'PERANCANGAN PRIVATE CLOUD COMPUTING PADA FAKULTAS REKAYASA INDUSTRI UNIVERSITAS TELKOM', 'TEGUH KURNIAWAN', 'ADITYAS WIDJAJARTO', '2015', 'TEGAR PAMUNGKAS', 'PERANCANGAN PRIVATE CLOUD COMPUTING DENGAN MODEL INFRASTRUCTURE AS A SERVICE (IAAS) PADA FAKULTAS REKAYASA INDUSTRI UNIVERSITAS TELKOM', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(20, 'PLD_16', 'SISTEM INFORMASI', 'TS_1', 'DESAIN DAN ANALISA INFRASTRUKTUR JARINGAN DI PDII-LIPI JAKARTA DENGAN MENGGUNAKAN METODE NETWORK DEVELOPMENT LIFE CYCLE (NDLC)', 'TEGUH KURNIAWAN', 'UMAR YUNAN KURNIA SEPTO ', '2015', 'OKTA PUSPITA DWI ANGGOROWATI', 'DESAIN DAN ANALISA INFRASTRUKTUR JARINGAN WIRELESS DI PDII-LIPI JAKARTA DENGAN MENGGUNAKAN METODE NETWORK DEVELOPMENT LIFE CYCLE (NDLC)', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(21, 'PLD_16', 'SISTEM INFORMASI', 'TS_1', 'DESAIN DAN ANALISA INFRASTRUKTUR JARINGAN DI PDII-LIPI JAKARTA DENGAN MENGGUNAKAN METODE NETWORK DEVELOPMENT LIFE CYCLE (NDLC)', 'TEGUH KURNIAWAN', 'UMAR YUNAN KURNIA SEPTO ', '2015', 'ARIF NURFAJAR', 'DESAIN DAN ANALISA INFRASTRUKTUR JARINGAN WIRED DI PDII-LIPI JAKARTA DENGAN MENGGUNAKAN METODE NETWORK DEVELOPMENT LIFE CYCLE (NDLC)', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(22, 'PLD_17', 'SISTEM INFORMASI', 'TS_1', 'PERANCANGAN SERVICE PADA LAYANAN ANGKUTAN PT KERETA API INDONESIA (PERSERO) MENGGUNAKAN FRAMEWORK ITIL VERSI 3', 'MURAHARTAWATY', 'EKO K. UMAR ', '2015', 'SABRINA ANDIYANI', 'PERANCANGAN SERVICE DESIGN PADA LAYANAN ANGKUTAN PENUMPANG PT KERETA API INDONESIA (PERSERO) MENGGUNAKAN FRAMEWORK ITIL VERSI 3', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(23, 'PLD_17', 'SISTEM INFORMASI', 'TS_1', 'PERANCANGAN SERVICE PADA LAYANAN ANGKUTAN PT KERETA API INDONESIA (PERSERO) MENGGUNAKAN FRAMEWORK ITIL VERSI 3', 'MURAHARTAWATY', 'EKO K. UMAR ', '2015', 'REXY SEPTIAN ARAFAT', 'PERANCANGAN SERVICE OPERATION PADA LAYANAN ANGKUTAN PENUMPANG PT KERETA API INDONESIA (PERSERO) MENGGUNAKAN FRAMEWORK ITIL VERSI 3', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(24, 'PLD_17', 'SISTEM INFORMASI', 'TS_1', 'PERANCANGAN SERVICE PADA LAYANAN ANGKUTAN PT KERETA API INDONESIA (PERSERO) MENGGUNAKAN FRAMEWORK ITIL VERSI 3', 'MURAHARTAWATY', 'EKO K. UMAR ', '2015', 'MIFTA AZIZ', 'PERANCANGAN SERVICE DESIGN PADA LAYANAN ANGKUTAN BARANG PT KERETA API INDONESIA (PERSERO) MENGGUNAKAN FRAMEWORK ITIL VERSI 3', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(25, 'PLD_17', 'SISTEM INFORMASI', 'TS_1', 'PERANCANGAN SERVICE PADA LAYANAN ANGKUTAN PT KERETA API INDONESIA (PERSERO) MENGGUNAKAN FRAMEWORK ITIL VERSI 3', 'MURAHARTAWATY', 'EKO K. UMAR ', '2015', 'RINALDY FERDY FERDIANO', 'PERANCANGAN SERVICE TRANSITION PADA LAYANAN ANGKUTAN PENUMPANG PT KERETA API INDONESIA MENGGUNAKAN FRAMEWORK ITIL VERSI 3', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(26, 'PLD_18', 'SISTEM INFORMASI', 'TS_1', 'ASSESSMENT DAN PERANCANGAN ITSM DOMAIN SERVICE BERDASARKAN ITIL VERSI 2011, ISO 20000 SERIES, DAN ISO 15504 SERIES UNTUK MENINGKATKAN CAPABILITY LEVEL DENGAN PEMANFAATAN TOOLS REMEDY PADA PT TELKOM INDONESIA', 'MURAHARTAWATY', 'EKO K. UMAR ', '2015', 'REZA ALDIANSYAH', 'ASSESSMENT DAN PERANCANGAN ITSM DOMAIN SERVICE OPERATION BERDASARKAN ITIL VERSI 2011, ISO 20000 SERIES, DAN ISO 15504 SERIES UNTUK MENINGKATKAN CAPABILITY LEVEL DENGAN PEMANFAATAN TOOLS REMEDY PADA PT TELKOM INDONESIA', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(27, 'PLD_18', 'SISTEM INFORMASI', 'TS_1', 'ASSESSMENT DAN PERANCANGAN ITSM DOMAIN SERVICE BERDASARKAN ITIL VERSI 2011, ISO 20000 SERIES, DAN ISO 15504 SERIES UNTUK MENINGKATKAN CAPABILITY LEVEL DENGAN PEMANFAATAN TOOLS REMEDY PADA PT TELKOM INDONESIA', 'MURAHARTAWATY', 'EKO K. UMAR ', '2015', 'VIKY HERMANA PRATAMA', 'ASSESSMENT DAN PERANCANGAN ITSM DOMAIN SERVICE DESIGN BERDASARKAN ITIL VERSI 2011, ISO 20.000 SERIES, DAN ISO 15.504 SERIES UNTUK MENINGKATKAN CAPABILITY LEVEL DENGAN PEMANFAATAN TOOLS REMEDY (STUDY KASUS: PT TELKOM INDONESIA TBK)', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(28, 'PLD_19', 'SISTEM INFORMASI', 'TS_1', 'PERANCANGAN SERVICE PADA LAYANAN IT PUSAIR DENGAN MENGGUNAKAN FRAMEWORK ITIL VERSI 3', 'MURAHARTAWATY', 'NIA AMBARSARI', '2015', 'FRANSISKA', 'PERANCANGAN SERVICE OPERATION PADA LAYANAN IT PUSAIR DENGAN MENGGUNAKAN FRAMEWORK ITIL VERSI 3', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(29, 'PLD_19', 'SISTEM INFORMASI', 'TS_1', 'PERANCANGAN SERVICE PADA LAYANAN IT PUSAIR DENGAN MENGGUNAKAN FRAMEWORK ITIL VERSI 3', 'MURAHARTAWATY', 'NIA AMBARSARI', '2015', 'VERA ANANDA', 'PERANCANGAN SERVICE TRANSITION PADA LAYANAN IT PUSAIR DENGAN MENGGUNAKAN FRAMEWORK ITIL VERSI 3', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(30, 'PLD_20', 'SISTEM INFORMASI', 'TS_1', 'ANALISIS DAN PERANCANGAN ITSM PADA LAYANAN AKADEMIK INSTITUT PEMERINTAHAN DALAM NEGERI (IPDN) DENGAN MENGGUNAKAN FRAMEWORK ITIL VERSI 3', 'MURAHARTAWATY', 'RIDHA HANAFI', '2015', 'ARIDHA MEITYA ARIFIN', 'ANALISIS DAN PERANCANGAN ITSM DOMAIN SERVICE OPERATION PADA LAYANAN AKADEMIK INSTITUT PEMERINTAHAN DALAM NEGERI (IPDN) DENGAN MENGGUNAKAN FRAMEWORK ITIL VERSI 3', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(31, 'PLD_20', 'SISTEM INFORMASI', 'TS_1', 'ANALISIS DAN PERANCANGAN ITSM PADA LAYANAN AKADEMIK INSTITUT PEMERINTAHAN DALAM NEGERI (IPDN) DENGAN MENGGUNAKAN FRAMEWORK ITIL VERSI 3', 'MURAHARTAWATY', 'RIDHA HANAFI', '2015', 'CHARLIE SUGIARTO', 'ANALISIS DAN PERANCANGAN ITSM DOMAIN SERVICE TRANSITION PADA LAYANAN AKADEMIK INSTITUT PEMERINTAHAN DALAM NEGERI (IPDN) DENGAN MENGGUNAKAN FRAMEWORK ITIL VERSI 3', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(32, 'PLD_20', 'SISTEM INFORMASI', 'TS_1', 'ANALISIS DAN PERANCANGAN ITSM PADA LAYANAN AKADEMIK INSTITUT PEMERINTAHAN DALAM NEGERI (IPDN) DENGAN MENGGUNAKAN FRAMEWORK ITIL VERSI 3', 'MURAHARTAWATY', 'RIDHA HANAFI', '2015', 'THESSA SILVIANA MANURUNG', 'ANALISIS DAN PERANCANGAN ITSM DOMAIN SERVICE DESIGN PADA LAYANAN AKADEMIK INSTITUT PEMERINTAHAN DALAM NEGERI (IPDN) DENGAN MENGGUNAKAN FRAMEWORK ITIL VERSI 3', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(33, 'PLD_21', 'SISTEM INFORMASI', 'TS_1', 'PERANCANGAN ARCHITECTURE UNTUK FUNGSI AKADEMIK PADA INSTITUT PEMERINTAHAN DALAM NEGERI MENGGUNAKAN FRAMEWORK TOGAF ADM STUDI KASUS SISTEM INFORMASI AKADEMIK (SIAKAD)', 'MURAHARTAWATY', 'RIDHA HANAFI', '2015', 'SATRIA JANAKA', 'PERANCANGAN APPLICATION ARCHITECTURE UNTUK FUNGSI AKADEMIK PADA INSTITUT PEMERINTAHAN DALAM NEGERI (IPDN) MENGGUNAKAN FRAMEWORK TOGAF ADM STUDI KASUS SISTEM INFORMASI AKADEMIK (SIAKAD)', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(34, 'PLD_21', 'SISTEM INFORMASI', 'TS_1', 'PERANCANGAN ARCHITECTURE UNTUK FUNGSI AKADEMIK PADA INSTITUT PEMERINTAHAN DALAM NEGERI MENGGUNAKAN FRAMEWORK TOGAF ADM STUDI KASUS SISTEM INFORMASI AKADEMIK (SIAKAD)', 'MURAHARTAWATY', 'RIDHA HANAFI', '2015', 'RAHAYU MANOLITA', 'PERANCANGAN BUSINESS ARCHITECTURE UNTUK FUNGSI AKADEMIK PADA INSTITUT PEMERINTAHAN DALAM NEGERI (IPDN) MENGGUNAKAN FRAMEWORK TOGAF ADM STUDI KASUS SISTEM INFORMASI AKADEMIK (SIAKAD)', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(35, 'PLD_21', 'SISTEM INFORMASI', 'TS_1', 'PERANCANGAN ARCHITECTURE UNTUK FUNGSI AKADEMIK PADA INSTITUT PEMERINTAHAN DALAM NEGERI MENGGUNAKAN FRAMEWORK TOGAF ADM STUDI KASUS SISTEM INFORMASI AKADEMIK (SIAKAD)', 'MURAHARTAWATY', 'RIDHA HANAFI', '2015', 'ANDIKA DESTA GINANJAR', 'PERANCANGAN DATA ARCHITECTURE UNTUK FUNGSI AKADEMIK PADA INSTITUT PEMERINTAHAN DALAM NEGERI (IPDN) MENGGUNAKAN FRAMEWORK TOGAF ADM STUDI KASUS SISTEM INFORMASI AKADEMIK (SIAKAD)', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(36, 'PLD_22', 'SISTEM INFORMASI', 'TS_1', 'ANALISIS DAN PERANCANGAN ARCHITECTURE MENGGUNAKAN THE OPEN GROUP ARCHITECTURE FRAMEWORK ARCHITECTURE DEVELOPMENT METHOD (TOGAF ADM) PADA PT SHAFCO MULTI TRADING', 'MURAHARTAWATY', 'RIDHA HANAFI', '2015', 'RENANTIA INDRIANI', 'ANALISIS DAN PERANCANGAN TECHNOLOGY ARCHITECTURE MENGGUNAKAN THE OPEN GROUP ARCHITECTURE FRAMEWORK ARCHITECTURE DEVELOPMENT METHOD (TOGAF ADM) PADA PT SHAFCO MULTI TRADING', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(37, 'PLD_22', 'SISTEM INFORMASI', 'TS_1', 'ANALISIS DAN PERANCANGAN ARCHITECTURE MENGGUNAKAN THE OPEN GROUP ARCHITECTURE FRAMEWORK ARCHITECTURE DEVELOPMENT METHOD (TOGAF ADM) PADA PT SHAFCO MULTI TRADING', 'MURAHARTAWATY', 'RIDHA HANAFI', '2015', 'FAMILA FARADIBA', 'ANALISIS DAN PERANCANGAN BUSINESS ARCHITECTURE MENGGUNAKAN THE OPEN GROUP ARCHITECTURE FRAMEWORK ARCHITECTURE DEVELOPMENT METHOD (TOGAF ADM) PADA PT SHAFCO MULTI TRADING', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(38, 'PLD_22', 'SISTEM INFORMASI', 'TS_1', 'ANALISIS DAN PERANCANGAN ARCHITECTURE MENGGUNAKAN THE OPEN GROUP ARCHITECTURE FRAMEWORK ARCHITECTURE DEVELOPMENT METHOD (TOGAF ADM) PADA PT SHAFCO MULTI TRADING', 'MURAHARTAWATY', 'RIDHA HANAFI', '2015', 'I GEDE MINDRAYASA', 'ANALISIS DAN PERANCANGAN DATA ARCHITECTURE DAN APPLICATION ARCHITECTURE MENGGUNAKAN THE OPEN GROUP ARCHITECTURE FRAMEWORK ARCHITECTURE DEVELOPMENT METHOD (TOGAF ADM) PADA PT SHAFCO MULTI TRADING', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(39, 'PLD_23', 'SISTEM INFORMASI', 'TS_1', 'PERANCANGAN TATA KELOLA TEKNOLOGI INFORMASI PT INDUSTRI TELEKOMUNIKASI INDONESIA (INTI) MENGGUNAKAN FRAMEWORK COBIT 5', 'MURAHARTAWATY', 'SONI FAJAR SURYA GUMILANG', '2015', 'IDA BAGUS KRISNA WEDANTA PRASADA', 'PERANCANGAN TATA KELOLA TEKNOLOGI INFORMASI PT INDUSTRI TELEKOMUNIKASI INDONESIA (INTI) MENGGUNAKAN FRAMEWORK COBIT 5 PADA DOMAIN DELIVER, SERVICE, AND SUPPORT', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(40, 'PLD_23', 'SISTEM INFORMASI', 'TS_1', 'PERANCANGAN TATA KELOLA TEKNOLOGI INFORMASI PT INDUSTRI TELEKOMUNIKASI INDONESIA (INTI) MENGGUNAKAN FRAMEWORK COBIT 5', 'MURAHARTAWATY', 'SONI FAJAR SURYA GUMILANG', '2015', 'KOMANG INDAH DESINTHYA WATI', 'PERANCANGAN TATA KELOLA TEKNOLOGI INFORMASI DI PT INTI MENGGUNAKAN FRAMEWORK COBIT 5 PADA DOMAIN BUILD, ACQUIRE, AND IMPLEMENT (BAI)', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(41, 'PLD_23', 'SISTEM INFORMASI', 'TS_1', 'PERANCANGAN TATA KELOLA TEKNOLOGI INFORMASI PT INDUSTRI TELEKOMUNIKASI INDONESIA (INTI) MENGGUNAKAN FRAMEWORK COBIT 5', 'MURAHARTAWATY', 'SONI FAJAR SURYA GUMILANG', '2015', 'I KETUT ADI PUTRA PRANANTA', 'PERANCANGAN TATA KELOLA DI PT INDUSTRI TELEKOMUNIKASI INDONESIA (INTI) MENGGUNAKAN FRAMEWORK COBIT 5 PADA DOMAIN ALIGN, PLAN, AND ORGANIZE (APO)', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(42, 'PLD_23', 'SISTEM INFORMASI', 'TS_1', 'PERANCANGAN TATA KELOLA TEKNOLOGI INFORMASI PT INDUSTRI TELEKOMUNIKASI INDONESIA (INTI) MENGGUNAKAN FRAMEWORK COBIT 5', 'MURAHARTAWATY', 'SONI FAJAR SURYA GUMILANG', '2015', 'BILLA ANANDA SUWANDI', 'PERANCANGAN TATA KELOLA TEKNOLOGI INFORMASI DI PT INTI (INDUSTRI TELEKOMUNIKASI INDONESIA) MENGGUNAKAN FRAMEWORK COBIT 5 PADA DOMAIN EDM DAN MEA', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(43, 'PLD_24', 'SISTEM INFORMASI', 'TS_1', 'PERANCANGAN ARSITEKTUR PADA PT TELEHOUSE ENGINEERING MENGGUNAKAN FRAMEWORK TOGAF ADM', 'MURAHARTAWATY', 'UMAR YUNAN, KURNIA SEPTO', '2015', 'ADITYA PRADANA PUTRA', 'PERANCANGAN ARSITEKTUR BISNIS PADA PT TELEHOUSE ENGINEERING MENGGUNAKAN FRAMEWORK TOGAF ADM', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(44, 'PLD_24', 'SISTEM INFORMASI', 'TS_1', 'PERANCANGAN ARSITEKTUR PADA PT TELEHOUSE ENGINEERING MENGGUNAKAN FRAMEWORK TOGAF ADM', 'MURAHARTAWATY', 'UMAR YUNAN, KURNIA SEPTO', '2015', 'ASHOF YUDHISTIRA M.P.', 'PERANCANGAN ARSITEKTUR DATA DAN APLIKASI PADA PT. TELEHOUSE ENGINEERING MENGGUNAKAN FRAMEWORK TOGAF ADM', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(45, 'PLD_25', 'SISTEM INFORMASI', 'TS_1', 'PENERAPAN SISTEM INFORMASI MENGGUNAKAN OPENERP PADA PT KHARISMA BUANA JAYA DENGAN METODE SPIRAL', 'NIA AMBARSARI', 'R. WAHJOE WITJAKSONO', '2015', 'PUSPITA AYU KARTIKA', 'PENERAPAN SISTEM PURCHASE MANAGEMENT MENGGUNAKAN OPENERP PADA PT KHARISMA BUANA JAYA DENGAN METODE SPIRAL', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(46, 'PLD_25', 'SISTEM INFORMASI', 'TS_1', 'PENERAPAN SISTEM INFORMASI MENGGUNAKAN OPENERP PADA PT KHARISMA BUANA JAYA DENGAN METODE SPIRAL', 'NIA AMBARSARI', 'R. WAHJOE WITJAKSONO', '2015', 'RIDHO ARYA DUTA MAHENDRA', 'PENERAPAN SISTEM AKUTANSI MENGGUNAKAN OPENERP PADA PT KHARISMA BUANA JAYA DENGAN METODE SPIRAL', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(47, 'PLD_25', 'SISTEM INFORMASI', 'TS_1', 'PENERAPAN SISTEM INFORMASI MENGGUNAKAN OPENERP PADA PT KHARISMA BUANA JAYA DENGAN METODE SPIRAL', 'NIA AMBARSARI', 'R. WAHJOE WITJAKSONO', '2015', 'ANNISA UTAMI', 'PENERAPAN SISTEM SALES MANAGEMENT MENGGUNAKAN OPENERP PADA PT KHARISMA BUANA JAYA DENGAN METODE SPIRAL', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(48, 'PLD_25', 'SISTEM INFORMASI', 'TS_1', 'PENERAPAN SISTEM INFORMASI MENGGUNAKAN OPENERP PADA PT KHARISMA BUANA JAYA DENGAN METODE SPIRAL', 'NIA AMBARSARI', 'R. WAHJOE WITJAKSONO', '2015', 'MUHAMMAD AULIA RENDY', 'PENERAPAN SISTEM WAREHOUSE MANAGEMENT MENGGUNAKAN OPENERP PADA PT KHARISMA BUANA JAYA DENGAN METODE SPIRAL', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(49, 'PLD_26', 'SISTEM INFORMASI', 'TS_1', 'MEMBANGUN SISTEM INFORMASI PERIZINAN ANGKUTAN UMUM DENGAN METODE AGILE DEVELOPMENT EXTREME PROGRAMMING (STUDI KASUS DINAS PERHUBUNGAN KABUPATEN BANDUNG BARAT)', 'NIA AMBARSARI', 'RIDHA HANAFI', '2015', 'OLAF ARMAN SEBASTIAN SIHOMBING', 'MEMBANGUN SISTEM INFORMASI PERIZINAN ANGKUTAN UMUM DENGAN METODE AGILE DEVELOPMENT EXTREME PROGRAMMING (STUDI KASUS DINAS PERHUBUNGAN KABUPATEN BANDUNG BARAT)', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(50, 'PLD_26', 'SISTEM INFORMASI', 'TS_1', 'MEMBANGUN SISTEM INFORMASI PERIZINAN ANGKUTAN UMUM DENGAN METODE AGILE DEVELOPMENT EXTREME PROGRAMMING (STUDI KASUS DINAS PERHUBUNGAN KABUPATEN BANDUNG BARAT)', 'NIA AMBARSARI', 'RIDHA HANAFI', '2015', 'FATHIYYAH NUR AZIZAH', 'MEMBANGUN WEB SERVICE PADA SISTEM INFORMASI PERIZINAN ANGKUTAN UMUM DENGAN METODE EXTREME PROGRAMMING (STUDI KASUS DINAS PERHUBUNGAN KABUPATEN BANDUNG BARAT)', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(51, 'PLD_26', 'SISTEM INFORMASI', 'TS_1', 'MEMBANGUN SISTEM INFORMASI PERIZINAN ANGKUTAN UMUM DENGAN METODE AGILE DEVELOPMENT EXTREME PROGRAMMING (STUDI KASUS DINAS PERHUBUNGAN KABUPATEN BANDUNG BARAT)', 'NIA AMBARSARI', 'RIDHA HANAFI', '2015', 'GREGORIUS PRAHASWARA DEWANTA', 'MEMBANGUN APLIKASI MOBILE BERBASIS ANDROID UNTUK INFORMASI PERIZINAN ANGKUTAN UMUM DENGAN METODE EXTREME PROGRAMMING (STUDI KASUS DINAS PERHUBUNGAN KABUPATEN BANDUNG BARAT)', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(52, 'PLD_27', 'SISTEM INFORMASI', 'TS_1', 'MEMBANGUN APLIKASI WEB BEASISWA BERBASIS CROWDFUNDING MENGGUNAKAN METODE ITERATIVE INCREMENTAL', 'NIA AMBARSARI', 'TAUFIK NUR ADI', '2015', 'RANDYANSYAH RACHMANALAN', 'MEMBANGUN APLIKASI WEB BEASISWA BERBASIS CROWDFUNDING PADA MODUL DONATUR DAN ADMINISTRATOR MENGGUNAKAN METODE ITERATIVE INCREMENTAL', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(53, 'PLD_27', 'SISTEM INFORMASI', 'TS_1', 'MEMBANGUN APLIKASI WEB BEASISWA BERBASIS CROWDFUNDING MENGGUNAKAN METODE ITERATIVE INCREMENTAL', 'NIA AMBARSARI', 'TAUFIK NUR ADI', '2015', 'MUHAMMAD AKBAR SATRIA', 'MEMBANGUN APLIKASI WEB BEASISWA BERBASIS CROWDFUNDING PADA MODUL PEMOHON BEASISWA DAN REVIEWER MENGGUNAKAN METODE ITERATIVE INCREMENTAL', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(54, 'PLD_28', 'SISTEM INFORMASI', 'TS_1', 'MEMBANGUN APLIKASI E-COMMERCE FOTO PREWED BERBASIS WEB MENGGUNAKAN MODEL CROWDSOURCING DAN METODE ITERATIVE INCREMENTAL (MODUL FOTOGRAFER)', 'NIA AMBARSARI', 'TAUFIK NUR ADI', '2015', 'MAHDY ARIEF', 'MEMBANGUN APLIKASI E-COMMERCE FOTO PREWED BERBASIS WEB MENGGUNAKAN MODEL CROWDSOURCING DAN METODE ITERATIVE INCREMENTAL', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(55, 'PLD_28', 'SISTEM INFORMASI', 'TS_1', 'MEMBANGUN APLIKASI E-COMMERCE FOTO PREWED BERBASIS WEB MENGGUNAKAN MODEL CROWDSOURCING DAN METODE ITERATIVE INCREMENTAL (MODUL FOTOGRAFER)', 'NIA AMBARSARI', 'TAUFIK NUR ADI', '2015', 'INSAN HARISH', 'MEMBANGUN APLIKASI E-COMMERCE FOTO PREWED BERBASIS WEB MENGGUNAKAN MODEL CROWDSOURCING DAN MENGGUNAKAN METODE ITERATIVE INCREMENTAL (MODUL PENGANTIN DAN FOTOSESI)', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(56, 'PLD_29', 'SISTEM INFORMASI', 'TS_1', 'MEMBANGUN WEB CROWDSOURCING E-PREPARATION SMB UNIVERSITAS TELKOM DENGAN MENGGUNAKAN METODE ITERATIVE INCREMENTAL', 'NIA AMBARSARI', 'TAUFIK NUR ADI', '2015', 'HOSIANA ARISKA SILALAHI', 'MEMBANGUN WEB E-PREPARATION SMB UNIVERSITAS TELKOM MODUL TRY OUT ONLINE DENGAN MENGGUNAKAN METODE ITERATIVE INCREMENTAL', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(57, 'PLD_29', 'SISTEM INFORMASI', 'TS_1', 'MEMBANGUN WEB CROWDSOURCING E-PREPARATION SMB UNIVERSITAS TELKOM DENGAN MENGGUNAKAN METODE ITERATIVE INCREMENTAL', 'NIA AMBARSARI', 'TAUFIK NUR ADI', '2015', 'DHANI RAHUTAMI PURWASTUTI', 'MEMBANGUN WEB CROWDSOURCING E-PREPARATION SMB UNIVERSITAS TELKOM MODUL PEMANTAPAN DENGAN MENGGUNAKAN METODE ITERATIVE INCREMENTAL', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(58, 'PLD_29', 'SISTEM INFORMASI', 'TS_1', 'MEMBANGUN WEB CROWDSOURCING E-PREPARATION SMB UNIVERSITAS TELKOM DENGAN MENGGUNAKAN METODE ITERATIVE INCREMENTAL', 'NIA AMBARSARI', 'TAUFIK NUR ADI', '2015', 'JOSES ADELWIN SITEPU', 'MEMBANGUN WEB CROWDSOURCING E-PREPARATION SMB UNIVERSITAS TELKOM MODUL E-LEARNING DENGAN MENGGUNAKAN METODE ITERATIVE INCREMENTAL', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(59, 'PLD_30', 'SISTEM INFORMASI', 'TS_1', 'MEMBANGUN WEB BERBASIS CROWDSOURCING UNTUK JUAL BELI SAMPAH PLASTIK DENGAN METODE ITERATIVE INCREMENTAL MENGGUNAKAN FRAMEWORK CODEIGNITER', 'NIA AMBARSARI', 'TAUFIK NUR ADI', '2015', 'AGUNG INSANI ALAM', 'Membangun Web Berbasis Crowdsourcing untuk Pembelian Sampah Plastik dengan Metode Iterative Incremental Menggunakan Framework Codeigniter', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(60, 'PLD_30', 'SISTEM INFORMASI', 'TS_1', 'MEMBANGUN WEB BERBASIS CROWDSOURCING UNTUK JUAL BELI SAMPAH PLASTIK DENGAN METODE ITERATIVE INCREMENTAL MENGGUNAKAN FRAMEWORK CODEIGNITER', 'NIA AMBARSARI', 'TAUFIK NUR ADI', '2015', 'MARTHA OKRINA', 'MEMBANGUN WEB BERBASIS CROWDSOURCING UNTUK PENJUALAN SAMPAH PLASTIK DENGAN METODE ITERATIVE INCREMENTAL MENGGUNAKAN FRAMEWORK CODEIGNITER', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(61, 'PLD_31', 'SISTEM INFORMASI', 'TS_1', 'PENERAPAN SISTEM INFORMASI BERBASIS OPENERP PADA CV HUDA JAYA DENGAN METODE RAPID APPLICATION DEVELOPMENT', 'SONI FAJAR SURYA GUMILANG', 'R. WAHJOE WITJAKSONO', '2015', 'FADHIL MUHAMMAD', 'PENERAPAN SISTEM PAYROLL BERBASIS OPENERP PADA CV HUDA JAYA DENGAN METODE RAPID APPLICATION DEVELOPMENT', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(62, 'PLD_31', 'SISTEM INFORMASI', 'TS_1', 'PENERAPAN SISTEM INFORMASI BERBASIS OPENERP PADA CV HUDA JAYA DENGAN METODE RAPID APPLICATION DEVELOPMENT', 'SONI FAJAR SURYA GUMILANG', 'R. WAHJOE WITJAKSONO', '2015', 'SHINTA SINDI NURYANI', 'PENERAPAN SISTEM ABSENSI BERBASIS OPENERP PADA CV HUDA JAYA DENGAN METODE RAPID APPLICATION DEVELOPMENT', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(63, 'PLD_32', 'SISTEM INFORMASI', 'TS_1', 'MEMBANGUN CROWDSOURCING DIET SEHAT MENGGUNAKAN METODOLOGI SCRUM', 'SONI FAJAR SURYA GUMILANG', 'TAUFIK NUR ADI', '2015', 'RIZKI DWI KURNIA DEWI', 'MEMBANGUN CROWDSOURCING DIET SEHAT MENGGUNAKAN METODE SCRUM (SISI PENYEDIA EXERCISE)', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(64, 'PLD_32', 'SISTEM INFORMASI', 'TS_1', 'MEMBANGUN CROWDSOURCING DIET SEHAT MENGGUNAKAN METODOLOGI SCRUM', 'SONI FAJAR SURYA GUMILANG', 'TAUFIK NUR ADI', '2015', 'FITRIA', 'MEMBANGUN CROWDSOURCING DIET SEHAT MENGGUNAKAN METODOLOGI SCRUM', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(65, 'PLD_33', 'SISTEM INFORMASI', 'TS_1', 'PENGEMBANGAN APLIKASI WEB BERBASIS CROWDSOURCING UNTUK MANAJEMEN PERSEWAAN MOBIL DENGAN MENGGUNAKAN METODOLOGI SCRUM', 'SONI FAJAR SURYA GUMILANG', 'TAUFIK NUR ADI', '2015', 'FAQIH PUTRA KUSUMAWIJAYA', 'PENGEMBANGAN APLIKASI WEB BERBASIS CROWDSOURCING UNTUK MANAJEMEN PERSEWAAN MOBIL DENGAN MENGGUNAKAN METODOLOGI SCRUM (SISI PERSEWAAN)', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(66, 'PLD_33', 'SISTEM INFORMASI', 'TS_1', 'PENGEMBANGAN APLIKASI WEB BERBASIS CROWDSOURCING UNTUK MANAJEMEN PERSEWAAN MOBIL DENGAN MENGGUNAKAN METODOLOGI SCRUM', 'SONI FAJAR SURYA GUMILANG', 'TAUFIK NUR ADI', '2015', 'GHOZIYAH HAITAN RACHMAN', 'PENGEMBANGAN APLIKASI WEB BERBASIS CROWDSOURCING UNTUK MANAJEMEN PERSEWAAN MOBIL DENGAN MENGGUNAKAN METODOLOGI SCRUM (SISI PELANGGAN)', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(67, 'PLD_44', 'SISTEM INFORMASI', 'TS', 'RANCANG BANGUN ODOO MODUL WAREHOUSE PADA GUDANG PT. TARUMATEX MENGGUNAKAN METODE RAPID APPLICATION DEVELOPMENT', 'DEDEN WITARSYAH', 'R. WAHJOE WITJAKSONO', '2016', 'PARASETIA ABU ADITYA', 'PERANCANGAN ODOO MODUL QUALITY CONTROL PADA INSPECTION PT. TARUMATEX MENGGUNAKAN METODE RAPID APPLICATION DEVELOPMENT', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(68, 'PLD_44', 'SISTEM INFORMASI', 'TS', 'RANCANG BANGUN ODOO MODUL WAREHOUSE PADA GUDANG PT. TARUMATEX MENGGUNAKAN METODE RAPID APPLICATION DEVELOPMENT', 'DEDEN WITARSYAH', 'R. WAHJOE WITJAKSONO', '2016', 'ALDI MUSTAFRI', 'PENGEMBANGAN ODOO MODUL WAREHOUSE PADA GUDANG PT. TARUMATEX MENGGUNAKAN METODE RAPID APPLICATION DEVELOPMENT', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(69, 'PLD_45', 'SISTEM INFORMASI', 'TS', 'RANCANG BANGUN E-COMMERCE TANDURAN UNTUK MENINGKATKAN DAYA JUAL PELAKU BISNIS PERTANIAN', 'IRFAN DARMAWAN', 'FAISHAL MUFIED AL-ANSHARY', '2016', 'ELVIRA LAILATUTH THOHIROH', 'PERANCANGAN MODUL ADMIN PADA E-COMMERCE TANDURAN BERBASIS MARKETPLACE UNTUK MENINGKATKAN KEPUASAN PELAYANAN BAGI PARA PELAKU PERTANIAN INDONESIA', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(70, 'PLD_45', 'SISTEM INFORMASI', 'TS', 'RANCANG BANGUN E-COMMERCE TANDURAN UNTUK MENINGKATKAN DAYA JUAL PELAKU BISNIS PERTANIAN', 'IRFAN DARMAWAN', 'FAISHAL MUFIED AL-ANSHARY', '2016', 'RAULIA RISKI', 'PERANCANGAN SISTEM DEBT-BASED CROWDFUNDING PADA E-COMMERCE TANDURAN UNTUK MEMBANTU MENGEMBANGKAN USAHA PETANI INDONESIA', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(71, 'PLD_45', 'SISTEM INFORMASI', 'TS', 'RANCANG BANGUN E-COMMERCE TANDURAN UNTUK MENINGKATKAN DAYA JUAL PELAKU BISNIS PERTANIAN', 'IRFAN DARMAWAN', 'FAISHAL MUFIED AL-ANSHARY', '2016', 'I MADE GELGEL WESNAWA PUTRA', 'PERANCANGAN E-COMMERCE TANDURAN BERBASIS MARKETPLACE UNTUK MENINGKATKAN DAYA JUAL PELAKU BISNIS PERTANIAN (MODUL PETANI)', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(72, 'PLD_45', 'SISTEM INFORMASI', 'TS', 'RANCANG BANGUN E-COMMERCE TANDURAN UNTUK MENINGKATKAN DAYA JUAL PELAKU BISNIS PERTANIAN', 'IRFAN DARMAWAN', 'FAISHAL MUFIED AL-ANSHARY', '2016', 'MUHAMMAD HILHAM RAMADHAN', 'PERANCANGAN E-COMMERCE TANDURAN BAGI PELAKU BISNIS PERTANIAN BERBASIS MARKETPLACE UNTUK MENINGKATKAN KEUNTUNGAN PENGGUNA (MODUL SUPPLIER)', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(73, 'PLD_46', 'SISTEM INFORMASI', 'TS', 'PENGEMBANGAN SISTEM RENTAL MOBIL ONLINE MENGGUNAKAN METODE ITERATIVE INCREMENTAL (www.rental-go.com)', 'IRFAN DARMAWAN', 'FAISHAL MUFIED AL-ANSHARY', '2016', 'FHATYA ANDINI', 'PENGEMBANGAN SISTEM BOOKING RENTAL MOBIL ONLINE MENGGUNAKAN METODE ITERATIVE INCREMENTAL (www.rental-go.com)', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(74, 'PLD_46', 'SISTEM INFORMASI', 'TS', 'PENGEMBANGAN SISTEM RENTAL MOBIL ONLINE MENGGUNAKAN METODE ITERATIVE INCREMENTAL (www.rental-go.com)', 'IRFAN DARMAWAN', 'FAISHAL MUFIED AL-ANSHARY', '2016', 'GINA MARIAM GUSTINA', 'PENGEMBANGAN SISTEM ADMINISTRASI RENTAL MOBIL ONLINE MENGGUNAKAN METODE ITERATIVE INCREMENTAL (www.rental-go.com)', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(75, 'PLD_47', 'SISTEM INFORMASI', 'TS', 'MEMBANGUN APLIKASI E-COMMERCE MANAJEMEN HUBUNGAN PELANGGAN UNTUK CV. PERCEKA MENGGUNAKAN FRAMEWORK LARAVEL DENGAN METODE ITERATIVE INCREMENTAL', 'IRFAN DARMAWAN', 'TAUFIK NUR ADI', '2016', 'CAKRA WARDHANA', 'MEMBANGUN APLIKASI E-COMMERCE UNTUK CV. PERCEKA MENGGUNAKAN FRAMEWORK LARAVEL DAN METODE ITERATIVE INCREMENTAL', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(76, 'PLD_47', 'SISTEM INFORMASI', 'TS', 'MEMBANGUN APLIKASI E-COMMERCE MANAJEMEN HUBUNGAN PELANGGAN UNTUK CV. PERCEKA MENGGUNAKAN FRAMEWORK LARAVEL DENGAN METODE ITERATIVE INCREMENTAL', 'IRFAN DARMAWAN', 'TAUFIK NUR ADI', '2016', 'TRIYADI YANUAR', 'MEMBANGUN APLIKASI MANAJEMEN HUBUNGAN PELANGGAN UNTUK CV. PERCEKA MENGGUNAKAN FRAMEWORK LARAVEL DENGAN METODE ITERATIVE INCREMENTAL', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(77, 'PLD_48', 'SISTEM INFORMASI', 'TS', 'PERANCANGAN DAN PEMBANGUNAN KNOWLEDGE MANAGEMENT SYSTEM DENGAN MENGGUNAKAN METODE ITERATIVE INCREMENTAL DI DORMITORY UNIVERSITAS TELKOM', 'LUCIANA ANDRAWINA', 'FAISHAL MUFIED AL-ANSHARY', '2016', 'ABDI ROBANA AGNIA', 'PERANCANGAN DAN PEMBANGUNAN KNOWLEDGE MANAGEMENT SYSTEM MODUL REGISTRASI DAN RESERVASI DENGAN MENGGUNAKAN METODE ITERATIVE INCREMENTAL DI DORMITORY UNIVERSITAS TELKOM', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(78, 'PLD_48', 'SISTEM INFORMASI', 'TS', 'PERANCANGAN DAN PEMBANGUNAN KNOWLEDGE MANAGEMENT SYSTEM DENGAN MENGGUNAKAN METODE ITERATIVE INCREMENTAL DI DORMITORY UNIVERSITAS TELKOM', 'LUCIANA ANDRAWINA', 'FAISHAL MUFIED AL-ANSHARY', '2016', 'MOCHAMAD ANNAFIE YANUAR HAKIM', 'PERANCANGAN DAN PEMBANGUNAN KNOWLEDGE MANAGEMENT SYSTEM MODUL PINDAH DAN TUKAR KAMAR DENGAN MENGGUNAKAN METODE ITERATIVE INCREMENTAL DI DORMITORY TELKOM UNIVERSITY', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(79, 'PLD_49', 'SISTEM INFORMASI', 'TS', 'PERANCANGAN EXPERT LOCATOR SYSTEM PADA PT. TELEKOMUNIKASI SELULAR MENGGUNAKAN METODE ITERATIVE INCREMENTAL', 'LUCIANA ANDRAWINA', 'MUHAMMAD AZANI HASIBUAN', '2016', 'ATIKA LUTHFIANI IFAN', 'PERANCANGAN EXPERT LOCATOR SYSTEM PADA PT. TELEKOMUNIKASI SELULAR MODUL USER MANAGEMENT MENGGUNAKAN METODE ITERATIVE INCREMENTAL', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(80, 'PLD_49', 'SISTEM INFORMASI', 'TS', 'PERANCANGAN EXPERT LOCATOR SYSTEM PADA PT. TELEKOMUNIKASI SELULAR MENGGUNAKAN METODE ITERATIVE INCREMENTAL', 'LUCIANA ANDRAWINA', 'MUHAMMAD AZANI HASIBUAN', '2016', 'SELLY LARASATI', 'PERANCANGAN EXPERT LOCATOR SYSTEM PADA PT. TELEKOMUNIKASI SELULAR MODUL GROUP DISCUSSION MENGGUNAKAN METODE ITERATIVE INCREMENTAL', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(81, 'PLD_49', 'SISTEM INFORMASI', 'TS', 'PERANCANGAN EXPERT LOCATOR SYSTEM PADA PT. TELEKOMUNIKASI SELULAR MENGGUNAKAN METODE ITERATIVE INCREMENTAL', 'LUCIANA ANDRAWINA', 'MUHAMMAD AZANI HASIBUAN', '2016', 'JAN FANDRO SIAHAAN', 'PERANCANGAN EXPERT LOCATOR SYSTEM PADA PT. TELEKOMUNIKASI SELULAR MODUL COMMUNITIES OF PRACTICE MENGGUNAKAN METODE ITERATIVE INCREMENTAL', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(82, 'PLD_49', 'SISTEM INFORMASI', 'TS', 'PERANCANGAN EXPERT LOCATOR SYSTEM PADA PT. TELEKOMUNIKASI SELULAR MENGGUNAKAN METODE ITERATIVE INCREMENTAL', 'LUCIANA ANDRAWINA', 'MUHAMMAD AZANI HASIBUAN', '2016', 'MAWADDAH TIFANI', 'PERANCANGAN EXPERT LOCATOR SYSTEM PADA PT. TELEKOMUNIKASI SELULAR MODUL COURSE AND JOURNAL MENGGUNAKAN METODE ITERATIVE INCREMENTAL', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(83, 'PLD_50', 'SISTEM INFORMASI', 'TS', 'PERANCANGAN SISTEM INFORMASI BERBASIS ODOO DENGAN SOFT SYSTEM METHODOLOGY DI RUMAH SAKIT MUHAMMADIYAH BANDUNG', 'LUCIANA ANDRAWINA', 'R. WAHJOE WITJAKSONO', '2016', 'ADAM RAMADHAN', 'PERANCANGAN SISTEM PROCUREMENT BERBASIS ODOO DENGAN SOFT SYSTEM METHODOLOGY DI RUMAH SAKIT MUHAMMADIYAH BANDUNG', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(84, 'PLD_50', 'SISTEM INFORMASI', 'TS', 'PERANCANGAN SISTEM INFORMASI BERBASIS ODOO DENGAN SOFT SYSTEM METHODOLOGY DI RUMAH SAKIT MUHAMMADIYAH BANDUNG', 'LUCIANA ANDRAWINA', 'R. WAHJOE WITJAKSONO', '2016', 'AHMAD AKBAR LINGGO M', 'PERANCANGAN SISTEM WAREHOUSE BERBASIS ODOO DENGAN SOFT SYSTEM METHODOLOGY DI RUMAH SAKIT MUHAMMADIYAH BANDUNG', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(85, 'PLD_50', 'SISTEM INFORMASI', 'TS', 'PERANCANGAN SISTEM INFORMASI BERBASIS ODOO DENGAN SOFT SYSTEM METHODOLOGY DI RUMAH SAKIT MUHAMMADIYAH BANDUNG', 'LUCIANA ANDRAWINA', 'R. WAHJOE WITJAKSONO', '2016', 'RIZA RAHMA PUTRI', 'PERANCANGAN SISTEM ASSET MANAGEMENT BERBASIS ODOO DENGAN SOFT SYSTEM METHODOLOGY DI RUMAH SAKIT MUHAMMADIYAH BANDUNG', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(86, 'PLD_51', 'SISTEM INFORMASI', 'TS', 'IMPLEMENTASI ENTERPRISE RESOURCE PLANNING MENGGUNAKAN SOFTWARE ODOO DENGAN METODE RAPID APPLICATION DEVELOPMENT (STUDI KASUS: PT. TRIKOMSA INDONESIA', 'MUHAMMAD AZANI HASIBUAN', 'FAISHAL MUFIED AL-ANSHARY', '2016', 'SYAHRIANDA', 'IMPLEMENTASI ENTERPRISE RESOURCE PLANNING MENGGUNAKAN SOFTWARE ODOO MODUL SALES MANAGEMENT METODE RAPID APPLICATION DEVELOPMENT (STUDI KASUS: PT. TRIKOMSA INDONESIA', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(87, 'PLD_51', 'SISTEM INFORMASI', 'TS', 'IMPLEMENTASI ENTERPRISE RESOURCE PLANNING MENGGUNAKAN SOFTWARE ODOO DENGAN METODE RAPID APPLICATION DEVELOPMENT (STUDI KASUS: PT. TRIKOMSA INDONESIA', 'MUHAMMAD AZANI HASIBUAN', 'FAISHAL MUFIED AL-ANSHARY', '2016', 'IDA BAGUS GEDE ROMA HARSANA PUTRA', 'IMPLEMENTASI ENTERPRISE RESOURCE PLANNING MENGGUNAKAN SOFTWARE ODOO MODUL WAREHOUSE MANAGEMENT METODE RAPID APPLICATION DEVELOPMENT (STUDI KASUS: PT.TRIKOMSA INDONESIA)', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(88, 'PLD_52', 'SISTEM INFORMASI', 'TS', 'PERANCANGAN TATA KELOLA MANAJEMEN LAYANAN TEKNOLOGI INFORMASI BERDASARKAN ITIL V3 DI PEMERINTAH KOTA BANDUNG', 'MURAHARTAWATY', 'LUTHFI RAMADANI', '2016', 'FIKROTUN NADIYYA', 'PERANCANGAN TATA KELOLA MANAJEMEN LAYANAN TEKNOLOGI INFORMASI BERDASARKAN ITIL V3 DOMAIN SERVICE DESIGN DI PEMERINTAH KOTA BANDUNG', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(89, 'PLD_52', 'SISTEM INFORMASI', 'TS', 'PERANCANGAN TATA KELOLA MANAJEMEN LAYANAN TEKNOLOGI INFORMASI BERDASARKAN ITIL V3 DI PEMERINTAH KOTA BANDUNG', 'MURAHARTAWATY', 'LUTHFI RAMADANI', '2016', 'LUKI AISHA KUSUMA WARDANI', 'PERANCANGAN TATA KELOLA MANAJEMEN LAYANAN TEKNOLOGI INFORMASI BERDASARKAN ITIL V3 DOMAIN SERVICE TRANSITION DAN SERVICE OPERATION DI PEMERINTAH KOTA BANDUNG', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(90, 'PLD_53', 'SISTEM INFORMASI', 'TS', 'ANALISIS DAN PERANCANGAN TATA KELOLA TI MENGGUNAKAN FRAMEWORK COBIT 4.1 : STUDI KASUS PT BIO FARMA (PERSERO)', 'MURAHARTAWATY', 'R. WAHJOE WITJAKSONO', '2016', 'AMANDA YUNIA ZAFARINA', 'ANALISIS DAN PERANCANGAN TATA KELOLA TI MENGGUNAKAN FRAMEWORK COBIT 4.1 DOMAIN PLAN AND ORGANISE (PO) DAN ACQUIRE AND IMPLEMENT (AI): STUDI KASUS PT BIO FARMA (PERSERO)', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(91, 'PLD_53', 'SISTEM INFORMASI', 'TS', 'ANALISIS DAN PERANCANGAN TATA KELOLA TI MENGGUNAKAN FRAMEWORK COBIT 4.1 : STUDI KASUS PT BIO FARMA (PERSERO)', 'MURAHARTAWATY', 'R. WAHJOE WITJAKSONO', '2016', 'INAYATUL MAGHFIROH', 'ANALISIS DAN PERANCANGAN TATA KELOLA TI MENGGUNAKAN FRAMEWORK COBIT 4.1 DOMAIN DELIVER AND SUPPORT (DS) DAN MONITOR AND EVALUATE (ME) : STUDI KASUS PT BIO FARMA (PERSERO)', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(92, 'PLD_54', 'SISTEM INFORMASI', 'TS', 'PERANCANGAN SISTEM INFORMASI PADA LIPI KEBUN RAYA CIBODAS MENGGUNAKAN METODE WATERFALL', 'M. TEGUH KURNIAWAN', 'ADITYAS WIDJAJARTO', '2016', 'RIKI RAMDHANI', 'Perancangan Infrastruktur jaringan wired pada LIPI Kebun Raya Cibodas Cianjur Menggunakan Metode Network Development Life Cycle (NDLC)', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(93, 'PLD_54', 'SISTEM INFORMASI', 'TS', 'PERANCANGAN SISTEM INFORMASI PADA LIPI KEBUN RAYA CIBODAS MENGGUNAKAN METODE WATERFALL', 'M. TEGUH KURNIAWAN', 'ADITYAS WIDJAJARTO', '2016', 'NAUFAL AFRA FIRDAUS', 'PERANCANGAN SISTEM INFORMASI GAJI PADA KEBUN RAYA LIPI CIBODAS MENGGUNAKAN METODE WATERFALL', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(94, 'PLD_54', 'SISTEM INFORMASI', 'TS', 'PERANCANGAN SISTEM INFORMASI PADA LIPI KEBUN RAYA CIBODAS MENGGUNAKAN METODE WATERFALL', 'M. TEGUH KURNIAWAN', 'ADITYAS WIDJAJARTO', '2016', 'RIDEL EZRI SAMBOW', 'PERANCANGAN SISTEM INFORMASI FILE-SHARING DAN ABSENSI DENGAN ALAT ABSENSI TIMETECH F10+ PADA LIPI KEBUN RAYA CIBODAS MENGGUNAKAN METODE WATERFALL', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(95, 'PLD_55', 'SISTEM INFORMASI', 'TS', 'PENERAPAN SISTEM ERP BERBASIS ODOO DENGAN METODE SPIRAL PADA PT. PROGRESSIO INDONESIA', 'M. TEGUH KURNIAWAN', 'R. WAHJOE WITJAKSONO', '2016', 'I KOMANG JAKA AKSARA WIGUNA', 'PENERAPAN SISTEM ERP AKTIVITAS PENJUALAN DAN DISTRIBUSI BERBASIS ODOO DENGAN METODE SPIRAL (STUDI KASUS PADA PT. PROGRESSIO INDONESIA)', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(96, 'PLD_55', 'SISTEM INFORMASI', 'TS', 'PENERAPAN SISTEM ERP BERBASIS ODOO DENGAN METODE SPIRAL PADA PT. PROGRESSIO INDONESIA', 'M. TEGUH KURNIAWAN', 'R. WAHJOE WITJAKSONO', '2016', 'SHELVIA ARDIAN PERDANA', 'PENERAPAN SISTEM ERP AKTIVITAS PEMBELIAN BERBASIS ODOO DENGAN METODE SPIRAL PADA PT. PROGRESSIO INDONESIA', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(97, 'PLD_55', 'SISTEM INFORMASI', 'TS', 'PENERAPAN SISTEM ERP BERBASIS ODOO DENGAN METODE SPIRAL PADA PT. PROGRESSIO INDONESIA', 'M. TEGUH KURNIAWAN', 'R. WAHJOE WITJAKSONO', '2016', 'MUHAMMAD AMMAR RIFQI', 'PENERAPAN SISTEM ERP AKTIVITAS PRODUKSI BERBASIS ODOO DENGAN METODE SPIRAL (Studi Kasus pada PT. Progessio Indonesia)', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(98, 'PLD_56', 'SISTEM INFORMASI', 'TS', 'ANALISIS DAN PERANCANGAN INFRASTRUKTUR JARINGAN KOMPUTER DI PEMERINTAHAN KABUPATEN BANDUNG', 'M. TEGUH KURNIAWAN', 'UMAR YUNAN KURNIA SEPTO HERDIYANTO', '2016', 'ARIES ARIEFFANDY SUMARSO', 'ANALISIS DAN PERANCANGAN KEAMANAN INFRASTRUKTUR JARINGAN KOMPUTER DI PEMERINTAHAN KABUPATEN BANDUNG DENGAN METODE NDLC DAN STANDAR ISO/IEC 27000 SERIES', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(99, 'PLD_56', 'SISTEM INFORMASI', 'TS', 'ANALISIS DAN PERANCANGAN INFRASTRUKTUR JARINGAN KOMPUTER DI PEMERINTAHAN KABUPATEN BANDUNG', 'M. TEGUH KURNIAWAN', 'UMAR YUNAN KURNIA SEPTO HERDIYANTO', '2016', 'FAISAL RIZIQ AKBAR', 'PERANCANGAN INFRASTRUKTUR JARINGAN FIBER OPTIK DENGAN KONFIGURASI TEKNOLOGI FTTX MENGGUNAKAN METODE NDLC DI KABUPATEN BANDUNG', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(100, 'PLD_56', 'SISTEM INFORMASI', 'TS', 'ANALISIS DAN PERANCANGAN INFRASTRUKTUR JARINGAN KOMPUTER DI PEMERINTAHAN KABUPATEN BANDUNG', 'M. TEGUH KURNIAWAN', 'UMAR YUNAN KURNIA SEPTO HERDIYANTO', '2016', 'AKBAR MAULANA DAFIK TORICA', 'Analisis dan Perancangan Optimasi Infrastruktur Jaringan Wired Dengan Pendekatan Green IT Computing Di Pemerintah Kabupaten Bandung Menggunakan Metode Network Development Life Cycle', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(101, 'PLD_56', 'SISTEM INFORMASI', 'TS', 'ANALISIS DAN PERANCANGAN INFRASTRUKTUR JARINGAN KOMPUTER DI PEMERINTAHAN KABUPATEN BANDUNG', 'M. TEGUH KURNIAWAN', 'UMAR YUNAN KURNIA SEPTO HERDIYANTO', '2016', 'NAZWAR SYAMSU', 'PERANCANGAN INFRASTRUKTUR TEKNOLOGI JARINGAN WIRELESS MENGGUNAKAN METODOLOGI NETWORK DEVELOPMENT LIFE CYCLE (NDLC) DENGAN PENDEKATAN GREEN TI DI RURAL AREA (STUDI KASUS : KABUPATEN BANDUNG)', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(102, 'PLD_56', 'SISTEM INFORMASI', 'TS', 'ANALISIS DAN PERANCANGAN INFRASTRUKTUR JARINGAN KOMPUTER DI PEMERINTAHAN KABUPATEN BANDUNG', 'M. TEGUH KURNIAWAN', 'UMAR YUNAN KURNIA SEPTO HERDIYANTO', '2016', 'KARNANDO NABABAN', 'PERANCANGAN INFRASTRUKTUR JARINGAN KOMPUTER DENGAN MENGGUNAKAN FIBER OPTIK SEBAGAI PENGEMBANGAN INFRASRUKTUR TEKNOLOGI INFORMASI MENGGUNAKAN METODE NETWORK DEVELOPMENT LIFE CYCLE (NDLC) DI KABUPATEN BANDUNG', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(103, 'PLD_57', 'SISTEM INFORMASI', 'TS', 'PENGEMBANGAN SISTEM INFORMASI PADA WEBSITE SAHABATKERTAS.COM', 'NIA AMBARSARI', 'ILHAM PERDANA', '2016', 'FAUZAN MEDISON ADRYAN', 'PENGEMBANGAN SISTEM PENJUALAN PADA WEBSITE SAHABATKERTAS.COM', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(104, 'PLD_57', 'SISTEM INFORMASI', 'TS', 'PENGEMBANGAN SISTEM INFORMASI PADA WEBSITE SAHABATKERTAS.COM', 'NIA AMBARSARI', 'ILHAM PERDANA', '2016', 'ARIZONA DWI FAJARWATI', 'PENGEMBANGAN SISTEM INFORMASI PENGGALANGAN DANA PADA WEBSITE SAHABATKERTAS.COM', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(105, 'PLD_57', 'SISTEM INFORMASI', 'TS', 'PENGEMBANGAN SISTEM INFORMASI PADA WEBSITE SAHABATKERTAS.COM', 'NIA AMBARSARI', 'ILHAM PERDANA', '2016', 'GALANG TANAH MERDEKA', 'PENGEMBANGAN SISTEM INFORMASI PENGUMPULAN LIMBAH PADA WEBSITE SAHABATKERTAS.COM', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(106, 'PLD_58', 'SISTEM INFORMASI', 'TS', 'MEMBANGUN SISTEM INFORMASI E-SCHOOL BERBASIS QR CODE MENGGUNAKAN METODE EXTREME PROGRAMMING DI SMPN 13 BANDUNG', 'NIA AMBARSARI', 'MUHAMMAD AZANI HASIBUAN', '2016', 'TIMBUL PRAWIRA GULTOM', 'MEMBANGUN SISTEM INFORMASI PERPUSTAKAAN E-SCHOOL BERBASIS QR CODE MENGGUNAKAN METODE EXTREME PROGRAMMING DI SMPN 13 BANDUNG', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(107, 'PLD_58', 'SISTEM INFORMASI', 'TS', 'MEMBANGUN SISTEM INFORMASI E-SCHOOL BERBASIS QR CODE MENGGUNAKAN METODE EXTREME PROGRAMMING DI SMPN 13 BANDUNG', 'NIA AMBARSARI', 'MUHAMMAD AZANI HASIBUAN', '2016', 'MOCHAMAD THARIQ JANUAR', 'MEMBANGUN SISTEM INFORMASI AKADEMIK PADA E-SCHOOL BERBASIS QR-CODE DI SMP NEGERI 13 BANDUNG DENGAN MENGGUNAKAN METODE EXTREME PROGRAMMING', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(108, 'PLD_58', 'SISTEM INFORMASI', 'TS', 'MEMBANGUN SISTEM INFORMASI E-SCHOOL BERBASIS QR CODE MENGGUNAKAN METODE EXTREME PROGRAMMING DI SMPN 13 BANDUNG', 'NIA AMBARSARI', 'MUHAMMAD AZANI HASIBUAN', '2016', 'ERLIN SUSILOWATI', 'MEMBANGUN SISTEM INFORMASI MANAJEMEN DATA PEGAWAI DAN PENILAIAN KINERJA GURU PADA E-SCHOOL DENGAN MENERAPKAN TEKNOLOGI QR-CODE PADA KARTU PEGAWAI DI SEKOLAH MENENGAH PERTAMA NEGERI 13 BANDUNG MENGGUNAKAN METODE EXTREME PROGRAMMING', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(109, 'PLD_58', 'SISTEM INFORMASI', 'TS', 'MEMBANGUN SISTEM INFORMASI E-SCHOOL BERBASIS QR CODE MENGGUNAKAN METODE EXTREME PROGRAMMING DI SMPN 13 BANDUNG', 'NIA AMBARSARI', 'MUHAMMAD AZANI HASIBUAN', '2016', 'MUCHAMAD REZA JULIANSYAH', 'MEMBANGUN SISTEM INFORMASI KESISWAAN DAN BK PADA E-SCHOOL DENGAN MENERAPKAN TEKNOLOGI QR CODE PADA KARTU SISWA DI SMP NEGERI 13 BANDUNG MENGGUNAKAN METODE EXTREME PROGRAMMING', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(110, 'PLD_59', 'SISTEM INFORMASI', 'TS', 'PENGEMBANGAN SISTEM ERP MENGGUNAKAN ODOO PADA PT. PUTRI DAYA USAHATAMA DENGAN METODE ASAP', 'RD. ROHMAT SAEDUDIN', 'R. WAHJOE WITJAKSONO', '2016', 'INTAN DWI ARIESTA PUTRI', 'PENGEMBANGAN SISTEM PROCUREMENT MENGGUNAKAN ADEMPIERE PADA PT.PDU DENGAN METODA ASAP', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(111, 'PLD_59', 'SISTEM INFORMASI', 'TS', 'PENGEMBANGAN SISTEM ERP MENGGUNAKAN ODOO PADA PT. PUTRI DAYA USAHATAMA DENGAN METODE ASAP', 'RD. ROHMAT SAEDUDIN', 'R. WAHJOE WITJAKSONO', '2016', 'UGI CHANDRA WIGUNA', 'PENGEMBANGAN SISTEM ERP SALES MANAGEMENT MENGGUNAKAN ODOO PADA PT PUTRI DAYA USAHATAMA DENGAN METODE ASAP', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(112, 'PLD_59', 'SISTEM INFORMASI', 'TS', 'PENGEMBANGAN SISTEM ERP MENGGUNAKAN ODOO PADA PT. PUTRI DAYA USAHATAMA DENGAN METODE ASAP', 'RD. ROHMAT SAEDUDIN', 'R. WAHJOE WITJAKSONO', '2016', 'MOCHAMAD HAFIZ KURNIAWAN', 'PENGEMBANGAN SISTEM ERP WAREHOUSE MANAGEMENT MENGGUNAKAN ODOO PADA PT. PUTRI DAYA USAHATAMA DENGAN METODE ASAP', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(113, 'PLD_60', 'SISTEM INFORMASI', 'TS', 'IMPLEMENTASI ERP BERBASIS ODOO PADA PT. PRIMARINDO ASIA INFRASTRUCTURE Tbk DENGAN METODOLOGI ASAP', 'RD. ROHMAT SAEDUDIN', 'R. WAHJOE WITJAKSONO', '2016', 'YUANIKA INDANEA', 'IMPLEMENTASI SISTEM PRODUKSI BERBASIS ODOO PADA PT. PRIMARINDO ASIA INFRASTRUCTURE TBK DENGAN METODOLOGI ASAP', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(114, 'PLD_60', 'SISTEM INFORMASI', 'TS', 'IMPLEMENTASI ERP BERBASIS ODOO PADA PT. PRIMARINDO ASIA INFRASTRUCTURE Tbk DENGAN METODOLOGI ASAP', 'RD. ROHMAT SAEDUDIN', 'R. WAHJOE WITJAKSONO', '2016', 'FIRDAYAKUSMAWARNI', 'IMPLEMENTASI SISTEM PURCHASING DAN WAREHOUSE MANAGEMENT BERBASIS ODOO PADA PT. PRIMARINDO ASIA INFRASTRUCTURE Tbk DENGAN METODOLOGI ASAP', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(115, 'PLD_61', 'SISTEM INFORMASI', 'TS', 'PENGEMBANGAN ERP BERBASIS ODOO DENGAN METODE ACCELERATED SAP PADA INGLORIOUS INDUSTRIES', 'RD. ROHMAT SAEDUDIN', 'R. WAHJOE WITJAKSONO', '2016', 'FIEGA DWI NOVWARI', 'PENGEMBANGAN MODUL PURCHASE DAN WAREHOUSE MANAGEMENT BERBASIS ODOO DENGAN METODE ACCELERATED SAP PADA INGLORIOUS INDUSTRIES', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(116, 'PLD_61', 'SISTEM INFORMASI', 'TS', 'PENGEMBANGAN ERP BERBASIS ODOO DENGAN METODE ACCELERATED SAP PADA INGLORIOUS INDUSTRIES', 'RD. ROHMAT SAEDUDIN', 'R. WAHJOE WITJAKSONO', '2016', 'VEGI FRANSISCA', 'PENGEMBANGAN MODUL MANUFACTURING BERBASIS ODOO DENGAN METODE ACCELERATED SAP PADA INGLORIOUS INDUSTRIES', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(117, 'PLD_61', 'SISTEM INFORMASI', 'TS', 'PENGEMBANGAN ERP BERBASIS ODOO DENGAN METODE ACCELERATED SAP PADA INGLORIOUS INDUSTRIES', 'RD. ROHMAT SAEDUDIN', 'R. WAHJOE WITJAKSONO', '2016', 'IBRAHIM HANIF ALKHALIL', 'PENGEMBANGAN SISTEM SALES MANAGEMENT BERBASIS ODOO DENGAN METODE ACCELERATED SAP PADA INGLORIOUS INDUSTRIES', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(118, 'PLD_62', 'SISTEM INFORMASI', 'TS', 'PENERAPAN ERP ODOO PADA PT. PRIMARINDO ASIA INFRASTRUCTURE TBK. DENGAN METODE RAPID APPLICATION DEVELOPMENT', 'R. WAHJOE WITJAKSONO', 'FAISHAL MUFIED AL-ANSHARY', '2016', 'ERLA AYU PERTIWI', 'Penerapan SIstem ERP Performance Appraisal Berbasis Odoo Pada PT Primarindo Asia Infrastructure Tbk Dengan Metode RAD', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(119, 'PLD_62', 'SISTEM INFORMASI', 'TS', 'PENERAPAN ERP ODOO PADA PT. PRIMARINDO ASIA INFRASTRUCTURE TBK. DENGAN METODE RAPID APPLICATION DEVELOPMENT', 'R. WAHJOE WITJAKSONO', 'FAISHAL MUFIED AL-ANSHARY', '2016', 'INDRA ROLANDO SINAGA', 'PENERAPAN SISTEM INFORMASI REKRUTMEN BERBASIS ERP ODOO PADA PT. PRIMARINDO ASIA INFRASTRUCTURE TBK. DENGAN METODE RAPID APPLICATION DEVELOPMENT', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(120, 'PLD_62', 'SISTEM INFORMASI', 'TS', 'PENERAPAN ERP ODOO PADA PT. PRIMARINDO ASIA INFRASTRUCTURE TBK. DENGAN METODE RAPID APPLICATION DEVELOPMENT', 'R. WAHJOE WITJAKSONO', 'FAISHAL MUFIED AL-ANSHARY', '2016', 'KEVIN ROHNI GOKLAS SINAGA', 'PENERAPAN SISTEM ADMINISTRASI PAYROLL BERBASIS ODOO DI PT PRIMARINDO ASIA INFRASTRUCTURE Tbk DENGAN METODE RAPID APPLICATION DEVELOPMENT', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(121, 'PLD_63', 'SISTEM INFORMASI', 'TS', 'PENGEMBANGAN SISTEM INFORMASI  ERP ODOO DENGAN METODE RAPID APPLICATION DEVELOPMENT DI PT ADETEX FILAMENT', 'R. WAHJOE WITJAKSONO', 'FAISHAL MUFIED AL-ANSHARY', '2016', 'SITI ALMIRA DANIA', 'PENGEMBANGAN SISTEM INFORMASI REKRUTMEN MENGGUNAKAN KONSEP ENTERPRISE RESOURCE PLANNING DENGAN METODE RAPID APPLICATION DEVELOPMENT DI PT ADETEX FILAMENT', NULL, 'TA', NULL, 1, NULL, NULL, NULL);
INSERT INTO `tb_peldosen_ta` (`no`, `id_peldos`, `prodi`, `no_ts`, `judul_penelitian_dosen`, `nama_dosenketua`, `nama_dosenanggota`, `tahun`, `nama_mahasiswa`, `judul_ta_mahasiswa`, `bukti_laporan_akhir`, `keterangan`, `sts_valid`, `id_user`, `file_name`, `file_type`, `file_date`) VALUES
(122, 'PLD_63', 'SISTEM INFORMASI', 'TS', 'PENGEMBANGAN SISTEM INFORMASI  ERP ODOO DENGAN METODE RAPID APPLICATION DEVELOPMENT DI PT ADETEX FILAMENT', 'R. WAHJOE WITJAKSONO', 'FAISHAL MUFIED AL-ANSHARY', '2016', 'DINTA AYU SHAVIRA', 'Pengembangan Sistem Informasi Performance Appraisal Menggunakan Konsep Enterprise Resource Planning Pada PT.Adetex Filament Dengan Metode Rapid Application', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(123, 'PLD_63', 'SISTEM INFORMASI', 'TS', 'PENGEMBANGAN SISTEM INFORMASI  ERP ODOO DENGAN METODE RAPID APPLICATION DEVELOPMENT DI PT ADETEX FILAMENT', 'R. WAHJOE WITJAKSONO', 'FAISHAL MUFIED AL-ANSHARY', '2016', 'AIDIYA SAFIRA', 'PENGEMBANGAN SISTEM INFORMASI EMPLOYEE MANAGEMENT MENGGUNAKAN KONSEP ENTERPRISE RESOURCE PLANNING PADA PT.ADETEX FILAMENT DENGAN METODE RAPID APPLICATION DEVELOPMENT', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(124, 'PLD_64', 'SISTEM INFORMASI', 'TS', 'PENERAPAN SISTEM ERP MENGGUNAKAN ODOO PADA PERUSAHAAN TEKSTIL MENGGUNAKAN METODE ACCELERATED SAP (Studi Kasus PT. SAMPOERNA JAYA SENTOSA)', 'R. WAHJOE WITJAKSONO', 'NIA AMBARSARI', '2016', 'WIDYASARI OKTAVIANI', 'PENERAPAN SISTEM ERP MANUFACTURING MENGGUNAKAN ODOO PADA PERUSAHAAN TEKSTIL MENGGUNAKAN METODE ACCELERATED SAP (Studi Kasus PT. SAMPOERNA JAYA SENTOSA)', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(125, 'PLD_64', 'SISTEM INFORMASI', 'TS', 'PENERAPAN SISTEM ERP MENGGUNAKAN ODOO PADA PERUSAHAAN TEKSTIL MENGGUNAKAN METODE ACCELERATED SAP (Studi Kasus PT. SAMPOERNA JAYA SENTOSA)', 'R. WAHJOE WITJAKSONO', 'NIA AMBARSARI', '2016', 'ROBBY PANGESTU', 'PENERAPAN SISTEM ERP SALES MANAGEMENT MENGGUNAKAN ODOO PADA PERUSAHAAN TEKSTIL DENGAN METODE ACCELERATED SAP (STUDI KASUS PT. SAMPOERNA JAYA SENTOSA)', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(126, 'PLD_64', 'SISTEM INFORMASI', 'TS', 'PENERAPAN SISTEM ERP MENGGUNAKAN ODOO PADA PERUSAHAAN TEKSTIL MENGGUNAKAN METODE ACCELERATED SAP (Studi Kasus PT. SAMPOERNA JAYA SENTOSA)', 'R. WAHJOE WITJAKSONO', 'NIA AMBARSARI', '2016', 'MASANGGA FERBIYANA MUKTI', 'PENERAPAN SISTEM ERP PURCHASE MANAGEMENT MENGGUNAKAN ODOO PADA PERUSAHAAN TEKSTIL DENGAN METODE ACCELERATED SAP (STUDI KASUS PT. SAMPOERNA JAYA SENTOSA)', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(127, 'PLD_65', 'SISTEM INFORMASI', 'TS', 'PENGEMBANGAN SISTEM INFORMASI BERBASIS ERP MENGGUNAKAN SAP DENGAN METODE ASAP DI PT. LEN INDUSTRI', 'R. WAHJOE WITJAKSONO', 'NIA AMBARSARI', '2016', 'CLARISSA DILASARI SETIAWAN', 'PENGEMBANGAN SISTEM INFORMASI BERBASIS ENTERPRISE RESOURCE PLANNING PADA PROSES PRODUKSI MENGGUNAKAN SAP DENGAN METODE ASAP DI PT. LEN INDUSTRI', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(128, 'PLD_65', 'SISTEM INFORMASI', 'TS', 'PENGEMBANGAN SISTEM INFORMASI BERBASIS ERP MENGGUNAKAN SAP DENGAN METODE ASAP DI PT. LEN INDUSTRI', 'R. WAHJOE WITJAKSONO', 'NIA AMBARSARI', '2016', 'DELIS SEPTIANTI BALGIS', 'Pengembangan Sistem Informasi Berbasis Enterprise Resource Planning pada Proses Perencanaan dan Pengendalian Produksi Menggunakan SAP dengan Metode ASAP di PT. Len Industri', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(129, 'PLD_65', 'SISTEM INFORMASI', 'TS', 'PENGEMBANGAN SISTEM INFORMASI BERBASIS ERP MENGGUNAKAN SAP DENGAN METODE ASAP DI PT. LEN INDUSTRI', 'R. WAHJOE WITJAKSONO', 'NIA AMBARSARI', '2016', 'I MADE TEKAD KALIMANTARA', 'PENGEMBANGAN SISTEM INFORMASI BERBASIS ENTERPRISE RESOURCE PLANNING MODUL QUALITY MANAGEMENT MENGGUNAKAN SAP DENGAN METODE ASAP PADA BAGIAN PRODUKSI PT. LEN INDUSTRI', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(130, 'PLD_65', 'SISTEM INFORMASI', 'TS', 'PENGEMBANGAN SISTEM INFORMASI BERBASIS ERP MENGGUNAKAN SAP DENGAN METODE ASAP DI PT. LEN INDUSTRI', 'R. WAHJOE WITJAKSONO', 'NIA AMBARSARI', '2016', 'DWI PRATAMA', 'PENGEMBANGAN SISTEM INFORMASI BERBASIS ENTERPRISE RESOURCE PLANNING MENGGUNAKAN SAP MODUL PLANT MAINTENANCE DENGAN METODE ASAP DI PT. LEN INDUSTRI (PERSERO)', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(131, 'PLD_66', 'SISTEM INFORMASI', 'TS', 'MEMBANGUN APLIKASI KEMAHASISWAAN BERBASIS WEB MENGGUNAKAN METODE ITERATIVE AND INCREMENTAL', 'SONI FAJAR SURYA GUMILANG', 'MUHAMMAD AZANI HASIBUAN', '2016', 'Reza Harli Saputra', 'MEMBANGUN APLIKASI KEMAHASISWAAN BERBASIS WEB MODUL PENGELOLAAN LOMBA DAN BEASISWA PRESTASI PADA SISI KEMAHASISWAAN MENGGUNAKAN METODE ITERATIVE AND INCREMENTAL', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(132, 'PLD_66', 'SISTEM INFORMASI', 'TS', 'MEMBANGUN APLIKASI KEMAHASISWAAN BERBASIS WEB MENGGUNAKAN METODE ITERATIVE AND INCREMENTAL', 'SONI FAJAR SURYA GUMILANG', 'MUHAMMAD AZANI HASIBUAN', '2016', 'ANTON ILMIAR WINDRISWARA', 'MEMBANGUN APLIKASI KEMAHASISWAAN BERBASIS WEB MODUL PENGELOLAAN LOMBA DAN BEASISWA PRESTASI PADA SISI MAHASISWA MENGGUNAKAN METODE ITERATIVE AND INCREMENTAL (Studi Kasus Fakultas Rekayasa Industri Universitas Telkom)', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(133, 'PLD_66', 'SISTEM INFORMASI', 'TS', 'MEMBANGUN APLIKASI KEMAHASISWAAN BERBASIS WEB MENGGUNAKAN METODE ITERATIVE AND INCREMENTAL', 'SONI FAJAR SURYA GUMILANG', 'MUHAMMAD AZANI HASIBUAN', '2016', 'ANISATUN NAFIAH', 'MEMBANGUN APLIKASI KEMAHASISWAAN BERBASIS WEB MODUL PENGELOLAAN KEGIATAN HIMPUNAN PADA SISI HIMPUNAN MENGGUNAKAN METODE ITERATIVE AND INCREMENTAL (Studi Kasus Fakultas Rekayasa Industri Universitas Telkom)', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(134, 'PLD_66', 'SISTEM INFORMASI', 'TS', 'MEMBANGUN APLIKASI KEMAHASISWAAN BERBASIS WEB MENGGUNAKAN METODE ITERATIVE AND INCREMENTAL', 'SONI FAJAR SURYA GUMILANG', 'MUHAMMAD AZANI HASIBUAN', '2016', 'NURRIDA AINI ZUHROH', 'MEMBANGUN APLIKASI KEMAHASISWAAN BERBASIS WEB MODUL PENGELOLAAN KEGIATAN HIMPUNAN PADA SISI KEMAHASISWAAN MENGGUNAKAN METODE ITERATIVE DAN INCREMENTAL (Studi Kasus : Fakultas Rekayasa Industri Universitas Telkom)', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(135, 'PLD_67', 'SISTEM INFORMASI', 'TS', 'PERANCANGAN SISTEM INFORMASI PADA AGROBISNIS DAN PARIWISATA DESA CIBODAS KECAMATAN LEMBANG MENGGUNAKAN METODE ITERATIVE INCREMENTAL', 'SONI FAJAR SURYA GUMILANG', 'NUR ICHSAN UTAMA', '2016', 'CAHYA NOFANDIYAN PUTRA', 'PERANCANGAN APLIKASI WEB PADA AGROBISNIS DAN PARIWISATA DESA CIBODAS KECAMATAN LEMBANG MENGGUNAKAN METODE ITERATIVE INCREMENTAL (Sisi Penjual)', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(136, 'PLD_67', 'SISTEM INFORMASI', 'TS', 'PERANCANGAN SISTEM INFORMASI PADA AGROBISNIS DAN PARIWISATA DESA CIBODAS KECAMATAN LEMBANG MENGGUNAKAN METODE ITERATIVE INCREMENTAL', 'SONI FAJAR SURYA GUMILANG', 'NUR ICHSAN UTAMA', '2016', 'ADNAN BAHARRUDIN FANANI', 'PERANCANGAN APLIKASI MOBILE PADA AGRIBISNIS DAN PARIWISATA MASYARAKAT DESA CIBODAS KECAMATAN LEMBANG MENGGUNAKAN METODE ITERATIVE INCREMENTAL (SISI PELANGGAN)', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(137, 'PLD_67', 'SISTEM INFORMASI', 'TS', 'PERANCANGAN SISTEM INFORMASI PADA AGROBISNIS DAN PARIWISATA DESA CIBODAS KECAMATAN LEMBANG MENGGUNAKAN METODE ITERATIVE INCREMENTAL', 'SONI FAJAR SURYA GUMILANG', 'NUR ICHSAN UTAMA', '2016', 'SLAMET MAMAT RACHMAT', 'PERANCANGAN APLIKASI WEB PADA AGROBISNIS DAN PARIWISATA DESA CIBODAS MARIBAYA KECAMATAN LEMBANG MENGGUNAKAN METODE ITERATIVE INCREMENTAL (SISI PEMBELI)', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(138, 'PLD_68', 'SISTEM INFORMASI', 'TS', 'PERANCANGAN APLIKASI SOSIAL ENTERPRISE PADA PERANGKAT ANDROID DENGAN MENGGUNAKAN SCRUM DEVELOPMENT', 'SONI FAJAR SURYA GUMILANG', 'NUR ICHSAN UTAMA', '2016', 'AGUNG CANDRA DUTIYA PURWANTA', 'PERANCANGAN APPLICATION PROGRAMMING INTERFACE UNTUK APLIKASI SOSIAL ENTERPRISE PADA PERANGKAT ANDROID DENGAN MENGGUNAKAN SCRUM DEVELOPMENT', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(139, 'PLD_68', 'SISTEM INFORMASI', 'TS', 'PERANCANGAN APLIKASI SOSIAL ENTERPRISE PADA PERANGKAT ANDROID DENGAN MENGGUNAKAN SCRUM DEVELOPMENT', 'SONI FAJAR SURYA GUMILANG', 'NUR ICHSAN UTAMA', '2016', 'ADITYA PRATAMA NUGRAHA RAHMAYADI', 'PERANCANGAN USER INTERFACE DASHBOARD APLIKASI SOCIAL ENTERPRISE PADA PERANGKAT ANDROID DENGAN MENGGUNAKAN SCRUM', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(140, 'PLD_69', 'SISTEM INFORMASI', 'TS', 'PENGEMBANGAN SISTEM INFORMASI BERBASIS ENTERPRISE RESOURCE PLANNING PADA ODOO DENGAN METODE RAPID APPLICATION DEVELOPMENT DI PT. BRODO GANESHA INDONESIA', 'SONI FAJAR SURYA GUMILANG', 'R. WAHJOE WITJAKSONO', '2016', 'AJI WIRA PRADHANA', 'PENGEMBANGAN SISTEM INFORMASI BERBASIS ENTERPRISE RESOURCE PLANNING MODUL SALES MANAGEMENT PADA ODOO DENGAN METODE RAPID APPLICATION DEVELOPMENT DI PT. BRODO GANESHA INDONESIA', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(141, 'PLD_69', 'SISTEM INFORMASI', 'TS', 'PENGEMBANGAN SISTEM INFORMASI BERBASIS ENTERPRISE RESOURCE PLANNING PADA ODOO DENGAN METODE RAPID APPLICATION DEVELOPMENT DI PT. BRODO GANESHA INDONESIA', 'SONI FAJAR SURYA GUMILANG', 'R. WAHJOE WITJAKSONO', '2016', 'RISNA RISDIANTI JUNIAR', 'PENGEMBANGAN SISTEM INFORMASI BERBASIS ENTERPRISE RESOURCE PLANNING MODUL PURCHASE MANAGEMENT PADA ODOO DENGAN METODE RAPID APPLICATION DEVELOPMENT DI PT. BRODO GANESHA INDONESIA', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(142, 'PLD_69', 'SISTEM INFORMASI', 'TS', 'PENGEMBANGAN SISTEM INFORMASI BERBASIS ENTERPRISE RESOURCE PLANNING PADA ODOO DENGAN METODE RAPID APPLICATION DEVELOPMENT DI PT. BRODO GANESHA INDONESIA', 'SONI FAJAR SURYA GUMILANG', 'R. WAHJOE WITJAKSONO', '2016', 'ANITA RAHMA MAULIDA', 'PENGEMBANGAN SISTEM INFORMASI BERBASIS ENTERPRISE RESOURCE PLANNING MODUL WAREHOUSE MANAGEMENT PADA ODOO DENGAN METODE RAPID APPLICATION DEVELOPMENT DI PT. BRODO GANESHA INDONESIA', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(143, 'PLD_69', 'SISTEM INFORMASI', 'TS', 'PENGEMBANGAN SISTEM INFORMASI BERBASIS ENTERPRISE RESOURCE PLANNING PADA ODOO DENGAN METODE RAPID APPLICATION DEVELOPMENT DI PT. BRODO GANESHA INDONESIA', 'SONI FAJAR SURYA GUMILANG', 'R. WAHJOE WITJAKSONO', '2016', 'PARAMITA RAHMAWATI', 'PENGEMBANGAN SISTEM ERP MODUL MANUFACTURING PADA ODOO DENGAN METODE RAPID APPLICATION DEVELOPMENT DI PT. BRODO GANESHA', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(144, 'PLD_70', 'SISTEM INFORMASI', 'TS', 'PERANCANGAN ENTERPRISE ARCHITECTURE MENGGUNAKAN FRAMEWORK TOGAF ADM PADA PT. HERONA EXPRESS', 'YULI ADAM PRASETYO ', 'BASUKI RAHMAD', '2016', 'FAISAL WISNU PRADHANA', 'PERANCANGAN ENTERPRISE ARCHITECTURE PADA FUNGSI OPERASIONAL DI PT. HERONA EXPRESS DENGAN MENGGUNAKAN FRAMEWORK TOGAF ADM', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(145, 'PLD_70', 'SISTEM INFORMASI', 'TS', 'PERANCANGAN ENTERPRISE ARCHITECTURE MENGGUNAKAN FRAMEWORK TOGAF ADM PADA PT. HERONA EXPRESS', 'YULI ADAM PRASETYO ', 'BASUKI RAHMAD', '2016', 'HENDRIK HENDRIANA FIRMANSYAH', 'PERANCANGAN ENTERPRISE ARCHITECTURE MENGGUNAKAN FRAMEWORK TOGAF ADM FUNGSI HUMAN RESOURCES DEVELOPMENT DAN PENGADAAN PADA PT. HERONA EXPRESS', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(146, 'PLD_70', 'SISTEM INFORMASI', 'TS', 'PERANCANGAN ENTERPRISE ARCHITECTURE MENGGUNAKAN FRAMEWORK TOGAF ADM PADA PT. HERONA EXPRESS', 'YULI ADAM PRASETYO ', 'BASUKI RAHMAD', '2016', 'IAN FAHMI NUGRAHA', 'PERANCANGAN ENTERPRISE ARCHITECTURE MENGGUNAKAN FRAMEWORK TOGAF ADM FUNGSI MARKETING DAN PELAYANAN PELANGGAN PADA PT. HERONA EXPRESS', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(147, 'PLD_71', 'SISTEM INFORMASI', 'TS', 'PERANCANGAN ENTERPRISE ARCHITECTURE DI PT. PLN DISTRIBUSI JAWA BARAT MENGGUNAKAN FRAMEWORK TOGAF ADM', 'YULI ADAM PRASETYO ', 'FAISHAL MUFIED AL-ANSHARY', '2016', 'RECSA ANDRIYANI PUTRI', 'PERANCANGAN ENTERPRISE ARCHITECTURE PADA BIDANG DISTRIBUSI DI PT. PLN DISTRIBUSI JAWA BARAT MENGGUNAKAN FRAMEWORK TOGAF ADM', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(148, 'PLD_71', 'SISTEM INFORMASI', 'TS', 'PERANCANGAN ENTERPRISE ARCHITECTURE DI PT. PLN DISTRIBUSI JAWA BARAT MENGGUNAKAN FRAMEWORK TOGAF ADM', 'YULI ADAM PRASETYO ', 'FAISHAL MUFIED AL-ANSHARY', '2016', 'MUTIA DEWI KURNIASIH', 'PERANCANGAN ENTERPRISE ARCHITECTURE PADA BIDANG PERENCANAAN DAN BIDANG KEUANGAN DI PT. PLN DISTRIBUSI JAWA BARAT MENGGUNAKAN FRAMEWORK TOGAF ADM', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(149, 'PLD_71', 'SISTEM INFORMASI', 'TS', 'PERANCANGAN ENTERPRISE ARCHITECTURE DI PT. PLN DISTRIBUSI JAWA BARAT MENGGUNAKAN FRAMEWORK TOGAF ADM', 'YULI ADAM PRASETYO ', 'FAISHAL MUFIED AL-ANSHARY', '2016', 'RINALDI HARRY LEKSANA', 'PERANCANGAN ENTERPRISE ARCHITECTURE PADA BIDANG NIAGA DAN PELAYANAN PELANGGAN PT. PLN DISTRIBUSI JAWA BARAT MENGGUNAKAN FRAMEWORK TOGAF ADM', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(150, 'PLD_71', 'SISTEM INFORMASI', 'TS', 'PERANCANGAN ENTERPRISE ARCHITECTURE DI PT. PLN DISTRIBUSI JAWA BARAT MENGGUNAKAN FRAMEWORK TOGAF ADM', 'YULI ADAM PRASETYO ', 'FAISHAL MUFIED AL-ANSHARY', '2016', 'ERSA NOVIA FAJRIN', 'PERANCANGAN ENTERPRISE ARCHITECTURE PADA BIDANG SUMBER DAYA MANUSIA DAN ORGANISASI SERTA BIDANG KOMUNIKASI HUKUM DAN ADMINISTRASI PT PLN DISTRIBUSI JAWA BARAT MENGGUNAKAN FRAMEWORK TOGAF ADM', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(151, 'PLD_72', 'SISTEM INFORMASI', 'TS', 'ANALISIS DAN PERANCANGAN APLIKASI PERSONAL FILE MENGGUNAKAN METODE ITERATIVE DAN INCREMENTAL PADA TELKOM PCC', 'YULI ADAM PRASETYO ', 'IRFAN DARMAWAN', '2016', 'IRVAN GUNAWAN', 'ANALISIS DAN PERANCANGAN APLIKASI PERSONAL FILE MODUL E-LEARNING PADA TELKOM PCC DENGAN METODE ITERATIVE DAN INCREMENTAL', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(152, 'PLD_72', 'SISTEM INFORMASI', 'TS', 'ANALISIS DAN PERANCANGAN APLIKASI PERSONAL FILE MENGGUNAKAN METODE ITERATIVE DAN INCREMENTAL PADA TELKOM PCC', 'YULI ADAM PRASETYO ', 'IRFAN DARMAWAN', '2016', 'BALA PUTRA DEWA', 'ANALISIS DAN PERANCANGAN APLIKASI PERSONAL FILE MENGGUNAKAN METODE ITERATIVE DAN INCREMENTAL PADA TELKOM PCC', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(153, 'PLD_73', 'SISTEM INFORMASI', 'TS', 'PERANCANGAN ENTERPRISE ARCHITECTURE BAPPEDA KABUPATEN BANDUNG MENGGUNAKAN FRAMEWORK TOGAF ADM', 'YULI ADAM PRASETYO ', 'RIDHA HANAFI', '2016', 'WIDYATASYA AGUSTIKA NURTRISHA', 'PERANCANGAN ENTERPRISE ARCHITECTURE PADA FUNGSI MONITORING DAN EVALUASI BAPPEDA KABUPATEN BANDUNG MENGGUNAKAN FRAMEWORK TOGAF ADM', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(154, 'PLD_73', 'SISTEM INFORMASI', 'TS', 'PERANCANGAN ENTERPRISE ARCHITECTURE BAPPEDA KABUPATEN BANDUNG MENGGUNAKAN FRAMEWORK TOGAF ADM', 'YULI ADAM PRASETYO ', 'RIDHA HANAFI', '2016', 'MUHAMMAD FACHRY PUTERA PRATAMA', 'PERANCANGAN ENTERPRISE ARCHITECTURE PADA FUNGSI PERENCANAAN PEMBANGUNAN BAPPEDA KABUPATEN BANDUNG MENGGUNAKAN FRAMEWORK TOGAF ADM', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(155, 'PLD_73', 'SISTEM INFORMASI', 'TS', 'PERANCANGAN ENTERPRISE ARCHITECTURE BAPPEDA KABUPATEN BANDUNG MENGGUNAKAN FRAMEWORK TOGAF ADM', 'YULI ADAM PRASETYO ', 'RIDHA HANAFI', '2016', 'ADELIA INDAH OKTAVIYANTI', 'PERANCANGAN ENTERPRISE ARCHITECTURE PADA FUNGSI PENELITIAN DAN PENGEMBANGAN BAPPEDA KABUPATEN BANDUNG MENGGUNAKAN FRAMEWORK TOGAF ADM', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(156, 'PLD_74', 'SISTEM INFORMASI', 'TS', 'ANALISIS DAN PERANCANGAN ENTERPRISE ARCHITECTURE PADA BADAN PERENCANAAN DAN PEMBANGUNAN DAERAH (BAPPEDA) PROVINSI JAWA BARAT MENGGUNAKAN FRAMEWORK TOGAF ADM', 'YULI ADAM PRASETYO ', 'RAHMAT MULYANA', '2016', 'DUMAULI NOVITASARI BORU SIMANJUNTAK', 'ANALISIS DAN PERANCANGAN ENTERPRISE ARCHITECTURE FUNGSI BISNIS PERENCANAAN PEMBANGUNAN PADA BADAN PERENCANAAN DAN PEMBANGUNAN DAERAH (BAPPEDA) PROVINSI JAWA BARAT MENGGUNAKAN FRAMEWORK TOGAF ADM', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(157, 'PLD_74', 'SISTEM INFORMASI', 'TS', 'ANALISIS DAN PERANCANGAN ENTERPRISE ARCHITECTURE PADA BADAN PERENCANAAN DAN PEMBANGUNAN DAERAH (BAPPEDA) PROVINSI JAWA BARAT MENGGUNAKAN FRAMEWORK TOGAF ADM', 'YULI ADAM PRASETYO ', 'RAHMAT MULYANA', '2016', 'AFRIANDA GAZA ONTOREZA', 'ANALISIS DAN PERANCANGAN ENTERPRISE ARCHITECTURE FUNGSI BISNIS PENGENDALIAN DAN EVALUASI PADA BADAN PERENCANAAN DAN PEMBANGUNAN DAERAH (BAPPEDA) PROVINSI JAWA BARAT MENGGUNAKAN FRAMEWORK TOGAF ADM', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(158, 'PLD_74', 'SISTEM INFORMASI', 'TS', 'ANALISIS DAN PERANCANGAN ENTERPRISE ARCHITECTURE PADA BADAN PERENCANAAN DAN PEMBANGUNAN DAERAH (BAPPEDA) PROVINSI JAWA BARAT MENGGUNAKAN FRAMEWORK TOGAF ADM', 'YULI ADAM PRASETYO ', 'RAHMAT MULYANA', '2016', 'ANIDA SHAFA', 'ANALISIS DAN PERANCANGAN ENTERPRISE ARCHITECTURE FUNGSI BISNIS ANALISIS PEMBANGUNAN PADA BADAN PERENCANAAN DAN PEMBANGUNAN DAERAH (BAPPEDA) PROVINSI JAWA BARAT MENGGUNAKAN FRAMEWORK TOGAF ADM', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(159, 'PLD_75', 'SISTEM INFORMASI', 'TS', 'PERANCANGAN ARSITEKTUR BISNIS, DATA,TEKNOLOGI DAN APLIKASI UNTUK LAYANAN JASA KEUANGAN PADA PT.POS INDONESIA MENGGUNAKAN TOGAF ADM', 'YULI ADAM PRASETYO ', 'RAHMAT MULYANA', '2016', 'AFFIFIANA PRISYANTI', 'PERANCANGAN ARSITEKTUR BISNIS DAN ARSITEKTUR DATA UNTUK LAYANAN JASA KEUANGAN PADA PT.POS INDONESIA MENGGUNAKAN TOGAF ADM', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(160, 'PLD_75', 'SISTEM INFORMASI', 'TS', 'PERANCANGAN ARSITEKTUR BISNIS, DATA,TEKNOLOGI DAN APLIKASI UNTUK LAYANAN JASA KEUANGAN PADA PT.POS INDONESIA MENGGUNAKAN TOGAF ADM', 'YULI ADAM PRASETYO ', 'RAHMAT MULYANA', '2016', 'DELPHINE YUSTICIA RATNASARI', 'PERANCANGAN ARSITEKTUR BISNIS DAN ARSITEKTUR TEKNOLOGI UNTUK LAYANAN JASA KEUANGAN PADA PT. POS INDONESIA MENGGUNAKAN FRAMEWORK TOGAF ADM', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(161, 'PLD_75', 'SISTEM INFORMASI', 'TS', 'PERANCANGAN ARSITEKTUR BISNIS, DATA,TEKNOLOGI DAN APLIKASI UNTUK LAYANAN JASA KEUANGAN PADA PT.POS INDONESIA MENGGUNAKAN TOGAF ADM', 'YULI ADAM PRASETYO ', 'RAHMAT MULYANA', '2016', 'CITRA MELATI', 'PERANCANGAN ARSITEKTUR BISNIS DAN ARSITEKTUR APLIKASI UNTUK LAYANAN JASA KEUANGAN PT.POS INDONESIA DENGAN MENGGUNAKAN TOGAF ADM', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(162, 'PLD_76', 'SISTEM INFORMASI', 'TS', 'PERANCANGAN ENTERPRISE ARCHITECTURE MENGGUNAKAN FRAMEWORK TOGAF ADM BAPAPSI KABUPATEN BANDUNG', 'YULI ADAM PRASETYO ', 'SONI FAJAR SURYA GUMILANG', '2016', 'FARID HAKIM NISWANSYAH', 'PERANCANGAN ENTERPRISE ARCHITECTURE MENGGUNAKAN FRAMEWORK TOGAF ADM PADA BIDANG PERPUSTAKAAN BAPAPSI KABUPATEN BANDUNG', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(163, 'PLD_76', 'SISTEM INFORMASI', 'TS', 'PERANCANGAN ENTERPRISE ARCHITECTURE MENGGUNAKAN FRAMEWORK TOGAF ADM BAPAPSI KABUPATEN BANDUNG', 'YULI ADAM PRASETYO ', 'SONI FAJAR SURYA GUMILANG', '2016', 'EMA TRIA WAHYUNINGTIHAS', 'Perancangan Enterprise Architecture Menggunakan Framework TOGAF ADM Pada Bidang Kearsipan di BAPAPSI Kabupaten Bandung', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(164, 'PLD_77', 'SISTEM INFORMASI', 'TS', 'PENGEMBANGAN BUSINESS INTELLIGENCE SYSTEM BERBASIS DATA WAREHOUSE MENGGUNAKAN PENTAHO DENGAN METODOLOGI BUSINESS DIMENSIONAL LIFE-CYCLE', 'ARI YANUAR RIDWAN', 'RACHMADITA ANDRESWARI', '2016', 'M. FIRMAN HELMI ARIYANSYAH', 'PENGEMBANGAN BUSINESS INTELLIGENCE SYSTEM BERBASIS DATA WAREHOUSE MENGGUNAKAN PENTAHO UNTUK PROSES PROCUREMENT DENGAN METODOLOGI BUSINESS DIMENSIONAL LIFE-CYCLE', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(165, 'PLD_77', 'SISTEM INFORMASI', 'TS', 'PENGEMBANGAN BUSINESS INTELLIGENCE SYSTEM BERBASIS DATA WAREHOUSE MENGGUNAKAN PENTAHO DENGAN METODOLOGI BUSINESS DIMENSIONAL LIFE-CYCLE', 'ARI YANUAR RIDWAN', 'RACHMADITA ANDRESWARI', '2016', 'RIFKIANSYAH ABDILA', 'PENGEMBANGAN BUSINESS INTELLIGENCE BERBASIS DATA WAREHOUSE MENGGUNAKAN PENTAHO UNTUK PROSES SALES & DISTRIBUTION DENGAN METODOLOGI BUSINESS DIMENSIONAL LIFE CYCLE (Studi kasus: Perum BULOG Divre Jabar)', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(166, 'PLD_78', 'SISTEM INFORMASI', 'TS', 'PENGEMBANGAN APLIKASI E-OFFICE PADA BADAN KEPEGAWAIAN DAERAH PROVINSI JAWA BARAT DENGAN MENGGUNAKAN METODE RAD', 'ARI YANUAR RIDWAN', 'RACHMADITA ANDRESWARI', '2016', 'YUDHA ADITYA RAMADHANA', 'PENGEMBANGAN APLIKASI E-OFFICE DISPOSISI DOKUMEN PADA BADAN KEPEGAWAIAN DAERAH PROVINSI JAWA BARAT DENGAN METODE RAD', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(167, 'PLD_78', 'SISTEM INFORMASI', 'TS', 'PENGEMBANGAN APLIKASI E-OFFICE PADA BADAN KEPEGAWAIAN DAERAH PROVINSI JAWA BARAT DENGAN MENGGUNAKAN METODE RAD', 'ARI YANUAR RIDWAN', 'RACHMADITA ANDRESWARI', '2016', 'REGINA AYU PRAMESWARI WADE', 'PENGEMBANGAN APLIKASI E-OFFICE PENCATATAN DOKUMEN PADA BADAN KEPEGAWAIAN DAERAH PROVINSI JAWA BARAT DENGAN MENGGUNAKAN METODE RAD', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(168, 'PLD_79', 'SISTEM INFORMASI', 'TS', 'PENGEMBANGAN SISTEM ERP BERBASIS ADEMPIERE DENGAN METODOLOGI ASAP PADA PERUM BULOG DIVRE JABAR', 'ARI YANUAR RIDWAN', 'R. WAHJOE WITJAKSONO', '2016', 'VICTOR MAROLOP PATRIASI SIREGAR', 'PENGEMBANGAN SISTEM ERP BERBASIS ADEMPIERE UNTUK MODUL PROCUREMENT DENGAN METODOLOGI ASAP PADA PERUM BULOG DIVRE JABAR', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(169, 'PLD_79', 'SISTEM INFORMASI', 'TS', 'PENGEMBANGAN SISTEM ERP BERBASIS ADEMPIERE DENGAN METODOLOGI ASAP PADA PERUM BULOG DIVRE JABAR', 'ARI YANUAR RIDWAN', 'R. WAHJOE WITJAKSONO', '2016', 'ARNOLD REWADIPO PURBA', 'PERANCANGAN SISTEM ERP PADA PROSES SALES AND DISTRIBUTION PADA PERUM BULOG MENGGUNAKAN SOFTWARE ADEMPIERE DENGAN METODE ASAP', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(170, 'PLD_80', 'SISTEM INFORMASI', 'TS', 'PERANCANGAN SISTEM INFORMASI BERBASIS OPENERP DENGAN METODE SOFT SYSTEM METHODOLOGY (STUDI KASUS: RUMAH SAKIT UMUM DAERAH AL-IHSAN)', 'ARI YANUAR RIDWAN, S.T., M.T.', 'R. WAHJOE WITJAKSONO', '2016', 'PUTU GEDE WIBAWA HARTAWAN', 'PERANCANGAN SISTEM PENGADAAN (PROCUREMENT) BERBASIS OPENERP DENGAN METODE SOFT SYSTEM METHODOLOGI (STUDI KASUS: RUMAH SAKIT UMUM DAERAH AL-IHSAN)', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(171, 'PLD_80', 'SISTEM INFORMASI', 'TS', 'PERANCANGAN SISTEM INFORMASI BERBASIS OPENERP DENGAN METODE SOFT SYSTEM METHODOLOGY (STUDI KASUS: RUMAH SAKIT UMUM DAERAH AL-IHSAN)', 'ARI YANUAR RIDWAN, S.T., M.T.', 'R. WAHJOE WITJAKSONO', '2016', 'ZENITA ROKHMANINGSIH', 'PERANCANGAN SISTEM FIXED ASSET MANAGEMENT BERBASIS OPENERP DENGAN METODE SOFT SYSTEM METHODOLOGY (STUDI KASUS: RSUD AL-IHSAN)', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(172, 'PLD_80', 'SISTEM INFORMASI', 'TS', 'PERANCANGAN SISTEM INFORMASI BERBASIS OPENERP DENGAN METODE SOFT SYSTEM METHODOLOGY (STUDI KASUS: RUMAH SAKIT UMUM DAERAH AL-IHSAN)', 'ARI YANUAR RIDWAN, S.T., M.T.', 'R. WAHJOE WITJAKSONO', '2016', 'SATRIA NARENDRA WIBAWA', 'PERANCANGAN SISTEM AKUNTASI KEUANGAN BERBASIS OPENERP DENGAN METODE SOFT SYSTEM METHODOLOGY (STUDI KASUS: RUMAH SAKIT UMUM DAERAH AL-IHSAN)', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(173, 'PLD_81', 'SISTEM INFORMASI', 'TS', 'PERANCANGAN SISTEM INFORMASI PADA PABRIK GULA JATIBARANG DENGAN MENGGUNAKAN SOFT SYSTEM METHODOLOGY', 'DEDEN WITARSYAH', 'R. WAHJOE WITJAKSONO', '2016', 'NADILA LINTANG HAPSARI', 'PERANCANGAN FITUR SALES AND DISTRIBUTION PADA PABRIK GULA JATIBARANG DENGAN MENGGUNAKAN SOFT SYSTEM METHODOLOGY', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(174, 'PLD_81', 'SISTEM INFORMASI', 'TS', 'PERANCANGAN SISTEM INFORMASI PADA PABRIK GULA JATIBARANG DENGAN MENGGUNAKAN SOFT SYSTEM METHODOLOGY', 'DEDEN WITARSYAH', 'R. WAHJOE WITJAKSONO', '2016', 'NILASARI NURAMANATI', 'PERANCANGAN FITUR SISTEM PURCHASING PADA PABRIK GULA JATIBARANG DENGAN MENGGUNAKAN SOFT SYSTEM METHODOLOGY', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(175, 'PLD_82', 'SISTEM INFORMASI', 'TS', 'PENERAPAN SISTEM INFORMASI MENGGUNAKAN APLIKASI ERP OPEN SOURCE DENGAN METODOLOGY AGILE (STUDI KASUS : RESTORAN DAN RESORT DE TUIK)', 'DEDEN WITARSYAH', 'R. WAHJOE WITJAKSONO', '2016', 'NURULLIANI SAFITRI MUTMAINAH', 'PENERAPAN SISTEM PRODUKSI MENGGUNAKAN APLIKASI ERP OPEN SOURCE PADA RESTORAN DAN RESORT DETUIK DENGAN PENGEMBANGAN METHODOLOGY AGILE', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(176, 'PLD_82', 'SISTEM INFORMASI', 'TS', 'PENERAPAN SISTEM INFORMASI MENGGUNAKAN APLIKASI ERP OPEN SOURCE DENGAN METODOLOGY AGILE (STUDI KASUS : RESTORAN DAN RESORT DE TUIK)', 'DEDEN WITARSYAH', 'R. WAHJOE WITJAKSONO', '2016', 'FABIYOLA NINDYA SUSILO', 'PENERAPAN SISTEM PURCHASING MENGGUNAKAN APLIKASI ERP OPEN SOURCE DENGAN PENGEMBANGAN METODOLOGY AGILE (STUDI KASUS : RESTORAN DAN RESORT DE TUIK)', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(177, 'PLD_83', 'SISTEM INFORMASI', 'TS', 'PEMBANGUNAN PORTAL WEB CROWDSOURCING EVENT PERGURUAN TINGGI MENGGUNAKAN METODE ITERATIVE INCREMENTAL', 'IRFAN DARMAWAN', 'TAUFIK NUR ADI', '2016', 'ALIFIA INDRA DAMARANI', 'PEMBANGUNAN PORTAL WEB CROWDSOURCING EVENT PERGURUAN TINGGI MENGGUNAKAN METODE ITERATIVE INCREMENTAL (MODUL PESERTA EVENT)', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(178, 'PLD_83', 'SISTEM INFORMASI', 'TS', 'PEMBANGUNAN PORTAL WEB CROWDSOURCING EVENT PERGURUAN TINGGI MENGGUNAKAN METODE ITERATIVE INCREMENTAL', 'IRFAN DARMAWAN', 'TAUFIK NUR ADI', '2016', 'MIA MEILANI', 'PEMBANGUNAN PORTAL WEB CROWDSOURCING EVENT PERGURUAN TINGGI MENGGUNAKAN METODE ITERATIVE INCREMENTAL (MODUL PENYELENGGARA EVENT)', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(179, 'PLD_84', 'SISTEM INFORMASI', 'TS', 'MEMBANGUN WEBSITE MANAJEMEN INVESTASI BERBASIS CROWDFUNDING MENGGUNAKAN METODE ITERATIVE DAN INCREMENTAL', 'NIA AMBARSARI', 'TAUFIK NUR ADI', '2016', 'FAUZA ANNISA', 'MEMBANGUN WEBSITE MANAJEMEN INVESTASI BERBASIS CROUWDFUNDING MODUL UKM MENGGUNAKAN METODE ITERATIVE DAN INCREMENTAL', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(180, 'PLD_84', 'SISTEM INFORMASI', 'TS', 'MEMBANGUN WEBSITE MANAJEMEN INVESTASI BERBASIS CROWDFUNDING MENGGUNAKAN METODE ITERATIVE DAN INCREMENTAL', 'NIA AMBARSARI', 'TAUFIK NUR ADI', '2016', 'FATHIMAH MUTHI LUTHFIYAH', 'MEMBANGUN WEBSITE MANAJEMEN INVESTASI BERBASIS CROWDFUNDING MODUL MONITORING DANA INVESTASI MENGGUNAKAN METODE ITERATIVE DAN INCREMENTAL', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(181, 'PLD_84', 'SISTEM INFORMASI', 'TS', 'MEMBANGUN WEBSITE MANAJEMEN INVESTASI BERBASIS CROWDFUNDING MENGGUNAKAN METODE ITERATIVE DAN INCREMENTAL', 'NIA AMBARSARI', 'TAUFIK NUR ADI', '2016', 'MADE FEBRIYANA DYASTAMA PUTRA', 'MEMBANGUN WEBSITE MANAJEMEN INVESTASI BERBASIS CROWDFUNDING MODUL INVESTOR MENGGUNAKAN METODE ITERATIVE DAN INCREMENTAL', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(182, 'PLD_85', 'SISTEM INFORMASI', 'TS', 'PENGEMBANGAN APLIKASI WEB MANAJEMEN ASET PEMERINTAHAN KABUPATEN BANDUNG MENGGUNAKAN METODE ITERATIVE DAN INCREMENTAL', 'SENO ADI PUTRA', 'DEDEN WITARSYAH', '2016', 'ADVENTUS ANGGA KURNIAWAN', 'PENGEMBANGAN APLIKASI WEB MANAJEMEN ASET PEMERINTAHAN KABUPATEN BANDUNG MODUL INVENTARISASI, PENILAIAN, PENGHAPUSAN MENGGUNAKAN METODE ITERATIVE DAN INCREMENTAL', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(183, 'PLD_85', 'SISTEM INFORMASI', 'TS', 'PENGEMBANGAN APLIKASI WEB MANAJEMEN ASET PEMERINTAHAN KABUPATEN BANDUNG MENGGUNAKAN METODE ITERATIVE DAN INCREMENTAL', 'SENO ADI PUTRA', 'DEDEN WITARSYAH', '2016', 'DENANDRA PRADIPTA', 'PENGEMBANGAN APLIKASI MANAJEMEN ASET PEMERINTAHAN KABUPATEN BANDUNG BERBASIS WEB BAGIAN PERENCANAAN, PENGADAAN, PENERIMAAN, PENGELUARAN DAN PEMELIHARAAN MENGGUNAKAN METODE ITERATIVE DAN INCREMENTAL', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(184, 'PLD_86', 'SISTEM INFORMASI', 'TS', 'PENGEMBANGAN WEB PORTAL UKM CROWDFUNDING DENGAN METODE PROTOTYPE DAN FRAMEWORK CODEIGNITER', 'YULI ADAM PRASETYO', 'FAISHAL MUFIED AL-ANSHARY', '2016', 'ARIS SETYO NUGROHO', 'PENGEMBANGAN MODUL INVESTOR WEB PORTAL CROWDFUNDING DENGAN METODE PROTOTYPE DAN FRAMEWORK CODEIGNITER', NULL, 'TA', NULL, 1, NULL, NULL, NULL),
(185, 'PLD_86', 'SISTEM INFORMASI', 'TS', 'PENGEMBANGAN WEB PORTAL UKM CROWDFUNDING DENGAN METODE PROTOTYPE DAN FRAMEWORK CODEIGNITER', 'YULI ADAM PRASETYO', 'FAISHAL MUFIED AL-ANSHARY', '2016', 'HARPANDI WIBOWO', 'PENGEMBANGAN MODUL UKM WEB PORTAL CROWDFUNDING DENGAN METODE PROTOTYPE DAN FRAMEWORK CODEIGNITER', NULL, 'TA', NULL, 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_penelitian_dosen`
--

CREATE TABLE `tb_penelitian_dosen` (
  `peldos_no` int(11) NOT NULL,
  `idpeldos` varchar(100) NOT NULL,
  `program_studi` varchar(100) DEFAULT NULL,
  `no_ts` varchar(50) DEFAULT NULL,
  `tipe` varchar(100) DEFAULT NULL,
  `judul` varchar(500) DEFAULT NULL,
  `tahun` varchar(50) DEFAULT NULL,
  `nama_dosenketua` varchar(100) DEFAULT NULL,
  `nama_dosenanggota_1` varchar(1000) DEFAULT NULL,
  `nama_dosenanggota_2` varchar(500) DEFAULT NULL,
  `nama_dosenanggota_3` varchar(500) DEFAULT NULL,
  `nama_dosenanggota_4` varchar(500) DEFAULT NULL,
  `nama_dosenanggota_5` varchar(500) DEFAULT NULL,
  `nama_mahasiswa_1` varchar(1000) DEFAULT NULL,
  `status` varchar(200) DEFAULT NULL,
  `anggaran` int(11) DEFAULT NULL,
  `keterangan` varchar(100) DEFAULT NULL,
  `sumber_pembiayaan` varchar(200) DEFAULT NULL,
  `jenis_penelitian` varchar(200) NOT NULL,
  `sts_valid` varchar(50) DEFAULT NULL,
  `file_name` varchar(200) DEFAULT NULL,
  `file_type` varchar(100) DEFAULT NULL,
  `file_date` varchar(200) DEFAULT NULL,
  `id_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_penelitian_dosen`
--

INSERT INTO `tb_penelitian_dosen` (`peldos_no`, `idpeldos`, `program_studi`, `no_ts`, `tipe`, `judul`, `tahun`, `nama_dosenketua`, `nama_dosenanggota_1`, `nama_dosenanggota_2`, `nama_dosenanggota_3`, `nama_dosenanggota_4`, `nama_dosenanggota_5`, `nama_mahasiswa_1`, `status`, `anggaran`, `keterangan`, `sumber_pembiayaan`, `jenis_penelitian`, `sts_valid`, `file_name`, `file_type`, `file_date`, `id_user`) VALUES
(1, 'PLD_1', 'SISTEM INFORMASI', 'TS_2', 'Penelitian Hibah Bersaing 2013', 'Pengembangan Sistem Informasi Pemantauan, Pengawasan Serta Manajemen Rumah Sakit Dan Puskesmas Berbasis Web Dengan Studi Kasus Di Dinas Kesehatan Kota Bandung', '2013', 'RIZA AGUSTIANSYAH', 'NIA AMBARSARI, R. WAHJOE WITJAKSONO', '', '', '', '', '', '', 40000000, 'valid ppm', 'Depdiknas', 'Penelitian Dosen', 'valid', NULL, NULL, NULL, 1),
(2, 'PLD_2', 'SISTEM INFORMASI', 'TS_2', 'Penelitian Hibah Bersaing 2013 (Multy Year Tahun Pertama)', 'Pembuatan Portal Konsumen sebagai Sumber Informasi Perceived Value Konsumen terhadap Produk dan Jasa', '2013', 'YULI ADAM PRASETYO', 'FAUZAN AZMI', '', '', '', '', '', '', 50000000, 'valid ppm', 'Depdiknas', 'Penelitian Dosen', NULL, NULL, NULL, NULL, 1),
(3, 'PLD_3', 'SISTEM INFORMASI', 'TS_1', 'PENELITIAN DIKTI 2014 (Multy Year Tahun Kedua)', 'Pembuatan Portal Konsumen sebagai Sumber Informasi Perceived Value Konsumen terhadap Produk dan Jasa', '22/5/2014', 'YULI ADAM PRASETYO ', 'FAUZAN AZMI', '', '', '', '', '', '', 50000000, 'valid ppm', 'Depdiknas', 'Penelitian Dosen', NULL, NULL, NULL, NULL, 1),
(4, 'PLD_4', 'SISTEM INFORMASI', 'TS_1', 'PENELITIAN KEMITRAAN 2014 TAHAP II', 'Analisa Kebutuhan Program Pembinaan Peningkatan Kemampuan Pemasaran Produk-Produk UKM untuk Pengembangan Usaha UMKM dalam Asosiasi Pengolahan Hasil Pertanian Hortikultura Kab. Bandung', '23/9/2014', 'YATI ROHAYATI ', 'SARI WULANDARI, AULIA FASHANAH HADINING, TAUFIK NUR ADI', '', '', '', '', '', '', 45000000, 'valid ppm', 'PT yang bersangkutan', 'Penelitian Dosen', NULL, NULL, NULL, NULL, 1),
(5, 'PLD_5', 'SISTEM INFORMASI', 'TS_1', 'PENELITIAN KEMITRAAN 2014 TAHAP II', 'Perancangan Sistem Pendistribusian Sampah pada Dinas PERTASIH Kabupaten Bandung', '23/9/2014', 'RD. ROHMAT SAEDUDIN ', 'MUHAMMAD NASHIR ARDIANSYAH, BUDI SANTOSA, RD. ROHMAT SAEDUDIN ', '', '', '', '', '', '', 70000000, 'valid ppm', 'PT yang bersangkutan', 'Penelitian Dosen', NULL, NULL, NULL, NULL, 1),
(6, 'PLD_6', 'SISTEM INFORMASI', 'TS_1', 'PENELITIAN KEMITRAAN 2014 TAHAP II', 'Perencanaan Implementasi Knowledge Management System Di Pusat Dokumentasi Dan Informasi Ilmiah Lembaga Ilmu Pengetahuan Indonesia (PDII-LIPI)', '23/9/2014', 'LUCIANA ANDRAWINA ', 'AMELIA KURNIAWATI, NIA AMBARSARI, UMAR YUNAN KURNIA SEPTO HEDIYANTO', '', '', '', '', '', '', 60000000, 'valid ppm', 'PT yang bersangkutan', 'Penelitian Dosen', NULL, NULL, NULL, NULL, 1),
(7, 'PLD_7', 'SISTEM INFORMASI', 'TS_1', 'PENELITIAN KEMITRAAN 2015 TAHAP I', 'Kajian Kampung Energi dan Pupuk Mandiri Berbahan Baku Limbah Mendong di Kabupaten Tasikmalaya', '27/1/2015', 'ROSAD MA', 'RINO ANDIAS A., ARI YANUAR RIDWAN, MELDY RENDRA, FRANSISKUS TATAS DWI ATMAJI', '', '', '', '', '', '', 70340000, 'valid ppm', 'PT yang bersangkutan', 'Penelitian Dosen', NULL, NULL, NULL, NULL, 1),
(8, 'PLD_8', 'SISTEM INFORMASI', 'TS_1', 'PENELITIAN MANDIRI 2015 (TAHAP 1)', 'INTEGRASI ALGORITMA GENETIK DAN TABOO SEARCH UNTUK MENYEIMBANGKAN BEBAN PROSES TERHADAP SUMBER KOMPUTASI', '16/4/2015', 'IRFAN DARMAWAN ', '', '', '', '', '', '', '', 12500000, 'valid ppm', 'Pembiayaan sendiri oleh peneliti', 'Penelitian Dosen', NULL, NULL, NULL, NULL, 1),
(9, 'PLD_9', 'SISTEM INFORMASI', 'TS_1', 'PENELITIAN DANA INTERNAL 2015 ', 'PENERAPAN SISTEM INFORMASI BERBASIS OPENERP DENGAN METODE RAPID APPLICATION DEVELOPMENT (STUDI KASUS: PT GENTA TRIKARYA)', '2015', 'ARI YANUAR RIDWAN', 'R. WAHJOE WITJAKSONO', '', '', '', '', 'MUHAMMAD SYAIFUL RAMADHAN, FAJRUL ALFIAN', '', 11800000, 'TA', 'PT yang bersangkutan', 'Penelitian TA', NULL, NULL, NULL, NULL, 1),
(10, 'PLD_10', 'SISTEM INFORMASI', 'TS_1', 'PENELITIAN DANA INTERNAL 2015 ', 'PENERAPAN ERP MENGGUNAKAN OPEN ERP DENGAN METODE SURE STEP (STUDI KASUS : PT XYZ RETAIL FASHION)', '2015', 'DEDEN WITARSYAH', 'R. WAHJOE WITJAKSONO', '', '', '', '', 'MOHD RHEDO MARGEN, IKHSAN YUDHA PRADANA,ADELIA FEBIYANTI M.', '', 11750000, 'TA', 'PT yang bersangkutan', 'Penelitian TA', 'valid', NULL, NULL, NULL, 1),
(11, 'PLD_11', 'SISTEM INFORMASI', 'TS_1', 'PENELITIAN DANA INTERNAL 2015 ', 'PERANCANGAN DAN PEMBANGUNAN KNOWLEDGE MANAGEMENT SYSTEM PADA MODUL PENELITIAN DAN PENGABDIAN MASYARAKAT DAN PENUNJANG MENGGUNAKAN FRAMEWORK CODEIGNITER DENGAN METODE ITERATIVE INCREMENTAL', '2015', 'LUCIANA ANDRAWINA', 'AHMAD MUSNANSYAH', '', '', '', '', 'NEVISIA PUSPA AYUDHAN, LIFFI NOFERIANTI', '', 11400000, 'TA', 'PT yang bersangkutan', 'Penelitian TA', 'valid', NULL, NULL, NULL, 1),
(12, 'PLD_12', 'SISTEM INFORMASI', 'TS_1', 'PENELITIAN DANA INTERNAL 2015 ', 'PERANCANGAN KNOWLEDGE MANAGEMENT SYSTEM UNTUK PENGELOLAAN SELEKSI MAHASISWA BARU DI UNIVERSITAS TELKOM DENGAN METODE ITERATIVE INCREMENTAL', '2015', 'LUCIANA ANDRAWINA', 'FAISHAL MUFIED AL-ANSHARY', '', '', '', '', 'PUTU PUSPITHA SARASWATI, INEZ SEKARAYU NAWANGWULAN, SOFIA FARIDAH', '', 11500000, 'TA', 'PT yang bersangkutan', 'Penelitian TA', 'valid', NULL, NULL, NULL, 1),
(13, 'PLD_13', 'SISTEM INFORMASI', 'TS_1', 'PENELITIAN DANA INTERNAL 2015 ', 'PENERAPAN SISTEM INFORMASI BERBASIS OPENERP DENGAN METODE SURESTEP (STUDI KASUS: UMKM KONVEKSI RAJUTAN)', '2015', 'IRFAN DARMAWAN', 'R. WAHJOE WITJAKSONO', '', '', '', '', 'IRENA ARSYKA DEWI, MOHAMAD IQBAL, PRADITO SETIADI', '', 10000000, 'TA', 'PT yang bersangkutan', 'Penelitian TA', 'valid', NULL, NULL, NULL, 1),
(14, 'PLD_14', 'SISTEM INFORMASI', 'TS_1', 'PENELITIAN DANA INTERNAL 2015 ', 'PERANCANGAN ENTERPRISE ARCHITECTURE E-COMMERCE DI PT XYZ MENGGUNAKAN FRAMEWORK TOGAF ADM', '2015', 'IRFAN DARMAWAN', 'BASUKI RAHMAD', '', '', '', '', 'RINI SETYANINGSIH, Ajeng Citra Rizkyanur, SRI HARYANI BR. MANJUNTAK, PUTRI MYKE WAHYUNI', '', 11900000, 'TA', 'PT yang bersangkutan', 'Penelitian TA', 'valid', NULL, NULL, NULL, 1),
(15, 'PLD_15', 'SISTEM INFORMASI', 'TS_1', 'PENELITIAN DANA INTERNAL 2015 ', 'PERANCANGAN PRIVATE CLOUD COMPUTING PADA FAKULTAS REKAYASA INDUSTRI UNIVERSITAS TELKOM', '2015', 'TEGUH KURNIAWAN', 'ADITYAS WIDJAJARTO', '', '', '', '', 'DECKY RADITAMA MEGANTAR,TEGAR PAMUNGKAS', '', 10500000, 'TA', 'PT yang bersangkutan', 'Penelitian TA', 'valid', NULL, NULL, NULL, 1),
(16, 'PLD_16', 'SISTEM INFORMASI', 'TS_1', 'PENELITIAN DANA INTERNAL 2015 ', 'DESAIN DAN ANALISA INFRASTRUKTUR JARINGAN DI PDII-LIPI JAKARTA DENGAN MENGGUNAKAN METODE NETWORK DEVELOPMENT LIFE CYCLE (NDLC)', '2015', 'TEGUH KURNIAWAN', 'UMAR YUNAN KURNIA SEPTO ', '', '', '', '', 'OKTA PUSPITA DWI ANGGOROWATI, ARIF NURFAJAR', '', 11000000, 'TA', 'PT yang bersangkutan', 'Penelitian TA', 'valid', NULL, NULL, NULL, 1),
(17, 'PLD_17', 'SISTEM INFORMASI', 'TS_1', 'PENELITIAN DANA INTERNAL 2015 ', 'PERANCANGAN SERVICE PADA LAYANAN ANGKUTAN PT KERETA API INDONESIA (PERSERO) MENGGUNAKAN FRAMEWORK ITIL VERSI 3', '2015', 'MURAHARTAWATY', 'EKO K. UMAR ', '', '', '', '', 'SABRINA ANDIYANI, REXY SEPTIAN ARAFAT, MIFTA AZIZ, RINALDY FERDY FERDIANO', '', 11800000, 'TA', 'PT yang bersangkutan', 'Penelitian TA', 'valid', NULL, NULL, NULL, 1),
(18, 'PLD_18', 'SISTEM INFORMASI', 'TS_1', 'PENELITIAN DANA INTERNAL 2015 ', 'ASSESSMENT DAN PERANCANGAN ITSM DOMAIN SERVICE BERDASARKAN ITIL VERSI 2011, ISO 20000 SERIES, DAN ISO 15504 SERIES UNTUK MENINGKATKAN CAPABILITY LEVEL DENGAN PEMANFAATAN TOOLS REMEDY PADA PT TELKOM INDONESIA', '2015', 'MURAHARTAWATY', 'EKO K. UMAR ', '', '', '', '', 'REZA ALDIANSYAH, VIKY HERMANA PRATAMA', '', 10670000, 'TA', 'PT yang bersangkutan', 'Penelitian TA', NULL, NULL, NULL, NULL, 1),
(19, 'PLD_19', 'SISTEM INFORMASI', 'TS_1', 'PENELITIAN DANA INTERNAL 2015 ', 'PERANCANGAN SERVICE PADA LAYANAN IT PUSAIR DENGAN MENGGUNAKAN FRAMEWORK ITIL VERSI 3', '2015', 'MURAHARTAWATY', 'NIA AMBARSARI', '', '', '', '', 'FRANSISK, VERA ANANDA', '', 11100000, 'TA', 'PT yang bersangkutan', 'Penelitian TA', NULL, NULL, NULL, NULL, 1),
(20, 'PLD_20', 'SISTEM INFORMASI', 'TS_1', 'PENELITIAN DANA INTERNAL 2015 ', 'ANALISIS DAN PERANCANGAN ITSM PADA LAYANAN AKADEMIK INSTITUT PEMERINTAHAN DALAM NEGERI (IPDN) DENGAN MENGGUNAKAN FRAMEWORK ITIL VERSI 3', '2015', 'MURAHARTAWATY', 'RIDHA HANAFI', '', '', '', '', 'ARIDHA MEITYA ARIFIN, CHARLIE SUGIARTO, THESSA SILVIANA MANURUNG', '', 11875000, 'TA', 'PT yang bersangkutan', 'Penelitian TA', NULL, NULL, NULL, NULL, 1),
(21, 'PLD_21', 'SISTEM INFORMASI', 'TS_1', 'PENELITIAN DANA INTERNAL 2015 ', 'PERANCANGAN ARCHITECTURE UNTUK FUNGSI AKADEMIK PADA INSTITUT PEMERINTAHAN DALAM NEGERI MENGGUNAKAN FRAMEWORK TOGAF ADM STUDI KASUS SISTEM INFORMASI AKADEMIK (SIAKAD)', '2015', 'MURAHARTAWATY', 'RIDHA HANAFI', '', '', '', '', 'SATRIA JANAKA, RAHAYU MANOLITA, ANDIKA DESTA GINANJAR', '', 10500000, 'TA', 'PT yang bersangkutan', 'Penelitian TA', NULL, NULL, NULL, NULL, 1),
(22, 'PLD_22', 'SISTEM INFORMASI', 'TS_1', 'PENELITIAN DANA INTERNAL 2015 ', 'ANALISIS DAN PERANCANGAN ARCHITECTURE MENGGUNAKAN THE OPEN GROUP ARCHITECTURE FRAMEWORK ARCHITECTURE DEVELOPMENT METHOD (TOGAF ADM) PADA PT SHAFCO MULTI TRADING', '2015', 'MURAHARTAWATY', 'RIDHA HANAFI', '', '', '', '', 'RENANTIA INDRIANI, FAMILA FARADIBA, I GEDE MINDRAYASA', '', 10600000, 'TA', 'PT yang bersangkutan', 'Penelitian TA', NULL, NULL, NULL, NULL, 1),
(23, 'PLD_23', 'SISTEM INFORMASI', 'TS_1', 'PENELITIAN DANA INTERNAL 2015 ', 'PERANCANGAN TATA KELOLA TEKNOLOGI INFORMASI PT INDUSTRI TELEKOMUNIKASI INDONESIA (INTI) MENGGUNAKAN FRAMEWORK COBIT 5', '2015', 'MURAHARTAWATY', 'SONI FAJAR SURYA GUMILANG', '', '', '', '', 'IDA BAGUS KRISNA WEDANTA PRASADA, KOMANG INDAH DESINTHYA WATI, I KETUT ADI PUTRA PRANANTA, BILLA ANANDA SUWANDI', '', 11300000, 'TA', 'PT yang bersangkutan', 'Penelitian TA', NULL, NULL, NULL, NULL, 1),
(24, 'PLD_24', 'SISTEM INFORMASI', 'TS_1', 'PENELITIAN DANA INTERNAL 2015 ', 'PERANCANGAN ARSITEKTUR PADA PT TELEHOUSE ENGINEERING MENGGUNAKAN FRAMEWORK TOGAF ADM', '2015', 'MURAHARTAWATY', 'UMAR YUNAN, KURNIA SEPTO', '', '', '', '', 'ADITYA PRADANA PUTRA, ASHOF YUDHISTIRA M.P.', '', 11895000, 'TA', 'PT yang bersangkutan', 'Penelitian TA', NULL, NULL, NULL, NULL, 1),
(25, 'PLD_25', 'SISTEM INFORMASI', 'TS_1', 'PENELITIAN DANA INTERNAL 2015 ', 'PENERAPAN SISTEM INFORMASI MENGGUNAKAN OPENERP PADA PT KHARISMA BUANA JAYA DENGAN METODE SPIRAL', '2015', 'NIA AMBARSARI', 'R. WAHJOE WITJAKSONO', '', '', '', '', 'PUSPITA AYU KARTIKA, RIDHO ARYA DUTA MAHENDRA, ANNISA UTAMI, MUHAMMAD AULIA RENDY', '', 10800000, 'TA', 'PT yang bersangkutan', 'Penelitian TA', NULL, NULL, NULL, NULL, 1),
(26, 'PLD_26', 'SISTEM INFORMASI', 'TS_1', 'PENELITIAN DANA INTERNAL 2015 ', 'MEMBANGUN SISTEM INFORMASI PERIZINAN ANGKUTAN UMUM DENGAN METODE AGILE DEVELOPMENT EXTREME PROGRAMMING (STUDI KASUS DINAS PERHUBUNGAN KABUPATEN BANDUNG BARAT)', '2015', 'NIA AMBARSARI', 'RIDHA HANAFI', '', '', '', '', 'OLAF ARMAN SEBASTIAN SIHOMBING, FATHIYYAH NUR AZIZAH, GREGORIUS PRAHASWARA DEWANTA', '', 10780000, 'TA', 'PT yang bersangkutan', 'Penelitian TA', NULL, NULL, NULL, NULL, 1),
(27, 'PLD_27', 'SISTEM INFORMASI', 'TS_1', 'PENELITIAN DANA INTERNAL 2015 ', 'MEMBANGUN APLIKASI WEB BEASISWA BERBASIS CROWDFUNDING MENGGUNAKAN METODE ITERATIVE INCREMENTAL', '2015', 'NIA AMBARSARI', 'TAUFIK NUR ADI', '', '', '', '', 'RANDYANSYAH RACHMANALAN, MUHAMMAD AKBAR SATRIA', '', 10200000, 'TA', 'PT yang bersangkutan', 'Penelitian TA', NULL, NULL, NULL, NULL, 1),
(28, 'PLD_28', 'SISTEM INFORMASI', 'TS_1', 'PENELITIAN DANA INTERNAL 2015 ', 'MEMBANGUN APLIKASI E-COMMERCE FOTO PREWED BERBASIS WEB MENGGUNAKAN MODEL CROWDSOURCING DAN METODE ITERATIVE INCREMENTAL (MODUL FOTOGRAFER)', '2015', 'NIA AMBARSARI', 'TAUFIK NUR ADI', '', '', '', '', 'MAHDY ARIEF, INSAN HARISH', '', 10500000, 'TA', 'PT yang bersangkutan', 'Penelitian TA', NULL, NULL, NULL, NULL, 1),
(29, 'PLD_29', 'SISTEM INFORMASI', 'TS_1', 'PENELITIAN DANA INTERNAL 2015 ', 'MEMBANGUN WEB CROWDSOURCING E-PREPARATION SMB UNIVERSITAS TELKOM DENGAN MENGGUNAKAN METODE ITERATIVE INCREMENTAL', '2015', 'NIA AMBARSARI', 'TAUFIK NUR ADI', '', '', '', '', 'HOSIANA ARISKA SILALAHI, DHANI RAHUTAMI PURWASTUTI, JOSES ADELWIN SITEPU', '', 11300000, 'TA', 'PT yang bersangkutan', 'Penelitian TA', NULL, NULL, NULL, NULL, 1),
(30, 'PLD_30', 'SISTEM INFORMASI', 'TS_1', 'PENELITIAN DANA INTERNAL 2015 ', 'MEMBANGUN WEB BERBASIS CROWDSOURCING UNTUK JUAL BELI SAMPAH PLASTIK DENGAN METODE ITERATIVE INCREMENTAL MENGGUNAKAN FRAMEWORK CODEIGNITER', '2015', 'NIA AMBARSARI', 'TAUFIK NUR ADI', '', '', '', '', 'AGUNG INSANI ALAM, MARTHA OKRINA', '', 11600000, 'TA', 'PT yang bersangkutan', 'Penelitian TA', NULL, NULL, NULL, NULL, 1),
(31, 'PLD_31', 'SISTEM INFORMASI', 'TS_1', 'PENELITIAN DANA INTERNAL 2015 ', 'PENERAPAN SISTEM INFORMASI BERBASIS OPENERP PADA CV HUDA JAYA DENGAN METODE RAPID APPLICATION DEVELOPMENT', '2015', 'SONI FAJAR SURYA GUMILANG', 'R. WAHJOE WITJAKSONO', '', '', '', '', 'FADHIL MUHAMMAD, SHINTA SINDI NURYANI', '', 11900000, 'TA', 'PT yang bersangkutan', 'Penelitian TA', NULL, NULL, NULL, NULL, 1),
(32, 'PLD_32', 'SISTEM INFORMASI', 'TS_1', 'PENELITIAN DANA INTERNAL 2015 ', 'MEMBANGUN CROWDSOURCING DIET SEHAT MENGGUNAKAN METODOLOGI SCRUM', '2015', 'SONI FAJAR SURYA GUMILANG', 'TAUFIK NUR ADI', '', '', '', '', 'RIZKI DWI KURNIA DEWI, FITRIA', '', 10100000, 'TA', 'PT yang bersangkutan', 'Penelitian TA', NULL, NULL, NULL, NULL, 1),
(33, 'PLD_33', 'SISTEM INFORMASI', 'TS_1', 'PENELITIAN DANA INTERNAL 2015 ', 'PENGEMBANGAN APLIKASI WEB BERBASIS CROWDSOURCING UNTUK MANAJEMEN PERSEWAAN MOBIL DENGAN MENGGUNAKAN METODOLOGI SCRUM', '2015', 'SONI FAJAR SURYA GUMILANG', 'TAUFIK NUR ADI', '', '', '', '', 'FAQIH PUTRA KUSUMAWIJAYA, GHOZIYAH HAITAN RACHMAN', '', 11100000, 'TA', 'PT yang bersangkutan', 'Penelitian TA', NULL, NULL, NULL, NULL, 1),
(34, 'PLD_34', 'SISTEM INFORMASI', 'TS', 'PENELITIAN HIBAH INTERNASIONAL 2015 (TAHAP 2)', 'Developing Trust and Privacy in E-government services Base on the UTAUT Model', '29/12/2015', 'DIDA DIAH DAMAYANTI', 'DEDEN WITARSYAH, ARI YANUAR RIDWAN, TEDDY SYAFRIZAL', '', '', '', '', 'SHELVIA ARDIAN PERDANA, EVA NOVIANTI, ALIMA INDRIATI, I KOMANG JAKA AKSARA WIGUN', '', 120000000, 'valid ppm', 'Depdiknas', 'Penelitian Dosen', NULL, NULL, NULL, NULL, 1),
(35, 'PLD_35', 'SISTEM INFORMASI', 'TS', 'PENELITIAN KEMITRAAN 2015 TAHAP II', 'KAJIAN PEMBUATAN DAN PEMANFAATAN PUPUK BERBAHAN BAKU SAMPAH ORGANIK KAWASAN UNIVERSITAS TELKOM DENGAN METODE PENGOMPOSANAN', '29/12/2015', 'ARI YANUAR RIDWAN', 'TEDDY SYAFRIZAL, FAISHAL MUFIED AL ANSHARY, BUDI PRAPTONO, LUCIANA ANDRAWINA', '', '', '', '', 'ZETA FARIDIAH INGE PUT, M. FAJRUL NUGRAHA, MAHARDIANI BENINGRUM, RATNA WULAN SARI, HARLY MIFTA NURFALA', '', 66800000, 'valid ppm', 'PT yang bersangkutan', 'Penelitian Dosen', NULL, NULL, NULL, NULL, 1),
(36, 'PLD_36', 'SISTEM INFORMASI', 'TS', 'PENELITIAN KEMITRAAN 2015 TAHAP II', 'PENERAPAN TEKNOLOGI ECVT (ELECTRICAL CAPACITANCE VOLUME TOMOGRAPHY) UNTUK MENAMPILKAN CITRA ISI BUAH DALAM RANGKA PEMERIKSAAN BUAH GUNA KEPENTINGAN EKSPOR', '29/12/2015', 'RD. ROHMAT SAEDUDIN ', 'RIZA AGUSTIANSYAH, MARDIYANTO WIYOGO, AGUS SETIAWAN', '', '', '', '', 'AYU CAHYANI FEBRYANTI, SUKRINA HERMAN, IKA PUSPITASARI, MUHAMMAD FIRSON DESTADIAWAN, GITA RIESTA', '', 65000000, 'valid ppm', 'PT yang bersangkutan', 'Penelitian Dosen', NULL, NULL, NULL, NULL, 1),
(37, 'PLD_37', 'SISTEM INFORMASI', 'TS', 'PENELITIAN KEMITRAAN 2015 TAHAP II', 'PERANCANGAN MODEL eREADINESS ADOPSI TEKNOLOGI INFORMASI PADA INSTITUSI PEMERINTAHAN KABUPATEN BANDUNG', '29/12/2015', 'SONI FAJAR SURYA GUMILANG ', 'DEDEN WITARSYAH, MUHAMMADH AZANI HASIBUAN, HERU NUGROHO, TAUFIK NUR ADI', '', '', '', '', 'PATRA BRIGANANDA, RANI NURBAITI LUKMAN, RATI AMANDA FAJRIN, LUCKY SURYA P., EMA TRIA W.', '', 53000000, 'valid ppm', 'PT yang bersangkutan', 'Penelitian Dosen', NULL, NULL, NULL, NULL, 1),
(38, 'PLD_38', 'SISTEM INFORMASI', 'TS', 'PENELITIAN DANA MANDIRI 2015 (TAHAP II)', 'REFACTORING BASIS DATA DAN PERAWATAN SISTEM INFORMASI MONITORING EMISI GAS RUMAH KACA PADA SEKTOR INDUSTRI', '30/12/2015', 'TIEN FABRIANTI KUSUMASARI ', '', '', '', '', '', 'FITRIA ', '', 20000000, 'valid ppm', 'Pembiayaan sendiri oleh peneliti', 'Penelitian Dosen', NULL, NULL, NULL, NULL, 1),
(39, 'PLD_39', 'SISTEM INFORMASI', 'TS', 'PENELITIAN KEMITRAAN 2016 (TAHAP I)', 'PENENTUAN RACKING SELECTION DAN PERANCANGAN ALOKASI PENYIMPANAN PADA GUDANG PT BINA SINAR AMITY, SERTA PENENTUAN ROUTING METHOD MENGGUNAKAN DYNAMIC PROGRAMMING DAN PENDEKATAN ALGORITMA GENETIK SERTA PENGAPLIKASIAN TEKNOLOGI PICK TO LIGHT UNTUK MENINGKATKAN ORDER FULFILLMENT', '31/3/2016', 'LUCIANA ANDRAWINA', 'DIDA DIAH DAMAYANTI, BUDI SANTOSA, ARI YANUAR RIDWAN', '', '', '', '', 'ERLANGGA BAYU SETYAWAN, NIA NOVITASARI', '', 70000000, 'valid ppm', 'PT yang bersangkutan', 'Penelitian Dosen', NULL, NULL, NULL, NULL, 1),
(40, 'PLD_40', 'SISTEM INFORMASI', 'TS', 'PENELITIAN KEMITRAAN 2016 (TAHAP I)', 'PERANCANGAN DAN PENGEMBANGAN INTEGRATED SINGLE WINDOW DAN MONITORING E-GOVERNMENT BERBASIS SERVICE-ORIENTED ARCHITECTURE', '31/3/2016', 'YULI ADAM PRASETYO ', 'NUR ICHSAN UTAMA, LUTHFI RAMADANI, SONI FAJAR SURYA GUMILANG, RIDHA HANAFI', '', '', '', '', 'LANUSGANA AMERTA, DHANY NURDIANSYAH', '', 57490000, 'valid ppm', 'PT yang bersangkutan', 'Penelitian Dosen', NULL, NULL, NULL, NULL, 1),
(41, 'PLD_41', 'SISTEM INFORMASI', 'TS', 'PENELITIAN DANA INTERNAL 2016 (TAHAP I)', 'PERANCANGAN DAN REALISASI SISTEM PEMBANGKIT LISTRIK TENAGA HIBRID BERDAYA RENDAH', '31/3/2016', 'TATANG MULYANA ', 'EKKI KURNIAWAN, RD. ROHMAT SAEDUDIN', '', '', '', '', 'MUHAMMAD FARIS IZZUDDIN, MOHAMMAD TAJUDIN ', '', 9500000, 'valid ppm', 'PT yang bersangkutan', 'Penelitian Dosen', NULL, NULL, NULL, NULL, 1),
(42, 'PLD_42', 'SISTEM INFORMASI', 'TS', 'PENELITIAN KEMITRAAN 2016 (TAHAP I)', 'Perancangan Strategi Implementasi Sistem Informasi Pelayanan Publik di RW 15 Kelurahan Sekeloa Kota Bandung', '31/3/2016', 'ILHAM PERDANA ', 'NIA AMBARSARI, R. WAHJOE WITJAKSONO, NUR ICHSAN UTAMA', '', '', '', '', 'DWI PRATAMA ', '', 65000000, 'valid ppm', 'PT yang bersangkutan', 'Penelitian Dosen', NULL, NULL, NULL, NULL, 1),
(43, 'PLD_43', 'SISTEM INFORMASI', 'TS', 'PENELITIAN KEMITRAAN 2016 (TAHAP I)', 'SISTEM ANALISA PENENTU KESESUAIAN ANTARA ANAK DAN ORANG TUA ASUH UNTUK MENCEGAH TINDAK KEKERASAN PADA ANAK DENGAN METODE CLASSIFICATION AND REGRESSION TREES (CART)', '31/3/2016', 'IRFAN DARMAWAN ', 'WARIH PUSPITASARI, RACHMADITA ANDRESWARI', '', '', '', '', 'NUR INTAN PARAMANISA, ATIKA ELYSIA, PRATIWI GALUH PUTRI', '', 70000000, 'valid ppm', 'PT yang bersangkutan', 'Penelitian Dosen', NULL, NULL, NULL, NULL, 1),
(44, 'PLD_44', 'SISTEM INFORMASI', 'TS', 'PENELITIAN DANA INTERNAL 2016 ', 'RANCANG BANGUN ODOO MODUL WAREHOUSE PADA GUDANG PT. TARUMATEX MENGGUNAKAN METODE RAPID APPLICATION DEVELOPMENT', '2016', 'DEDEN WITARSYAH', 'R. WAHJOE WITJAKSONO', '', '', '', '', 'PARASETIA ABU ADITYA, ALDI MUSTAFRI', '', 10731500, 'TA', 'PT yang bersangkutan', 'Penelitian TA', NULL, NULL, NULL, NULL, 1),
(45, 'PLD_45', 'SISTEM INFORMASI', 'TS', 'PENELITIAN DANA INTERNAL 2016 ', 'RANCANG BANGUN E-COMMERCE TANDURAN UNTUK MENINGKATKAN DAYA JUAL PELAKU BISNIS PERTANIAN', '2016', 'IRFAN DARMAWAN', 'FAISHAL MUFIED AL-ANSHARY', '', '', '', '', 'ELVIRA LAILATUTH THOHIROH, RAULIA RISKI, I MADE GELGEL WESNAWA PUTRA, MUHAMMAD HILHAM RAMADHAN', '', 11353000, 'TA', 'PT yang bersangkutan', 'Penelitian TA', NULL, NULL, NULL, NULL, 1),
(46, 'PLD_45', 'SISTEM INFORMASI', 'TS', 'PENELITIAN DANA INTERNAL 2016 ', 'PENGEMBANGAN SISTEM RENTAL MOBIL ONLINE MENGGUNAKAN METODE ITERATIVE INCREMENTAL (www.rental-go.com)', '2016', 'IRFAN DARMAWAN', 'FAISHAL MUFIED AL-ANSHARY', '', '', '', '', 'FHATYA ANDINI,  GINA MARIAM GUSTINA', '', 11316700, 'TA', 'PT yang bersangkutan', 'Penelitian TA', NULL, NULL, NULL, NULL, 1),
(47, 'PLD_47', 'SISTEM INFORMASI', 'TS', 'PENELITIAN DANA INTERNAL 2016 ', 'MEMBANGUN APLIKASI E-COMMERCE MANAJEMEN HUBUNGAN PELANGGAN UNTUK CV. PERCEKA MENGGUNAKAN FRAMEWORK LARAVEL DENGAN METODE ITERATIVE INCREMENTAL', '2016', 'IRFAN DARMAWAN', 'TAUFIK NUR ADI', '', '', '', '', 'CAKRA WARDHANA, TRIYADI YANUAR', '', 11975600, 'TA', 'PT yang bersangkutan', 'Penelitian TA', NULL, NULL, NULL, NULL, 1),
(48, 'PLD_48', 'SISTEM INFORMASI', 'TS', 'PENELITIAN DANA INTERNAL 2016 ', 'PERANCANGAN DAN PEMBANGUNAN KNOWLEDGE MANAGEMENT SYSTEM DENGAN MENGGUNAKAN METODE ITERATIVE INCREMENTAL DI DORMITORY UNIVERSITAS TELKOM', '2016', 'LUCIANA ANDRAWINA', 'FAISHAL MUFIED AL-ANSHARY', '', '', '', '', 'ABDI ROBANA AGNIA, MOCHAMAD ANNAFIE YANUAR HAKIM', '', 11779000, 'TA', 'PT yang bersangkutan', 'Penelitian TA', NULL, NULL, NULL, NULL, 1),
(49, 'PLD_49', 'SISTEM INFORMASI', 'TS', 'PENELITIAN DANA INTERNAL 2016 ', 'PERANCANGAN EXPERT LOCATOR SYSTEM PADA PT. TELEKOMUNIKASI SELULAR MENGGUNAKAN METODE ITERATIVE INCREMENTAL', '2016', 'LUCIANA ANDRAWINA', 'MUHAMMAD AZANI HASIBUAN', '', '', '', '', 'ATIKA LUTHFIANI IFAN, SELLY LARASATI, JAN FANDRO SIAHAAN, MAWADDAH TIFANI', '', 10369700, 'TA', 'PT yang bersangkutan', 'Penelitian TA', NULL, NULL, NULL, NULL, 1),
(50, 'PLD_50', 'SISTEM INFORMASI', 'TS', 'PENELITIAN DANA INTERNAL 2016 ', 'PERANCANGAN SISTEM INFORMASI BERBASIS ODOO DENGAN SOFT SYSTEM METHODOLOGY DI RUMAH SAKIT MUHAMMADIYAH BANDUNG', '2016', 'LUCIANA ANDRAWINA', 'R. WAHJOE WITJAKSONO', '', '', '', '', 'ADAM RAMADHAN, AHMAD AKBAR LINGGO M, RIZA RAHMA PUTRI, ', '', 12088000, 'TA', 'PT yang bersangkutan', 'Penelitian TA', NULL, NULL, NULL, NULL, 1),
(51, 'PLD_51', 'SISTEM INFORMASI', 'TS', 'PENELITIAN DANA INTERNAL 2016 ', 'IMPLEMENTASI ENTERPRISE RESOURCE PLANNING MENGGUNAKAN SOFTWARE ODOO DENGAN METODE RAPID APPLICATION DEVELOPMENT (STUDI KASUS: PT. TRIKOMSA INDONESIA', '2016', 'MUHAMMAD AZANI HASIBUAN', 'FAISHAL MUFIED AL-ANSHARY', '', '', '', '', 'SYAHRIANDA, IDA BAGUS GEDE ROMA HARSANA PUTRA, ', '', 11634000, 'TA', 'PT yang bersangkutan', 'Penelitian TA', NULL, NULL, NULL, NULL, 1),
(52, 'PLD_52', 'SISTEM INFORMASI', 'TS', 'PENELITIAN DANA INTERNAL 2016 ', 'PERANCANGAN TATA KELOLA MANAJEMEN LAYANAN TEKNOLOGI INFORMASI BERDASARKAN ITIL V3 DI PEMERINTAH KOTA BANDUNG', '2016', 'MURAHARTAWATY', 'LUTHFI RAMADANI', '', '', '', '', 'FIKROTUN NADIYYA, LUKI AISHA KUSUMA WARDANI', '', 10313000, 'TA', 'PT yang bersangkutan', 'Penelitian TA', NULL, NULL, NULL, NULL, 1),
(53, 'PLD_53', 'SISTEM INFORMASI', 'TS', 'PENELITIAN DANA INTERNAL 2016 ', 'ANALISIS DAN PERANCANGAN TATA KELOLA TI MENGGUNAKAN FRAMEWORK COBIT 4.1 : STUDI KASUS PT BIO FARMA (PERSERO)', '2016', 'MURAHARTAWATY', 'R. WAHJOE WITJAKSONO', '', '', '', '', 'AMANDA YUNIA ZAFARINA, INAYATUL MAGHFIROH', '', 12012000, 'TA', 'PT yang bersangkutan', 'Penelitian TA', NULL, NULL, NULL, NULL, 1),
(54, 'PLD_54', 'SISTEM INFORMASI', 'TS', 'PENELITIAN DANA INTERNAL 2016 ', 'PERANCANGAN SISTEM INFORMASI PADA LIPI KEBUN RAYA CIBODAS MENGGUNAKAN METODE WATERFALL', '2016', 'M. TEGUH KURNIAWAN', 'ADITYAS WIDJAJARTO', '', '', '', '', 'RIKI RAMDHANI, NAUFAL AFRA FIRDAUS, RIDEL EZRI SAMBOW', '', 11800000, 'TA', 'PT yang bersangkutan', 'Penelitian TA', NULL, NULL, NULL, NULL, 1),
(55, 'PLD_55', 'SISTEM INFORMASI', 'TS', 'PENELITIAN DANA INTERNAL 2016 ', 'PENERAPAN SISTEM ERP BERBASIS ODOO DENGAN METODE SPIRAL PADA PT. PROGRESSIO INDONESIA', '2016', 'M. TEGUH KURNIAWAN', 'R. WAHJOE WITJAKSONO', '', '', '', '', 'I KOMANG JAKA AKSARA WIGUNA, SHELVIA ARDIAN PERDANA, MUHAMMAD AMMAR RIFQI, ', '', 11659500, 'TA', 'PT yang bersangkutan', 'Penelitian TA', NULL, NULL, NULL, NULL, 1),
(56, 'PLD_56', 'SISTEM INFORMASI', 'TS', 'PENELITIAN DANA INTERNAL 2016 ', 'ANALISIS DAN PERANCANGAN INFRASTRUKTUR JARINGAN KOMPUTER DI PEMERINTAHAN KABUPATEN BANDUNG', '2016', 'M. TEGUH KURNIAWAN', 'UMAR YUNAN KURNIA SEPTO HERDIYANTO', '', '', '', '', 'ARIES ARIEFFANDY SUMARSO, FAISAL RIZIQ AKBAR, AKBAR MAULANA DAFIK TORICA, NAZWAR SYAMSU, KARNANDO NABABAN', '', 11000000, 'TA', 'PT yang bersangkutan', 'Penelitian TA', NULL, NULL, NULL, NULL, 1),
(57, 'PLD_57', 'SISTEM INFORMASI', 'TS', 'PENELITIAN DANA INTERNAL 2016 ', 'PENGEMBANGAN SISTEM INFORMASI PADA WEBSITE SAHABATKERTAS.COM', '2016', 'NIA AMBARSARI', 'ILHAM PERDANA', '', '', '', '', 'FAUZAN MEDISON ADRYAN, ARIZONA DWI FAJARWATI, GALANG TANAH MERDEKA, ', '', 10500000, 'TA', 'PT yang bersangkutan', 'Penelitian TA', NULL, NULL, NULL, NULL, 1),
(58, 'PLD_58', 'SISTEM INFORMASI', 'TS', 'PENELITIAN DANA INTERNAL 2016 ', 'MEMBANGUN SISTEM INFORMASI E-SCHOOL BERBASIS QR CODE MENGGUNAKAN METODE EXTREME PROGRAMMING DI SMPN 13 BANDUNG', '2016', 'NIA AMBARSARI', 'MUHAMMAD AZANI HASIBUAN', '', '', '', '', 'TIMBUL PRAWIRA GULTOM, MOCHAMAD THARIQ JANUAR, ERLIN SUSILOWATI, MUCHAMAD REZA JULIANSYAH', '', 12500000, 'TA', 'PT yang bersangkutan', 'Penelitian TA', NULL, NULL, NULL, NULL, 1),
(59, 'PLD_59', 'SISTEM INFORMASI', 'TS', 'PENELITIAN DANA INTERNAL 2016 ', 'PENGEMBANGAN SISTEM ERP MENGGUNAKAN ODOO PADA PT. PUTRI DAYA USAHATAMA DENGAN METODE ASAP', '2016', 'RD. ROHMAT SAEDUDIN', 'R. WAHJOE WITJAKSONO', '', '', '', '', 'INTAN DWI ARIESTA PUTRI, UGI CHANDRA WIGUNA, MOCHAMAD HAFIZ KURNIAWAN, ', '', 12340000, 'TA', 'PT yang bersangkutan', 'Penelitian TA', NULL, NULL, NULL, NULL, 1),
(60, 'PLD_60', 'SISTEM INFORMASI', 'TS', 'PENELITIAN DANA INTERNAL 2016 ', 'IMPLEMENTASI ERP BERBASIS ODOO PADA PT. PRIMARINDO ASIA INFRASTRUCTURE Tbk DENGAN METODOLOGI ASAP', '2016', 'RD. ROHMAT SAEDUDIN', 'R. WAHJOE WITJAKSONO', '', '', '', '', 'YUANIKA INDANEA, FIRDAYAKUSMAWARNI, FIEGA DWI NOVWARI, VEGI FRANSISCA, IBRAHIM HANIF ALKHALIL', '', 11500000, 'TA', 'PT yang bersangkutan', 'Penelitian TA', NULL, NULL, NULL, NULL, 1),
(61, 'PLD_61', 'SISTEM INFORMASI', 'TS', 'PENELITIAN DANA INTERNAL 2016 ', 'PENGEMBANGAN ERP BERBASIS ODOO DENGAN METODE ACCELERATED SAP PADA INGLORIOUS INDUSTRIES', '2016', 'RD. ROHMAT SAEDUDIN', 'R. WAHJOE WITJAKSONO', '', '', '', '', 'FIEGA DWI NOVWARI, VEGI FRANSISCA, IBRAHIM HANIF ALKHALIL', '', 10200000, 'TA', 'PT yang bersangkutan', 'Penelitian TA', NULL, NULL, NULL, NULL, 1),
(62, 'PLD_62', 'SISTEM INFORMASI', 'TS', 'PENELITIAN DANA INTERNAL 2016 ', 'PENERAPAN ERP ODOO PADA PT. PRIMARINDO ASIA INFRASTRUCTURE TBK. DENGAN METODE RAPID APPLICATION DEVELOPMENT', '2016', 'R. WAHJOE WITJAKSONO', 'FAISHAL MUFIED AL-ANSHARY', '', '', '', '', 'ERLA AYU PERTIWI, INDRA ROLANDO SINAGA, KEVIN ROHNI GOKLAS SINAGA, ', '', 11378800, 'TA', 'PT yang bersangkutan', 'Penelitian TA', NULL, NULL, NULL, NULL, 1),
(63, 'PLD_63', 'SISTEM INFORMASI', 'TS', 'PENELITIAN DANA INTERNAL 2016 ', 'PENGEMBANGAN SISTEM INFORMASI  ERP ODOO DENGAN METODE RAPID APPLICATION DEVELOPMENT DI PT ADETEX FILAMENT', '2016', 'R. WAHJOE WITJAKSONO', 'FAISHAL MUFIED AL-ANSHARY', '', '', '', '', 'SITI ALMIRA DANIA, DINTA AYU SHAVIRA, AIDIYA SAFIRA, ', '', 12424000, 'TA', 'PT yang bersangkutan', 'Penelitian TA', NULL, NULL, NULL, NULL, 1),
(64, 'PLD_64', 'SISTEM INFORMASI', 'TS', 'PENELITIAN DANA INTERNAL 2016 ', 'PENERAPAN SISTEM ERP MENGGUNAKAN ODOO PADA PERUSAHAAN TEKSTIL MENGGUNAKAN METODE ACCELERATED SAP (Studi Kasus PT. SAMPOERNA JAYA SENTOSA)', '2016', 'R. WAHJOE WITJAKSONO', 'NIA AMBARSARI', '', '', '', '', 'WIDYASARI OKTAVIANI, ROBBY PANGESTU, MASANGGA FERBIYANA MUKTI, ', '', 10766000, 'TA', 'PT yang bersangkutan', 'Penelitian TA', NULL, NULL, NULL, NULL, 1),
(65, 'PLD_65', 'SISTEM INFORMASI', 'TS', 'PENELITIAN DANA INTERNAL 2016 ', 'PENGEMBANGAN SISTEM INFORMASI BERBASIS ERP MENGGUNAKAN SAP DENGAN METODE ASAP DI PT. LEN INDUSTRI', '2016', 'R. WAHJOE WITJAKSONO', 'NIA AMBARSARI', '', '', '', '', 'CLARISSA DILASARI SETIAWAN, DELIS SEPTIANTI BALGIS, I MADE TEKAD KALIMANTARA, DWI PRATAMA, ', '', 10065000, 'TA', 'PT yang bersangkutan', 'Penelitian TA', NULL, NULL, NULL, NULL, 1),
(66, 'PLD_66', 'SISTEM INFORMASI', 'TS', 'PENELITIAN DANA INTERNAL 2016 ', 'MEMBANGUN APLIKASI KEMAHASISWAAN BERBASIS WEB MENGGUNAKAN METODE ITERATIVE AND INCREMENTAL', '2016', 'SONI FAJAR SURYA GUMILANG', 'MUHAMMAD AZANI HASIBUAN', '', '', '', '', 'Reza Harli Saputra, ANTON ILMIAR WINDRISWARA, ANISATUN NAFIAH, NURRIDA AINI ZUHROH, ', '', 11530000, 'TA', 'PT yang bersangkutan', 'Penelitian TA', NULL, NULL, NULL, NULL, 1),
(67, 'PLD_67', 'SISTEM INFORMASI', 'TS', 'PENELITIAN DANA INTERNAL 2016 ', 'PERANCANGAN SISTEM INFORMASI PADA AGROBISNIS DAN PARIWISATA DESA CIBODAS KECAMATAN LEMBANG MENGGUNAKAN METODE ITERATIVE INCREMENTAL', '2016', 'SONI FAJAR SURYA GUMILANG', 'NUR ICHSAN UTAMA', '', '', '', '', 'CAHYA NOFANDIYAN PUTRA, ADNAN BAHARRUDIN FANANI, SLAMET MAMAT RACHMAT', '', 10850000, 'TA', 'PT yang bersangkutan', 'Penelitian TA', NULL, NULL, NULL, NULL, 1),
(68, 'PLD_68', 'SISTEM INFORMASI', 'TS', 'PENELITIAN DANA INTERNAL 2016 ', 'PERANCANGAN APLIKASI SOSIAL ENTERPRISE PADA PERANGKAT ANDROID DENGAN MENGGUNAKAN SCRUM DEVELOPMENT', '2016', 'SONI FAJAR SURYA GUMILANG', 'NUR ICHSAN UTAMA', '', '', '', '', 'AGUNG CANDRA DUTIYA PURWANTA, ADITYA PRATAMA NUGRAHA RAHMAYADI, ', '', 11600000, 'TA', 'PT yang bersangkutan', 'Penelitian TA', NULL, NULL, NULL, NULL, 1),
(69, 'PLD_69', 'SISTEM INFORMASI', 'TS', 'PENELITIAN DANA INTERNAL 2016 ', 'PENGEMBANGAN SISTEM INFORMASI BERBASIS ENTERPRISE RESOURCE PLANNING PADA ODOO DENGAN METODE RAPID APPLICATION DEVELOPMENT DI PT. BRODO GANESHA INDONESIA', '2016', 'SONI FAJAR SURYA GUMILANG', 'R. WAHJOE WITJAKSONO', '', '', '', '', 'AJI WIRA PRADHANA, RISNA RISDIANTI JUNIAR, ANITA RAHMA MAULIDA, PARAMITA RAHMAWATI', '', 11041000, 'TA', 'PT yang bersangkutan', 'Penelitian TA', NULL, NULL, NULL, NULL, 1),
(70, 'PLD_70', 'SISTEM INFORMASI', 'TS', 'PENELITIAN DANA INTERNAL 2016 ', 'PERANCANGAN ENTERPRISE ARCHITECTURE MENGGUNAKAN FRAMEWORK TOGAF ADM PADA PT. HERONA EXPRESS', '2016', 'YULI ADAM PRASETYO ', 'BASUKI RAHMAD', '', '', '', '', 'FAISAL WISNU PRADHANA, HENDRIK HENDRIANA FIRMANSYAH, IAN FAHMI NUGRAHA, ', '', 11362000, 'TA', 'PT yang bersangkutan', 'Penelitian TA', NULL, NULL, NULL, NULL, 1),
(71, 'PLD_71', 'SISTEM INFORMASI', 'TS', 'PENELITIAN DANA INTERNAL 2016 ', 'PERANCANGAN ENTERPRISE ARCHITECTURE DI PT. PLN DISTRIBUSI JAWA BARAT MENGGUNAKAN FRAMEWORK TOGAF ADM', '2016', 'YULI ADAM PRASETYO ', 'FAISHAL MUFIED AL-ANSHARY', '', '', '', '', 'RECSA ANDRIYANI PUTRI, MUTIA DEWI KURNIASIH, RINALDI HARRY LEKSANA, ERSA NOVIA FAJRIN', '', 10727000, 'TA', 'PT yang bersangkutan', 'Penelitian TA', NULL, NULL, NULL, NULL, 1),
(72, 'PLD_72', 'SISTEM INFORMASI', 'TS', 'PENELITIAN DANA INTERNAL 2016 ', 'ANALISIS DAN PERANCANGAN APLIKASI PERSONAL FILE MENGGUNAKAN METODE ITERATIVE DAN INCREMENTAL PADA TELKOM PCC', '2016', 'YULI ADAM PRASETYO ', 'IRFAN DARMAWAN', '', '', '', '', 'IRVAN GUNAWAN, BALA PUTRA DEWA, ', '', 11090000, 'TA', 'PT yang bersangkutan', 'Penelitian TA', NULL, NULL, NULL, NULL, 1),
(73, 'PLD_73', 'SISTEM INFORMASI', 'TS', 'PENELITIAN DANA INTERNAL 2016 ', 'PERANCANGAN ENTERPRISE ARCHITECTURE BAPPEDA KABUPATEN BANDUNG MENGGUNAKAN FRAMEWORK TOGAF ADM', '2016', 'YULI ADAM PRASETYO ', 'RIDHA HANAFI', '', '', '', '', 'WIDYATASYA AGUSTIKA NURTRISHA, MUHAMMAD FACHRY PUTERA PRATAMA, ADELIA INDAH OKTAVIYANTI, ', '', 11824000, 'TA', 'PT yang bersangkutan', 'Penelitian TA', NULL, NULL, NULL, NULL, 1),
(74, 'PLD_74', 'SISTEM INFORMASI', 'TS', 'PENELITIAN DANA INTERNAL 2016 ', 'ANALISIS DAN PERANCANGAN ENTERPRISE ARCHITECTURE PADA BADAN PERENCANAAN DAN PEMBANGUNAN DAERAH (BAPPEDA) PROVINSI JAWA BARAT MENGGUNAKAN FRAMEWORK TOGAF ADM', '2016', 'YULI ADAM PRASETYO ', 'RAHMAT MULYANA', '', '', '', '', 'DUMAULI NOVITASARI BORU SIMANJUNTAK, AFRIANDA GAZA ONTOREZA, ANIDA SHAFA', '', 11763000, 'TA', 'PT yang bersangkutan', 'Penelitian TA', NULL, NULL, NULL, NULL, 1),
(75, 'PLD_75', 'SISTEM INFORMASI', 'TS', 'PENELITIAN DANA INTERNAL 2016 ', 'PERANCANGAN ARSITEKTUR BISNIS, DATA,TEKNOLOGI DAN APLIKASI UNTUK LAYANAN JASA KEUANGAN PADA PT.POS INDONESIA MENGGUNAKAN TOGAF ADM', '2016', 'YULI ADAM PRASETYO ', 'RAHMAT MULYANA', '', '', '', '', 'AFFIFIANA PRISYANTI, DELPHINE YUSTICIA RATNASARI, CITRA MELATI', '', 11291900, 'TA', 'PT yang bersangkutan', 'Penelitian TA', NULL, NULL, NULL, NULL, 1),
(76, 'PLD_76', 'SISTEM INFORMASI', 'TS', 'PENELITIAN DANA INTERNAL 2016 ', 'PERANCANGAN ENTERPRISE ARCHITECTURE MENGGUNAKAN FRAMEWORK TOGAF ADM BAPAPSI KABUPATEN BANDUNG', '2016', 'YULI ADAM PRASETYO ', 'SONI FAJAR SURYA GUMILANG', '', '', '', '', 'FARID HAKIM NISWANSYAH, EMA TRIA WAHYUNINGTIHAS, ', '', 12300000, 'TA', 'PT yang bersangkutan', 'Penelitian TA', NULL, NULL, NULL, NULL, 1),
(77, 'PLD_77', 'SISTEM INFORMASI', 'TS', 'PENELITIAN DANA INTERNAL 2016 ', 'PENGEMBANGAN BUSINESS INTELLIGENCE SYSTEM BERBASIS DATA WAREHOUSE MENGGUNAKAN PENTAHO DENGAN METODOLOGI BUSINESS DIMENSIONAL LIFE-CYCLE', '2016', 'ARI YANUAR RIDWAN', 'RACHMADITA ANDRESWARI', '', '', '', '', 'M. FIRMAN HELMI ARIYANSYAH, RIFKIANSYAH ABDILA', '', 12200000, 'TA', 'PT yang bersangkutan', 'Penelitian TA', NULL, NULL, NULL, NULL, 1),
(78, 'PLD_78', 'SISTEM INFORMASI', 'TS', 'PENELITIAN DANA INTERNAL 2016 ', 'PENGEMBANGAN APLIKASI E-OFFICE PADA BADAN KEPEGAWAIAN DAERAH PROVINSI JAWA BARAT DENGAN MENGGUNAKAN METODE RAD', '2015', 'ARI YANUAR RIDWAN', 'RACHMADITA ANDRESWARI', '', '', '', '', 'YUDHA ADITYA RAMADHANA, REGINA AYU PRAMESWARI WADE', '', 10612000, 'TA', 'PT yang bersangkutan', 'Penelitian TA', NULL, NULL, NULL, NULL, 1),
(79, 'PLD_79', 'SISTEM INFORMASI', 'TS', 'PENELITIAN DANA INTERNAL 2016 ', 'PENGEMBANGAN SISTEM ERP BERBASIS ADEMPIERE DENGAN METODOLOGI ASAP PADA PERUM BULOG DIVRE JABAR', '2016', 'ARI YANUAR RIDWAN', 'R. WAHJOE WITJAKSONO', '', '', '', '', 'VICTOR MAROLOP PATRIASI SIREGAR, ARNOLD REWADIPO PURBA, ', '', 11500000, 'TA', 'PT yang bersangkutan', 'Penelitian TA', NULL, NULL, NULL, NULL, 1),
(80, 'PLD_80', 'SISTEM INFORMASI', 'TS', 'PENELITIAN DANA INTERNAL 2016 ', 'PERANCANGAN SISTEM INFORMASI BERBASIS OPENERP DENGAN METODE SOFT SYSTEM METHODOLOGY (STUDI KASUS: RUMAH SAKIT UMUM DAERAH AL-IHSAN)', '2015', 'ARI YANUAR RIDWAN, S.T., M.T.', 'R. WAHJOE WITJAKSONO', '', '', '', '', 'PUTU GEDE WIBAWA HARTAWAN, ZENITA ROKHMANINGSIH, SATRIA NARENDRA WIBAWA', '', 10300000, 'TA', 'PT yang bersangkutan', 'Penelitian TA', NULL, NULL, NULL, NULL, 1),
(81, 'PLD_81', 'SISTEM INFORMASI', 'TS', 'PENELITIAN DANA INTERNAL 2016 ', 'PERANCANGAN SISTEM INFORMASI PADA PABRIK GULA JATIBARANG DENGAN MENGGUNAKAN SOFT SYSTEM METHODOLOGY', '2015', 'DEDEN WITARSYAH', 'R. WAHJOE WITJAKSONO', '', '', '', '', 'NADILA LINTANG HAPSARI, NILASARI NURAMANATI', '', 11100000, 'TA', 'PT yang bersangkutan', 'Penelitian TA', NULL, NULL, NULL, NULL, 1),
(82, 'PLD_82', 'SISTEM INFORMASI', 'TS', 'PENELITIAN DANA INTERNAL 2016 ', 'PENERAPAN SISTEM INFORMASI MENGGUNAKAN APLIKASI ERP OPEN SOURCE DENGAN METODOLOGY AGILE (STUDI KASUS : RESTORAN DAN RESORT DE TUIK)', '2015', 'DEDEN WITARSYAH', 'R. WAHJOE WITJAKSONO', '', '', '', '', 'NURULLIANI SAFITRI MUTMAINAH, FABIYOLA NINDYA SUSILO, ', '', 11500000, 'TA', 'PT yang bersangkutan', 'Penelitian TA', NULL, NULL, NULL, NULL, 1),
(83, 'PLD_83', 'SISTEM INFORMASI', 'TS', 'PENELITIAN DANA INTERNAL 2016 ', 'PEMBANGUNAN PORTAL WEB CROWDSOURCING EVENT PERGURUAN TINGGI MENGGUNAKAN METODE ITERATIVE INCREMENTAL', '2015', 'IRFAN DARMAWAN', 'TAUFIK NUR ADI', '', '', '', '', 'ALIFIA INDRA DAMARANI, MIA MEILANI, ', '', 10300000, 'TA', 'PT yang bersangkutan', 'Penelitian TA', NULL, NULL, NULL, NULL, 1),
(84, 'PLD_84', 'SISTEM INFORMASI', 'TS', 'PENELITIAN DANA INTERNAL 2016 ', 'MEMBANGUN WEBSITE MANAJEMEN INVESTASI BERBASIS CROWDFUNDING MENGGUNAKAN METODE ITERATIVE DAN INCREMENTAL', '2015', 'NIA AMBARSARI', 'TAUFIK NUR ADI', '', '', '', '', 'FAUZA ANNISA, FATHIMAH MUTHI LUTHFIYAH, MADE FEBRIYANA DYASTAMA PUTRA', '', 10866000, 'TA', 'PT yang bersangkutan', 'Penelitian TA', NULL, NULL, NULL, NULL, 1),
(85, 'PLD_85', 'SISTEM INFORMASI', 'TS', 'PENELITIAN DANA INTERNAL 2016 ', 'PENGEMBANGAN APLIKASI WEB MANAJEMEN ASET PEMERINTAHAN KABUPATEN BANDUNG MENGGUNAKAN METODE ITERATIVE DAN INCREMENTAL', '2015', 'SENO ADI PUTRA', 'DEDEN WITARSYAH', '', '', '', '', 'ADVENTUS ANGGA KURNIAWAN, DENANDRA PRADIPTA, ', '', 10816000, 'TA', 'PT yang bersangkutan', 'Penelitian TA', NULL, NULL, NULL, NULL, 1),
(86, 'PLD_86', 'SISTEM INFORMASI', 'TS', 'PENELITIAN DANA INTERNAL 2016 ', 'PENGEMBANGAN WEB PORTAL UKM CROWDFUNDING DENGAN METODE PROTOTYPE DAN FRAMEWORK CODEIGNITER', '2015', 'YULI ADAM PRASETYO', 'FAISHAL MUFIED AL-ANSHARY', '', '', '', '', 'ARIS SETYO NUGROHO, HARPANDI WIBOWO', '', 10400000, 'TA', 'PT yang bersangkutan', 'Penelitian TA', NULL, NULL, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_publikasi`
--

CREATE TABLE `tb_publikasi` (
  `id` int(11) NOT NULL,
  `prodi` varchar(200) DEFAULT NULL,
  `no_ts` varchar(50) DEFAULT NULL,
  `skl_publikasi` varchar(100) DEFAULT NULL,
  `judul` varchar(500) DEFAULT NULL,
  `jns_publikasi` varchar(200) DEFAULT NULL,
  `nm_seminar` varchar(500) DEFAULT NULL,
  `penulis_utm` varchar(200) DEFAULT NULL,
  `penulis_tmbh` varchar(500) DEFAULT NULL,
  `penulis_tmbh2` varchar(300) DEFAULT NULL,
  `penulis_tmbh3` varchar(300) DEFAULT NULL,
  `penulis_tmbh4` varchar(300) DEFAULT NULL,
  `penulis_tmbh5` varchar(300) DEFAULT NULL,
  `tanggal` varchar(200) DEFAULT NULL,
  `ket` varchar(1000) DEFAULT NULL,
  `jlm_dosen_si` varchar(500) DEFAULT NULL,
  `sts_valid` varchar(50) DEFAULT NULL,
  `file_names` varchar(500) NOT NULL,
  `file_types` varchar(500) NOT NULL,
  `file_dates` varchar(100) NOT NULL,
  `file_name` varchar(500) NOT NULL,
  `file_type` varchar(500) NOT NULL,
  `file_date` varchar(500) NOT NULL,
  `id_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_publikasi`
--

INSERT INTO `tb_publikasi` (`id`, `prodi`, `no_ts`, `skl_publikasi`, `judul`, `jns_publikasi`, `nm_seminar`, `penulis_utm`, `penulis_tmbh`, `penulis_tmbh2`, `penulis_tmbh3`, `penulis_tmbh4`, `penulis_tmbh5`, `tanggal`, `ket`, `jlm_dosen_si`, `sts_valid`, `file_names`, `file_types`, `file_dates`, `file_name`, `file_type`, `file_date`, `id_user`) VALUES
(1, 'SISTEM INFORMASI', 'TS_2', 'Internasional', 'IT Value Analysis by Resource-Based View Theory: The Case Study of PT. Telekomunikasi Indonesia, Tbk.', 'Jurnal Internasional (Tidak Terindeks Scopus)', 'LEARNING ORGANIZATION: Management and Business International Journal, 1(1), pp. 55-70, 2013.', 'LUKMAN ABDURRAHMAN ', 'Armein Z.R. Langi dan Suhardi', NULL, NULL, NULL, NULL, '12-04-2013', 'valid', '1', 'valid', '', '', '', '', '', '', 1),
(2, 'SISTEM INFORMASI', 'TS_2', 'Internasional', 'Implementation of Analytic Network Process (ANP) and Analytic Hierarchy Process (AHP) Method to Determine Priorities of Roads to Be Repaired At Bogor City Department of Public Works', 'Prosiding Internasional Tidak Terindeks Scopus', 'International Conference of Information and Communication Technology ( ICoICT ) 2013', 'Riza Agustiansyah', NULL, NULL, NULL, NULL, NULL, '13/8/2013', 'valid', '1', 'valid', '', '', '', '', '', '', 1),
(3, 'SISTEM INFORMASI', 'TS_1', 'Nasional', 'Analisis dan Desain Infrastruktur Jaringan Wireless di Universitas Telkom dengan Metode Network Development Life Cycle', 'Prosiding Nasional', 'Seminar Nasional Teknologi dan Sains', 'SALMAN FAROZI ', 'M. TEGUH KURNIAWAN ', NULL, NULL, NULL, NULL, '10-08-2014', 'valid', '1', 'valid', '', '', '', '', '', '', 1),
(4, 'SISTEM INFORMASI', 'TS_1', 'Internasional', 'CROWDSOURCING WEB MODEL OF PRODUCT REVIEW AND RATING BASED ON CONSUMER BEHAVIOUR MODEL USING MIXED SERVICE-ORIENTED SYSTEM DESIGN', 'Prosiding Internasional (Tidak Terindeks Scopus)', 'The 1st International Conference on Information Technology and Security', 'YULI ADAM PRASETYO ', NULL, NULL, NULL, NULL, NULL, '27/11/2014', 'valid', '1', 'valid', '', '', '', '', '', '', 1),
(5, 'SISTEM INFORMASI', 'TS_1', 'Internasional', 'Development of Methodologies for Measuring IT Capability in the Information and Communication Industries', 'Prosiding Internasional Terindeks Scopus', 'International Conference on Information Technology Systems and Innovation (ICITSI 2014)', 'LUKMAN ABDURRAHMAN ', 'Armein Z.R. Langi dan Suhardi', NULL, NULL, NULL, NULL, '24/11/2014', 'valid', '1', 'valid', '', '', '', '', '', '', 1),
(6, 'SISTEM INFORMASI', 'TS_2', 'Internasional', 'Collaborative Model in Construction Phase of Software', 'Prosiding Internasional Terindeks Scopus', '2014 International Conference on Information Technology Systems and Innovation (ICITSI)', 'TIEN FABRIANTI KUSUMASARI ', NULL, NULL, NULL, NULL, NULL, '24/11/2014', 'valid', '1', 'valid', '', '', '', '', '', '', 1),
(7, 'SISTEM INFORMASI', 'TS_1', 'Internasional', 'IMPLEMENTATION OF E-GOVERNMENT STRATEGIC PLANNING TO PROVIDE BETTER SERVICE FOR THE CITIZEN: THE CASE OF THE BANDUNG REGENCY GOVERNMENT', 'Jurnal Internasional Terindeks Scopus', 'International Conference on Emerging Trends in Academic Research??? (ETAR ???November 25-26, 2014) at Bali, Indonesia.', 'DEDEN WITARSYAH ', NULL, NULL, NULL, NULL, NULL, '25/11/2014', 'valid', '1', 'valid', '', '', '', '', '', '', 1),
(8, 'SISTEM INFORMASI', 'TS_2', 'Internasional', 'Comparison of RBV-Based Information Technology (IT) Value and IBM-Based IT Value Model', 'Prosiding Internasional (Tidak Terindeks Scopus)', '4th International Conference on Information Systems and Technologies', 'LUKMAN ABDURRAHMAN ', 'Armein Z.R. Langi dan Suhardi', NULL, NULL, NULL, NULL, '22/3/2014', 'valid', '1', 'valid', '', '', '', '', '', '', 1),
(9, 'SISTEM INFORMASI', 'TS_2', 'Internasional', 'Information Technology (IT) Value Model using Variance-Based Structural Equation Modeling: Towards IT Value Engineering', 'Jurnal Internasional Terindeks Scopus', 'The Second International Conference on Information and Communication Technology', 'LUKMAN ABDURRAHMAN ', 'Armein Z.R. Langi dan Suhardi', NULL, NULL, NULL, NULL, '28/5/2014', 'valid', '1', 'valid', '', '', '', '', '', '', 1),
(10, 'SISTEM INFORMASI', 'TS_1', 'Lokal', 'Pengembangan Aplikasi GIS Konservasi Sungai Cikapundung Bandung', 'Jurnal Nasional Tidak Terakreditasi', 'Jurnal Rekayasa Sistem dan Industri', 'YULI ADAM PRASETYO ', NULL, NULL, NULL, NULL, NULL, '31/10/2014', 'valid', '1', 'valid', '', '', '', '', '', '', 1),
(11, 'SISTEM INFORMASI', 'TS_2', 'Internasional', 'Modeling Information Technology (IT) Value: An Ontological Approach towards IT Value Engineering', 'Jurnal Internasional (Tidak Terindeks Scopus)', 'International Journal of Advances in Software Engineering & Research Methodology??? IJSERM Volume 1 : Issue 2', 'LUKMAN ABDURRAHMAN ', 'Armein Z.R. Langi dan Suhardi', NULL, NULL, NULL, NULL, '25/6/2014', 'valid', '1', 'valid', '', '', '', '', '', '', 1),
(12, 'SISTEM INFORMASI', 'TS_1', 'Internasional', 'Valuation of Information Technology (IT) Capability in the Business: A Case Study at PT. Telekomunikasi Indonesia, Tbk.', 'Jurnal Internasional Terindeks Scopus', 'International Journal on Electrical Engineering and Informatics', 'LUKMAN ABDURRAHMAN ', 'Armein Z.R. Langi dan Suhardi', NULL, NULL, NULL, NULL, 'PUBLIKASI PENELITIAN TRIWULAN II S.D. IV TAHUN 2014', 'valid', '1', 'valid', '', '', '', '', '', '', 1),
(13, 'SISTEM INFORMASI', 'TS_2', 'Nasional', 'Perancangan Topologi Jaringan Pada Pemerintah Kabupaten Bandung Dengan Metodologi NDLC Menggunakan GNS3', 'Prosiding Nasional', 'Seminar Nasional Teknologi Informasi dan Aplikasi', 'M FATHINUDDIN ', 'M. TEGUH KURNIAWAN, AMELIA KURNIAWATI ', NULL, NULL, NULL, NULL, '06-05-2014', 'valid', '1', 'valid', '', '', '', '', '', '', 1),
(14, 'SISTEM INFORMASI', 'TS_1', 'Internasional', 'Business Process Modeling of Lecturing Application using Heuristic Miner Algorithm', 'Prosiding Internasional Tidak Terindeks Scopus', 'Asia-Pasific Conference of Business Process Management 2015', 'RACHMADITA ANDRESWARI ', 'AULIA FASHANAH HADINING, ATYA NUR AISHA ', NULL, NULL, NULL, NULL, '23/6/2015', 'valid', '1', 'valid', '', '', '', '', '', '', 1),
(15, 'SISTEM INFORMASI', 'TS_1', 'Nasional', 'DASHBOARD OPERASIONAL UNTUK MEMANTAU PERFORMA JARINGAN SPEEDY MENURUT PENGUKURAN NETWORK ANALYSIS', 'Prosiding Nasional', 'Konferensi Nasional Sistem Informasi', 'RACHMADITA ANDRESWARI', 'RULLY AGUS HENDRAWAN, RADITYO PRASETIANTO WIBOWO', NULL, NULL, NULL, NULL, '26/2/2015', 'valid', '1', 'valid', '', '', '', '', '', '', 1),
(16, 'SISTEM INFORMASI', 'TS', 'Internasional', 'DEVELOPING MOBILE APPLICATION BASED ON CROWDSOURCING FOR CAR RENTAL MANAGEMENT USING SCRUM METHODOLOGY', 'Prosiding Internasional Tidak Terindeks Scopus', 'Indonesia International Conference on Business, Management and Communication 2015', 'KOMANG ADITYA RESPA PUTRA', 'SONI FAJAR SURYA GUMILANG, MUHAMMAD AZANI HASIBUAN', NULL, NULL, NULL, NULL, '27/8/2015', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(17, 'SISTEM INFORMASI', 'TS', 'Internasional', 'Development of Web Application Based on Crowdsourcing for Car Rental Management on Customer Side', 'Prosiding Internasional Tidak Terindeks Scopus', 'International Conference on Business, Management and Communication 2015', 'GHOZIYAH HAITAN RACHMAN', 'SONI FAJAR SURYA GUMILANG, TAUFIK NUR ADI', NULL, NULL, NULL, NULL, '27/8/2015', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(18, 'SISTEM INFORMASI', 'TS', 'Internasional', 'End-User Acceptance of E-Government Services in an Indonesia Regency', 'Prosiding Internasional Terindeks Scopus', 'IAES EECSI 2015', 'DEDEN WITARSYAH ', NULL, NULL, NULL, NULL, NULL, '19/8/2015', 'valid', '1', NULL, '', '', '', '', '', '', 1),
(19, 'SISTEM INFORMASI', 'TS_1', 'Internasional', 'Engineering information technology value in IT-based industries using partial adjustment valuation and resource-based view approach', 'Jurnal Internasional Terindeks Scopus', 'Int. J. Information and Communication Technology', 'LUKMAN ABDURRAHMAN ', 'SUHARDI dan ARMEIN Z.R. LANGI (STEI ITB)', NULL, NULL, NULL, NULL, '16/6/2015', 'valid', '1', NULL, '', '', '', '', '', '', 1),
(20, 'SISTEM INFORMASI', 'TS', 'Internasional', 'A New Search Direction for Broyden''s Family Method in Solving Unconstrained Optimization Problems', 'Jurnal Internasional Tidak Terindeks Scopus', 'Recent Advances on Soft Computing and Data Mining', 'MOHD ASRUL HERY IBRAHIM', 'Zailani Abdullah, Mohd Ashlyzan Razik, Irfan Darmawan, Sri Rahmawati, Tutut Herawan', NULL, NULL, NULL, NULL, '18/8/2016', 'valid', '1', NULL, '', '', '', '', '', '', 1),
(21, 'SISTEM INFORMASI', 'TS', 'Internasional', 'Instruments Measurement Design of Human Behavior in Collaborative Software Construction', 'Prosiding Internasional Terindeks Scopus', 'The 2015 International Conference on Advanced Informatics: Concepts, Theory and Application (ICAICTA2015)', 'TIEN FABRIANTI KUSUMASARI ', NULL, NULL, NULL, NULL, NULL, '19/8/2015', 'valid', '1', NULL, '', '', '', '', '', '', 1),
(22, 'SISTEM INFORMASI', 'TS', 'Internasional', 'ELP-M2: An Efficient Model for Mining Least Patterns from Data Repository', 'Jurnal Internasional Tidak Terindeks Scopus', 'Recent Advances on Soft Computing and Data Mining', 'ZAILANI ABDULLAH', 'Amir Ngah, Tutut Herawan, Noraziah Ahmad, Siti Zaharah Mohamad, Tien Fabrianti Kusumasari, Silfia Andini, and Abdul Razak Hamdan', NULL, NULL, NULL, NULL, '18/8/2016', 'valid', '1', NULL, '', '', '', '', '', '', 1),
(23, 'SISTEM INFORMASI', 'TS', 'Internasional', 'Integration of Self-Adaptation Approach on Requirements Modeling', 'Prosiding Internasional Terindeks Scopus', 'Recent Advances on Soft Computing and Data Mining', 'ARADEA', 'Iping Supriana, Kridanto Surendro, Irfan Darmawan', NULL, NULL, NULL, NULL, '18/8/2016', 'valid', '1', NULL, '', '', '', '', '', '', 1),
(24, 'SISTEM INFORMASI', 'TS_1', 'Internasional', 'Measurement The Level Of The Knowledge Management System Implementation On People, Structure, Culture, And Technology Factor Using Analitycal Hierarchy Process (AHP) Method At Telkom University', 'Prosiding Internasional Terindeks Scopus', 'Global Illuminators', 'NIA AMBARSARI', 'YOGI ARIFANTO, AMELIA KURNIAWATI', NULL, NULL, NULL, NULL, '20/4/2015', 'valid', '1', NULL, '', '', '', '', '', '', 1),
(25, 'SISTEM INFORMASI', 'TS_1', 'Lokal', 'MEMBANGUN MOBILE GAME SEBAGAI ASSISTIVE TECHNOLOGY UNTUK MEMBANTU MENGEMBANGKAN SOCIAL INTERACTION SKILL PADA PENDERITA ATTENTION DEFICIT HYPERACTIVITY DISORDER (ADHD) MENGGUNAKAN METODE AGILE DEVELOPMENT', 'Jurnal Nasional Tidak Terakreditasi', 'Jurnal Rekayasa Sistem & Industri', 'RICKY TRI WANDA PUTRA ', 'AMELIA KURNIAWATI, WARIH PUSPITASARI ', NULL, NULL, NULL, NULL, '04-02-2015', 'valid', '1', NULL, '', '', '', '', '', '', 1),
(26, 'SISTEM INFORMASI', 'TS_1', 'Nasional', 'PENERAPAN ERP MODUL WAREHOUSE MANAGEMENT PADA WAROENK LAUNDRY', 'Jurnal Nasional Tidak Terakreditasi', 'JURNAL REKAYASA SISTEM & INDUSTRI (JRSI)', 'R. WAHJOE WITJAKSONO ', 'NIA AMBARSARI, MUHAMAD ARI SADEWO ', NULL, NULL, NULL, NULL, '28/3/2015', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(27, 'SISTEM INFORMASI', 'TS_1', 'Lokal', 'Penerapan ERP Pada Usaha Mikro, Kecil dan Menengah (UMKM) dengan Metode Spiral', 'Jurnal Nasional Tidak Terakreditas', 'academia.edu', 'ZULFIKAR AKBAR ', 'R. WAHJOE WITJAKSONO, NIA AMBARSARI ', NULL, NULL, NULL, NULL, 'PUBLIKASI ILMIAH 2015', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(28, 'SISTEM INFORMASI', 'TS_1', 'Nasional', 'PENGUKURAN eREADINESS CLOUD COMPUTING SERVICE MODEL PADA PERGURUAN TINGGI', 'Prosiding Nasional', 'Konferensi Nasional Sistem Informasi (KNSI) 2015', 'SONI FAJAR SURYA GUMILANG', 'HERU NUGROHO', NULL, NULL, NULL, NULL, '26/2/2015', 'valid', '1', NULL, '', '', '', '', '', '', 1),
(29, 'SISTEM INFORMASI', 'TS_1', 'Internasional', 'Secure Software Engineering for Agile Methodology: Preliminary Investigation', 'Prosiding Internasional Tidak Terindeks Scopus', 'The Second International Conference on Computing Technology and Information Management (ICCTIM2015)', 'LUTHFI RAMADANI ', 'NUR ICHSAN UTAMA', NULL, NULL, NULL, NULL, '21/4/2015', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(30, 'SISTEM INFORMASI', 'TS', 'Internasional', 'Preliminary study for the Implementation of Electrical Capacitance Volume Tomography (ECVT) to Display Fruit Content', 'Jurnal Internasional Tidak Terindeks Scopus', 'Recent Advances on Soft Computing and Data Mining', 'RIZA AGUSTIANSYAH', 'Rohmat Saedudin, Mahfudz Al Huda', NULL, NULL, NULL, NULL, '18/8/2016', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(31, 'SISTEM INFORMASI', 'TS', 'Internasional', 'THE ROLE OF CHARACTERISTICS HUMAN BEHAVIOR IN SOFTWARE DEVELOPMENT TEAM TO IMPROVE SOURCE CODE QUALITY', 'Jurnal Internasional Terindeks Scopus', 'International Journal of Applied Engineering Research (IJAER)', 'TIEN FABRIANTI KUSUMASARI ', NULL, NULL, NULL, NULL, NULL, '09-01-2015', 'valid', '1', NULL, '', '', '', '', '', '', 1),
(32, 'SISTEM INFORMASI', 'TS', 'Nasional', 'Penerapan Metode Analytical Hierarchy Process (AHP) pada Pengembangan Sistem Pengambilan Keputusan Penilaian Kinerja Lembaga/Unit, Studi Kasus: PT PLN Distribusi Jawa Barat dan Banten', 'Prosiding Nasional', 'Prosiding Seminar Nasional Energi Telekomunikasi dan Otomasi (SNETO 2015), ITENAS Bandung', 'Riza Agustiansyah', NULL, NULL, NULL, NULL, NULL, '19/11/2015', 'valid', '1', NULL, '', '', '', '', '', '', 1),
(33, 'SISTEM INFORMASI', 'TS', 'Nasional', 'Penerapan Metode Analytical Hierarchy Process (AHP) Pada Pengembangan Sistem Pengambilan Keputusan Penilaian Behaviour Karyawan, Studi Kasus: PT TELKOM Wilayah Purwokerto', 'Prosiding Nasional', 'Prosiding Seminar Nasional Energi Telekomunikasi dan Otomasi (SNETO 2015), ITENAS Bandung', 'Riza Agustiansyah', NULL, NULL, NULL, NULL, NULL, '19/11/2015', 'valid', '1', NULL, '', '', '', '', '', '', 1),
(34, 'SISTEM INFORMASI', 'TS', 'Nasional', 'Penerapan Metode Walking Weight-Promethee Pada Pengembangan Sistem Pengambilan Keputusan Rekrutmen Karyawan, Studi Kasus: PT Walden Global Services Jawa Barat', 'Prosiding Nasional', 'Prosiding Seminar Nasional Energi Telekomunikasi dan Otomasi (SNETO 2015), ITENAS Bandung', 'Riza Agustiansyah', NULL, NULL, NULL, NULL, NULL, '19/11/2015', 'valid', '1', NULL, '', '', '', '', '', '', 1),
(35, 'SISTEM INFORMASI', 'TS', 'Nasional', 'Penerapan Metode Fuzzy-Analytical Hierarchy Process (AHP) Pada Sistem Informasi Pendukung Keputusan Untuk Menentukan Penjurusan di SMA', 'Prosiding Nasional', 'Seminar Nasional & Konferensi Sistem Informasi, Informatika & Komunikasi (SEMMAU 2015), STIKOM UYELINDO Kupang', 'Riza Agustiansyah', NULL, NULL, NULL, NULL, NULL, '28/11/2015', 'valid', '1', NULL, '', '', '', '', '', '', 1),
(36, 'SISTEM INFORMASI', 'TS_1', 'Lokal', 'Analisis Dan Perancangan Sistem Social E-Learning Untuk Mendukung Program Bandung Smart City ', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Yudha Arif Budiman', 'Irfan Darmawan', NULL, NULL, NULL, NULL, '04-01-2015', 'valid', '1', NULL, '', '', '', '', '', '', 1),
(37, 'SISTEM INFORMASI', 'TS_1', 'Lokal', 'Membangun Sistem Informasi Pengolahan Data Po Box Berbasis Web Menggunakan Metode Waterfall Studi Kasus PT. Pos Indonesia Bandung ', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Andrini Hanariana', 'Riza Agustiansyah', NULL, NULL, NULL, NULL, '04-01-2015', 'valid', '1', NULL, '', '', '', '', '', '', 1),
(38, 'SISTEM INFORMASI', 'TS_1', 'Lokal', 'Analisis Dan Perancangan Enterprise Architecture Untuk Mendukung Fungsi Terkait System Online Payment Point Menggunakan Framework Togaf Admpada PT. Pos Indonesia ', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Anfusa Gandri Herucakra', 'Ari Fajar', NULL, NULL, NULL, NULL, '04-01-2015', 'valid', '1', NULL, '', '', '', '', '', '', 1),
(39, 'SISTEM INFORMASI', 'TS_1', 'Lokal', 'Pengembangan Aplikasi Sistem Pendataan Aset Pada Perusahaan Pengolah Kelapa Sawit Menggunakan Microsoft .Net (Studi Kasus PT. Mns) ', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Yoga Samudro Utomo', 'Irfan Darmawan', NULL, NULL, NULL, NULL, '04-01-2015', 'valid', '1', NULL, '', '', '', '', '', '', 1),
(40, 'SISTEM INFORMASI', 'TS_1', 'Lokal', 'Perancangan Application Management Dengan Menggunakan Framework Asl Cluster Maintenance & Control Studi Kasus Implementasi Aplikasi Pospay Di Pt. Pos Indonesia Bandung ', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'B. Ricardo Saputra', 'Ari Fajar', NULL, NULL, NULL, NULL, '04-01-2015', 'valid', '1', NULL, '', '', '', '', '', '', 1),
(41, 'SISTEM INFORMASI', 'TS_1', 'Lokal', 'Pengembangan Web E-Commerce Bojana Sari Menggunakan Metode Prototype ', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Afghan Amar Pradipta', 'Yuli Adam Prasetyo', NULL, NULL, NULL, NULL, '04-01-2015', 'valid', '1', NULL, '', '', '', '', '', '', 1),
(42, 'SISTEM INFORMASI', 'TS_1', 'Lokal', 'Pengembangan Aplikasi E-Crm Bojana Sari Menggunakan Metode Prototype ', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Mochamad Akif', 'Yuli Adam Prasetyo', NULL, NULL, NULL, NULL, '04-01-2015', 'valid', '1', NULL, '', '', '', '', '', '', 1),
(43, 'SISTEM INFORMASI', 'TS_1', 'Lokal', 'Portal Alih Daya Pekerjaan Pembuatan Software Di Indonesia Menggunakan Metode Iterative Incremental ', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'M. Thofhan Hannanto', 'Yuli Adam Prasetyo', NULL, NULL, NULL, NULL, '04-01-2015', 'valid', '1', NULL, '', '', '', '', '', '', 1),
(44, 'SISTEM INFORMASI', 'TS_1', 'Lokal', 'Membangun Aplikasi Mobile Crowdsourcing Event Berbasis Android Menggunakan Metode Iterative & Incremental ', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Annisa Nurul Fitria F', 'Yuli Adam Prasetyo', NULL, NULL, NULL, NULL, '04-01-2015', 'valid', '1', NULL, '', '', '', '', '', '', 1),
(45, 'SISTEM INFORMASI', 'TS_1', 'Lokal', 'Perancangan Order Management System Berbasis Web Application Pada Umkm Dengan Menggunakan Metode Waterfall ', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Yafshil Adipura', 'R. Wahyu Wicaksono', NULL, NULL, NULL, NULL, '04-01-2015', 'valid', '1', NULL, '', '', '', '', '', '', 1),
(46, 'SISTEM INFORMASI', 'TS_1', 'Lokal', 'Perancangan Aplikasi Penjualan Berbasis Web Dengan Metode Prototyping Pada Cv Khatulistiwa ', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Izharyan Iqbal', 'R. Wahyu Wicaksono', NULL, NULL, NULL, NULL, '04-01-2015', 'valid', '1', NULL, '', '', '', '', '', '', 1),
(47, 'SISTEM INFORMASI', 'TS_1', 'Lokal', 'Membangun Sistem Informasi Pengelolaan Surat Menggunakan Metode Waterfall Studi Kasus Direktorat Perencanaan Dan Pengembangan Pendanaan Pembangunan ', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Kholifah Gina N', 'Riza Agustiansyah', NULL, NULL, NULL, NULL, '04-01-2015', 'valid', '1', NULL, '', '', '', '', '', '', 1),
(48, 'SISTEM INFORMASI', 'TS_1', 'Lokal', 'Perancangan Strategi & Prosedur Continuity Plan Pada Layanan Akademik, Keuangan Dan Kepegawaian (Studi Kasus: Universitas Telkom) ', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Chrishya Buti Pama', 'Basuki Rahmad', NULL, NULL, NULL, NULL, '04-01-2015', 'valid', '1', NULL, '', '', '', '', '', '', 1),
(49, 'SISTEM INFORMASI', 'TS_1', 'Lokal', 'Analisis Dan Perancangan Arsitektur Aplikasi Berbasis Service Oriented Architecture Pada Badan Pelayanan Perizinan Terpadu Kota Bandung (Bidang Ii) Menggunakan Metode Enterprise Architecture Planning Dan Model Driven Architecture ', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Husnur Ridha Syafni', 'Taufik Nur Adi', NULL, NULL, NULL, NULL, '04-01-2015', 'valid', '1', NULL, '', '', '', '', '', '', 1),
(50, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Development Of Web Application Based On Crowdsourcing For Car Rental Managementby Using Scrum Methodology (rental Side)', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Faqih Putra Kusumawijaya', 'Soni Fajar Surya, Taufik Nur Adi', NULL, NULL, NULL, NULL, '08-02-2015', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(51, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Perancangan Application Architecture Untuk Fungsi akademik Pada Institut Pemerintahan Dalam Negeri (ipdn) Menggunakan Framework Togaf Adm Studi Kasussistem Informasi Akademik (siakad)', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Satria Janaka', 'Murahartawaty, Ridha Hanafi', NULL, NULL, NULL, NULL, '08-02-2015', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(52, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Penerapan Sistem Sales Management Dan Warehouse Management Pada Umkm Konveksi Rajutan Berbasis Openerp Dengan Metode Sure Step', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Pradito Setiadi', 'Irfan Darmawan, R. Wahjoe Witjaksono', NULL, NULL, NULL, NULL, '08-02-2015', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(53, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Membangun Aplikasi Mobile Berbasis Android Untuk Informasi Perizinan Angkutan Umum Dengan Metode Extreme Programming (studi Kasus Dinas Perhubungan Kabupaten Bandung Barat)', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Gregorius Prahaswara Dewanta', 'Nia Ambarsari', NULL, NULL, NULL, NULL, '08-02-2015', 'valid', '1', NULL, '', '', '', '', '', '', 1),
(54, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Perancangan Tata Kelola Teknologi Informasi Di Pt.inti (industri Telekomunikasi Indonesia) Menggunakan Framework Cobit 5 Pada Domain Build, Acquire And Implement (BAI)', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Komang Indah Desinthya Wati', 'Murahartawaty, Soni Fajar S. Gumilang', NULL, NULL, NULL, NULL, '08-02-2015', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(55, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Analisis Dan Perancangan Information System Architecture Dengan Framework Togaf Admstudi Kasus Payment Point Online Bank PT. Finnet Indonesia', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Muhammad Fikri', 'Ari Fajar Santoso, Ridha Hanafi', NULL, NULL, NULL, NULL, '08-02-2015', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(56, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Perancangan Technology Architectureuntuk Fungsi Akademik Pada Institut Pemerintahan Dalam Negeri (ipdn) Menggunakan Framework Togaf Adm Studi Kasus Sistem Informasi Akademik (siakad)', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Fadly Muhammad', 'Murahartawaty, Ridha Hanafi', NULL, NULL, NULL, NULL, '08-02-2015', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(57, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Pengembangan Awal Knowledge Management System Modul Knowledge Repository Dengan Metode Iterative Incremental Di Pdii-lipi', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Fajri Gustia Nanda', 'Nia Ambarsari', NULL, NULL, NULL, NULL, '08-02-2015', 'valid', '1', NULL, '', '', '', '', '', '', 1),
(58, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Membangun Aplikasi Web Beasiswa Berbasis Crowdfunding Pada Modul Pemohon Beasiswa Dan Reviewer Menggunakan Metode Iterative Incremental', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Muhammad Akbar Satria', 'Nia Ambarsari, Taufik Nur Adi', NULL, NULL, NULL, NULL, '08-02-2015', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(59, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Perancangan Service Operation Pada Layanan It Pusair Dengan Menggunakan Framework Itil Versi 3', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Fransiska', 'Murahartawaty, Ade Karma', NULL, NULL, NULL, NULL, '08-02-2015', 'valid', '1', NULL, '', '', '', '', '', '', 1),
(60, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Analisis Dan Perancangan Technology Architecture Dengan Framework Togaf Adm Studi Kasus Sistem Payment Point Online Bank PT. Finnet Indonesia', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Firdaus Setya Pratama', 'Ari Fajar Santoso, Ridha Hanafi', NULL, NULL, NULL, NULL, '08-02-2015', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(61, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Desain Dan Analisis Green Data Center Di Fakultas Rekayasa Industri Universitas Telkom Menggunakan Standar Tia-942 Heat Dissipation', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Sufyan Sauri', 'Mochamad Teguh Kurniawan', NULL, NULL, NULL, NULL, '08-02-2015', 'valid', '1', NULL, '', '', '', '', '', '', 1),
(62, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Pengembangan Modul Ukm Web Portal crowdfunding Dengan Metode Prototype Dan framework Codeigniter', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Harpandi Wibowo', 'Yuli Adam Prasetyo', NULL, NULL, NULL, NULL, '08-02-2015', 'valid', '1', NULL, '', '', '', '', '', '', 1),
(63, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Perancangan Enterprise Architecture E-commerce Pada Bagian Manajemen Hubungan Pelanggan di PT. XYZ Menggunakan framework Togaf ADM', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Ajeng Citra Rizkyanur', 'Irfan Darmawan, Basuki Rahmad', NULL, NULL, NULL, NULL, '08-02-2015', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(64, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Perancangan Service Design Pada Layanan IT Pusair Dengan Menggunakan Framework Itil Versi 3', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Diana Meiriana Selvianti', 'Murahartawaty, Wildan Herwindo', NULL, NULL, NULL, NULL, '08-02-2015', 'valid', '1', NULL, '', '', '', '', '', '', 1),
(65, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Membangun Web E-preparation SMB Universitas Telkom Modultry Out Online Dengan Menggunakan Metode Iterativeincremental', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Hosiana Ariska Silalahi', 'Nia Ambarsari, Taufik Nur Adi', NULL, NULL, NULL, NULL, '08-02-2015', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(66, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Membangun Web Crowdsourcing E-preparation Smb Universitas Telkom Modul Pemantapan Dengan Menggunakan Metode Iterative Incremental', NULL, 'e-Proceeding of Engineering ISSN : 2355-9366', 'Dhani Rahutami Purwastuti', 'Nia Ambarsari, Taufik Nur Adi', NULL, NULL, NULL, NULL, '08-02-2015', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(67, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Perancangan Knowledge Management System Untuk Mengelola Knowledge Sharing Pada Seleksi Mahasiswa Baru Di Universitas Telkom Dengan Metode Iterative Incremental', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Sofia Faridah', 'Luciana Andrawina, Faisal M.alshary', NULL, NULL, NULL, NULL, '08-02-2015', 'valid', '1', NULL, '', '', '', '', '', '', 1),
(68, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Penerapan Sistem Purchase Management Dan Warehouse Management Pada Umkm Konveksi Rajutan Berbasis Openerp Dengan Metode Surestep', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Mohamad Iqbal', 'Irfan Darmawan, Wahyu Wicaksono', NULL, NULL, NULL, NULL, '08-02-2015', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(69, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Membangun Website Manajemen Investasi Berbasis Crowdfunding Modul Monitoring Dana Investasi Menggunakan Metode Iterative Dan Incrementaldedisupriadi', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Fathimah Muthi Luthfiyah', 'Nia Ambarsari', NULL, NULL, NULL, NULL, '08-02-2015', 'valid', '1', NULL, '', '', '', '', '', '', 1),
(70, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Perancangan Tata Kelola Teknologi Informasi Pt. Industri Telekomunikasi Indonesia (inti) Menggunakan Framework Cobit 5 Pada Domain Deliver, Service, And Support', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Ida Bagus Krisna Wedanta Prasada', 'Murahartawaty, Soni Fajar S. Gumilang', NULL, NULL, NULL, NULL, '08-02-2015', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(71, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Perancangan Arsitektur Teknologi Pada Pt. Telehouse Engineering Menggunakan Framework Togaf Adm', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Galih Fathoni', 'Murahartawati, Dk Diandana Raija', NULL, NULL, NULL, NULL, '08-02-2015', 'valid', '1', NULL, '', '', '', '', '', '', 1),
(72, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Assessment Dan Perancangan Itsm Domain Service Design Berdasarkan Itil Versi 2011, Iso 20.000 Series, Dan Iso 15.504 Series Untuk Meningkatkan Capability Level Dengan Pemanfaatan Tools Remedy (study Kasus: Pt. Telkom Indonesia Tbk)', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Viky Hermana Pratama', 'Murahartawaty, Eko Kusbang Umar', NULL, NULL, NULL, NULL, '08-02-2015', 'valid', '1', NULL, '', '', '', '', '', '', 1),
(73, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Pengembangan Aplikasi Social Commerce Berbasis Gis Di Android Dengan Metode Iterative Dan Incremental', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'I Made Prawira Indrawan', 'Soni Fajar Surya Gumilang , Muhammad Azani Hasibuan', NULL, NULL, NULL, NULL, '08-02-2015', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(74, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Perancangan Enterprise Architecture E-commerce Pada Bagian Payment Di Pt Xyz Menggunakan Framework Togaf Adm', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Rini Setyaningsih', 'Irfan Darmawan, Basuki Rahmad', NULL, NULL, NULL, NULL, '08-02-2015', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(75, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Pembangunan Portal Web Crowdsourcing Event Perguruan Tinggi Menggunakan Metode Iterative Incremental (modul Peserta Event)', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Alifia Indra Damarani', 'Irfan Darmawan, Taufik Nur Adi', NULL, NULL, NULL, NULL, '08-02-2015', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(76, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Membangun Web Service Pada Sistem Informasi Perizinan Angkutan Umum Dengan Metode Extreme Programming (studi Kasus Dinas Perhubungan Kabupaten Bandung Barat)', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Fathiyyah Nur Azizah', 'Nia Ambarsari', NULL, NULL, NULL, NULL, '08-02-2015', 'valid', '1', NULL, '', '', '', '', '', '', 1),
(77, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Perancangan Arsitektur Bisnis Pada Pt. Telehouse Engineering Menggunakan Framework Togaf Adm', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Aditya Pradana Putra', 'Murahartawati, Umar Yunan', NULL, NULL, NULL, NULL, '08-02-2015', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(78, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Penerapan Sistem Sales Management Menggunakan Openerp Pada Pt.kharisma Buana Jaya Dengan Metode Spiral', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Annisa Utami', 'Nia Ambarsari, R. Wahyu Wicaksono', NULL, NULL, NULL, NULL, '08-02-2015', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(79, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Analisis Dan Perancangan Business Architecture Menggunakan The Open Group Architecture Framework Architecture Development Method (togaf Adm) Pada Pt. Shafco Multi Trading', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Famila Faradiba', 'Murahartawaty, Ridha Hanafi', NULL, NULL, NULL, NULL, '08-02-2015', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(80, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Assessment Dan Perancangan Itsm Domain Service Operation Berdasarkan Itil Versi 2011, Iso 20000, Dan Iso 15504 Untuk Meningkatkan Capability Level Dengan Pemanfaatan Tools Remedy', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Reza Aldiansyah', 'Murahartawaty, Eko Kusbang Umar', NULL, NULL, NULL, NULL, '08-02-2015', 'valid', '1', NULL, '', '', '', '', '', '', 1),
(81, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Penerapan Sistem Purchase Management Menggunakan Openerpdengan Metode Rapid Application Development(studi Kasus : Pt Genta Trikarya)', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Muhammad Syaiful Ramadhan', 'Ari Yanuar Ridwan, R.wahjoe Witjaksono', NULL, NULL, NULL, NULL, '08-02-2015', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(82, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Membangun Game Mobile Sebagai Assistive Technology Untuk Membantu Meningkatkan Fokus Pada Anak Penderita Attention Deficit Hyperactivity Disorder (adhd) Dengan Metode Agile Development', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Pargaulan Siagian', 'Nia Ambarsari, Warih Puspitasari', NULL, NULL, NULL, NULL, '08-02-2015', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(83, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Perancangan Dan Pembangunan Knowledge Management Systemmodul Penelitian Menggunakan Framework Codeigniter Dengan Metode Iterative Incremental', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Liffi Noferianti', 'Luciana Andrawina, Ahmad Musnansyah', NULL, NULL, NULL, NULL, '08-02-2015', 'valid', '1', NULL, '', '', '', '', '', '', 1),
(84, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Penerapan Sistem Akutansi Menggunakan Openerp Pada Pt. Kharisma Buana Jaya Dengan Metode Spiral', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Ridho Arya Duta Mahendra', 'Nia Ambarsari, R. Wadjoe Witjaksono', NULL, NULL, NULL, NULL, '08-02-2015', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(85, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Analisis Dan Perancangan Technology Architecture Menggunakan The Open Group Architecture Framework Architecture Development Method (togaf Adm) Pada Pt. Shafco Multi Trading', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Renantia Indriani', 'Murahartawaty, Ridha Hanafi', NULL, NULL, NULL, NULL, '08-02-2015', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(86, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Penilaian Terhadap Penerapan Proses It Governance Menggunakan Cobit Versi 5 Pada Domain Bai Untuk Pengembangan Aplikasi Studi Kasus Ipos Di Pt. Pos Indonesia', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Josua Kristian Sitinjak', 'Ari Fajar, Ridha Hanaf', NULL, NULL, NULL, NULL, '08-02-2015', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(87, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Analisis Dan Perancangan Itsm Domain Service Design Pada Layanan Akademik Institut Pemerintahan Dalam Negeri (ipdn) Dengan Menggunakan Framework Itil Versi 3', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Thessa Silviana Manurung', 'Murahartawaty, Ridha Hanafi', NULL, NULL, NULL, NULL, '08-02-2015', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(88, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Perancangan Service Design Pada Layanan Angkutan Barang Pt. Kereta Api Indonesia (persero) Menggunakan Framework Itil Versi 3', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Mifta Aziz', 'Murahartawaty, Eko Kusbang Umar', NULL, NULL, NULL, NULL, '08-02-2015', 'valid', '1', NULL, '', '', '', '', '', '', 1),
(89, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Design And Development Of Programming Learning Platform Based On Heuristic Approach In Assessment Module With Iterative And Incremental Method (case Study: Information System Telkom University)', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Dewansyah Adi Saputra', 'Soni Fajar Surya Gumilang', NULL, NULL, NULL, NULL, '08-02-2015', 'valid', '1', NULL, '', '', '', '', '', '', 1),
(90, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Perancangan Dan Analisis Enterprise Architecture Layanan Kesehatan (yakes) Telkom Pada Domain Arsitektur Bisnis Dengan Menggunakan Framework Togaf Adm', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Ratih Cintya Lestari', 'Mochamad Teguh Kurniawan, Rahmat Mulyana', NULL, NULL, NULL, NULL, '08-02-2015', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(91, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Membangun Aplikasi E-commerce Jasa Fotografi Pre Wedding Berbasis Web Crowdsourcing Modul Calon Pengantin Dan Foto Sesi Menggunakan Metode Iterative Incremental', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Insan Harish', 'Nia Ambarsari, Taufik Nur Adi', NULL, NULL, NULL, NULL, '08-02-2015', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(92, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Design And Development Of Programminglearning Platform Based On Heuristicapproach In Gamification Module Withiterative And Incremental Method(case Study : Information System Major Telkomuniversity)', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Adelia Chitra Sazkia', 'Soni Fajar Surya Gemilang, Muhammad Azani Hasibuan', NULL, NULL, NULL, NULL, '08-02-2015', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(93, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Perancangan Data Architecture Untuk Fungsi Akademik Pada Institut Pemerintahan Dalam Negeri (ipdn) Menggunakan Framework Togaf Adm Studi Kasus Sistem Informasi Akademik (siakad)', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Andika Desta Ginanjar', 'Murahartawaty, Ridha Hanafi', NULL, NULL, NULL, NULL, '08-02-2015', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(94, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Perancangan Service Operation Pada Layanan Angkutan Penumpang Pt. Kereta Api Indonesia (persero) Menggunakan Framework Itil Versi 3', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Rexy Septian Arafat', 'Murahartawaty, Eko Kusbang Umar', NULL, NULL, NULL, NULL, '08-02-2015', 'valid', '1', NULL, '', '', '', '', '', '', 1),
(95, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Perancangan Private Cloud Computing Dengan Pendekatan Private Cloud Computing Software As A Service Pada Fakultas Rekayasa Industri Universitas Telkom Menggunakan Metode Rapi Application Development', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Decky Raditama Megantara', 'Mochamad Teguh Kurniawan, Adityas Widjajarto', NULL, NULL, NULL, NULL, '08-02-2015', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(96, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Membangun Sistem Informasi Perizinan Angkutan Umum Dengan Metode Extreme Programming (studi Kasus Dinas Perhubungan Kabupaten Bandung Barat)', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Olaf Arman Sebastian Sihombing', 'Nia Ambarsari', NULL, NULL, NULL, NULL, '08-02-2015', 'valid', '1', NULL, '', '', '', '', '', '', 1),
(97, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Perancangan Tata Kelola Teknologi Informasi Di Pt.inti (industri Telekomunikasi Indonesia) Menggunakan Framework Cobit 5 Pada Domain Edm Dan Mea', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Billa Ananda Suwandi', 'Murahartawaty, Soni Fajar S. Gumilang', NULL, NULL, NULL, NULL, '08-02-2015', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(98, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Pengembangan Modul Freemium Aplikasi Tel-us (telkom University Store) Menggunakan Metode Iterative Incremental Dan Framework Laravel', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Yusuf Rahmadi', 'Yuli Adam Prasetyo, Muhammad Azani Hasibuan', NULL, NULL, NULL, NULL, '08-02-2015', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(99, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Pembangunan Aplikasi Web Distribusi Kelompok Tani Katata Dengan Menggunakan Metode Extreme Programming', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Arnie Nur Ramadhani', 'Yuli Adam P., Taufik Nur Adi', NULL, NULL, NULL, NULL, '08-02-2015', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(100, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Perancangan Business Architecture Untukfungsi Akademik Pada Institutpemerintahan Dalam Negeri (ipdn)menggunakan Framework Togaf Adm Studikasus Sistem Informasi Akademik (siakad)', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Rahayu Manolita', 'Murahartawaty, Ridha Hanafi', NULL, NULL, NULL, NULL, '08-02-2015', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(101, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Penerapan Sistem Manufacturing Pada Umkm Konveksi Rajutan Berbasis Openerp Dengan Metode Sure Step', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Irena Arsyka Dewi', 'Irfan Darmawan, Wahjoe Witjaksono', NULL, NULL, NULL, NULL, '08-02-2015', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(102, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Assessment Dan Perancangan Itsm Domain Service Transition Berdasarkan Itil V.2011, Iso 20000 Series Dan Iso 15504 Series Untuk Meningkatkan Capability Level Dengan Pemanfaatan Tools Remedy (studi Kasus : Pt Telkom Indonesia Tbk)', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Arrazaq Zakaria M.s', 'Murahartawaty', NULL, NULL, NULL, NULL, '08-02-2015', 'valid', '1', NULL, '', '', '', '', '', '', 1),
(103, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Penerapan Sistem Purchase Management Menggunakan Openerp Pada Pt. Kharisma Buana Jaya Dengan Metode Spiral', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Puspita Ayu Kartika', 'Nia Ambarsari, R. Wahyu Wicaksono', NULL, NULL, NULL, NULL, '08-02-2015', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(104, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Pembangunan Aplikasi Web E-commerce Kelompok Tani Katata Dengan Metode Itterative And Incremental', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Maulana Assidqi', 'Yuli Adam P., Taufik Nur Adi', NULL, NULL, NULL, NULL, '08-02-2015', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(105, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Penerapan Sistem Manufacturing Berbasis Openerp Dengan Metode Rapid Application Development(studi Kasus : Pt Genta Trikarya)', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Fajrul Alfian', 'Ari Yanuar Ridwan, R.wahjoe Witjaksono,', NULL, NULL, NULL, NULL, '08-02-2015', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(106, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Audit Keberlangsungan Layanan Pada Perusahaan Jasa Pengiriman Berbasis Cobit 5 Dan National Institute Of Standards And Technology (nist)', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Zyas Esa Anarki', 'Mochamad Teguh Kurniawan', NULL, NULL, NULL, NULL, '08-02-2015', 'valid', '1', NULL, '', '', '', '', '', '', 1),
(107, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Design And Development Of Programming Learning Platform Based On Heuristic Approach In Course Management Module With Iterative And Incremental Method (case Study : Information System Telkom University)', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Alima Indriati', 'Soni Fajar Surya Gumilang', NULL, NULL, NULL, NULL, '08-02-2015', 'valid', '1', NULL, '', '', '', '', '', '', 1),
(108, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Perancangan Enterprise Architecture E-commerce Pada Bagian E-marketplace Di Pt Xyz Menggunakan Framework Togaf Adm', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Sri Haryani Br. Manjuntak', 'Irfan Darmawan, Basuki Rahmad', NULL, NULL, NULL, NULL, '08-02-2015', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(109, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Perancangan Service Transition Pada Layanan It Pusair Dengan Menggunakan Framework Itil Versi 3', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Vera Ananda', 'Murahartawaty, Ade Karma', NULL, NULL, NULL, NULL, '08-02-2015', 'valid', '1', NULL, '', '', '', '', '', '', 1),
(110, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Penerapan Sistem Absensi Berbasis Openerp Pada Cv.huda Jaya Dengan Metode Rapid Application Development', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Shinta Sindi Nuryani', 'Soni Fajar S. Gumilang, R. Wahjoe Witjaksono', NULL, NULL, NULL, NULL, '08-02-2015', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(111, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Perancangan Knowledge Management System Untuk Mengelola Data Seleksi Mahasiswa Baru Di Universitas Telkom Dengan Metode Iterative Incremental', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Inez Sekarayu Nawangwulan', 'Luciana Andrawina, Faishal Mufied Al-anshary', NULL, NULL, NULL, NULL, '08-02-2015', 'valid', '1', NULL, '', '', '', '', '', '', 1),
(112, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Desain Dan Analisis Best Practice Physical Security Dan Logical Security Pada Data Center Fakultas Rekayasa Industri Universitas Telkom Menggunakan Standar Tia-942 Dan Open Enterprise Security Architecture', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'I Gede Iswara Darmawan', 'Mochamad Teguh Kurniawan', NULL, NULL, NULL, NULL, '08-02-2015', 'valid', '1', NULL, '', '', '', '', '', '', 1),
(113, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Development Of Wedding Planner Module Siapsiapnikah.com Using Extreme Programming Method', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Eva Novianti', 'Yuli Adam', NULL, NULL, NULL, NULL, '08-02-2015', 'valid', '1', NULL, '', '', '', '', '', '', 1),
(114, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Penerapan Sistem Warehouse Management Menggunakan Openerp Pada Pt. Kharisma Buana Jaya Dengan Metode Spiral', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Muhammad Aulia Rendy', 'Nia Ambarsari, R.wahjoe Witjaksono', NULL, NULL, NULL, NULL, '08-02-2015', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(115, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Audit Security Services Pada Pt. Xyz', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Aulia Primadani', 'Basuki Rahmad, M.teguh Kurniawan', NULL, NULL, NULL, NULL, '08-02-2015', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(116, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Penerapan Sistem Payroll Berbasis Openerp Pada Cv. Huda Jaya Dengan Metode Rapid Application Development', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Fadhil Muhammad', 'Soni Fajar S. Gumilang, R. Wahjoe Witjaksono', NULL, NULL, NULL, NULL, '08-02-2015', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(117, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Perancangan It Master Plan Menggunakan Metode Ward And Peppard Pada Pt. Telehouse Engineering', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'I Made Adi Jayantika', 'Murahartawaty, Dk Diadnyana Raija', NULL, NULL, NULL, NULL, '08-02-2015', 'valid', '1', NULL, '', '', '', '', '', '', 1),
(118, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Membangun Portal Jual Beli Universitas Telkomdengan Metode Prototyping', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'M Rizki Mozar Kadean', 'Nia Ambarsari, Umar Yunan', NULL, NULL, NULL, NULL, '08-02-2015', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(119, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Membangun Website Manajemen Investasi Berbasis Crowdfunding Modul Investor Menggunakan Metode Iterative Dan Incremental', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Made Febriyana Dyastama Putra', 'Nia Ambarsari', NULL, NULL, NULL, NULL, '08-02-2015', 'valid', '1', NULL, '', '', '', '', '', '', 1),
(120, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Perancangan Enterprise Architecture E-commerce Pada Bagian Manajemen Produk Dan Pemasok Di Pt. Xyz Menggunakan Framework Togaf Adm', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Putri Myke Wahyuni', 'Irfan Darmawan, Basuki Rahmad', NULL, NULL, NULL, NULL, '08-02-2015', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(121, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Development Of Auction Module Siapsiapnikah.com Using Extreme Programming Method And Crowdsourcing Concept', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Fitasari Wiharni', 'Yuli Adam Prasetyo, Taufik Nur Adi', NULL, NULL, NULL, NULL, '08-02-2015', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(122, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Perancangan Arsitektur Data Dan Aplikasi Pada Pt. Telehouse Engineering Menggunakan Framework Togaf Adm', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Ashof Yudhistira M.p.', 'Murahartawati, Umar Yunan', NULL, NULL, NULL, NULL, '08-02-2015', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(123, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Perancangan Service Design Pada Layanan Angkutan Penumpang Pt. Kereta Api Indonesia (persero) Menggunakan Framework Itil Versi 3', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Sabrina Andiyani', 'Murahartawaty, Eko Kusbang Umar', NULL, NULL, NULL, NULL, '08-02-2015', 'valid', '1', NULL, '', '', '', '', '', '', 1),
(124, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Pengembangan Application Programming Interfaces Aplikasi Geo Social Commerce Dengan Metode Iterative Dan Incremental', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Ainu Faisal Pambudy', 'Soni Fajar Surya Gumilang, Muhammad Azani Hasibuan', NULL, NULL, NULL, NULL, '08-02-2015', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(125, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Perancangan Enterprise Architecture E-commerce Pada Bagian Shipping And Warehouse Di Pt. Xyz Menggunakan Framework Togaf Adm', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Chintamy Christini', 'Liane Okdinawati, Basuki Rahmad', NULL, NULL, NULL, NULL, '08-02-2015', 'valid', '1', NULL, '', '', '', '', '', '', 1),
(126, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Pembangunan Portal Web Crowdsourcing Event Perguruan Tinggi Menggunakan Metode Iterative Incremental (modul Penyelenggara Event)', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Mia Meilani', 'Irfan Darmawan, Taufik Nur Adi', NULL, NULL, NULL, NULL, '08-02-2015', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(127, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Design And Development Of Investment Management Website Based On Crowdfunding In Small And Medium-sized Enterprises Using Iterative And Incremental Method', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Fauza Annisa', 'Nia Ambarsari, Taufik Nur Adi', NULL, NULL, NULL, NULL, '08-02-2015', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(128, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Membangun Web Crowdsourcing E-preparation SMB Universitas Telkom Modul E-learning Dengan Menggunakan Metode Iterative Incremental', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Joses Adelwin Sitepu', 'Nia Ambasari, Taufik Nur Adi', NULL, NULL, NULL, NULL, '08-02-2015', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(129, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Penerapan Erp Modul Logistics Menggunakan Openerp Dengan Metode Sure Step(studi Kasus: Pt. Xyz Bidang Retail Fashion)', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Ikhsan Yudha Pradana', 'Deden Witarsyah, R. Wahjoe Witjaksono', NULL, NULL, NULL, NULL, '08-02-2015', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(130, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Analisis Dan Perancangan Data Architecture Dan Application Architecture Menggunakan The Open Group Architecture Framework Architecture Development Method (togaf Adm) Pada Pt. Shafco Multi Trading', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'I Gede Mindrayasa', 'Murahartawaty, Ridha Hanafi', NULL, NULL, NULL, NULL, '08-02-2015', 'valid', '2', NULL, '', '', '', '', '', '', 1);
INSERT INTO `tb_publikasi` (`id`, `prodi`, `no_ts`, `skl_publikasi`, `judul`, `jns_publikasi`, `nm_seminar`, `penulis_utm`, `penulis_tmbh`, `penulis_tmbh2`, `penulis_tmbh3`, `penulis_tmbh4`, `penulis_tmbh5`, `tanggal`, `ket`, `jlm_dosen_si`, `sts_valid`, `file_names`, `file_types`, `file_dates`, `file_name`, `file_type`, `file_date`, `id_user`) VALUES
(131, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Membangun Web Berbasis Crowdsourcing Untuk Penjualan Sampah Plastik Dengan Metode Iterative Incremental Menggunakan Framework Codeigniter', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Martha Okrina', 'Nia Ambarsari, Taufik Nur Adi', NULL, NULL, NULL, NULL, '08-02-2015', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(132, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Analisis Dan Perancangan Itsm Domain Service Transition Pada Layanan Akademik Institut Pemerintahan Dalam Negeri (ipdn) Dengan Menggunakan Framework Itil Versi 3', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Charlie Sugiarto', 'Murahartawaty, Ridha Hanafi', NULL, NULL, NULL, NULL, '08-02-2015', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(133, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Perancangan Sistem Pengadaan (procurement) Berbasis Openerp Dengan Metode Soft System Methdology', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Putu Gede Wibawa Hartawan', 'Ary Yanuar Ridwan, R. Wahjoe Witjaksono', NULL, NULL, NULL, NULL, '08-02-2015', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(134, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Perancangan Dan Pembangunan Knowledge Management System Pada Modul Pengabdian Masyarakat Dan Penunjang Menggunakan Framework Codeigniter Dengan Metode Iterative Incremental', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Nevisia Puspa Ayudhana', 'Luciana Andrawina, Ahmad Musnansyah', NULL, NULL, NULL, NULL, '08-02-2015', 'valid', '1', NULL, '', '', '', '', '', '', 1),
(135, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Perancangan Dan Analisis Enterprise Architecture Yayasan Kesehatan (yakes) Telkom Pada Domain Arsitektur Teknologi Dengan Menggunakan Framework Togaf Adm', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Irma Angraeini', 'Mochamad Teguh Kurniawan, Rahmat Mulyana', NULL, NULL, NULL, NULL, '08-02-2015', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(136, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Membangun Aplikasi E-commerce Jasa Fotografi Pre Wedding Berbasis Web Crowdsourcing Modul Fotografer Menggunakan Metode Iterative Incremental', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Mahdy Arief', 'Nia Ambarsari, Taufik Nur Adi', NULL, NULL, NULL, NULL, '08-02-2015', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(137, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Design And Development Of Programming Learning Platform Based On Heuristic Approach In Live Code Module With Iterative And Incremental Method(case Study: Information System Telkom University)', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Ekky Novriza Alam', 'Soni Fajar Surya Gemilang, Muhammad Azani Hasibuan', NULL, NULL, NULL, NULL, '08-02-2015', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(138, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Analisis Dan Perancangan Itsm Domain Service Operation Pada Layanan Akademik Institut Pemerintahan Dalam Negeri (ipdn) Dengan Menggunakan Framework Itil Versi 3', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Aridha Meitya Arifin', 'Murahartawaty, Ridha Hanafi', NULL, NULL, NULL, NULL, '08-02-2015', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(139, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Perancangan Knowledge Management System Untuk Seleksi Mahasiswa Baru Di Universitas Telkom Menggunakan Aplikasi Mobile Berbasis Android Dengan Metode Iterative Incremental', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Putu Puspitha Saraswati', 'Luciana Andrawina, Faisal M. Al-anshary', NULL, NULL, NULL, NULL, '08-02-2015', 'valid', '1', NULL, '', '', '', '', '', '', 1),
(140, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Pengembangan Modul Investor Web Portal Crowdfunding Dengan Metode Protoype Dan Framework Codeigniter', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Aris Setyo Nugroho', 'Yuli Adam Prasetyo', NULL, NULL, NULL, NULL, '08-02-2015', 'valid', '1', NULL, '', '', '', '', '', '', 1),
(141, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Perancangan Tata Kelola Teknologi Informasidi Pt. Industri Telekomunikasi Indonesia (inti)menggunakan Framework Cobit 5 Pada Domainalign, Plan, And Organize (apo)', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'I Ketut Adi Putra Prananta', 'Murahartawaty, Soni Fajar S. Gumilang', NULL, NULL, NULL, NULL, '08-02-2015', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(142, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Pengembangan Modul Berbayar Aplikasi Tel_us (telkom University Store) Menggunakan Metodologi Iterative Incremental Dan Framework Laravel', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Ardhyan Zulfikar Malik', 'Yuli Adam Prasetyo, Adityas Widjajarto', NULL, NULL, NULL, NULL, '08-02-2015', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(143, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Perancangan Dan Analisis Desain Jaringan Wire Dan Wireless Dengan Pendekatan Green Network Di Gedung Karang Fakultas Rekayasa Industr Iuniversitas Telkom', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Aries Putra Pratama', 'Murahartawaty, M. Teguh Kurniawan', NULL, NULL, NULL, NULL, '12-03-2015', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(144, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Pengembangan Aplikasi E-office Disposisi Dokumen Pada Badan Kepegawaian Daerah Provinsi Jawa Barat Dengan Metode Rad', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Yudha Aditya Ramadhana', 'Ari Yanuar, Rachmadita A', NULL, NULL, NULL, NULL, '12-03-2015', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(145, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Perancangan Dan Analisis Enterprise Architecture Yayasan Kesehatan (yakes) Tekom Pada Domain Arsitektur Sistem Informasi Dengan Menggunakan Framework Togaf Adm', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Meirizky Anjani Purwati Ningsih', 'M. Teguh Kurniawan, Rahmat Mulyana', NULL, NULL, NULL, NULL, '12-03-2015', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(146, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Penilaian Dan Perancangan Tata Kelola Manajemen Layanan Teknologi Informasi Berdasarkan Iso 20000,iso 15504, Dan Itil V3 Pada Direktorat Sistem Informasi Telkom University', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Fajri Arfan', 'M. Teguh Kurniawan, Eko Kusbang Umar', NULL, NULL, NULL, NULL, '12-03-2015', 'valid', '1', NULL, '', '', '', '', '', '', 1),
(147, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Pengembangan Aplikasi E-office Pencatatan Dokumen Pada Badan Kepegawaian Daerah Provinsi Jawa Barat Dengan Menggunakan Metode Rad', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Regina Ayu Prameswari Wade', 'Ary Yanuar Ridwan, Rachmadita Andreswari', NULL, NULL, NULL, NULL, '12-03-2015', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(148, 'SISTEM INFORMASI', 'TS', 'Internasional', 'A Maintenance Task Optimization of the BTS using RCM and LCC Methods', 'Jurnal Internasional Terindeks Scopus', 'Internetworking Indonesia Journal (IIJ)', 'RD. ROHMAT SAEDUDIN ', NULL, NULL, NULL, NULL, NULL, '09-07-2015', 'valid', '1', NULL, '', '', '', '', '', '', 1),
(149, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Desain Topologi Jaringan Kabel Nirkabel PDIILIPI dengan Cisco Three-Layered Hierarchical menggunakan NDLC', 'Jurnal Nasional TIDAK Terakreditasi', 'Jurnal Elkomika', 'M. TEGUH KURNIAWAN ', 'ARIF NURFAJAR, OKTA PUSPITA DWI ANGGOROWATI, umar yunan', NULL, NULL, NULL, NULL, '15/1/2016', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(150, 'SISTEM INFORMASI', 'TS', 'Internasional', 'RELIABILITY BASED PERFORMANCE ANALYSIS OF BASE TRANSCEIVER STATION (BTS) USING RELIABILITY, AVAILABILITY, AND MAINTAINABILITY (RAM) METHOD', 'Prosiding Internasional TIDAK Terindeks', 'International Seminar on Industrial Engineering and Management (ISIEM)', 'JUDI ALHILMAN', 'RD. ROHMAT SAEDUDIN ', NULL, NULL, NULL, NULL, '20/9/2016', 'valid', '1', NULL, '', '', '', '', '', '', 1),
(151, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Membangun Aplikasi Kemahasiswaan Berbasis WEB Modul Pengelolaan Kegiatan Himpunan pada Sisi Himpunan Menggunakan Metode Iterative and Incremental', 'Jurnal Nasional Tidak Terakreditasi', 'JRSI (Jurnal Rekayasa Sistem dan Industri)', 'Anisatun Nafi''ah', 'SONI FAJAR SURYA GUMILANG, MUHAMMAD AZANI HASIBUAN', NULL, NULL, NULL, NULL, '23/6/2016', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(152, 'SISTEM INFORMASI', 'TS', 'Internasional', 'Data Center Power Management Design and Analysis in PDII-LIPI Using TIA-942 Standard', 'Prosiding Internasional TIDAK Terindeks', 'ISIEM', 'Algadilan Susanto', 'M. Teguh Kurniawan', NULL, NULL, NULL, NULL, '25/5/2016', 'valid', '1', NULL, '', '', '', '', '', '', 1),
(153, 'SISTEM INFORMASI', 'TS', 'Internasional', 'Analisis dan Perancangan Enterprise Architecture Fungsi Bisnis Analisis Pembangunan pada Badan Perencanaan dan Pembangunan Daerah (Bappeda) Provinsi Jawa Barat menggunakan Framework Togaf ADM', 'Prosiding Internasional TIDAK Terindeks', 'The 11th ICLS 2016', 'Anida Shafa', 'Yuli Adam Prasetyo, Rahmat Mulyana', NULL, NULL, NULL, NULL, '27/7/2016', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(154, 'SISTEM INFORMASI', 'TS', 'Internasional', 'Design E-commerce Angon Based on Marketplace to Increase Revenue for Livestock''s Actor (Selling Module)', 'Prosiding Internasional TIDAK Terindeks', 'ISIEM', 'Atika Elysia', 'Irfan Darmawan, M. Azani Hasibuan', NULL, NULL, NULL, NULL, '25/5/2016', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(155, 'SISTEM INFORMASI', 'TS', 'Internasional', 'Information System Development based on Enterprise Resource Planning using SAP with ASAP Methodology in Production Process PT. Len Industri', 'Prosiding Internasional TIDAK Terindeks', 'The 11th ICLS 2016', 'Clarissa Dilasari Setiawan', 'R. WAHJOE WITJAKSONO, NIA AMBARSARI ', NULL, NULL, NULL, NULL, '27/7/2016', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(156, 'SISTEM INFORMASI', 'TS', 'Internasional', 'Information System Development based on Enterprise Resource Planning using SAP with ASAP Methodology in Production Planning and Control ProcessPT. Len Industri', 'Prosiding Internasional TIDAK Terindeks', 'The 11th ICLS 2016', 'Delis Septianti Balgis', 'R. WAHJOE WITJAKSONO, NIA AMBARSARI ', NULL, NULL, NULL, NULL, '27/7/2016', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(157, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Pengembangan Sistem Informasi Berbasis Enterprise Resource Planning Menggunakan SAP Modul Plant Maintenance Di PT LEN Industri', 'Jurnal Nasional Tidak Terakreditasi', 'Jurnal SISFO Special Issue: Enterprise Systems 2016', 'Dwi Pratama', 'R. WAHJOE WITJAKSONO, NIA AMBARSARI ', NULL, NULL, NULL, NULL, '14/7/2016', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(158, 'SISTEM INFORMASI', 'TS', 'Lokal', 'ANALISIS DAN PERANCANGAN TATA KELOLA TI MENGGUNAKAN COBIT 4.1 DOMAIN DELIVER AND SUPPORT (DS) PT XYZ', 'Jurnal Nasional Tidak Terakreditasi', 'Jurnal Sistem Informasi', 'Inayatul Maghfiroh', 'Murahartawaty, Rahmat Mulyana', NULL, NULL, NULL, NULL, '30/5/2016', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(159, 'SISTEM INFORMASI', 'TS', 'Internasional', 'Design and Analysis Physical and Logical Security Using TIA-942 and ISO/IEC 27000 Series in Data Center of PDII-LIPI', 'Prosiding Internasional TIDAK Terindeks', 'ISIEM', 'Mukhlis Anugrah Pratama', 'M. Teguh Kurniawan, Adityas Widjajarto', NULL, NULL, NULL, NULL, '25/5/2016', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(160, 'SISTEM INFORMASI', 'TS', 'Nasional', 'PERANCANGAN SISTEM MANAJEMEN HUBUNGAN PELANGGAN UNTUK CV. PERCEKA MENGGUNAKAN DENGAN METODE ITERATIVE INCREMENTAL', 'Prosiding Nasional', 'Seminar Nasional Teknologi informasi (SNATI', 'Triyadi Yanuar', 'Irfan Darmawan, Taufik Nur Adi', NULL, NULL, NULL, NULL, '08-06-2016', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(161, 'SISTEM INFORMASI', 'TS', 'Internasional', 'Data Analysis of Li-Ion and Lead Acid Batteries Discharge Parameters with Simulink-MATLAB', 'Prosiding Internasional Terindeks IEEE', 'IcoICT 2016', 'Ekki Kurniawan', 'Basuki Rahmad, Tatang Mulyana', NULL, NULL, NULL, NULL, '25/5/2016', 'valid', '1', NULL, '', '', '', '', '', '', 1),
(162, 'SISTEM INFORMASI', 'TS', 'Nasional', 'Sebuah Usulan Model Kesiapan Adopsi Teknologi Informasi di Lingkungan Pemerintahan Daerah', 'Prosiding Nasional', 'Konferensi Nasional Sistem Informasi (KNSI) 2016', 'SONI FAJAR SURYA GUMILANG', 'HERU NUGROHO, Muhammad Azani Hasibuan', NULL, NULL, NULL, NULL, '08-11-2016', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(163, 'SISTEM INFORMASI', 'TS', 'Nasional', 'Pengembangan Sistem ERP Modul Manufacturing Odoo Dengan Metode RAD Di PT. Brodo Ganesha Indonesia', 'Prosiding Nasional', 'Konferensi Nasional Sistem Informasi (KNSI) 2016', 'Paramita Rahmawati', 'Soni Fajar Surya Gumilang, R. Wahjoe Witjaksono', NULL, NULL, NULL, NULL, '08-11-2016', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(164, 'SISTEM INFORMASI', 'TS', 'Internasional', 'Variety of Approaches in Self-Adaptation Requirements: A Case Study', 'Prosiding Internasional Terindeks Scopus', 'SCDM', 'Irfan Darmawan', NULL, NULL, NULL, NULL, NULL, '18/8/2016', 'valid', '1', NULL, '', '', '', '', '', '', 1),
(165, 'SISTEM INFORMASI', 'TS', 'Internasional', 'Distribution Disposal System Optimization', 'Prosiding yang diteruskan ke Jurnal', 'International Congress On Logistics And SCM Systems', 'Budi Santosa', 'Rd. Rohmat Saedudin, Erlangga Bayu Setyawan', NULL, NULL, NULL, NULL, '27/7/2016', 'valid', '1', NULL, '', '', '', '', '', '', 1),
(166, 'SISTEM INFORMASI', 'TS', 'Internasional', 'Soft Set Approach for Clustering Graduated Dataset', 'Jurnal Internasional Terindeks Thomson Reuters', 'SCDM', 'Rd. Rohmat Saedudin', 'Muhammad Azani Hasibuan', NULL, NULL, NULL, NULL, '18/8/2016', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(167, 'SISTEM INFORMASI', 'TS', 'Internasional', 'A Categorical Data Clustering Technique based on Classification Quality of Variable Precision Rough Set Model', 'Prosiding Internasional Terindeks Scopus', 'SCDM', 'Rd. Rohmat Saedudin', NULL, NULL, NULL, NULL, NULL, '18/8/2016', 'valid', '1', NULL, '', '', '', '', '', '', 1),
(168, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Perancangan Enterprise Architecture Pada Fungsi Penelitian Dan Pengembangan Bappeda Kabupaten Bandung Menggunakan Framework Togaf Adm', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Adelia Indah Oktaviyanti', 'Yuli Adam Prasetyo, Ridha Hanafi', NULL, NULL, NULL, NULL, '08-02-2016', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(169, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Perancangan Dan Pembangunan Knowledge Management System Modul Pindah Dan Tukar Kamar Dengan Menggunakan Metode Iterative Incremental Di Dormitory Telkom University', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Mochamad Annafie Yanuar Hakim', 'Luciana Andrawina, Faishal Mufied Al-anshary', NULL, NULL, NULL, NULL, '08-02-2016', 'valid', '1', NULL, '', '', '', '', '', '', 1),
(170, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Perancangan Enterprise Architecture Menggunakan Framework Togaf Adm Pada Bidang Perpustakaan Bapapsi Kabupaten Bandung', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Farid Hakim Niswansyah', 'Yuli Adam Prasetyo, Soni Fajar Surya Gumilang', NULL, NULL, NULL, NULL, '08-02-2016', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(171, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Perancangan E-commerce Angon Untuk Pelaku Peternakan Berbasis Marketplace Untuk Meningkatkan Efisiensi Pembelian (module Pembelian)', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Pratiwi Galuh Putri', 'Irfan Darmawan, Muhammad Azani Hasibuan', NULL, NULL, NULL, NULL, '08-02-2016', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(172, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Membangun Sistem Informasi Pelatihan Mahasiswa Berbasis Web Menggunakan Framework Codeigniter Pada Career Development Centre (cdc) Universitas Telkom', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Ferdy F Sibuea', 'Irfan Darmawan, Warih Puspitasari', NULL, NULL, NULL, NULL, '08-02-2016', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(173, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Perancangan Enterprise Architecture Fungsi Human Resources Development Dan Pengadaan Barang Menggunakan Framework Togaf Adm Pada Pt. Herona Express', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Hendrik Hendriana Firmansyah', 'Yuli Adam Prasetyo, Basuki Rahmad', NULL, NULL, NULL, NULL, '08-02-2016', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(174, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Analisis Dan Perancangan Enterprise Architecture Fungsi Bisnis Perencanaan Pembangunan Pada Badan Perencanaan Dan Pembangunan Daerah (bappeda) Provinsi Jawa Barat Menggunakan Framework Togaf Adm', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Dumauli Novitasari Boru Simanjuntak', 'Yuli Adam Prasetyo, Rahmat Mulyana', NULL, NULL, NULL, NULL, '08-02-2016', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(175, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Perancangan Enterprise Architecture Pada Bidang Sumber Daya Manusia Dan Organisasi Serta Bidang Komunikasi Hukum Dan Administrasi Pt Pln Distribusi Jawa Barat Menggunakan Framework Togaf Adm', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Ersa Novia Fajrin', 'Yuli Adam Prasetyo, Faishal Mufied Al-anshary', NULL, NULL, NULL, NULL, '08-02-2016', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(176, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Perancangan Sistem Procurement Berbasis Odoo Dengan Soft System Methodology Di Rumah Sakit Muhammadiyah Bandung', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Adam Ramadhan', 'Lucia Andrawina, R. Wahjoe Witjaksono', NULL, NULL, NULL, NULL, '08-02-2016', 'valid', '1', NULL, '', '', '', '', '', '', 1),
(177, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Analisis Dan Perancangan Enterprise Architecture Fungsi Bisnis Pengendalian Dan Evaluasi Pembangunan Pada Badan Perencanaan Dan Pembangunan Daerah (bappeda) Provinsi Jawa Barat Menggunakan Framework Togaf Adm', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Afrianda Gaza Ontoreza', 'Yuli Adam Prasetyo, Rahmat Mulyana', NULL, NULL, NULL, NULL, '08-02-2016', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(178, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Pengembangan Odoo Modul Warehouse Pada Gudang Pt. Tarumatex Menggunakan Metode Rapid Application Development', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Aldi Mustafri', 'Deden Witarsyah, R. Wahjoe Witjaksono', NULL, NULL, NULL, NULL, '08-02-2016', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(179, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Perancangan Enterprise Architecture Pada Bidang Distribusi Di Pt. Pln Distribusi Jawa Barat Menggunakan Framework Togaf Adm', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Recsa Andriyani Putri', 'Yuli Adam Prasetyo, Faishal Mufied Al-anshary', NULL, NULL, NULL, NULL, '08-02-2016', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(180, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Perancangan Odoo Modul Quality Control Pada Inspection Pt. Tarumatex Menggunakan Metode Rapid Application Development', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Parasetia Abu Aditya', 'Deden Witarsyah, R. Wahjoe Witjaksono', NULL, NULL, NULL, NULL, '08-02-2016', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(181, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Implementasi Sistem Produksi Berbasis Odoo Pada Pt. Primarindo Asia Infrastructure Tbk Dengan Metodologi Asap', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Yuanika Indanea', 'Rd. Rohmat Saedudin, R. Wahjoe Witjaksono', NULL, NULL, NULL, NULL, '08-02-2016', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(182, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Perancangan Arsitektur Bisnis Dan Arsitektur Teknologi Untuk Layanan Jasa Keuangan Pada Pt.xyz Menggunakan Togaf Adm', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Delphine Yusticia Ratnasari', 'Yuli Adam Prasetyo, Rahmat Mulyana', NULL, NULL, NULL, NULL, '08-02-2016', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(183, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Pengembangan Point Of Sales & Inventory Manajemen Pada Aplikasi E-apotik Dengan Metode Waterfall (studi Kasus Klinik Medika 24)', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Lia Avita Sari', 'Yuli Adam Prasetyo, Budi Santosa', NULL, NULL, NULL, NULL, '08-02-2016', 'valid', '1', NULL, '', '', '', '', '', '', 1),
(184, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Pengembangan Modul Sales Management Berbasis Odoo Dengan Metode Accelerated Sap Pada Inglorious Industries', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Ibrahim Hanif Alkhalil', 'Rd. Rohmat Saedudin, R. Wahjoe Witjaksono', NULL, NULL, NULL, NULL, '08-02-2016', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(185, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Perancangan Sistem Erp Modul Accounting Odoo 9 Pada Pt. Aretha Nusantara Farm Dengan Metode', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Syinta Kesuma Aisyah', 'Rd. Rohmat Saedudin, R. Wahjoe Witjaksono', NULL, NULL, NULL, NULL, '08-02-2016', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(186, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Pengembangan Sistem Erp Purchase Management Menggunakan Odoo Pada Pt.pdu Dengan Metoda Asap', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Intan Dwi Ariesta Putri', 'Rd. Rohmat Saedudin, R. Wahjoe Witjaksono', NULL, NULL, NULL, NULL, '08-02-2016', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(187, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Pengembangan Modul Purchase Dan Warehouse Management Berbasis Odoo Dengan Metode Accelerated Sap Pada Inglorious Industries', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Fiega Dwi Novwari', 'Rd. Rohmat Saedudin, R. Wahjoe Witjaksono', NULL, NULL, NULL, NULL, '08-02-2016', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(188, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Perancangan Manajemen Risiko Teknologi Informasi Pada Core Processes Edm03 Dan Key Supporting Processes Edm01, Edm02, Dan Edm05 Di Dinas Komunikasi Dan Informatika (diskominfo) Pemerintah Kota Bandung Menggunakan Framework Cobit 5', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Rissa Puspita', 'Murahartawaty, Eko Kusbang Umar', NULL, NULL, NULL, NULL, '08-02-2016', 'valid', '1', NULL, '', '', '', '', '', '', 1),
(189, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Perancangan Enterprise Architecture Pada Bidang Niaga Dan Pelayanan Pelanggan Pt Pln Distribusi Jawa Barat Menggunakan Framework Togaf Adm', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Rinaldi Harry Leksana', 'Yuli Adam Prasetyo, Faishal Mufied Al-anshary', NULL, NULL, NULL, NULL, '08-02-2016', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(190, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Perancangan Tata Kelola Manajemen Layanan Teknologi Informasi Menggunakan Itil V3 Domain Service Transition Dan Service Operation Di Pemerintah Kota Bandung', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Luki Aisha Kusuma Wardani', 'Murahartawaty, Luthfi Ramadani', NULL, NULL, NULL, NULL, '08-02-2016', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(191, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Analysis And Design Of Enterprise Architecture Using Togaf Adm Framework In Procurement Division And Material Management Division Pt. Inti (persero)', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Dian Suci Astuti', 'Yuli Adam Prasetyo, Rahmat Mulyana', NULL, NULL, NULL, NULL, '08-02-2016', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(192, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Analisis Dan Perancangan Aplikasi Personal File Menggunakan Metode Itterative Incremental Pada Telkom Pcc', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Bala Putra Dewa', 'Yuli Adam Prasetyo, Irfan Darmawan', NULL, NULL, NULL, NULL, '08-02-2016', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(193, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Developing Web Application For Islamic Banking Module Using Iterative Incremental Method And Laravel Framework For Distribution Ziswaf Fund To Sme’s', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Elzar Esen Uulu', 'Murahartawaty, Ari Pratiwi', NULL, NULL, NULL, NULL, '08-02-2016', 'valid', '1', NULL, '', '', '', '', '', '', 1),
(194, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Pengembangan Sistem Informasi Berbasis Enterprise Resource Planning Modul Warehouse Management Pada Odoo Dengan Metode Rapid Application Development Di Pt. Brodo Ganesha Indonesia', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Anita Rahma Maulida', 'Soni Fajar Surya Gumilang, R. Wahjoe Witjaksono', NULL, NULL, NULL, NULL, '08-02-2016', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(195, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Perancangan Enterprise Architecture Pada Fungsi Monitoring Dan Evaluasi Bappeda Kabupaten Bandung Menggunakan Framework Togaf Adm', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Widyatasya Agustika Nurtrisha', 'Yuli Adam Prasetyo, Ridha Hanafi', NULL, NULL, NULL, NULL, '08-02-2016', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(196, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Perancangan Dan Pembangunan Knowledge Management System Modul Registrasi Dan Reservasi Dengan Menggunakan Metode Iterative Incremental Di Universitas Telkom', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Abdi Robana Agnia', 'Luciana Andrawina, Faishal Mufied Al-anshary', NULL, NULL, NULL, NULL, '08-02-2016', 'valid', '1', NULL, '', '', '', '', '', '', 1),
(197, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Information System Strategic Planning Based On Togaf Adm Framework In 2nd Revenue Functions Dinas Pendapatan Dan Pengelolaan Keuangan Bandung Regency', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Anisa Fatakh Sabila', ' Yuli Adam Prasetyo, Ridha Hanafi', NULL, NULL, NULL, NULL, '08-02-2016', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(198, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Pengembangan Sistem Informasi Berbasis Enterprise Resource Planning Modul Sales Management Pada Odoo Dengan Metode Rapid Application Development Di Pt. Brodo Ganesha Indonesia', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Aji Wira Pradhana', 'Soni Fajar Surya Gumilang, R. Wahjoe Witjaksono', NULL, NULL, NULL, NULL, '08-02-2016', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(199, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Perancangan Enterprise Architecture Pada Fungsi Perencanaan Pembangunan Bappeda Kabupaten Bandung Menggunakan Framework Togaf Adm', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Muh. Fachry Putera P.', 'Yuli Adam Prasetyo, Ridha Hanafi', NULL, NULL, NULL, NULL, '08-02-2016', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(200, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Perancangan Dan Pembangunan Knowledge Management System Untuk Modul Publikasi Menggunakan Framework Codeigniter Dengan Metode Iterative Incremental', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'M. Denis Syahputra Nasution', 'Lucia Andrawina, Ahmad Munansyah', NULL, NULL, NULL, NULL, '08-02-2016', 'valid', '1', NULL, '', '', '', '', '', '', 1),
(201, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Perancangan Enterprise Architecture Menggunakan Framework Togaf Adm Pada Bidang Kearsipan Bapapsi Kabupaten Bandung', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Ema Tria Wahyuningtihas', 'Yuli Adam Prasetyo, Soni Fajar Surya Gumilang', NULL, NULL, NULL, NULL, '08-02-2016', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(202, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Perancangan E-commerce Angon Untuk Pelaku Peternakan Berbasis Marketplace Untuk Meningkatkan Kepercayaan Pengguna (modul Pengelolaan Pembayaran Dan Keluhan)', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Nur Intan Paramanisa', 'Irfan Darmawan, Muhammad Azani Hasibuan', NULL, NULL, NULL, NULL, '08-02-2016', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(203, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Perancangan Sistem Warehouse Berbasis Odoo Dengan Soft System Methodology Di Rumah Sakit Muhammadiyah Bandung', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Ahmad Akbar Linggo M.', 'Lucia Andrawina, R. Wahjoe Witjaksono', NULL, NULL, NULL, NULL, '08-02-2016', 'valid', '1', NULL, '', '', '', '', '', '', 1),
(204, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Perancangan Enterprise Architecture Fungsi Marketing Dan Layanan Pelanggan Menggunakan Framework Togaf Adm Pada Pt. Herona Express', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Ian Fahmi Nugraha', 'Yuli Adam Prasetyo, Basuki Rahmad', NULL, NULL, NULL, NULL, '08-02-2016', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(205, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Analysis And Design Enterprise Architecture Using Togaf Adm In Account Team And Sales And Marketing Support Division Of Pt. Inti (persero)', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Afifah Nurul Izzati', 'Yuli Adam Prasetyo, Rahmat Mulyana', NULL, NULL, NULL, NULL, '08-02-2016', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(206, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Perancangan Tata Kelola Manajemen Layanan Teknologi Informasi Berdasarkan Itil V3 Domain Service Design Di Pemerintah Kota Bandung', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Fikrotun Nadiyya', 'Murahartawaty, Luthfi Ramadani', NULL, NULL, NULL, NULL, '08-02-2016', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(207, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Perancangan Enterprise Architecture Pada Bidang Perencanaan Dan Bidang Keuangan Di Pt. Pln Distribusi Jawa Barat Menggunakan Framework Togaf Adm', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Mutia Dewi Kurniasih', 'Yuli Adam Prasetyo, Faishal Mufied Al-anshary', NULL, NULL, NULL, NULL, '08-02-2016', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(208, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Perancangan Sistem Asset Management Berbasis Odoo Dengan Soft System Methodology Di Rumah Sakit Muhammadiyah Bandung', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Riza Rahma Putri', 'Luciana Andrawina, R. Wahjoe Witjaksono', NULL, NULL, NULL, NULL, '08-02-2016', 'valid', '1', NULL, '', '', '', '', '', '', 1),
(209, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Analysis And Design Of Enterprise Architecture Using Togaf Adm In Product Development Division And Production And Operation Division Of Pt. Inti (persero)', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Inas Nisrina', 'Yuli Adam Prasetyo, Rahmat Mulyana', NULL, NULL, NULL, NULL, '08-02-2016', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(210, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Membangun Inventory Decision Support System Untuk Penentuan Kebijakan Order Obat Dengan Menerapkan Metode Continous Review Dan Waterfall', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Dhiya Afwan Taufiq', 'Rachmadita Andreswari, Budi Santosa', NULL, NULL, NULL, NULL, '08-02-2016', 'valid', '1', NULL, '', '', '', '', '', '', 1),
(211, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Perancangan Arsitektur Bisnis Dan Arsitektur Aplikasi Untuk Layanan Jasa Keuangan Pt.xyz Dengan Menggunakan Togaf Adm', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Citra Melati', 'Yuli Adam Prasetyo, Rahmat Mulyana', NULL, NULL, NULL, NULL, '08-02-2016', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(212, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Developing Business Intelligence System Based On Data Warehouse Using Pentaho For Procurement Process With Business Dimensional Life-cycle Methodology (case Study: Perum Bulog Divre Jabar)', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'M. Firman Helmi Ariyansyah', ' Ari Yanuar Ridwan, Rachmadita Andreswari', NULL, NULL, NULL, NULL, '08-02-2016', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(213, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Implementasi Sistem Purchasing Dan Warehouse Management Berbasis Odoo Pada Pt. Primarindo Asia Infrastructure Tbk Dengan Metodologi Asap', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Firdayakusmawarni', 'Rd. Rohmat Saedudin, R. Wahjoe Witjaksono', NULL, NULL, NULL, NULL, '08-02-2016', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(214, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Perancangan Arsitektur Bisnis Dan Arsitektur Data Untuk Layanan Jasa Keuangan Pada Pt.xyz Menggunakan Togaf Adm', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Affifiana Prisyanti', 'Yuli Adam Prasetyo, Rahmat Mulyana', NULL, NULL, NULL, NULL, '08-02-2016', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(215, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Pengembangan Sistem Erp Warehouse Management Menggunakan Odoo Pada Pt. Putri Daya Usahatama Dengan Metode Asap', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Mochamad Hafiz Kurniawan', 'Rd. Rohmat Saedudin, R. Wahjoe Witjaksono', NULL, NULL, NULL, NULL, '08-02-2016', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(216, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Pengembangan Modul Manufacturing Berbasis Odoo Dengan Metode Accelerated Sap Pada Inglorious Industries', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Vegi Fransisca', 'Rd. Rohmat Saedudin, R. Wahjoe Witjaksono', NULL, NULL, NULL, NULL, '08-02-2016', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(217, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Perancangan Manajemen Risiko Teknologi Informasi Pada Key Supporting Process Apo02, Apo06 Dan Apo08 Di Dinas Komunikasi Dan Informatika (diskominfo) Pemerintah Kota Bandung Menggunakan Framework Cobit 5', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Dyah Wahyuningtias Iswari', 'Murahartawaty, Eko Kusbang Umar', NULL, NULL, NULL, NULL, '08-02-2016', 'valid', '1', NULL, '', '', '', '', '', '', 1),
(218, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Perancangan Sistem Erp Dengan Modul Purchasing Dan Inventory Berbasis Odoo 9 Dengan Metode Asap Pada Pt. Aretha Nusantara Farm', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Afrizal Samsul Hidayat', 'Rd. Rohmat Saedudin, R. Wahjoe Witjaksono', NULL, NULL, NULL, NULL, '08-02-2016', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(219, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Pengembangan Sistem Erp Sales Management Menggunakan Odoo Pada Pt Putri Daya Usahatama Dengan Metode Asap', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Ugi Chandra Wiguna', 'Rd. Rohmat Saedudin, R. Wahjoe Witjaksono', NULL, NULL, NULL, NULL, '08-02-2016', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(220, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Analysis And Design Of Enterprise Architecture Using Togaf Adm Framework In Project Divisions Of Pt. Inti (persero)', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Dini Salsabila Di Kusumah', 'Yuli Adam Prasetyo, Rahmat Mulyana', NULL, NULL, NULL, NULL, '08-02-2016', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(221, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Analisis Dan Perancangan Aplikasi Personal File Modul E-learning Pada Telkom Pcc Dengan Metode Iterative Dan Incremental', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Irvan Gunawan', 'Yuli Adam Prasetyo, Irfan Darmawan', NULL, NULL, NULL, NULL, '08-02-2016', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(222, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Information System Strategic Planning Based On Togaf Adm Framework In Budget, Treasury, And Accounting Function Of Revenue And Financial Management Department Bandung Regency', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Almareta Harasti Nuraini', 'Yuli Adam Prasetyo, Ridha Hanafi', NULL, NULL, NULL, NULL, '08-02-2016', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(223, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Pengembangan Sistem Informasi Berbasis Enterprise Resource Planning Modul Quality Management Menggunakan Sap Dengan Metode Asap Pada Bagian Produksi Pt. Len Industri', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'I Made Tekad Kalimantara', 'R. Wahjoe Witjaksono, Nia Ambarsari', NULL, NULL, NULL, NULL, '08-02-2016', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(224, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Developing Reporting Module In Micro Monitoring System For Small Medium Enterprise Using Iterative Incremental Method And Laravel Framework', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Nana Ramadhewi', 'Murahartawaty, Ari Pratiwi, Nur Ichsan Utama', NULL, NULL, NULL, NULL, '08-02-2016', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(225, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Penerapan Sistem Sales Management Menggunakan Openerp Dengan Metode Rapid Application Development', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Dika Anugerah Pratama', 'Ari Yanuar Ridwan, R. Wahjoe Witjaksono', NULL, NULL, NULL, NULL, '08-02-2016', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(226, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Developing Monitoring System Dashboard Module Using Iterative Incremental Method For Monitoring Ziswaf Productive Reporting Performance', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Reicka Sofi Azura Ridhallah', 'Murahartawaty, Ari Pratiwi', NULL, NULL, NULL, NULL, '08-02-2016', 'valid', '1', NULL, '', '', '', '', '', '', 1),
(227, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Pengembangan Sistem Informasi Berbasis Enterprise Resource Planning Modul Purchase Management Pada Odoo Dengan Metode Rapid Application Development Di Pt. Brodo Ganesha Indonesia', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Risna Risdianti Juniar', 'Soni Fajar Surya Gumilang, R. Wahjoe Witjaksono', NULL, NULL, NULL, NULL, '08-02-2016', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(228, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Membangun Sistem Informasi Manajemen Kerja Praktik Berbasis Website Dengan Metode Iterative Incremental (modul Mahasiswa)', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Ichsan Akbar', 'Muhammad Azani Hasibuan, Warih Puspitasari', NULL, NULL, NULL, NULL, '12-03-2016', 'valid', '2', NULL, '', '', '', '', '', '', 1),
(229, 'SISTEM INFORMASI', 'TS', 'Lokal', 'Membangun Sistem Informasi Manajemen Kerja Praktik Berbasis Website Dengan Metode Iterative Incremental (modul Perusahaan)', NULL, 'e-Proceeding of Engineering ISSN : 2355-9365', 'Zigit Maha Putra', 'Muhammad Azani Hasibuan, Warih Puspitasari', NULL, NULL, NULL, NULL, '12-03-2016', 'valid', '2', NULL, '', '', '', '', '', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_tamahasiswa`
--

CREATE TABLE `tb_tamahasiswa` (
  `no` int(11) NOT NULL,
  `no_ts` varchar(100) DEFAULT NULL,
  `tahun_ajrn` varchar(1000) DEFAULT NULL,
  `nim` varchar(100) DEFAULT NULL,
  `nama_mahasiswa` varchar(500) DEFAULT NULL,
  `kelas` varchar(100) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `input_date` varchar(100) DEFAULT NULL,
  `pembimbing_akdmk` varchar(100) DEFAULT NULL,
  `kode_pngjr` varchar(100) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_tamahasiswa`
--

INSERT INTO `tb_tamahasiswa` (`no`, `no_ts`, `tahun_ajrn`, `nim`, `nama_mahasiswa`, `kelas`, `status`, `input_date`, `pembimbing_akdmk`, `kode_pngjr`, `id_user`) VALUES
(1, 'TS', 'Genap 2015/2016', '1106110085', 'ABDI ROBANA AGNIA', 'SI-35-03', 'FIX', '03-01-2016', 'MOW', 'MHY', 1),
(2, 'TS', 'Genap 2015/2016', '1106120102', 'ABDUL FATAH AL-LATIF', 'SI-36-04', 'FIX', '01-01-2016', 'SNP', 'MHY', 1),
(3, 'TS', 'Genap 2015/2016', '1106120146', 'ABDULLAH', 'SI-36-01', 'FIX', '04-01-2016', 'MTK', 'MHY', 1),
(4, 'TS_2', 'Genap 2013/2014', '1106100002', 'ABU YAZID AFIF', 'SI-34-01', 'FIX', '24-03-2014', 'UMY', 'MHY', 1),
(5, 'TS', 'Genap 2015/2016', '1106120071', 'ADAM RAMADHAN', 'SI-36-03', 'FIX', '01-01-2016', 'YAP', 'MHY', 3),
(6, 'TS_1', 'Genap 2014/2015', '1106110119', 'ADELIA CHITRA SAZKIA', 'SI-35-04', 'FIX', '08-01-2015', 'MTK', 'MHY', 1),
(7, 'TS_1', 'Genap 2014/2015', '1106110091', 'ADELIA FEBIYANTI M.', 'SI-35-03', 'FIX', '07-01-2015', 'MOW', 'MHY', 1),
(8, 'TS', 'Genap 2015/2016', '1106120045', 'ADELIA INDAH OKTAVIYANTI', 'SI-36-02', 'FIX', '04-01-2016', 'WRP', 'MHY', 1),
(9, 'TS_2', 'Genap 2013/2014', '1106104110', 'ADELINA DARYL SOFI', 'SI-34-03', 'FIX', '24-03-2014', 'SNP', 'MHY', 1),
(10, 'TS_2', 'Ganjil 2013/2014', '1106090018', 'ADITYA BANGUN MAHARDHIKA KUSRIADI', 'SI-33-01', 'FIX', '26-08-2013', 'MHY', 'MHY', 1),
(11, 'TS_2', 'Genap 2013/2014', '1106100046', 'ADITYA PERDANA', 'SI-34-02', 'FIX', '24-03-2014', 'YAP', 'MHY', 1),
(12, 'TS_1', 'Genap 2014/2015', '1106110114', 'ADITYA PRADANA PUTRA', 'SI-35-04', 'FIX', '06-01-2015', 'MTK', 'MHY', 1),
(13, 'TS', 'Genap 2015/2016', '1106120097', 'ADITYA PRATAMA NUGRAHA RAHMAYADI', 'SI-36-04', 'FIX', '30-12-2015', 'SNP', 'MHY', 1),
(14, 'TS_2', 'Genap 2013/2014', '1106101081', 'ADITYA WIDYA WARDANA', 'SI-34-03', 'FIX', '24-03-2014', 'SNP', 'MHY', 1),
(15, 'TS_2', 'Genap 2013/2014', '1106100015', 'ADJI HARRI SUBAGDO', 'SI-34-01', 'FIX', '24-03-2014', 'UMY', 'MHY', 1),
(16, 'TS', 'Genap 2015/2016', '1106122109', 'ADNAN BAHARRUDIN FANANI', 'SI-36-04', 'FIX', '01-01-2016', 'SNP', 'MHY', 1),
(17, 'TS_2', 'Ganjil 2013/2014', '1106090010', 'ADVENTUS ANGGA KURNIAWAN', 'SI-33-01', 'FIX', '23-08-2013', 'MHY', 'MHY', 1),
(18, 'TS', 'Genap 2015/2016', '1106120012', 'AFFIFIANA PRISYANTI', 'SI-36-01', 'FIX', '31-12-2015', 'MTK', 'MHY', 1),
(19, 'TS_2', 'Genap 2013/2014', '1106101096', 'AFGHAN AMAR PRADIPTA', 'SI-34-03', 'FIX', '24-03-2014', 'SNP', 'MHY', 1),
(20, 'TS', 'Genap 2015/2016', '1106120042', 'AFIFAH NURUL IZZATI', 'SI-36-02', 'FIX', '04-01-2016', 'WRP', 'MHY', 1),
(21, 'TS', 'Genap 2015/2016', '1106120040', 'AFRIANDA GAZA ONTOREZA', 'SI-36-02', 'FIX', '04-01-2016', 'WRP', 'MHY', 1),
(22, 'TS', 'Genap 2015/2016', '1106120142', 'AFRIZAL SAMSUL HIDAYAT', 'SI-36-01', 'FIX', '04-01-2016', 'MTK', 'MHY', 1),
(23, 'TS_2', 'Genap 2013/2014', '1106100052', 'AGITYA FAJAR ROKHMAT', 'SI-34-02', 'FIX', '24-03-2014', 'YAP', 'MHY', 1),
(24, 'TS', 'Genap 2015/2016', '1106124202', 'AGUNG CANDRA DUTIYA PURWANTA', 'SI-36-04', 'FIX', '30-12-2015', 'SNP', 'MHY', 1),
(25, 'TS_1', 'Genap 2014/2015', '1106110087', 'AGUNG INSANI ALAM', 'SI-35-03', 'FIX', '12-01-2015', 'MOW', 'MHY', 1),
(26, 'TS_2', 'Ganjil 2013/2014', '1106080026', 'AGUS GUNAWAN', 'SI-32-02', 'FIX', '26-08-2013', 'RIZ', 'MHY', 1),
(27, 'TS', 'Genap 2015/2016', '1106120082', 'AHMAD AKBAR LINGGO M', 'SI-36-03', 'FIX', '31-12-2015', 'YAP', 'MHY', 1),
(28, 'TS_2', 'Genap 2013/2014', '1106100072', 'AHMAD ASYARY LUKMAN', 'SI-34-02', 'FIX', '24-03-2014', 'YAP', 'MHY', 1),
(29, 'TS_2', 'Genap 2013/2014', '1106080002', 'AHMAD BAIDAWI', 'SI-32-01', 'FIX', '24-03-2014', 'MTK', 'MHY', 1),
(30, 'TS', 'Genap 2015/2016', '1106110137', 'AHMAD RASYIDDIN', 'SI-35-04', 'FIX', '05-01-2016', 'MTK', 'MHY', 1),
(31, 'TS_2', 'Genap 2013/2014', '1106100031', 'AHSANI TAQWIM', 'SI-34-01', 'FIX', '24-03-2014', 'UMY', 'MHY', 1),
(32, 'TS', 'Genap 2015/2016', '1106120116', 'AIDIYA SAFIRA', 'SI-36-05', 'FIX', '02-01-2016', 'MOW', 'MHY', 1),
(33, 'TS_1', 'Genap 2014/2015', '1106114144', 'AINU FAISAL PAMBUDY', 'SI-35-04', 'FIX', '06-01-2015', 'MTK', 'MHY', 1),
(34, 'TS_2', 'Genap 2013/2014', '1106100045', 'AINUN KHAIRIYAH FADLA', 'SI-34-02', 'FIX', '24-03-2014', 'YAP', 'MHY', 1),
(35, 'TS_1', 'Genap 2014/2015', '1106110023', 'Ajeng Citra Rizkyanur', 'SI-35-01', 'FIX', '07-01-2015', 'AGU', 'MHY', 1),
(36, 'TS', 'Genap 2015/2016', '1106124199', 'AJI WIRA PRADHANA', 'SI-36-04', 'FIX', '30-12-2015', 'SNP', 'MHY', 1),
(37, 'TS', 'Genap 2015/2016', '1106120119', 'AKBAR MAULANA DAFIK TORICA', 'SI-36-05', 'FIX', '30-12-2015', 'MOW', 'MHY', 1),
(38, 'TS', 'Ganjil 2015/2016', '1106110097', 'ALDI MUSTAFRI', 'SI-35-03', 'FIX', '10-08-2015', 'MOW', 'MHY', 1),
(39, 'TS_2', 'Ganjil 2013/2014', '1106080014', 'ALEXANDER', 'SI-32-02', 'FIX', '24-08-2013', 'IMR', 'MHY', 1),
(40, 'TS_2', 'Ganjil 2013/2014', '1106090030', 'ALFI HANIF NOOR', 'SI-33-02', 'FIX', '25-08-2013', 'WRP', 'MHY', 1),
(41, 'TS', 'Genap 2015/2016', '1106120007', 'ALGADILAN SUSANTO', 'SI-36-01', 'FIX', '30-12-2015', 'MTK', 'MHY', 1),
(42, 'TS_1', 'Genap 2014/2015', '1106110038', 'ALIFIA INDRA DAMARANI', 'SI-35-02', 'FIX', '07-01-2015', 'TNA', 'MHY', 1),
(43, 'TS_1', 'Genap 2014/2015', '1106110027', 'ALIMA INDRIATI', 'SI-35-01', 'FIX', '12-01-2015', 'AGU', 'MHY', 1),
(44, 'TS', 'Genap 2015/2016', '1106120086', 'ALMARETA HARASTI NURAINI', 'SI-36-04', 'FIX', '30-12-2015', 'SNP', 'MHY', 1),
(45, 'TS_2', 'Ganjil 2013/2014', '1106081031', 'ALVIAN SESA MAHARDHIKA', 'SI-32-02', 'FIX', '25-08-2013', 'RIZ', 'MHY', 1),
(46, 'TS', 'Genap 2015/2016', '1106120089', 'AMANDA YUNIA ZAFARINA', 'SI-36-04', 'FIX', '30-12-2015', 'SNP', 'MHY', 1),
(47, 'TS_2', 'Genap 2013/2014', '1106100036', 'ANANG PRASETYO MULYO', 'SI-34-01', 'FIX', '24-03-2014', 'UMY', 'MHY', 1),
(48, 'TS_2', 'Ganjil 2013/2014', '1106080021', 'ANDI MUHAMMAD IRHAM', 'SI-32-02', 'FIX', '25-08-2013', 'RIZ', 'MHY', 1),
(49, 'TS_2', 'Genap 2013/2014', '1106101097', 'ANDIDA FATINAH', 'SI-34-03', 'FIX', '24-03-2014', 'SNP', 'MHY', 1),
(50, 'TS_1', 'Genap 2014/2015', '1106110064', 'ANDIKA DESTA GINANJAR', 'SI-35-02', 'FIX', '07-01-2015', 'TNA', 'MHY', 1),
(51, 'TS_2', 'Ganjil 2013/2014', '1106081039', 'ANDRE NUGROHO', 'SI-32-01', 'FIX', '24-08-2013', 'MHY', 'MHY', 1),
(52, 'TS_2', 'Ganjil 2013/2014', '1106081029', 'ANDRINI HANARIANA', 'SI-32-02', 'FIX', '24-08-2013', 'RIZ', 'MHY', 1),
(53, 'TS_2', 'Genap 2013/2014', '1106100049', 'ANFUSA GANDRI HERUCAKRA', 'SI-34-02', 'FIX', '24-03-2014', 'YAP', 'MHY', 1),
(54, 'TS_2', 'Genap 2013/2014', '1106104102', 'ANGGITA REGIA MARPAUNG', 'SI-34-03', 'FIX', '24-03-2014', 'SNP', 'MHY', 1),
(55, 'TS', 'Genap 2015/2016', '1106120041', 'ANIDA SHAFA', 'SI-36-02', 'FIX', '04-01-2016', 'WRP', 'MHY', 1),
(56, 'TS', 'Genap 2015/2016', '1106120037', 'ANISA FATAKH SABILA', 'SI-36-02', 'FIX', '30-12-2015', 'WRP', 'MHY', 1),
(57, 'TS', 'Genap 2015/2016', '1106124198', 'ANISATUN NAFIAH', 'SI-36-04', 'FIX', '30-12-2015', 'SNP', 'MHY', 1),
(58, 'TS', 'Genap 2015/2016', '1106120175', 'ANITA RAHMA MAULIDA', 'SI-36-04', 'FIX', '04-01-2016', 'SNP', 'MHY', 1),
(59, 'TS_2', 'Genap 2013/2014', '1106100004', 'ANNISA NURUL FITRIA. F', 'SI-34-01', 'FIX', '24-03-2014', 'UMY', 'MHY', 1),
(60, 'TS_2', 'Genap 2013/2014', '1106100005', 'ANNISA RAHMANIA', 'SI-34-01', 'FIX', '24-03-2014', 'UMY', 'MHY', 1),
(61, 'TS_1', 'Genap 2014/2015', '1106110031', 'ANNISA UTAMI', 'SI-35-01', 'FIX', '09-01-2015', 'AGU', 'MHY', 1),
(62, 'TS', 'Genap 2015/2016', '1106122134', 'ANTON ILMIAR WINDRISWARA', 'SI-36-05', 'FIX', '02-01-2016', 'MOW', 'MHY', 1),
(63, 'TS_1', 'Genap 2014/2015', '1106110035', 'ARDHYAN ZULFIKAR MALIK', 'SI-35-01', 'FIX', '07-01-2015', 'AGU', 'MHY', 1),
(64, 'TS', 'Genap 2015/2016', '1106120092', 'ARI CHANDRA WARDANI', 'SI-36-04', 'FIX', '30-12-2015', 'IFD', 'MHY', 1),
(65, 'TS_1', 'Genap 2014/2015', '1106110063', 'ARIDHA MEITYA ARIFIN', 'SI-35-02', 'FIX', '08-01-2015', 'TNA', 'MHY', 1),
(66, 'TS', 'Genap 2015/2016', '1106120164', 'ARIES ARIEFFANDY SUMARSO', 'SI-36-03', 'FIX', '30-12-2015', 'YAP', 'MHY', 1),
(67, 'TS_2', 'Ganjil 2013/2014', '1106090033', 'ARIES PUTRA PRATAMA', 'SI-33-02', 'FIX', '26-08-2013', 'WRP', 'MHY', 1),
(68, 'TS_2', 'Ganjil 2013/2014', '1106090020', 'ARIF AHMADSYAH', 'SI-33-01', 'FIX', '23-08-2013', 'MHY', 'MHY', 1),
(69, 'TS', 'Genap 2015/2016', '1106120096', 'ARIF MAULANA NP', 'SI-36-04', 'FIX', '01-01-2016', 'IFD', 'MHY', 1),
(70, 'TS_1', 'Genap 2014/2015', '1106110134', 'ARIF NURFAJAR', 'SI-35-04', 'FIX', '06-01-2015', 'MTK', 'MHY', 1),
(71, 'TS_1', 'Genap 2014/2015', '1106110095', 'ARIS SETYO NUGROHO', 'SI-35-03', 'FIX', '12-01-2015', 'MOW', 'MHY', 1),
(72, 'TS', 'Genap 2015/2016', '1106120154', 'ARIZONA DWI FAJARWATI', 'SI-36-02', 'FIX', '30-12-2015', 'WRP', 'MHY', 1),
(73, 'TS_1', 'Ganjil 2014/2015', '1106100035', 'ARNIE NUR RAMADHANI', 'SI-34-01', 'FIX', '19-08-2014', 'UMY', 'MHY', 1),
(74, 'TS', 'Genap 2015/2016', '1106120062', 'ARNOLD REWADIPO PURBA', 'SI-36-03', 'FIX', '02-01-2016', 'YAP', 'MHY', 1),
(75, 'TS_1', 'Genap 2014/2015', '1106110121', 'ARRAZAQ ZAKARIA M.S.', 'SI-35-04', 'FIX', '07-01-2015', 'MTK', 'MHY', 1),
(76, 'TS_1', 'Genap 2014/2015', '1106110118', 'ASHOF YUDHISTIRA M.P.', 'SI-35-04', 'FIX', '08-01-2015', 'MTK', 'MHY', 1),
(77, 'TS', 'Genap 2015/2016', '1106124194', 'ATIKA ELYSIA', 'SI-36-02', 'FIX', '03-01-2016', 'WRP', 'MHY', 1),
(78, 'TS', 'Genap 2015/2016', '1106120065', 'ATIKA LUTHFIANI IFAN', 'SI-36-03', 'FIX', '01-01-2016', 'YAP', 'MHY', 1),
(79, 'TS', 'Genap 2015/2016', '1106120171', 'AULIA EKWIN PERMANASARI', 'SI-36-04', 'FIX', '30-12-2015', 'IFD', 'MHY', 1),
(80, 'TS_2', 'Genap 2013/2014', '1106100047', 'AULIA PRIMADANI', 'SI-34-02', 'FIX', '24-03-2014', 'YAP', 'MHY', 1),
(81, 'TS_2', 'Ganjil 2013/2014', '1106080003', 'AZIZA PUSTHIKA TANAYA', 'SI-32-01', 'FIX', '25-08-2013', 'IMR', 'MHY', 1),
(82, 'TS_2', 'Ganjil 2013/2014', '1106091055', 'AZZAHRA RATU KAMILA', 'SI-33-02', 'FIX', '23-08-2013', 'YAP', 'MHY', 1),
(83, 'TS_2', 'Genap 2013/2014', '1106101078', 'B. RICARDO SAPUTRA', 'SI-34-02', 'FIX', '24-03-2014', 'YAP', 'MHY', 1),
(84, 'TS_2', 'Genap 2013/2014', '1106101093', 'BAGUS DWI SANDI PUTRA', 'SI-34-03', 'FIX', '24-03-2014', 'SNP', 'MHY', 1),
(85, 'TS_2', 'Ganjil 2013/2014', '1106090028', 'BAGYAS YURIAN NUGROHO', 'SI-33-02', 'FIX', '23-08-2013', 'WRP', 'MHY', 1),
(86, 'TS', 'Ganjil 2015/2016', '1106110103', 'BALA PUTRA DEWA', 'SI-35-03', 'FIX', '13-08-2015', 'MOW', 'MHY', 1),
(87, 'TS_2', 'Genap 2013/2014', '1106100017', 'BAMBANG DWI ASMORO', 'SI-34-01', 'FIX', '24-03-2014', 'UMY', 'MHY', 1),
(88, 'TS', 'Genap 2015/2016', '1106110078', 'BAYU PRADANA', 'SI-35-03', 'FIX', '31-12-2015', 'MOW', 'MHY', 1),
(89, 'TS_2', 'Genap 2013/2014', '1106100065', 'BERNADETTA ASTANIA HASUGIAN', 'SI-34-02', 'FIX', '24-03-2014', 'YAP', 'MHY', 1),
(90, 'TS_1', 'Genap 2014/2015', '1106110073', 'BILLA ANANDA SUWANDI', 'SI-35-02', 'FIX', '08-01-2015', 'TNA', 'MHY', 1),
(91, 'TS_2', 'Ganjil 2013/2014', '1106091053', 'BINAR CANDRA AUNI', 'SI-33-02', 'FIX', '23-08-2013', 'YAP', 'MHY', 1),
(92, 'TS_2', 'Ganjil 2013/2014', '1106081041', 'BOBBY IRFAN ANDI', 'SI-32-01', 'FIX', '24-08-2013', 'MHY', 'MHY', 1),
(93, 'TS', 'Genap 2015/2016', '1106122084', 'BRILIAN MERCRIZKY', 'SI-36-03', 'FIX', '30-12-2015', 'YAP', 'MHY', 1),
(94, 'TS_2', 'Ganjil 2013/2014', '1106092057', 'BUCHORI WAHYU H', 'SI-33-02', 'FIX', '26-08-2013', 'YAP', 'MHY', 1),
(95, 'TS', 'Genap 2015/2016', '1106122108', 'CAHYA NOFANDIYAN PUTRA', 'SI-36-04', 'FIX', '31-12-2015', 'SNP', 'MHY', 1),
(96, 'TS_2', 'Ganjil 2013/2014', '1106090040', 'CAKRA WARDHANA', 'SI-33-02', 'FIX', '24-08-2013', 'YAP', 'MHY', 1),
(97, 'TS_2', 'Ganjil 2013/2014', '1106090026', 'CHANAN', 'SI-33-02', 'FIX', '24-08-2013', 'WRP', 'MHY', 1),
(98, 'TS_2', 'Genap 2013/2014', '1106104112', 'CHANDRA PRATIWI', 'SI-34-03', 'FIX', '24-03-2014', 'SNP', 'MHY', 1),
(99, 'TS_1', 'Genap 2014/2015', '1106110040', 'CHARLIE SUGIARTO', 'SI-35-02', 'FIX', '07-01-2015', 'TNA', 'MHY', 1),
(100, 'TS', 'Genap 2015/2016', '1106120099', 'CHATUR WAHYU SYAHPUTRA', 'SI-36-04', 'FIX', '03-01-2016', 'SNP', 'MHY', 1),
(101, 'TS_1', 'Genap 2014/2015', '1106110029', 'CHINTAMY CHRISTINI', 'SI-35-01', 'FIX', '06-01-2015', 'AGU', 'MHY', 1),
(102, 'TS_2', 'Genap 2013/2014', '1106100048', 'CHRISHYA BUTI PAMA', 'SI-34-02', 'FIX', '24-03-2014', 'YAP', 'MHY', 1),
(103, 'TS_1', 'Ganjil 2014/2015', '1106100043', 'CHRISTINE PURNAMA SARI SIBARANI', 'SI-34-02', 'FIX', '19-08-2014', 'YAP', 'MHY', 1),
(104, 'TS_2', 'Genap 2013/2014', '1106102099', 'CHRISTOPHORUS DRYANTORO', 'SI-34-03', 'FIX', '24-03-2014', 'SNP', 'MHY', 1),
(105, 'TS', 'Genap 2015/2016', '1106110076', 'CIO BERRY LOVIARY TAMBOEN', 'SI-35-03', 'FIX', '03-01-2016', 'MOW', 'MHY', 1),
(106, 'TS', 'Genap 2015/2016', '1106120138', 'CITRA MELATI', 'SI-36-01', 'FIX', '30-12-2015', 'MTK', 'MHY', 1),
(107, 'TS', 'Genap 2015/2016', '1106120044', 'CLARISSA DILASARI SETIAWAN', 'SI-36-02', 'FIX', '30-12-2015', 'WRP', 'MHY', 1),
(108, 'TS_2', 'Ganjil 2013/2014', '1106090022', 'DANNY ARIWICAKSONO', 'SI-33-01', 'FIX', '24-08-2013', 'SNP', 'MHY', 1),
(109, 'TS', 'Ganjil 2015/2016', '1106110133', 'DATUK ARIEF VIKRY', 'SI-35-04', 'FIX', '05-08-2015', 'MTK', 'MHY', 1),
(110, 'TS', 'Ganjil 2015/2016', '1106110046', 'DAVIS RAPALA TANJUNG', 'SI-35-02', 'FIX', '05-08-2015', 'SFJ', 'MHY', 1),
(111, 'TS_1', 'Genap 2014/2015', '1106110001', 'DECKY RADITAMA MEGANTARA', 'SI-35-01', 'FIX', '12-01-2015', 'AGU', 'MHY', 1),
(112, 'TS_2', 'Ganjil 2013/2014', '1106090034', 'DEDDY APRIAN PRASESI', 'SI-33-02', 'FIX', '26-08-2013', 'WRP', 'MHY', 1),
(113, 'TS_1', 'Genap 2014/2015', '1106080013', 'DEDDY R GIRSANG', 'SI-32-02', 'FIX', '13-01-2015', 'MTK', 'MHY', 1),
(114, 'TS', 'Genap 2015/2016', '1106120015', 'DELIS SEPTIANTI BALGIS', 'SI-36-01', 'FIX', '04-01-2016', 'MTK', 'MHY', 1),
(115, 'TS', 'Genap 2015/2016', '1106120023', 'DELPHINE YUSTICIA RATNASARI', 'SI-36-01', 'FIX', '31-12-2015', 'MTK', 'MHY', 1),
(116, 'TS_2', 'Ganjil 2013/2014', '1106090025', 'DENANDRA PRADIPTA', 'SI-33-01', 'FIX', '23-08-2013', 'WRP', 'MHY', 1),
(117, 'TS_2', 'Genap 2013/2014', '1106100011', 'DESI BELLA P S', 'SI-34-01', 'FIX', '24-03-2014', 'UMY', 'MHY', 1),
(118, 'TS_1', 'Genap 2014/2015', '1106110136', 'DEWANSYAH ADI SAPUTRA', 'SI-35-04', 'FIX', '12-01-2015', 'MTK', 'MHY', 1),
(119, 'TS_1', 'Genap 2014/2015', '1106110081', 'DHANI RAHUTAMI PURWASTUTI', 'SI-35-03', 'FIX', '09-01-2015', 'MOW', 'MHY', 1),
(120, 'TS_1', 'Genap 2014/2015', '1106110093', 'DHIYA AFWAN TAUFIQ', 'SI-35-03', 'FIX', '06-01-2015', 'MOW', 'MHY', 1),
(121, 'TS', 'Genap 2015/2016', '1106120005', 'DIAN SUCI ASTUTI', 'SI-36-01', 'FIX', '30-12-2015', 'MTK', 'MHY', 1),
(122, 'TS_1', 'Genap 2014/2015', '1106110037', 'DIANA MEIRIANA SELVIANTI', 'SI-35-01', 'FIX', '09-01-2015', 'AGU', 'MHY', 1),
(123, 'TS', 'Ganjil 2015/2016', '1106110115', 'DIKA ANUGERAH PRATAMA', 'SI-35-04', 'FIX', '13-08-2015', 'MTK', 'MHY', 1),
(124, 'TS_2', 'Genap 2013/2014', '1106101084', 'DIMAS OCTAVIANTO WIBOWO', 'SI-34-03', 'FIX', '24-03-2014', 'SNP', 'MHY', 1),
(125, 'TS_2', 'Ganjil 2013/2014', '1106090019', 'DINA CATUR PUSPITASARI', 'SI-33-01', 'FIX', '27-08-2013', 'MHY', 'MHY', 1),
(126, 'TS_2', 'Genap 2013/2014', '1106101094', 'DINDA SEKAR PUTRI', 'SI-34-03', 'FIX', '24-03-2014', 'SNP', 'MHY', 1),
(127, 'TS', 'Genap 2015/2016', '1106120106', 'DINI SALSABILA DI KUSUMAH', 'SI-36-04', 'FIX', '30-12-2015', 'SNP', 'MHY', 1),
(128, 'TS', 'Genap 2015/2016', '1106120182', 'DINTA AYU SHAVIRA', 'SI-36-05', 'FIX', '04-01-2016', 'MOW', 'MHY', 1),
(129, 'TS', 'Genap 2015/2016', '1106120034', 'DUMAULI NOVITASARI BORU SIMANJUNTAK', 'SI-36-02', 'FIX', '30-12-2015', 'WRP', 'MHY', 1),
(130, 'TS_2', 'Ganjil 2013/2014', '1106081032', 'DWI CHONDRO KUSUMANINGPUTRI', 'SI-32-02', 'FIX', '25-08-2013', 'MHY', 'MHY', 1),
(131, 'TS_2', 'Genap 2013/2014', '1106100073', 'DWI ERNANI', 'SI-34-02', 'FIX', '24-03-2014', 'YAP', 'MHY', 1),
(132, 'TS', 'Genap 2015/2016', '1106120024', 'DWI PRATAMA', 'SI-36-01', 'FIX', '04-01-2016', 'MTK', 'MHY', 1),
(133, 'TS', 'Genap 2015/2016', '1106120172', 'DYAH WAHYUNINGTIAS ISWARI', 'SI-36-04', 'FIX', '01-01-2016', 'IFD', 'MHY', 1),
(134, 'TS_1', 'Genap 2014/2015', '1106111141', 'EKKY NOVRIZA ALAM', 'SI-35-04', 'FIX', '07-01-2015', 'MTK', 'MHY', 1),
(135, 'TS', 'Genap 2015/2016', '1106110092', 'ELANG MAULANA JAUHARI', 'SI-35-03', 'FIX', '31-12-2015', 'MOW', 'MHY', 1),
(136, 'TS', 'Genap 2015/2016', '1106124191', 'ELVIRA LAILATUTH THOHIROH', 'SI-36-02', 'FIX', '04-01-2016', 'WRP', 'MHY', 1),
(137, 'TS', 'Genap 2015/2016', '1106120136', 'ELZAR ESEN UULU', 'SI-36-03', 'FIX', '04-01-2016', 'YAP', 'MHY', 1),
(138, 'TS', 'Genap 2015/2016', '1106120100', 'EMA TRIA WAHYUNINGTIHAS', 'SI-36-04', 'FIX', '30-12-2015', 'SNP', 'MHY', 1),
(139, 'TS_2', 'Ganjil 2013/2014', '1106090013', 'ENDAH NIKY ESTU. R', 'SI-33-01', 'FIX', '23-08-2013', 'MHY', 'MHY', 1),
(140, 'TS_2', 'Ganjil 2013/2014', '1106081037', 'ENDURO RIVALDY', 'SI-32-01', 'FIX', '24-08-2013', 'MHY', 'MHY', 1),
(141, 'TS_2', 'Genap 2013/2014', '1106100060', 'ERIZA ERFIANA AMIR', 'SI-34-02', 'FIX', '24-03-2014', 'YAP', 'MHY', 1),
(142, 'TS', 'Genap 2015/2016', '1106120122', 'ERLA AYU PERTIWI', 'SI-36-05', 'FIX', '04-01-2016', 'MOW', 'MHY', 1),
(143, 'TS', 'Genap 2015/2016', '1106120075', 'ERLIN SUSILOWATI', 'SI-36-03', 'FIX', '30-12-2015', 'YAP', 'MHY', 1),
(144, 'TS', 'Genap 2015/2016', '1106120144', 'ERSA NOVIA FAJRIN', 'SI-36-01', 'FIX', '04-01-2016', 'MTK', 'MHY', 1),
(145, 'TS_1', 'Genap 2014/2015', '1106110044', 'EVA NOVIANTI', 'SI-35-02', 'FIX', '11-01-2015', 'TNA', 'MHY', 1),
(146, 'TS', 'Genap 2015/2016', '1106120073', 'EVINIA CANDRASARI', 'SI-36-03', 'FIX', '04-01-2016', 'YAP', 'MHY', 1),
(147, 'TS_1', 'Genap 2014/2015', '1106110105', 'FABIYOLA NINDYA SUSILO', 'SI-35-03', 'FIX', '10-01-2015', 'MOW', 'MHY', 1),
(148, 'TS_1', 'Genap 2014/2015', '1106110013', 'FACHRUL RAMZI PUTRA', 'SI-35-01', 'FIX', '11-01-2015', 'MTK', 'MHY', 1),
(149, 'TS_1', 'Genap 2014/2015', '1106110048', 'FADHIL MUHAMMAD', 'SI-35-02', 'FIX', '10-01-2015', 'TNA', 'MHY', 1),
(150, 'TS_1', 'Genap 2014/2015', '1106110039', 'FADLY MUHAMMAD', 'SI-35-02', 'FIX', '07-01-2015', 'TNA', 'MHY', 1),
(151, 'TS_2', 'Ganjil 2013/2014', '1106090015', 'FAHMIE YANARDI', 'SI-33-01', 'FIX', '23-08-2013', 'MHY', 'MHY', 1),
(152, 'TS', 'Genap 2015/2016', '1106120117', 'FAISAL RIZIQ AKBAR', 'SI-36-05', 'FIX', '04-01-2016', 'MOW', 'MHY', 1),
(153, 'TS', 'Genap 2015/2016', '1106120170', 'FAISAL WISNU PRADHANA', 'SI-36-04', 'FIX', '04-01-2016', 'SNP', 'MHY', 1),
(154, 'TS', 'Genap 2015/2016', '1106120173', 'FAJAR MUKHLISIN', 'SI-36-04', 'FIX', '01-01-2016', 'SNP', 'MHY', 1),
(155, 'TS_1', 'Genap 2014/2015', '1106110049', 'FAJRI ARFAN', 'SI-35-02', 'FIX', '12-01-2015', 'TNA', 'MHY', 1),
(156, 'TS_1', 'Genap 2014/2015', '1106110014', 'FAJRI GUSTIA NANDA', 'SI-35-01', 'FIX', '06-01-2015', 'AGU', 'MHY', 1),
(157, 'TS_1', 'Genap 2014/2015', '1106110125', 'FAJRUL ALFIAN', 'SI-35-04', 'FIX', '12-01-2015', 'MTK', 'MHY', 1),
(158, 'TS_1', 'Genap 2014/2015', '1106110129', 'FAMILA FARADIBA', 'SI-35-04', 'FIX', '07-01-2015', 'MTK', 'MHY', 1),
(159, 'TS_2', 'Ganjil 2013/2014', '1106100028', 'FANNY FATHYA NURUL FATIMAH', 'SI-34-01', 'FIX', '23-08-2013', 'UMY', 'MHY', 1),
(160, 'TS_2', 'Genap 2013/2014', '1106101079', 'FANTRI R SUHARJA', 'SI-34-02', 'FIX', '24-03-2014', 'YAP', 'MHY', 1),
(161, 'TS_1', 'Genap 2014/2015', '1106110016', 'FAQIH PUTRA KUSUMAWIJAYA', 'SI-35-01', 'FIX', '06-01-2015', 'AGU', 'MHY', 1),
(162, 'TS_2', 'Genap 2013/2014', '1106100058', 'Farianti Ika Putri', 'SI-34-02', 'FIX', '24-03-2014', 'YAP', 'MHY', 1),
(163, 'TS', 'Genap 2015/2016', '1106120169', 'FARID HAKIM NISWANSYAH', 'SI-36-04', 'FIX', '02-01-2016', 'SNP', 'MHY', 1),
(164, 'TS_2', 'Genap 2013/2014', '1106101083', 'FARIZ ABDUSSALAM', 'SI-34-03', 'FIX', '24-03-2014', 'SNP', 'MHY', 1),
(165, 'TS_1', 'Genap 2014/2015', '1106110054', 'FATHIMAH MUTHI LUTHFIYAH', 'SI-35-02', 'FIX', '12-01-2015', 'TNA', 'MHY', 1),
(166, 'TS_1', 'Genap 2014/2015', '1106110062', 'FATHIYYAH NUR AZIZAH', 'SI-35-02', 'FIX', '07-01-2015', 'TNA', 'MHY', 1),
(167, 'TS_1', 'Genap 2014/2015', '1106110047', 'FAUZA ANNISA', 'SI-35-02', 'FIX', '12-01-2015', 'TNA', 'MHY', 1),
(168, 'TS', 'Genap 2015/2016', '1106124192', 'FAUZAN MEDISON ADRYAN', 'SI-36-02', 'FIX', '30-12-2015', 'WRP', 'MHY', 1),
(169, 'TS_2', 'Genap 2013/2014', '1106100037', 'FAUZIAH WILDAN NURUL H', 'SI-34-01', 'FIX', '24-03-2014', 'UMY', 'MHY', 1),
(170, 'TS_2', 'Ganjil 2013/2014', '1106100012', 'Femi Yumna Anjani', 'SI-34-01', 'FIX', '23-08-2013', 'UMY', 'MHY', 1),
(171, 'TS', 'Genap 2015/2016', '1106110130', 'FERDIAN PRATAMA NUGRAHA', 'SI-35-04', 'FIX', '01-01-2016', 'MTK', 'MHY', 1),
(172, 'TS_1', 'Ganjil 2014/2015', '1106090038', 'FERDY FERNANDO SIBUEA', 'SI-33-02', 'FIX', '19-08-2014', 'WRP', 'MHY', 1),
(173, 'TS', 'Genap 2015/2016', '1106120030', 'FHATYA ANDINI', 'SI-36-02', 'FIX', '01-01-2016', 'WRP', 'MHY', 1),
(174, 'TS', 'Genap 2015/2016', '1106120008', 'FIEGA DWI NOVWARI', 'SI-36-01', 'FIX', '04-01-2016', 'MTK', 'MHY', 1),
(175, 'TS', 'Genap 2015/2016', '1106122135', 'FIKROTUN NADIYYA', 'SI-36-05', 'FIX', '01-01-2016', 'MOW', 'MHY', 1),
(176, 'TS_1', 'Ganjil 2014/2015', '1106100051', 'FIRDAUS SETYA PRATAMA', 'SI-34-02', 'FIX', '20-08-2014', 'YAP', 'MHY', 1),
(177, 'TS', 'Genap 2015/2016', '1106120016', 'FIRDAYAKUSMAWARNI', 'SI-36-01', 'FIX', '31-12-2015', 'MTK', 'MHY', 1),
(178, 'TS_1', 'Genap 2014/2015', '1106110012', 'FITASARI WIHARNI', 'SI-35-01', 'FIX', '07-01-2015', 'AGU', 'MHY', 1),
(179, 'TS_1', 'Genap 2014/2015', '1106110138', 'FITRIA', 'SI-35-04', 'FIX', '07-01-2015', 'MTK', 'MHY', 1),
(180, 'TS_1', 'Genap 2014/2015', '1106110018', 'FRANSISKA', 'SI-35-01', 'FIX', '07-01-2015', 'AGU', 'MHY', 1),
(181, 'TS_2', 'Genap 2013/2014', '1106100066', 'FUADI HILMAN', 'SI-34-02', 'FIX', '24-03-2014', 'YAP', 'MHY', 1),
(182, 'TS_2', 'Genap 2013/2014', '1106100032', 'FURNAWAN', 'SI-34-01', 'FIX', '24-03-2014', 'UMY', 'MHY', 1),
(183, 'TS', 'Genap 2015/2016', '1106120159', 'GALANG TANAH MERDEKA', 'SI-36-02', 'FIX', '31-12-2015', 'WRP', 'MHY', 1),
(184, 'TS_1', 'Genap 2014/2015', '1106110123', 'GALIH FATHONI', 'SI-35-04', 'FIX', '09-01-2015', 'MTK', 'MHY', 1),
(185, 'TS_2', 'Ganjil 2013/2014', '1106090031', 'GALIH R WIDAGDO', 'SI-33-02', 'FIX', '23-08-2013', 'WRP', 'MHY', 1),
(186, 'TS_1', 'Genap 2014/2015', '1106110034', 'GANANG AFIF RIJAZIM', 'SI-35-01', 'FIX', '12-01-2015', 'AGU', 'MHY', 1),
(187, 'TS_2', 'Ganjil 2013/2014', '1106090027', 'GARDHA RESPATI', 'SI-33-02', 'FIX', '25-08-2013', 'WRP', 'MHY', 1),
(188, 'TS', 'Genap 2015/2016', '1106110122', 'GHANI ABDUL MALIK', 'SI-35-04', 'FIX', '08-03-2016', 'MTK', 'MHY', 1),
(189, 'TS_1', 'Genap 2014/2015', '1106111140', 'GHOZIYAH HAITAN RACHMAN', 'SI-35-04', 'FIX', '06-01-2015', 'MTK', 'MHY', 1),
(190, 'TS_2', 'Genap 2013/2014', '1106104109', 'GILANG RAMADHAN AJIE SANTOSA', 'SI-34-03', 'FIX', '24-03-2014', 'SNP', 'MHY', 1),
(191, 'TS', 'Genap 2015/2016', '1106120047', 'GINA MARIAM GUSTINA', 'SI-36-02', 'FIX', '31-12-2015', 'WRP', 'MHY', 1),
(192, 'TS_2', 'Ganjil 2013/2014', '1106090041', 'GRACE LAN RIZQI', 'SI-33-02', 'FIX', '26-08-2013', 'YAP', 'MHY', 1),
(193, 'TS_1', 'Genap 2014/2015', '1106110068', 'GREGORIUS PRAHASWARA DEWANTA', 'SI-35-02', 'FIX', '11-01-2015', 'TNA', 'MHY', 1),
(194, 'TS_2', 'Genap 2013/2014', '1106100053', 'GRIYATRIE GIVANIE', 'SI-34-02', 'FIX', '24-03-2014', 'YAP', 'MHY', 1),
(195, 'TS_2', 'Genap 2013/2014', '1106100008', 'HAFIDH RASHEMI RAFSANJANY', 'SI-34-01', 'FIX', '24-03-2014', 'UMY', 'MHY', 1),
(196, 'TS_2', 'Ganjil 2013/2014', '1106092058', 'HAFIDZ KURNIADI', 'SI-33-02', 'FIX', '23-08-2013', 'YAP', 'MHY', 1),
(197, 'TS', 'Genap 2015/2016', '1106120121', 'HANADIA OLENDRA', 'SI-36-05', 'FIX', '30-12-2015', 'MOW', 'MHY', 1),
(198, 'TS_2', 'Ganjil 2013/2014', '1106091052', 'HANAFIANTHO', 'SI-33-02', 'FIX', '26-08-2013', 'YAP', 'MHY', 1),
(199, 'TS', 'Genap 2015/2016', '1106120009', 'HANIF FAJRI', 'SI-36-01', 'FIX', '04-01-2016', 'MTK', 'MHY', 1),
(200, 'TS_2', 'Genap 2013/2014', '1106100067', 'HANIFAH DESMITA AZWAR', 'SI-34-02', 'FIX', '24-03-2014', 'YAP', 'MHY', 1),
(201, 'TS_2', 'Ganjil 2013/2014', '1106080005', 'HARIYANTO', 'SI-32-01', 'FIX', '25-08-2013', 'MTK', 'MHY', 1),
(202, 'TS_1', 'Genap 2014/2015', '1106110099', 'HARPANDI WIBOWO', 'SI-35-03', 'FIX', '07-01-2015', 'MOW', 'MHY', 1),
(203, 'TS', 'Genap 2015/2016', '1106120094', 'HENDRIK HENDRIANA FIRMANSYAH', 'SI-36-04', 'FIX', '01-01-2016', 'SNP', 'MHY', 1),
(204, 'TS_2', 'Ganjil 2013/2014', '1106080018', 'HENDY WISNU ARINDRA', 'SI-32-02', 'FIX', '25-08-2013', 'RIZ', 'MHY', 1),
(205, 'TS_2', 'Genap 2013/2014', '1106104103', 'HERDIANTO', 'SI-34-03', 'FIX', '24-03-2014', 'SNP', 'MHY', 1),
(206, 'TS_1', 'Genap 2014/2015', '1106110055', 'HIZRIAN', 'SI-35-02', 'FIX', '09-01-2015', 'SFJ', 'MHY', 1),
(207, 'TS_1', 'Genap 2014/2015', '1106110075', 'HOSIANA ARISKA SILALAHI', 'SI-35-03', 'FIX', '12-01-2015', 'MOW', 'MHY', 1),
(208, 'TS_2', 'Ganjil 2013/2014', '1106081034', 'HUDHI GUMILAR', 'SI-32-01', 'FIX', '25-08-2013', 'MHY', 'MHY', 1),
(209, 'TS_2', 'Genap 2013/2014', '1106100023', 'HUSNUR RIDHA SYAFNI', 'SI-34-01', 'FIX', '24-03-2014', 'UMY', 'MHY', 1),
(210, 'TS_1', 'Genap 2014/2015', '1106114148', 'I GEDE ISWARA DARMAWAN', 'SI-35-04', 'FIX', '07-01-2015', 'MTK', 'MHY', 1),
(211, 'TS_1', 'Genap 2014/2015', '1106111139', 'I GEDE MINDRAYASA', 'SI-35-04', 'FIX', '06-01-2015', 'MTK', 'MHY', 1),
(212, 'TS_2', 'Genap 2013/2014', '1106100074', 'I GNP ARDITYA YOGA MAHENDRA', 'SI-34-02', 'FIX', '24-03-2014', 'YAP', 'MHY', 1),
(213, 'TS_2', 'Ganjil 2013/2014', '1106090029', 'I GUSTI NGURAH BAYU', 'SI-33-02', 'FIX', '23-08-2013', 'WRP', 'MHY', 1),
(214, 'TS_1', 'Genap 2014/2015', '1106110071', 'I KETUT ADI PUTRA PRANANTA', 'SI-35-02', 'FIX', '09-01-2015', 'TNA', 'MHY', 1),
(215, 'TS', 'Genap 2015/2016', '1106120068', 'I KOMANG JAKA AKSARA WIGUNA', 'SI-36-03', 'FIX', '01-01-2016', 'YAP', 'MHY', 1),
(216, 'TS_1', 'Genap 2014/2015', '1106110128', 'I MADE ADI JAYANTIKA', 'SI-35-04', 'FIX', '07-01-2015', 'MTK', 'MHY', 1),
(217, 'TS', 'Genap 2015/2016', '1106120029', 'I MADE GELGEL WESNAWA PUTRA', 'SI-36-02', 'FIX', '02-01-2016', 'WRP', 'MHY', 1),
(218, 'TS_1', 'Genap 2014/2015', '1106114143', 'I MADE PRAWIRA INDRAWAN', 'SI-35-04', 'FIX', '06-01-2015', 'MTK', 'MHY', 1),
(219, 'TS', 'Genap 2015/2016', '1106120020', 'I MADE TEKAD KALIMANTARA', 'SI-36-01', 'FIX', '31-12-2015', 'MTK', 'MHY', 1),
(220, 'TS_2', 'Genap 2013/2014', '1106104114', 'I WAYAN HENDRA PERMADI', 'SI-34-03', 'FIX', '24-03-2014', 'SNP', 'MHY', 1),
(221, 'TS', 'Genap 2015/2016', '1106120107', 'IAN FAHMI NUGRAHA', 'SI-36-04', 'FIX', '01-01-2016', 'SNP', 'MHY', 1),
(222, 'TS', 'Genap 2015/2016', '1106120018', 'IBRAHIM HANIF ALKHALIL', 'SI-36-01', 'FIX', '02-01-2016', 'MTK', 'MHY', 1),
(223, 'TS', 'Genap 2015/2016', '1106120104', 'IBRAHIM MA''ANI', 'SI-36-04', 'FIX', '03-01-2016', 'IFD', 'MHY', 1),
(224, 'TS', 'Genap 2015/2016', '1106120180', 'ICHSAN AKBAR', 'SI-36-05', 'FIX', '30-12-2015', 'MOW', 'MHY', 1),
(225, 'TS', 'Genap 2015/2016', '1106120130', 'IDA BAGUS GEDE ROMA HARSANA PUTRA', 'SI-36-05', 'FIX', '31-12-2015', 'MOW', 'MHY', 1),
(226, 'TS_1', 'Genap 2014/2015', '1106110043', 'IDA BAGUS KRISNA WEDANTA PRASADA', 'SI-35-02', 'FIX', '08-01-2015', 'TNA', 'MHY', 1),
(227, 'TS_2', 'Ganjil 2013/2014', '1106080025', 'IKA NURMILA', 'SI-32-02', 'FIX', '26-08-2013', 'RIZ', 'MHY', 1),
(228, 'TS_1', 'Genap 2014/2015', '1106110030', 'IKHSAN YUDHA PRADANA', 'SI-35-01', 'FIX', '07-01-2015', 'AGU', 'MHY', 1),
(229, 'TS', 'Genap 2015/2016', '1106110080', 'ILHAM MEGA DWIJAYA', 'SI-35-03', 'FIX', '01-01-2016', 'MOW', 'MHY', 1),
(230, 'TS', 'Genap 2015/2016', '1106120032', 'INAS NISRINA', 'SI-36-02', 'FIX', '30-12-2015', 'WRP', 'MHY', 1),
(231, 'TS', 'Genap 2015/2016', '1106120101', 'INAYATUL MAGHFIROH', 'SI-36-04', 'FIX', '30-12-2015', 'SNP', 'MHY', 1),
(232, 'TS_2', 'Genap 2013/2014', '1106104106', 'INDARYANI KUMALA WARDANI', 'SI-34-03', 'FIX', '24-03-2014', 'SNP', 'MHY', 1),
(233, 'TS', 'Genap 2015/2016', '1106120124', 'INDRA ROLANDO SINAGA', 'SI-36-05', 'FIX', '04-01-2016', 'MOW', 'MHY', 1),
(234, 'TS_1', 'Genap 2014/2015', '1106110120', 'INEZ SEKARAYU NAWANGWULAN', 'SI-35-04', 'FIX', '09-01-2015', 'MTK', 'MHY', 1),
(235, 'TS_1', 'Genap 2014/2015', '1106110088', 'INSAN HARISH', 'SI-35-03', 'FIX', '06-01-2015', 'MOW', 'MHY', 1),
(236, 'TS', 'Genap 2015/2016', '1106124205', 'INTAN DWI ARIESTA PUTRI', 'SI-36-05', 'FIX', '30-12-2015', 'MOW', 'MHY', 1),
(237, 'TS', 'Genap 2015/2016', '1106120006', 'IQBAL NUSA', 'SI-36-01', 'FIX', '02-01-2016', 'MTK', 'MHY', 1),
(238, 'TS_1', 'Genap 2014/2015', '1106110011', 'IRENA ARSYKA DEWI', 'SI-35-01', 'FIX', '06-01-2015', 'AGU', 'MHY', 1),
(239, 'TS', 'Genap 2015/2016', '1106124190', 'IRFANSYA UTAMA', 'SI-36-01', 'FIX', '30-12-2015', 'MTK', 'MHY', 1),
(240, 'TS_1', 'Genap 2014/2015', '1106114145', 'IRMA ANGRAEINI', 'SI-35-04', 'FIX', '07-01-2015', 'MTK', 'MHY', 1),
(241, 'TS', 'Ganjil 2015/2016', '1106110086', 'IRVAN GUNAWAN', 'SI-35-03', 'FIX', '06-08-2015', 'MOW', 'MHY', 1),
(242, 'TS_2', 'Genap 2013/2014', '1106101088', 'IZHARYAN IQBAL', 'SI-34-03', 'FIX', '24-03-2014', 'SNP', 'MHY', 1),
(243, 'TS_2', 'Genap 2013/2014', '1106104115', 'IZKAWATI FAUHIDAH', 'SI-34-03', 'FIX', '24-03-2014', 'SNP', 'MHY', 1),
(244, 'TS', 'Genap 2015/2016', '1106120078', 'JAN FANDRO SIAHAAN', 'SI-36-03', 'FIX', '30-12-2015', 'YAP', 'MHY', 1),
(245, 'TS_2', 'Genap 2013/2014', '1106101087', 'JERRYSTAMA ABIPRADJA', 'SI-34-03', 'FIX', '24-03-2014', 'SNP', 'MHY', 1),
(246, 'TS_2', 'Ganjil 2013/2014', '1106090014', 'JODDY I R', 'SI-33-01', 'FIX', '27-08-2013', 'MHY', 'MHY', 1),
(247, 'TS', 'Genap 2015/2016', '1106120011', 'JOHAN INDRA SUKMANA', 'SI-36-01', 'FIX', '31-12-2015', 'MTK', 'MHY', 1),
(248, 'TS_1', 'Genap 2014/2015', '1106110107', 'JOSES ADELWIN SITEPU', 'SI-35-03', 'FIX', '06-01-2015', 'MOW', 'MHY', 1),
(249, 'TS_1', 'Genap 2014/2015', '1106100025', 'JOSUA KRISTIAN SITINJAK', 'SI-34-01', 'FIX', '07-01-2015', 'UMY', 'MHY', 1),
(250, 'TS', 'Genap 2015/2016', '1106120067', 'KARNANDO NABABAN', 'SI-36-03', 'FIX', '31-12-2015', 'YAP', 'MHY', 1),
(251, 'TS_2', 'Ganjil 2013/2014', '1106090044', 'KARTIKA NUR O', 'SI-33-02', 'FIX', '23-08-2013', 'YAP', 'MHY', 1),
(252, 'TS_2', 'Genap 2013/2014', '1106104113', 'KETUT GEDE SURYA ATMAJA', 'SI-34-03', 'FIX', '24-03-2014', 'SNP', 'MHY', 1),
(253, 'TS', 'Genap 2015/2016', '1106120110', 'KEVIN ROHNI GOKLAS SINAGA', 'SI-36-05', 'FIX', '03-01-2016', 'MOW', 'MHY', 1),
(254, 'TS', 'Genap 2015/2016', '1106120098', 'KHAIRUL AKBAR', 'SI-36-04', 'FIX', '04-01-2016', 'IFD', 'MHY', 1),
(255, 'TS_2', 'Ganjil 2013/2014', '1106080004', 'KHOLIFAH GINA NURYANI', 'SI-32-01', 'FIX', '24-08-2013', 'IMR', 'MHY', 1),
(256, 'TS_2', 'Genap 2013/2014', '1106100069', 'KHOLIFATUL UMMAH', 'SI-34-02', 'FIX', '24-03-2014', 'YAP', 'MHY', 1),
(257, 'TS_1', 'Genap 2014/2015', '1106110025', 'KOMANG ADITYA RESPA PUTRA', 'SI-35-01', 'FIX', '12-01-2015', 'AGU', 'MHY', 1),
(258, 'TS_1', 'Genap 2014/2015', '1106110058', 'KOMANG INDAH DESINTHYA WATI', 'SI-35-02', 'FIX', '06-01-2015', 'TNA', 'MHY', 1),
(259, 'TS_2', 'Ganjil 2013/2014', '1106081030', 'KUKUH GINANJAR S', 'SI-32-02', 'FIX', '26-08-2013', 'RIZ', 'MHY', 1),
(260, 'TS_2', 'Genap 2013/2014', '1106104104', 'KURNIA MUSTIKA SARI', 'SI-34-03', 'FIX', '24-03-2014', 'SNP', 'MHY', 1),
(261, 'TS_2', 'Ganjil 2013/2014', '1106080024', 'LANA C. HAKIM', 'SI-32-02', 'FIX', '26-08-2013', 'RIZ', 'MHY', 1),
(262, 'TS', 'Ganjil 2015/2016', '1106110084', 'LIA AVITA SARI SIREGAR', 'SI-35-03', 'FIX', '05-08-2015', 'MOW', 'MHY', 1),
(263, 'TS_1', 'Genap 2014/2015', '1106110135', 'LIFFI NOFERIANTI', 'SI-35-04', 'FIX', '12-01-2015', 'MTK', 'MHY', 1),
(264, 'TS_2', 'Genap 2013/2014', '1106101080', 'LILIAN RAMADHANI PUTRI', 'SI-34-02', 'FIX', '24-03-2014', 'YAP', 'MHY', 1),
(265, 'TS', 'Genap 2015/2016', '1106112065', 'LUCKY SURYA PERMADI', 'SI-35-02', 'FIX', '31-12-2015', 'SFJ', 'MHY', 1),
(266, 'TS', 'Genap 2015/2016', '1106121189', 'LUKI AISHA KUSUMA WARDANI', 'SI-36-05', 'FIX', '30-12-2015', 'MOW', 'MHY', 1),
(267, 'TS_2', 'Ganjil 2013/2014', '1106090021', 'M FATHINUDDIN', 'SI-33-01', 'FIX', '23-08-2013', 'MHY', 'MHY', 1),
(268, 'TS_2', 'Ganjil 2013/2014', '1106080012', 'M IHSANUDDIN H', 'SI-32-02', 'FIX', '24-08-2013', 'IMR', 'MHY', 1),
(269, 'TS_2', 'Ganjil 2013/2014', '1106090005', 'M RIZKI MOZAR KADEAN', 'SI-33-01', 'FIX', '23-08-2013', 'MHY', 'MHY', 1),
(270, 'TS', 'Ganjil 2015/2016', '1106110127', 'M. DENIS SYAHPUTRA NASUTION', 'SI-35-04', 'FIX', '14-08-2015', 'MTK', 'MHY', 1),
(271, 'TS', 'Genap 2015/2016', '1106122027', 'M. FIRMAN HELMI ARIYANSYAH', 'SI-36-01', 'FIX', '04-01-2016', 'MTK', 'MHY', 1),
(272, 'TS_2', 'Ganjil 2013/2014', '1106091051', 'M.KAMAL IKMAL', 'SI-33-02', 'FIX', '24-08-2013', 'YAP', 'MHY', 1),
(273, 'TS_1', 'Genap 2014/2015', '1106110056', 'MADE FEBRIYANA DYASTAMA PUTRA', 'SI-35-02', 'FIX', '06-01-2015', 'TNA', 'MHY', 1),
(274, 'TS_1', 'Genap 2014/2015', '1106110045', 'MAHDY ARIEF', 'SI-35-02', 'FIX', '10-01-2015', 'TNA', 'MHY', 1),
(275, 'TS_2', 'Genap 2013/2014', '1106101090', 'MARCHELLA DARA PUSPITA', 'SI-34-03', 'FIX', '24-03-2014', 'SNP', 'MHY', 1),
(276, 'TS_1', 'Genap 2014/2015', '1106110106', 'MARTHA OKRINA', 'SI-35-03', 'FIX', '07-01-2015', 'MOW', 'MHY', 1),
(277, 'TS', 'Genap 2015/2016', '1106120091', 'MASANGGA FERBIYANA MUKTI', 'SI-36-04', 'FIX', '04-01-2016', 'SNP', 'MHY', 1),
(278, 'TS_2', 'Genap 2013/2014', '1106100029', 'MAULANA ASSIDQI', 'SI-34-01', 'FIX', '24-03-2014', 'UMY', 'MHY', 1),
(279, 'TS', 'Genap 2015/2016', '1106120081', 'MAWADDAH TIFANI', 'SI-36-03', 'FIX', '02-01-2016', 'YAP', 'MHY', 1),
(280, 'TS_1', 'Genap 2014/2015', '1106110117', 'MEIRIZKY ANJANI PURWATI NINGSIH', 'SI-35-04', 'FIX', '12-01-2015', 'MTK', 'MHY', 1),
(281, 'TS_2', 'Ganjil 2013/2014', '1106080007', 'MELRANDY ARDHILLA F', 'SI-32-01', 'FIX', '27-08-2013', 'IMR', 'MHY', 1),
(282, 'TS_1', 'Genap 2014/2015', '1106110057', 'MIA MEILANI', 'SI-35-02', 'FIX', '12-01-2015', 'TNA', 'MHY', 1),
(283, 'TS_1', 'Genap 2014/2015', '1106110008', 'MIFTA AZIZ', 'SI-35-01', 'FIX', '08-01-2015', 'AGU', 'MHY', 1),
(284, 'TS_2', 'Genap 2013/2014', '1106101085', 'MOCHAMAD AKIF', 'SI-34-03', 'FIX', '24-03-2014', 'SNP', 'MHY', 1),
(285, 'TS', 'Genap 2015/2016', '1106110104', 'MOCHAMAD ANNAFIE YANUAR HAKIM', 'SI-35-03', 'FIX', '03-01-2016', 'MOW', 'MHY', 1),
(286, 'TS', 'Genap 2015/2016', '1106120069', 'MOCHAMAD HAFIZ KURNIAWAN', 'SI-36-03', 'FIX', '04-01-2016', 'YAP', 'MHY', 1),
(287, 'TS_2', 'Ganjil 2013/2014', '1106090035', 'MOCHAMAD REZA INDRA', 'SI-33-02', 'FIX', '23-08-2013', 'WRP', 'MHY', 1),
(288, 'TS', 'Genap 2015/2016', '1106120083', 'MOCHAMAD THARIQ JANUAR', 'SI-36-03', 'FIX', '04-01-2016', 'YAP', 'MHY', 1),
(289, 'TS_2', 'Genap 2013/2014', '1106100018', 'MOH FAHRUR RIZQON', 'SI-34-01', 'FIX', '24-03-2014', 'UMY', 'MHY', 1),
(290, 'TS_1', 'Genap 2014/2015', '1106110017', 'MOHAMAD IQBAL', 'SI-35-01', 'FIX', '12-01-2015', 'AGU', 'MHY', 1),
(291, 'TS_1', 'Genap 2014/2015', '1106110019', 'MOHD RHEDO MARGEN', 'SI-35-01', 'FIX', '09-01-2015', 'AGU', 'MHY', 1),
(292, 'TS_2', 'Genap 2013/2014', '1106101091', 'MONICA N P', 'SI-34-03', 'FIX', '24-03-2014', 'SNP', 'MHY', 1),
(293, 'TS', 'Genap 2015/2016', '1106120057', 'MUCHAMAD REZA JULIANSYAH', 'SI-36-03', 'FIX', '30-12-2015', 'YAP', 'MHY', 1),
(294, 'TS_2', 'Genap 2013/2014', '1106101095', 'MUHAMAD ARI SADEWO', 'SI-34-03', 'FIX', '24-03-2014', 'SNP', 'MHY', 1),
(295, 'TS_2', 'Genap 2013/2014', '1106102101', 'MUHAMAD MULYA FUADI AGISNA', 'SI-34-03', 'FIX', '24-03-2014', 'SNP', 'MHY', 1),
(296, 'TS_1', 'Genap 2014/2015', '1106110072', 'MUHAMAD RIZKY VIDHIAN', 'SI-35-02', 'FIX', '11-01-2015', 'SFJ', 'MHY', 1),
(297, 'TS_2', 'Ganjil 2013/2014', '1106080022', 'MUHAMAD TOSHIO', 'SI-32-02', 'FIX', '24-08-2013', 'RIZ', 'MHY', 1),
(298, 'TS_1', 'Genap 2014/2015', '1106110006', 'MUHAMMAD AKBAR SATRIA', 'SI-35-01', 'FIX', '11-01-2015', 'AGU', 'MHY', 1),
(299, 'TS_2', 'Genap 2013/2014', '1106100059', 'MUHAMMAD AKFI RIZQUN AJI', 'SI-34-02', 'FIX', '24-03-2014', 'YAP', 'MHY', 1),
(300, 'TS', 'Genap 2015/2016', '1106120080', 'MUHAMMAD AMMAR RIFQI', 'SI-36-03', 'FIX', '04-01-2016', 'YAP', 'MHY', 1),
(301, 'TS_1', 'Genap 2014/2015', '1106110131', 'MUHAMMAD AULIA RENDY', 'SI-35-04', 'FIX', '07-01-2015', 'MTK', 'MHY', 1),
(302, 'TS_1', 'Genap 2014/2015', '1106110050', 'MUHAMMAD EDWIN BAIHAQI', 'SI-35-02', 'FIX', '09-01-2015', 'SFJ', 'MHY', 1),
(303, 'TS', 'Genap 2015/2016', '1106120033', 'MUHAMMAD FACHRY PUTERA PRATAMA', 'SI-36-02', 'FIX', '30-12-2015', 'WRP', 'MHY', 1),
(304, 'TS_2', 'Genap 2013/2014', '1106100030', 'MUHAMMAD FAJRI', 'SI-34-01', 'FIX', '24-03-2014', 'UMY', 'MHY', 1),
(305, 'TS_1', 'Ganjil 2014/2015', '1106101086', 'MUHAMMAD FIKRI', 'SI-34-03', 'FIX', '20-08-2014', 'SNP', 'MHY', 1),
(306, 'TS', 'Genap 2015/2016', '1106120039', 'MUHAMMAD HILHAM RAMADHAN', 'SI-36-02', 'FIX', '04-01-2016', 'WRP', 'MHY', 1),
(307, 'TS_2', 'Ganjil 2013/2014', '1106080019', 'MUHAMMAD IQBAL IDRUS', 'SI-32-02', 'FIX', '24-08-2013', 'RIZ', 'MHY', 1),
(308, 'TS', 'Genap 2015/2016', '1106120066', 'MUHAMMAD MUFID LUTHFI', 'SI-36-03', 'FIX', '31-12-2015', 'YAP', 'MHY', 1),
(309, 'TS_2', 'Genap 2013/2014', '1106101076', 'MUHAMMAD NUR', 'SI-34-02', 'FIX', '24-03-2014', 'YAP', 'MHY', 1),
(310, 'TS_1', 'Genap 2014/2015', '1106110101', 'MUHAMMAD SYAIFUL RAMADHAN', 'SI-35-03', 'FIX', '08-01-2015', 'MOW', 'MHY', 1),
(311, 'TS_2', 'Ganjil 2013/2014', '1106090039', 'MUHAMMAD THOFHAN HANNANTO', 'SI-33-02', 'FIX', '25-08-2013', 'WRP', 'MHY', 1),
(312, 'TS_2', 'Genap 2013/2014', '1106100063', 'MUHAMMAD WALIYYUDIN AFDHALUL IHSAN', 'SI-34-02', 'FIX', '24-03-2014', 'YAP', 'MHY', 1),
(313, 'TS', 'Genap 2015/2016', '1106120093', 'MUKHAMAD DANIEL IRIANTO', 'SI-36-04', 'FIX', '04-01-2016', 'IFD', 'MHY', 1),
(314, 'TS_2', 'Genap 2013/2014', '1106100050', 'MUKHAMMAD DZIKRI FALAKH', 'SI-34-02', 'FIX', '24-03-2014', 'YAP', 'MHY', 1),
(315, 'TS', 'Genap 2015/2016', '1106120003', 'MUKHLIS ANUGRAH PRATAMA', 'SI-36-01', 'FIX', '30-12-2015', 'MTK', 'MHY', 1),
(316, 'TS', 'Genap 2015/2016', '1106120137', 'MUTIA DEWI KURNIASIH', 'SI-36-01', 'FIX', '04-01-2016', 'MTK', 'MHY', 1),
(317, 'TS_1', 'Genap 2014/2015', '1106110074', 'NADILA LINTANG HAPSARI', 'SI-35-03', 'FIX', '12-01-2015', 'MOW', 'MHY', 1),
(318, 'TS', 'Genap 2015/2016', '1106120064', 'NANA RAMADHEWI', 'SI-36-03', 'FIX', '04-01-2016', 'YAP', 'MHY', 1),
(319, 'TS_2', 'Genap 2013/2014', '1106104108', 'NATALIA IRAWATI S', 'SI-34-03', 'FIX', '24-03-2014', 'SNP', 'MHY', 1),
(320, 'TS', 'Genap 2015/2016', '1106120060', 'NAUFAL AFRA FIRDAUS', 'SI-36-03', 'FIX', '04-01-2016', 'YAP', 'MHY', 1),
(321, 'TS', 'Genap 2015/2016', '1106120087', 'NAZWAR SYAMSU', 'SI-36-04', 'FIX', '04-01-2016', 'SNP', 'MHY', 1),
(322, 'TS_1', 'Genap 2014/2015', '1106110126', 'NEVISIA PUSPA AYUDHANA', 'SI-35-04', 'FIX', '10-01-2015', 'MTK', 'MHY', 1),
(323, 'TS_2', 'Ganjil 2013/2014', '1106080028', 'NIA HAPSARI PUTRI', 'SI-32-02', 'FIX', '24-08-2013', 'RIZ', 'MHY', 1),
(324, 'TS_2', 'Ganjil 2013/2014', '1106081033', 'NIKE ENDAH SUSANTI', 'SI-32-02', 'FIX', '24-08-2013', 'MHY', 'MHY', 1),
(325, 'TS_1', 'Genap 2014/2015', '1106110082', 'NILASARI NURAMANATI', 'SI-35-03', 'FIX', '06-01-2015', 'MOW', 'MHY', 1),
(326, 'TS_2', 'Genap 2013/2014', '1106100071', 'NINA TANTYABUDI', 'SI-34-02', 'FIX', '24-03-2014', 'YAP', 'MHY', 1),
(327, 'TS_2', 'Ganjil 2013/2014', '1106090008', 'NOOR KATIKAH SEJATI', 'SI-33-01', 'FIX', '24-08-2013', 'MHY', 'MHY', 1),
(328, 'TS_2', 'Genap 2013/2014', '1106104105', 'NUNUNG NOER HIDAYATI', 'SI-34-03', 'FIX', '24-03-2014', 'SNP', 'MHY', 1),
(329, 'TS_2', 'Genap 2013/2014', '1106100001', 'NUR AULIA YUNUS', 'SI-34-01', 'FIX', '24-03-2014', 'UMY', 'MHY', 1),
(330, 'TS', 'Genap 2015/2016', '1106122050', 'NUR INTAN PARAMANISA', 'SI-36-02', 'FIX', '30-12-2015', 'WRP', 'MHY', 1),
(331, 'TS', 'Genap 2015/2016', '1106124204', 'NURRIDA AINI ZUHROH', 'SI-36-05', 'FIX', '01-01-2016', 'MOW', 'MHY', 1),
(332, 'TS_1', 'Genap 2014/2015', '1106110083', 'NURULLIANI SAFITRI MUTMAINAH', 'SI-35-03', 'FIX', '07-01-2015', 'MOW', 'MHY', 1),
(333, 'TS_2', 'Ganjil 2013/2014', '1106091050', 'NURVITA SETYO UTAMI', 'SI-33-02', 'FIX', '23-08-2013', 'YAP', 'MHY', 1),
(334, 'TS_2', 'Genap 2013/2014', '1106100061', 'NYOMAN PRAJNA MAS BRAHMADIPA', 'SI-34-02', 'FIX', '24-03-2014', 'YAP', 'MHY', 1),
(335, 'TS_2', 'Ganjil 2013/2014', '1106090006', 'OGI HANGGARA AGUSTANTYA', 'SI-33-01', 'FIX', '23-08-2013', 'MHY', 'MHY', 1),
(336, 'TS_1', 'Genap 2014/2015', '1106110024', 'OKTA PUSPITA DWI ANGGOROWATI', 'SI-35-01', 'FIX', '08-01-2015', 'AGU', 'MHY', 1),
(337, 'TS_1', 'Genap 2014/2015', '1106110059', 'OLAF ARMAN SEBASTIAN SIHOMBING', 'SI-35-02', 'FIX', '06-01-2015', 'TNA', 'MHY', 1),
(338, 'TS', 'Genap 2015/2016', '1106090016', 'PANJI NUGROHO', 'SI-33-01', 'FIX', '04-01-2016', 'MHY', 'MHY', 1),
(339, 'TS', 'Genap 2015/2016', '1106120088', 'PARAMITA RAHMAWATI', 'SI-36-04', 'FIX', '03-01-2016', 'SNP', 'MHY', 1),
(340, 'TS', 'Ganjil 2015/2016', '1106110094', 'PARASETIA ABU ADITYA', 'SI-35-03', 'FIX', '14-08-2015', 'MOW', 'MHY', 1),
(341, 'TS_2', 'Genap 2013/2014', '1106100022', 'PARGAULAN SIAGIAN', 'SI-34-01', 'FIX', '24-03-2014', 'UMY', 'MHY', 1),
(342, 'TS', 'Genap 2015/2016', '1106120036', 'PATRA BRIGANANDA', 'SI-36-02', 'FIX', '30-12-2015', 'WRP', 'MHY', 1),
(343, 'TS', 'Genap 2015/2016', '1106120118', 'PERI NURPAZRI', 'SI-36-05', 'FIX', '31-12-2015', 'MOW', 'MHY', 1),
(344, 'TS_1', 'Genap 2014/2015', '1106110026', 'PRADITO SETIADI', 'SI-35-01', 'FIX', '12-01-2015', 'AGU', 'MHY', 1),
(345, 'TS', 'Genap 2015/2016', '1106124197', 'PRATIWI GALUH PUTRI', 'SI-36-04', 'FIX', '04-01-2016', 'SNP', 'MHY', 1),
(346, 'TS_2', 'Genap 2013/2014', '1106100064', 'PREFIKS WAHYU FEBRINA', 'SI-34-02', 'FIX', '24-03-2014', 'YAP', 'MHY', 1),
(347, 'TS_2', 'Ganjil 2013/2014', '1106090002', 'PRIMA NANDA W', 'SI-33-01', 'FIX', '23-08-2013', 'MHY', 'MHY', 1),
(348, 'TS_1', 'Genap 2014/2015', '1106110002', 'PUSPITA AYU KARTIKA', 'SI-35-01', 'FIX', '07-01-2015', 'AGU', 'MHY', 1),
(349, 'TS_1', 'Genap 2014/2015', '1106110032', 'PUTRI MYKE WAHYUNI', 'SI-35-01', 'FIX', '09-01-2015', 'AGU', 'MHY', 1),
(350, 'TS_1', 'Genap 2014/2015', '1106110089', 'PUTU GEDE WIBAWA HARTAWAN', 'SI-35-03', 'FIX', '12-01-2015', 'MOW', 'MHY', 1),
(351, 'TS_1', 'Genap 2014/2015', '1106114147', 'PUTU PUSPITHA SARASWATI', 'SI-35-04', 'FIX', '09-01-2015', 'MTK', 'MHY', 1),
(352, 'TS_1', 'Genap 2014/2015', '1106110060', 'RAHAYU MANOLITA', 'SI-35-02', 'FIX', '09-01-2015', 'TNA', 'MHY', 1),
(353, 'TS_2', 'Ganjil 2013/2014', '1106091049', 'RAISSA AULIA RAHMAWATI', 'SI-33-02', 'FIX', '26-08-2013', 'YAP', 'MHY', 1),
(354, 'TS_2', 'Genap 2013/2014', '1106102098', 'RAMADHAN TRIYANTO PRABOWO', 'SI-34-03', 'FIX', '24-03-2014', 'SNP', 'MHY', 1),
(355, 'TS_2', 'Genap 2013/2014', '1106100007', 'RANDHI RAMADHAN', 'SI-34-01', 'FIX', '24-03-2014', 'UMY', 'MHY', 1),
(356, 'TS_1', 'Genap 2014/2015', '1106110004', 'RANDYANSYAH RACHMANALAN', 'SI-35-01', 'FIX', '06-01-2015', 'AGU', 'MHY', 1),
(357, 'TS_2', 'Ganjil 2013/2014', '1106090011', 'RANGGA YUDHA PRADESA', 'SI-33-01', 'FIX', '23-08-2013', 'MHY', 'MHY', 1),
(358, 'TS_2', 'Genap 2013/2014', '1106100056', 'RANI AULIYA SYAFRUDIN', 'SI-34-02', 'FIX', '24-03-2014', 'YAP', 'MHY', 1),
(359, 'TS_2', 'Genap 2013/2014', '1106100024', 'RANI ERVAN', 'SI-34-01', 'FIX', '24-03-2014', 'UMY', 'MHY', 1),
(360, 'TS', 'Genap 2015/2016', '1106120155', 'RANI NURBAITI', 'SI-36-02', 'FIX', '30-12-2015', 'WRP', 'MHY', 1),
(361, 'TS', 'Genap 2015/2016', '1106120157', 'RATI AMANDA FAJRIN', 'SI-36-02', 'FIX', '30-12-2015', 'WRP', 'MHY', 1),
(362, 'TS_1', 'Genap 2014/2015', '1106111142', 'RATIH CINTYA LESTARI', 'SI-35-04', 'FIX', '06-01-2015', 'MTK', 'MHY', 1),
(363, 'TS', 'Genap 2015/2016', '1106124196', 'RAULIA RISKI', 'SI-36-03', 'FIX', '04-01-2016', 'YAP', 'MHY', 1),
(364, 'TS', 'Genap 2015/2016', '1106120166', 'RECSA ANDRIYANI PUTRI', 'SI-36-03', 'FIX', '04-01-2016', 'YAP', 'MHY', 1),
(365, 'TS_1', 'Genap 2014/2015', '1106110112', 'REGINA AYU PRAMESWARI WADE', 'SI-35-03', 'FIX', '11-01-2015', 'MOW', 'MHY', 1),
(366, 'TS', 'Genap 2015/2016', '1106120043', 'REICKA SOFI AZURA RIDHALLAH', 'SI-36-02', 'FIX', '05-01-2016', 'WRP', 'MHY', 1),
(367, 'TS_1', 'Genap 2014/2015', '1106114149', 'RENANTIA INDRIANI', 'SI-35-04', 'FIX', '06-01-2015', 'MTK', 'MHY', 1),
(368, 'TS_2', 'Ganjil 2013/2014', '1106090023', 'RENDY APRIYANDO', 'SI-33-01', 'FIX', '26-08-2013', 'WRP', 'MHY', 1),
(369, 'TS_1', 'Genap 2014/2015', '1106110015', 'RENGGA YOGANANDHITA', 'SI-35-01', 'FIX', '09-01-2015', 'AGU', 'MHY', 1),
(370, 'TS', 'Genap 2015/2016', '1106120123', 'RENI MERDEKAWATI', 'SI-36-05', 'FIX', '04-01-2016', 'MOW', 'MHY', 1),
(371, 'TS_2', 'Genap 2013/2014', '1106100033', 'RESTU AMALIA', 'SI-34-01', 'FIX', '24-03-2014', 'UMY', 'MHY', 1),
(372, 'TS_2', 'Genap 2013/2014', '1106100062', 'REVILIYANA EKA PRATIWI', 'SI-34-02', 'FIX', '24-03-2014', 'YAP', 'MHY', 1),
(373, 'TS_1', 'Genap 2014/2015', '1106110007', 'REXY SEPTIAN ARAFAT', 'SI-35-01', 'FIX', '11-01-2015', 'AGU', 'MHY', 1),
(374, 'TS_1', 'Genap 2014/2015', '1106110041', 'REZA ALDIANSYAH', 'SI-35-02', 'FIX', '07-01-2015', 'TNA', 'MHY', 1),
(375, 'TS_2', 'Ganjil 2013/2014', '1106091054', 'REZA ALPERTASHENA', 'SI-33-02', 'FIX', '26-08-2013', 'YAP', 'MHY', 1),
(376, 'TS', 'Genap 2015/2016', '1106122133', 'Reza Harli Saputra', 'SI-36-05', 'FIX', '04-01-2016', 'MOW', 'MHY', 1),
(377, 'TS_2', 'Genap 2013/2014', '1106100003', 'RICKY TRI WANDA PUTRA S', 'SI-34-01', 'FIX', '24-03-2014', 'UMY', 'MHY', 1),
(378, 'TS', 'Genap 2015/2016', '1106120059', 'RIDEL EZRI SAMBOW', 'SI-36-03', 'FIX', '04-01-2016', 'YAP', 'MHY', 1),
(379, 'TS_1', 'Genap 2014/2015', '1106110022', 'RIDHO ARYA DUTA MAHENDRA', 'SI-35-01', 'FIX', '09-01-2015', 'AGU', 'MHY', 1),
(380, 'TS_2', 'Genap 2013/2014', '1106101089', 'RIDHO MOCHAMAD', 'SI-34-03', 'FIX', '24-03-2014', 'SNP', 'MHY', 1),
(381, 'TS_2', 'Genap 2013/2014', '1106100019', 'RIFI MELANIE', 'SI-34-01', 'FIX', '24-03-2014', 'UMY', 'MHY', 1),
(382, 'TS', 'Genap 2015/2016', '1106120061', 'RIFKIANSYAH ABDILA', 'SI-36-03', 'FIX', '30-12-2015', 'YAP', 'MHY', 1),
(383, 'TS_1', 'Genap 2014/2015', '1106110116', 'RIKA RIHANA ILMAR', 'SI-35-04', 'TIDAK FIX', '09-01-2015', 'MTK', 'MHY', 1),
(384, 'TS', 'Genap 2015/2016', '1106120165', 'RIKI RAMDHANI', 'SI-36-03', 'FIX', '04-01-2016', 'YAP', 'MHY', 1),
(385, 'TS', 'Genap 2015/2016', '1106120140', 'RINALDI HARRY LEKSANA', 'SI-36-01', 'FIX', '04-01-2016', 'MTK', 'MHY', 1),
(386, 'TS_1', 'Genap 2014/2015', '1106110010', 'RINALDY FERDY FERDIANO', 'SI-35-01', 'FIX', '09-01-2015', 'AGU', 'MHY', 1),
(387, 'TS_1', 'Genap 2014/2015', '1106110020', 'RINI SETYANINGSIH', 'SI-35-01', 'FIX', '09-01-2015', 'AGU', 'MHY', 1),
(388, 'TS_2', 'Ganjil 2013/2014', '1106080001', 'RIO FARIZKI', 'SI-32-01', 'FIX', '26-08-2013', 'IMR', 'MHY', 1),
(389, 'TS_2', 'Ganjil 2013/2014', '1106080027', 'RISMERIAN FRANSISCA', 'SI-32-02', 'TIDAK FIX', '25-08-2013', 'RIZ', 'MHY', 1),
(390, 'TS', 'Genap 2015/2016', '1106120174', 'RISNA RISDIANTI JUNIAR', 'SI-36-04', 'FIX', '05-01-2016', 'SNP', 'MHY', 1),
(391, 'TS', 'Genap 2015/2016', '1106120103', 'RISSA PUSPITA', 'SI-36-04', 'FIX', '04-01-2016', 'IFD', 'MHY', 1),
(392, 'TS', 'Genap 2015/2016', '1106120056', 'RIZA RAHMA PUTRI', 'SI-36-03', 'FIX', '30-12-2015', 'YAP', 'MHY', 1),
(393, 'TS_1', 'Genap 2014/2015', '1106114150', 'RIZKI DWI KURNIA DEWI', 'SI-35-04', 'FIX', '07-01-2015', 'MTK', 'MHY', 1),
(394, 'TS_2', 'Genap 2013/2014', '1106100055', 'RIZKI PUTRA RAMADHAN', 'SI-34-02', 'FIX', '24-03-2014', 'YAP', 'MHY', 1),
(395, 'TS', 'Genap 2015/2016', '1106120105', 'RIZKITA BAGUS PERDHANA', 'SI-36-04', 'FIX', '04-01-2016', 'IFD', 'MHY', 1),
(396, 'TS', 'Genap 2015/2016', '1106120128', 'RIZQA DEBBY ASYROFAH', 'SI-36-05', 'FIX', '31-12-2015', 'MOW', 'MHY', 1),
(397, 'TS', 'Genap 2015/2016', '1106120090', 'ROBBY PANGESTU', 'SI-36-04', 'FIX', '30-12-2015', 'SNP', 'MHY', 1),
(398, 'TS_1', 'Genap 2014/2015', '1106110052', 'RODNOVRY JOSHUA LUMBAN TOBING', 'SI-35-02', 'FIX', '07-01-2015', 'TNA', 'MHY', 1),
(399, 'TS_2', 'Ganjil 2013/2014', '1106080011', 'RUDY NUGRAHA PARTAHUTAMA', 'SI-32-01', 'FIX', '24-08-2013', 'MTK', 'MHY', 1),
(400, 'TS_2', 'Genap 2013/2014', '1106100042', 'RYANDA ARGANTARA', 'SI-34-02', 'FIX', '24-03-2014', 'YAP', 'MHY', 1),
(401, 'TS_1', 'Genap 2014/2015', '1106110003', 'SABRINA ANDIYANI', 'SI-35-01', 'FIX', '06-01-2015', 'AGU', 'MHY', 1),
(402, 'TS_2', 'Ganjil 2013/2014', '1106080009', 'SAJIDA AZ ZAHRA', 'SI-32-01', 'FIX', '27-08-2013', 'IMR', 'MHY', 1),
(403, 'TS_2', 'Ganjil 2013/2014', '1106080015', 'SALMAN AL FARISI', 'SI-32-02', 'FIX', '25-08-2013', 'RIZ', 'MHY', 1),
(404, 'TS_2', 'Genap 2013/2014', '1106100039', 'SALMAN FAROZI', 'SI-34-02', 'FIX', '24-03-2014', 'YAP', 'MHY', 1),
(405, 'TS_1', 'Genap 2014/2015', '1106110053', 'SATRIA JANAKA', 'SI-35-02', 'FIX', '08-01-2015', 'TNA', 'MHY', 1),
(406, 'TS_1', 'Genap 2014/2015', '1106110111', 'SATRIA NARENDRA WIBAWA', 'SI-35-03', 'FIX', '09-01-2015', 'MOW', 'MHY', 1),
(407, 'TS_2', 'Ganjil 2013/2014', '1106100013', 'SEKAR MUTAQINA', 'SI-34-01', 'FIX', '23-08-2013', 'UMY', 'MHY', 1),
(408, 'TS', 'Genap 2015/2016', '1106120072', 'SELLY LARASATI', 'SI-36-03', 'FIX', '31-12-2015', 'YAP', 'MHY', 1),
(409, 'TS_2', 'Genap 2013/2014', '1106100014', 'SEPTIAN HARI NUGROHO', 'SI-34-01', 'FIX', '24-03-2014', 'UMY', 'MHY', 1),
(410, 'TS_2', 'Genap 2013/2014', '1106100038', 'SHELA MANDRASARI PRASETYO', 'SI-34-01', 'FIX', '24-03-2014', 'UMY', 'MHY', 1),
(411, 'TS', 'Genap 2015/2016', '1106120079', 'SHELVIA ARDIAN PERDANA', 'SI-36-03', 'FIX', '31-12-2015', 'YAP', 'MHY', 1),
(412, 'TS_2', 'Ganjil 2013/2014', '1106080020', 'SHERLY YUANITA L.S', 'SI-32-02', 'FIX', '27-08-2013', 'RIZ', 'MHY', 1),
(413, 'TS_1', 'Genap 2014/2015', '1106110069', 'SHINTA SINDI NURYANI', 'SI-35-02', 'FIX', '11-01-2015', 'TNA', 'MHY', 1),
(414, 'TS', 'Genap 2015/2016', '1106120131', 'SITI ALMIRA DANIA', 'SI-36-05', 'FIX', '30-12-2015', 'MOW', 'MHY', 1),
(415, 'TS', 'Genap 2015/2016', '1106120077', 'SLAMET MAMAT RACHMAT', 'SI-36-03', 'FIX', '30-12-2015', 'YAP', 'MHY', 1),
(416, 'TS_1', 'Genap 2014/2015', '1106110124', 'SOFIA FARIDAH', 'SI-35-04', 'FIX', '06-01-2015', 'MTK', 'MHY', 1),
(417, 'TS_2', 'Ganjil 2013/2014', '1106100034', 'SONIA HELENA LADASI', 'SI-34-01', 'FIX', '23-08-2013', 'UMY', 'MHY', 1),
(418, 'TS_1', 'Genap 2014/2015', '1106110028', 'SRI HARYANI BR. MANJUNTAK', 'SI-35-01', 'FIX', '12-01-2015', 'AGU', 'MHY', 1),
(419, 'TS_1', 'Genap 2014/2015', '1106114146', 'SUFYAN SAURI', 'SI-35-04', 'FIX', '07-01-2015', 'MTK', 'MHY', 1),
(420, 'TS_2', 'Genap 2013/2014', '1106100054', 'SURAIYA SAFURA', 'SI-34-02', 'FIX', '24-03-2014', 'YAP', 'MHY', 1),
(421, 'TS', 'Genap 2015/2016', '1106110090', 'SWADIPTA YUDA PRAWIRA', 'SI-35-03', 'FIX', '01-01-2016', 'MOW', 'MHY', 1),
(422, 'TS', 'Genap 2015/2016', '1106120126', 'SYAHRIANDA', 'SI-36-05', 'FIX', '30-12-2015', 'MOW', 'MHY', 1);
INSERT INTO `tb_tamahasiswa` (`no`, `no_ts`, `tahun_ajrn`, `nim`, `nama_mahasiswa`, `kelas`, `status`, `input_date`, `pembimbing_akdmk`, `kode_pngjr`, `id_user`) VALUES
(423, 'TS', 'Genap 2015/2016', '1106120181', 'SYINTA KESUMA AISYAH', 'SI-36-05', 'FIX', '31-12-2015', 'MOW', 'MHY', 1),
(424, 'TS_2', 'Genap 2013/2014', '1106100027', 'T DANI HADYANSYAH PRATAMA', 'SI-34-01', 'FIX', '24-03-2014', 'UMY', 'MHY', 1),
(425, 'TS_2', 'Genap 2013/2014', '1106100057', 'TANTRI DAMAR PRASTUTI', 'SI-34-02', 'FIX', '24-03-2014', 'YAP', 'MHY', 1),
(426, 'TS_1', 'Genap 2014/2015', '1106110005', 'TEGAR PAMUNGKAS', 'SI-35-01', 'FIX', '13-01-2015', 'AGU', 'MHY', 1),
(427, 'TS', 'Genap 2015/2016', '1106120085', 'THERESIA YUDITH DWI PRISILA', 'SI-36-04', 'FIX', '02-01-2016', 'SNP', 'MHY', 1),
(428, 'TS_1', 'Genap 2014/2015', '1106110042', 'THESSA SILVIANA MANURUNG', 'SI-35-02', 'FIX', '12-01-2015', 'TNA', 'MHY', 1),
(429, 'TS_2', 'Genap 2013/2014', '1106102100', 'TIKA RAHMAWATI', 'SI-34-03', 'FIX', '24-03-2014', 'SNP', 'MHY', 1),
(430, 'TS', 'Genap 2015/2016', '1106121188', 'TIMBUL PRAWIRA GULTOM', 'SI-36-03', 'FIX', '30-12-2015', 'YAP', 'MHY', 1),
(431, 'TS_2', 'Ganjil 2013/2014', '1106090009', 'TONI SAPUTRO', 'SI-33-01', 'FIX', '23-08-2013', 'MHY', 'MHY', 1),
(432, 'TS_2', 'Ganjil 2013/2014', '1106080016', 'TRI ANDRI PAMBUDI PRASSETYO', 'SI-32-02', 'FIX', '28-08-2013', 'RIZ', 'MHY', 1),
(433, 'TS_2', 'Ganjil 2013/2014', '1106090042', 'TRIYADI YANUAR', 'SI-33-02', 'FIX', '23-08-2013', 'YAP', 'MHY', 1),
(434, 'TS_2', 'Genap 2013/2014', '1106100044', 'UDADHI BAWIKO', 'SI-34-02', 'FIX', '24-03-2014', 'YAP', 'MHY', 1),
(435, 'TS', 'Genap 2015/2016', '1106120179', 'UGI CHANDRA WIGUNA', 'SI-36-05', 'FIX', '30-12-2015', 'MOW', 'MHY', 1),
(436, 'TS', 'Genap 2015/2016', '1106122026', 'UMMI KHAIRA LATIF', 'SI-36-01', 'FIX', '30-12-2015', 'MTK', 'MHY', 1),
(437, 'TS', 'Genap 2015/2016', '1106120014', 'VEGI FRANSISCA', 'SI-36-01', 'FIX', '05-01-2016', 'MTK', 'MHY', 1),
(438, 'TS_1', 'Genap 2014/2015', '1106110036', 'VERA ANANDA', 'SI-35-01', 'FIX', '07-01-2015', 'AGU', 'MHY', 1),
(439, 'TS', 'Genap 2015/2016', '1106120162', 'VICTOR MAROLOP PATRIASI SIREGAR', 'SI-36-03', 'FIX', '30-12-2015', 'YAP', 'MHY', 1),
(440, 'TS', 'Genap 2015/2016', '1106120095', 'VIDYAKIRANA DWI JULITA', 'SI-36-04', 'FIX', '04-01-2016', 'IFD', 'MHY', 1),
(441, 'TS_2', 'Genap 2013/2014', '1106100026', 'VIERGIETHA LIENDRIANDARI', 'SI-34-01', 'FIX', '24-03-2014', 'UMY', 'MHY', 1),
(442, 'TS_1', 'Genap 2014/2015', '1106110051', 'VIKY HERMANA PRATAMA', 'SI-35-02', 'FIX', '07-01-2015', 'TNA', 'MHY', 1),
(443, 'TS_2', 'Genap 2013/2014', '1106100075', 'VITA HASYANTIERSI', 'SI-34-02', 'FIX', '24-03-2014', 'YAP', 'MHY', 1),
(444, 'TS_1', 'Genap 2014/2015', '1106110061', 'WAHYU DWI PRAMONO', 'SI-35-02', 'FIX', '08-01-2015', 'SFJ', 'MHY', 1),
(445, 'TS_2', 'Ganjil 2013/2014', '1106091046', 'WAHYU ROMADHAN', 'SI-33-02', 'FIX', '26-08-2013', 'YAP', 'MHY', 1),
(446, 'TS', 'Genap 2015/2016', '1106120063', 'WARHAN FAIZULLAH', 'SI-36-03', 'FIX', '04-01-2016', 'YAP', 'MHY', 1),
(447, 'TS', 'Genap 2015/2016', '1106120168', 'WIDYASARI OKTAVIANI', 'SI-36-04', 'FIX', '03-01-2016', 'SNP', 'MHY', 1),
(448, 'TS', 'Genap 2015/2016', '1106120031', 'WIDYATASYA AGUSTIKA NURTRISHA', 'SI-36-02', 'FIX', '30-12-2015', 'WRP', 'MHY', 1),
(449, 'TS_2', 'Ganjil 2013/2014', '1106080017', 'WIGAMA PROKLAMASI', 'SI-32-02', 'FIX', '26-08-2013', 'RIZ', 'MHY', 1),
(450, 'TS_2', 'Genap 2013/2014', '1106100006', 'WIJAYA HUTAMA ANGGARA', 'SI-34-01', 'FIX', '24-03-2014', 'UMY', 'MHY', 1),
(451, 'TS_2', 'Genap 2013/2014', '1106100016', 'WINDY WIDIASTUTI', 'SI-34-01', 'FIX', '24-03-2014', 'UMY', 'MHY', 1),
(452, 'TS_2', 'Genap 2013/2014', '1106104111', 'WISMI NUR ISDIANI', 'SI-34-03', 'FIX', '24-03-2014', 'SNP', 'MHY', 1),
(453, 'TS_2', 'Genap 2013/2014', '1106101092', 'WORO NINDITARINI', 'SI-34-03', 'FIX', '24-03-2014', 'SNP', 'MHY', 1),
(454, 'TS_1', 'Ganjil 2014/2015', '1106080008', 'WULUNG DANU PRASETYA', 'SI-32-01', 'FIX', '19-08-2014', 'IMR', 'MHY', 1),
(455, 'TS_1', 'Ganjil 2014/2015', '1106100041', 'YAFSHIL ADIPURA', 'SI-34-02', 'FIX', '20-08-2014', 'YAP', 'MHY', 1),
(456, 'TS', 'Genap 2015/2016', '1106110108', 'YOGA CHANIAGO', 'SI-35-03', 'FIX', '31-12-2015', 'MOW', 'MHY', 1),
(457, 'TS_2', 'Genap 2013/2014', '1106100040', 'YOGA SAMUDRO UTOMO', 'SI-34-02', 'FIX', '24-03-2014', 'YAP', 'MHY', 1),
(458, 'TS_2', 'Ganjil 2013/2014', '1106090007', 'YOGA SURYA ARDI PUTRA', 'SI-33-01', 'FIX', '24-08-2013', 'MHY', 'MHY', 1),
(459, 'TS_2', 'Ganjil 2013/2014', '1106091056', 'YOGY PRAKOSO', 'SI-33-02', 'FIX', '23-08-2013', 'YAP', 'MHY', 1),
(460, 'TS', 'Genap 2015/2016', '1106110113', 'YONGKI INDRA LESMANA', 'SI-35-03', 'FIX', '31-12-2015', 'MOW', 'MHY', 1),
(461, 'TS', 'Ganjil 2015/2016', '1106100009', 'YOSI ARIEF WICAKSONO', 'SI-34-01', 'FIX', '17-08-2015', 'UMY', 'MHY', 1),
(462, 'TS', 'Genap 2015/2016', '1106120013', 'YUANIKA INDANEA', 'SI-36-01', 'FIX', '04-01-2016', 'MTK', 'MHY', 1),
(463, 'TS_1', 'Genap 2014/2015', '1106110098', 'YUDHA ADITYA RAMADHANA', 'SI-35-03', 'FIX', '09-01-2015', 'MOW', 'MHY', 1),
(464, 'TS_2', 'Genap 2013/2014', '1106100010', 'YUDHA ARIF BUDIMAN', 'SI-34-01', 'FIX', '24-03-2014', 'UMY', 'MHY', 1),
(465, 'TS_2', 'Genap 2013/2014', '1106100021', 'YUKI ALQADRI', 'SI-34-01', 'FIX', '24-03-2014', 'UMY', 'MHY', 1),
(466, 'TS_2', 'Ganjil 2013/2014', '1106090036', 'YUNDIKA NOFRISANDI', 'SI-33-02', 'FIX', '26-08-2013', 'WRP', 'MHY', 1),
(467, 'TS_2', 'Ganjil 2013/2014', '1106090024', 'YUSFITA DYAH FANANI', 'SI-33-01', 'FIX', '24-08-2013', 'WRP', 'MHY', 1),
(468, 'TS', 'Genap 2015/2016', '1106110021', 'YUSRIL MAULIDAN RAJI', 'SI-35-01', 'FIX', '08-03-2016', 'AGU', 'MHY', 1),
(469, 'TS_1', 'Genap 2014/2015', '1106110009', 'YUSUF RAHMADI', 'SI-35-01', 'FIX', '07-01-2015', 'AGU', 'MHY', 1),
(470, 'TS_2', 'Ganjil 2013/2014', '1106080006', 'YUSUP FIRDAUS', 'SI-32-01', 'FIX', '24-08-2013', 'IMR', 'MHY', 1),
(471, 'TS_1', 'Genap 2014/2015', '1106110100', 'ZENITA ROKHMANINGSIH', 'SI-35-03', 'FIX', '09-01-2015', 'MOW', 'MHY', 1),
(472, 'TS', 'Genap 2015/2016', '1106120054', 'ZIGIT MAHA PUTRA', 'SI-36-03', 'FIX', '30-12-2015', 'YAP', 'MHY', 1),
(473, 'TS_2', 'Genap 2013/2014', '1106101082', 'ZULFIKAR AKBAR', 'SI-34-03', 'FIX', '24-03-2014', 'SNP', 'MHY', 1),
(474, 'TS_2', 'Genap 2013/2014', '1106100068', 'ZYAS ESA ANARKI', 'SI-34-02', 'FIX', '24-03-2014', 'YAP', 'MHY', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id_user` int(255) NOT NULL,
  `email` varchar(50) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `jenis_kelamin` varchar(20) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `hp` int(15) NOT NULL,
  `profilpic_path` varchar(255) NOT NULL,
  `tugas` varchar(500) NOT NULL,
  `hak_akses` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id_user`, `email`, `nama`, `password`, `jenis_kelamin`, `alamat`, `hp`, `profilpic_path`, `tugas`, `hak_akses`) VALUES
(1, 'adminbaru', 'adminbaru', 'dbcddd2b55ec5b104a2a1a64b8707d4a', 'Laki-Laki', 'kedasih', 922424, 'uploads/fotoprofil/1/6566937_20140518073804.jpg', 'll', 'admin'),
(19, 'diansuci19@gmail.com', 'Dian Suci Astuti', 'da06fdc8729ea9b3379961a5f49a5013', 'Perempuan', '', 822919191, '', '', 'standar 1'),
(26, 'dwilemurian@yahoo.com', 'Teguh', 'f5cd3a020bc94866049206a7cf14e266', 'Laki-Laki', '', 2147483647, '', '', 'standar 7'),
(4, 'kaprodi@fri.com', 'KAPRODI', '3c13922905d2bc454cc35e665335e2fd', 'Perempuan', '', 0, '', '', 'kaprodi'),
(2, 'kiswanto1996@gmail.com', 'Kiswanto', 'a398c6ff9fa0cd98baa901e741f46d80', 'Laki-Laki', 'Jl. Sukabirus', 2147483647, 'uploads/fotoprofil/2/PasFotoKiswanto.jpg', 'test1', 'standar 5'),
(27, 'penilai@xyz.com', 'aa', 'a2343deed565b1ffad7238bf72387886', 'Laki-Laki', '', 2232, '', '', 'tim penilaian'),
(23, 'prisyantiaffifiana@gmail.com', 'Fina', '7a732585028412129fb68f5154932355', 'Perempuan', '', 0, '', '', 'standar 6'),
(21, 'recsaandriyaniputri@gmail.com', 'Eca', '50f9a313a38d759803f489ad0c16bbff', 'Perempuan', '', 0, '', '', 'standar 3'),
(20, 'reginindiana@gmail.com', 'Egin', '4a939cb414b340bdd9391343b2612766', 'Perempuan', '', 88, '', '', 'standar 2'),
(18, 'riskiky19@gmail.com', 'Riskiky', 'b2404207ee2701aba1d8ead459ee0e2f', 'Perempuan', '', 0, '', '', 'standar 5'),
(3, 'teguh@xyz.com', 'Teguh', 'f5cd3a020bc94866049206a7cf14e266', 'Laki-Laki', '', 822222, 'uploads/fotoprofil/3/Picture-042-copy-2.jpg', 'wwwwww', 'standar 7'),
(22, 'utarianandaputri@gmail.com', 'Nanda', '87362c2914e9b22be3eea7f536704626', 'Perempuan', '', 0, '', '', 'standar 4'),
(24, 'vivizelviraa@gmail.com', 'Vivi', 'b1fcf56c78de19793116c776878ba206', 'Perempuan', '', 0, '', '', 'standar 7');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `file_uploads_standar5`
--
ALTER TABLE `file_uploads_standar5`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `numberts`
--
ALTER TABLE `numberts`
  ADD PRIMARY KEY (`no`);

--
-- Indexes for table `record_penilai`
--
ALTER TABLE `record_penilai`
  ADD PRIMARY KEY (`no`),
  ADD KEY `nama_penilai` (`nama_penilai`);

--
-- Indexes for table `report_version`
--
ALTER TABLE `report_version`
  ADD PRIMARY KEY (`no`);

--
-- Indexes for table `std5_52peninjauankurikulum`
--
ALTER TABLE `std5_52peninjauankurikulum`
  ADD PRIMARY KEY (`no`);

--
-- Indexes for table `std5_56upayaperbaikanpembelajaran`
--
ALTER TABLE `std5_56upayaperbaikanpembelajaran`
  ADD PRIMARY KEY (`no`);

--
-- Indexes for table `std5_511kompetensi`
--
ALTER TABLE `std5_511kompetensi`
  ADD PRIMARY KEY (`no`);

--
-- Indexes for table `std5_513matkulpilihan`
--
ALTER TABLE `std5_513matkulpilihan`
  ADD PRIMARY KEY (`no`);

--
-- Indexes for table `std5_514substansipraktikum`
--
ALTER TABLE `std5_514substansipraktikum`
  ADD PRIMARY KEY (`no`) USING BTREE;

--
-- Indexes for table `std5_531mekanismepenyusunan`
--
ALTER TABLE `std5_531mekanismepenyusunan`
  ADD PRIMARY KEY (`no`);

--
-- Indexes for table `std5_532contohsoalujian`
--
ALTER TABLE `std5_532contohsoalujian`
  ADD PRIMARY KEY (`no`);

--
-- Indexes for table `std5_541daftardosenpembimbing`
--
ALTER TABLE `std5_541daftardosenpembimbing`
  ADD PRIMARY KEY (`no`);

--
-- Indexes for table `std5_542prosespembimbingan`
--
ALTER TABLE `std5_542prosespembimbingan`
  ADD PRIMARY KEY (`no`);

--
-- Indexes for table `std5_551pelaksanaanpembimbingan`
--
ALTER TABLE `std5_551pelaksanaanpembimbingan`
  ADD PRIMARY KEY (`no`);

--
-- Indexes for table `std5_552ratapenyelesaianta`
--
ALTER TABLE `std5_552ratapenyelesaianta`
  ADD PRIMARY KEY (`no`);

--
-- Indexes for table `std5_571kebijakan`
--
ALTER TABLE `std5_571kebijakan`
  ADD PRIMARY KEY (`no`);

--
-- Indexes for table `std5_572ketersediaanprasarana`
--
ALTER TABLE `std5_572ketersediaanprasarana`
  ADD PRIMARY KEY (`no`);

--
-- Indexes for table `std5_573program`
--
ALTER TABLE `std5_573program`
  ADD PRIMARY KEY (`no`);

--
-- Indexes for table `std5_574interaksi`
--
ALTER TABLE `std5_574interaksi`
  ADD PRIMARY KEY (`no`);

--
-- Indexes for table `std5_575pengembangan`
--
ALTER TABLE `std5_575pengembangan`
  ADD PRIMARY KEY (`no`);

--
-- Indexes for table `std5_5121jumlahsksps`
--
ALTER TABLE `std5_5121jumlahsksps`
  ADD UNIQUE KEY `no` (`no`);

--
-- Indexes for table `std5_5122strukturkurikulummk`
--
ALTER TABLE `std5_5122strukturkurikulummk`
  ADD PRIMARY KEY (`no`) USING BTREE;

--
-- Indexes for table `std5_aspekpenilaian`
--
ALTER TABLE `std5_aspekpenilaian`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `std5_filependukung`
--
ALTER TABLE `std5_filependukung`
  ADD PRIMARY KEY (`id`),
  ADD KEY `iduser_fk` (`id_user`);

--
-- Indexes for table `std5_history_uploadfile`
--
ALTER TABLE `std5_history_uploadfile`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user_fk` (`id_user`) USING BTREE;

--
-- Indexes for table `std5_penilaian`
--
ALTER TABLE `std5_penilaian`
  ADD PRIMARY KEY (`id`),
  ADD KEY `iduser_fk` (`oleh`),
  ADD KEY `aspekpenilaian_fk` (`aspek_penilaian`);

--
-- Indexes for table `std5_penilaian52`
--
ALTER TABLE `std5_penilaian52`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oleh` (`oleh`);

--
-- Indexes for table `std5_penilaian54`
--
ALTER TABLE `std5_penilaian54`
  ADD PRIMARY KEY (`id`),
  ADD KEY `iduser_fk` (`oleh`);

--
-- Indexes for table `std5_penilaian56`
--
ALTER TABLE `std5_penilaian56`
  ADD PRIMARY KEY (`id`),
  ADD KEY `iduser_fk` (`oleh`);

--
-- Indexes for table `std5_penilaian511`
--
ALTER TABLE `std5_penilaian511`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oleh` (`oleh`);

--
-- Indexes for table `std5_penilaian512`
--
ALTER TABLE `std5_penilaian512`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oleh` (`oleh`);

--
-- Indexes for table `std5_penilaian513`
--
ALTER TABLE `std5_penilaian513`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oleh` (`oleh`);

--
-- Indexes for table `std5_penilaian514`
--
ALTER TABLE `std5_penilaian514`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oleh` (`oleh`);

--
-- Indexes for table `std5_penilaian531`
--
ALTER TABLE `std5_penilaian531`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_user_fk` (`oleh`);

--
-- Indexes for table `std5_penilaian532`
--
ALTER TABLE `std5_penilaian532`
  ADD PRIMARY KEY (`id`),
  ADD KEY `iduser_fk` (`oleh`);

--
-- Indexes for table `std5_penilaian551`
--
ALTER TABLE `std5_penilaian551`
  ADD PRIMARY KEY (`id`),
  ADD KEY `iduser_fk` (`oleh`);

--
-- Indexes for table `std5_penilaian552`
--
ALTER TABLE `std5_penilaian552`
  ADD PRIMARY KEY (`id`),
  ADD KEY `iduser_fk` (`catatan552`(767)) USING BTREE;

--
-- Indexes for table `std5_peranpekerjaanalumni`
--
ALTER TABLE `std5_peranpekerjaanalumni`
  ADD PRIMARY KEY (`no`);

--
-- Indexes for table `std5_profil_lulusan`
--
ALTER TABLE `std5_profil_lulusan`
  ADD PRIMARY KEY (`no`);

--
-- Indexes for table `std7_penilaian`
--
ALTER TABLE `std7_penilaian`
  ADD PRIMARY KEY (`id`),
  ADD KEY `penilai_terakhir` (`penilai_terakhir`);

--
-- Indexes for table `tb_7_2_2_abdimas`
--
ALTER TABLE `tb_7_2_2_abdimas`
  ADD PRIMARY KEY (`no`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `tb_abdimas`
--
ALTER TABLE `tb_abdimas`
  ADD PRIMARY KEY (`no`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `tb_dosen`
--
ALTER TABLE `tb_dosen`
  ADD PRIMARY KEY (`no`),
  ADD UNIQUE KEY `no` (`no`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `tb_haki`
--
ALTER TABLE `tb_haki`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `tb_kerjasama`
--
ALTER TABLE `tb_kerjasama`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `tb_peldosen_ta`
--
ALTER TABLE `tb_peldosen_ta`
  ADD PRIMARY KEY (`no`),
  ADD KEY `judul_penelitian_dosen` (`judul_penelitian_dosen`),
  ADD KEY `id_user` (`id_user`),
  ADD KEY `id_peldos` (`id_peldos`);

--
-- Indexes for table `tb_penelitian_dosen`
--
ALTER TABLE `tb_penelitian_dosen`
  ADD UNIQUE KEY `no` (`peldos_no`) USING BTREE,
  ADD KEY `judul` (`judul`),
  ADD KEY `judul_2` (`judul`),
  ADD KEY `id_user` (`id_user`),
  ADD KEY `idpeldos` (`idpeldos`);

--
-- Indexes for table `tb_publikasi`
--
ALTER TABLE `tb_publikasi`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `tb_tamahasiswa`
--
ALTER TABLE `tb_tamahasiswa`
  ADD PRIMARY KEY (`no`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`email`),
  ADD UNIQUE KEY `id` (`id_user`),
  ADD KEY `nama` (`nama`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `numberts`
--
ALTER TABLE `numberts`
  MODIFY `no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `record_penilai`
--
ALTER TABLE `record_penilai`
  MODIFY `no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;
--
-- AUTO_INCREMENT for table `report_version`
--
ALTER TABLE `report_version`
  MODIFY `no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tb_7_2_2_abdimas`
--
ALTER TABLE `tb_7_2_2_abdimas`
  MODIFY `no` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `tb_abdimas`
--
ALTER TABLE `tb_abdimas`
  MODIFY `no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `tb_dosen`
--
ALTER TABLE `tb_dosen`
  MODIFY `no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
--
-- AUTO_INCREMENT for table `tb_haki`
--
ALTER TABLE `tb_haki`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tb_kerjasama`
--
ALTER TABLE `tb_kerjasama`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;
--
-- AUTO_INCREMENT for table `tb_peldosen_ta`
--
ALTER TABLE `tb_peldosen_ta`
  MODIFY `no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=186;
--
-- AUTO_INCREMENT for table `tb_penelitian_dosen`
--
ALTER TABLE `tb_penelitian_dosen`
  MODIFY `peldos_no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88;
--
-- AUTO_INCREMENT for table `tb_publikasi`
--
ALTER TABLE `tb_publikasi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=230;
--
-- AUTO_INCREMENT for table `tb_tamahasiswa`
--
ALTER TABLE `tb_tamahasiswa`
  MODIFY `no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=475;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id_user` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
