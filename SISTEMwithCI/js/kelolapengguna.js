(function($){
$(document).ready(function(){
	var table_user = $('#table_user').DataTable({
		"lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
		"oLanguage": {
		  "oPaginate": {
			"sFirst":       " ",
			"sPrevious":    " ",
			"sNext":        " ",
			"sLast":        " ",
		  },
		  "sLengthMenu":    "Records per page: _MENU_",
		  "sInfo":          "Total of _TOTAL_ records (showing _START_ to _END_)",
		  "sInfoFiltered":  "(filtered from _MAX_ total records)"
		}
	  });
				  
	$.validator.setDefaults({
		success: 'valid',
		/*
		rules: {
		fiscal_year: {
		required: true,
		min:      2000,
		max:      2025
		}
		},
		*/
		errorPlacement: function(error, element){
		error.insertBefore(element);
	},
		highlight: function(element ){
			$(element).parent('.field_container').removeClass('valid').addClass('error');
		},
		unhighlight: function(element, element2){
			$(element).parent('.field_container').addClass('valid').removeClass('error');
		}
	});


	$('#form_user').validate({
		rules : {
			password : {
				minlength : 5
			}
		}	
	});
				
				
  // On page load: datatable
  
  
  // On page load: form validation
  

  // Show message
  function show_message(message_text, message_type){
    $('#message').html('<p>' + message_text + '</p>').attr('class', message_type);
    $('#message_container').show();
    if (typeof timeout_message !== 'undefined'){
      window.clearTimeout(timeout_message);
    }
    timeout_message = setTimeout(function(){
      hide_message();
    }, 8000);
  }
  // Hide message
  function hide_message(){
    $('#message').html('').attr('class', '');
    $('#message_container').hide();
  }

  // Show loading message
  function show_loading_message(){
    $('#loading_container').show();
  }
  // Hide loading message
  function hide_loading_message(){
    $('#loading_container').hide();
  }

  // Show lightbox
  function show_lightbox(){
    $('.lightbox_bg').show();
    $('.lightbox_container').show();
  }
  // Hide lightbox
  function hide_lightbox(){
    $('.lightbox_bg').hide();
    $('.lightbox_container').hide();
  }
  // Lightbox background
  $(document).on('click', '.lightbox_bg', function(){
    hide_lightbox();
  });
  // Lightbox close button
  $(document).on('click', '.lightbox_close', function(){
    hide_lightbox();
	$('#form_user #input_container_password').show();
	$("input:radio[name=jenis_kelamin]").prop('checked', false);
  });
  // Escape keyboard key
  $(document).keyup(function(e){
    if (e.keyCode == 27){
      hide_lightbox();
    }
  });
  
  // Hide iPad keyboard
  function hide_ipad_keyboard(){
    document.activeElement.blur();
    $('input').blur();
  }

  // Add company button
  $(document).on('click', '#add_user', function(e){
    e.preventDefault();
    $('.lightbox_content h2').text('Tambah Pengguna');
    $('#form_user button').text('Tambah Pengguna');
    $('#form_user').attr('class', 'form add');
    $('#form_user').attr('data-id', '');
    $('#form_user .field_container label.error').hide();
    $('#form_user .field_container').removeClass('valid').removeClass('error');
    $('#form_user #email').val('');
    $('#form_user #nohp').val('');
    $('#form_user #hak_akses').val('');
    $('#form_user #nama_lengkap').val('');
    $('#form_user #alamat').val('');
	
    show_lightbox();
  });

  // Add company submit form
  $(document).on('submit', '#form_user.add', function(e){
    e.preventDefault();
    // Validate form
    if ($('#form_user').valid() == true){
      // Send company information to database
      hide_ipad_keyboard();
      hide_lightbox();
      show_loading_message();
      var form_data = $('#form_user').serialize();
      var request   = $.ajax({
        url:          'http://localhost/ta/sistemwithci/admin/kelolapengguna_proses?job=add_user',
        cache:        false,
        data:         form_data,
        dataType:     'json',
        contentType:  'application/json; charset=utf-8',
        type:         'get'
      });
      request.done(function(output){
        if (output.result == 'success'){
          // Reload datable
			table_user.draw();
            hide_loading_message();
            var nama_user = $('#nama_lengkap').val();
            show_message("Pengguna '" + nama_user + "' added successfully.", 'success');
			location.reload();
        } else {
          hide_loading_message();
          show_message('Add request failed', 'error');
        }
      });
      request.fail(function(jqXHR, textStatus){
        hide_loading_message();
        show_message('Add request failed: ' + textStatus, 'error');
      });
    }
  });

  // Edit company button
  $(document).on('click', '.function_edit a', function(e){
    e.preventDefault();
    // Get company information from database
    show_loading_message();
    var id      = $(this).data('id');
    var request = $.ajax({
      url:          'http://localhost/ta/sistemwithci/admin/kelolapengguna_proses?job=get_user',
      cache:        false,
      data:         'id=' + id,
      dataType:     'json',
      contentType:  'application/json; charset=utf-8',
      type:         'get'
    });
    request.done(function(output){
      if (output.result == 'success'){
        $('.lightbox_content h2').text('Edit Pengguna');
        $('#form_user button').text('Simpan');
        $('#form_user').attr('class', 'form edit');
        $('#form_user').attr('data-id', id);
        $('#form_user .field_container label.error').hide();
        $('#form_user .field_container').removeClass('valid').removeClass('error');
        $('#form_user #email').val(output.data[0].email);
        $('#form_user #input_container_password').hide();
        $('#form_user #nama_lengkap').val(output.data[0].nama);
        $('#form_user #nohp').val(output.data[0].nohp);
        $('#form_user #alamat').val(output.data[0].alamat);
        
		if (output.data[0].jenis_kelamin == "Laki-Laki"){
			$("input:radio[name=jenis_kelamin]:nth(0)").prop('checked', true);
		} else if (output.data[0].jenis_kelamin == "Perempuan") {
			$("input:radio[name=jenis_kelamin]:nth(1)").prop('checked', true);
		} else{
			$("input:radio[name=jenis_kelamin]").prop('checked', false);
		}
        $('#form_user #hak_akses').val(output.data[0].hak_akses);
 
        hide_loading_message();
        show_lightbox();
      } else {
        hide_loading_message();
        show_message('Information request failed', 'error');
      }
    });
    request.fail(function(jqXHR, textStatus){
      hide_loading_message();
      show_message('Information request failed: ' + textStatus, 'error');
    });
  });
  
  // Edit company submit form
  $(document).on('submit', '#form_user.edit', function(e){
    e.preventDefault();
    // Validate form
    if ($('#form_user').valid() == true){
      // Send company information to database
      hide_ipad_keyboard();
      hide_lightbox();
      show_loading_message();
      var id        = $('#form_user').attr('data-id');
      var form_data = $('#form_user').serialize();
      var request   = $.ajax({
        url:          'http://localhost/ta/sistemwithci/admin/kelolapengguna_proses?job=edit_user&id=' + id,
        cache:        false,
        data:         form_data,
        dataType:     'json',
        contentType:  'application/json; charset=utf-8',
        type:         'get'
      });
      request.done(function(output){
        if (output.result == 'success'){
          // Reload datable
			table_user.draw()
            hide_loading_message();
            var nama_user = $('#nama_lengkap').val();
            show_message("Pengguna '" + nama_user + "' edited successfully.", 'success');
			location.reload();
        } else {
          hide_loading_message();
          show_message('Edit request failed', 'error');
        }
      });
      request.fail(function(jqXHR, textStatus){
        hide_loading_message();
        show_message('Edit request failed: ' + textStatus, 'error');
      });
    }
  });
  
  // Delete company
  $(document).on('click', '.function_delete a', function(e){
    e.preventDefault();
    var nama_user = $(this).attr('data-name');
	console.log()
    if (confirm("Are you sure you want to delete '" + nama_user + "'?")){
      show_loading_message();
      var id      = $(this).data('id');
      var request = $.ajax({
        url:          'http://localhost/ta/sistemwithci/admin/kelolapengguna_proses?job=delete_user&id=' + id,
        cache:        false,
        dataType:     'json',
        contentType:  'application/json; charset=utf-8',
        type:         'get'
      });
      request.done(function(output){
        if (output.result == 'success'){
          // Reload datable
			table_user.draw
            hide_loading_message();
            show_message("Pengguna '" + nama_user + "' deleted successfully.", 'success');
			location.reload();
        } else {
          hide_loading_message();
          show_message('Delete request failed', 'error');
        }
      });
      request.fail(function(jqXHR, textStatus){
        hide_loading_message();
        show_message('Delete request failed: ' + textStatus, 'error');
      });
    }
  });

});
})(jQuery);	