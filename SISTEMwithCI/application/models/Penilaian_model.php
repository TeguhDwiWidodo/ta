<?php

defined('BASEPATH') OR exit('No direct script access allowed');

Class Penilaian_model extends CI_Model{
	public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
	
	function getHasilPenilaian(){
		$this->db->select('std5_penilaian.*, users.nama, std5_aspekpenilaian.aspek_penilaian AS deskripsi_aspeknilai');
		$this->db->from('std5_penilaian');
		$this->db->join('users', 'std5_penilaian.oleh = users.id_user', 'inner');
		$this->db->join('std5_aspekpenilaian', 'std5_penilaian.aspek_penilaian = std5_aspekpenilaian.id', 'inner');
		
		$query = $this->db->get();

		if ( $query->num_rows() > 0 ){
			$result = $query->result_array();
			return $result;
		} else {
			return "kosong";
		}
	}
	
	function sumHasilPenilaian(){
		$this->db->select_sum('bobotxnilai');
		$this->db->from('std5_penilaian');
		$query = $this->db->get();
		if ( $query->num_rows() > 0 ){
			$result = $query->result_array();
			return $result;
		} else {
			return 0;
		}
	}
	
	function insertNilai511($nilai511a, $nilai511b, $catatan511, $userid){
		$data511a = array(
					'id'				=> '50',
					'butir_penilaian'	=> '5.1.1.a',
					'aspek_penilaian'	=> '50',
					'bobot'				=> '0.57',
					'nilai'				=> $nilai511a,
					'bobotxnilai'		=> $nilai511a*0.57,
					'catatan'			=> $catatan511[0][0],
					'oleh'				=> $userid
				);
		
		$data511b =	array(
					'id'				=> '51',
					'butir_penilaian'	=> '5.1.1.b',
					'aspek_penilaian'	=> '51',
					'bobot'				=> '0.57',
					'nilai'				=> $nilai511b,
					'bobotxnilai'		=> $nilai511b*0.57,
					'catatan'			=> $catatan511[0][1],
					'oleh'				=> $userid
				);
		

		$this->db->replace('std5_penilaian', $data511a);
		$this->db->replace('std5_penilaian', $data511b);
		return "success";
	}
	
	function insertNilai512($nilai512a, $nilai512b, $nilai512c, $catatan512, $userid){
		$data512a = array(
			'id'				=> '52',
			'butir_penilaian'	=> '5.1.2.a',
			'aspek_penilaian'	=> '52',
			'bobot'				=> '0.57',
			'nilai' 			=> $nilai512a,
			'bobotxnilai' 		=> $nilai512a*0.57,
			'catatan'			=> $catatan512[0][0],	
			'oleh'				=> $userid
		);
		$data512b = array(
			'id'				=> '53',
			'butir_penilaian'	=> '5.1.2.b',
			'aspek_penilaian'	=> '53',
			'bobot'				=> '0.57',
			'nilai' 			=> $nilai512b,
			'bobotxnilai' 		=> $nilai512b*0.57,
			'catatan'			=> $catatan512[0][1],
			'oleh'				=> $userid
		);
		$data512c = array(
			'id'				=> '54',
			'butir_penilaian'	=> '5.1.2.c',
			'aspek_penilaian'	=> '54',
			'bobot'				=> '0.57',
			'nilai' 			=> $nilai512c,
			'bobotxnilai' 		=> $nilai512c*0.57,
			'catatan'			=> $catatan512[0][2],
			'oleh'				=> $userid
		);

		$this->db->replace('std5_penilaian', $data512a);		
		$this->db->replace('std5_penilaian', $data512b);		
		$this->db->replace('std5_penilaian', $data512c);		
		return "success";
	}
	
	function insertNilai513($nilai513, $catatan513, $userid){
		$data = array(
			'id'				=> '55',
			'butir_penilaian'	=> '5.1.3',
			'aspek_penilaian'	=> '55',
			'bobot'				=> '0.57',
			'nilai' 			=> $nilai513,
			'bobotxnilai' 		=> $nilai513*0.57,
			'catatan'			=> $catatan513,
			'oleh'				=> $userid
		);

		$this->db->replace('std5_penilaian', $data);		
		return "success";
	}
	
	function insertNilai514($nilai514, $catatan514, $userid){
		$data = array(
			'id'				=> '56',
			'butir_penilaian'	=> '5.1.4',
			'aspek_penilaian'	=> '56',
			'bobot'				=> '1.14',
			'nilai' 			=> $nilai514,
			'bobotxnilai' 		=> $nilai514*1.14,
			'catatan'			=> $catatan514,
			'oleh'				=> $userid
		);

		$this->db->replace('std5_penilaian', $data);		
		return "success";
	}
	
	function insertNilai52($nilai52, $catatan52, $userid){
		$data52a = array(
			'id'				=> '57',
			'butir_penilaian'	=> '5.2.a',
			'aspek_penilaian'	=> '57',
			'bobot'				=> '0.57',
			'nilai' 			=> $nilai52[0][0],
			'bobotxnilai' 		=> $nilai52[0][0]*0.57,
			'catatan'			=> $catatan52[0][0],
			'oleh'				=> $userid
		);
		
		$data52b = array(
			'id'				=> '58',
			'butir_penilaian'	=> '5.2.b',
			'aspek_penilaian'	=> '58',
			'bobot'				=> '0.57',
			'nilai' 			=> $nilai52[0][1],
			'bobotxnilai' 		=> $nilai52[0][1]*0.57,
			'catatan'			=> $catatan52[0][1],
			'oleh'				=> $userid
		);

		$this->db->replace('std5_penilaian', $data52a);		
		$this->db->replace('std5_penilaian', $data52b);		
		return "success";
	}
	
	function insertNilai531($nilai531, $catatan531, $userid){
		$data531a = array(
			'id'				=> '59',
			'butir_penilaian'	=> '5.3.1.a',
			'aspek_penilaian'	=> '59',
			'bobot'				=> '1.14',
			'nilai' 			=> $nilai531[0][0]/3,
			'bobotxnilai' 		=> ($nilai531[0][0]/3)*1.14,
			'catatan'			=> $catatan531[0][0],
			'oleh'				=> $userid,
		);
			
		$data531b = array(	
			'id'				=> '60',
			'butir_penilaian'	=> '5.3.1.b',
			'aspek_penilaian'	=> '60',
			'bobot'				=> '0.57',
			'nilai' 			=> $nilai531[0][1],
			'bobotxnilai' 		=> $nilai531[0][1]*0.57,
			'catatan'			=> $catatan531[0][1],
			'oleh'				=> $userid
		);

		$this->db->replace('std5_penilaian', $data531a);		
		$this->db->replace('std5_penilaian', $data531b);		
		return "success";
	}
	
	function insertNilai532($nilai532, $catatan532, $userid){
		$data = array(
			'id'				=> '61',
			'butir_penilaian'	=> '5.3.2',
			'aspek_penilaian'	=> '61',
			'bobot'				=> '0.57',
			'nilai' 			=> $nilai532,
			'bobotxnilai' 		=> $nilai532*0.57,
			'catatan'			=> $catatan532,
			'oleh'				=> $userid
		);

		$this->db->replace('std5_penilaian', $data);		
		return "success";
	}
	
	function insertNilai54($nilai54, $nilai_number54, $catatan54, $userid){
		$data541a = array(
			'id'				=> '62',
			'butir_penilaian'	=> '5.4.1.a',
			'aspek_penilaian'	=> '62',
			'bobot'				=> '0.57',
			'nilai' 			=> $nilai_number54[0][0],
			'bobotxnilai' 		=> $nilai_number54[0][0]*0.57,
			'catatan'			=> $catatan54[0][0],
			'oleh'				=> $userid
		);
		$data541b = array(
			'id'				=> '63',
			'butir_penilaian'	=> '5.4.1.b',
			'aspek_penilaian'	=> '63',
			'bobot'				=> '0.57',
			'nilai' 			=> $nilai54[0][3],
			'bobotxnilai' 		=> $nilai54[0][3]*0.57,
			'catatan'			=> $catatan54[0][1],
			'oleh'				=> $userid
		);
		$data541c = array(
			'id'				=> '64',
			'butir_penilaian'	=> '5.4.1.c',
			'aspek_penilaian'	=> '64',
			'bobot'				=> '0.57',
			'nilai' 			=> $nilai_number54[0][1],
			'bobotxnilai' 		=> $nilai_number54[0][1]*0.57,
			'catatan'			=> $catatan54[0][2],
			'oleh'				=> $userid
		);
		$data542 = array(
			'id'				=> '65',
			'butir_penilaian'	=> '5.4.1.d',
			'aspek_penilaian'	=> '65',
			'bobot'				=> '0.57',
			'nilai' 			=> $nilai54[0][5],
			'bobotxnilai' 		=> $nilai54[0][5]*0.57,
			'catatan'			=> $catatan54[0][3],
			'oleh'				=> $userid
		);

		$this->db->replace('std5_penilaian', $data541a);		
		$this->db->replace('std5_penilaian', $data541b);		
		$this->db->replace('std5_penilaian', $data541c);		
		$this->db->replace('std5_penilaian', $data542);		
		return "success";
	}
	
	function insertNilai551($nilai551, $nilai_number551, $catatan551, $userid){
		$data551a = array(
			'id'				=> '66',
			'butir_penilaian'	=> '5.5.1.a',
			'aspek_penilaian'	=> '66',
			'bobot'				=> '0.57',
			'nilai' 			=> $nilai551[0][0],
			'bobotxnilai' 		=> $nilai551[0][0]*0.57,
			'catatan'			=> $catatan551[0][0],
			'oleh'				=> $userid
		);
		$data551b = array(
			'id'				=> '67',
			'butir_penilaian'	=> '5.5.1.b',
			'aspek_penilaian'	=> '67',
			'bobot'				=> '0.57',
			'nilai' 			=> $nilai_number551[0][0],
			'bobotxnilai' 		=> $nilai_number551[0][0]*0.57,
			'catatan'			=> $catatan551[0][1],
			'oleh'				=> $userid
		);
		$data551c = array(
			'id'				=> '68',
			'butir_penilaian'	=> '5.5.1.c',
			'aspek_penilaian'	=> '68',
			'bobot'				=> '0.57',
			'nilai' 			=> $nilai_number551[0][1],
			'bobotxnilai' 		=> $nilai_number551[0][1]*0.57,
			'catatan'			=> $catatan551[0][2],
			'oleh'				=> $userid
		);
		$data551d = array(
			'id'				=> '69',
			'butir_penilaian'	=> '5.5.1.d',
			'aspek_penilaian'	=> '69',
			'bobot'				=> '1.14',
			'nilai' 			=> $nilai551[0][4],
			'bobotxnilai' 		=> $nilai551[0][4]*1.14,
			'catatan'			=> $catatan551[0][3],
			'oleh'				=> $userid
		);

		$this->db->replace('std5_penilaian', $data551a);		
		$this->db->replace('std5_penilaian', $data551b);		
		$this->db->replace('std5_penilaian', $data551c);		
		$this->db->replace('std5_penilaian', $data551d);		
		return "success";
	}
	
	function insertNilai552($nilai552, $catatan552, $userid){
		$data = array(
			'id'				=> '70',
			'butir_penilaian'	=> '5.5.2',
			'aspek_penilaian'	=> '70',
			'bobot'				=> '1.14',
			'nilai' 			=> $nilai552,
			'bobotxnilai' 		=> $nilai552*1.14,
			'catatan'			=> $catatan552,
			'oleh'				=> $userid
		);

		$this->db->replace('std5_penilaian', $data);		
		return "success";
	}
	
	function insertNilai56($nilai56, $catatan56, $userid){
		$data = array(
			'id'				=> '71',
			'butir_penilaian'	=> '5.6',
			'aspek_penilaian'	=> '71',
			'bobot'				=> '0.57',
			'nilai' 			=> $nilai56,
			'bobotxnilai' 		=> $nilai56*0.57,
			'catatan'			=> $catatan56,
			'oleh'				=> $userid
		);

		$this->db->replace('std5_penilaian', $data);		
		return "success";
	}
	
	function insertNilai57($nilai57, $catatan57, $userid){
		$data571 = array(
			'id'				=> '72',
			'butir_penilaian'	=> '5.7.1',
			'aspek_penilaian'	=> '72',
			'bobot'				=> '0.57',
			'nilai' 			=> $nilai57[0][0],
			'bobotxnilai' 		=> $nilai57[0][0]*0.57,
			'catatan'			=> $catatan57[0][0],
			'oleh'				=> $userid
		);
		$data572 = array(
			'id'				=> '73',
			'butir_penilaian'	=> '5.7.2',
			'aspek_penilaian'	=> '73',
			'bobot'				=> '1.14',
			'nilai' 			=> $nilai57[0][1],
			'bobotxnilai' 		=> $nilai57[0][1]*1.14,
			'catatan'			=> $catatan57[0][1],
			'oleh'				=> $userid
		);
		$data573 = array(
			'id'				=> '74',
			'butir_penilaian'	=> '5.7.3',
			'aspek_penilaian'	=> '74',
			'bobot'				=> '1.14',
			'nilai' 			=> $nilai57[0][2],
			'bobotxnilai' 		=> $nilai57[0][2]*1.14,
			'catatan'			=> $catatan57[0][2],
			'oleh'				=> $userid
		);
		$data574 = array(
			'id'				=> '75',
			'butir_penilaian'	=> '5.7.4',
			'aspek_penilaian'	=> '75',
			'bobot'				=> '0.57',
			'nilai' 			=> $nilai57[0][3],
			'bobotxnilai' 		=> $nilai57[0][3]*0.57,
			'catatan'			=> $catatan57[0][3],
			'oleh'				=> $userid
		);
		$data575 = array(
			'id'				=> '76',
			'butir_penilaian'	=> '5.7.5',
			'aspek_penilaian'	=> '76',
			'bobot'				=> '0.57',
			'nilai' 			=> $nilai57[0][4],
			'bobotxnilai' 		=> $nilai57[0][4]*0.57,
			'catatan'			=> $catatan57[0][4],
			'oleh'				=> $userid
		);

		$this->db->replace('std5_penilaian', $data571);		
		$this->db->replace('std5_penilaian', $data572);		
		$this->db->replace('std5_penilaian', $data573);		
		$this->db->replace('std5_penilaian', $data574);		
		$this->db->replace('std5_penilaian', $data575);		
		return "success";
	}
}