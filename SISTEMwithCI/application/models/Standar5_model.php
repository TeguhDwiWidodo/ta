<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Standar5_model extends CI_Model {
	public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
	
	function get_data($table){
		$this->db->select('*');
		$this->db->from($table);
		if($table == 'std5_5121jumlahsksps'){
			$this->db->not_like('jenis_mata_kuliah', 'total');
		} else if($table == 'std5_5122strukturkurikulummk'){
			$this->db->not_like('smt', 'total');
		} else if($table == 'std5_513matkulpilihan'){
			$this->db->not_like('semester', 'total');
		} else if($table == 'std5_541daftardosenpembimbing'){
			$this->db->not_like('nama_dosen_pembimbing_akademik', 'total');
			$this->db->not_like('nama_dosen_pembimbing_akademik', 'rata-rata');
		}
		
		$query = $this->db->get();

		if ( $query->num_rows() > 0 ){
			$result = $query->result_array();
			return $result;
		} else {
			return "kosong";
		}
	}
	/*
	function get_data_511(){
		$this->db->select('*');
		$this->db->from('std5_511kompetensi');
		
		$query = $this->db->get();

		if ( $query->num_rows() > 0 ){
			$result = $query->result_array();
			return $result;
		} else {
			return "kosong";
		}
	}
	
	function get_data_5121(){
		$this->db->select('*');
		$this->db->from('std5_5121jumlahsksps');
		
		$query = $this->db->get();

		if ( $query->num_rows() > 0 ){
			$result = $query->result_array();
			return $result;
		} else {
			return "kosong";
		}
	}
	
	function get_data_5122(){
		$this->db->select('*');
		$this->db->from('std5_5122strukturkurikulummk');
		
		$query = $this->db->get();

		if ( $query->num_rows() > 0 ){
			$result = $query->result_array();
			return $result;
		} else {
			return "kosong";
		}
	}
	
	function get_data_513(){
		$this->db->select('*');
		$this->db->from('std5_513matkulpilihan');
		
		$query = $this->db->get();

		if ( $query->num_rows() > 0 ){
			$result = $query->result_array();
			return $result;
		} else {
			return "kosong";
		}
	}
	
	function get_data_514(){
		$this->db->select('*');
		$this->db->from('std5_514substansipraktikum');
		
		$query = $this->db->get();

		if ( $query->num_rows() > 0 ){
			$result = $query->result_array();
			return $result;
		} else {
			return "kosong";
		}
	}
	
	function get_data_52(){
		$this->db->select('*');
		$this->db->from('std5_52peninjauankurikulum');
		
		$query = $this->db->get();

		if ( $query->num_rows() > 0 ){
			$result = $query->result_array();
			return $result;
		} else {
			return "kosong";
		}
	}
	
	function get_data_531(){
		$this->db->select('*');
		$this->db->from('std5_531mekanismepenyusunan');
		
		$query = $this->db->get();

		if ( $query->num_rows() > 0 ){
			$result = $query->result_array();
			return $result;
		} else {
			return "kosong";
		}
	}
	
	function get_data_532(){
		$this->db->select('*');
		$this->db->from('std5_532contohsoalujian');
		
		$query = $this->db->get();

		if ( $query->num_rows() > 0 ){
			$result = $query->result_array();
			return $result;
		} else {
			return "kosong";
		}
	}
	
	function get_data_541(){
		$this->db->select('*');
		$this->db->from('std5_541daftardosenpembimbing');
		
		$query = $this->db->get();

		if ( $query->num_rows() > 0 ){
			$result = $query->result_array();
			return $result;
		} else {
			return "kosong";
		}
	}
	
	function get_data_542(){
		$this->db->select('*');
		$this->db->from('std5_542prosespembimbingan');
		
		$query = $this->db->get();

		if ( $query->num_rows() > 0 ){
			$result = $query->result_array();
			return $result;
		} else {
			return "kosong";
		}
	}
	
	function get_data_551(){
		$this->db->select('*');
		$this->db->from('std5_551pelaksanaanpembimbingan');
		
		$query = $this->db->get();

		if ( $query->num_rows() > 0 ){
			$result = $query->result_array();
			return $result;
		} else {
			return "kosong";
		}
	}
	
	function get_data_552(){
		$this->db->select('*');
		$this->db->from('std5_552ratapenyelesaianta');
		
		$query = $this->db->get();

		if ( $query->num_rows() > 0 ){
			$result = $query->result_array();
			return $result;
		} else {
			return "kosong";
		}
	}
	
	function get_data_56(){
		$this->db->select('*');
		$this->db->from('std5_56upayaperbaikanpembelajaran');
		
		$query = $this->db->get();

		if ( $query->num_rows() > 0 ){
			$result = $query->result_array();
			return $result;
		} else {
			return "kosong";
		}
	}
	
	function get_data_571(){
		$this->db->select('*');
		$this->db->from('std5_571kebijakan');
		
		$query = $this->db->get();

		if ( $query->num_rows() > 0 ){
			$result = $query->result_array();
			return $result;
		} else {
			return "kosong";
		}
	}
	
	function get_data_572(){
		$this->db->select('*');
		$this->db->from('std5_572ketersediaanprasarana');
		
		$query = $this->db->get();

		if ( $query->num_rows() > 0 ){
			$result = $query->result_array();
			return $result;
		} else {
			return "kosong";
		}
	}
	
	function get_data_573(){
		$this->db->select('*');
		$this->db->from('std5_573program');
		
		$query = $this->db->get();

		if ( $query->num_rows() > 0 ){
			$result = $query->result_array();
			return $result;
		} else {
			return "kosong";
		}
	}
	
	function get_data_574(){
		$this->db->select('*');
		$this->db->from('std5_574interaksi');
		
		$query = $this->db->get();

		if ( $query->num_rows() > 0 ){
			$result = $query->result_array();
			return $result;
		} else {
			return "kosong";
		}
	}
	
	function get_data_575(){
		$this->db->select('*');
		$this->db->from('std5_575pengembangan');
		
		$query = $this->db->get();

		if ( $query->num_rows() > 0 ){
			$result = $query->result_array();
			return $result;
		} else {
			return "kosong";
		}
	}
	
	*/
	function select_table_field_data($table){
		$this->db->select('*');
		$this->db->from($table);
		
		$query = $this->db->get();

		if ( $query->num_rows() > 0 ){
			$result = $query->field_data();
			return $result;
		} else {
			return "kosong";
		}
	}
	
	
	function edit_ckeditor($columns_data, $table, $no, $textarea_val){
		$this->db->select($columns_data[0][2]);
		$this->db->from($table);
		
		$query = $this->db->get();

		if ( $query->num_rows() > 0 ){
			$result = $query->result_array();
			if($result){
				$data = array(
					$columns_data[0][2] => $textarea_val
				);
				$this->db->where($columns_data[0][1], $no);
				$this->db->update($table, $data);	
				return "success";
			} else {
				$data = array(
					$columns_data[0][1] => $no,
					$columns_data[0][2] => $textarea_val
				);

				$this->db->insert($table, $data);
				return "success";
			}	
		} else {
			$data = array(
				$columns_data[0][1] => $no,
				$columns_data[0][2] => $textarea_val
			);

			$this->db->insert($table, $data);
			return "success";
		}
	}
	
	function editTbl_standar5($table, $numbColumns, $no, $col, $datatbl_val){
		
		if($numbColumns == 2){
			$data = array(
				$col[0] => $datatbl_val[0][1],
				$col[1] => $datatbl_val[0][2]
				);
		} else if($numbColumns == 3){
			$data = array(
				$col[0] => $datatbl_val[0][1],
				$col[1] => $datatbl_val[0][2],
				$col[2] => $datatbl_val[0][3]
				);	
		} else if($numbColumns == 4){
			$data = array(
				$col[0] => $datatbl_val[0][1],
				$col[1] => $datatbl_val[0][2],
				$col[2] => $datatbl_val[0][3],
				$col[3] => $datatbl_val[0][4]
				);
		} else if($numbColumns == 5){
			$data = array(
				$col[0] => $datatbl_val[0][1],
				$col[1] => $datatbl_val[0][2],
				$col[2] => $datatbl_val[0][3],
				$col[3] => $datatbl_val[0][4],
				$col[4] => $datatbl_val[0][5]
				);
		} else if($numbColumns == 6){
			$data = array(
				$col[0] => $datatbl_val[0][1],
				$col[1] => $datatbl_val[0][2],
				$col[2] => $datatbl_val[0][3],
				$col[3] => $datatbl_val[0][4],
				$col[4] => $datatbl_val[0][5],
				$col[5] => $datatbl_val[0][6]
				);
		} else if($numbColumns == 8){
			$data = array(
				$col[0] => $datatbl_val[0][1],
				$col[1] => $datatbl_val[0][2],
				$col[2] => $datatbl_val[0][3],
				$col[3] => $datatbl_val[0][4],
				$col[4] => $datatbl_val[0][5],
				$col[5] => $datatbl_val[0][6],
				$col[6] => $datatbl_val[0][7],
				$col[7] => $datatbl_val[0][8]
				);
		} else if($numbColumns == 12){
			$data = array(
				$col[0] => $datatbl_val[0][1],
				$col[1] => $datatbl_val[0][2],
				$col[2] => $datatbl_val[0][3],
				$col[3] => $datatbl_val[0][4],
				$col[4] => $datatbl_val[0][5],
				$col[5] => $datatbl_val[0][6],
				$col[6] => $datatbl_val[0][7],
				$col[7] => $datatbl_val[0][8],
				$col[8] => $datatbl_val[0][9],
				$col[9] => $datatbl_val[0][10],
				$col[10] => $datatbl_val[0][11],
				$col[11] => $datatbl_val[0][12]
				);
		} else {
			return "gagal";
		}
		
		$this->db->where('no', $no);
		$this->db->update($table, $data);	
		return "success";
	}
	
	// Jumlah Matakuliah Wajib + Pilihan
	function sumMatkulWajibPilihan(){
		$this->db->select_sum('sks');
		$this->db->not_like('jenis_mata_kuliah', 'khusus');
		$this->db->not_like('jenis_mata_kuliah', 'total');
		$this->db->not_like('jenis_mata_kuliah', 'optional');
		$this->db->from('std5_5121jumlahsksps');
		$query = $this->db->get();
		if ( $query->num_rows() > 0 ){
			$result = $query->result_array()[0]['sks'];
			return $result;
		} else {
			return 0;
		}
	}
	// sampe sini
	// Jumlah Bobot SKS, SKS MK dalam Kurikulum Inti dan Institusional
	function sumBobotIntiInstitusional(){
		$this->db->select('SUM(bobot_sks) AS bobotsks, SUM(sks_mk_dalam_kurikulum_inti) AS inti, SUM(sks_mk_dalam_kurikulum_Institusional) AS institusional');
		$this->db->not_like('smt', 'total');
		$this->db->from('std5_5122strukturkurikulummk');
		$query = $this->db->get();
		if ( $query->num_rows() > 0 ){
			$result = $query->result_array()[0];
			return $result;
		} else {
			return 0;
		}
	}
	// sampe sini
	// Jumlah MK pada kolom 7 yang diberi huruf "Ada"
	function getAllCountBobotTugas(){
		$this->db->like('bobot_tugas', 'Ada');
		$this->db->from('std5_5122strukturkurikulummk');
		$hasil = $this->db->count_all_results();
		return $hasil;
	}
	// sampe sini
	// Jumlah MK wajib + pilihan
	function getAllCountMK(){
		$this->db->where("kode_mk IS NOT NULL AND kode_mk !=''");
		$this->db->from('std5_5122strukturkurikulummk');
		$hasil = $this->db->count_all_results();
		return $hasil;
	}
	// sampe sini
	// Jumlah MK dengan SAP
	function getAllCountKelengkapanSAP(){
		$this->db->like('kelengkapan_sap', 'Ada');
		$this->db->from('std5_5122strukturkurikulummk');
		$hasil = $this->db->count_all_results();
		return $hasil;
	}
	// sampe sini
	// Jumlah SKS MK pilihan yang disediakan / Bobot SKS pada 5.1.3 Mata kuliah pilihan yang dilaksanakan dalam tiga tahun terakhir
	function sumSksMkPilihan(){
		$this->db->select_sum('bobot_sks');
		$this->db->where("kode_mk IS NOT NULL AND kode_mk !=''");
		$this->db->from('std5_513matkulpilihan');
		$query = $this->db->get();
		if ( $query->num_rows() > 0 ){
			$result = $query->result_array();
			return $result;
		} else {
			return 0;
		}
	}
	// sampe sini
	// Banyaknya dosen pembimbing akademik
	function getAllCountDosenPA(){
		$this->db->not_like('nama_dosen_pembimbing_akademik', 'rata-rata', 'after');
		$this->db->not_like('nama_dosen_pembimbing_akademik', 'total');
		$this->db->from('std5_541daftardosenpembimbing');
		$hasil = $this->db->count_all_results();
		return $hasil;
	}
	// sampe sini
	// Rata-rata banyak pertemuan per mhs per smt
	function getRataRataPertemuanMhsPerSmt(){
		$this->db->select_avg('rata_rata_banyaknya_pertemuan_mhs_smt');
		$this->db->not_like('nama_dosen_pembimbing_akademik', 'rata-rata', 'after');
		$this->db->not_like('nama_dosen_pembimbing_akademik', 'total');
		$this->db->from('std5_541daftardosenpembimbing');
		$query = $this->db->get();
		if ( $query->num_rows() > 0 ){
			$result = $query->row_array();
			return $result;
		} else {
			return 0;
		}
	}
	// sampe sini
	// Banyaknya mahasiswa bimbingan Pembimbing akademik
	function sumMhsBimbinganPA(){
		$this->db->select_sum('jumlah_mahasiswa_bimbingan');
		$this->db->not_like('nama_dosen_pembimbing_akademik', 'rata-rata', 'after');
		$this->db->not_like('nama_dosen_pembimbing_akademik', 'total');
		$this->db->from('std5_541daftardosenpembimbing');
		$query = $this->db->get();
		if ( $query->num_rows() > 0 ){
			$result = $query->result_array();
			return $result;
		} else {
			return 0;
		}
	}
	// sampe sini
	// Jumlah dosen pembimbing tugas akhir
	function getAllCountDosbingTA(){
		$this->db->not_like('nama_dosen_pembimbing', 'Total');
		$this->db->from('std5_551pelaksanaanpembimbingan');
		$hasil = $this->db->count_all_results();
		return $hasil;
	}
	// sampe sini
	// Jumlah mahasiswa tugas akhir
	function sumMhsTA(){
		$this->db->select_sum('jumlah_mahasiswa');
		$this->db->not_like('nama_dosen_pembimbing', 'total');
		$this->db->from('std5_551pelaksanaanpembimbingan');
		$query = $this->db->get();
		if ( $query->num_rows() > 0 ){
			$result = $query->row_array();
			return $result;
		} else {
			return 0;
		}
	}
	// sampe sini

	
	
}