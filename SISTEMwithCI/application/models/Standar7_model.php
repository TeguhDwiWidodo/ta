<?php
class Standar7_model extends CI_Model{


//------------------------- Model Kerjasama -----------------------------------
   function kerjasamaviewdalamnegerimodel()
   {

	   $this->db->like('instasi_negeri', 'Dalam', 'both');
	return $this->db->get('tb_kerjasama')->result();

   }



    function kerjasamaviewluarnegerimodel()
   {

	   $this->db->like('instasi_negeri', 'Luar', 'both');
	return $this->db->get('tb_kerjasama')->result();

   }



   function get_by_nip($id)
  {
$this->db->where('nip', $id);

return $this->db->get('tb_dosen')->num_rows();

  }


 function get_allpeldos($judul) {

        $this->db->like('judul',$judul,'both');
        return $this->db->get('tb_penelitian_dosen');
    }


    function getPeldosDetails($id){
      $this->db->select('*');
      $this->db->where('idpeldos', $id);
            return $this->db->get('tb_penelitian_dosen');
    }

    function getPeldostaDetails($id){
      $this->db->select('*');
      $this->db->where('id_peldos', $id);
            return $this->db->get('tb_peldosen_ta');
    }



  function dosenhapussemuamodel($table)
 {
  $this->db->truncate($table);

 }
  function dosenhapuschecklistmodel($table)
 {
  $check = json_decode($this->input->post('result'), true);
  $jumlah_dipilih = count($check);
  for($x=0;$x<$jumlah_dipilih;$x++){
    $this->db->where('no' , $check[$x]);
    $this->db->delete($table);
  }
 }
  function dosenahapusmodel($where,$table)
  {

 $this->db->where($where);
   $this->db->delete($table);
  }

	 function nilai_sementaraluar()
	{

	 $this->db->where('id', '7.3.2');
return $this->db->get('std7_penilaian')->result();

	}


  function pengupdatekerjasama()
 {

  $this->db->order_by('id', 'desc');
return $this->db->get('tb_kerjasama')->row();

 }

 function pengupdatedosen()
{

 $this->db->order_by('no', 'desc');
return $this->db->get('tb_dosen')->row();

}

function pengupdatehaki()
{

$this->db->order_by('id', 'desc');
return $this->db->get('tb_haki')->row();

}

function pengupdatepublikasi()
{

$this->db->order_by('id', 'desc');
return $this->db->get('tb_publikasi')->row();

}
function kerjasamahapusmodel($where,$table)
{

$this->db->where($where);
 $this->db->delete($table);
}

function penelitandosentahapusmodel($where,$table)
 {

$this->db->where($where);
  $this->db->delete($table);
 }

function pengupdatepenelitiandosen()
{

$this->db->order_by('peldos_no', 'desc');
return $this->db->get('tb_penelitian_dosen')->row();

}

 function pengupdateabdimas()
{

 $this->db->order_by('no', 'desc');
return $this->db->get('tb_abdimas')->row();

}

  function sum_nilai()
 {
   $this->db->select(' SUM(nilai) as nilai_count');

 $result = $this->db->get('std7_penilaian')->result();
 return $result[0]->nilai_count;
 }

 function sum_nilaihrkt()
{
  $this->db->select(' SUM(simulasi_nilai) as nilaihrkt_count');

$result = $this->db->get('std7_penilaian')->result();
return $result[0]->nilaihrkt_count;
}

   function  listhaki(){



     return $this->db->get('tb_haki')->result();

   }

   function  listhaki_getid($id){

   $this->db->where('id', $id);
     return $this->db->get('tb_haki')->row();

   }

   function hakidataupdate($id, $data)
    {
   $this->db->where('id', $id);
    $this->db->update('tb_haki', $data);
    }

    function penelitian_dosenupdate($id, $data)
     {
    $this->db->where('peldos_no', $id);
     $this->db->update('tb_penelitian_dosen', $data);
     }

     function penelitian_dosenvalid($id, $data)
      {
     $this->db->where('peldos_no', $id);
      $this->db->update('tb_penelitian_dosen', $data);
      }

      function abdimasvalid($id, $data)
       {
      $this->db->where('no', $id);
       $this->db->update('tb_abdimas', $data);
       }

       function publikasivalid($id, $data)
        {
       $this->db->where('id', $id);
        $this->db->update('tb_publikasi', $data);
        }

        function peldostavalid($id, $data)
         {
        $this->db->where('no', $id);
         $this->db->update('tb_peldosen_ta', $data);
         }

    function publikasidataupdate($id, $data)
     {
    $this->db->where('id', $id);
     $this->db->update('tb_publikasi', $data);
     }


   function hakihapusmodel($where,$table)
   {

 $this->db->where($where);
   $this->db->delete($table);
   }


   function penelitiandosenhapusmodel($where,$table)
   {

 $this->db->where($where);
   $this->db->delete($table);
   }

   function penelitiandosenhapuschecklistmodel($table)
  {
   $check = json_decode($this->input->post('result'), true);
   $jumlah_dipilih = count($check);
   for($x=0;$x<$jumlah_dipilih;$x++){
     $this->db->where('peldos_no' , $check[$x]);
     $this->db->delete($table);
   }
  }

   function hakimashapuschecklistmodel($table)
  {
   $check = json_decode($this->input->post('result'), true);
   $jumlah_dipilih = count($check);
   for($x=0;$x<$jumlah_dipilih;$x++){
     $this->db->where('id' , $check[$x]);
     $this->db->delete($table);
   }
}

   function hakishapussemuamodel($table)
  {
   $this->db->truncate($table);

  }

	function nilai_sementaradalam()
 {

	$this->db->where('id', '7.3.1');
return $this->db->get('std7_penilaian')->result();

 }
   function count_dn()
   {

	   $this->db->like('instasi_negeri', 'Dalam', 'both');
	return $this->db->get('tb_kerjasama')->num_rows();

   }

	 function count_ln()
   {

	   $this->db->like('instasi_negeri', 'Luar', 'both');
	return $this->db->get('tb_kerjasama')->num_rows();

   }

   function kerjasamainsert($data)
   {

	$this->db->insert('tb_kerjasama', $data);

   }

   function record_penilai($data)
   {

	$this->db->insert('record_penilai', $data);

   }

   function tb_7_2_2_abdimas($data)
   {

 $this->db->insert('tb_7_2_2_abdimas', $data);

   }
   function dosenupdate($id, $data)
    {
		$this->db->where('no', $id);
    $this->db->update('tb_dosen', $data);
    }
    function viewdosen()
    {

    return $this->db->get('tb_dosen')->result();

    }


    function viewpenelitian_dosenta()
    {


    return $this->db->get('tb_peldosen_ta')->result();

    }


    function viewpenelitian_dosenta_get_id()
    {
		$this->db->where('no', $id);

    return $this->db->get('tb_peldosen_ta')->result();

    }

    function viewpenelitian_dosenta_get_id1()
    {
		$this->db->where('no', $id);

    return $this->db->get('tb_peldosen_ta')->row();

    }

    function viewpenilaian()
    {


    return $this->db->get('std7_penilaian')->result();

    }

    function view722()
    {

$this->db->order_by("no","desc");
    return $this->db->get('tb_7_2_2_abdimas')->row();

    }
    function hitung_penilaian_tugas_akhir_dosen()
    {

    $this->db->where('jenis_penelitian', 'Penelitian TA');
    return $this->db->get('tb_penelitian_dosen')->num_rows();

    }

    function jumlah_mahasiswa_peldosta()
    {
    $this->db->where('sts_valid', 'valid');
    return $this->db->get('tb_peldosen_ta')->num_rows();

    }
    function jmldosen()
    {

      $this->db->where('status_dosen', 'Dosen_PS');
    return $this->db->get('tb_dosen')->num_rows();

    }
   function doseninsert($data)
   {

	$this->db->insert('tb_dosen', $data);

   }

   function hakiinsert($data)
   {

 $this->db->insert('tb_haki', $data);

   }

   function hakiupdatenilai($id, $data)
    {
   $this->db->where('id', $id);
    $this->db->update('std7_penilaian', $data);
    }


   function kerjasamadataupdate($id, $data)
    {
		$this->db->where('id', $id);
    $this->db->update('tb_kerjasama', $data);
    }


    function penelitian_tasiupdate($id, $data)
     {
     $this->db->where('no', $id);
     $this->db->update('tb_peldosen_ta', $data);
     }



		function kerjasamaupdateln($id, $data)
     {
 		$this->db->where('id', $id);
     $this->db->update('std7_penilaian', $data);
     }

		 function simulasinilai($id, $data)
      {
  		$this->db->where('id', $id);
      $this->db->update('std7_penilaian', $data);
      }

	  function kerjasamadatauploadpdf($id, $data)
    {
		$this->db->where('id', $id);
    $this->db->update('tb_kerjasama', $data);
    }

    function  penelitiandosentauploadpdf($id, $data)
    {
		$this->db->where('no', $id);
    $this->db->update('tb_peldosen_ta', $data);
    }

    function penelitiandosentahapuschecklistmodel($table)
   {
		$check = json_decode($this->input->post('result'), true);
		$jumlah_dipilih = count($check);
		for($x=0;$x<$jumlah_dipilih;$x++){



			$this->db->where('no' , $check[$x]);
			$this->db->delete($table);
		}
   }

    function viewpublikasi()
    {


    return $this->db->get('tb_publikasi')->result();

    }

    function viewpublikasi_getid($id)
    {
  		$this->db->where('id', $id);

    return $this->db->get('tb_publikasi')->row();

    }

    function publikasihapussemuamodel($table)
   {
    $this->db->truncate($table);

   }

   function publikasihapusmodel($where,$table)
   {

 $this->db->where($where);
   $this->db->delete($table);
   }


   function publikasihapuschecklistmodel($table)
  {
   $check = json_decode($this->input->post('result'), true);
   $jumlah_dipilih = count($check);
   for($x=0;$x<$jumlah_dipilih;$x++){
     $this->db->where('id' , $check[$x]);
     $this->db->delete($table);
   }
}

    function abdimasdataupdate($id, $data)
     {
     $this->db->where('no', $id);
     $this->db->update('tb_abdimas', $data);
     }

    function viewabdimas()
    {


    return $this->db->get('tb_abdimas')->result();

    }

    function abdimas_getid($id)
    {
 $this->db->where('no', $id);

    return $this->db->get('tb_abdimas')->row();

    }

    function abdimashapussemuamodel($table)
   {
    $this->db->truncate($table);

   }

   function abdimashapusmodel($where,$table)
   {

	$this->db->where($where);
		$this->db->delete($table);
   }

   function abdimashapuschecklistmodel($table)
  {
   $check = json_decode($this->input->post('result'), true);
   $jumlah_dipilih = count($check);
   for($x=0;$x<$jumlah_dipilih;$x++){
     $this->db->where('no' , $check[$x]);
     $this->db->delete($table);
   }
}
    function get_biaya_luar_negeri_abdimas_hitung_ts2()
   {
     $this->db->select(' COUNT(sumber_pembiayaan) as sumber_pembiayaan_ln_ab_ts2');
     $this->db->where('sumber_pembiayaan', 'Institusi luar negeri');
     $this->db->where('no_ts', 'TS_2');
     $this->db->where('sts_valid', 'valid');
   $result = $this->db->get('tb_abdimas')->result();
   return $result[0]->sumber_pembiayaan_ln_ab_ts2;
   }


   function get_biaya_luar_negeri_abdimas_hitung_ts1()
  {
    $this->db->select(' COUNT(sumber_pembiayaan) as sumber_pembiayaan_ln_ab_ts1');
    $this->db->where('sumber_pembiayaan', 'Institusi luar negeri');
    $this->db->where('no_ts', 'TS_1');
    $this->db->where('sts_valid', 'valid');
  $result = $this->db->get('tb_abdimas')->result();
  return $result[0]->sumber_pembiayaan_ln_ab_ts1;
  }

  function get_biaya_luar_negeri_abdimas_hitung_ts()
 {
   $this->db->select(' COUNT(sumber_pembiayaan) as sumber_pembiayaan_ln_ab_ts');
   $this->db->where('sumber_pembiayaan', 'Institusi luar negeri');
   $this->db->where('no_ts', 'TS');
   $this->db->where('sts_valid', 'valid');
 $result = $this->db->get('tb_abdimas')->result();
 return $result[0]->sumber_pembiayaan_ln_ab_ts;
 }
    function get_biaya_luar_negeri_abdimas()
   {
     $this->db->select(' COUNT(sumber_pembiayaan) as sumber_pembiayaan_ln_ab');
     $this->db->where('sumber_pembiayaan', 'Institusi luar negeri');
     $this->db->where('sts_valid', 'valid');
   $result = $this->db->get('tb_abdimas')->result();
   return $result[0]->sumber_pembiayaan_ln_ab;
   }

   function get_biaya_sendiri_abdimas()
  {
    $this->db->select('COUNT(sumber_pembiayaan) as sumber_pembiayaan_by_ab');
    $this->db->where('sumber_pembiayaan', 'pembiayaan sendiri oleh peneliti');
    $this->db->where('sts_valid', 'valid');
  $result = $this->db->get('tb_abdimas')->result();
  return $result[0]->sumber_pembiayaan_by_ab;
  }

  function get_biaya_sendiri_abdimas_ts2()
 {
   $this->db->select('COUNT(sumber_pembiayaan) as sumber_pembiayaan_by_ab_ts2');
   $this->db->where('sumber_pembiayaan', 'pembiayaan sendiri oleh peneliti');
      $this->db->where('no_ts', 'TS_2');
      $this->db->where('sts_valid', 'valid');
 $result = $this->db->get('tb_abdimas')->result();
 return $result[0]->sumber_pembiayaan_by_ab_ts2;
 }

 function get_biaya_sendiri_abdimas_ts1()
{
  $this->db->select('COUNT(sumber_pembiayaan) as sumber_pembiayaan_by_ab_ts1');
  $this->db->where('sumber_pembiayaan', 'pembiayaan sendiri oleh peneliti');
     $this->db->where('no_ts', 'TS_1');
     $this->db->where('sts_valid', 'valid');
$result = $this->db->get('tb_abdimas')->result();
return $result[0]->sumber_pembiayaan_by_ab_ts1;
}

function get_biaya_sendiri_abdimas_ts()
{
 $this->db->select('COUNT(sumber_pembiayaan) as sumber_pembiayaan_by_ab_ts');
 $this->db->where('sumber_pembiayaan', 'pembiayaan sendiri oleh peneliti');
    $this->db->where('no_ts', 'TS');
    $this->db->where('sts_valid', 'valid');
$result = $this->db->get('tb_abdimas')->result();
return $result[0]->sumber_pembiayaan_by_ab_ts;
}

  function get_pt_bersangkutan_abdimas()
 {
   $this->db->select('COUNT(sumber_pembiayaan) as sumber_pembiayaan_pt_ab');
   $this->db->where('sumber_pembiayaan', 'PT yang Bersangkutan');
   $this->db->where('sts_valid', 'valid');
 $result = $this->db->get('tb_abdimas')->result();
 return $result[0]->sumber_pembiayaan_pt_ab;
 }

 function get_pt_bersangkutan_abdimas_ts()
{
  $this->db->select('COUNT(sumber_pembiayaan) as sumber_pembiayaan_pt_ab_ts');
  $this->db->where('sumber_pembiayaan', 'PT yang Bersangkutan');
      $this->db->where('no_ts', 'TS');
      $this->db->where('sts_valid', 'valid');
$result = $this->db->get('tb_abdimas')->result();
return $result[0]->sumber_pembiayaan_pt_ab_ts;
}

function get_pt_bersangkutan_abdimas_ts1()
{
 $this->db->select('COUNT(sumber_pembiayaan) as sumber_pembiayaan_pt_ab');
 $this->db->where('sumber_pembiayaan', 'PT yang Bersangkutan');
    $this->db->where('no_ts', 'TS_1');
    $this->db->where('sts_valid', 'valid');
$result = $this->db->get('tb_abdimas')->result();
return $result[0]->sumber_pembiayaan_pt_ab;
}

function get_pt_bersangkutan_abdimas_ts2()
{
 $this->db->select('COUNT(sumber_pembiayaan) as sumber_pembiayaan_pt_ab_ts2');
 $this->db->where('sumber_pembiayaan', 'PT yang Bersangkutan');
     $this->db->where('no_ts', 'TS_2');
     $this->db->where('sts_valid', 'valid');
$result = $this->db->get('tb_abdimas')->result();
return $result[0]->sumber_pembiayaan_pt_ab_ts2;
}

 function get_depdiknas_abdimas()
 {
  $this->db->select('COUNT(sumber_pembiayaan) as sumber_pembiayaan_dp_ab');
  $this->db->where('sumber_pembiayaan', 'Depdiknas');
  $this->db->where('sts_valid', 'valid');
 $result = $this->db->get('tb_abdimas')->result();
 return $result[0]->sumber_pembiayaan_dp_ab;
 }



 function get_depdiknas_abdimas_ts()
 {
  $this->db->select('COUNT(sumber_pembiayaan) as sumber_pembiayaan_dp_ab_ts');
  $this->db->where('sumber_pembiayaan', 'Depdiknas');
       $this->db->where('no_ts', 'TS');
       $this->db->where('sts_valid', 'valid');
 $result = $this->db->get('tb_abdimas')->result();
 return $result[0]->sumber_pembiayaan_dp_ab_ts;
 }


 function get_depdiknas_abdimas_ts1()
 {
  $this->db->select('COUNT(sumber_pembiayaan) as sumber_pembiayaan_dp_ab_ts1');
  $this->db->where('sumber_pembiayaan', 'Depdiknas');
         $this->db->where('no_ts', 'TS_1');
         $this->db->where('sts_valid', 'valid');
 $result = $this->db->get('tb_abdimas')->result();
 return $result[0]->sumber_pembiayaan_dp_ab_ts1;
 }


 function get_depdiknas_abdimas_ts2()
 {
  $this->db->select('COUNT(sumber_pembiayaan) as sumber_pembiayaan_dp_ab_ts2');
  $this->db->where('sumber_pembiayaan', 'Depdiknas');
         $this->db->where('no_ts', 'TS_2');
         $this->db->where('sts_valid', 'valid');
 $result = $this->db->get('tb_abdimas')->result();
 return $result[0]->sumber_pembiayaan_dp_ab_ts2;
 }

 function get_luar_depdiknas_abdimas()
 {
  $this->db->select('COUNT(sumber_pembiayaan) as sumber_pembiayaan_dep_ab');
  $this->db->where('sumber_pembiayaan', 'Institusi dalam negeri diluar depdiknas');
  $this->db->where('sts_valid', 'valid');
 $result = $this->db->get('tb_abdimas')->result();
 return $result[0]->sumber_pembiayaan_dep_ab;
 }

 function get_luar_depdiknas_abdimas_ts2()
 {
  $this->db->select('COUNT(sumber_pembiayaan) as sumber_pembiayaan_dep_ab_ts2');
  $this->db->where('sumber_pembiayaan', 'Institusi dalam negeri diluar depdiknas');
           $this->db->where('no_ts', 'TS_2');
           $this->db->where('sts_valid', 'valid');
 $result = $this->db->get('tb_abdimas')->result();
 return $result[0]->sumber_pembiayaan_dep_ab_ts2;
 }
 function get_luar_depdiknas_abdimas_ts1()
 {
  $this->db->select('COUNT(sumber_pembiayaan) as sumber_pembiayaan_dep_ab_ts1');
  $this->db->where('sumber_pembiayaan', 'Institusi dalam negeri diluar depdiknas');
           $this->db->where('no_ts', 'TS_1');
           $this->db->where('sts_valid', 'valid');
 $result = $this->db->get('tb_abdimas')->result();
 return $result[0]->sumber_pembiayaan_dep_ab_ts1;
 }
 function get_luar_depdiknas_abdimas_ts()
 {
  $this->db->select('COUNT(sumber_pembiayaan) as sumber_pembiayaan_dep_ab_ts');
  $this->db->where('sumber_pembiayaan', 'Institusi dalam negeri diluar depdiknas');
           $this->db->where('no_ts', 'TS');
           $this->db->where('sts_valid', 'valid');
 $result = $this->db->get('tb_abdimas')->result();
 return $result[0]->sumber_pembiayaan_dep_ab_ts;
 }


   function get_biaya_luar_negeri()
  {
    $this->db->select(' COUNT(sumber_pembiayaan) as sumber_pembiayaan_ln');
    $this->db->where('sumber_pembiayaan', 'Institusi luar negeri');
    $this->db->where('sts_valid', 'valid');
  $result = $this->db->get('tb_penelitian_dosen')->result();
  return $result[0]->sumber_pembiayaan_ln;
  }


  function get_biaya_luar_negeri_ts()
 {
   $this->db->select(' COUNT(sumber_pembiayaan) as sumber_pembiayaan_ln_ts');
   $this->db->where('sumber_pembiayaan', 'Institusi luar negeri');
   $this->db->where('no_ts', 'TS');
   $this->db->where('sts_valid', 'valid');
 $result = $this->db->get('tb_penelitian_dosen')->result();
 return $result[0]->sumber_pembiayaan_ln_ts;
 }

 function get_biaya_luar_negeri_ts1()
{
  $this->db->select(' COUNT(sumber_pembiayaan) as sumber_pembiayaan_ln_ts1');
  $this->db->where('sumber_pembiayaan', 'Institusi luar negeri');
  $this->db->where('no_ts', 'TS_1');
  $this->db->where('sts_valid', 'valid');
$result = $this->db->get('tb_penelitian_dosen')->result();
return $result[0]->sumber_pembiayaan_ln_ts1;
}
function get_biaya_luar_negeri_ts2()
{
 $this->db->select(' COUNT(sumber_pembiayaan) as sumber_pembiayaan_ln_ts2');
 $this->db->where('sumber_pembiayaan', 'Institusi luar negeri');
 $this->db->where('no_ts', 'TS_2');
 $this->db->where('sts_valid', 'valid');
$result = $this->db->get('tb_penelitian_dosen')->result();
return $result[0]->sumber_pembiayaan_ln_ts2;
}

  function get_biaya_sendiri()
 {
   $this->db->select('COUNT(sumber_pembiayaan) as sumber_pembiayaan_by');
   $this->db->where('sumber_pembiayaan', 'pembiayaan sendiri oleh peneliti');
   $this->db->where('sts_valid', 'valid');
 $result = $this->db->get('tb_penelitian_dosen')->result();
 return $result[0]->sumber_pembiayaan_by;
 }

 function get_biaya_sendiri_ts()
{
  $this->db->select('COUNT(sumber_pembiayaan) as sumber_pembiayaan_by_ts');
  $this->db->where('sumber_pembiayaan', 'pembiayaan sendiri oleh peneliti');
   $this->db->where('no_ts', 'TS');
   $this->db->where('sts_valid', 'valid');
$result = $this->db->get('tb_penelitian_dosen')->result();
return $result[0]->sumber_pembiayaan_by_ts;
}

function get_biaya_sendiri_ts1()
{
 $this->db->select('COUNT(sumber_pembiayaan) as sumber_pembiayaan_by_ts1');
 $this->db->where('sumber_pembiayaan', 'pembiayaan sendiri oleh peneliti');
  $this->db->where('no_ts', 'TS_1');
  $this->db->where('sts_valid', 'valid');
$result = $this->db->get('tb_penelitian_dosen')->result();
return $result[0]->sumber_pembiayaan_by_ts1;
}


function get_biaya_sendiri_ts2()
{
 $this->db->select('COUNT(sumber_pembiayaan) as sumber_pembiayaan_by_ts2');
 $this->db->where('sumber_pembiayaan', 'pembiayaan sendiri oleh peneliti');
  $this->db->where('no_ts', 'TS_2');
  $this->db->where('sts_valid', 'valid');
$result = $this->db->get('tb_penelitian_dosen')->result();
return $result[0]->sumber_pembiayaan_by_ts2;
}

 function get_pt_bersangkutan()
{
  $this->db->select('COUNT(sumber_pembiayaan) as sumber_pembiayaan_pt');
  $this->db->where('sumber_pembiayaan', 'PT yang Bersangkutan');
  $this->db->where('sts_valid', 'valid');
$result = $this->db->get('tb_penelitian_dosen')->result();
return $result[0]->sumber_pembiayaan_pt;
}

function get_pt_bersangkutan_ts()
{
 $this->db->select('COUNT(sumber_pembiayaan) as sumber_pembiayaan_pt_ts');
 $this->db->where('sumber_pembiayaan', 'PT yang Bersangkutan');
   $this->db->where('no_ts', 'TS');
   $this->db->where('sts_valid', 'valid');
$result = $this->db->get('tb_penelitian_dosen')->result();
return $result[0]->sumber_pembiayaan_pt_ts;
}


function get_pt_bersangkutan_ts1()
{
 $this->db->select('COUNT(sumber_pembiayaan) as sumber_pembiayaan_pt_ts1');
 $this->db->where('sumber_pembiayaan', 'PT yang Bersangkutan');
   $this->db->where('no_ts', 'TS_1');
   $this->db->where('sts_valid', 'valid');
$result = $this->db->get('tb_penelitian_dosen')->result();
return $result[0]->sumber_pembiayaan_pt_ts1;
}


function get_pt_bersangkutan_ts2()
{
 $this->db->select('COUNT(sumber_pembiayaan) as sumber_pembiayaan_pt_ts2');
 $this->db->where('sumber_pembiayaan', 'PT yang Bersangkutan');
    $this->db->where('no_ts', 'TS_2');
    $this->db->where('sts_valid', 'valid');
$result = $this->db->get('tb_penelitian_dosen')->result();
return $result[0]->sumber_pembiayaan_pt_ts2;
}



function get_depdiknas()
{
 $this->db->select('COUNT(sumber_pembiayaan) as sumber_pembiayaan_dp');
 $this->db->where('sumber_pembiayaan', 'Depdiknas');
 $this->db->where('sts_valid', 'valid');
$result = $this->db->get('tb_penelitian_dosen')->result();
return $result[0]->sumber_pembiayaan_dp;
}


function get_depdiknas_ts()
{
 $this->db->select('COUNT(sumber_pembiayaan) as sumber_pembiayaan_dp_ts');
 $this->db->where('sumber_pembiayaan', 'Depdiknas');
     $this->db->where('no_ts', 'TS');
     $this->db->where('sts_valid', 'valid');
$result = $this->db->get('tb_penelitian_dosen')->result();
return $result[0]->sumber_pembiayaan_dp_ts;
}

function get_depdiknas_ts1()
{
 $this->db->select('COUNT(sumber_pembiayaan) as sumber_pembiayaan_dp_ts1');
 $this->db->where('sumber_pembiayaan', 'Depdiknas');
     $this->db->where('no_ts', 'TS_1');
     $this->db->where('sts_valid', 'valid');
$result = $this->db->get('tb_penelitian_dosen')->result();
return $result[0]->sumber_pembiayaan_dp_ts1;
}

function get_depdiknas_ts2()
{
 $this->db->select('COUNT(sumber_pembiayaan) as sumber_pembiayaan_dp_ts2');
 $this->db->where('sumber_pembiayaan', 'Depdiknas');
      $this->db->where('no_ts', 'TS_2');
      $this->db->where('sts_valid', 'valid');
$result = $this->db->get('tb_penelitian_dosen')->result();
return $result[0]->sumber_pembiayaan_dp_ts2;
}

function get_luar_depdiknas()
{
 $this->db->select('COUNT(sumber_pembiayaan) as sumber_pembiayaan_dep');
 $this->db->where('sumber_pembiayaan', 'Institusi dalam negeri diluar depdiknas');
 $this->db->where('sts_valid', 'valid');
$result = $this->db->get('tb_penelitian_dosen')->result();
return $result[0]->sumber_pembiayaan_dep;
}

function get_luar_depdiknas_ts()
{
 $this->db->select('COUNT(sumber_pembiayaan) as sumber_pembiayaan_dep_ts');
 $this->db->where('sumber_pembiayaan', 'Institusi dalam negeri diluar depdiknas');
       $this->db->where('no_ts', 'TS');
       $this->db->where('sts_valid', 'valid');
$result = $this->db->get('tb_penelitian_dosen')->result();
return $result[0]->sumber_pembiayaan_dep_ts;
}


function get_luar_depdiknas_ts1()
{
 $this->db->select('COUNT(sumber_pembiayaan) as sumber_pembiayaan_dep_ts1');
 $this->db->where('sumber_pembiayaan', 'Institusi dalam negeri diluar depdiknas');
       $this->db->where('no_ts', 'TS_1');
       $this->db->where('sts_valid', 'valid');
$result = $this->db->get('tb_penelitian_dosen')->result();
return $result[0]->sumber_pembiayaan_dep_ts1;
}


function get_luar_depdiknas_ts2()
{
 $this->db->select('COUNT(sumber_pembiayaan) as sumber_pembiayaan_dep_ts2');
 $this->db->where('sumber_pembiayaan', 'Institusi dalam negeri diluar depdiknas');
       $this->db->where('no_ts', 'TS_2');
       $this->db->where('sts_valid', 'valid');
$result = $this->db->get('tb_penelitian_dosen')->result();
return $result[0]->sumber_pembiayaan_dep_ts2;
}

function get_dsninter()
{
 $this->db->select_sum('jlm_dosen_si', 'dosen_inter');
 $this->db->where('skl_publikasi', 'Internasional');
 $this->db->where('sts_valid', 'valid');
$result = $this->db->get('tb_publikasi')->result();
return $result[0]->dosen_inter;
}
   function get_dsnnsl()
  {
     $this->db->select_sum('jlm_dosen_si', 'dosen_nasional');
    $this->db->where('skl_publikasi', 'Nasional');
    $this->db->where('sts_valid', 'valid');
   $result = $this->db->get('tb_publikasi')->result();
   return $result[0]->dosen_nasional;
   }
  function get_dsnlkl()
 {
    $this->db->select_sum('jlm_dosen_si', 'dosen_lokal');
   $this->db->where('skl_publikasi', 'Lokal');
   $this->db->where('sts_valid', 'valid');
$result = $this->db->get('tb_publikasi')->result();
 return $result[0]->dosen_lokal;
  }

  function penelitiandosenviewmodel()
 {
 return $this->db->get('tb_penelitian_dosen')->result();
 }

 function penelitiandosenviewmodel1($id)
{
    $this->db->where('peldos_no', $id);
return $this->db->get('tb_penelitian_dosen')->row();
}

 function penelitiandosentaviewmodel_getid($id)
{

  $this->db->where('no', $id);
return $this->db->get('tb_peldosen_ta')->row();
}

function penelitiantaviewmodel_getid($id)
{

 $this->db->where('no', $id);
return $this->db->get('tb_peldosen_ta')->row();
}



 function penelitianhapussemuamodel($table)
{
 $this->db->truncate($table);

}

function peldosentahapussemuamodel($table)
{
$this->db->truncate($table);

}

function peltahapussemuamodel($table)
{
$this->db->truncate($table);

}

 function kerjasamaviewmodel()
{
return $this->db->get('tb_kerjasama')->result();
}




 function kerjasamaviewmodel_getid($id){

$this->db->where('id' , $id);
return $this->db->get('tb_kerjasama')->row();
}

function record_penilaian_view()
{
$this->db->from('record_penilai');
$this->db->order_by("date desc");
$this->db->limit('20');
return $this->db->get()->result();
}



    function kerjasamahapussemuamodel($table)
   {
		$this->db->truncate($table);

   }

    function kerjasamahapuschecklistmodel($table)
   {
		$check = json_decode($this->input->post('result'), true);
		$jumlah_dipilih = count($check);
		for($x=0;$x<$jumlah_dipilih;$x++){



			$this->db->where('id' , $check[$x]);
			$this->db->delete($table);
		}
   }
 //------------------------- Model Kerjasama end -----------------------------------

 function viewpenelitian_ta()
 {


 return $this->db->get('tb_tamahasiswa')->result();

 }

 function jumlah_mahasiswa_pelta()
 {

 return $this->db->get('tb_tamahasiswa')->num_rows();

 }

 function penelitian_taupdate($id, $data)
  {
  $this->db->where('no', $id);
  $this->db->update('tb_tamahasiswa', $data);
  }

  function get_judul($q){
    $this->db->select('*');
    $this->db->like('judul', $q);
    $query = $this->db->get('tb_penelitian_dosen');
    if($query->num_rows() > 0){
      foreach ($query->result_array() as $row){
        $new_row['label']=htmlentities(stripslashes($row['judul']));
        $new_row['value']=htmlentities(stripslashes($row['nama_dosenketua']));
        $row_set[] = $new_row; //build an array
      }
      echo json_encode($row_set); //format the array into json data
    }
  }

  function versionpdf($data)
  {

 $this->db->insert('report_version', $data);

  }

  function viewversionpdf()
  {
    $this->db->order_by("no","desc");
    $this->db->limit('5');
  return $this->db->get('report_version')->result();

  }



  function number_ts()
 {
  $this->db->order_by('no', 'desc');
return $this->db->get('numberts')->result();
 }

    function number_tsinsert($data)
    {

 	$this->db->insert('numberts', $data);

    }

    function number_tsupdate($id, $data)
     {
     $this->db->where('no', $id);
     $this->db->update('numberts', $data);
     }

     function number_tshapus($where,$table)
     {

    $this->db->where($where);
      $this->db->delete($table);
     }
}
?>
