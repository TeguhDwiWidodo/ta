<?php

class File_model extends CI_Model {
	public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
	
	function insert_history_upload_excel($inputfilename, $table, $saveDate, $iduser, $excel_path, $excelhtml_path){
		$data = array(
			'nama_file' 		=> $inputfilename,
			'tabel_db' 			=> $table,
			'tanggal' 			=> $saveDate,
			'id_user' 			=> $iduser,
			'excel_path' 		=> $excel_path,
			'excelhtml_path' 	=> $excelhtml_path
		);

		$this->db->insert('std5_history_uploadfile', $data);
		return "sukses";
	}
	
	function get_riwayat_file($table_db_value, $info){
		$this->db->select('std5_history_uploadfile.*, users.nama');
		$this->db->from('std5_history_uploadfile');
		$this->db->join('users', 'std5_history_uploadfile.id_user = users.id_user', 'inner');
		$this->db->where('tabel_db', $table_db_value);
		$this->db->order_by('tanggal', 'DESC');
		$this->db->limit(5);
		
		$query = $this->db->get();

		if ( $query->num_rows() > 0 ){
			$result = $query->result_array();
			return $result;
		} else {
			return "kosong";
		}
	}
	
	function upload_excel($row, $table, $highestRow, $highestColumn, $updatedheadings, $rowData){
		$this->db->select('*');
		$this->db->from($table);
		
		$query = $this->db->get();
		
		if ( $query->num_rows() > 0 ){
			$result = $query->result_array();
			if($result){ 
				if($highestColumn == 'C' ){
					$data = array(
						$updatedheadings[0] => $rowData[0][0],
						$updatedheadings[1] => $rowData[0][1],
						$updatedheadings[2] => $rowData[0][2]
					);	
				} else if($highestColumn == 'D' ){
					$data = array(
						$updatedheadings[0] => $rowData[0][0],
						$updatedheadings[1] => $rowData[0][1],
						$updatedheadings[2] => $rowData[0][2],
						$updatedheadings[3] => $rowData[0][3]
					);	
				} else if($highestColumn == 'E' ){
					$data = array(
						$updatedheadings[0] => $rowData[0][0],
						$updatedheadings[1] => $rowData[0][1],
						$updatedheadings[2] => $rowData[0][2],
						$updatedheadings[3] => $rowData[0][3],
						$updatedheadings[4] => $rowData[0][4]
					);	
				} else if($highestColumn == 'F' ){
					$data = array(
						$updatedheadings[0] => $rowData[0][0],
						$updatedheadings[1] => $rowData[0][1],
						$updatedheadings[2] => $rowData[0][2],
						$updatedheadings[3] => $rowData[0][3],
						$updatedheadings[4] => $rowData[0][4],
						$updatedheadings[5] => $rowData[0][5]
					);	
				} else if($highestColumn == 'I' ){
					$data = array(
						$updatedheadings[0] => $rowData[0][0],
						$updatedheadings[1] => $rowData[0][1],
						$updatedheadings[2] => $rowData[0][2],
						$updatedheadings[3] => $rowData[0][3],
						$updatedheadings[4] => $rowData[0][4],
						$updatedheadings[5] => $rowData[0][5],
						$updatedheadings[6] => $rowData[0][6],
						$updatedheadings[7] => $rowData[0][7],
						$updatedheadings[8] => $rowData[0][8]
					);	
				} else if($highestColumn == 'L' ){
					$data = array(
						$updatedheadings[0] => $rowData[0][0],
						$updatedheadings[1] => $rowData[0][1],
						$updatedheadings[2] => $rowData[0][2],
						$updatedheadings[3] => $rowData[0][3],
						$updatedheadings[4] => $rowData[0][4],
						$updatedheadings[5] => $rowData[0][5],
						$updatedheadings[6] => $rowData[0][6],
						$updatedheadings[7] => $rowData[0][7]
					);	
				} else if($highestColumn == 'H' ){
					$data = array(
						$updatedheadings[0] => $rowData[0][0],
						$updatedheadings[1] => $rowData[0][1],
						$updatedheadings[2] => $rowData[0][2],
						$updatedheadings[3] => $rowData[0][3],
						$updatedheadings[4] => $rowData[0][4],
						$updatedheadings[5] => $rowData[0][5],
						$updatedheadings[6] => $rowData[0][6],
						$updatedheadings[7] => $rowData[0][7],
						$updatedheadings[8] => $rowData[0][8],
						$updatedheadings[9] => $rowData[0][9],
						$updatedheadings[10] => $rowData[0][10],
						$updatedheadings[11] => $rowData[0][11]
					);	
				}
					
				//$this->db->where($updatedheadings[0], $rowData[0][0]);
				$this->db->replace($table, $data);	
				return "success";
			} else {
				if($highestColumn == 'C' ){
					$data = array(
						$updatedheadings[0] => $rowData[0][0],
						$updatedheadings[1] => $rowData[0][1],
						$updatedheadings[2] => $rowData[0][2]
					);	
				} else if($highestColumn == 'D' ){
					$data = array(
						$updatedheadings[0] => $rowData[0][0],
						$updatedheadings[1] => $rowData[0][1],
						$updatedheadings[2] => $rowData[0][2],
						$updatedheadings[3] => $rowData[0][3]
					);	
				} else if($highestColumn == 'E' ){
					$data = array(
						$updatedheadings[0] => $rowData[0][0],
						$updatedheadings[1] => $rowData[0][1],
						$updatedheadings[2] => $rowData[0][2],
						$updatedheadings[3] => $rowData[0][3],
						$updatedheadings[4] => $rowData[0][4]
					);	
				} else if($highestColumn == 'F' ){
					$data = array(
						$updatedheadings[0] => $rowData[0][0],
						$updatedheadings[1] => $rowData[0][1],
						$updatedheadings[2] => $rowData[0][2],
						$updatedheadings[3] => $rowData[0][3],
						$updatedheadings[4] => $rowData[0][4],
						$updatedheadings[5] => $rowData[0][5]
					);	
				} else if($highestColumn == 'I' ){
					$data = array(
						$updatedheadings[0] => $rowData[0][0],
						$updatedheadings[1] => $rowData[0][1],
						$updatedheadings[2] => $rowData[0][2],
						$updatedheadings[3] => $rowData[0][3],
						$updatedheadings[4] => $rowData[0][4],
						$updatedheadings[5] => $rowData[0][5],
						$updatedheadings[6] => $rowData[0][6],
						$updatedheadings[7] => $rowData[0][7],
						$updatedheadings[8] => $rowData[0][8]
					);	
				} else if($highestColumn == 'L' ){
					$data = array(
						$updatedheadings[0] => $rowData[0][0],
						$updatedheadings[1] => $rowData[0][1],
						$updatedheadings[2] => $rowData[0][2],
						$updatedheadings[3] => $rowData[0][3],
						$updatedheadings[4] => $rowData[0][4],
						$updatedheadings[5] => $rowData[0][5],
						$updatedheadings[6] => $rowData[0][6],
						$updatedheadings[7] => $rowData[0][7]
					);	
				} else if($highestColumn == 'H' ){
					$data = array(
						$updatedheadings[0] => $rowData[0][0],
						$updatedheadings[1] => $rowData[0][1],
						$updatedheadings[2] => $rowData[0][2],
						$updatedheadings[3] => $rowData[0][3],
						$updatedheadings[4] => $rowData[0][4],
						$updatedheadings[5] => $rowData[0][5],
						$updatedheadings[6] => $rowData[0][6],
						$updatedheadings[7] => $rowData[0][7],
						$updatedheadings[8] => $rowData[0][8],
						$updatedheadings[9] => $rowData[0][9],
						$updatedheadings[10] => $rowData[0][10],
						$updatedheadings[11] => $rowData[0][11]
					);	
				}
			$this->db->replace($table, $data);
			return "success";
			}
		} else {
			if($highestColumn == 'C' ){
				$data = array(
					$updatedheadings[0] => $rowData[0][0],
					$updatedheadings[1] => $rowData[0][1],
					$updatedheadings[2] => $rowData[0][2]
				);	
			} else if($highestColumn == 'D' ){
				$data = array(
					$updatedheadings[0] => $rowData[0][0],
					$updatedheadings[1] => $rowData[0][1],
					$updatedheadings[2] => $rowData[0][2],
					$updatedheadings[3] => $rowData[0][3]
				);	
			} else if($highestColumn == 'E' ){
				$data = array(
					$updatedheadings[0] => $rowData[0][0],
					$updatedheadings[1] => $rowData[0][1],
					$updatedheadings[2] => $rowData[0][2],
					$updatedheadings[3] => $rowData[0][3],
					$updatedheadings[4] => $rowData[0][4]
				);	
			} else if($highestColumn == 'F' ){
				$data = array(
					$updatedheadings[0] => $rowData[0][0],
					$updatedheadings[1] => $rowData[0][1],
					$updatedheadings[2] => $rowData[0][2],
					$updatedheadings[3] => $rowData[0][3],
					$updatedheadings[4] => $rowData[0][4],
					$updatedheadings[5] => $rowData[0][5]
				);	
			} else if($highestColumn == 'I' ){
				$data = array(
					$updatedheadings[0] => $rowData[0][0],
					$updatedheadings[1] => $rowData[0][1],
					$updatedheadings[2] => $rowData[0][2],
					$updatedheadings[3] => $rowData[0][3],
					$updatedheadings[4] => $rowData[0][4],
					$updatedheadings[5] => $rowData[0][5],
					$updatedheadings[6] => $rowData[0][6],
					$updatedheadings[7] => $rowData[0][7],
					$updatedheadings[8] => $rowData[0][8]
				);	
			} else if($highestColumn == 'L' ){
				$data = array(
					$updatedheadings[0] => $rowData[0][0],
					$updatedheadings[1] => $rowData[0][1],
					$updatedheadings[2] => $rowData[0][2],
					$updatedheadings[3] => $rowData[0][3],
					$updatedheadings[4] => $rowData[0][4],
					$updatedheadings[5] => $rowData[0][5],
					$updatedheadings[6] => $rowData[0][6],
					$updatedheadings[7] => $rowData[0][7]
				);	
			} else if($highestColumn == 'H' ){
				$data = array(
					$updatedheadings[0] => $rowData[0][0],
					$updatedheadings[1] => $rowData[0][1],
					$updatedheadings[2] => $rowData[0][2],
					$updatedheadings[3] => $rowData[0][3],
					$updatedheadings[4] => $rowData[0][4],
					$updatedheadings[5] => $rowData[0][5],
					$updatedheadings[6] => $rowData[0][6],
					$updatedheadings[7] => $rowData[0][7],
					$updatedheadings[8] => $rowData[0][8],
					$updatedheadings[9] => $rowData[0][9],
					$updatedheadings[10] => $rowData[0][10],
					$updatedheadings[11] => $rowData[0][11]
				);	
			}

			$this->db->replace($table, $data);
			return "success";
		}	
	}
	
	function insertFilePendukung($final_fileName, $poinmateri, $saveDate, $iduser, $pathfile){
		$data = array(
			'nama_file' 		=> $final_fileName,
			'poindata' 			=> $poinmateri,
			'uploaddate' 		=> $saveDate,
			'id_user' 			=> $iduser,
			'filepath' 			=> $pathfile
		);

		$this->db->insert('std5_filependukung', $data);
		return "sukses";
	}
	
	function selectFilePendukung($poinmateri){
		$this->db->select('std5_filependukung.*, users.nama');
		$this->db->from('std5_filependukung');
		$this->db->join('users', 'std5_filependukung.id_user = users.id_user', 'inner');
		$this->db->where('poindata', $poinmateri);
		$this->db->order_by('uploaddate', 'DESC');
		$this->db->limit(3);
		
		$query = $this->db->get();

		if ( $query->num_rows() > 0 ){
			$result = $query->result_array();
			return $result;
		} else {
			return "kosong";
		}
	}
}