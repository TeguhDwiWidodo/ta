<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users_model extends CI_Model {


	public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

		function get_userinfo($id)
	    {
	     $this->db->where('email', $id);
	        $query = $this->db->get('users');

	        return $query->result();
	    }

	function login($username, $password){
		$this->db->select('*');
		$this->db->from('users');
		$this->db->where('email', $username );
		// $this->db->where('password', $password );
		$this->db->limit(1);

		$query = $this->db->get();

		if ( $query->num_rows() > 0 ){
			$row = $query->row_array();
			return $row;
		} else {
			return "kosong";
		}
	}

	function addUser($email, $password, $nama, $nohp, $hakakses, $jenis_kelamin){
		$data = array(
        'email' 		=> $email,
        'nama' 			=> $nama,
        'password' 		=> $password,
        'hp' 			=> $nohp,
        'hak_akses' 	=> $hakakses,
        'jenis_kelamin' => $jenis_kelamin
		);

		$this->db->insert('users', $data);
		return "sukses";
	}

	function get_all_tim_akreditasi(){
		$this->db->select('id_user,email,nama,hp,hak_akses,tugas');
		$this->db->from('users');
		$hak_akses = array('admin', 'kaprodi');
		$this->db->where_not_in('hak_akses', $hak_akses );

		$query = $this->db->get();

		if ( $query->num_rows() > 0 ){
			$result = $query->result_array();
			return $result;
		} else {
			return "kosong";
		}
	}

	function get_all_users(){
		$this->db->select('id_user,email,nama,hp,hak_akses,tugas,alamat,jenis_kelamin');
		$this->db->from('users');

		$query = $this->db->get();

		if ( $query->num_rows() > 0 ){
			$result = $query->result_array();
			return $result;
		} else {
			return "kosong";
		}
	}


	function edit_tugas_user_akreditasi($iduser, $tugas_val){
		$data = array(
				'tugas' => $tugas_val
				);
		$this->db->where('id_user', $iduser);
		$this->db->update('users', $data);
		return "success";
	}

	function display_profile($userid){
		$this->db->select('*');
		$this->db->from('users');
		$this->db->where('id_user', $userid);
		$this->db->limit(1);

		$query = $this->db->get();

		if ( $query->num_rows() > 0 ){
			$row = $query->row_array();
			return $row;
		} else {
			return "kosong";
		}
	}

	function get_one_user($userid){
		$this->db->select('*');
		$this->db->from('users');
		$this->db->where('id_user', $userid);
		$this->db->limit(1);

		$query = $this->db->get();

		if ( $query->num_rows() > 0 ){
			$row = $query->result_array();
			return $row;
		} else {
			return "kosong";
		}
	}

	function edit_profileinfo($session_iduser, $useremail, $usernama, $userjenisk, $useralamat, $usernohp, $hak_akses){
		if(empty($userjenisk)){
			$data = array(
				'email' => $useremail,
				'nama' => $usernama,
				'alamat' => $useralamat,
				'hak_akses' => $hak_akses,
				'hp' => $usernohp
				);
			$this->db->where('id_user', $session_iduser);
			$this->db->update('users', $data);
			return "success";
		} else if(empty($hak_akses)){
			$data = array(
				'email' => $useremail,
				'nama' => $usernama,
				'jenis_kelamin' => $userjenisk,
				'alamat' => $useralamat,
				'hp' => $usernohp
					);
			$this->db->where('id_user', $session_iduser);
			$this->db->update('users', $data);
			return "success";
		} else {
			$data = array(
				'email' => $useremail,
				'nama' => $usernama,
				'jenis_kelamin' => $userjenisk,
				'alamat' => $useralamat,
				'hak_akses' => $hak_akses,
				'hp' => $usernohp
					);
			$this->db->where('id_user', $session_iduser);
			$this->db->update('users', $data);
			return "success";
		}

	}

	function delete_user($userid){
		$this->db->where('id_user', $userid);
		$this->db->delete('users');
		return "success";
	}

	function edit_pw($pass_baru, $session_iduser){
		$data = array(
				'password' => $pass_baru
				);
		$this->db->where('id_user', $session_iduser);
		$this->db->update('users', $data);
		return "sukses";
	}


	function cekpass($session_iduser){
		$this->db->select('password');
		$this->db->from('users');
		$this->db->where('id_user', $session_iduser);
		$this->db->limit(1);

		$query = $this->db->get();

		if ( $query->num_rows() > 0 ){
			$row = $query->row_array();
			return $row;
		} else {
			return "kosong";
		}
	}

	function changePhoto($profilpic_path, $session_iduser){
		$data = array(
				'profilpic_path' => $profilpic_path
				);
		$this->db->where('id_user', $session_iduser);
		$this->db->update('users', $data);
		return "sukses";
	}
}
