<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Timakreditasi extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('users_model','user');
	}
	  
	public function index() {
		$timAkreditasi = $this->user->get_all_tim_akreditasi();
		$data['dataTim'] = $timAkreditasi;
		$this->load->view('timakreditasipage', $data); 
	}

	public function edittugas(){
		$ajaxPost = $this->input->post('edit_row');
		if($ajaxPost){
			$iduser = $this->input->post('row_no');
			$tugas_val = $this->input->post('tugas_val');
			$user = $this->input->post('nama');
			$updateTugasDB = $this->user->edit_tugas_user_akreditasi($iduser, $tugas_val);
			echo "success";
		}
		
		// $this->load->view('timakreditasi_proses'); 
	}
	  
	
   } 