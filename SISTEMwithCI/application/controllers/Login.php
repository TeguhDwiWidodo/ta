<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Login extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('users_model','login');
	}
		
    public function index() {
        $this->load->view('loginpage'); 
    } 
	
	public function loginproses(){
		$btn = $this->input->post('loginbtn');
		if($btn){
			$username = $this->input->post('username');
			$password = md5($this->input->post('password'));
			$db = $this->login->login($username, $password);
			if($db == "kosong"){
				//$_SESSION['message'] = "Akun anda belum terdaftar";
				$this->session->set_flashdata('message', 'Akun anda belum terdaftarh');
				header("Location: ../?accountnotregistered");
			} else {
				if($password != $db['password']) {
					//$_SESSION['message'] = "Password Salah";
					$this->session->set_flashdata('message', 'Password Salah');
					header("Location: ../?wrongpw");
				} else {
					$this->session->nama = $db['nama'];
					$this->session->hak_akses = $db['hak_akses'];
					$this->session->email = $db['email'];
					$this->session->id_user = $db['id_user'];
					$this->session->profilpic_path = $db['profilpic_path'];
					header('location: ../dashboard');
				}
			}
		} else {
			redirect("/");
		}
        
		// $this->load->view('login_proses'); 
	}
   } 