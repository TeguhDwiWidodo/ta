<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Profile extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('users_model','user');
	}

	public function index() {
		$userid = $this->session->id_user;
		$db = $this->user->display_profile($userid);
		$data['db'] = $db;
        $this->load->view('profile/profilepage', $data);

    }

	public function user($userid) {

        if($userid != ""){
			$dbuser = $this->user->display_profile($userid);
			$data['dbuser'] = $dbuser;
			$this->load->view('profile/profilepage', $data);
		} else {
			redirect("/profile/");
		}

    }


	public function edit(){
		$btn = $this->input->post('simpanbtn');
		if($btn){
			$session_iduser = $this->session->id_user;
			$useremail = $this->input->post('email');
			$usernama = $this->input->post('namalengkap');
			$userjenisk = $this->input->post('jeniskelamin');
			$useralamat = $this->input->post('alamat');
			$usernohp = $this->input->post('nohandphone');
			$edit_user = $this->user->edit_profileinfo($session_iduser, $useremail, $usernama, $userjenisk, $useralamat, $usernohp);
			if($edit_user == "success"){
				$_SESSION['message'] = "Profile details updated";
				header("Location: ../profile/?profiledetailsupdated");
			} else {
				$_SESSION['message'] = "ERROR: Profile details failed to update";
				header("Location: ../profile/?profiledetailsfailed");
			}

		} else {
			redirect("/profile/");
		}

        // $this->load->view('profile/editprofilepage_proses');
	}

	public function editpw(){
        $btn = $this->input->post('ubahbtn');
		if($btn){
			$session_iduser = $this->session->id_user;
			$pass_skrg = md5($this->input->post('password-saatini'));
			$pass_baru = md5($this->input->post('password-konfirmasi'));
			$cekPass = $this->user->cekpass($session_iduser);
			if($cekPass['password'] == $pass_skrg){
				$edit_pw = $this->user->edit_pw($pass_baru, $session_iduser);
				$_SESSION['message_pw'] = "Password anda telah terganti";
				header("Location: ../profile/?pwupdated");
			} else {
				$_SESSION['message_pw'] = "Password salah";
				header("Location: ../profile/?pwiswrong");
			}
		} else {
			redirect("/profile/");
		}

		// $this->load->view('profile/editpw_proses');
	}

	public function editpp(){
        $btn = $this->input->post('simpanPhotobtn');
		if($btn){
			$session_iduser = $this->session->id_user;
			foreach($_FILES['picfile']['tmp_name'] as $key => $tmp_name ){
				$userfoto = $_FILES['picfile']['tmp_name'][$key];
				$userfoto_size = $_FILES['picfile']['size'][$key];
				$userfoto_nama = $_FILES['picfile']['name'][$key];
				$extension = pathinfo($userfoto_nama, PATHINFO_EXTENSION );
				$folder= "uploads/fotoprofil/".$session_iduser."/";

				if (!file_exists($folder)) {
					mkdir($folder, 0777, true);

				}
				if($userfoto_size > 4200000){
					$_SESSION['message'] = 'File size must be less than 4 MB';
					header("Location: ../profile/?sizeimageistoobig");
				} else {
					$imageData = @getimagesize($userfoto);
					if($imageData === FALSE || !($imageData[2] == IMAGETYPE_GIF || $imageData[2] == IMAGETYPE_JPEG || $imageData[2] == IMAGETYPE_PNG)) {
						$_SESSION['message'] = "Foto profil yang diterima hanya format jpg, jpeg, png, gif dan maksimal ukuran file 4MB. Format foto yang anda coba unggah adalah ".$extension." dan ukuran file anda adalah ".$userfoto_size;
						// header("Location: ../profile/?wrongextensionfile");
					} else {
						$final_file=str_replace(' ','-',$userfoto_nama);
						$profilpic_path = $folder.$final_file;
						if(move_uploaded_file($userfoto,$folder.$final_file)){
							$changeFotoDB = $this->user->changePhoto($profilpic_path, $session_iduser);
							$_SESSION['message'] = "Foto profil berhasil dirubah";
							header("Location: ../profile/?photoupdated");
						} else {
							$_SESSION['message'] = "Gagal mengubah foto profil, silahkan coba lagi";
							header("Location: ../profile/?faileduploadphoto");
						}
					}
				}

			}
		} else {
			redirect("/profile/");
		}
	}
		// $this->load->view('profile/editpp_proses');

}
