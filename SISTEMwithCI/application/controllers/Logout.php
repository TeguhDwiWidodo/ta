<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Logout extends CI_Controller {
	public function index() {
		$this->load->helper('url');
		$this->session->unset_userdata();
		$this->session->sess_destroy();
		$_SESSION['message'] = "Anda berhasil keluar";
		header("Location:" . base_url()."?logoutsuccessfuly");
        // $this->load->view('logout'); 
    } 
}