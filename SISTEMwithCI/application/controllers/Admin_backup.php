<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Admin extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('users_model','user');
	}
		  
	public function tambahtim() {
		$users = $this->user->get_all_users();
		$data['all_users'] = $users;
        $this->load->view('admin/tambahtimpage', $data);
		
    }
	
	public function tambahtim_proses() {
		$btn = $this->input->post('simpanbtn');
		if($btn){
			$email = $this->input->post('email');
			$password = md5($this->input->post('password'));
			$nama = $this->input->post('namalengkap');
			$nohp = $this->input->post('nohandphone');
			$hakakses = $this->input->post('hakakses');
			$insertUser = $this->user->addUser($email, $password, $nama, $nohp, $hakakses);
			$_SESSION['message'] = "Anggota ". $nama . " telah terdaftar";
			header("Location: ../admin/tambahtim?userregistered");
		}
        // $this->load->view('admin/tambahtim_proses');
		
    }

}
	
?>