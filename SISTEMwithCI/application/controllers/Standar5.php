<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Standar5 extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->helper(array('form', 'url'));
		$this->load->model('uploadedfile_model','uploadfile');
		$this->load->model('standar5_model','standar5_model');
		$this->load->model('file_model','file_model');
		$this->load->model('penilaian_model','penilaian_model');
	}

	public function simulasipenilaian($submenu = '', $simpan = ''){
		$hakakses = $this->session->hak_akses;
		if($hakakses == 'kaprodi' || $hakakses == 'admin' || $hakakses == 'standar 5' || $hakakses == 'tim penilaian'){
			if($submenu != ''){
				if($submenu == '5.1.1'){
					$data['result_set511'] = $this->standar5_model->get_data($table = 'std5_511kompetensi');
					$this->load->view('standar5/simulasipenilaian/5.1.1.php', $data);
					if($simpan == 'simpan'){
						$btn = $this->input->post('simpanBtn');
						if(isset($btn)){
							$nilai511a = $this->input->post('nilai1');
							$nilai511b = $this->input->post('nilai2');
							$catatan511[] = $this->input->post('catatan[]');
							$userid = $this->session->id_user;
							//$catatan511b = $this->input->post('catatan2');
							$insertnilai = $this->penilaian_model->insertNilai511($nilai511a, $nilai511b, $catatan511, $userid);
							$this->session->set_flashdata('message', 'SUKSES: Penilaian 5.1.1 (5.1.1.a dan 5.1.1.b) berhasil disimpan');
							header("Location: ../?success");
						} else {
							//$this->session->set_flashdata('message', 'ERROR: Penilaian 5.1.1 (5.1.1.a dan 5.1.1.b) gagal disimpan');
							header("Location: ../5.1.1");
						}

					}
				} else if($submenu == '5.1.2'){
					$data['jumlahMatkulWajibPilihan'] = $this->standar5_model->sumMatkulWajibPilihan();
					$data['jumlahBobotIntiInstitusional'] = $this->standar5_model->sumBobotIntiInstitusional();
					$data['result_set5121'] = $this->standar5_model->get_data($table = 'std5_5121jumlahsksps');
					$data['result_set5122'] = $this->standar5_model->get_data($table = 'std5_5122strukturkurikulummk');
					$data['input1_512b'] = $this->standar5_model->getAllCountBobotTugas();
					$data['totalMatkulWajibPilihan'] = $this->standar5_model->getAllCountMK();
					$data['input1_512c'] = $this->standar5_model->getAllCountKelengkapanSAP();
					$this->load->view('standar5/simulasipenilaian/5.1.2.php', $data);
					if($simpan == 'simpan'){
						$btn = $this->input->post('simpanBtn');
						if(isset($btn)){
							$nilai512a = $this->input->post('nilai[0]');
							$nilai512b = $this->input->post('nilai-number[0]');
							$nilai512c = $this->input->post('nilai-number[1]');
							//$nilai511 = $this->input->post('nilai2');
							$catatan512[] = $this->input->post('catatan[]');
							$userid = $this->session->id_user;
							//$catatan511b = $this->input->post('catatan2');
							$insertnilai = $this->penilaian_model->insertNilai512($nilai512a, $nilai512b, $nilai512c, $catatan512, $userid);
							$this->session->set_flashdata('message', 'SUKSES: Penilaian 5.1.2 (5.1.2.a , 5.1.2.b dan 5.1.1.c) berhasil disimpan');
							header("Location: ../?success");
						} else {
							//$this->session->set_flashdata('message', 'ERROR: Penilaian 5.1.2 (5.1.2.a , 5.1.2.b dan 5.1.1.c) gagal disimpan');
							header("Location: ../5.1.2");
						}

					}
				} else if($submenu == '5.1.3'){
					$data['result_set513'] = $this->standar5_model->get_data($table = 'std5_513matkulpilihan');
					$data['jumlahSksMkPilihan'] = $this->standar5_model->sumSksMkPilihan()[0]['bobot_sks'];
					$this->load->view('standar5/simulasipenilaian/5.1.3.php', $data);
					if($simpan == 'simpan'){
						$btn = $this->input->post('simpanBtn');
						if(isset($btn)){
							$nilai513 = $this->input->post('nilai-number');
							//$nilai511 = $this->input->post('nilai2');
							$catatan513 = $this->input->post('catatan');
							$userid = $this->session->id_user;
							//$catatan511b = $this->input->post('catatan2');
							$insertnilai = $this->penilaian_model->insertNilai513($nilai513, $catatan513, $userid);
							$this->session->set_flashdata('message', 'SUKSES: Penilaian 5.1.3 berhasil disimpan');
							header("Location: ../?success");
						} else {
							//$this->session->set_flashdata('message', 'ERROR: Penilaian 5.1.3 gagal disimpan');
							header("Location: ../5.1.3");
						}
					}
				} else if($submenu == '5.1.4'){
					$data['result_set514'] = $this->standar5_model->get_data($table = 'std5_514substansipraktikum');
					$this->load->view('standar5/simulasipenilaian/5.1.4.php', $data);
					if($simpan == 'simpan'){
						$btn = $this->input->post('simpanBtn');
						if(isset($btn)){
							$nilai514 = $this->input->post('nilai');
							//$nilai511 = $this->input->post('nilai2');
							$catatan514 = $this->input->post('catatan');
							$userid = $this->session->id_user;
							//$catatan511b = $this->input->post('catatan2');
							$insertnilai = $this->penilaian_model->insertNilai514($nilai514, $catatan514, $userid);
							$this->session->set_flashdata('message', 'SUKSES: Penilaian 5.1.4 berhasil disimpan');
							header("Location: ../?success");
						} else {
							//$this->session->set_flashdata('message', 'ERROR: Penilaian 5.1.3 gagal disimpan');
							header("Location: ../5.1.4");
						}
					}
				} else if($submenu == '5.2'){
					$data['result_set52'] = $this->standar5_model->get_data($table = 'std5_52peninjauankurikulum');
					$this->load->view('standar5/simulasipenilaian/5.2.php', $data);
					if($simpan == 'simpan'){
						$btn = $this->input->post('simpanBtn');
						if(isset($btn)){
							$nilai52[] = $this->input->post('nilai[]');
							//$nilai511 = $this->input->post('nilai2');
							$catatan52[] = $this->input->post('catatan[]');
							$userid = $this->session->id_user;
							//$catatan511b = $this->input->post('catatan2');
							$insertnilai = $this->penilaian_model->insertNilai52($nilai52, $catatan52, $userid);
							$this->session->set_flashdata('message', 'SUKSES: Penilaian 5.2 berhasil disimpan');
							header("Location: ../?success");
						} else {
							//$this->session->set_flashdata('message', 'ERROR: Penilaian 5.1.3 gagal disimpan');
							header("Location: ../5.2");
						}
					}
				} else if($submenu == '5.3.1'){
					$data['result_set531'] = $this->standar5_model->get_data($table = 'std5_531mekanismepenyusunan');
					$this->load->view('standar5/simulasipenilaian/5.3.1.php', $data);
					if($simpan == 'simpan'){
						$btn = $this->input->post('simpanBtn');
						if(isset($btn)){
							$nilai531[] = $this->input->post('nilai[]');
							//$nilai511 = $this->input->post('nilai2');
							$catatan531[] = $this->input->post('catatan[]');
							$userid = $this->session->id_user;
							//$catatan511b = $this->input->post('catatan2');
							$insertnilai = $this->penilaian_model->insertNilai531($nilai531, $catatan531, $userid);
							$this->session->set_flashdata('message', 'SUKSES: Penilaian 5.3.1 (5.3.1.a dan 5.3.1.b) berhasil disimpan');
							header("Location: ../?success");
						} else {
							//$this->session->set_flashdata('message', 'ERROR: Penilaian 5.1.3 gagal disimpan');
							header("Location: ../5.3.1");
						}
					}
				} else if($submenu == '5.3.2'){
					$data['result_set532'] = $this->standar5_model->get_data($table = 'std5_532contohsoalujian');
					$this->load->view('standar5/simulasipenilaian/5.3.2.php', $data);
					if($simpan == 'simpan'){
						$btn = $this->input->post('simpanBtn');
						if(isset($btn)){
							$nilai532 = $this->input->post('nilai');
							//$nilai511 = $this->input->post('nilai2');
							$catatan532 = $this->input->post('catatan');
							$userid = $this->session->id_user;
							//$catatan511b = $this->input->post('catatan2');
							$insertnilai = $this->penilaian_model->insertNilai532($nilai532, $catatan532, $userid);
							$this->session->set_flashdata('message', 'SUKSES: Penilaian 5.3.2 berhasil disimpan');
							header("Location: ../?success");
						} else {
							//$this->session->set_flashdata('message', 'ERROR: Penilaian 5.1.3 gagal disimpan');
							header("Location: ../5.3.2");
						}
					}
				} else if($submenu == '5.4'){
					$data['result_set541'] = $this->standar5_model->get_data($table = 'std5_541daftardosenpembimbing');
					$data['result_set542'] = $this->standar5_model->get_data($table = 'std5_542prosespembimbingan');
					$data['banyaknyaDosenPA'] = $this->standar5_model->getAllCountDosenPA();
					$data['banyaknyaMhsBimbinganPA'] = $this->standar5_model->sumMhsBimbinganPA()[0]['jumlah_mahasiswa_bimbingan'];
					$data['rata2PertemuanMhsPerSmt'] = $this->standar5_model->getRataRataPertemuanMhsPerSmt()['rata_rata_banyaknya_pertemuan_mhs_smt'];
					$this->load->view('standar5/simulasipenilaian/5.4.php', $data);
					if($simpan == 'simpan'){
						$btn = $this->input->post('simpanBtn');
						if(isset($btn)){
							$nilai54[] = $this->input->post('nilai[]');
							$nilai_number54[] = $this->input->post('nilai-number[]');
							$catatan54[] = $this->input->post('catatan[]');
							$userid = $this->session->id_user;
							$insertnilai = $this->penilaian_model->insertNilai54($nilai54, $nilai_number54, $catatan54, $userid);
							$this->session->set_flashdata('message', 'SUKSES: Penilaian 5.4 berhasil disimpan');
							header("Location: ../?success");
						} else {
							//$this->session->set_flashdata('message', 'ERROR: Penilaian 5.1.3 gagal disimpan');
							header("Location: ../5.4");
						}
					}
				} else if($submenu == '5.5.1'){
					$data['result_set551'] = $this->standar5_model->get_data($table = 'std5_551pelaksanaanpembimbingan');
					$data['jumlahDosbingTA'] = $this->standar5_model->getAllCountDosbingTA();
					$data['jumlahMhsTA'] = $this->standar5_model->sumMhsTA()['jumlah_mahasiswa'];
					$this->load->view('standar5/simulasipenilaian/5.5.1.php', $data);
					if($simpan == 'simpan'){
						$btn = $this->input->post('simpanBtn');
						if(isset($btn)){
							$nilai551[] = $this->input->post('nilai[]');
							$nilai_number551[] = $this->input->post('nilai-number[]');
							$catatan551[] = $this->input->post('catatan[]');
							$userid = $this->session->id_user;
							$insertnilai = $this->penilaian_model->insertNilai551($nilai551, $nilai_number551, $catatan551, $userid);
							$this->session->set_flashdata('message', 'SUKSES: Penilaian 5.5.1 (5.5.1.a, 5.5.1.b, 5.5.1.c dan 5.5.1.d) berhasil disimpan');
							header("Location: ../?success");
						} else {
							//$this->session->set_flashdata('message', 'ERROR: Penilaian 5.1.3 gagal disimpan');
							header("Location: ../5.5.1");
						}
					}
				} else if($submenu == '5.5.2'){
					$data['result_set552'] = $this->standar5_model->get_data($table = 'std5_552ratapenyelesaianta');
					$this->load->view('standar5/simulasipenilaian/5.5.2.php', $data);
					if($simpan == 'simpan'){
						$btn = $this->input->post('simpanBtn');
						if(isset($btn)){
							$nilai552 = $this->input->post('nilai-akhir');
							$catatan552 = $this->input->post('catatan');
							$userid = $this->session->id_user;
							$insertnilai = $this->penilaian_model->insertNilai552($nilai552, $catatan552, $userid);
							$this->session->set_flashdata('message', 'SUKSES: Penilaian 5.5.2 berhasil disimpan');
							header("Location: ../?success");
						} else {
							//$this->session->set_flashdata('message', 'ERROR: Penilaian 5.1.3 gagal disimpan');
							header("Location: ../5.5.2");
						}
					}
				} else if($submenu == '5.6'){
					$data['result_set56'] = $this->standar5_model->get_data($table = 'std5_56upayaperbaikanpembelajaran');
					$this->load->view('standar5/simulasipenilaian/5.6.php', $data);
					if($simpan == 'simpan'){
						$btn = $this->input->post('simpanBtn');
						if(isset($btn)){
							$nilai56 = $this->input->post('nilai');
							$catatan56 = $this->input->post('catatan');
							$userid = $this->session->id_user;
							$insertnilai = $this->penilaian_model->insertNilai56($nilai56, $catatan56, $userid);
							$this->session->set_flashdata('message', 'SUKSES: Penilaian 5.6 berhasil disimpan');
							header("Location: ../?success");
						} else {
							//$this->session->set_flashdata('message', 'ERROR: Penilaian 5.1.3 gagal disimpan');
							header("Location: ../5.6");
						}
					}
				} else if($submenu == '5.7'){
					$data['result_set571'] = $this->standar5_model->get_data($table = 'std5_571kebijakan');
					$data['result_set572'] = $this->standar5_model->get_data($table = 'std5_572ketersediaanprasarana');
					$data['result_set573'] = $this->standar5_model->get_data($table = 'std5_573program');
					$data['result_set574'] = $this->standar5_model->get_data($table = 'std5_574interaksi');
					$data['result_set575'] = $this->standar5_model->get_data($table = 'std5_575pengembangan');
					$this->load->view('standar5/simulasipenilaian/5.7.php', $data);
					if($simpan == 'simpan'){
						$btn = $this->input->post('simpanBtn');
						if(isset($btn)){
							$nilai57[] = $this->input->post('nilai[]');
							$catatan57[] = $this->input->post('catatan[]');
							$userid = $this->session->id_user;
							$insertnilai = $this->penilaian_model->insertNilai57($nilai57, $catatan57, $userid);
							$this->session->set_flashdata('message', 'SUKSES: Penilaian 5.7 (5.7.1, 5.7.2, 5.7.3, 5.7.4 dan 5.7.5) berhasil disimpan');
							header("Location: ../?success");
						} else {
							//$this->session->set_flashdata('message', 'ERROR: Penilaian 5.1.3 gagal disimpan');
							header("Location: ../5.7");
						}
					}
				}
			} else {
				$data['hasil_penilaian'] = $this->penilaian_model->getHasilPenilaian();
				$data['jumlahhasil_penilaian'] = $this->penilaian_model->sumHasilPenilaian()[0]['bobotxnilai'];
				$this->load->view('standar5/simulasi_penilaian', $data);
			}
		} else {
			$this->session->set_flashdata('message', 'Silahkan melakukan simulasi input penilaian sesuai dengan hak akses standar anda');
			$data['hasil_penilaian'] = $this->penilaian_model->getHasilPenilaian();
			$data['jumlahhasil_penilaian'] = $this->penilaian_model->sumHasilPenilaian()[0]['bobotxnilai'];
			$this->load->view('standar5/simulasi_penilaian', $data);
		}

	}

	public function riwayat_uploadfile(){
		$table_db_value = $this->input->post('table_riwayat');
		$info = $this->input->post('info');
		if($table_db_value){
			$riwayatFile = $this->file_model->get_riwayat_file($table_db_value, $info);
			$data['riwayatFile'] = $riwayatFile;
			$data['table_db_value'] = $table_db_value;
			$data['info'] = $info;

			$this->load->view('standar5/riwayat_uploadfile', $data);
		} else {
			redirect("/standar5/kurikulum/");
		}

	}

	public function kurikulum($submenu = '') {
		if($submenu != ''){
			if($submenu == '5.1.1kompetensidan5.1.2sk'){
				$data['jumlahMatkulWajibPilihan'] = $this->standar5_model->sumMatkulWajibPilihan();
				$data['jumlahBobotIntiInstitusional'] = $this->standar5_model->sumBobotIntiInstitusional();
				$data['filependukung_5111'] = $this->getFilePendukung($poinmateri1 = 'std5_5111');
				$data['filependukung_5112'] = $this->getFilePendukung($poinmateri2 = 'std5_5112');
				$data['filependukung_5113'] = $this->getFilePendukung($poinmateri3 = 'std5_5113');
				$data['filependukung_5121'] = $this->getFilePendukung($poinmateri4 = 'std5_5121');
				$data['filependukung_5122'] = $this->getFilePendukung($poinmateri5 = 'std5_5122');
				$data['result_set511'] = $this->standar5_model->get_data($table = 'std5_511kompetensi');
				$data['result_set5121'] = $this->standar5_model->get_data($table = 'std5_5121jumlahsksps');
				$data['result_set5122'] = $this->standar5_model->get_data($table = 'std5_5122strukturkurikulummk');

				$this->load->view('standar5/data&dokumenpendukung/5.1.1dan5.1.2.php', $data);
			} else if($submenu == '5.1.3dan5.1.4matakuliah'){
				$data['jumlahSksMkPilihan'] = $this->standar5_model->sumSksMkPilihan()[0]['bobot_sks'];
				$data['filependukung_513'] = $this->getFilePendukung($poinmateri1 = 'std5_513');
				$data['filependukung_514'] = $this->getFilePendukung($poinmateri2 = 'std5_514');
				$data['result_set513'] = $this->standar5_model->get_data($table = 'std5_513matkulpilihan');
				$data['result_set514'] = $this->standar5_model->get_data($table = 'std5_514substansipraktikum');

				$this->load->view('standar5/data&dokumenpendukung/5.1.3dan5.1.4.php', $data);
			} else if($submenu == '5.2peninjauan'){
				$data['filependukung_52'] = $this->getFilePendukung($poinmateri1 = 'std5_52');
				$data['result_set52'] = $this->standar5_model->get_data($table = 'std5_52peninjauankurikulum');

				$this->load->view('standar5/data&dokumenpendukung/5.2peninjauan.php', $data);
			}
		} else {
			$this->load->view('standar5/kurikulum');
		}
	}

	public function pembelajaran($submenu = '') {
		if($submenu != ''){
			if($submenu == '5.3ppp'){
				$data['filependukung_531'] = $this->getFilePendukung($poinmateri1 = 'std5_531');
				$data['filependukung_532'] = $this->getFilePendukung($poinmateri2 = 'std5_532');
				$data['result_set531'] = $this->standar5_model->get_data($table = 'std5_531mekanismepenyusunan');
				$data['result_set532'] = $this->standar5_model->get_data($table = 'std5_532contohsoalujian');
				$this->load->view('standar5/data&dokumenpendukung/5.3ppp.php', $data);
			} else if($submenu == '5.4spa'){
				$data['banyaknyaMhsBimbinganPA'] = $this->standar5_model->sumMhsBimbinganPA()[0]['jumlah_mahasiswa_bimbingan'];
				$data['rata2PertemuanMhsPerSmt'] = $this->standar5_model->getRataRataPertemuanMhsPerSmt()['rata_rata_banyaknya_pertemuan_mhs_smt'];
				$data['filependukung_541'] = $this->getFilePendukung($poinmateri1 = 'std5_541');
				$data['filependukung_542'] = $this->getFilePendukung($poinmateri2 = 'std5_542');
				$data['result_set541'] = $this->standar5_model->get_data($table = 'std5_541daftardosenpembimbing');
				$data['result_set542'] = $this->standar5_model->get_data($table = 'std5_542prosespembimbingan');
				$this->load->view('standar5/data&dokumenpendukung/5.4spa.php', $data);
			} else if($submenu == '5.5pta'){
				$poinmateri1 = 'std5_551';
				$poinmateri2 = 'std5_552';
				$data['filependukung_551'] = $this->getFilePendukung($poinmateri1);
				$data['filependukung_552'] = $this->getFilePendukung($poinmateri2);
				$data['result_set551'] = $this->standar5_model->get_data($table = 'std5_551pelaksanaanpembimbingan');
				$data['result_set552'] = $this->standar5_model->get_data($table = 'std5_552ratapenyelesaianta');
				$this->load->view('standar5/data&dokumenpendukung/5.5pta.php', $data);
			} else if($submenu == '5.6upp'){
				$poinmateri1 = 'std5_56';
				$data['filependukung_56'] = $this->getFilePendukung($poinmateri1);
				$data['result_set56'] = $this->standar5_model->get_data($table = 'std5_56upayaperbaikanpembelajaran');
				$this->load->view('standar5/data&dokumenpendukung/5.6upp.php', $data);
			}
		} else {
			$this->load->view('standar5/pembelajaran');
		}
	}

	public function sa($submenu = '') {
		if($submenu != ''){
			if($submenu == '5.7upayapeningkatan'){
				$poinmateri1 = 'std5_571';
				$poinmateri2 = 'std5_572';
				$poinmateri3 = 'std5_573';
				$poinmateri4 = 'std5_574';
				$poinmateri5 = 'std5_575';
				$data['filependukung_571'] = $this->getFilePendukung($poinmateri1);
				$data['filependukung_572'] = $this->getFilePendukung($poinmateri2);
				$data['filependukung_573'] = $this->getFilePendukung($poinmateri3);
				$data['filependukung_574'] = $this->getFilePendukung($poinmateri4);
				$data['filependukung_575'] = $this->getFilePendukung($poinmateri5);
				$data['result_set571'] = $this->standar5_model->get_data($table = 'std5_571kebijakan');
				$data['result_set572'] = $this->standar5_model->get_data($table = 'std5_572ketersediaanprasarana');
				$data['result_set573'] = $this->standar5_model->get_data($table = 'std5_573program');
				$data['result_set574'] = $this->standar5_model->get_data($table = 'std5_574interaksi');
				$data['result_set575'] = $this->standar5_model->get_data($table = 'std5_575pengembangan');
				$this->load->view('standar5/data&dokumenpendukung/5.7upayapeningkatan.php', $data);
			}
		} else {
			$this->load->view('standar5/suasanaakademik');
		}
	}

	public function edit_ckeditor() {
		$btn = $this->input->post('table');
		if($btn){
			$table = $this->input->post('table');
			$columns_data[] = $this->input->post('columns_data');
			$no = $this->input->post('row_no');
			$textarea_val = $this->input->post('textarea_val');
			$update = $this->standar5_model->edit_ckeditor($columns_data, $table, $no, $textarea_val);
			echo $update;
		}

		// $this->load->view('standar5/data&dokumenpendukung/5.1kurikulum_editproses.php');
	}

	private function getFilePendukung($poinmateri){
		$result = $this->file_model->selectFilePendukung($poinmateri);
		return $result;
	}

	public function deleteFilePendukung(){
		$nav = $this->input->get('nav');
		$namafile = $this->input->get('namafile');
		$id = $this->input->get('id');
		$remove = $this->file_model->deleteFilePendukung($id);
		$this->session->set_flashdata('message', 'Dokumen '.$namafile.' '.$remove.' dihapus.');
		header("Location: ../$nav?sukses");
	}

	public function doUpload_FilePendukung(){
		$btn = $this->input->post('btn-upload');
		$poinmateri = $this->input->post('poinmateri');
		$nav = $this->input->post('nav');
		$iduser = $this->session->id_user;
		$path = 'uploads/dokumenpendukungstd5/'.$poinmateri.'/';

		if(isset($btn)){
			if(isset($_FILES['file_pendukung'])){
				if (!file_exists($path)) {
				mkdir($path, 0777, true);
				}
				foreach($_FILES['file_pendukung']['tmp_name'] as $key => $tmp_name ){
					$saveDate = time();
					$file_name = $_FILES['file_pendukung']['name'][$key];
					$file_size = $_FILES['file_pendukung']['size'][$key];
					$file_tmp = $_FILES['file_pendukung']['tmp_name'][$key];
					$file_type = $_FILES['file_pendukung']['type'][$key];
					$final_fileName = str_replace(' ','','['.$saveDate.']'.$file_name);
					$pathfile = $path.$final_fileName;

					if(move_uploaded_file($tmp_name,$path.$final_fileName)) {
						$sql = $this->file_model->insertFilePendukung($final_fileName, $poinmateri, $saveDate, $iduser, $pathfile);
						$this->session->set_flashdata('message','Dokumen '.$file_name.' berhasil diunggah.');
						header("Location: ../$nav");
					} else {
						$this->session->set_flashdata('message','Dokumen '.$file_name.' gagal diunggah.');
						header("Location: ../$nav");
					}
				}
			} else {
				$this->session->set_flashdata('message', 'Silahkan pilih/browse dokumen terlebih dahulu.');
				header("Location: ../$nav?fail");
			}


		} else {
			redirect("/standar5/kurikulum/");
		}
	}

	public function getPdf(){
		$poin = $this->input->get('poin');
		$this->load->library('M_pdf');
		$this->m_pdf->mpdf->SetHeader('Akreditasi SI | | {DATE d/m/Y}');
		$this->m_pdf->mpdf->SetFooter('{PAGENO}');
		$this->m_pdf->mpdf->WriteHTML('<style> body,table,thead,tbody,th,tr,td{text-align: justify}</style>');
		//$this->m_pdf->mpdf->useFixedNormalLineHeight = false;
		//$this->m_pdf->mpdf->useFixedTextBaseline = false;
		$this->m_pdf->mpdf->adjustFontDescLineheight = 1.5;
		if($poin == '5111'){
			$html = $this->standar5_model->get_data($table = 'std5_511kompetensi')[0]['kompetensi_utama'];
			$informasi = '5.1.1.1 Kompetensi Utama Lulusan Program Studi Sistem Informasi';
			$this->m_pdf->mpdf->SetTitle($informasi);
			$this->m_pdf->mpdf->WriteHTML('<h3>'.$informasi.'</h3>');
			$this->m_pdf->mpdf->WriteHTML($html);
		} else if($poin == '5112'){
			$html = $this->standar5_model->get_data($table = 'std5_511kompetensi')[0]['kompetensi_pendukung'];
			$informasi = '5.1.1.2 Kompetensi Pendukung Lulusan Program Studi Sistem Informasi';
			$this->m_pdf->mpdf->SetTitle($informasi);
			$this->m_pdf->mpdf->WriteHTML('<h3>'.$informasi.'</h3>');
			$this->m_pdf->mpdf->WriteHTML($html);
		} else if($poin == '5113'){
			$html = $this->standar5_model->get_data($table = 'std5_511kompetensi')[0]['kompetensi_lainnya'];
			$informasi = '5.1.1.3 Kompetensi Lainnya Lulusan Program Studi Sistem Informasi';
			$this->m_pdf->mpdf->SetTitle($informasi);
			$this->m_pdf->mpdf->WriteHTML('<h3>'.$informasi.'</h3>');
			$this->m_pdf->mpdf->WriteHTML($html);
		} else if($poin == '531'){
			$html = $this->standar5_model->get_data($table = 'std5_531mekanismepenyusunan')[0]['mekanisme_penyusunan'];
			$informasi = '5.3.1 Mekanisme Penyusunan Materi Kuliah dan Monitoring Perkuliahan';
			$this->m_pdf->mpdf->SetTitle($informasi);
			$this->m_pdf->mpdf->WriteHTML('<h3>'.$informasi.'</h3>');
			$this->m_pdf->mpdf->WriteHTML($html);
		} else if($poin == '532'){
			$html = $this->standar5_model->get_data($table = 'std5_532contohsoalujian')[0]['contoh_soal_ujian'];
			$informasi = '5.3.2 Contoh Soal Ujian dalam 1 Tahun Terakhir untuk 5 Mata Kuliah Keahlian Berikut Silabus dan SAP dapat Dilihat pada Lampiran Borang';
			$this->m_pdf->mpdf->SetTitle($informasi);
			$this->m_pdf->mpdf->WriteHTML('<h3>'.$informasi.'</h3>');
			$this->m_pdf->mpdf->WriteHTML($html);
		} else if($poin == '552'){
			$html = $this->standar5_model->get_data($table = 'std5_552ratapenyelesaianta')[0]['waktu_penyelesaian_skripsi'];
			$informasi = '5.5.2 Rata-Rata Lama Penyelesaian Tugas Akhir/Skripsi Pada Tiga Tahun Terakhir';
			$this->m_pdf->mpdf->SetTitle($informasi);
			$this->m_pdf->mpdf->WriteHTML('<h3>'.$informasi.'</h3>');
			$this->m_pdf->mpdf->WriteHTML($html);
		} else if($poin == '571'){
			$html = $this->standar5_model->get_data($table = 'std5_571kebijakan')[0]['kebijakan_suasana_akademik'];
			$informasi = '5.7.1 Kebijakan Tentang Suasana Akademik (Otonomi Keilmuan, Kebebasan Akademik, Kebebasan Mimbar Akademik)';
			$this->m_pdf->mpdf->SetTitle($informasi);
			$this->m_pdf->mpdf->WriteHTML('<h3>'.$informasi.'</h3>');
			$this->m_pdf->mpdf->WriteHTML($html);
		} else if($poin == '572'){
			$html = $this->standar5_model->get_data($table = 'std5_572ketersediaanprasarana')[0]['ketersediaan_dan_jenis_prasarana'];
			$informasi = '5.7.2 Ketersediaan dan Jenis Prasarana, Sarana dan Dana yang Memungkinkan Terciptanya Interaksi Akademik Antara Sivitas Akademika';
			$this->m_pdf->mpdf->SetTitle($informasi);
			$this->m_pdf->mpdf->WriteHTML('<h3>'.$informasi.'</h3>');
			$this->m_pdf->mpdf->WriteHTML($html);
		} else if($poin == '573'){
			$html = $this->standar5_model->get_data($table = 'std5_573program')[0]['program_dan_kegiatan'];
			$informasi = '5.7.3 Program dan Kegiatan di Dalam dan di Luar Proses Pembelajaran, yang Dilaksanakan Baik di Dalam Maupun di Luar Kelas,  Untuk Menciptakan Suasana Akademik yang Kondusif';
			$this->m_pdf->mpdf->SetTitle($informasi);
			$this->m_pdf->mpdf->WriteHTML('<h3>'.$informasi.'</h3>');
			$this->m_pdf->mpdf->WriteHTML($html);
		} else if($poin == '574'){
			$html = $this->standar5_model->get_data($table = 'std5_574interaksi')[0]['interaksi_akademik'];
			$informasi = '5.7.4 Interaksi Akademik Antara Dosen-Mahasiswa, Antar Mahasiswa, Serta Antar Dosen';
			$this->m_pdf->mpdf->SetTitle($informasi);
			$this->m_pdf->mpdf->WriteHTML('<h3>'.$informasi.'</h3>');
			$this->m_pdf->mpdf->WriteHTML($html);
		} else if($poin == '575'){
			$html = $this->standar5_model->get_data($table = 'std5_575pengembangan')[0]['pengembangan_perilaku_kecendekiawanan'];
			$informasi = '5.7.5 Pengembangan Perilaku Kecendekiawanan';
			$this->m_pdf->mpdf->SetTitle($informasi);
			$this->m_pdf->mpdf->WriteHTML('<h3>'.$informasi.'</h3>');
			$this->m_pdf->mpdf->WriteHTML($html);
		} else if($poin == 'all'){
			$data511 = $this->standar5_model->get_data($table = 'std5_511kompetensi');
			$data531 = $this->standar5_model->get_data($table = 'std5_531mekanismepenyusunan');
			$data532 = $this->standar5_model->get_data($table = 'std5_532contohsoalujian');
			$data552 = $this->standar5_model->get_data($table = 'std5_552ratapenyelesaianta');
			$data571 = $this->standar5_model->get_data($table = 'std5_571kebijakan');
			$data572 = $this->standar5_model->get_data($table = 'std5_572ketersediaanprasarana');
			$data573 = $this->standar5_model->get_data($table = 'std5_573program');
			$data574 = $this->standar5_model->get_data($table = 'std5_574interaksi');
			$data575 = $this->standar5_model->get_data($table = 'std5_575pengembangan');
			$this->m_pdf->mpdf->SetTitle('Data Informasi Semua Poin Standar 5');
			$this->m_pdf->mpdf->WriteHTML('<h3>5.1.1.1 Kompetensi Utama Lulusan Program Studi Sistem Informasi</h3>');
			$this->m_pdf->mpdf->WriteHTML($data511[0]['kompetensi_utama']);
			$this->m_pdf->mpdf->AddPage();
			$this->m_pdf->mpdf->WriteHTML('<h3>5.1.1.2 Kompetensi Pendukung Lulusan Program Studi Sistem Informasi</h3>');
			$this->m_pdf->mpdf->WriteHTML($data511[0]['kompetensi_pendukung']);
			$this->m_pdf->mpdf->AddPage();
			$this->m_pdf->mpdf->WriteHTML('<h3>5.1.1.3 Kompetensi Lainnya Lulusan Program Studi Sistem Informasi</h3>');
			$this->m_pdf->mpdf->WriteHTML($data511[0]['kompetensi_lainnya']);
			$this->m_pdf->mpdf->AddPage();
			$this->m_pdf->mpdf->WriteHTML('<h3>5.3.1 Mekanisme Penyusunan Materi Kuliah dan Monitoring Perkuliahan</h3>');
			$this->m_pdf->mpdf->WriteHTML($data531[0]['mekanisme_penyusunan']);
			$this->m_pdf->mpdf->AddPage();
			$this->m_pdf->mpdf->WriteHTML('<h3>5.3.2 Contoh Soal Ujian dalam 1 Tahun Terakhir untuk 5 Mata Kuliah Keahlian Berikut Silabus dan SAP dapat Dilihat pada Lampiran Borang</h3>');
			$this->m_pdf->mpdf->WriteHTML($data532[0]['contoh_soal_ujian']);
			$this->m_pdf->mpdf->AddPage();
			$this->m_pdf->mpdf->WriteHTML('<h3>5.5.2 Rata-Rata Lama Penyelesaian Tugas Akhir/Skripsi Pada Tiga Tahun Terakhir</h3>');
			$this->m_pdf->mpdf->WriteHTML($data552[0]['waktu_penyelesaian_skripsi']);
			$this->m_pdf->mpdf->AddPage();
			$this->m_pdf->mpdf->WriteHTML('<h3>5.7.1 Kebijakan Tentang Suasana Akademik (Otonomi Keilmuan, Kebebasan Akademik, Kebebasan Mimbar Akademik)</h3>');
			$this->m_pdf->mpdf->WriteHTML($data571[0]['kebijakan_suasana_akademik']);
			$this->m_pdf->mpdf->AddPage();
			$this->m_pdf->mpdf->WriteHTML('<h3>5.7.2 Ketersediaan dan Jenis Prasarana, Sarana dan Dana yang Memungkinkan Terciptanya Interaksi Akademik Antara Sivitas Akademika</h3>');
			$this->m_pdf->mpdf->WriteHTML($data572[0]['ketersediaan_dan_jenis_prasarana']);
			$this->m_pdf->mpdf->AddPage();
			$this->m_pdf->mpdf->WriteHTML('<h3>5.7.3 Program dan Kegiatan di Dalam dan di Luar Proses Pembelajaran, yang Dilaksanakan Baik di Dalam Maupun di Luar Kelas,  Untuk Menciptakan Suasana Akademik yang Kondusif</h3>');
			$this->m_pdf->mpdf->WriteHTML($data573[0]['program_dan_kegiatan']);
			$this->m_pdf->mpdf->AddPage();
			$this->m_pdf->mpdf->WriteHTML('<h3>5.7.4 Interaksi Akademik Antara Dosen-Mahasiswa, Antar Mahasiswa, Serta Antar Dosen</h3>');
			$this->m_pdf->mpdf->WriteHTML($data574[0]['interaksi_akademik']);
			$this->m_pdf->mpdf->AddPage();
			$this->m_pdf->mpdf->WriteHTML('<h3>5.7.5 Pengembangan Perilaku Kecendekiawanan</h3>');
			$this->m_pdf->mpdf->WriteHTML($data575[0]['pengembangan_perilaku_kecendekiawanan']);
		} else {
			redirect("/standar5/kurikulum/");
		}

		$this->m_pdf->mpdf->Output('[AkreditasiSI] '.$poin.'.pdf', I);
	}

	public function getTemplateExcel(){
		$poindata = $this->input->get('poindata');
		$getFile = $this->file_model->selectTemplateExcel($poindata)[0]['filepath'];
		//print_r(base_url($getFile));
		redirect(base_url($getFile));
	}

	public function uploaddokumen_excel() {
		include ( APPPATH . "libraries\PHPExcel\IOFactory.php");
		//$this->load->library('PHPExcel');
		$btn = $this->input->post('btn-upload');
		$nama = $this->session->nama;
		$iduser = $this->session->id_user;

		$nav = $this->input->post('nav');

		if(isset($btn)){
			if(isset($_FILES['files'])){
				foreach($_FILES['files']['tmp_name'] as $key => $tmp_name) {
					$table = $this->input->post('table');
					$saveDate = time();
					$rawFileName = $_FILES['files']['name'][$key];
					$inputfilename = str_replace(' ','',$_FILES['files']['name'][$key]);
					$rawBaseName = pathinfo($inputfilename, PATHINFO_FILENAME );
					$extension = pathinfo($inputfilename, PATHINFO_EXTENSION );
					$folder = "uploads/dokumendatastd5/".$table."/";

					$counter = 0;
					if (!file_exists($folder)) {
						mkdir($folder, 0777, true);
					}
					$inputfilename = '[Versi'.$counter.']'.$rawBaseName . '.' . $extension;
					while(file_exists($folder.$inputfilename)) {
						$inputfilename = '[Versi'.$counter.']'.$rawBaseName . '.' . $extension;
						$counter++;
					};
					$excelhtml_path = str_replace(' ','',$folder.$inputfilename.'.html');
					$excel_path = str_replace(' ','',$folder.$inputfilename);
					if(move_uploaded_file($tmp_name,$folder.$inputfilename)) {
						try {
							$inputfiletype = PHPExcel_IOFactory::identify($folder.$inputfilename);
							$objReader = PHPExcel_IOFactory::createReader($inputfiletype);
							$objPHPExcel = $objReader->load($folder.$inputfilename);
							$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'HTML');
							$objWriter->save($folder.$inputfilename.'.html');
						} catch(Exception $e) {
							die('Error loading file "'.pathinfo($folder.$inputfilename,PATHINFO_BASENAME).'": '.$e->getMessage());
						}
						$sheet = $objPHPExcel->getSheet(0);
						$highestRow = $sheet->getHighestRow();
						$highestColumn = $sheet->getHighestColumn();
						$headings = $sheet->rangeToArray('A1:' . $highestColumn . 1, NULL, TRUE, FALSE);
						$lowercaseheadings = array_map('strtolower', $headings[0]);
						$updatedheadings = str_replace(' ','_', $lowercaseheadings);
						$updatedheadings = str_replace('/','_', $updatedheadings);
						$updatedheadings = str_replace('-','_', $updatedheadings);
						for ($row = 2; $row <= $highestRow; $row++){
							//  Read a row of data into an array
							$rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, "", TRUE, FALSE);
							//  Insert row data array into your database of choice here
							$sql = $this->file_model->upload_excel($row, $table, $highestRow, $highestColumn, $updatedheadings, $rowData);
						}
						$sql2 = $this->file_model->insert_history_upload_excel($inputfilename, $table, $saveDate, $iduser, $excel_path, $excelhtml_path);

						/*
						$sql2 = "INSERT INTO std5_history_uploadfile (nama_file, tabel_db, tanggal, oleh, id_user, excel_path, excelhtml_path)
						VALUES ('$inputfilename','$table','$saveDate', '$nama', '$iduser', '$excel_path', '$excelhtml_path');";
						$this->db->query($sql2);
						*/
						$_SESSION['message'] = "Dokumen '".$rawFileName."' Berhasil diunggah.";
						header("Location: ../$nav?success");
					}
				}
			} else {
				$_SESSION['message'] = 'Tidak ada dokumen yang diupload.';
				header("Location: ../$nav?fail");
			}
		} else {
			redirect("/standar5/kurikulum/");
		}

		// $this->load->view('standar5/data&dokumenpendukung/5.1kurikulum_uploadproses.php');
	}

	public function edittabel() {
		$btn = $this->input->post('edit_row');
		if($btn){
			$table = $this->input->post('edit_row');
			$no = $this->input->post('row_no');
			$numbColumns = $this->input->post('columns');
			// $j = 1;
			$datatbl_val[] = $this->input->post('datatbl_val');
			if($table){
				// while($j <= $numbColumns){
				$columns = $this->standar5_model->select_table_field_data($table);
				while($row = $columns){
					if($numbColumns == 2){
						$col[] = $row[0]->name;
						$col[] = $row[1]->name;

					} else if ($numbColumns == 3){
						$col[] = $row[0]->name;
						$col[] = $row[1]->name;
						$col[] = $row[2]->name;

					} else if($numbColumns == 4){
						$col[] = $row[0]->name;
						$col[] = $row[1]->name;
						$col[] = $row[2]->name;
						$col[] = $row[3]->name;

					} else if($numbColumns == 5){
						$col[] = $row[0]->name;
						$col[] = $row[1]->name;
						$col[] = $row[2]->name;
						$col[] = $row[3]->name;
						$col[] = $row[4]->name;

					} else if($numbColumns == 6){
						$col[] = $row[0]->name;
						$col[] = $row[1]->name;
						$col[] = $row[2]->name;
						$col[] = $row[3]->name;
						$col[] = $row[4]->name;
						$col[] = $row[5]->name;

					} else if ($numbColumns == 8){
						$col[] = $row[0]->name;
						$col[] = $row[1]->name;
						$col[] = $row[2]->name;
						$col[] = $row[3]->name;
						$col[] = $row[4]->name;
						$col[] = $row[5]->name;
						$col[] = $row[6]->name;
						$col[] = $row[7]->name;

					} else if ($numbColumns == 12){
						$col[] = $row[0]->name;
						$col[] = $row[1]->name;
						$col[] = $row[2]->name;
						$col[] = $row[3]->name;
						$col[] = $row[4]->name;
						$col[] = $row[5]->name;
						$col[] = $row[6]->name;
						$col[] = $row[7]->name;
						$col[] = $row[8]->name;
						$col[] = $row[9]->name;
						$col[] = $row[10]->name;
						$col[] = $row[11]->name;

					} else {
						echo "gagal";
						exit();
					}
					$query = $this->standar5_model->editTbl_standar5($table, $numbColumns, $no, $col, $datatbl_val);
					echo "success";
					exit();

				}


				// }
			}
		} else {
			redirect("/standar5/kurikulum/");
		}

		// $this->load->view('standar5/data&dokumenpendukung/5.1kurikulum_edittblproses.php');
	}





	/*
	public function uploaddokumenpendukung() {
		$this->load->view('standar5/dokumenpendukungstd5_uploadproses');
	}

	public function editdokumenpendukung() {
		$this->load->view('standar5/dokumenpendukungstd5_editproses');
	}
	*/







	public function uploadfile() {
		$this->load->view('standar5/uploadfile');
	}

	public function uploading() {
		$this->load->view('standar5/upload-file-proses');
	}















	  public function ajax_list()
    {
        $list = $this->uploadfile->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $filedetail) {
            $no++;
            $row = array();
            $row[] = $filedetail->file_name;
            $row[] = $filedetail->kebutuhan;
            $row[] = $filedetail->deskripsi;
            $row[] = $filedetail->date;
            $row[] = $filedetail->size;

            $data[] = $row;
        }

        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->uploadfile->count_all(),
                        "recordsFiltered" => $this->uploadfile->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
   }
}
