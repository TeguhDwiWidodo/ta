<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Reportstd extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('uploadedfile_modelstd7','standar7');

		//$this->load->library('PHPExcel');
	  //$this->load->library(array('PHPExcel','PHPExcel/IOFactory'));
		$this->load->helper(array('form', 'url','download'));
		$this->load->library('session');
	}


	function rptpenelitian(){
		$this->load->helper('url');
			$this->load->model('Users_model');

			$numberfile = time();

			$data['user']=$this->Users_model->get_userinfo($this->session->userdata('email'));
			$this->load->model('Standar7_model');


						$data['nilai']=$this->Standar7_model->viewpenilaian();
						$data['nilai_count']=$this->Standar7_model->sum_nilai();

				 $data['data_dosen_internasional']=$this->Standar7_model->get_dsninter();
				 $data['data_dosen_nasional']=$this->Standar7_model->get_dsnnsl();
				 $data['data_dosen_lokal']=$this->Standar7_model->get_dsnlkl();
				 $data['jml_dosen']=$this->Standar7_model->jmldosen();
				 $data['biaya_luar_negeri']=$this->Standar7_model->get_biaya_luar_negeri();
				 $data['biaya_sendiri']=$this->Standar7_model->get_biaya_sendiri();
				 $data['pt_bersangkutan']=$this->Standar7_model->get_pt_bersangkutan();
				 $data['depdiknas']=$this->Standar7_model->get_depdiknas();
				 $data['luar_depdiknas']=$this->Standar7_model->get_luar_depdiknas();



				 $data['biaya_luar_negeri_ts']=$this->Standar7_model->get_biaya_luar_negeri_ts();
				 $data['biaya_luar_negeri_ts1']=$this->Standar7_model->get_biaya_luar_negeri_ts1();
				 $data['biaya_luar_negeri_ts2']=$this->Standar7_model->get_biaya_luar_negeri_ts2();
				 $data['biaya_sendiri_ts']=$this->Standar7_model->get_biaya_sendiri_ts();
				 $data['biaya_sendiri_ts1']=$this->Standar7_model->get_biaya_sendiri_ts1();
				 $data['biaya_sendiri_ts2']=$this->Standar7_model->get_biaya_sendiri_ts2();
				 $data['pt_bersangkutan_ts']=$this->Standar7_model->get_pt_bersangkutan_ts();

				 $data['pt_bersangkutan_ts1']=$this->Standar7_model->get_pt_bersangkutan_ts1();
				 $data['pt_bersangkutan_ts2']=$this->Standar7_model->get_pt_bersangkutan_ts2();
				 $data['depdiknas_ts']=$this->Standar7_model->get_depdiknas_ts();
				 $data['depdiknas_ts1']=$this->Standar7_model->get_depdiknas_ts1();
				 $data['depdiknas_ts2']=$this->Standar7_model->get_depdiknas_ts2();
				 $data['luar_depdiknas_ts']=$this->Standar7_model->get_luar_depdiknas_ts();
				 $data['luar_depdiknas_ts1']=$this->Standar7_model->get_luar_depdiknas_ts1();
				 $data['luar_depdiknas_ts2']=$this->Standar7_model->get_luar_depdiknas_ts2();

				 $data['hitung_penilaian_tugas_akhir_dosen']=$this->Standar7_model->hitung_penilaian_tugas_akhir_dosen();


	 			 $data['jumlah_mahasiswa_pelta']=$this->Standar7_model->jumlah_mahasiswa_pelta();
			 		$data['jumlah_mahasiswa_peldosta']=$this->Standar7_model->jumlah_mahasiswa_peldosta();

					$data['publikasi']=$this->Standar7_model->viewpublikasi();
					$data['data_dosen_internasional']=$this->Standar7_model->get_dsninter();
					$data['data_dosen_nasional']=$this->Standar7_model->get_dsnnsl();
					$data['data_dosen_lokal']=$this->Standar7_model->get_dsnlkl();

					 $data['result_haki']=$this->Standar7_model->listhaki();

					 $data['biaya_luar_negeri_abdimas']=$this->Standar7_model->get_biaya_luar_negeri_abdimas();
				 	$data['biaya_sendiri_abdimas']=$this->Standar7_model->get_biaya_sendiri_abdimas();
				 	$data['pt_bersangkutan_abdimas']=$this->Standar7_model->get_pt_bersangkutan_abdimas();
				 	$data['depdiknas_abdimas']=$this->Standar7_model->get_depdiknas_abdimas();
				 	$data['luar_depdiknas_abdimas']=$this->Standar7_model->get_luar_depdiknas_abdimas();
				 	$data['jml_dosen']=$this->Standar7_model->jmldosen();
				 	$data['nm_mahasiswa']=$this->Standar7_model->viewabdimas();
				 	$data['nm_mahasiswa']=$this->Standar7_model->viewabdimas();



				 	$data['biaya_luar_negeri_abdimas_hitung_ts2']=$this->Standar7_model->get_biaya_luar_negeri_abdimas_hitung_ts2();
				 	$data['biaya_luar_negeri_abdimas_hitung_ts1']=$this->Standar7_model->get_biaya_luar_negeri_abdimas_hitung_ts1();
				 	$data['biaya_luar_negeri_abdimas_hitung_ts']=$this->Standar7_model->get_biaya_luar_negeri_abdimas_hitung_ts();
				 	$data['biaya_sendiri_abdimas_ts2']=$this->Standar7_model->get_biaya_sendiri_abdimas_ts2();
				 	$data['biaya_sendiri_abdimas_ts1']=$this->Standar7_model->get_biaya_sendiri_abdimas_ts1();
				 	$data['biaya_sendiri_abdimas_ts']=$this->Standar7_model->get_biaya_sendiri_abdimas_ts();
				 	$data['pt_bersangkutan_abdimas_ts']=$this->Standar7_model->get_pt_bersangkutan_abdimas_ts();
				 		$data['pt_bersangkutan_abdimas_ts1']=$this->Standar7_model->get_pt_bersangkutan_abdimas_ts1();


				 		$data['pt_bersangkutan_abdimas_ts2']=$this->Standar7_model->get_pt_bersangkutan_abdimas_ts2();
				 		$data['depdiknas_abdimas_ts']=$this->Standar7_model->get_depdiknas_abdimas_ts();
				 		$data['depdiknas_abdimas_ts1']=$this->Standar7_model->get_depdiknas_abdimas_ts1();
				 		$data['depdiknas_abdimas_ts2']=$this->Standar7_model->get_depdiknas_abdimas_ts2();
				 		$data['luar_depdiknas_abdimas_ts2']=$this->Standar7_model->get_luar_depdiknas_abdimas_ts2();
				 		$data['luar_depdiknas_abdimas_ts1']=$this->Standar7_model->get_luar_depdiknas_abdimas_ts1();
				 		$data['luar_depdiknas_abdimas_ts']=$this->Standar7_model->get_luar_depdiknas_abdimas_ts();
				 		$data['view722']=$this->Standar7_model->view722();

						$data['result']=$this->Standar7_model->kerjasamaviewdalamnegerimodel();
							$data['result1']=$this->Standar7_model->kerjasamaviewluarnegerimodel();
							$data['count_dn']=$this->Standar7_model->count_dn();
							$data['count_ln']=$this->Standar7_model->count_ln();



										$file_name = $numberfile."_standar7.pdf";

				            $pdfFilePath = FCPATH."uploads/report/".$file_name; //tentukan nama file dan lokasi report yang akan kita buat
											date_default_timezone_set('Asia/Jakarta');
												$tgl_cetak = date("Y-m-d H:i:s");
											 ini_set('memory_limit','128M');



											 $html = $this->load->view('reportstd7', $data,true); // menyimpan hasil HTML ke variabel $html
											 $this->load->library('m_pdf1');
											 $pdf = $this->m_pdf1->load();
											 $stylesheet = file_get_contents(base_url().'pdf.css');
											 $pdf->SetFooter(base_url().'|{PAGENO}|'.$tgl_cetak);
											 $pdf->WriteHTML($html); // generate file pdf dari $html
											 $pdf->Output($pdfFilePath, 'F'); // save ke direktori $pdfFilePath
											 $pdf->Output($pdfFilePath, 'I');
											  // force_download($pdfFilePath, null);


																							 $config['file_name'] = $file_name;
																							 $this->upload->initialize($config);
																							 $info_upload = $this->upload->data();

																							 $data = array(
																								'file_names' => $info_upload['file_name'],
																								'file_date' =>  date("Y-m-d H:i:s"),
																								 //"text"=> $this->input->post('data'),


																						 );
																						 $this->Standar7_model->versionpdf($data);





	//	 $m_pdf1->Output($pdfFilePath, 'f');
	}

	function rptpenelitian711(){
		$this->load->helper('url');
			$this->load->model('Users_model');

			$numberfile = time();

			$data['user']=$this->Users_model->get_userinfo($this->session->userdata('email'));
			$this->load->model('Standar7_model');

			$data['data_dosen_internasional']=$this->Standar7_model->get_dsninter();
		  $data['data_dosen_nasional']=$this->Standar7_model->get_dsnnsl();
		  $data['data_dosen_lokal']=$this->Standar7_model->get_dsnlkl();
		  $data['jml_dosen']=$this->Standar7_model->jmldosen();
		  $data['biaya_luar_negeri']=$this->Standar7_model->get_biaya_luar_negeri();
		  $data['biaya_sendiri']=$this->Standar7_model->get_biaya_sendiri();
		  $data['pt_bersangkutan']=$this->Standar7_model->get_pt_bersangkutan();
		  $data['depdiknas']=$this->Standar7_model->get_depdiknas();
		  $data['luar_depdiknas']=$this->Standar7_model->get_luar_depdiknas();



		  $data['biaya_luar_negeri_ts']=$this->Standar7_model->get_biaya_luar_negeri_ts();
		  $data['biaya_luar_negeri_ts1']=$this->Standar7_model->get_biaya_luar_negeri_ts1();
		  $data['biaya_luar_negeri_ts2']=$this->Standar7_model->get_biaya_luar_negeri_ts2();
		  $data['biaya_sendiri_ts']=$this->Standar7_model->get_biaya_sendiri_ts();
		  $data['biaya_sendiri_ts1']=$this->Standar7_model->get_biaya_sendiri_ts1();
		  $data['biaya_sendiri_ts2']=$this->Standar7_model->get_biaya_sendiri_ts2();
		  $data['pt_bersangkutan_ts']=$this->Standar7_model->get_pt_bersangkutan_ts();

		  $data['pt_bersangkutan_ts1']=$this->Standar7_model->get_pt_bersangkutan_ts1();
		  $data['pt_bersangkutan_ts2']=$this->Standar7_model->get_pt_bersangkutan_ts2();
		  $data['depdiknas_ts']=$this->Standar7_model->get_depdiknas_ts();
		  $data['depdiknas_ts1']=$this->Standar7_model->get_depdiknas_ts1();
		  $data['depdiknas_ts2']=$this->Standar7_model->get_depdiknas_ts2();
		  $data['luar_depdiknas_ts']=$this->Standar7_model->get_luar_depdiknas_ts();
		  $data['luar_depdiknas_ts1']=$this->Standar7_model->get_luar_depdiknas_ts1();
		  $data['luar_depdiknas_ts2']=$this->Standar7_model->get_luar_depdiknas_ts2();

						            $pdfFilePath = FCPATH."uploads/report/".$numberfile."_standar7_7_1_1.pdf"; //tentukan nama file dan lokasi report yang akan kita buat

														$tgl_cetak = date('d F Y H:i:s');
													 ini_set('memory_limit','128M');

													 $html = $this->load->view('standar7/reportstd7711', $data,true); // menyimpan hasil HTML ke variabel $html
													 $this->load->library('m_pdf1');
													 $pdf = $this->m_pdf1->load();
													 $stylesheet = file_get_contents(base_url().'pdf.css');
													 $pdf->SetFooter(base_url().'|{PAGENO}|'.$tgl_cetak);
													 $pdf->WriteHTML($html); // generate file pdf dari $html
													 $pdf->Output($pdfFilePath, I); // save ke direktori $pdfFilePath
													//force_download($pdfFilePath, null);

	}
	function rptpenelitian712(){
		$this->load->helper('url');
			$this->load->model('Users_model');

			$numberfile = time();

			$data['user']=$this->Users_model->get_userinfo($this->session->userdata('email'));
			$this->load->model('Standar7_model');
			 $data['jumlah_mahasiswa_peldosta']=$this->Standar7_model->jumlah_mahasiswa_peldosta();

 			 $data['jumlah_mahasiswa_pelta']=$this->Standar7_model->jumlah_mahasiswa_pelta();


						            $pdfFilePath = FCPATH."uploads/report/".$numberfile."_standar7_7_1_2.pdf"; //tentukan nama file dan lokasi report yang akan kita buat

														$tgl_cetak = date('d F Y H:i:s');
													 ini_set('memory_limit','128M');

													 $html = $this->load->view('standar7/reportstd7712', $data,true); // menyimpan hasil HTML ke variabel $html
													 $this->load->library('m_pdf1');
													 $pdf = $this->m_pdf1->load();
													 $stylesheet = file_get_contents(base_url().'pdf.css');
													 $pdf->SetFooter(base_url().'|{PAGENO}|'.$tgl_cetak);
													 $pdf->WriteHTML($html); // generate file pdf dari $html
													 $pdf->Output($pdfFilePath, I); // save ke direktori $pdfFilePath
													//force_download($pdfFilePath, null);

	}
	function rptpenelitian713(){
		$this->load->helper('url');
			$this->load->model('Users_model');

			$numberfile = time();

			$data['user']=$this->Users_model->get_userinfo($this->session->userdata('email'));
			$this->load->model('Standar7_model');

			$data['publikasi']=$this->Standar7_model->viewpublikasi();
			$data['data_dosen_internasional']=$this->Standar7_model->get_dsninter();
			$data['data_dosen_nasional']=$this->Standar7_model->get_dsnnsl();
			$data['data_dosen_lokal']=$this->Standar7_model->get_dsnlkl();

						            $pdfFilePath = FCPATH."uploads/report/".$numberfile."_standar7_1_1_3.pdf"; //tentukan nama file dan lokasi report yang akan kita buat

														$tgl_cetak = date('d F Y H:i:s');
													 ini_set('memory_limit','128M');

													 $html = $this->load->view('standar7/reportstd7713', $data,true); // menyimpan hasil HTML ke variabel $html
													 $this->load->library('m_pdf1');
													 $pdf = $this->m_pdf1->load();
													 $stylesheet = file_get_contents(base_url().'pdf.css');
													 $pdf->SetFooter(base_url().'|{PAGENO}|'.$tgl_cetak);
													 $pdf->WriteHTML($html); // generate file pdf dari $html
													 $pdf->Output($pdfFilePath, I); // save ke direktori $pdfFilePath
													//force_download($pdfFilePath, null);

	}
	function rptpenelitian714(){
		$this->load->helper('url');
			$this->load->model('Users_model');

			$numberfile = time();

			$data['user']=$this->Users_model->get_userinfo($this->session->userdata('email'));
			$this->load->model('Standar7_model');

			 $data['result_haki']=$this->Standar7_model->listhaki();

						            $pdfFilePath = FCPATH."uploads/report/".$numberfile."_standar7_7_1_4.pdf"; //tentukan nama file dan lokasi report yang akan kita buat

														$tgl_cetak = date('d F Y H:i:s');
													 ini_set('memory_limit','128M');

													 $html = $this->load->view('standar7/reportstd7714', $data,true); // menyimpan hasil HTML ke variabel $html
													 $this->load->library('m_pdf1');
													 $pdf = $this->m_pdf1->load();
													 $stylesheet = file_get_contents(base_url().'pdf.css');
													 $pdf->SetFooter(base_url().'|{PAGENO}|'.$tgl_cetak);
													 $pdf->WriteHTML($html); // generate file pdf dari $html
													 $pdf->Output($pdfFilePath, I); // save ke direktori $pdfFilePath
													//force_download($pdfFilePath, null);

	}
	function rptpenelitian721(){
		$this->load->helper('url');
			$this->load->model('Users_model');

			$numberfile = time();

			$data['user']=$this->Users_model->get_userinfo($this->session->userdata('email'));
			$this->load->model('Standar7_model');

			$data['biaya_luar_negeri_abdimas']=$this->Standar7_model->get_biaya_luar_negeri_abdimas();
		 $data['biaya_sendiri_abdimas']=$this->Standar7_model->get_biaya_sendiri_abdimas();
		 $data['pt_bersangkutan_abdimas']=$this->Standar7_model->get_pt_bersangkutan_abdimas();
		 $data['depdiknas_abdimas']=$this->Standar7_model->get_depdiknas_abdimas();
		 $data['luar_depdiknas_abdimas']=$this->Standar7_model->get_luar_depdiknas_abdimas();
		 $data['jml_dosen']=$this->Standar7_model->jmldosen();
		 $data['nm_mahasiswa']=$this->Standar7_model->viewabdimas();
		 $data['nm_mahasiswa']=$this->Standar7_model->viewabdimas();



		 $data['biaya_luar_negeri_abdimas_hitung_ts2']=$this->Standar7_model->get_biaya_luar_negeri_abdimas_hitung_ts2();
		 $data['biaya_luar_negeri_abdimas_hitung_ts1']=$this->Standar7_model->get_biaya_luar_negeri_abdimas_hitung_ts1();
		 $data['biaya_luar_negeri_abdimas_hitung_ts']=$this->Standar7_model->get_biaya_luar_negeri_abdimas_hitung_ts();
		 $data['biaya_sendiri_abdimas_ts2']=$this->Standar7_model->get_biaya_sendiri_abdimas_ts2();
		 $data['biaya_sendiri_abdimas_ts1']=$this->Standar7_model->get_biaya_sendiri_abdimas_ts1();
		 $data['biaya_sendiri_abdimas_ts']=$this->Standar7_model->get_biaya_sendiri_abdimas_ts();
		 $data['pt_bersangkutan_abdimas_ts']=$this->Standar7_model->get_pt_bersangkutan_abdimas_ts();
			 $data['pt_bersangkutan_abdimas_ts1']=$this->Standar7_model->get_pt_bersangkutan_abdimas_ts1();


			 $data['pt_bersangkutan_abdimas_ts2']=$this->Standar7_model->get_pt_bersangkutan_abdimas_ts2();
			 $data['depdiknas_abdimas_ts']=$this->Standar7_model->get_depdiknas_abdimas_ts();
			 $data['depdiknas_abdimas_ts1']=$this->Standar7_model->get_depdiknas_abdimas_ts1();
			 $data['depdiknas_abdimas_ts2']=$this->Standar7_model->get_depdiknas_abdimas_ts2();
			 $data['luar_depdiknas_abdimas_ts2']=$this->Standar7_model->get_luar_depdiknas_abdimas_ts2();
			 $data['luar_depdiknas_abdimas_ts1']=$this->Standar7_model->get_luar_depdiknas_abdimas_ts1();
			 $data['luar_depdiknas_abdimas_ts']=$this->Standar7_model->get_luar_depdiknas_abdimas_ts();

						            $pdfFilePath = FCPATH."uploads/report/".$numberfile."_standar7_7_2_1.pdf"; //tentukan nama file dan lokasi report yang akan kita buat

														$tgl_cetak = date('d F Y H:i:s');
													 ini_set('memory_limit','128M');

													 $html = $this->load->view('standar7/reportstd7721', $data,true); // menyimpan hasil HTML ke variabel $html
													 $this->load->library('m_pdf1');
													 $pdf = $this->m_pdf1->load();
													 $stylesheet = file_get_contents(base_url().'pdf.css');
													 $pdf->SetFooter(base_url().'|{PAGENO}|'.$tgl_cetak);
													 $pdf->WriteHTML($html); // generate file pdf dari $html
													 $pdf->Output($pdfFilePath, I); // save ke direktori $pdfFilePath
													//force_download($pdfFilePath, null);

	}
	function rptpenelitian722(){
		$this->load->helper('url');
			$this->load->model('Users_model');

			$numberfile = time();

			$data['user']=$this->Users_model->get_userinfo($this->session->userdata('email'));
			$this->load->model('Standar7_model');

			$data['view722']=$this->Standar7_model->view722();

						            $pdfFilePath = FCPATH."uploads/report/".$numberfile."_standar7_7_2_2.pdf"; //tentukan nama file dan lokasi report yang akan kita buat

														$tgl_cetak = date('d F Y H:i:s');
													 ini_set('memory_limit','128M');

													 $html = $this->load->view('standar7/reportstd7722', $data,true); // menyimpan hasil HTML ke variabel $html
													 $this->load->library('m_pdf1');
													 $pdf = $this->m_pdf1->load();
													 $stylesheet = file_get_contents(base_url().'pdf.css');
													 $pdf->SetFooter(base_url().'|{PAGENO}|'.$tgl_cetak);
													 $pdf->WriteHTML($html); // generate file pdf dari $html
													 $pdf->Output($pdfFilePath, I); // save ke direktori $pdfFilePath
													//force_download($pdfFilePath, null);

	}
	function rptpenelitian731(){

		$this->load->helper('url');
			$this->load->model('Users_model');

			$numberfile = time();

			$data['user']=$this->Users_model->get_userinfo($this->session->userdata('email'));
			$this->load->model('Standar7_model');

			$data['result']=$this->Standar7_model->kerjasamaviewdalamnegerimodel();
				$data['result1']=$this->Standar7_model->kerjasamaviewluarnegerimodel();
				$data['count_dn']=$this->Standar7_model->count_dn();
				$data['count_ln']=$this->Standar7_model->count_ln();

						            $pdfFilePath = FCPATH."uploads/report/".$numberfile."_standar7_7_3_1.pdf"; //tentukan nama file dan lokasi report yang akan kita buat

														$tgl_cetak = date('d F Y H:i:s');
													 ini_set('memory_limit','128M');

													 $html = $this->load->view('standar7/reportstd7731', $data,true); // menyimpan hasil HTML ke variabel $html
													 $this->load->library('m_pdf1');
													 $pdf = $this->m_pdf1->load();
													 $stylesheet = file_get_contents(base_url().'pdf.css');
													 $pdf->SetFooter(base_url().'|{PAGENO}|'.$tgl_cetak);
													 $pdf->WriteHTML($html); // generate file pdf dari $html
													 $pdf->Output($pdfFilePath, I); // save ke direktori $pdfFilePath
													//force_download($pdfFilePath, null);
	}
	function rptpenelitian732(){
		$this->load->helper('url');
			$this->load->model('Users_model');

			$numberfile = time();

			$data['user']=$this->Users_model->get_userinfo($this->session->userdata('email'));
			$this->load->model('Standar7_model');

			$data['result']=$this->Standar7_model->kerjasamaviewdalamnegerimodel();
				$data['result1']=$this->Standar7_model->kerjasamaviewluarnegerimodel();
				$data['count_dn']=$this->Standar7_model->count_dn();
				$data['count_ln']=$this->Standar7_model->count_ln();


						            $pdfFilePath = FCPATH."uploads/report/".$numberfile."_standar7_7_3_2.pdf"; //tentukan nama file dan lokasi report yang akan kita buat

														$tgl_cetak = date('d F Y H:i:s');
													 ini_set('memory_limit','128M');

													 $html = $this->load->view('standar7/reportstd7732', $data,true); // menyimpan hasil HTML ke variabel $html
													 $this->load->library('m_pdf1');
													 $pdf = $this->m_pdf1->load();
													 $stylesheet = file_get_contents(base_url().'pdf.css');
													 $pdf->SetFooter(base_url().'|{PAGENO}|'.$tgl_cetak);
													 $pdf->WriteHTML($html); // generate file pdf dari $html
													 $pdf->Output($pdfFilePath, I); // save ke direktori $pdfFilePath
													//force_download($pdfFilePath, null);
	}



}

?>
