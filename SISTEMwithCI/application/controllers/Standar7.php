<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Standar7 extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('uploadedfile_modelstd7','standar7');

		//$this->load->library('PHPExcel');
	  //$this->load->library(array('PHPExcel','PHPExcel/IOFactory'));

		$this->load->helper(array('form', 'url'));
		$this->load->library('session');
	}



//------------------Controller Kerjasama-----------------------

	  public function kerjasama() {
		$this->load->helper('url');
		$this->load->model('Standar7_model');
		$data['result']=$this->Standar7_model->kerjasamaviewdalamnegerimodel();
			$data['result1']=$this->Standar7_model->kerjasamaviewluarnegerimodel();
			$data['count_dn']=$this->Standar7_model->count_dn();
			$data['count_ln']=$this->Standar7_model->count_ln();
	$data['nilai_luar']=$this->Standar7_model->nilai_sementaraluar();
		$data['nilai_dalam']=$this->Standar7_model->nilai_sementaradalam();
		$this->load->model('Users_model');
		$data['user']=$this->Users_model->get_userinfo($this->session->userdata('email'));

		$this->load->view('standar7/kerjasama', $data);
	}

	public function kerjasamaupdateln() {
	$this->load->model('Standar7_model');
		$data = array(
		'nilai' => $this->input->post('nilai'),



		);
		$id =  $this->input->post('id');

		$this->Standar7_model->kerjasamaupdateln($id, $data);



}


public function publikasiupdate() {
$this->load->model('Standar7_model');
	$data = array(
		'prodi' => $this->input->post('prodi'),
	'no_ts' => $this->input->post('no_ts'),
	'skl_publikasi' => $this->input->post('skl_publikasi'),
	'judul' => $this->input->post('judul'),
	'jns_publikasi' => $this->input->post('jns_publikasi'),
	'nm_seminar' => $this->input->post('nm_seminar'),
	'penulis_utm' => $this->input->post('penulis_utm'),
	'penulis_tmbh' => $this->input->post('penulis_tmbh'),
	'penulis_tmbh2' => $this->input->post('penulis_tmbh2'),
	'penulis_tmbh3' => $this->input->post('penulis_tmbh3'),
	'penulis_tmbh4' => $this->input->post('penulis_tmbh4'),
	'penulis_tmbh5' => $this->input->post('penulis_tmbh5'),
	'tanggal' => $this->input->post('tanggal'),
	'ket' => $this->input->post('ket'),
	'jlm_dosen_si' => $this->input->post('jlm_dosen_si'),
	'id_user' => $this->session->userdata('id_user'),


	);
	$id =  $this->input->post('id');

	$this->Standar7_model->publikasidataupdate($id, $data);



}

public function publikasiinsert() {
$this->load->model('Standar7_model');
	$data = array(
	'prodi' => $this->input->post('prodi'),
	'no_ts' => $this->input->post('no_ts'),
	'skl_publikasi' => $this->input->post('skl_publikasi'),
	'judul' => $this->input->post('judul'),
	'jns_publikasi' => $this->input->post('jns_publikasi'),
	'nm_seminar' => $this->input->post('nm_seminar'),
	'penulis_utm' => $this->input->post('penulis_utm'),
	'penulis_tmbh' => $this->input->post('penulis_tmbh'),
	'penulis_tmbh2' => $this->input->post('penulis_tmbh2'),
	'penulis_tmbh3' => $this->input->post('penulis_tmbh3'),
	'penulis_tmbh4' => $this->input->post('penulis_tmbh4'),
	'penulis_tmbh5' => $this->input->post('penulis_tmbh5'),
	'tanggal' => $this->input->post('tanggal'),
	'ket' => $this->input->post('ket'),
	'jlm_dosen_si' => $this->input->post('jlm_dosen_si'),
	'id_user' => $this->session->userdata('id_user'),


	);

  $insert = $this->db->insert("tb_publikasi",$data);
	 	 $this->session->set_flashdata('msgp', 'Berhasil menginsert data');
redirect('Standar7/publikasi');


}
public function hakiiinsert() {
$this->load->model('Standar7_model');
	$data = array(
	'karya' => $this->input->post('karya'),
	'jenis_haki' => $this->input->post('jenishaki'),
	'tahun' => $this->input->post('tahun'),
'id_user' => $this->session->userdata('id_user'),

	);



  $insert = $this->db->insert("tb_haki",$data);
	 $this->session->set_flashdata('msg', 'Berhasil menginsert data');
redirect('Standar7/haki');


}


public function penelitiandoseninsert() {
$this->load->model('Standar7_model');
	$data = array(
		'idpeldos' => $this->input->post('idpeldos'),
	'program_studi' => $this->input->post('program_studi'),
	'no_ts' => $this->input->post('no_ts','strtoupper'),
	'tipe' => $this->input->post('tipe'),
	'judul' => $this->input->post('judul'),
	'tahun' => $this->input->post('tahun'),
	'nama_dosenketua' => $this->input->post('nama_ketua_dosen'),
	'nama_dosenanggota_1' => $this->input->post('nama_dosen_anggota'),
	'nama_dosenanggota_2' => $this->input->post('nama_dosen_anggota2'),
	'nama_dosenanggota_3' => $this->input->post('nama_dosen_anggota3'),
	'nama_dosenanggota_4' => $this->input->post('nama_dosen_anggota4'),
	'nama_dosenanggota_5' => $this->input->post('nama_dosen_anggota5'),
	'nama_mahasiswa_1' => $this->input->post('nama_mahasiswa'),
	'status' => $this->input->post('status'),
	'anggaran' => $this->input->post('anggaran'),
	'keterangan' => $this->input->post('keterangan'),
	'sumber_pembiayaan' => $this->input->post('sumber_pembiayaan'),
	'jenis_penelitian' => $this->input->post('jenis_penelitian'),
	"id_user"=> $this->session->userdata('id_user'),
	);



  $insert = $this->db->insert("tb_penelitian_dosen",$data);
	 $this->session->set_flashdata('msga', 'Berhasil menginsert data');
redirect('Standar7/penelitiandosen');


}

public function penelitiandosentainsert() {
$this->load->model('Standar7_model');
	$data = array(
	'prodi' => $this->input->post('program_studi'),
	'no_ts' => $this->input->post('no_ts'),
	'id_peldos' => $this->input->post('idpeldos'),
	'judul_penelitian_dosen' => $this->input->post('judul_penelitian_dosen'),
	'nama_dosenketua' => $this->input->post('nama_dosenketua'),
	'nama_dosenanggota' => $this->input->post('nama_dosenanggota'),

	'tahun' => $this->input->post('tahun'),

	'nama_mahasiswa' => $this->input->post('nama_mahasiswa'),
	'judul_ta_mahasiswa' => $this->input->post('judul_ta_mahasiswa'),
		'bukti_laporan_akhir' => $this->input->post('bukti_laporan_akhir'),

	'keterangan' => $this->input->post('keterangan'),
	'id_user' => $this->session->userdata('id_user'),
	);



  $insert = $this->db->insert("tb_peldosen_ta",$data);
	 $this->session->set_flashdata('msgta', 'Berhasil menginsert data');
redirect('Standar7/penelitiandosenta');


}





public function abdimasinsert() {
$this->load->model('Standar7_model');
	$data = array(
	'program_studi' => $this->input->post('program_studi'),
	'no_ts' => $this->input->post('no_ts','strtoupper'),
	'type' => $this->input->post('tipe'),
	'judul' => $this->input->post('judul'),
	'lokasi' => $this->input->post('lokasi'),
	'mitra' => $this->input->post('mitra'),
	'tahun' => $this->input->post('tahun'),

	'nama_dosen_ketua' => $this->input->post('nama_dosen_ketua'),


	'nama_dosen_anggota' => $this->input->post('nama_dosen_anggota'),
	'nama_anggotadosen2' => $this->input->post('nama_dosen_anggota2'),
	'nama_anggotadosen3' => $this->input->post('nama_dosen_anggota3'),
	'nama_anggotadosen4' => $this->input->post('nama_dosen_anggota4'),
	'nama_anggotadosen5' => $this->input->post('nama_dosen_anggota5'),
		'nama_mahasiswa' => $this->input->post('nama_mahasiswa'),
'anggaran' => $this->input->post('anggaran'),
'bukti_laporan_akhir' => $this->input->post('bukti_laporan_akhir'),
	'keterangan' => $this->input->post('keterangan'),
	'sumber_pembiayaan' => $this->input->post('sumber_pembiayaan'),
	'id_user' => $this->session->userdata('id_user'),
	);



  $insert = $this->db->insert("tb_abdimas",$data);
	 $this->session->set_flashdata('msg', 'Berhasil menginsert data');
redirect('Standar7/abdimasdata');


}



public function simulasinilai() {
$this->load->model('Standar7_model');
$napen = $this->session->userdata('nama');
date_default_timezone_set('Asia/Jakarta');
	$data = array(
	'nilai' => $this->input->post('nilai1'),
	'simulasi_nilai' => $this->input->post('sim_nilai'),
	'penilai_terakhir' => $napen,
	'date' =>  date("d-m-Y H:i:s"),
	);

	$data1 = array(


		'id_standar7' => $this->input->post('id1'),
	'nama_penilai' => $napen,
'bobot' => $this->input->post('bobot'),
'nilai' => $this->input->post('nilai1'),
'date' => date("d-m-Y H:i:s"),

	);


	$id =  $this->input->post('id1');
	$this->Standar7_model->record_penilai($data1);
	$this->Standar7_model->simulasinilai($id, $data);



}

public function kerjasamadata() {
		$this->load->helper('url');
		$this->load->model('Standar7_model');
		$data['result']=$this->Standar7_model->kerjasamaviewmodel();
		$data['pengupdate']=$this->Standar7_model->pengupdatekerjasama();
		$this->load->model('Users_model');



		$data['user']=$this->Users_model->get_userinfo($this->session->userdata('email'));
		$this->load->view('standar7/kerjasama_data', $data);
	}

	public function kerjasamaupload() {
		$this->load->helper('url');
		$this->load->view('standar7/kerjasama_uploaddoc');
	}

	public function kerjasamaupdate() {

	 $this->load->model('Standar7_model');
	 $data = array(
	 'nama_instansi' => $this->input->post('nama_instansi'),
	 'jenis_kegiatan' => $this->input->post('jenis_kegiatan'),
	 'mulai_kerjasama' => $this->input->post('mulai_kerjasama'),
		 'akhir_kerjasama' => $this->input->post('akhir_kerjasama'),
	 'instasi_negeri' => $this->input->post('instansi_negeri'),
	 'keterangan' => $this->input->post('keterangan'),
	 'manfaat' => $this->input->post('manfaat'),
	 'id_user' => $this->session->userdata('id_user'),

	 );
 $id =  $this->input->post('id');

	 $this->Standar7_model->kerjasamadataupdate($id, $data);



	 }

 public function kerjasamauploadpdf($id) {
	 $this->load->model('Standar7_model');

		 unlink(FCPATH.'uploads/dokumen_kerjasama_std7/'.$this->input->post('data-filename'));

			 $fileName = $_FILES['files']['name'];

			 $config['file_name'] = $fileName;
			 date_default_timezone_set('Asia/Jakarta');



	 $config['upload_path']   = './uploads/dokumen_kerjasama_std7/';
				$config['allowed_types'] = 'pdf';
				$config['max_size']      = 60000 ;


			 $this->load->library('upload');
			 $this->upload->initialize($config);



				if ( ! $this->upload->do_upload('files')) {
					 $error = array('error' => $this->upload->display_errors());

		 $this->session->set_flashdata('msg_upload', $this->upload->display_errors());

				}

				else {
			 $this->load->model('Standar7_model');

				 $info_upload = $this->upload->data();
			 $data = array('file_name' => $info_upload['file_name'],  'file_type' => $info_upload['file_type'], 'file_date' =>  date("Y-m-d H:i:s"));
	 $this->Standar7_model->kerjasamadatauploadpdf($id, $data);

				}
			 redirect('/Standar7/kerjasamadata');

 }

	 public function kerjasamaimport() {

		$this->load->library('PHPExcel');
		$this->load->library(array('PHPExcel','PHPExcel/IOFactory'));
        $fileName = $_FILES['files']['name'];

        $config['upload_path'] = './uploads/dokumenstandar7/';
        $config['file_name'] = $fileName;
        $config['allowed_types'] = 'xls|xlsx|csv';
        $config['max_size'] = 40000;

        $this->load->library('upload');
        $this->upload->initialize($config);

        if(! $this->upload->do_upload('files') )
        $this->upload->display_errors();

        $media = $this->upload->data('files');
       $inputFileName = $this->upload->data('full_path');

        try {
                $inputFileType = IOFactory::identify($inputFileName);
                $objReader = IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($inputFileName);
            } catch(Exception $e) {
                die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
            }

            $sheet = $objPHPExcel->getSheet(0);
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();

            for ($row = 2; $row <= $highestRow; $row++){                  //  Read a row of data into an array
                $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                                                NULL,
                                                TRUE,
                                                FALSE);

                //Sesuaikan sama nama kolom tabel di database
                 $data = array(
                    "nama_instansi"=> $rowData[0][0],
                    "jenis_kegiatan"=> $rowData[0][1],
                    "mulai_kerjasama"=> PHPExcel_Style_NumberFormat::toFormattedString($rowData[0][2],'DD-MM-YYYY' ),
                    "akhir_kerjasama"=> PHPExcel_Style_NumberFormat::toFormattedString($rowData[0][3],'DD-MM-YYYY' ),
					"instasi_negeri"=> $rowData[0][4],
                    "keterangan"=> $rowData[0][5],
										  "manfaat"=> $rowData[0][6],
										  "id_user"=> $this->session->userdata('id_user'),


                );

                //sesuaikan nama dengan nama tabel
                $insert = $this->db->insert("tb_kerjasama",$data);
                delete_files($media['file_path']);

            }
			$this->session->set_flashdata('msg', 'Berhasil Mengimport Data');
        redirect('/Standar7/kerjasamadata?success');
    }

		public function kerjasamainsert() {

	 $this->load->model('Standar7_model');
	 $data = array(
'nama_instansi' => $this->input->post('nama-instansi'),
'jenis_kegiatan' => $this->input->post('jenis-kegiatan'),
'mulai_kerjasama' => $this->input->post('mulaikegiatan'),
'akhir_kerjasama' => $this->input->post('akhirkegiatan'),
'instasi_negeri' => $this->input->post('instansinegeri'),
'keterangan' => $this->input->post('ket'),
'manfaat' => $this->input->post('manfaat'),
"id_user"=> $this->session->userdata('id_user')

);

$this->Standar7_model->kerjasamainsert($data);
$this->session->set_flashdata('msga', 'Berhasil input data');
redirect('Standar7/kerjasamadata');
 }

	function kerjasamahapus($id){
		$where = array('id' => $id);
unlink(FCPATH.'uploads/dokumen_kerjasama_std7/'.$this->input->post('data-filename'));

		$this->load->model('Standar7_model');
		$this->Standar7_model->kerjasamahapusmodel($where,'tb_kerjasama');
		$this->session->set_flashdata('msg', 'Berhasil menghapus data');
		redirect('Standar7/kerjasamadata?success');
	}


	function kerjasamahapuschecklist(){
$this->load->model('Standar7_model');
		$check = json_decode($this->input->post('result'), true);
		$jumlah_dipilih = count($check);
		for($x=0;$x<$jumlah_dipilih;$x++){


$a = $this->Standar7_model->kerjasamaviewmodel_getid($check[$x]);
unlink(FCPATH.'uploads/dokumen_kerjasama_std7/'.$a->file_name);
		}

		$this->Standar7_model->kerjasamahapuschecklistmodel('tb_kerjasama');
		redirect('Standar7/kerjasamadata?success');
	}

	function kerjasamahapussemua(){
		$this->load->model('Standar7_model');

$ha['listhapus'] = $this->Standar7_model->kerjasamaviewmodel();

foreach($ha['listhapus']  as $as ){
if(isset($as->file_name)){
unlink(FCPATH.'uploads/dokumen_kerjasama_std7/'.$as->file_name);

}


}

		$this->Standar7_model->kerjasamahapussemuamodel('tb_kerjasama');




		$this->session->set_flashdata('msg', 'Berhasil menghapus semua data');
		redirect('Standar7/kerjasamadata?success');
	}

 //------------------------- Controller Kerjasama end -----------------------------------










//--------------------- Penelitian -----------------------------

function penelitiandosentahapus($id){
	$where = array('no' => $id);
	$this->load->model('Standar7_model');
	unlink(FCPATH.'uploads/dokumen_peldosTA_std7/'.$this->input->post('data-filename'));

	$this->Standar7_model->penelitandosentahapusmodel($where,'tb_peldosen_ta');
	$this->session->set_flashdata('msg', 'Berhasil menghapus data');
	redirect('Standar7/penelitiandosenta');
}

function penelitiandosentahapuschecklist(){
$this->load->model('Standar7_model');

$check = json_decode($this->input->post('result'), true);
$jumlah_dipilih = count($check);
for($x=0;$x<$jumlah_dipilih;$x++){



	$a = $this->Standar7_model->penelitiandosentaviewmodel_getid($check[$x]);
	unlink(FCPATH.'uploads/dokumen_peldosTA_std7/'.$a->file_name);
}

	$this->Standar7_model->penelitiandosentahapuschecklistmodel('tb_peldosen_ta');
	redirect('Standar7/penelitiandosenta');
}

function penelitiandosentahapussemua() {


	$this->load->model('Standar7_model');

$ha['listhapus'] = $this->Standar7_model->kerjasamaviewmodel();

foreach($ha['listhapus']  as $as ){
if(isset($as->file_name)){
unlink(FCPATH.'uploads/dokumen_peldosTA_std7/'.$as->file_name);

}


}
}

function peldostahapussemua(){
	$this->load->model('Standar7_model');
	$ha['listhapus'] = $this->Standar7_model->viewpenelitian_dosenta();

	foreach($ha['listhapus']  as $as ){
	if(isset($as->file_name)){
	unlink(FCPATH.'uploads/dokumen_peldosTA_std7/'.$as->file_name);

	}
}
	$this->Standar7_model->peldosentahapussemuamodel('tb_peldosen_ta');
	$this->session->set_flashdata('msg', 'Berhasil menghapus semua data');
	redirect('Standar7/penelitiandosenta');
}


function importpenelitiandosenproses(){

	$this->load->library('PHPExcel');
  $this->load->library(array('PHPExcel','PHPExcel/IOFactory'));
 		 $fileName = $_FILES['files']['name'];

 		 $config['upload_path'] = './uploads/dokumenstandar7/';
 		 $config['file_name'] = $fileName;
 		 $config['allowed_types'] = 'xls|xlsx|csv';
 		 $config['max_size'] = 40000;

 		 $this->load->library('upload');
 		 $this->upload->initialize($config);

 		 if(! $this->upload->do_upload('files') )
 		 $this->upload->display_errors();

 		 $media = $this->upload->data('files');
 		$inputFileName = $this->upload->data('full_path');

 		 try {
 						 $inputFileType = IOFactory::identify($inputFileName);
 						 $objReader = IOFactory::createReader($inputFileType);
 						 $objPHPExcel = $objReader->load($inputFileName);
 				 } catch(Exception $e) {
 						 die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
 				 }

 				 $sheet = $objPHPExcel->getSheet(0);
 				 $highestRow = $sheet->getHighestRow();
 				 $highestColumn = $sheet->getHighestColumn();

 				 for ($row = 2; $row <= $highestRow; $row++){                  //  Read a row of data into an array
 						 $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
 																						 NULL,
 																						 TRUE,
 																						 FALSE);

 						 //Sesuaikan sama nama kolom tabel di database
 							$data = array(
								"idpeldos"=> $rowData[0][0],
 								 "program_studi"=> $rowData[0][1],
 								 "no_ts"=> $rowData[0][2],
 								 "tipe"=> $rowData[0][3],
 								 "judul"=> $rowData[0][4],
 			 			 		"tahun"=> $rowData[0][5],
 								 "nama_dosenketua"=> $rowData[0][6],
 								 "nama_dosenanggota_1"=> $rowData[0][7],
								 "nama_dosenanggota_2"=> $rowData[0][8],
								 "nama_dosenanggota_3"=> $rowData[0][9],
								 "nama_dosenanggota_4"=> $rowData[0][10],
								 "nama_dosenanggota_5"=> $rowData[0][11],
 								 "nama_mahasiswa_1"=> $rowData[0][12],
 								 "status"=> $rowData[0][13],
 								 "anggaran"=> $rowData[0][14],
 								"	keterangan"=> $rowData[0][15],
									"sumber_pembiayaan"=> $rowData[0][16],
									"jenis_penelitian"=> $rowData[0][17],
									 "id_user"=> $this->session->userdata('id_user'),


 						 );

 						 //sesuaikan nama dengan nama tabel
 						 $insert = $this->db->insert("tb_penelitian_dosen",$data);
 						 delete_files($media['file_path']);

 				 }
 	 $this->session->set_flashdata('msg', 'Berhasil Mengimport Data');
 		 redirect('/Standar7/penelitiandosen');

}

function peldoshapuschecklist1(){
	$this->load->model('Standar7_model');
	$check = json_decode($this->input->post('result'), true);
	$jumlah_dipilih = count($check);
	for($x=0;$x<$jumlah_dipilih;$x++){


$a = $this->Standar7_model->penelitiandosenviewmodel1($check[$x]);
unlink(FCPATH.'uploads/dokumen_peldos_std7/'.$a->file_name);
	}



	$this->Standar7_model->penelitiandosenhapuschecklistmodel('tb_penelitian_dosen');
	redirect('Standar7/penelitiandosen?success');
}

function peldoshapuschecklist(){
	$this->load->model('Standar7_model');
	$check = json_decode($this->input->post('result'), true);
	$jumlah_dipilih = count($check);
	for($x=0;$x<$jumlah_dipilih;$x++){


$a = $this->Standar7_model->viewpenelitian_dosenta_get_id1($check[$x]);
unlink(FCPATH.'uploads/dokumen_peldos_std7/'.$a->file_name);
	}



	$this->Standar7_model->penelitiandosenhapuschecklistmodel('tb_penelitian_dosen');
	redirect('Standar7/penelitiandosen?success');
}

function penelitianhapussemua(){
	$this->load->model('Standar7_model');
	$ha['listhapus'] = $this->Standar7_model->penelitiandosenviewmodel();

	foreach($ha['listhapus']  as $as ){
	if(isset($as->file_name)){
	unlink(FCPATH.'uploads/dokumen_peldos_std7/'.$as->file_name);

	}


	}
	$this->Standar7_model->penelitianhapussemuamodel('tb_penelitian_dosen');
	$this->session->set_flashdata('msg', 'Berhasil menghapus semua data');
	redirect('Standar7/penelitiandosen?success');
}

   public function penelitian() {

	$this->load->helper('url');
	$this->load->model('Users_model');
	$data['user']=$this->Users_model->get_userinfo($this->session->userdata('email'));
	$this->load->model('Standar7_model');
	$data['data_dosen_internasional']=$this->Standar7_model->get_dsninter();
	$data['data_dosen_nasional']=$this->Standar7_model->get_dsnnsl();
	$data['data_dosen_lokal']=$this->Standar7_model->get_dsnlkl();

	$data['jml_dosen']=$this->Standar7_model->jmldosen();

	$data['biaya_luar_negeri']=$this->Standar7_model->get_biaya_luar_negeri();
	$data['biaya_sendiri']=$this->Standar7_model->get_biaya_sendiri();
	$data['pt_bersangkutan']=$this->Standar7_model->get_pt_bersangkutan();
	$data['depdiknas']=$this->Standar7_model->get_depdiknas();
	$data['luar_depdiknas']=$this->Standar7_model->get_luar_depdiknas();
$data['publikasi']=$this->Standar7_model->viewpublikasi();

	$data['hitung_penilaian_tugas_akhir_dosen']=$this->Standar7_model->hitung_penilaian_tugas_akhir_dosen();
		$data['jumlah_mahasiswa_peldosta']=$this->Standar7_model->jumlah_mahasiswa_peldosta();
		$data['jumlah_mahasiswa_pelta']=$this->Standar7_model->jumlah_mahasiswa_pelta();


	$data['biaya_luar_negeri_ts']=$this->Standar7_model->get_biaya_luar_negeri_ts();
	$data['biaya_luar_negeri_ts1']=$this->Standar7_model->get_biaya_luar_negeri_ts1();
	$data['biaya_luar_negeri_ts2']=$this->Standar7_model->get_biaya_luar_negeri_ts2();
	$data['biaya_sendiri_ts']=$this->Standar7_model->get_biaya_sendiri_ts();
	$data['biaya_sendiri_ts1']=$this->Standar7_model->get_biaya_sendiri_ts1();
	$data['biaya_sendiri_ts2']=$this->Standar7_model->get_biaya_sendiri_ts2();
	$data['pt_bersangkutan_ts']=$this->Standar7_model->get_pt_bersangkutan_ts();

	$data['pt_bersangkutan_ts1']=$this->Standar7_model->get_pt_bersangkutan_ts1();
	$data['pt_bersangkutan_ts2']=$this->Standar7_model->get_pt_bersangkutan_ts2();
	$data['depdiknas_ts']=$this->Standar7_model->get_depdiknas_ts();
	$data['depdiknas_ts1']=$this->Standar7_model->get_depdiknas_ts1();
	$data['depdiknas_ts2']=$this->Standar7_model->get_depdiknas_ts2();
	$data['luar_depdiknas_ts']=$this->Standar7_model->get_luar_depdiknas_ts();
	$data['luar_depdiknas_ts1']=$this->Standar7_model->get_luar_depdiknas_ts1();
	$data['luar_depdiknas_ts2']=$this->Standar7_model->get_luar_depdiknas_ts2();

		$data['result_haki']=$this->Standar7_model->listhaki();
		$this->load->view('standar7/penelitian', $data);
	}


//---------------------Penelitian-Dosen----------------------------

public function penelitiandosentasiupdate() {

 $this->load->model('Standar7_model');
 $data = array(
 'prodi' => $this->input->post('program_studi'),
 'no_ts' => $this->input->post('no_ts'),
 'judul_penelitian_dosen' => $this->input->post('judul_penelitian_dosen'),
 'nama_dosenketua' => $this->input->post('nama_dosenketua'),
 'nama_dosenanggota' => $this->input->post('nama_dosenanggota'),
	 'tahun' => $this->input->post('tahun'),
 'nama_mahasiswa' => $this->input->post('nama_mahasiswa'),
 'judul_ta_mahasiswa' => $this->input->post('judul_ta_mahasiswa'),
 'bukti_laporan_akhir' => $this->input->post('bukti_laporan_akhir'),
'keterangan' => $this->input->post('keterangan'),
'id_user' => $this->session->userdata('id_user'),


 );
$id =  $this->input->post('id');

 $this->Standar7_model->penelitian_tasiupdate($id, $data);



 }



	public function penelitiandosen() {
		$this->load->helper('url');
		$this->load->model('Users_model');
		$data['user']=$this->Users_model->get_userinfo($this->session->userdata('email'));
		$this->load->model('Standar7_model');
		$data['result']=$this->Standar7_model->penelitiandosenviewmodel();
		$data['pengupdate']=$this->Standar7_model->pengupdatepenelitiandosen();
		$data['result_ts']=$this->Standar7_model->number_ts();

		$this->load->view('standar7/penelitian_dosen',$data);


	}



	public function penelitian_dosenuploadpdf($id) {
		$this->load->model('Standar7_model');
			unlink(FCPATH.'uploads/dokumen_peldos_std7/'.$this->input->post('data-filename'));
				$fileName = $_FILES['files']['name'];

				$config['file_name'] = $fileName;
				date_default_timezone_set('Asia/Jakarta');


		$config['upload_path']   = './uploads/dokumen_peldos_std7/';
				 $config['allowed_types'] = 'pdf';
				 $config['max_size']      = 60000 ;


				$this->load->library('upload');
				$this->upload->initialize($config);



				 if ( ! $this->upload->do_upload('files')) {
						$error = array('error' => $this->upload->display_errors());

			$this->session->set_flashdata('msg_upload', $this->upload->display_errors());

				 }

				 else {
				$this->load->model('Standar7_model');

					$info_upload = $this->upload->data();
				$data = array('file_name' => $info_upload['file_name'],  'file_type' => $info_upload['file_type'], 'file_date' =>  date("Y-m-d H:i:s"));
		$this->Standar7_model->penelitian_dosenupdate($id, $data);

				 }
		redirect('/Standar7/penelitiandosen');

	}

	public function importpenelitiandosensita(){

		$this->load->library('PHPExcel');
		$this->load->library(array('PHPExcel','PHPExcel/IOFactory'));
				$fileName = $_FILES['files']['name'];

				$config['upload_path'] = './uploads/dokumenstandar7/';
				$config['file_name'] = $fileName;
				$config['allowed_types'] = 'xls|xlsx|csv';
				$config['max_size'] = 40000;

				$this->load->library('upload');
				$this->upload->initialize($config);

				if(! $this->upload->do_upload('files') )
				$this->upload->display_errors();

				$media = $this->upload->data('files');
			 $inputFileName = $this->upload->data('full_path');



							 try {
											 $inputFileType = IOFactory::identify($inputFileName);
											 $objReader = IOFactory::createReader($inputFileType);
											 $objPHPExcel = $objReader->load($inputFileName);
									 } catch(Exception $e) {
											 die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
									 }

									 $sheet = $objPHPExcel->getSheet(0);
									 $highestRow = $sheet->getHighestRow();
									 $highestColumn = $sheet->getHighestColumn();

									 for ($row = 2; $row <= $highestRow; $row++){                  //  Read a row of data into an array
											 $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
																											 NULL,
																											 TRUE,
																											 FALSE);

							 //Sesuaikan sama nama kolom tabel di database
								$data = array(
									"id_peldos"=> $rowData[0][0],
									 "prodi"=> $rowData[0][1],
									 "no_ts"=> $rowData[0][2],
									 "judul_penelitian_dosen"=> $rowData[0][3],
									 "nama_dosenketua"=> $rowData[0][4],
									 "nama_dosenanggota"=> $rowData[0][5],
									 "tahun"=> $rowData[0][6],
									 "nama_mahasiswa"=> $rowData[0][7],
				 				 "judul_ta_mahasiswa"=> $rowData[0][8],
									 "bukti_laporan_akhir"=> $rowData[0][9],
									 "keterangan"=> $rowData[0][10],


	"id_user"=> $this->session->userdata('id_user'),

							 );

							 //sesuaikan nama dengan nama tabel
							 $insert = $this->db->insert("tb_peldosen_ta",$data);
							 delete_files($media['file_path']);

					 }
		 $this->session->set_flashdata('msg', 'Berhasil Mengimport Data');
			 redirect('/Standar7/penelitiandosenta');

	}




	public function importpenelitiandosen(){

		$this->load->library('PHPExcel');
	  $this->load->library(array('PHPExcel','PHPExcel/IOFactory'));
	 		 $fileName = $_FILES['files']['name'];
 		 $fileName1 = $_FILES['files1']['name'];
	 		 $config['upload_path'] = './uploads/dokumenstandar7/';
	 		 $config['file_name'] = $fileName;
	 		 $config['allowed_types'] = 'xls|xlsx|csv';
	 		 $config['max_size'] = 40000;

	 		 $this->load->library('upload');
	 		 $this->upload->initialize($config);

	 		 if(! $this->upload->do_upload('files') )
	 		 $this->upload->display_errors();

	 		 $media = $this->upload->data('files');
	 		$inputFileName = $this->upload->data('full_path');
			$media1 = $this->upload->data('files1');
		 $inputFileName1 = $this->upload->data('full_path');
	 		 try {
	 						 $inputFileType = IOFactory::identify($inputFileName);
	 						 $objReader = IOFactory::createReader($inputFileType);
	 						 $objPHPExcel = $objReader->load($inputFileName);

							 $inputFileType1 = IOFactory::identify($inputFileName1);
							 $objReader1 = IOFactory::createReader($inputFileType1);
							 $objPHPExcel1 = $objReader1->load($inputFileName1);
	 				 } catch(Exception $e) {
	 						 die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
	 				 }

	 				 $sheet = $objPHPExcel->getSheet(0);
	 				 $highestRow = $sheet->getHighestRow();
	 				 $highestColumn = $sheet->getHighestColumn();

	 				 for ($row = 2; $row <= $highestRow; $row++){                  //  Read a row of data into an array
	 						 $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
	 																						 NULL,
	 																						 TRUE,
	 																						 FALSE);

																							 $sheet1 = $objPHPExcel1->getSheet(0);
																							 $highestRow1= $sheet1->getHighestRow();
																							 $highestColumn1 = $sheet->getHighestColumn();

																							 for ($row1 = 2; $row1 <= $highestRow1; $row1++){                  //  Read a row of data into an array
																									 $rowData = $sheet->rangeToArray('A' . $row1 . ':' . $highestColumn1 . $row1,
																																									 NULL,
																																									 TRUE,
																																									 FALSE);


	 						 //Sesuaikan sama nama kolom tabel di database
	 							$data = array(
	 								 "no_ts"=> $rowData[0][0],
	 								 "skl_publikasi"=> $rowData[0][1],
	 								 "judul"=> $rowData[0][2],
	 								 "jns_publikasi"=> $rowData[0][3],
	 			 "nm_seminar"=> $rowData[0][4],
	 								 "penulis_utm"=> $rowData[0][5],
	 								 "penulis_tmbh"=> $rowData[0][6],
	 								 "tanggal"=> $rowData[0][7],
	 								 "tahun"=> $rowData[0][8],
	 								 "bkti_loa"=> $rowData[0][9],
	 								"bukti_jurnal"=> $rowData[0][10],
	 								 "ket"=> $rowData[0][11],
	 								 "jlm_dosen_si"=> $rowData[0][12],

 "pengupdate"=> $this->session->userdata('nama'),

	 						 );

	 						 //sesuaikan nama dengan nama tabel
	 						 $insert = $this->db->insert("tb_publikasi",$data);
	 						 delete_files($media['file_path']);

	 				 }
	 	 $this->session->set_flashdata('msg', 'Berhasil Mengimport Data');
	 		 redirect('/Standar7/publikasi');

	}

}

	function dosenhapussemua(){
		$this->load->model('Standar7_model');
		$this->Standar7_model->dosenhapussemuamodel('tb_dosen');
		$this->session->set_flashdata('msg', 'Berhasil menghapus semua data');
		redirect('Standar7/dosen');
	}


	function penelitian_dosenhapus($id){
		$this->load->model('Standar7_model');
		$data = array('peldos_no' => $id);
		unlink(FCPATH.'uploads/dokumen_peldos_std7/'.$this->input->post('data-filename'));

		$this->Standar7_model->penelitiandosenhapusmodel($data , 'tb_penelitian_dosen');
		$this->session->set_flashdata('msg', 'Berhasil menghapus  data');
		redirect('Standar7/penelitiandosen');
	}

	public function penelitiandosenupload() {
		$this->load->helper('url');
		$this->load->view('standar7/penelitian_dosen_uploadfile');
	}

	public function importpeldosenproses() {
		$this->load->helper('url');
		$this->load->view('standar7/penelitian_dosen_proses');
	}

	public function penelitian_dosenupdate(){

	$this->load->model('Standar7_model');
		$data = array(
			'idpeldos' => $this->input->post('idpeldos'),
		'program_studi' => $this->input->post('program_studi'),
		'no_ts' => $this->input->post('no_ts'),
		'tipe' => $this->input->post('tipe'),
		'judul' => $this->input->post('judul'),
		'tahun' => $this->input->post('tahun'),
		'nama_dosenketua' => $this->input->post('nama_dosen_ketua'),
		'nama_dosenanggota_1' => $this->input->post('nama_dosenanggota_1'),
		'nama_dosenanggota_2' => $this->input->post('nama_dosenanggota_2'),
		'nama_dosenanggota_3' => $this->input->post('nama_dosenanggota_3'),
		'nama_dosenanggota_4' => $this->input->post('nama_dosenanggota_4'),
		'nama_dosenanggota_5' => $this->input->post('nama_dosenanggota_5'),
		'nama_mahasiswa_1' => $this->input->post('nama_mahasiswa_1'),
		'status' => $this->input->post('status'),
		'tahun' => $this->input->post('tahun'),
		'anggaran' => $this->input->post('anggaran'),
		'keterangan' => $this->input->post('keterangan'),
		'sumber_pembiayaan' => $this->input->post('sumber_pembiayaan'),
		'jenis_penelitian' => $this->input->post('jenis_penelitian'),
		"id_user"=> $this->session->userdata('id_user'),

		);
		$id =  $this->input->post('id');

		$this->Standar7_model->penelitian_dosenupdate($id, $data);

	}

	public function penelitiandosenvalid(){

	$this->load->model('Standar7_model');
		$data = array(
			'sts_valid' => $this->input->post('validasi')
		);
		$id =  $this->input->post('id');

		$this->Standar7_model->penelitian_dosenvalid($id, $data);

	}

	public function abdimas_valid(){

	$this->load->model('Standar7_model');
		$data = array(
			'sts_valid' => $this->input->post('validasi')
		);
		$id =  $this->input->post('id');

		$this->Standar7_model->abdimasvalid($id, $data);

	}


		public function publikasi_valid(){

		$this->load->model('Standar7_model');
			$data = array(
				'sts_valid' => $this->input->post('validasi')
			);
			$id =  $this->input->post('id');

			$this->Standar7_model->publikasivalid($id, $data);

		}

		public function peldosta_valid(){

		$this->load->model('Standar7_model');
			$data = array(
				'sts_valid' => $this->input->post('validasi')
			);
			$id =  $this->input->post('id');

			$this->Standar7_model->peldostavalid($id, $data);

		}

	public function penelitian_dosenupdate1(){

	$this->load->model('Standar7_model');
		$data = array(
		'file_name' => $this->input->post('file_name'),

		"id_user"=> $this->session->userdata('id_user'),

		);
		$id =  $this->input->post('id');

		$this->Standar7_model->penelitian_dosenupdate($id, $data);

	}
//---------------------Penelitian Dosen End----------------------------



//----------------------Penelitian Dosen TA---------------------------
	public function penelitiandosenta() {
		$this->load->helper('url');
		$this->load->model('Users_model');
$data['user']=$this->Users_model->get_userinfo($this->session->userdata('email'));
	$this->load->model('Standar7_model');
	$data['result']=$this->Standar7_model->viewpenelitian_dosenta();
	$data['result1']=$this->Standar7_model->viewpenelitian_ta();

		$this->load->view('standar7/penelitian_ta',$data);
	}


	 public function penelitiandosentauploadpdf($id) {
		 $this->load->model('Standar7_model');

			 unlink(FCPATH.'uploads/dokumen_peldosTA_std7/'.$this->input->post('data-filename'));
				 $fileName = $_FILES['files']['name'];

				 $config['file_name'] = $fileName;
				 date_default_timezone_set('Asia/Jakarta');


		 $config['upload_path']   = './uploads/dokumen_peldosTA_std7/';
					$config['allowed_types'] = 'pdf';
					$config['max_size']      = 60000 ;


				 $this->load->library('upload');
				 $this->upload->initialize($config);



					if ( ! $this->upload->do_upload('files')) {
						 $error = array('error' => $this->upload->display_errors());

			 $this->session->set_flashdata('msg_upload', $this->upload->display_errors());

					}

					else {
				 $this->load->model('Standar7_model');

					 $info_upload = $this->upload->data();
				 $data = array('file_name' => $info_upload['file_name'],  'file_type' => $info_upload['file_type'], 'file_date' =>  date("Y-m-d H:i:s"));
		 $this->Standar7_model->penelitiandosentauploadpdf($id, $data);

					}
				 redirect('/Standar7/penelitiandosenta');

	 }


//----------------------Penelitian Dosen TA End---------------------------


//----------------------Dosen----------------------------

public function dosenautocomplete() {
	$user = $_GET['term'];

	        $query = $this->db->select('nama_dosen')->like('nama_dosen',$user)->get('tb_dosen');

	         if($query->num_rows() > 0){
	            foreach($query->result_array() as $row) {
	                $row_set[] = htmlentities(stripslashes($row['nama_dosen']));
	            }

	            echo json_encode($row_set);
	        }

	}

	public function dosenautocomplete1() {
		$user = $_GET['term'];
								$this->db->where('jenis_penelitian', 'Penelitian TA');
		        $query = $this->db->select('*')->like('idpeldos',$user)->get('tb_penelitian_dosen');

		         if($query->num_rows() > 0){
		            foreach($query->result_array() as $row) {
		                $row['value'] = htmlentities(stripslashes($row['idpeldos']));
										$row['prodi'] = htmlentities(stripslashes($row['program_studi']));
										$row['nots'] = htmlentities(stripslashes($row['no_ts']));
										$row['judul'] = htmlentities(stripslashes($row['judul']));
										$row['tahun'] = htmlentities(stripslashes($row['tahun']));
										$row['ketua'] = htmlentities(stripslashes($row['nama_dosenketua']));
										$row['anggota'] = htmlentities(stripslashes($row['nama_dosenanggota_1']));
										$row_set[] = $row;
		            }

		            echo json_encode($row_set);
		        }
	}


	public function number_ts() {
		$this->load->helper('url');
		$this->load->model('Standar7_model');
		$data['number_ts']=$this->Standar7_model->number_ts();
ob_clean();
		echo json_encode($data);
	}


	public function dosen() {
		$this->load->helper('url');
		$this->load->model('Users_model');
		$data['user']=$this->Users_model->get_userinfo($this->session->userdata('email'));
		$this->load->model('Standar7_model');
		$data['data']=$this->Standar7_model->viewdosen();
		$data['pengupdate']=$this->Standar7_model->pengupdatedosen();
		$this->load->view('standar7/dosen',$data);
	}
	function dosenhapus(){
		$where = array('no' =>  $this->input->post('id'));
		$this->load->model('Standar7_model');
		$this->Standar7_model->dosenahapusmodel($where,'tb_dosen');

	}

	function dosenhapuschecklist(){
		$this->load->model('Standar7_model');
		$this->Standar7_model->dosenhapuschecklistmodel('tb_dosen');
		redirect('Standar7/dosen');
	}
	public function dosenupdate(){

	$this->load->model('Standar7_model');
		$data = array(
		'nama_dosen' => $this->input->post('nama_dosen'),
		'tempat_lahir' => $this->input->post('tempat_lahir'),
		'tanggal_lahir' => $this->input->post('tanggal_lahir'),
		'pendidikan_terakhir' => $this->input->post('pendidikan_terakhir'),
		'jabatan_akademik' => $this->input->post('jabatan_akademik'),
		'keterangan' => $this->input->post('keterangan'),
		'program_studi' => $this->input->post('program_studi'),
		'bidang_keahlian_pdd' => $this->input->post('bidang_keahlian_pdd'),
		'status_dosen' => $this->input->post('status_dosen'),
		"id_user"=> $this->session->userdata('id_user'),


		);
		$id =  $this->input->post('id');

		$this->Standar7_model->dosenupdate($id, $data);

	}
	public function get_nip_dosen() {
		$this->load->helper('url');


}
public function insert_dosen() {

$this->load->model('Standar7_model');


$dataa =$this->Standar7_model->get_by_nip($this->input->post('nip'));
if($dataa > 0){

	$this->session->set_userdata('nip_sama', 'Nip sama');


} else {
	$data = array(
	'nip' => $this->input->post('nip'),
	'nama_dosen' => $this->input->post('nama_dosen'),
	'tempat_lahir' => $this->input->post('tempat_lahir'),
	'tanggal_lahir' => $this->input->post('tanggal_lahir'),
	'pendidikan_terakhir' => $this->input->post('pendidikan_terakhir'),
	'jabatan_akademik' => $this->input->post('jabatan_akademik'),
	'keterangan' => $this->input->post('keterangan'),
	'program_studi' => $this->input->post('program_studi'),
	'bidang_keahlian_pdd' => $this->input->post('bidang_keahlian_pdd'),

	'status_dosen' => $this->input->post('status_dosen'),
	"id_user"=> $this->session->userdata('id_user'),

	);

	$this->Standar7_model->doseninsert($data);

}
//foreach ($dataa as $dr ){
//if($dr->nip ==   ){

//} else {


	//$this->session->set_flashdata('msga', 'Berhasil input data');


//}
redirect('Standar7/dosen');

}


	public function importdosenproses() {
		//$this->load->helper('url');
	///	$this->load->view('standar7/dosen_proses');

	$this->load->library('PHPExcel');
	$this->load->library(array('PHPExcel','PHPExcel/IOFactory'));
			$fileName = $_FILES['files']['name'];

			$config['upload_path'] = './uploads/dokumenstandar7/';
			$config['file_name'] = $fileName;
			$config['allowed_types'] = 'xls|xlsx|csv';
			$config['max_size'] = 40000;

			$this->load->library('upload');
			$this->upload->initialize($config);

			if(! $this->upload->do_upload('files') )
			$this->upload->display_errors();

			$media = $this->upload->data('files');
		 $inputFileName = $this->upload->data('full_path');



		         try {
		                 $inputFileType = IOFactory::identify($inputFileName);
		                 $objReader = IOFactory::createReader($inputFileType);
		                 $objPHPExcel = $objReader->load($inputFileName);
		             } catch(Exception $e) {
		                 die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
		             }

		             $sheet = $objPHPExcel->getSheet(0);
		             $highestRow = $sheet->getHighestRow();
		             $highestColumn = $sheet->getHighestColumn();

		             for ($row = 2; $row <= $highestRow; $row++){                  //  Read a row of data into an array
		                 $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
		                                                 NULL,
		                                                 TRUE,
		                                                 FALSE);

		                 //Sesuaikan sama nama kolom tabel di database
		                  $data = array(
		                     "nip"=> $rowData[0][0],
		                     "nama_dosen"=> $rowData[0][1],
		                     "tempat_lahir"=> $rowData[0][2],
		                     "tanggal_lahir"=> PHPExcel_Style_NumberFormat::toFormattedString($rowData[0][3],'DD-MM-YYYY' ),
		 					"pendidikan_terakhir"=> $rowData[0][4],
		                     "jabatan_akademik"=> $rowData[0][5],
												    "keterangan"=> $rowData[0][6],
														   "program_studi"=> $rowData[0][7],
															    "bidang_keahlian_pdd"=> $rowData[0][8],
   "status_dosen"=> $rowData[0][9],
   "id_user"=> $this->session->userdata('id_user'),
		                 );

		                 //sesuaikan nama dengan nama tabel
		                 $insert = $this->db->insert("tb_dosen",$data);
		                 delete_files($media['file_path']);

		             }
		 			$this->session->set_flashdata('msg', 'Berhasil Mengimport Data');
		         redirect('/Standar7/dosen');

	}


//----------------------Dosen End----------------------------


	public function haki() {
		$this->load->helper('url');
		$this->load->model('Users_model');
		$data['user']=$this->Users_model->get_userinfo($this->session->userdata('email'));
		$this->load->model('Standar7_model');
			$data['result']=$this->Standar7_model->listhaki();
				$data['pengupdate']=$this->Standar7_model->pengupdatehaki();
		$this->load->view('standar7/haki',$data);
	}

	public function hakiuploadpdf($id) {
		$this->load->model('Standar7_model');

			unlink(FCPATH.'uploads/dokumen_haki_std7/'.$this->input->post('data-filename'));

				$fileName = $_FILES['files']['name'];
				date_default_timezone_set('Asia/Jakarta');

				$config['file_name'] = $fileName;


		$config['upload_path']   = './uploads/dokumen_haki_std7/';
				 $config['allowed_types'] = 'pdf';
				 $config['max_size']      = 60000 ;


				$this->load->library('upload');
				$this->upload->initialize($config);



				 if ( ! $this->upload->do_upload('files')) {
						$error = array('error' => $this->upload->display_errors());

			$this->session->set_flashdata('msg_upload', $this->upload->display_errors());

				 }

				 else {
				$this->load->model('Standar7_model');
					$info_upload = $this->upload->data();
				$data = array('file_name' => $info_upload['file_name'],  'file_type' => $info_upload['file_type'], 'file_date' =>  date("Y-m-d H:i:s"));
		$this->Standar7_model->hakidataupdate($id, $data);

				 }
		redirect('/Standar7/haki');

	}



	public function hakiupdate() {

	 $this->load->model('Standar7_model');
	 $data = array(
	 'karya' => $this->input->post('karya'),
	 'jenis_haki' => $this->input->post('jenis'),
	 'tahun' => $this->input->post('tahun'),
	 'id_user' => $this->session->userdata('id_user'),

	 );
 $id =  $this->input->post('id');

	 $this->Standar7_model->hakidataupdate($id, $data);

	 }

	function hakihapus($id){
		$where = array('id' => $id);
		$this->load->model('Standar7_model');

		unlink(FCPATH.'uploads/dokumen_haki_std7/'.$this->input->post('data-filename'));
		$this->Standar7_model->hakihapusmodel($where,'tb_haki');
		$this->session->set_flashdata('msg', 'Berhasil menghapus data');
		redirect('Standar7/haki?success');
	}

	function hakihapussemua(){
		$this->load->model('Standar7_model');

		$ha['listhapus'] = $this->Standar7_model->listhaki();

		foreach($ha['listhapus']  as $as ){
		if(isset($as->file_name)){
		unlink(FCPATH.'uploads/dokumen_haki_std7/'.$as->file_name);

		}


		}

		$this->Standar7_model->hakishapussemuamodel('tb_haki');
		$this->session->set_flashdata('msg', 'Berhasil menghapus semua data');
		redirect('Standar7/haki?success');
	}

	function hakihapuschecklist(){
		$this->load->model('Standar7_model');
		$check = json_decode($this->input->post('result'), true);
	  $jumlah_dipilih = count($check);
	  for($x=0;$x<$jumlah_dipilih;$x++){


	$a = $this->Standar7_model->listhaki_getid($check[$x]);
	unlink(FCPATH.'uploads/dokumen_haki_std7/'.$a->file_name);
	  }
		$this->Standar7_model->hakimashapuschecklistmodel('tb_haki');
		redirect('Standar7/haki?success');
	}

	public function publikasi() {
		$this->load->helper('url');
		$this->load->model('Users_model');
		$data['user']=$this->Users_model->get_userinfo($this->session->userdata('email'));
		$this->load->model('Standar7_model');
		$data['publikasi']=$this->Standar7_model->viewpublikasi();
		$data['pengupdate']=$this->Standar7_model->pengupdatepublikasi();
		$this->load->view('standar7/publikasi',$data);
	}

	public function publikasiuploadpdf($id) {
		$this->load->model('Standar7_model');
			unlink(FCPATH.'uploads/dokumen_publikasi_std7/'.$this->input->post('data-filename'));
				$fileName = $_FILES['files']['name'];

				$config['file_name'] = $fileName;
				date_default_timezone_set('Asia/Jakarta');


		$config['upload_path']   = './uploads/dokumen_publikasi_std7/';
				 $config['allowed_types'] = 'pdf';
				 $config['max_size']      = 60000 ;


				$this->load->library('upload');
				$this->upload->initialize($config);



				 if ( ! $this->upload->do_upload('files')) {
						$error = array('error' => $this->upload->display_errors());

			$this->session->set_flashdata('msg_upload', $this->upload->display_errors());

				 }

				 else {
				$this->load->model('Standar7_model');

					$info_upload = $this->upload->data();
				$data = array('file_name' => $info_upload['file_name'],  'file_type' => $info_upload['file_type'], 'file_date' =>  date("Y-m-d H:i:s"));
		$this->Standar7_model->publikasidataupdate($id, $data);

				 }
		redirect('/Standar7/publikasi');

	}

	public function publikasiuploadloapdf($id) {
		$this->load->model('Standar7_model');
			unlink(FCPATH.'uploads/dokumen_publikasiloa_std7/'.$this->input->post('data-filename_'));
				$fileName = $_FILES['files']['name'];

				$config['file_name'] = $fileName;
				date_default_timezone_set('Asia/Jakarta');


		$config['upload_path']   = './uploads/dokumen_publikasiloa_std7/';
				 $config['allowed_types'] = 'pdf';
				 $config['max_size']      = 60000 ;


				$this->load->library('upload');
				$this->upload->initialize($config);



				 if ( ! $this->upload->do_upload('files')) {
						$error = array('error' => $this->upload->display_errors());

			$this->session->set_flashdata('msg_upload', $this->upload->display_errors());

				 }

				 else {
				$this->load->model('Standar7_model');

					$info_upload = $this->upload->data();
				$data = array('file_names' => $info_upload['file_name'],  'file_types' => $info_upload['file_type'], 'file_dates' =>  date("Y-m-d H:i:s"));
		$this->Standar7_model->publikasidataupdate($id, $data);

				 }
		redirect('/Standar7/publikasi');

	}


	function publikasihapus($id){
		$where = array('id' => $id);
		$this->load->model('Standar7_model');
		unlink(FCPATH.'uploads/dokumen_publikasi_std7/'.$this->input->post('data-filename'));
		unlink(FCPATH.'uploads/dokumen_publikasiloa_std7/'.$this->input->post('data-filename_'));
		$this->Standar7_model->publikasihapusmodel($where,'tb_publikasi');
		$this->session->set_flashdata('msgg', 'Berhasil menghapus data');
		redirect('Standar7/publikasi?success');
	}

	function penelitiandosenhapus($id){
		$where = array('peldos_no' => $id);
		$this->load->model('Standar7_model');
		unlink(FCPATH.'uploads/dokumen_peldos_std7/'.$this->input->post('data-filename'));
		$this->Standar7_model->publikasihapusmodel($where,'tb_penelitian_dosen');
		$this->session->set_flashdata('msgg', 'Berhasil menghapus data');
		redirect('Standar7/penelitiandosen');
	}

	function publikasihapuschecklist(){
		$this->load->model('Standar7_model');

		$check = json_decode($this->input->post('result'), true);
		$jumlah_dipilih = count($check);
		for($x=0;$x<$jumlah_dipilih;$x++){


	$a = $this->Standar7_model->viewpublikasi_getid($check[$x]);
	unlink(FCPATH.'uploads/dokumen_publikasi_std7/'.$a->file_name);
	unlink(FCPATH.'uploads/dokumen_publikasiloa_std7/'.$a->file_names);
		}
		$this->Standar7_model->publikasihapuschecklistmodel('tb_publikasi');
		redirect('Standar7/publikasi?success');
	}

	function publikasihapussemua(){
		$this->load->model('Standar7_model');

		$ha['listhapus'] = $this->Standar7_model->viewpublikasi();

		foreach($ha['listhapus']  as $as){
		if($as->file_name != null ){
		unlink(FCPATH.'uploads/dokumen_publikasi_std7/'.$as->file_name);



		}
	}
		foreach($ha['listhapus']  as $ad){
		if($ad->file_names != null ){
		unlink(FCPATH.'uploads/dokumen_publikasiloa_std7/'.$ad->file_names);



		}

}
		$this->Standar7_model->publikasihapussemuamodel('tb_publikasi');
		$this->session->set_flashdata('msg', 'Berhasil menghapus semua data');
		redirect('Standar7/publikasi?success');
	}


	public function publikasiimport() {

	 $this->load->library('PHPExcel');
	 $this->load->library(array('PHPExcel','PHPExcel/IOFactory'));
			 $fileName = $_FILES['files']['name'];

			 $config['upload_path'] = './uploads/dokumenstandar7/';
			 $config['file_name'] = $fileName;
			 $config['allowed_types'] = 'xls|xlsx|csv';
			 $config['max_size'] = 40000;

			 $this->load->library('upload');
			 $this->upload->initialize($config);

			 if(! $this->upload->do_upload('files') )
			 $this->upload->display_errors();

			 $media = $this->upload->data('files');
			$inputFileName = $this->upload->data('full_path');

			 try {
							 $inputFileType = IOFactory::identify($inputFileName);
							 $objReader = IOFactory::createReader($inputFileType);
							 $objPHPExcel = $objReader->load($inputFileName);
					 } catch(Exception $e) {
							 die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
					 }

					 $sheet = $objPHPExcel->getSheet(0);
					 $highestRow = $sheet->getHighestRow();
					 $highestColumn = $sheet->getHighestColumn();

					 for ($row = 2; $row <= $highestRow; $row++){                  //  Read a row of data into an array
							 $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
																							 NULL,
																							 TRUE,
																							 FALSE);

							 //Sesuaikan sama nama kolom tabel di database
								$data = array(
									"prodi"=> $rowData[0][0],
									 "no_ts"=> $rowData[0][1],
									 "skl_publikasi"=> $rowData[0][2],
									 "judul"=> $rowData[0][3],
									 "jns_publikasi"=> $rowData[0][4],
				 				 "nm_seminar"=> $rowData[0][5],
									 "penulis_utm"=> $rowData[0][6],
									 "penulis_tmbh"=> $rowData[0][7],
									 "penulis_tmbh2"=> $rowData[0][8],
									 "penulis_tmbh3"=> $rowData[0][9],
									 "penulis_tmbh4"=> $rowData[0][10],
									 "penulis_tmbh5"=> $rowData[0][11],
									 "tanggal"=> PHPExcel_Style_NumberFormat::toFormattedString($rowData[0][12],'DD-MM-YYYY' ),
									 "ket"=> $rowData[0][13],
									 "jlm_dosen_si"=> $rowData[0][14],
 'id_user' => $this->session->userdata('id_user'),


							 );

							 //sesuaikan nama dengan nama tabel
							 $insert = $this->db->insert("tb_publikasi",$data);
							 delete_files($media['file_path']);

					 }
		 $this->session->set_flashdata('msg', 'Berhasil Mengimport Data');
			 redirect('/Standar7/publikasi');
	 }

	/*public function hakinilai() {
	$this->load->model('Standar7_model');
		$data = array(
		'nilai' => $this->input->post('nilai1'),



		);

		$data1 = array(


			'id_standar7' => $this->input->post('id1'),
		'nama_penilai' => $this->session->userdata('nama'),
'bobot' => $this->input->post('bobot'),
'nilai' => $this->input->post('nilai1'),
'date' => date(dmY),

		);
		$id =  $this->input->post('id1');

		$this->Standar7_model->hakiupdatenilai($id, $data);

			$this->Standar7_model->record_penilai($data1);


} */

	/*public function haki_insert() {

 $this->load->model('Standar7_model');
 $data = array(
'karya' => $this->input->post('karya'),
'tahun' => $this->input->post('tahun'),
'id_user' => $this->session->userdata('id_user'),


);

$this->Standar7_model->hakiinsert($data);
$this->session->set_flashdata('msga', 'Berhasil input data');
redirect('Standar7/haki');
}*/
	public function importhakiproses() {
		//$this->load->helper('url');
	///	$this->load->view('standar7/dosen_proses');

	$this->load->library('PHPExcel');
	$this->load->library(array('PHPExcel','PHPExcel/IOFactory'));
			$fileName = $_FILES['files']['name'];

			$config['upload_path'] = './uploads/dokumenstandar7/';
			$config['file_name'] = $fileName;
			$config['allowed_types'] = 'xls|xlsx|csv';
			$config['max_size'] = 40000;

			$this->load->library('upload');
			$this->upload->initialize($config);

			if(! $this->upload->do_upload('files') )
			$this->upload->display_errors();
					$media = $this->upload->data('files');
		 $inputFileName = $this->upload->data('full_path');



		         try {
		                 $inputFileType = IOFactory::identify($inputFileName);
		                 $objReader = IOFactory::createReader($inputFileType);
		                 $objPHPExcel = $objReader->load($inputFileName);
		             } catch(Exception $e) {
		                 die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
		             }

		             $sheet = $objPHPExcel->getSheet(0);
		             $highestRow = $sheet->getHighestRow();
		             $highestColumn = $sheet->getHighestColumn();

		             for ($row = 2; $row <= $highestRow; $row++){                  //  Read a row of data into an array
		                 $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
		                                                 NULL,
		                                                 TRUE,
		                                                 FALSE);


																										 $info_upload = $this->upload->data();


		                 //Sesuaikan sama nama kolom tabel di database
		                  $data = array(

		                     "Karya"=> $rowData[0][0],
												 "jenis_haki"=> $rowData[0][1],
												 "tahun"=> $rowData[0][2],
																						 "id_user"=>  $this->session->userdata('id_user'),


		                 );

		                 //sesuaikan nama dengan nama tabel
		                 $insert = $this->db->insert("tb_haki",$data);

		             }
		 			$this->session->set_flashdata('msg', 'Berhasil Mengimport Data');
		         redirect('/Standar7/haki');

	}



//--------------------- Penelitian_End -----------------------------







//------------------Pengabdian Masyarakat-----------------------

public function abdimasuploadpdf($id) {
	$this->load->model('Standar7_model');
		unlink(FCPATH.'uploads/dokumen_abdimas_std7/'.$this->input->post('data-filename'));
			$fileName = $_FILES['files']['name'];

			$config['file_name'] = $fileName;
			date_default_timezone_set('Asia/Jakarta');


	$config['upload_path']   = './uploads/dokumen_abdimas_std7/';
			 $config['allowed_types'] = 'pdf';
			 $config['max_size']      = 60000 ;


			$this->load->library('upload');
			$this->upload->initialize($config);



			 if ( ! $this->upload->do_upload('files')) {
					$error = array('error' => $this->upload->display_errors());

		$this->session->set_flashdata('msg_upload', $this->upload->display_errors());

			 }

			 else {
			$this->load->model('Standar7_model');

				$info_upload = $this->upload->data();
			$data = array('file_name' => $info_upload['file_name'],  'file_type' => $info_upload['file_type'], 'file_date' =>  date("Y-m-d H:i:s"));
	$this->Standar7_model->abdimasdataupdate($id, $data);

			 }
	redirect('/Standar7/abdimasdata');

}


     public function abdimas() {
		$this->load->helper('url');
		$this->load->model('Users_model');
	$data['user']=$this->Users_model->get_userinfo($this->session->userdata('email'));
			$this->load->model('Standar7_model');
	$data['biaya_luar_negeri_abdimas']=$this->Standar7_model->get_biaya_luar_negeri_abdimas();
	$data['biaya_sendiri_abdimas']=$this->Standar7_model->get_biaya_sendiri_abdimas();
	$data['pt_bersangkutan_abdimas']=$this->Standar7_model->get_pt_bersangkutan_abdimas();
	$data['depdiknas_abdimas']=$this->Standar7_model->get_depdiknas_abdimas();
	$data['luar_depdiknas_abdimas']=$this->Standar7_model->get_luar_depdiknas_abdimas();
	$data['jml_dosen']=$this->Standar7_model->jmldosen();
	$data['nm_mahasiswa']=$this->Standar7_model->viewabdimas();
	$data['nm_mahasiswa']=$this->Standar7_model->viewabdimas();



	$data['biaya_luar_negeri_abdimas_hitung_ts2']=$this->Standar7_model->get_biaya_luar_negeri_abdimas_hitung_ts2();
	$data['biaya_luar_negeri_abdimas_hitung_ts1']=$this->Standar7_model->get_biaya_luar_negeri_abdimas_hitung_ts1();
	$data['biaya_luar_negeri_abdimas_hitung_ts']=$this->Standar7_model->get_biaya_luar_negeri_abdimas_hitung_ts();
	$data['biaya_sendiri_abdimas_ts2']=$this->Standar7_model->get_biaya_sendiri_abdimas_ts2();
	$data['biaya_sendiri_abdimas_ts1']=$this->Standar7_model->get_biaya_sendiri_abdimas_ts1();
	$data['biaya_sendiri_abdimas_ts']=$this->Standar7_model->get_biaya_sendiri_abdimas_ts();
	$data['pt_bersangkutan_abdimas_ts']=$this->Standar7_model->get_pt_bersangkutan_abdimas_ts();
		$data['pt_bersangkutan_abdimas_ts1']=$this->Standar7_model->get_pt_bersangkutan_abdimas_ts1();


		$data['pt_bersangkutan_abdimas_ts2']=$this->Standar7_model->get_pt_bersangkutan_abdimas_ts2();
		$data['depdiknas_abdimas_ts']=$this->Standar7_model->get_depdiknas_abdimas_ts();
		$data['depdiknas_abdimas_ts1']=$this->Standar7_model->get_depdiknas_abdimas_ts1();
		$data['depdiknas_abdimas_ts2']=$this->Standar7_model->get_depdiknas_abdimas_ts2();
		$data['luar_depdiknas_abdimas_ts2']=$this->Standar7_model->get_luar_depdiknas_abdimas_ts2();
		$data['luar_depdiknas_abdimas_ts1']=$this->Standar7_model->get_luar_depdiknas_abdimas_ts1();
		$data['luar_depdiknas_abdimas_ts']=$this->Standar7_model->get_luar_depdiknas_abdimas_ts();
		$data['view722']=$this->Standar7_model->view722();




		$this->load->view('standar7/abdimas', $data);
	}

	public function abdimasupdate() {

	 $this->load->model('Standar7_model');
	 $data = array(
	 'type' => $this->input->post('abdimas_typel'),
	 '	judul' => $this->input->post('abdimas_judull'),
	 'lokasi' => $this->input->post('abdimas_lokasi'),
		 'mitra' => $this->input->post('abdimas_mitra'),
	 'tahun' => $this->input->post('abdimas_tahun'),
	 'nama_dosen_ketua' => $this->input->post('abdimas_nama_dosen_ketua'),
	 'nama_dosen_anggota' => $this->input->post('abdimas_nama_dosen_anggota'),
	 'nama_anggotadosen2' => $this->input->post('abdimas_nama_dosen_anggota2'),
	 'nama_anggotadosen3' => $this->input->post('abdimas_nama_dosen_anggota3'),
	 'nama_anggotadosen4' => $this->input->post('abdimas_nama_dosen_anggota4'),
	 'nama_anggotadosen5' => $this->input->post('abdimas_nama_dosen_anggota5'),
'nama_mahasiswa' => $this->input->post('abdimas_nama_mahasiswa'),
'anggaran' => $this->input->post('abdimas_anggaran'),
'bukti_laporan_akhir' => $this->input->post('abdimas_bukti_laporan_akhir'),
'keterangan' => $this->input->post('abdimas_keterangan'),
'id_user' => $this->session->userdata('id_user'),

	 );
 $id =  $this->input->post('no');

	 $this->Standar7_model->abdimasdataupdate($id, $data);



	 }




	function abdimashapussemua(){
		$this->load->model('Standar7_model');

		$ha['listhapus'] = $this->Standar7_model->viewabdimas();

		foreach($ha['listhapus']  as $as ){
		if(isset($as->file_name)){
		unlink(FCPATH.'uploads/dokumen_abdimas_std7/'.$as->file_name);

		}


		}

		$this->Standar7_model->abdimashapussemuamodel('tb_abdimas');
		$this->session->set_flashdata('msg', 'Berhasil menghapus semua data');
		redirect('Standar7/abdimasdata?success');
	}


	function abdimashapus($id){
		$where = array('no' => $id);
		$this->load->model('Standar7_model');
		$this->Standar7_model->abdimashapusmodel($where,'tb_abdimas');

		unlink(FCPATH.'uploads/dokumen_abdimas_std7/'.$this->input->post('data-filename'));

		$this->session->set_flashdata('msg', 'Berhasil menghapus data');
		redirect('Standar7/abdimasdata?success');
	}

	function abdimashapuschecklist(){
		$this->load->model('Standar7_model');

		$check = json_decode($this->input->post('result'), true);
		$jumlah_dipilih = count($check);
		for($x=0;$x<$jumlah_dipilih;$x++){


	$a = $this->Standar7_model->abdimas_getid($check[$x]);
	unlink(FCPATH.'uploads/dokumen_abdimas_std7/'.$a->file_name);
		}

		$this->Standar7_model->abdimashapuschecklistmodel('tb_abdimas');
		redirect('Standar7/abdimasdata?success');
	}

	public function abdimasimport() {

	 $this->load->library('PHPExcel');
	 $this->load->library(array('PHPExcel','PHPExcel/IOFactory'));
			 $fileName = $_FILES['files']['name'];

			 $config['upload_path'] = './uploads/dokumenstandar7/';
			 $config['file_name'] = $fileName;
			 $config['allowed_types'] = 'xls|xlsx|csv';
			 $config['max_size'] = 40000;

			 $this->load->library('upload');
			 $this->upload->initialize($config);

			 if(! $this->upload->do_upload('files') )
			 $this->upload->display_errors();

			 $media = $this->upload->data('files');
			$inputFileName = $this->upload->data('full_path');

			 try {
							 $inputFileType = IOFactory::identify($inputFileName);
							 $objReader = IOFactory::createReader($inputFileType);
							 $objPHPExcel = $objReader->load($inputFileName);
					 } catch(Exception $e) {
							 die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
					 }

					 $sheet = $objPHPExcel->getSheet(0);
					 $highestRow = $sheet->getHighestRow();
					 $highestColumn = $sheet->getHighestColumn();

					 for ($row = 2; $row <= $highestRow; $row++){                  //  Read a row of data into an array
							 $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
																							 NULL,
																							 TRUE,
																							 FALSE);

							 //Sesuaikan sama nama kolom tabel di database


	$data = array(
		 "program_studi"=> $rowData[0][0],
		 "no_ts"=> $rowData[0][1],
		 "type"=> $rowData[0][2],
		 "judul"=> $rowData[0][3],
"	lokasi"=> $rowData[0][4],
		 "	mitra"=> $rowData[0][5],
		 "tahun"=> PHPExcel_Style_NumberFormat::toFormattedString($rowData[0][6],'DD-MM-YYYY' ),
		 "nama_dosen_ketua"=> $rowData[0][7],
		 "nama_dosen_anggota"=> $rowData[0][8],
		 "nama_anggotadosen2"=> $rowData[0][9],
		 "nama_anggotadosen3"=> $rowData[0][10],
		 "nama_anggotadosen4"=> $rowData[0][11],
		 "nama_anggotadosen5"=> $rowData[0][12],
		 "nama_mahasiswa"=> $rowData[0][13],
		"anggaran"=> $rowData[0][14],
		 "bukti_laporan_akhir"=> $rowData[0][15],
		 "keterangan"=> $rowData[0][16],
		 "sumber_pembiayaan"=> $rowData[0][17],
		 	 "id_user"=> $this->session->userdata('id_user'),

 );

 $insert = $this->db->insert("tb_abdimas",$data);
	delete_files($media['file_path']);
	 $this->session->set_flashdata('msg', 'Berhasil Mengimport Data');

							 //sesuaikan nama dengan nama tabel

					 }

			 redirect('/Standar7/abdimasdata');
	 }
	public function abdimasdata() {
		$this->load->helper('url');

		$this->load->model('Standar7_model');
		$data['result']=$this->Standar7_model->viewabdimas();
		$data['view722']=$this->Standar7_model->view722();
$data['pengupdate']=$this->Standar7_model->pengupdateabdimas();
		$this->load->model('Users_model');
		$data['user']=$this->Users_model->get_userinfo($this->session->userdata('email'));
		$this->load->view('standar7/abdimas_data', $data);
	}

	public function tb_7_2_2_abdimas_insert() {


		$this->load->model('Standar7_model');
		$data = array(
			'id_user' => $this->session->userdata('id_user'),
			 "text"=> $this->input->post('data'),


	 );
		$this->Standar7_model->tb_7_2_2_abdimas($data);


redirect('standar7/abdimasdata');
	}
//------------------Pengabdian Masyarakat End-----------------------










//------------------------------------------------------------------------//
	public function uploadfile() {
		$this->load->helper('url');
		//$this->load->model('Users_model');
		//$data['user']=$this->Users_model->get_userinfo($this->session->userdata('email'));
		$this->load->view('standar7/uploadfile', $data);
	}

	public function uploading() {
		$this->load->helper('url');
		$this->load->view('standar7/upload-file-proses');
	}

	  public function ajax_list()
    {
        $list = $this->standar7->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $filedetail) {
            $no++;
            $row = array();
            $row[] = $filedetail->file_name;
            $row[] = $filedetail->kebutuhan;
            $row[] = $filedetail->deskripsi;
            $row[] = $filedetail->date;
            $row[] = $filedetail->size;

            $data[] = $row;
        }

        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->standar7->count_all(),
                        "recordsFiltered" => $this->standar7->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
   }

//------------------------------------------------------ Penilaian ------------------------------------//


function penilaian() {
	$this->load->helper('url');
	$this->load->model('Standar7_model');
$data['nilai']=$this->Standar7_model->viewpenilaian();
$data['nilai_count']=$this->Standar7_model->sum_nilai();
$data['nilai_count']=$this->Standar7_model->sum_nilai();

$data['nilaihrkt_count']=$this->Standar7_model->sum_nilaihrkt();

$data['record_penilai_view']=$this->Standar7_model->record_penilaian_view();
$data['results']= $this->Standar7_model->viewversionpdf();

$this->load->model('Users_model');

	$data['user']=$this->Users_model->get_userinfo($this->session->userdata('email'));
	$this->load->view('standar7/penilaianpage', $data);

}



//-------------------------------TA MAHASISWA----------------------------------------------------------//

function penelitiantahapus($id){
	$where = array('no' => $id);
	$this->load->model('Standar7_model');
	unlink(FCPATH.'uploads/dokumen_peldosTA_std7/'.$this->input->post('data-filename'));

	$this->Standar7_model->penelitandosentahapusmodel($where,'tb_tamahasiswa');
	$this->session->set_flashdata('msg', 'Berhasil menghapus data');
	redirect('Standar7/penelitiandosenta');
}

function penelitiantahapuschecklist(){
$this->load->model('Standar7_model');

$check = json_decode($this->input->post('result'), true);
$jumlah_dipilih = count($check);
for($x=0;$x<$jumlah_dipilih;$x++){



	$a = $this->Standar7_model->penelitiandosentaviewmodel_getid($check[$x]);
	//unlink(FCPATH.'uploads/dokumen_peldosTA_std7/'.$a->file_name);
}

	$this->Standar7_model->penelitiandosentahapuschecklistmodel('tb_tamahasiswa');
	redirect('Standar7/penelitiandosenta');
}


function peltahapussemua(){
	$this->load->model('Standar7_model');
	$ha['listhapus'] = $this->Standar7_model->viewpenelitian_ta();

	foreach($ha['listhapus']  as $as ){
	if(isset($as->file_name)){
	//unlink(FCPATH.'uploads/dokumen_peldosTA_std7/'.$as->file_name);

	}
}
	$this->Standar7_model->peldosentahapussemuamodel('tb_tamahasiswa');
	$this->session->set_flashdata('msg', 'Berhasil menghapus semua data');
	redirect('Standar7/penelitiandosenta');
}

public function importpenelitiansita(){

	$this->load->library('PHPExcel');
	$this->load->library(array('PHPExcel','PHPExcel/IOFactory'));
			$fileName = $_FILES['files1']['name'];

			$config['upload_path'] = './uploads/dokumenstandar7/';
			$config['file_name'] = $fileName;
			$config['allowed_types'] = 'xls|xlsx|csv';
			$config['max_size'] = 40000;

			$this->load->library('upload');
			$this->upload->initialize($config);

			if(! $this->upload->do_upload('files1') )
			$this->upload->display_errors();

			$media = $this->upload->data('files1');
		 $inputFileName = $this->upload->data('full_path');



						 try {
										 $inputFileType = IOFactory::identify($inputFileName);
										 $objReader = IOFactory::createReader($inputFileType);
										 $objPHPExcel = $objReader->load($inputFileName);
								 } catch(Exception $e) {
										 die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
								 }

								 $sheet = $objPHPExcel->getSheet(0);
								 $highestRow = $sheet->getHighestRow();
								 $highestColumn = $sheet->getHighestColumn();

								 for ($row = 2; $row <= $highestRow; $row++){                  //  Read a row of data into an array
										 $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
																										 NULL,
																										 TRUE,
																										 FALSE);

						 //Sesuaikan sama nama kolom tabel di database
							$data = array(
								 "no_ts"=> $rowData[0][0],
								 "tahun_ajrn"=> $rowData[0][1],
								 "nim"=> $rowData[0][2],
								 "nama_mahasiswa"=> $rowData[0][3],
								 "kelas"=> $rowData[0][4],
									"status"=> $rowData[0][5],
								 "input_date"=> PHPExcel_Style_NumberFormat::toFormattedString($rowData[0][6],'DD-MM-YYYY' ),
								 "pembimbing_akdmk"=> $rowData[0][7],
								 "kode_pngjr"=> $rowData[0][8],



"id_user"=> $this->session->userdata('id_user'),

						 );

						 //sesuaikan nama dengan nama tabel
						 $insert = $this->db->insert("tb_tamahasiswa",$data);
						 delete_files($media['file_path']);

				 }
	 $this->session->set_flashdata('msg', 'Berhasil Mengimport Data');
		 redirect('/Standar7/penelitiandosenta');

}


public function penelitiantainsert() {
$this->load->model('Standar7_model');
	$data = array(
	'no_ts' => $this->input->post('no_ts'),
	'tahun_ajrn' => $this->input->post('tahun_ajrn'),

	'nim' => $this->input->post('nim'),

	'nama_mahasiswa' => $this->input->post('nama_mahasiswa'),
	'kelas' => $this->input->post('kelas'),

	'status' => $this->input->post('status'),
	'input_date' => $this->input->post('input_date'),
	'pembimbing_akdmk' => $this->input->post('pembimbing_akdmk'),
	'kode_pngjr' => $this->input->post('kode_pngjr'),
	"id_user"=> $this->session->userdata('id_user'),
	);



  $insert = $this->db->insert("tb_tamahasiswa",$data);
	 $this->session->set_flashdata('msgta', 'Berhasil menginsert data');
redirect('Standar7/penelitiandosenta');


}


public function penelitiantasiupdate() {

 $this->load->model('Standar7_model');
 $data = array(
	 'no_ts' => $this->input->post('no_ts'),
	 'tahun_ajrn' => $this->input->post('tahun_ajrn'),

	 'nim' => $this->input->post('nim'),

	 'nama_mahasiswa' => $this->input->post('nama_mahasiswa'),
	 'kelas' => $this->input->post('kelas'),

	 'status' => $this->input->post('status'),
	 'input_date' => $this->input->post('input_date'),
	 'pembimbing_akdmk' => $this->input->post('pembimbing_akdmk'),
	 'kode_pngjr' => $this->input->post('kode_pngjr'),

	 "id_user"=> $this->session->userdata('id_user'),


 );
$id =  $this->input->post('id');

 $this->Standar7_model->penelitian_taupdate($id, $data);



 }

 public function peldos_details($id){
         $this->load->model("Standar7_model");

		$id1 = urldecode($id);
		$data["result"] = $this->Standar7_model->getPeldosDetails($id1);

		$data["result1"] = $this->Standar7_model->getPeldostaDetails($id1);

				$this->load->view('Standar7/detailpeldos', $data);

}



}
