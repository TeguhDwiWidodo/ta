<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Admin extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('users_model','user');
	}

	public function kelolapengguna() {
		if($this->session->hak_akses == 'admin'){
			$users = $this->user->get_all_users();
			$data['all_users'] = $users;
			$this->load->view('admin/kelolapengguna', $data);
		} else {
			redirect('/dashboard');
		}


    }

	public function kelolapengguna_proses() {
		// Get job (and id)
		$job = '';
		$id  = '';
		if ($this->input->get('job')){
			$job = $this->input->get('job');
			if ($job == 'get_user'   ||
				$job == 'add_user'   ||
				$job == 'edit_user'  ||
				$job == 'delete_user'){
				if ($this->input->get('id')){
					$id = $this->input->get('id');
					if (!is_numeric($id)){
						$id = '';
					}
				}
			} else {
				$job = '';
			}
		} else {
			redirect("admin/kelolapengguna/");
		}

		// Prepare array
		$mysql_data = array();

		// Valid job found
		if ($job != ''){

		  // Connect to database
		  /*$db_connection = mysqli_connect($db_server, $db_username, $db_password, $db_name);
		  if (mysqli_connect_errno()){
			$result  = 'error';
			$message = 'Failed to connect to database: ' . mysqli_connect_error();
			$job     = '';
		  }*/

		  // Execute job

		  if ($job == 'get_users'){

			// Get Users
			/*
			$query = "SELECT * FROM it_companies ORDER BY rank";
			$query = mysqli_query($db_connection, $query);
			*/
			$query = $this->user->get_all_users();
			if (!$query){
			  $result  = 'error';
			  $message = 'query error';
			} else {
			  $result  = 'success';
			  $message = 'query success';
			  foreach ($query as $user){
				$functions  = '<div class="function_buttons"><ul>';
				$functions .= '<li class="function_edit"><a data-id="'   . $user['id_user'] . '" data-name="' . $user['nama'] . '"><span>Edit</span></a></li>';
				$functions .= '<li class="function_delete"><a data-id="' . $user['id_user'] . '" data-name="' . $user['nama'] . '"><span>Delete</span></a></li>';
				$functions .= '</ul></div>';
				$mysql_data[] = array(
				  "email"        	=> $user['email'],
				  "nama"  			=> $user['nama'],
				  "nohp"    		=> $user['hp'],
				  "alamat"       	=> $user['alamat'],
				  "jenis_kelamin"   => $user['jenis_kelamin'],
				  "hak_akses"     	=> $user['hak_akses'],
				  "functions"     	=> $functions
				);
			  }
			}

		  } else if ($job == 'get_user'){

			// Get User
			if ($id == ''){
			  $result  = 'error';
			  $message = 'id missing';
			  redirect("admin/kelolapengguna/");
			} else {

			  /* $query = "SELECT * FROM it_companies WHERE company_id = '" . mysqli_real_escape_string($db_connection, $id) . "'";
			  $query = mysqli_query($db_connection, $query); */
			  $userid = $id;
			  $query = $this->user->get_one_user($userid);
			  if (!$query){
				$result  = 'error';
				$message = 'query error';
			  } else {
				$result  = 'success';
				$message = 'query success';
				foreach ($query as $user){
				  $mysql_data[] = array(
						"email"        		=> $user['email'],
						"nama"  			=> $user['nama'],
						"nohp"    			=> $user['hp'],
						"alamat"       		=> $user['alamat'],
						"jenis_kelamin"  	=> $user['jenis_kelamin'],
						"hak_akses"     	=> $user['hak_akses']
				  );
				}
			  }
			}

		  } elseif ($job == 'add_user'){

			// Add User
			/*
			$query = "INSERT INTO it_companies SET ";
			*/
			$email 			= $this->input->get('email');
			$password 		= md5($this->input->get('password'));
			$nama 			= $this->input->get('nama_lengkap');
			$nohp 			= $this->input->get('nohp');
			$jenis_kelamin 	= $this->input->get('jenis_kelamin');
			$hak_akses 		= $this->input->get('hak_akses');

			$query = $this->user->addUser($email, $password, $nama, $nohp, $hak_akses, $jenis_kelamin);
			if (!$query){
			  $result  = 'error';
			  $message = 'query error';
			  redirect("admin/kelolapengguna/");
			} else {
			  $result  = 'success';
			  $message = 'query success';
			}

		  } elseif ($job == 'edit_user'){

			// Edit company
			if ($id == ''){
			  $result  = 'error';
			  $message = 'id missing';
			  redirect("admin/kelolapengguna/");
			} else {
				$session_iduser = $id;
				$useremail 		= $this->input->get('email');
				$usernama 		= $this->input->get('nama_lengkap');
				$userjenisk		= $this->input->get('jenis_kelamin');;
				$useralamat		= $this->input->get('alamat');
				$usernohp 		= $this->input->get('nohp');
				$hak_akses 		= $this->input->get('hak_akses');
				$query = $this->user->edit_profileinfo($session_iduser, $useremail, $usernama, $userjenisk, $useralamat, $usernohp, $hak_akses);
				if (!$query){
					$result  = 'error';
					$message = 'query error';
				} else {
					$result  = 'success';
					$message = 'query success';
				}
			}

		  } elseif ($job == 'delete_user'){

			// Delete User
			if ($id == ''){
			  $result  = 'error';
			  $message = 'id missing';
			  redirect("admin/kelolapengguna/");
			} else {
				$userid = $id;
				$query = $this->user->delete_user($userid);
				if (!$query){
					$result  = 'error';
					$message = 'query error';
				} else {
					$result  = 'success';
					$message = 'query success';
				}
			}

		  } else {
			  redirect("admin/kelolapengguna/");
		  }

		} else {
			  redirect("admin/kelolapengguna/");
		  }

		// Prepare data
		$data = array(
		  "result"  => $result,
		  "message" => $message,
		  "data"    => $mysql_data
		);

		// Convert PHP array to JSON array
		$json_data = json_encode($data);
		print $json_data;

    }

		public function tambahtsview(){
			$this->load->helper('url');
			$this->load->model('Standar7_model');
			$data['result']=$this->Standar7_model->number_ts();

			$this->load->view('admin/halamants',$data);
		}

		public function tambahtsinsert(){
			$this->load->model('Standar7_model');
	 	 $data = array(

	 'date_from' => $this->input->post('tahunaw'),

	 );

	 $this->Standar7_model->number_tsinsert($data);
	 redirect('/admin/tambahtsview');
		}

		public function numbertsupdate() {

		 $this->load->model('Standar7_model');
		 $data = array(

			 'date_from' => $this->input->post('dateaw'),


		 );
	 $id =  $this->input->post('id');

		 $this->Standar7_model->number_tsupdate($id, $data);



		 }

		 function numbertshapus($id){
			 $where = array('no' => $id);

			 $this->load->model('Standar7_model');
			 $this->Standar7_model->number_tshapus($where,'numberts');
			 redirect('/admin/tambahtsview');
		 }

}

?>
