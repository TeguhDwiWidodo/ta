<?php
if(!isset($_SESSION['nama'])){
      header("location:" . base_url());
      exit();
   }
?>

<!DOCTYPE html>
<html>
	<head>
		<title>Akreditasi | List Tim Akreditasi</title>
		<script type = 'text/javascript' src="<?php echo base_url(); ?>js/jquery-1.12.4.js"></script>
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/mainlayout.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/timakreditasi.css">
		<script type = 'text/javascript' src="<?php echo base_url(); ?>js/jquery.dataTables.js"></script>
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/jquery.dataTables.css">
		<script type = 'text/javascript' >
		(function ($){
			$(document).ready(function() {
				$('#table-view-data-tim-akreditasi').dataTable({
					"order": [[ 3, 'asc' ]],

				});
			});
		})(jQuery);
		</script>
	</head>

	<body>
		<div class="header">
			<h1><a href="<?php echo base_url(); ?>dashboard">Akreditasi SI</a></h1>
			<?php
				include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/header.php";
			?>
		</div>

		<div class="sidebar">
			<?php
				include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/sidebar.php";
			?>
		</div>

		<div class="main-layout">
			<div class="sub-header">
			<h2>Tim Pengumpul Data Akreditasi</h2>
			</div>
			<div class="main-content">
				<div class="sub-header">
				<h2 >List Data Diri Tim Akreditasi</h2>
				</div>
				<div class="tim-akreditasi" >
					<h3>Tim Anggota Akreditasi</h3>
					<table id="table-view-data-tim-akreditasi" class="display" width="100%" cellspacing="0">

						<thead class="table head">
							<th >Email</th>
							<th >Nama</th>
							<th >No Handphone</th>
							<th >Hak Akses</th>
							<th >Tugas</th>
							<?php
							if ($_SESSION['hak_akses'] == 'kaprodi' || $_SESSION['hak_akses'] == 'admin'){
								?>
								<th >Edit Tugas</th>
								<?php
							}
							?>
						</thead>


						<tbody class="table body">
							<?php

									foreach($dataTim as $row){
									?>
										<tr id="row:<?php echo $row['id_user'] ?>">
											<?php
											if($row['id_user'] == $_SESSION['id_user']){
											?>
												<td ><a href="<?php echo site_url();?>profile/"><?php echo $row['email'] ?></a></td>
											<?php
											} else {
											?>
												<td ><a href="<?php echo site_url();?>profile/user/<?php echo $row['id_user'] ?>"><?php echo $row['email'] ?></a></td>
											<?php
											}
											?>
											<td><?php echo $row['nama'] ?></td>
											<td><?php echo $row['hp'] ?></td>
											<td><?php echo $row['hak_akses'] ?></td>
											<td><textarea style="font-size:14px;border: none;" id="tugas:<?php echo $row['id_user'] ?>" readonly type="textarea"><?php echo $row['tugas'] ?></textarea></td>
											<?php
											if ($_SESSION['hak_akses'] == 'kaprodi' || $_SESSION['hak_akses'] == 'admin'){
											?>
												<td>
												<button style="display: block;" type="button" onclick="edit_row('<?php echo $row['id_user'] ?>')" id="edit_btn<?php echo $row['id_user'] ?>" class="edit" value="edit"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
												<button style="display: none;" type="button" onclick="save_row('<?php echo $row['id_user'] ?>')" id="save_btn<?php echo $row['id_user'] ?>" class="save" value="save"><i class="fa fa-floppy-o" aria-hidden="true"></i></button>
												</td>
											<?php
											}
											?>

										</tr>
								<?php
									}
								?>



						</tbody>
						<tfoot class="table head">
							<th >Email</th>
							<th >Nama</th>
							<th >No Handphone</th>
							<th >Hak Akses</th>
							<th >Tugas</th>
							<?php
							if ($_SESSION['hak_akses'] == 'kaprodi' || $_SESSION['hak_akses'] == 'admin'){
								?>
								<th >Edit Tugas</th>
								<?php
							}
							?>
						</tfoot>


					</table>
				</div>


			</div>
		</div>

		<div class="footer">
			<?php
			include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/footer.php";
			?>
		</div>
		<script>
		function edit_row(no)
		{
			 document.getElementById("edit_btn"+no).style.display="none";
			 document.getElementById("save_btn"+no).style.display="block";

			 var tugas = document.getElementById("tugas:"+no);

			 var tugas_data=tugas.innerHTML;
			 document.getElementById("tugas:"+no).removeAttribute('readonly');
			 document.getElementById("tugas:"+no).style.outline ="thin solid #bfbfbf";
			 document.getElementById("tugas:"+no).style.background ="#f2f2f2";
		}
		</script>
		<script>
		function save_row(no)
		{
			 var tugas_val = document.getElementById("tugas:"+no).value;
			 var row_no = document.getElementById("row:"+no).value;

			document.getElementById("tugas:"+no).innerHTML = tugas_val;


			$.ajax
			 ({
			  type:'post',
			  url:'<?php echo base_url(); ?>timakreditasi/edittugas',
			  data:{
			   edit_row:'edit_row',
			   row_no:no,
			   tugas_val:tugas_val
			  },
			  success:function(response) {
			   if(response == "success")
			   {
				document.getElementById("tugas:"+no).innerHTML=tugas_val;
				document.getElementById("edit_btn"+no).style.display="block";
				document.getElementById("save_btn"+no).style.display="none";
				document.getElementById("tugas:"+no).readOnly =true;
				document.getElementById("tugas:"+no).style.outline ="none";
				document.getElementById("tugas:"+no).style.background ="white";
			   }
			  }
			 });
		}
		</script>

	</body>



</html>
