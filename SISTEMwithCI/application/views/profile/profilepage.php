<?php
if(isset($dbuser)){
	$profilpic_path = $dbuser['profilpic_path'];
	$nama_user = $dbuser['nama'];
	$email_user = $dbuser['email'];
	$jenis_kel = $dbuser['jenis_kelamin'];
	$alamat_user = $dbuser['alamat'];
	$hp_user = $dbuser['hp'];
	$tugas_user = $dbuser['tugas'];
	$hak_akses_user = $dbuser['hak_akses'];
	$userpw = $dbuser['password'];
	$userid = $dbuser['id_user'];
} else if(isset($db)) {
	$profilpic_path = $db['profilpic_path'];
	$nama_user = $db['nama'];
	$email_user = $db['email'];
	$jenis_kel = $db['jenis_kelamin'];
	$alamat_user = $db['alamat'];
	$hp_user = $db['hp'];
	$tugas_user = $db['tugas'];
	$hak_akses_user = $db['hak_akses'];
	$userpw = $db['password'];
	$userid = $db['id_user'];
}

if(!isset($_SESSION['nama'])){
      header("location:" . base_url());
      exit();
   }	
?>

<!DOCTYPE html>
<html>
	<head>
		<title>Akreditasi | Profile</title>
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/mainlayout.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/profile.css">
		<script type = 'text/javascript' src="<?php echo base_url(); ?>js/jquery-1.12.4.js"></script>
		<script type = 'text/javascript'>
			function checkPass(){
				//Store the password field objects into variables ...
				var pass1 = document.getElementById('password-sekarang');
				var pass2 = document.getElementById('password-konfirmasi');
				var ubahPwBtn = document.getElementById('ubah-password-button');
				//Store the Confimation Message Object ...
				var message = document.getElementById('confirmMessage');				
				//Set the colors we will be using ...
				var goodColor = "#66cc66";
				var badColor = "#ff6666";
				//Compare the values in the password field 
				//and the confirmation field
				if(pass1.value == pass2.value){
					//The passwords match. 
					//Set the color to the good color and inform
					//the user that they have entered the correct password 
					pass2.style.backgroundColor = goodColor;
					message.style.color = goodColor;
					message.innerHTML = "Passwords Sama!"
					ubahPwBtn.disabled = false;
					ubahPwBtn.style.color = "white";
				}else{
					//The passwords do not match.
					//Set the color to the bad color and
					//notify the user.
					pass2.style.backgroundColor = badColor;
					message.style.color = badColor;
					message.innerHTML = "Password Tidak Sama!"
					ubahPwBtn.disabled = true;
					ubahPwBtn.style.color = "#c0c0c0";
				}
				
			
			}  
		</script>
		<script>
			function tampilkanPreview(gambar,idpreview){
				//membuat objek gambar
				var gb = gambar.files;									
				//loop untuk merender gambar
				for (var i = 0; i < gb.length; i++){
					//bikin variabel
					var gbPreview = gb[i];
					var imageType = /image.*/;
					var preview = document.getElementById(idpreview);            
					var reader = new FileReader();
					
					if (gbPreview.type.match(imageType)) {
					//jika tipe data sesuai
						preview.file = gbPreview;
						reader.onload = (function(element) { 
							return function(e) { 
								element.src = e.target.result; 
							}; 
						})(preview);
						document.getElementById('image-profil-button').disabled = false;	
						//membaca data URL gambar
						reader.readAsDataURL(gbPreview);
					}else{
						//jika tipe data tidak sesuai
						document.getElementById('image-profil-button').disabled = true;	
						alert("Type file tidak sesuai. Khusus image.");
					}
					   
				}    
			}
		</script>
		<script>
		(function(){
			$(document).ready(function(){
				$("#btn-messageSukses").click(function(){
					$("#messageSukses").hide(1000);  
					$("#btn-messageSukses").hide(1000);  
				});
			});
		})(jQuery);	
		</script>
	</head>
	
	<body>
		<div class="header">
			<h1><a href="<?php echo base_url(); ?>dashboard">Akreditasi SI</a></h1>
			<?php
				include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/header.php";
			?>
		</div>
		
		<div class="sidebar">
			<?php
				include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/sidebar.php";
			?>
		</div>
			
		<div class="main-layout">
			<div class="sub-header">
			<h2>Profil Anggota Tim Pengumpul Data Akreditasi</h2>
			</div>
			<div class="main-content">
				<div class="sub-header">
					<h2 >Data Diri Tim Akreditasi</h2>
				</div>
				<div class="data-tim">
					<?php
					if (isset($_SESSION['message'])) {
					?>		
						<div class="messageSukses" id="messageSukses">
						<button type="button" id="btn-messageSukses" class="btn-messageSukses"><?php echo $_SESSION['message']; ?></button>		
						</div>	
					<?php		
						unset($_SESSION['message']);
					}
					?>				
					<h3>Data Pribadi Anggota</h3>
					
					<?php 
					if(!isset($dbuser)){
					?>
					<table id="table-view-data-tim-akreditasi" class="display" width="100%" cellspacing="0">
						<form class="data-diri-form" name="data-diri-form" action="editpp" method="post" enctype="multipart/form-data">
						<tbody>
							<tr>
							<td colspan="3"><img class="image" id="preview" src="<?php echo base_url($profilpic_path); ?>" style="height:200px;width:160px;border:1px solid black;display:block;"></td>
							</tr>
							<tr>
								<td class="td-label"><p class="label">Foto Profil</p></td>
								<td style="width:10%;"><p> : </p></td>
								<td><input class="data-diri-input" id="data-diri-foto"  type="file" name="picfile[]" accept="image/png, image/jpg, image/jpeg, image/gif" onchange="tampilkanPreview(this,'preview')"><br><label class="ukuran">*Maksimal Size Foto 4MB</label></td>
							</tr>
							<tr>
							<td colspan="3">
							<input type="hidden" name="MAX_FILE_SIZE" value="4200000" />
							<input class="data-diri-button btn" id="image-profil-button" name="simpanPhotobtn" title="Ubah Foto Profil" type="submit" value="Ubah Foto" >
							</td>
							</tr>
						</tbody>
						
						</form>
					</table>
					<table id="table-view-data-tim-akreditasi" class="display" width="100%" cellspacing="0">
						<form class="data-diri-form" name="data-diri-form" action="edit" method="post" enctype="multipart/form-data">
						<tbody>
							<tr>
								<td><p class="label">Email</p></td>
								<td><p> : </p></td>
								<td><input class="data-diri-input" id="data-diri-email"  type="text"  name="email" placeholder="Username" required value="<?php echo $email_user ?>"></td>
							</tr>
							<tr>
								<td><p class="label">Nama Lengkap</p></td>
								<td><p> : </p></td>
								<td><input class="data-diri-input" id="data-diri-namalengkap"  type="text" name="namalengkap" placeholder="Nama Lengkap" required value="<?php echo $nama_user ?>"></td>
							</tr>
							<tr>
								<td><p class="label">Jenis Kelamin</p></td>
								<td><p> : </p></td>
								<td>
								<?php
									if ($jenis_kel == "Laki-Laki"){
								?>		
										<input class="data-diri-input" id="data-diri-jenis-kelamin"  type="radio"  name="jeniskelamin" checked value="Laki-Laki"><label>Laki-Laki</label></input>
										<input class="data-diri-input" id="data-diri-jenis-kelamin"  type="radio"  name="jeniskelamin" value="Perempuan"><label>Perempuan</label></input>
								<?php
									} else if($jenis_kel == "Perempuan"){
								?>
										<input class="data-diri-input" id="data-diri-jenis-kelamin"  type="radio"  name="jeniskelamin" value="Laki-Laki"><label>Laki-Laki</label></input>
										<input class="data-diri-input" id="data-diri-jenis-kelamin"  type="radio"  name="jeniskelamin" checked value="Perempuan"><label>Perempuan</label></input>
								<?php
									} else {
								?>
										<input class="data-diri-input" id="data-diri-jenis-kelamin"  type="radio"  name="jeniskelamin" value="Laki-Laki"><label>Laki-Laki</label></input>
										<input class="data-diri-input" id="data-diri-jenis-kelamin"  type="radio"  name="jeniskelamin" value="Perempuan"><label>Perempuan</label></input>
								<?php	
									}
								?>
								</td>
							</tr>
							<tr>
								<td><p class="label">Alamat</p></td>
								<td><p> : </p></td>
								<td><textarea class="data-diri-input" id="data-diri-alamat"  type="textarea"  name="alamat" placeholder="alamat" ><?php echo $alamat_user ?></textarea></td>
							</tr>
							<tr>
								<td><p class="label">No Handphone</p></td>
								<td><p> : </p></td>
								<td><input class="data-diri-input" id="data-diri-nohp" type="text"  name="nohandphone" placeholder="No Handphone" required value="<?php echo $hp_user ?>"></td>
							</tr>
							<tr>
								<td><p class="label">Tugas</p></td>
								<td><p> : </p></td>
								<td><input class="data-diri-input" id="data-diri-tugas"  type="textarea"  name="tugas" placeholder="Tugas" value="<?php echo $tugas_user ?>" readonly></td>
							</tr>
							<tr>
							<td colspan="3"><input class="data-diri-button btn" id="data-diri-button" name="simpanbtn" title="simpan" type="submit" value="Simpan" ></td>
							</tr>
						</tbody>
						
						</form>
					</table>
					<?php
					} else {
					?>
					<table id="table-view-data-tim-akreditasi" class="display" width="100%" cellspacing="0">
						<tbody>
							<tr>
								<td colspan="3"><img class="image" id="preview" src="<?php echo base_url($profilpic_path); ?>" style="height:200px;width:160px;border:1px solid black;display:block;"></td>
							</tr>								
						</tbody>
						
						
					</table>
					<table id="table-view-data-tim-akreditasi" class="display" width="100%" cellspacing="0">
						<form class="data-diri-form" name="data-diri-form" action="edit" method="post" enctype="multipart/form-data">
						<tbody>
							<tr>
								<td><p class="label">Email</p></td>
								<td><p> : </p></td>
								<td><input style="background:transparent" class="data-diri-input" id="data-diri-email"  type="email" pattern="[^ @]*@[^ @]*"  name="email" placeholder="Username" readonly value="<?php echo $email_user ?>"></td>
							</tr>
							<tr>
								<td><p class="label">Nama Lengkap</p></td>
								<td><p> : </p></td>
								<td><input style="background:transparent" class="data-diri-input" id="data-diri-namalengkap"  type="text" name="namalengkap" placeholder="Nama Lengkap" readonly value="<?php echo $nama_user ?>"></td>
							</tr>
							<tr>
								<td><p class="label">Jenis Kelamin</p></td>
								<td><p> : </p></td>
								<td>
								<?php
									if ($jenis_kel == "Laki-Laki"){
								?>		
										<input class="data-diri-input" id="data-diri-jenis-kelamin"  type="radio"  name="jeniskelamin" checked value="Laki-Laki"><label>Laki-Laki</label></input>
										<input class="data-diri-input" id="data-diri-jenis-kelamin"  type="radio"  name="jeniskelamin" disabled value="Perempuan"><label>Perempuan</label></input>
								<?php
									} else if($jenis_kel == "Perempuan"){
								?>
										<input class="data-diri-input" id="data-diri-jenis-kelamin"  type="radio"  name="jeniskelamin" disabled value="Laki-Laki"><label>Laki-Laki</label></input>
										<input class="data-diri-input" id="data-diri-jenis-kelamin"  type="radio"  name="jeniskelamin"  checked value="Perempuan"><label>Perempuan</label></input>
								<?php
									} else {
								?>
										<input class="data-diri-input" id="data-diri-jenis-kelamin"  type="radio"  name="jeniskelamin"  disabled value="Laki-Laki"><label>Laki-Laki</label></input>
										<input class="data-diri-input" id="data-diri-jenis-kelamin"  type="radio"  name="jeniskelamin"  disabled value="Perempuan"><label>Perempuan</label></input>
								<?php	
									}
								?>
								</td>
							</tr>
							<tr>
								<td><p class="label">Alamat</p></td>
								<td><p> : </p></td>
								<td><input style="background:transparent" class="data-diri-input" id="data-diri-alamat"  type="textarea"  name="alamat" placeholder="alamat" readonly value="<?php echo $alamat_user ?>"></td>
							</tr>
							<tr>
								<td><p class="label">No Handphone</p></td>
								<td><p> : </p></td>
								<td><input style="background:transparent" class="data-diri-input" id="data-diri-nohp" type="text"  name="nohandphone" placeholder="No Handphone" readonly value="<?php echo $hp_user ?>"></td>
							</tr>
							<tr>
								<td><p class="label">Tugas</p></td>
								<td><p> : </p></td>
								<td><input style="background:transparent" class="data-diri-input" id="data-diri-tugas"  type="textarea"  name="tugas" placeholder="Tugas" value="<?php echo $tugas_user ?>" readonly></td>
							</tr>
							
						</tbody>
						
						</form>
					</table>
					<?php
					}
					?>
				</div>
				<?php 
				if(!isset($dbuser)){
				?>
				<div class="ubah-password">
				<?php
				if (isset($_SESSION['message_pw'])) {
				?>		
					<div class="messageSukses" id="messageSukses">
					<button type="button" id="btn-messageSukses" class="btn-messageSukses"><?php echo $_SESSION['message_pw']; ?></button>		
					</div>	
				<?php		
					unset($_SESSION['message_pw']);
				}
				?>				
					<h3>Ubah Password</h3>
						<table id="table-view-data-tim-akreditasi" class="display" width="100%" cellspacing="0">
						<form class="data-diri-form" name="data-diri-form" action="editpw" method="post" enctype="multipart/form-data">
						<tbody>
							<tr>
								<td><p class="label">Password Sekarang</p></td>
								<td><p> : </p></td>
								<td><input class="data-diri-input" id="password-saat-ini"  type="password"  name="password-saatini" required>
								</br>
								</td>
							</tr>
							<tr>
								<td><p class="label">Password Baru</p></td>
								<td><p> : </p></td>
								<td><input class="data-diri-input" id="password-sekarang"  type="password" name="password-sekarang" required></td>
							</tr>
							<tr>
								<td><p class="label">Ketik Ulang Password Baru</p></td>
								<td><p> : </p></td>
								<td><input class="data-diri-input" id="password-konfirmasi" onkeyup="checkPass(); return false;" type="password" name="password-konfirmasi" required>
								</br>
								<span id="confirmMessage" class="confirmMessage"></span>
								</td>
							</tr>
							<tr>
							<td colspan="3"><input class="ubah-password-button btn" id="ubah-password-button" name="ubahbtn" title="Ubah Password" type="submit" value="Ubah Password" ></td>
							</tr>
						</tbody>
						
						</form>
					</table>		
				</div>
				<?php 
				}
				?>
			</div>
		</div>

		<div class="footer">
			<?php
			include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/footer.php";
			?>
		</div>
		
	</body>

	
	
</html>
