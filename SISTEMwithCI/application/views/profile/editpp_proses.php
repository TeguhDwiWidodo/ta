<?php

$session_iduser = $_SESSION['id_user'];

if (isset($_POST['simpanPhotobtn'])) {  
	if(isset($_FILES['picfile'])){
		$errors= array();
		foreach($_FILES['picfile']['tmp_name'] as $key => $tmp_name ){
			$userfoto = $_FILES['picfile']['tmp_name'][$key];
			$userfoto_size = $_FILES['picfile']['size'][$key];
			$userfoto_nama = $_FILES['picfile']['name'][$key];
			$folder= "uploads/fotoprofil/".$session_iduser."/";
			if($userfoto_size > 4194304){
				$errors[]='File size must be less than 4 MB';
			}
		
			if (!file_exists($folder)) {
				mkdir($folder, 0777, true);
				
				$final_file=str_replace(' ','-',$userfoto_nama);
				$profilpic_path = $folder.$final_file;
			
				if(move_uploaded_file($userfoto,$folder.$final_file)){
					$query="UPDATE users SET profilpic_path = '$profilpic_path' WHERE id_user = '$session_iduser'; ";
					$this->db->query($query);
					?>
					<script>
						alert('Information & Profile Picture Updated ');
						window.location.href='../profile/index?success';
					</script>
					<?php
				} else {
					?>
					<script>
						alert('error while updating information');
						window.location.href='../profile/index?failpp1';
					</script>
					<?php
				}
				
			} else {
				$final_file=str_replace(' ','-',$userfoto_nama);
				$profilpic_path = $folder.$final_file;
			
				if(move_uploaded_file($userfoto,$folder.$final_file)){
					$query="UPDATE users SET profilpic_path = '$profilpic_path' WHERE id_user = '$session_iduser'; ";
					$this->db->query($query);
					?>
					<script>
						alert('Profile Picture Updated');
						window.location.href='../profile/index?success';
					</script>
					<?php
				} else {
					?>
					<script>
						alert('error while updating information');
						window.location.href='../profile/index?failpp2';
					</script>
					<?php
				}
			}
		
		}
	}
}	