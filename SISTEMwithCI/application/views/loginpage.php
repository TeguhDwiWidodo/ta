

<!DOCTYPE html>
<html>
	<head>
		<title>Akreditasi | Welcome</title>
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/login.css">
		<script type = 'text/javascript' src="<?php echo base_url(); ?>js/jquery-1.12.4.js"></script>
		<script>
		(function(){
			$(document).ready(function(){
				$("#btn-messageSukses").click(function(){
					$("#messageSukses").hide(1000);  
					$("#btn-messageSukses").hide(1000);  
				});
			});
		})(jQuery);	
		</script>
	</head>
	
	<body>
		<div class="header">
			<h1><a href="index.php">Akreditasi SI</a></h1>
		</div>
		
		<div class="wrapper">
			<div id="loader-wrapper">
				<div class="loader"></div>
				<div class="loader-section"></div>
			</div>
	
			<div class="login-page">
			<?php
				if (isset($_SESSION['message'])) {
			?>		
					<div class="messageSukses" id="messageSukses">
					<button type="button" id="btn-messageSukses" class="btn-messageSukses"><?php echo $_SESSION['message']; ?></button>		
					</div>	
			<?php		
					unset($_SESSION['message']);
				}
			?>
				<form class="login-form" name="login-form" action="login/loginproses" method="post">
					<h3>Login Form</h3>
					<input class="login-input" id="login-username" required="" type="text" title="Please fill out this field Username" name="username" placeholder="Username">
					<br>
					<input class="login-input" id="login-password" required="" type="password" title="Please fill out this field Password" name="password" placeholder="Password">
					<br>
					<input class="login-button" id="login-button" name="loginbtn" title="Login" type="submit" value="Login" >
				</form>
			</div>
		</div>
		
		<footer>
			<h4>Akreditasi SI &copy 2017</h4>
		</footer>
	</body>
	

</html>