<?php
if(!isset($_SESSION['nama'])){
      header("location:" . base_url());
      exit();
   }
?>

<!DOCTYPE html>
<html>
	<head>
		<title>Report</title>
		<script type = 'text/javascript' src="<?php echo base_url(); ?>js/jquery-1.12.4.js"></script>
		<script type = 'text/javascript' src="<?php echo base_url(); ?>js/jquery.dataTables.js"></script>

    <link rel="stylesheet" href="<?php echo base_url(); ?>css/pdf.css">

 <!--    <style>
      .std711{
        background: red;
      }
    </style> -->
	</head>

	<body>

    <h2 >STANDAR 7 : PENELITIAN, PELAYANAN/PENGABDIAN MASYARAKAT, DAN KERJASAMA</h2>

    <div class="std711">
      <div>
        <label>7.1</label>  <label>Penelitian Dosen Tetap yang Bidang Keahliannya Sesuai dengan PS</label>
        </div>
        <div>
          <label><span>7.1.1  </span>Jumlah judul penelitian* yang sesuai dengan bidang keilmuan PS yang dilakukan oleh dosen tetap yang bidang keahliannya sesuai dengan PS selama tiga tahun terakhir sebagai berikut:</label>
          <br>
          <br>
          </div>
            <table align="center" style="width:70%;" class="display1">
              <tbody >
              <tr class="display">
              <th class="display">Sumber Pembiayaan</th>
              <th class="display">TS-2</th>
              <th class="display">TS-1</th>
              <th class="display">TS</th>
            </tr>
            <tr >
            <th class="display">(1)</th>
            <th class="display">(2)</th>
            <th class="display">(3)</th>
            <th class="display">(4)</th>
            </tr>


                    <tr>
                      <td class="display">
                        Pembiayaan sendiri oleh peneliti
                      </td >
                      <td class="display2">
                        <?php  echo $biaya_sendiri_ts2; ?>
                      </td>
                      <td class="display2">
                        <?php  echo $biaya_sendiri_ts1; ?>
                      </td>
                      <td class="display2">
                        <?php  echo $biaya_sendiri_ts; ?>
                      </td>
                    </tr>
                    <tr>
                      <td class="display">
                        PT yang bersangkutan
                      </td>
                      <td class="display2">
                        <?php echo $pt_bersangkutan_ts2; ?>
                      </td>
                      <td class="display2">
                        <?php echo $pt_bersangkutan_ts1; ?>
                      </td>
                      <td class="display2">
                        <?php echo $pt_bersangkutan_ts; ?>
                      </td>
                    </tr>
                    <tr>
                      <td class="display">
                        Depdiknas
                      </td>
                      <td class="display2">
                      <?php echo $depdiknas_ts2; ?>
                      </td>
                      <td class="display2">
                        <?php echo $depdiknas_ts1; ?>
                      </td>
                      <td class="display2">
                        <?php echo $depdiknas_ts; ?>
                      </td>
                    </tr>
                    <tr>
                      <td class="display">
                        Institusi dalam negeri di luar Depdiknas
                      </td>
                      <td class="display2">
                        <?php echo $luar_depdiknas_ts2; ?>
                      </td>
                      <td class="display2">
                        <?php echo $luar_depdiknas_ts1; ?>
                      </td>
                      <td class="display2">
                        <?php echo $luar_depdiknas_ts; ?>
                      </td>
                    </tr>
                    <tr>
                      <td class="display">
                        Institusi luar negeri
                      </td>
                      <td class="display2">
                        <?php echo $biaya_luar_negeri_ts2; ?>
                      </td>
                      <td class="display2">
                        <?php echo $biaya_luar_negeri_ts1; ?>
                      </td>
                      <td class="display2">
                      <?php echo $biaya_luar_negeri_ts; ?>
                      </td >
                    </tr>


                  </tbody>
            </table>
            <label>Catatan: (*) sediakan data pendukung pada saat asesmen lapangan</label>

          </div>
            <br>

        <div class="7_1_2">
            <label>7.1.2 Adakah mahasiswa tugas akhir yang dilibatkan dalam penelitian dosen dalam tiga tahun terakhir? </label>
            <br>
            <?php if($jumlah_mahasiswa_peldosta  == null && $jumlah_mahasiswa_pelta  != null){
              ?>
                    <div style="margin-left:5%;"><input  type="checkbox" name="tidakada" value="Tidak Ada" checked="checked">Tidak Ada</div>
                    <div style="margin-left:5%;"><input  type="checkbox" name="ada" value="Ada" >Ada</div>
            <?php } else { ?>
                   <div style="margin-left:5%;"><input  type="checkbox" name="tidakada" value="Tidak Ada">Tidak Ada</div>
                   <div style="margin-left:5%;"><input  type="checkbox" name="ada" value="Ada" checked="checked">Ada</div>
            <?php } ?>


            <div style="margin-left:5%;">Jika ada, banyaknya mahasiswa PS yang ikut serta dalam penelitian dosen adalah  <?php echo $jumlah_mahasiswa_peldosta; ?> orang, dari <?php echo $jumlah_mahasiswa_pelta; ?> mahasiswa yang melakukan tugas akhir melalui skripsi.</div>
        </div>

        <br>

       <div class="7_1_3">
          <label>7.1.3 Tuliskan judul artikel ilmiah/karya ilmiah/karya seni/buku yang dihasilkan selama tiga tahun terakhir oleh dosen tetap yang bidang keahliannya sesuai dengan PS dengan mengikuti format tabel berikut: </label>
          <br>
          <table align="center"   class="display1"  >


                <thead>
                  <tr>
                  <th id="display-th" class="display" rowspan="2" >No</th>

                  <th class="display"  rowspan="2" >Judul</th>

                  <th class="display" rowspan="2" >Penulis Utama</th>
                  <th class="display" rowspan="2">Penulis Tambahan</th>
                  <th class="display" rowspan="2">Tanggal</th>

                  <th class="display" colspan="3" >Tingkat* </th>

                </tr>

                <tr>
                  <th class="display" >Lokal   </th>

                  <th class="display" >Nasional   </th>

                  <th class="display" >Internasional   </th>
                </tr>
                <thead>

                <tbody  role="alert" aria-live="polite" aria-relevant="all">


                  <?php
                      $no = 0;

                      foreach($publikasi as $row){
                         $no++;
                      ?>
                        <tr >
                          <td class="display2"><?php echo $no;?></td>
                                <td class="display2"><?php echo $row->judul ?></td>
                            <td class="display2"><?php echo $row->penulis_utm ?></td>
                          <td class="display2"><?php echo $row->penulis_tmbh ?></td>
                          <td class="display2"><?php echo $row->tanggal ?> </td>
                              <td class="display2"><?php

                              if($row->skl_publikasi == "Lokal"){
                                echo $row->jlm_dosen_si;

                              }

                                ?></td>
                                <td class="display2"><?php

                                if($row->skl_publikasi == "Nasional"){
                                  echo $row->jlm_dosen_si;

                                }
                                  ?></td>


                                  <td class="display2"><?php

                                  if($row->skl_publikasi == "Internasional"){
                                    echo $row->jlm_dosen_si;

                                  }

                                    ?></td>


                                    <?php

                                  } ?>


                                <tr  >
                                  <td class="display2" colspan="5">Jumlah</td>
                                  <td class="display2">Nc : <?php echo $data_dosen_lokal ?></td>
                                  <td class="display2">Nb : <?php echo $data_dosen_nasional ?></td>
                                  <td class="display2">Na : <?php echo $data_dosen_internasional ?></td>
                                </tr>
                                </tbody>
                    </table>
                    <label>Catatan: * = Tuliskan banyaknya dosen pada sel yang sesuai</label>
        </div>
        <br>
        <div class="7_1_4">
          <label>7.1.4  Sebutkan karya dosen dan atau mahasiswa Program Studi yang telah memperoleh/sedang memproses perlindungan Hak atas Kekayaan Intelektual (HaKI) selama tiga tahun terakhir.</label>
                  <br>
                  <br>
                    <table  align="center" style="witdh:100%;" class="display1">
                          <thead >
                            <tr>
                              <th class="display">No</th>
                              <th class="display">Karya*</th>
                            </tr>
                                    </thead>

                            <tbody >
                              <tr >

                                <?php
                                  $no = 0;
                                foreach($result_haki as $row){
                                    $no++;
                                ?>
                                <tr>
                              <td class="display2">
                                <?php  echo $no; ?>
                              </td>
                              <td class="display2">
                                  <?php  echo $row->karya; ?>
                              </td>
                            </tr>
                      <?php
                      } ?>
                  </tbody>
            </table>
            <label>* Lampirkan surat paten HaKI atau keterangan sejenis.</label>
          </div>
          <br>
          <div class="7-2-1">
            <label>7.2   Kegiatan Pelayanan/Pengabdian kepada Masyarakat (PkM)</label><br>
            <label>7.2.1  Tuliskan jumlah kegiatan Pelayanan/Pengabdian kepada Masyarakat (*) yang sesuai dengan bidang keilmuan PS selama tiga tahun terakhir yang dilakukan oleh dosen tetap yang bidang keahliannya sesuai dengan PS dengan mengikuti format tabel berikut:</label>
            <br>
            <br>
            <table align="center" style="width:70%;" class="display1">
              <tbody >
              <tr class="display">
              <th class="display">Sumber Pembiayaan</th>
              <th class="display">TS-2</th>
              <th class="display">TS-1</th>
              <th class="display">TS</th>
            </tr>
            <tr >
            <th class="display">(1)</th>
            <th class="display">(2)</th>
            <th class="display">(3)</th>
            <th class="display">(4)</th>
            </tr>
                    <tr>
                      <td class="display">
                        Pembiayaan sendiri oleh peneliti
                      </td >
                      <td class="display2"><?php  echo $biaya_sendiri_abdimas_ts2; ?></td>
                      <td class="display2"><?php  echo $biaya_sendiri_abdimas_ts1; ?></td>
                      <td class="display2"><?php  echo $biaya_sendiri_abdimas_ts; ?></td>
                      </td>
                    </tr>
                    <tr>
                      <td class="display">
                        PT yang bersangkutan
                      </td>
                      <td class="display2"><?php echo $pt_bersangkutan_abdimas_ts2; ?></td>
                      <td class="display2"><?php echo $pt_bersangkutan_abdimas_ts1; ?></td>
                      <td class="display2"><?php echo $pt_bersangkutan_abdimas_ts; ?></td>
                    </tr>
                    <tr>
                      <td class="display">
                        Depdiknas
                      </td >
                      <td class="display2"><?php echo $depdiknas_abdimas_ts2; ?></td>
                      <td class="display2"><?php echo $depdiknas_abdimas_ts1; ?></td>
                      <td class="display2"><?php echo $depdiknas_abdimas_ts; ?></td>
                    </tr>
                    <tr>
                      <td class="display">
                        Institusi dalam negeri di luar Depdiknas
                      </td>
                      <td class="display2"><?php echo $luar_depdiknas_abdimas_ts2; ?></td>
                      <td class="display2"><?php echo $luar_depdiknas_abdimas_ts2; ?></td>
                      <td class="display2"><?php echo $luar_depdiknas_abdimas_ts2; ?></td>
                    </tr>
                    <tr>
                      <td class="display">
                        Institusi luar negeri
                      </td>
                      <td class="display2"><?php echo  $biaya_luar_negeri_abdimas_hitung_ts2; ?></td>
                      <td class="display2"><?php echo  $biaya_luar_negeri_abdimas_hitung_ts1; ?></td>
                      <td class="display2"><?php echo  $biaya_luar_negeri_abdimas_hitung_ts; ?></td>
                    </tr>


                  </tbody>
            </table>
            <labe>Catatan: (*) Pelayanan/Pengabdian kepada Masyarakat adalah penerapan bidang ilmu untuk menyelesaikan masalah di masyarakat (termasuk masyarakat industri, pemerintah, dsb.)</label>

          </div>
          <br>
          <div class="7-2-2">
            <label>7.1.2 Adakah mahasiswa tugas akhir yang dilibatkan dalam penelitian dosen dalam tiga tahun terakhir? </label>
            <br>
            <?php if( $view722->text  == null){
              ?>
                    <div style="margin-left:5%;"><input  type="checkbox" name="tidakada" value="Tidak Ada" checked="checked">Tidak</div>
                    <div style="margin-left:5%;"><input  type="checkbox" name="ada" value="Ada" >Ya</div>
            <?php } else { ?>
                   <div style="margin-left:5%;"><input  type="checkbox" name="tidakada" value="Tidak Ada">Tidak</div>
                   <div style="margin-left:5%;"><input  type="checkbox" name="ada" value="Ada" checked="checked">Ya</div>
            <?php } ?>


            <div style="text-align:justify;">
              <label><b>Deskripsi</b></label>
              <div style="border:1px solid gray;padding:1%;">
                <p >
                <?php
                      echo	 $view722->text;
                 ?>
                </p>
            </div>
          </div>
          </div>
          <br>
          <div class="7-3-1">
            <label>7.3   Kegiatan Kerjasama dengan Instansi Lain</label><br>
            <label>7.3.1  Tuliskan instansi dalam negeri yang menjalin kerjasama* yang terkait dengan program studi/jurusan dalam tiga tahun terakhir.</label>
            <table class="display1" >
              <thead >
                <tr>
                <th  class="display">No</th>
                <th  class="display">Nama Instansi</th>
                <th  class="display">Jenis Kegiatan</th>
                <th  class="display">Mulai Kerjasama</th>
                <th  class="display">Akhir Kerjasama</th>
                <th  class="display">Manfaat</th>
              </tr>
              </thead>

              <tbody  role="alert" aria-live="polite" aria-relevant="all">
                <?php
                    $no = 1;
                    foreach($result as $row){
                ?>
                <tr id="row:<?php echo $row->id ?>">
                  <td   class="display2"> <?php echo $no++ ?></td>
                  <td    class="display2"> <?php echo $row->nama_instansi ?></td>
                  <td    class="display2"> <?php echo $row->jenis_kegiatan ?></td>
                  <td    class="display2"> <?php echo $row->mulai_kerjasama ?></td>
                  <td   class="display2"> <?php echo $row->akhir_kerjasama ?></td>
                  <td class="display2"><?php echo $row->manfaat ?></td>
                </tr>
                <?php
                  }
                ?>
              </tbody>
              <tfoot >
                <tr>
              <th  class="display" colspan="5">Total Instansi Dalam Negeri</th>
              <th  class="display"><?php echo $count_dn; ?></th>
              <th  class="display"></th>
              </tr>
              </tfoot>
            </table>

            <label>Catatan : (*) dokumen pendukung disediakan pada saat asesmen lapangan</label>

          </div>
          <br>
          <div class="7-3-2">
            <label>7.3.2  Tuliskan instansi Luar negeri yang menjalin kerjasama* yang terkait dengan program studi/jurusan dalam tiga tahun terakhir.</label>
            <table  class="display1" width="100%" >

              <thead >
                <tr>
                <th class="display">no</th>
                <th class="display">Nama Instansi</th>
                <th class="display">Jenis Kegiatan</th>
                <th class="display">Mulai Kerjasama</th>
                <th class="display">Akhir Kerjasama</th>
                <th class="display">Manfaat</th>
              </tr>
              </thead>

              <tbody class="table body-peldosen" role="alert" aria-live="polite" aria-relevant="all">

                <?php
                    $no = 1;
                    foreach($result1 as $row){

                    ?>
                      <tr id="row:<?php echo $row->id ?>">
                        <td class="display2"><?php echo $no++ ?></td>
                        <td class="display2"><?php echo $row->nama_instansi ?></td>
                        <td class="display2"><?php echo $row->jenis_kegiatan ?></td>
                        <td class="display2"><?php echo $row->mulai_kerjasama ?></td>
                        <td class="display2"><?php echo $row->akhir_kerjasama ?></td>
                        <td class="display2"><?php echo $row->manfaat ?></td>
                      </tr>
                  <?php
                    }
                  ?>
              </tbody>
              <tfoot >
                <tr>
                <th class="display" colspan="5">Total Instansi Luar Negeri</th>
                <th class="display"><?php echo $count_ln; ?></th>
                <th class="display"></th>
              </tr>
              </tfoot>
            </table>
            <label>Catatan : (*) dokumen pendukung disediakan pada saat asesmen lapangan</label>

          </div>


          <div class="kerjasama_layout">

            <table style="text-align:center;border: 1px solid gray;margin-top:50px;">
                <tbody>
                  <tr >
                     <td >id </td>
                      <td >Aspek penilaian</td>
                       <td >Bobot</td>
                        <td >nilai</td>

                 </tr>

                 <?php

                           foreach($nilai as $nl){
                 echo "<tr><td>".$nl->id."</td><td> ".$nl->aspek_penilaian."</td><td>".$nl->bobot."</td><td>".round($nl->nilai, 2)."</td></tr>";




                           }
    echo "<tr><td colspan='2'>Total </td><td ></td><td>".round($nilai_count, 2)."</td></tr>";
                           ?>


              </tbody>
            </table>

        </div>
	    </body>
</html>
