<?php
	
	unset($_SESSION['nama']);
	unset($_SESSION['hak_akses']);
	session_destroy();
	header("Location:" . base_url());
?>