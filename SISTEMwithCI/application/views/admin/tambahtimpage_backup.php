
<!DOCTYPE html>
<html>
	<head>
		<title>Akreditasi | Anggota</title>
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/mainlayout.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/profile.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/timakreditasi.css">
		<script type = 'text/javascript' src="<?php echo base_url(); ?>js/jquery-1.12.4.js"></script>
		<script type = 'text/javascript' src="<?php echo base_url(); ?>js/jquery.dataTables.js"></script>
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/jquery.dataTables.css">
		<script type = 'text/javascript' >
		(function ($){	
			$(document).ready(function() {
				$('#table-view-data-tim-akreditasi').dataTable({
					"order": [[ 4, 'asc' ]],
					
				});
			});	
		})(jQuery);	
		</script><script>
		(function(){
			$(document).ready(function(){
				$("#btn-messageSukses").click(function(){
					$("#messageSukses").hide(1000);  
					$("#btn-messageSukses").hide(1000);  
				});
			});
		})(jQuery);	
		</script>
	</head>
	
	<body>
		<div class="header">
			<h1><a href="<?php echo base_url(); ?>dashboard">Akreditasi SI</a></h1>
			<?php
				include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/header.php";
			?>
		</div>
		
		<div class="sidebar">
			<?php
				include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/sidebar.php";
			?>
		</div>
			
		<div class="main-layout">
			<div class="sub-header">
			<h2>Anggota Tim Pengumpul Data Akreditasi</h2>
			</div>
			<div class="main-content">
				<div class="sub-header">
				<h2 >Daftar Anggota Tim Akreditasi</h2>
				</div>
				<div class="tambahtim">
				<div class="form-data-tim">
					<?php
					if (isset($_SESSION['message'])) {
					?>		
						<div class="messageSukses" id="messageSukses">
						<button type="button" id="btn-messageSukses" class="btn-messageSukses"><?php echo $_SESSION['message']; ?></button>		
						</div>	
					<?php		
						unset($_SESSION['message']);
					}
					?>				
					<h3>Form Daftar Anggota</h3>
					
					<table id="table-data-tim-akreditasi" class="display" width="100%" cellspacing="0">
						
						<form class="data-diri-form" name="data-diri-form" action="tambahtim_proses" method="post" enctype="multipart/form-data">
						<tbody>
							<tr>
								<td class="label-form"><p >Email</p></td>
								<td ><p > : </p></td>
								<td><input class="data-tim-input" id="data-diri-email" type="email" pattern="[^ @]*@[^ @]*"  name="email" placeholder="Username" required ></td>
							</tr>
							<tr>
								<td class="label-form"><p >Password</p></td>
								<td><p> : </p></td>
								<td><input class="data-tim-input" id="data-diri-password"  type="password" name="password" placeholder="Password" required ></td>
							</tr>
							<tr>
								<td class="label-form"><p>Nama Lengkap</p></td>
								<td><p> : </p></td>
								<td><input class="data-tim-input" id="data-diri-namalengkap"  type="text" name="namalengkap" placeholder="Nama Lengkap" required ></td>
							</tr>
							<tr>
								<td class="label-form"><p>No Handphone</p></td>
								<td><p> : </p></td>
								<td><input class="data-tim-input" id="data-diri-nohp" type="text"  name="nohandphone" placeholder="No Handphone" required ></td>
							</tr>
							<tr>
								<td class="label-form"><p>Hak Akses</p></td>
								<td><p> : </p></td>
								<td><select class="data-tim-input" id="data-diri-hakakses" name="hakakses" required>
									 <option value="admin">Admin</option>
									 <option value="kaprodi">Kaprodi</option>
									  <option value="standar 1" >Standar 1</option>
									  <option value="standar 2" >Standar 2</option>
									  <option value="standar 3" >Standar 3</option>
									  <option value="standar 4" >Standar 4</option>
									  <option value="standar 5" >Standar 5</option>
									  <option value="standar 6" >Standar 6</option>
									  <option value="standar 7" >Standar 7</option>
									</select>
								</td>
							</tr>
							<tr>
							<td colspan="3"><input class="data-diri-button btn" id="data-diri-button" name="simpanbtn" title="simpan" type="submit" value="Tambah" ></td>
							</tr>
						</tbody>
						
						</form>
					</table>
				</div>
				<div class="data-tim-akreditasi">
				<div id="messageEdit" style="display: none;">Updated</div>
					<h3>Pengguna Aplikasi Akreditasi Sistem Informasi</h3>
					<table id="table-view-data-tim-akreditasi" class="display" width="100%" cellspacing="0">
						<thead class="table head">
							<th >Email</th>
							<th >Nama</th>
							<th >No Handphone</th>
							<th >Alamat</th>
							<th >Hak Akses</th>
							<th >Edit</th>
							<th >Hapus</th>
						</thead>
						<tbody>
							<?php
								foreach($all_users as $row){
								?>	
									<tr id="row:<?php echo $row['id_user'] ?>">
										<?php 
										if($row['id_user'] == $_SESSION['id_user']){
										?>	
											<td ><a id="email:<?php echo $row['id_user'] ?>" href="<?php echo site_url();?>profile/"><?php echo $row['email'] ?></a></td> 
										<?php	
										} else {
										?>
											<td ><a id="email:<?php echo $row['id_user'] ?>" href="<?php echo site_url();?>profile/user/<?php echo $row['id_user'] ?>"><?php echo $row['email'] ?></a></td> 	
										<?php
										}
										?>											
										<td id="nama:<?php echo $row['id_user'] ?>"><?php echo $row['nama'] ?></td>
										<td id="hp:<?php echo $row['id_user'] ?>"><?php echo $row['hp'] ?></td>
										<td id="alamat:<?php echo $row['id_user'] ?>"><?php echo $row['alamat'] ?></td>
										<td id="hak_akses:<?php echo $row['id_user'] ?>"><?php echo $row['hak_akses'] ?></td>
										<td><button class="btnn" onclick="edit_row('<?php echo $row['id_user'] ?>')" id="edit_btn<?php echo $row['id_user'] ?>">Edit</button>
										<button style="display: none;" class="btnn" onclick="save_row('<?php echo $row['id_user'] ?>')" id="save_btn<?php echo $row['id_user'] ?>">Save</button></td>
										<td><button class="btnn" onclick="delete_row('<?php echo $row['id_user'] ?>')" id="delete_btn<?php echo $row['id_user'] ?>">Delete</button></td>
									</tr>
								<?php
									}
								?>
						</tbody>
						<tfoot class="table head">
							<th >Email</th>
							<th >Nama</th>
							<th >No Handphone</th>
							<th >Alamat</th>
							<th >Hak Akses</th>
							<th >Edit</th>
							<th >Hapus</th>
						</tfoot>
						
					</table>
				
				</div>
				</div>
		</div>

		<div class="footer">
			<?php
			include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/footer.php";
			?>
		</div>
		<script>
		function edit_row(no)
		{
			 document.getElementById("edit_btn"+no).style.display="none";
			 document.getElementById("save_btn"+no).style.display="block";
				
			 var email = document.getElementById("email:"+no).innerHTML;
			 var nama = document.getElementById("nama:"+no).innerHTML;
			 var hp = document.getElementById("hp:"+no).innerHTML;
			 var alamat = document.getElementById("alamat:"+no).innerHTML;
			 var hak_akses = document.getElementById("hak_akses:"+no).innerHTML;
			 
			 
			document.getElementById("email:"+no).removeAttribute("href");
			 document.getElementById("email:"+no).innerHTML="<input type='text' id='email_text"+no+"' value='"+email+"'>";
			 document.getElementById("nama:"+no).innerHTML="<input type='text' id='nama_text"+no+"' value='"+nama+"'>";
			 document.getElementById("hp:"+no).innerHTML="<input type='text' id='hp_text"+no+"' value='"+hp+"'>";
			 document.getElementById("alamat:"+no).innerHTML="<textarea id='alamat_text"+no+"'>"+alamat+"</textarea>";
			 document.getElementById("hak_akses:"+no).innerHTML="<select id='hak_akses_text"+no+"' required>"
									+ "<option value='admin'>Admin</option>"
									+ "<option value='kaprodi'>Kaprodi</option>"
									+  "<option value='standar 1' >Standar 1</option>"
									+  "<option value='standar 2' >Standar 2</option>"
									+  "<option value='standar 3' >Standar 3</option>"
									+  "<option value='standar 4' >Standar 4</option>"
									+  "<option value='standar 5' >Standar 5</option>"
									+  "<option value='standar 6' >Standar 6</option>"
									+  "<option value='standar 7' >Standar 7</option>"
									+ "</select>";
			
		}
		</script>
		<script>
		function save_row(no)
		{
			 var tugas_val = document.getElementById("tugas:"+no).value;
			 var row_no = document.getElementById("row:"+no).value;
			 var user = document.getElementById("nama:"+no).innerHTML;

			document.getElementById("tugas:"+no).innerHTML = tugas_val;

			
			$.ajax
			 ({
			  type:'post',
			  url:'edittugas',
			  data:{
			   edit_row:'edit_row',
			   row_no:no,
			   tugas_val:tugas_val,
			   nama:user
			  },
			  success:function(response) {
			   if(response == "success")
			   {
					var message_status = $("#status");
					message_status.show();
					message_status.text(data);
					//hide the message
					setTimeout(function(){message_status.hide()},3000);
					document.getElementById("tugas:"+no).innerHTML=tugas_val;
					document.getElementById("edit_btn"+no).style.display="block";
					document.getElementById("save_btn"+no).style.display="none";
					document.getElementById("tugas:"+no).readOnly =true;
					document.getElementById("tugas:"+no).style.outline ="none";
					document.getElementById("tugas:"+no).style.background ="white";
			   }
			  }
			 });
		}
		</script>
		
	</body>

	
	
</html>
