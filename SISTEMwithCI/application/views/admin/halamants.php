
<!DOCTYPE html>
<html>
	<head>
		<title>Akreditasi | Tambah TS</title>
		<script type = 'text/javascript' src="<?php echo base_url(); ?>js/jquery-1.12.4.js"></script>
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/mainlayout.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/penelitian.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/dosen.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/accordion.css">
		<script type = 'text/javascript' src="<?php echo base_url(); ?>assets/autosize-master/dist/autosize.js"></script>
		<script type = 'text/javascript' src="<?php echo base_url(); ?>js/jquery.dataTables.js"></script>
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/jquery.dataTables.css">
		<script type = 'text/javascript' >
		(function ($){
			$(document).ready(function() {
				$('#table-view-data-peldosen').dataTable({
					"order": [[ 0, 'asc' ]],
					"aLengthMenu": [5, 10, 25],
					iDisplayLength: 5,

				});
			});
		})(jQuery);
		</script>
	</head>

	<body>
		<div class="header">
			<h1><a href="<?php echo base_url(); ?>dashboard">Akreditasi SI</a></h1>
			<?php
				include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/header.php";
			?>
		</div>

		<div class="sidebar">
			<?php
				include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/sidebar.php";
			?>
		</div>

		<div class="main-layout">
			<?php

			if ( $this->session->userdata('hak_akses') == 'admin'){

				?>
			<div class="main-content">


							<table class="display" width="100%" cellspacing="0">
							<form  class="data-diri-form" name="data-diri-form" method="post"   action ="<?php echo base_url(); ?>Admin/tambahtsinsert" enctype="multipart/form-data">

							<tbody>
									<tr>
								<td colspan="3"><h3 class="head-panel">INPUT DATA HaKI</h3><hr/><br/></td>
								</tr>


								</tr>
								<tr>
									<td><p class="label">Tahun Awal</p></td>
									<td><p> : </p></td>
									<td><input class="data-input-kerjasama" id="tahunaw"  type="date"  name="tahunaw" placeholder="tahun" required ></td>

								</tr>

								<tr>
								<td colspan="3"><br/><hr/><input class="data-diri-button btn" id="data-diri-button" name="simpanbtn" title="simpan" type="submit" value="Simpan" ><br/></td>
								</tr>
							</tbody>

							</form>

						</table>

			</div>



			<div class="data-dosen" >
			<table id="table-view-data-peldosen" class="display" width="100%" cellspacing="0">

						<thead class="table head-peldosen">
							<th>#</th>
							<th>no</th>
							<th >Tahun Awal</th>
							<th><i class="fa fa-pencil-square-o" aria-hidden="true"></i></th>
							<th><i class="fa fa-trash-o" aria-hidden="true"></i></th>
						</thead>

						<tbody class="table body-peldosen" role="alert" aria-live="polite" aria-relevant="all">


														<?php
																$nom = 1;
																foreach($result as $row){

																?>
																	<tr id="row:<?php echo $row->no ?>">
																	<td ><input type="checkbox" name="pilih[]" class="check" value="<?php echo $row->no ?>" ></td>
																		<td ><?php echo $nom++ ?></td>
																		<td ><textarea class="data-diri-input" title="<?php echo $row->date_from?>" id="dateaw<?php echo $row->no ?>"  type="text" name="dateaw" placeholder="" readonly /><?php echo $row->date_from ?></textarea></td>

																			<td><button onclick="edit_row1('<?php echo $row->no ?>')" title="Ubah : <?php echo $row->date_from ?>" id="edit<?php echo $row->no ?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
																			<button  onclick="save_row1('<?php echo $row->no ?>')" title="Simpan : <?php echo $row->date_from ?>" id="save<?php echo $row->no ?>" style="display:none;" /><i class="fa fa-floppy-o" aria-hidden="true"></i></button>
																			</td>
																			<td>
																			<form action="<?php echo base_url(); ?>Admin/numbertshapus/<?php echo $row->no ?>" method="post" enctype="multipart/form-data">
																			<input type="hidden" value="<?php echo $row->no?>" name="data-id" >
																			<button name="btn-delete" title="Hapus : <?php echo $row->date_from?>" onclick="return ConfirmDelete(<?php echo $row->no ?>)"  id="delete-btn<?php echo $row->no ?>" type="submit"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
																			</form>
																				<script type="text/javascript">
																						var baseUrl='<?php echo base_url(); ?>admin/kerjasamahapus/<?php echo $row->no ?>';
																						var msgg = 'Apakah anda yakin untuk menghapus data ini ?';
																						function ConfirmDelete(no)
																						{
																						if (confirm(msgg)) {
																							location.href=baseUrl;
																						}
																						else {
																							 return false;
																						}
																						}
																				</script>
																			</td>


																						<script>
																			var no;
																			function edit_row1(no)
																			{
																					document.getElementById('edit'+no).style.display= 'none';
																					document.getElementById('save'+no).style.display= 'block';

																				document.getElementById("dateaw"+no).removeAttribute('readonly');


																				 document.getElementById('dateaw'+no).style.background = "#ccffff";



																			}



																			</script>
																			<script>

																			function save_row1(no){

																				var dateaw=  document.getElementById("dateaw"+no).value;


																				var id = no;


																							$.ajax({

																									type: "POST",
																									url: "<?php echo base_url(); ?>Admin/numbertsupdate",
																									data: {id:id, dateaw:dateaw},

																									success: function(data){
																									console.log(data);

																										},

																									error: function(xhr, status, errorThrown){
																									alert( xhr.status);
																									alert( xhr.errorThrown);
																									alert( xhr.responseText);
																									console.log(xhr.responseText);
																									}
																							});

																													document.getElementById('edit'+no).style.display= 'block';
																													document.getElementById('save'+no).style.display= 'none';
																													document.getElementById('dateaw'+no).readOnly =true;




								 																				 document.getElementById('dateaw'+no).style.background = "#f3f3f3";


																												}
																			</script>

																	</tr>
															<?php
																}
															?>
															</tbody>
															<tfoot class="table head-peldosen">
																<th>#</th>
																<th>no</th>
																<th >Tahun Awal</th>
																<th><i class="fa fa-pencil-square-o" aria-hidden="true"></i></th>
																<th><i class="fa fa-trash-o" aria-hidden="true"></i></th>
															</tfoot>

													</table>
												</div>

			</div>
			<?php
				}
					?>

		</div>

		<div class="footer">
			<?php
			include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/footer.php";
			?>
		</div>

	</body>



</html>
