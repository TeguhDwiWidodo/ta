
<!DOCTYPE html>
<html>
	<head>
		<title>Akreditasi | Anggota</title>
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/mainlayout.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/kelolapengguna.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>font-awesome-4.6.3/css/font-awesome.min.css">

		<!--<link rel="stylesheet" href="<?php //echo base_url(); ?>css/profile.css">-->
		<!--<link rel="stylesheet" href="<?php //echo base_url(); ?>css/timakreditasi.css">-->
		<script type = 'text/javascript' src="<?php echo base_url(); ?>js/jquery-1.12.4.js"></script>
		<script type = 'text/javascript' src="<?php echo base_url(); ?>js/jquery.dataTables.js"></script>
		<!--<link rel="stylesheet" href="<?php echo base_url(); ?>css/jquery.dataTables.css">-->
		<script type = 'text/javascript' src="<?php echo base_url(); ?>js/kelolapengguna.js"></script>
		<script type = 'text/javascript' src="<?php echo base_url(); ?>js/jquery.validate.min.js"></script>

		<script>
		(function(){
			$(document).ready(function(){
				$("#btn-messageSukses").click(function(){
					$("#messageSukses").hide(1000);
					$("#btn-messageSukses").hide(1000);
				});
			});
		})(jQuery);
		</script>
		<script>
		(function($){
			$(document).ready(function(){

			});
		})(jQuery);
		</script>

	</head>

	<body>
		<div class="header">
			<h1><a href="<?php echo base_url(); ?>dashboard">Akreditasi SI</a></h1>
			<?php
				include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/header.php";
			?>
		</div>

		<div class="sidebar">
			<?php
				include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/sidebar.php";
			?>
		</div>

		<div class="main-layout">
			<div class="sub-header">
			<h2>Admin</h2>
			</div>
			<div class="main-content">
				<div class="sub-header">
				<h2 >Kelola Pengguna Aplikasi Akreditasi Sistem Informasi</h2>
				</div>
				<div class="data-content">
					<h3 class="h3pengguna">Pengguna Aplikasi Akreditasi Sistem Informasi</h3>
					<button type="button" class="button" id="add_user">Tambah Pengguna</button>

					<table class="datatable" id="table_user">
						<thead>
							<tr>
								<th >Email</th>
								<th >Nama</th>
								<th >No Handphone</th>
								<th >Alamat</th>
								<th >Jenis Kelamin</th>
								<th >Hak Akses</th>
								<th>Functions</th>
							</tr>
						</thead>
						<tbody>
							<?php
							foreach($all_users as $row){
							?>
								<tr id="row:<?php echo $row['id_user'] ?>">
										<?php
										if($row['id_user'] == $_SESSION['id_user']){
										?>
											<td ><a id="email:<?php echo $row['id_user'] ?>" href="<?php echo site_url();?>profile/"><?php echo $row['email'] ?></a></td>
										<?php
										} else {
										?>
											<td ><a id="email:<?php echo $row['id_user'] ?>" href="<?php echo site_url();?>profile/user/<?php echo $row['id_user'] ?>"><?php echo $row['email'] ?></a></td>
										<?php
										}
										?>
										<td id="nama:<?php echo $row['id_user'] ?>"><?php echo $row['nama'] ?></td>
										<td id="hp:<?php echo $row['id_user'] ?>"><?php echo $row['hp'] ?></td>
										<td id="alamat:<?php echo $row['id_user'] ?>"><?php echo $row['alamat'] ?></td>
										<td id="jenis_kelamin:<?php echo $row['id_user'] ?>"><?php echo $row['jenis_kelamin'] ?></td>
										<td id="hak_akses:<?php echo $row['id_user'] ?>"><?php echo $row['hak_akses'] ?></td>
										<td class="functions">
											<div class="function_buttons">
											<ul>
											<li class="function_edit"><a data-id="<?php echo $row['id_user'] ?>" data-name="<?php echo $row['nama'] ?>"><span>Edit</span></a></li>
											<li class="function_delete"><a data-id="<?php echo $row['id_user'] ?>" data-name="<?php echo $row['nama'] ?>"><span>Delete</span></a></li>
											</ul>
											</div>
										</td>

									</tr>

							<?php
							}
							?>
						</tbody>
					</table>
				</div>
			</div>
		</div>

		<div class="lightbox_bg"></div>

		<div class="lightbox_container">
			<div class="lightbox_close"></div>
			<div class="lightbox_content">

				<h2>Tambah Pengguna Aplikasi Akreditasi Sistem Informasi</h2>
				<form class="form add" id="form_user" data-id="" novalidate>
					<div class="input_container">
						<label for="email">Email: <span class="required">*</span></label>
						<div class="field_container">
							<input type="email" class="text" name="email" id="email" value="" required>
						</div>
					</div>
					<div id="input_container_password" class="input_container">
						<label for="password">Password: <span class="required">*</span></label>
						<div class="field_container">
							<input type="password" class="text" name="password" id="password" value="" required>
						</div>
					</div>
					<div class="input_container">
						<label for="nama_lengkap">Nama Lengkap: <span class="required">*</span></label>
						<div class="field_container">
							<input type="text" class="text" name="nama_lengkap" id="nama_lengkap" value="" required>
						</div>
					</div>
					<div class="input_container">
						<label for="nohp">No Handphone: <span class="required">*</span></label>
						<div class="field_container">
							<input type="number" step="1" min="0" class="text" name="nohp" id="nohp" value="" required>
						</div>
					</div>
					<div class="input_container_radio">
						<label for="jenis_kelamin">Jenis Kelamin: <span class="required">*</span></label>
						<div class="field_container_radio">

							<input type="radio" id="jenis_kelamin_pria" class="jenis_kelamin" value="Laki-Laki" name="jenis_kelamin" required>
							<label class="radio_label" for="jenis_kelamin_pria">Laki-Laki</label>
							<br />
							<input type="radio" id="jenis_kelamin_perempuan" class="jenis_kelamin" value="Perempuan" name="jenis_kelamin" required>
							<label class="radio_label" for="jenis_kelamin_perempuan">Perempuan</label>
						</div>
					</div>
					<div class="input_container">
						<label for="hak_akses">Hak Akses: <span class="required">*</span></label>
						<div class="field_container">
							<select class="text" id="hak_akses" name="hak_akses" required>
								<option value="admin">Admin</option>
								<option value="kaprodi">Kaprodi</option>
								<option value="tim penilaian">Tim Penilaian</option>
								<option value="standar 1">Standar 1</option>
								<option value="standar 2">Standar 2</option>
								<option value="standar 3">Standar 3</option>
								<option value="standar 4">Standar 4</option>
								<option value="standar 5">Standar 5</option>
								<option value="standar 6">Standar 6</option>
								<option value="standar 7">Standar 7</option>

							</select>
						</div>
					</div>
					<div class="button_container">
						<button type="submit">Tambah Pengguna</button>
					</div>
				</form>

			</div>
		</div>

		<noscript id="noscript_container">
		  <div id="noscript" class="error">
			<p>JavaScript support is needed to use this page.</p>
		  </div>
		</noscript>

		<div id="message_container">
		  <div id="message" class="success">
			<p>This is a success message.</p>
		  </div>
		</div>

		<div id="loading_container">
		  <div id="loading_container2">
			<div id="loading_container3">
			  <div id="loading_container4">
				Loading, please wait...
			  </div>
			</div>
		  </div>
		</div>

		<div class="footer">
			<?php
			include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/footer.php";
			?>
		</div>


	</body>



</html>
