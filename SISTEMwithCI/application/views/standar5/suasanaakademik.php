<!DOCTYPE html>
<html>
	<head>
		<title>Akreditasi | Standar 5 | Kurikulum</title>
		<script type = 'text/javascript' src="<?php echo base_url(); ?>js/jquery-1.12.4.js"></script>
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/mainlayout.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/accordion.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/jquery.dataTables.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/data&dokumenstd5.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/dokumenpendukung.css">
		<link rel="shortcut icon" href="<?php echo base_url(); ?>/favicon.ico" type="image/x-icon">
		<link rel="icon" href="<?php echo base_url(); ?>/favicon.ico" type="image/x-icon">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/upload.css" />
		<script type = 'text/javascript' src="<?php echo base_url(); ?>js/jquery.dataTables.js"></script>
		
	</head>
	
	<body>
		<div class="header">
			<h1><a href="<?php echo base_url(); ?>dashboard">Akreditasi SI</a></h1>
			<?php
				include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/header.php";
			?>
		</div>
		
		<div class="sidebar">
			<?php
				include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/sidebar.php";
			?>
		</div>
			
		<div class="main-layout">
			<div class="main-content">
			<div class="menu-breadcrumb">
				<ul class="breadcrumb">
					<li><a href="<?php echo base_url(); ?>dashboard" id="menu-breadcrumb">Dashboard</a></li>
					<li>Suasana Akademik</li>
				</ul>
			</div>
			 <div class="menu-head">
				<ul>
					<li><a href="<?php echo base_url(); ?>standar5/kurikulum" id="menu-head" >Kurikulum</a></li>
					<li><a href="<?php echo base_url(); ?>standar5/pembelajaran" id="menu-head" ">Pembelajaran</a></li>
					<li><a href="<?php echo base_url(); ?>standar5/sa" id="menu-head" style="background:#d9d9d9;color:black;">Suasana Akademik</a></li>
				</ul>
			</div>
			<div class="menu-subhead">
				<ul>
					<li><a href="<?php echo base_url(); ?>standar5/sa/5.7upayapeningkatan" id="menu-subhead">5.7 Upaya Peningkatan Suasana Akademik</a></li>
				</ul>
			</div>
			
			<div class="sub-header">
				<h2>Standar 5: Suasana Akademik</h2>
			</div>
			<div class="main-content">
			
				
				
				
			</div>
		</div>

		<div class="footer">
			<?php
			include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/footer.php";
			?>
		</div>
		
	</body>

	
	
</html>
