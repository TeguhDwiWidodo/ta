<?php
if(!isset($_SESSION['nama'])){
      header("location:" . base_url());
      exit();
   }

?>
<!DOCTYPE html>
<html>
	<head>
		<title>Akreditasi | Standar 5 | Riwayat Upload Files</title>
		<script type = 'text/javascript' src="<?php echo base_url(); ?>js/jquery-1.12.4.js"></script>
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/data&dokumenstd5.css">
		<link rel="shortcut icon" href="<?php echo base_url(); ?>/favicon.ico" type="image/x-icon">
		<link rel="icon" href="<?php echo base_url(); ?>/favicon.ico" type="image/x-icon">
		<script type = 'text/javascript' src="<?php echo base_url(); ?>assets/datatables/js/jquery.dataTables.min.js"></script>
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/jquery.dataTables.css">

		<script type = 'text/javascript' >
		
		(function ($){	
			$(document).ready(function() {
				$('table.display').dataTable({
					responsive: true,
					"scrollX": true,
					"scrollY": true,
					pageResize: true,
					autoWidth: true,
					columnDefs: [{ 
						width: 10, targets: 0
						
						}
					]
					
				});
			});	
		})(jQuery);	
		
		</script>
		<style>
			body{
				background-color: #e5e6ad;
			}
			.main{
				padding: 20px 20px 20px 20px;
	
			}
			
		</style>
	</head>
	<body>
		<div class="main">
			<div class="inner">
				<h3>5 Terakhir Pengunggahan Dokumen <?php echo $info ?></h3>
				<table id="table-view-riwayat-<?php echo $table_db_value ?>" class="display" width="100%" cellspacing="0">
					
					<thead class="table head">
						<th>Nama dokumen</th>
						<th>Diunggah Oleh</th>
						<th>Pada tanggal</th>
						<th>Lihat isi dokumen</th>
					</thead>
						
					
					<tbody class="table body">
						<?php
							$dt = new DateTime();
							$tz = new DateTimeZone("Asia/Jakarta"); 
							
							foreach($riwayatFile as $row){
								$dt->setTimestamp($row["tanggal"]);
								$dt->setTimezone($tz);
								$tanggal = $dt->format("l, d F Y, H:i:s");
							?>	
								<tr id="riwayat1:<?php echo $row['id'] ?>">
									<td><a href="<?php echo base_url($row['excel_path']) ?>"><?php echo $row['nama_file'] ?></a></td>
									<td><?php echo $row['nama'] ?></td>
									<td><?php echo $tanggal ?></td>
									<td><a href="<?php echo base_url($row['excelhtml_path']) ?>" target="_blank">Lihat</a></td>
									
								</tr>
						<?php
							}
						?>						
					</tbody>	
				</table>
			</div>	
		</div>	
	</body>						