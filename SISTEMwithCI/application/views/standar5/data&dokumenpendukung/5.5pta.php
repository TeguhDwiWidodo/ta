<?php
if(!isset($_SESSION['nama'])){
      header("location:" . base_url());
      exit();
   }	
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Akreditasi | Standar 5 | Kurikulum | 5.5</title>
		<script type = 'text/javascript' src="<?php echo base_url(); ?>js/jquery-1.12.4.js"></script>
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/mainlayout.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/accordion.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/jquery.dataTables.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/data&dokumenstd5.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/dokumenpendukung.css">
		<script type = 'text/javascript' src="<?php echo base_url(); ?>js/jquery.dataTables.js"></script>
		<script type = 'text/javascript' src="<?php echo base_url(); ?>assets/autosize-master/dist/autosize.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/ckeditor/ckeditor.js"></script>

		<script type = 'text/javascript' >
		(function ($){	
			$(document).ready(function() {
				$('table.display').dataTable({
					responsive: true,
					"scrollX": true,
					"scrollY": true,
					pageResize: true,
					autoWidth: true,
					columnDefs: [{ 
						width: 10, targets: 0
						
						}
					]
					
				});
			});	
		})(jQuery);	
		</script>
		<script>
		(function(){
			$(document).ready(function(){
				autosize($('textarea'));   
			});
		})(jQuery);	
		</script>
		<script>
		(function(){
			$(document).ready(function(){
				$("#btn-messageSukses").click(function(){
					$("#messageSukses").hide(1000);  
					$("#btn-messageSukses").hide(1000);  
				});
			});
		})(jQuery);	
		</script>
		<script>
			function cekTipeFile(num) { 
				var id_value = document.getElementById('files'+num).value;
		
				if(id_value != '') { 
					var valid_extensions = /(.csv|.xls|.xlsx)$/i;   
					if(valid_extensions.test(id_value))
					{ 
						document.getElementById('btn-upload'+num).disabled = false;	
					} else {
						document.getElementById('btn-upload'+num).disabled = true;	
						alert("Tidak dapat mengunggah dokumen. Format file harus .csv/.xls/.xlsx");
					}
				} 
			}
		</script>
	</head>
	
	<body>
		<div class="header">
			<h1><a href="<?php echo base_url(); ?>dashboard">Akreditasi SI</a></h1>
			<?php
				include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/header.php";
			?>
		</div>
		
		<div class="sidebar">
			<?php
				include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/sidebar.php";
			?>
		</div>
			
		<div class="main-layout">
			<div class="main-content">
				<div class="menu-breadcrumb">
					<ul class="breadcrumb">
						<li><a href="<?php echo base_url(); ?>dashboard" id="menu-breadcrumb">Dashboard</a></li>
						<li><a href="<?php echo base_url(); ?>standar5/pembelajaran" id="menu-breadcrumb">Pembelajaran</a></li>
						<li>5.5 Pembimbingan Tugas Akhir / Skripsi</li>
					</ul>
				</div>
				 <div class="menu-head">
					<ul>
						<li><a href="<?php echo base_url(); ?>standar5/kurikulum" id="menu-head">Kurikulum</a></li>
						<li><a href="<?php echo base_url(); ?>standar5/pembelajaran" id="menu-head" style="background:#d9d9d9;color:black;">Pembelajaran</a></li>
						<li><a href="<?php echo base_url(); ?>standar5/sa" id="menu-head">Suasana Akademik</a></li>
					</ul>
				</div>
				<div class="menu-subhead">
					<ul>
						<li><a href="<?php echo base_url(); ?>standar5/pembelajaran/5.3ppp" id="menu-subhead">5.3 Pelaksanaan Proses Pembelajaran</a></li>
					<li><a href="<?php echo base_url(); ?>standar5/pembelajaran/5.4spa" id="menu-subhead" >5.4 Sistem Pembimbingan Akademik</a></li>
					<li><a href="<?php echo base_url(); ?>standar5/pembelajaran/5.5pta" id="menu-subhead" style="background:#404040;color:white;">5.5 Pembimbingan Tugas Akhir / Skripsi</a></li>
					<li><a href="<?php echo base_url(); ?>standar5/pembelajaran/5.6upp" id="menu-subhead">5.6 Upaya Perbaikan Pembelajaran</a></li>
					</ul>
				</div>
				
				<div class="sub-header">
					<h2>5.5 Pembimbingan Tugas Akhir / Skripsi</h2>
				</div>
				<div class="main-content">
					<?php
						if (isset($_SESSION['message'])) {
					?>		
							<div class="messageSukses" id="messageSukses">
							<button type="button" id="btn-messageSukses" class="btn-messageSukses"><?php echo $_SESSION['message']; ?></button>		
							</div>	
					<?php		
							unset($_SESSION['message']);
						}
					?>
					<div class="content">
						
						<h4>5.5.1 Pelaksanaan pembimbingan Tugas Akhir atau Skripsi yang Diterapkan pada Program Studi</h3>
						<button class="accordion">Pelaksanaan pembimbingan Tugas Akhir atau Skripsi yang Diterapkan pada Program Studi</button>
						<div class="panel">
							<div class="panel-content">
								<?php
								if ($_SESSION['hak_akses'] == 'kaprodi' || $_SESSION['hak_akses'] == 'admin' || $_SESSION['hak_akses'] == 'standar 5'){
								?>
								<form action="../uploaddokumen_excel" method="post" enctype="multipart/form-data">
									<label for="files1" class="huruf-upload-files">Upload Files: </label>
									<input required type="file" name="files[]" id="files1" onchange="cekTipeFile(1);" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" />
									<input type="hidden" name="table" value="std5_551pelaksanaanpembimbingan">
									<input type="hidden" name="nav" value="standar5/pembelajaran/5.5pta">
									<label class="custom-btnUpload-filependukung">
										<i class="fa fa-cloud-upload"></i> UPLOAD
										<button class="upload-btn" id="btn-upload3" type="submit" name="btn-upload">UPLOAD</button>
									</label>	
									<br />
									<label class="formatFileLbl" for="files1">*Format file .csv, .xls, .xlsx.</label><br/>
									<a href="../getTemplateExcel?poindata=5122" class="UploadTemplateExcel" target="_blank">*<i class="fa fa-download" aria-hidden="true"></i> Download template</a>
								</form>
								<br />
								<?php
								}
								?>
								<form action="../riwayat_uploadfile" method="post" target="_blank">
									<input type="hidden" name="table_riwayat" value="std5_551pelaksanaanpembimbingan">
									<input type="hidden" name="info" value="Standar 5.5.1 Pelaksanaan pembimbingan Tugas Akhir atau Skripsi yang Diterapkan pada Program Studi">
									<button class="riwayat_btn" id="riwayat_btn" type="submit" name="riwayat_btn">Lihat riwayat 5 terakhir unggahan dokumen </button>
								</form>
								
								<br />
								<table id="table-view-data-5-5-1" class="display" width="100%" cellspacing="0">
									
									<thead class="table head">
										<th>No</th>
										<th>Nama Dosen Pembimbing</th>
										<th>Jumlah Mahasiswa</th>
										<th>Edit</th>
									</thead>
										
									
									<tbody class="table body">
										<?php
										if($result_set551 !== "kosong" && $result_set551){	
											foreach($result_set551 as $row){
											?>	
												<tr id="row1:<?php echo $row['no'] ?>">
													<td><textarea style="font-size:14px;border: none;" id="datatbl11:<?php echo $row['no'] ?>" name="tbl1" readonly type="textarea"><?php echo $row['no'] ?></textarea></td> 	
													<td><textarea style="font-size:14px;border: none;" id="datatbl12:<?php echo $row['no'] ?>" name="tbl1" readonly type="textarea"><?php echo $row['nama_dosen_pembimbing'] ?></textarea></td>
													<td><textarea style="font-size:14px;border: none;" id="datatbl13:<?php echo $row['no'] ?>" name="tbl1" readonly type="textarea"><?php echo $row['jumlah_mahasiswa'] ?></textarea></td>
													<td><input style="display: block;" type="button" onclick="edit_row('1','<?php echo $row['no'] ?>', '3')" id="edit_btn1:<?php echo $row['no'] ?>" class="edit" value="edit">
													<input style="display: none;" type="button" onclick="save_row('1','<?php echo $row['no'] ?>', '3')" id="save_btn1:<?php echo $row['no'] ?>" class="save" value="save">
													<input type="hidden" id="table1" name="table1" value="std5_551pelaksanaanpembimbingan">
													</td>
												</tr>
										<?php
											}
										}
										?>						
									</tbody>
									<tfoot class="table head">
										<th>No</th>
										<th>Nama Dosen Pembimbing</th>
										<th>Jumlah Mahasiswa</th>
										<th>Edit</th>
									</tfoot>	
								</table>
							<?php
								if ($_SESSION['hak_akses'] == 'kaprodi' || $_SESSION['hak_akses'] == 'admin' || $_SESSION['hak_akses'] == 'standar 5'){
							?>	
								<div class="uploadFilePendukung">
								<p>Upload dokumen pendukung (optional):</p>
								<?php //echo $error;?>
								<?php echo form_open_multipart('standar5/doUpload_FilePendukung');?>
									<input class='browseFilePendukung' name="file_pendukung[]" type="file" size="4096"/>
									<input type="hidden" name="poinmateri" value="std5_551">
									<input type="hidden" name="nav" value="standar5/pembelajaran/5.5pta">
									<button type='button' class="add_more">Add More Files</button><br />
									<label class="custom-btnUpload-filependukung">
										<i class="fa fa-cloud-upload"></i> Upload Dokumen
										<input type="submit" name="btn-upload" value="Upload Dokumen" class="btnUpload-filependukung"/>
									</label>
								</form>
								</div>
							<?php
								}
							?>			
								<?php 
								if($filependukung_551 !== "kosong" & $filependukung_551){
								?>
								<div class="teks-listfileupload">3 Daftar terakhir dokumen pendukung yang sudah diunggah</div>
								<div class="listfileupload">
									<table id="DaftarDokumenPendukung" class="display" width="100%" cellspacing="0">
										<thead class="table head">
											<th>Nama Dokumen Pendukung</th>
											<th>Tanggal Upload</th>
											<th>Fungsi</th>
										</thead>
										<tbody>
											<?php
											$dt = new DateTime();
											$tz = new DateTimeZone("Asia/Jakarta"); 
											foreach($filependukung_551 as $row){
												$dt->setTimestamp($row["uploaddate"]);
												$dt->setTimezone($tz);
												$uploaddate = $dt->format("l, d F Y, H:i:s");
											?>
												<tr id="row<?php echo $row['id'];?>">
													<td id="namafile<?php echo $row['id'];?>"><?php echo $row['nama_file']; ?></td>
													<td id="uploaddate<?php echo $row['id'];?>"><?php echo $uploaddate ?></td>
													<td if="fungsi<?php echo $row['id'];?>">
													<a class="downloadFilePendukung" href="<?php echo base_url($row['filepath']) ?>"><i class="fa fa-download fa-lg" aria-hidden="true" style="margin-right: 10px;"></i></a>
											<?php
												if ($_SESSION['hak_akses'] == 'kaprodi' || $_SESSION['hak_akses'] == 'admin' || $_SESSION['hak_akses'] == 'standar 5'){
											?>
													<a class="deleteFilePendukung" data-name="<?php echo $row['nama_file']; ?>" href="../deleteFilePendukung?nav=standar5/pembelajaran/5.5pta&id=<?php echo $row['id'];?>&namafile=<?php echo $row['nama_file']; ?>"><i class="fa fa-trash fa-lg" aria-hidden="true"></i></a>
											<?php
												}
											?>		
													</td>
												</tr>
											<?php
											}
											?>
										</tbody>
									</table>		
								</div>
								<?php
								}
								?>
							</div>
						</div>
						
						<br />
						<h4>5.5.2 Rata-Rata Lama Penyelesaian Tugas Akhir/Skripsi Pada Tiga Tahun Terakhir</h3>
						<button class="accordion">Rata-Rata Lama Penyelesaian Tugas Akhir/Skripsi Pada Tiga Tahun Terakhir</button>
						<div class="panel">
							<div class="panel-content">
							<?php
								if ($_SESSION['hak_akses'] == 'kaprodi' || $_SESSION['hak_akses'] == 'admin' || $_SESSION['hak_akses'] == 'standar 5'){
							?>
								<div class="uploadFilePendukung">
								<p>Upload dokumen pendukung (optional):</p>
								<?php //echo $error;?>
								<?php echo form_open_multipart('standar5/doUpload_FilePendukung');?>
									<input class='browseFilePendukung' name="file_pendukung[]" type="file" size="4096"/>
									<input type="hidden" name="poinmateri" value="std5_552">
									<input type="hidden" name="nav" value="standar5/pembelajaran/5.5pta">
									<button type='button' class="add_more">Add More Files</button><br />
									<label class="custom-btnUpload-filependukung">
										<i class="fa fa-cloud-upload"></i> Upload Dokumen
										<input type="submit" name="btn-upload" value="Upload Dokumen" class="btnUpload-filependukung"/>
									</label>
								</form>
								</div>
							<?php
								}
							?>			
								<?php 
								if($filependukung_552 !== "kosong" & $filependukung_552){
								?>
								<div class="teks-listfileupload">3 Daftar terakhir dokumen pendukung yang sudah diunggah</div>
								<div class="listfileupload">
									<table id="DaftarDokumenPendukung" class="display" width="100%" cellspacing="0">
										<thead class="table head">
											<th>Nama Dokumen Pendukung</th>
											<th>Tanggal Upload</th>
											<th>Fungsi</th>
										</thead>
										<tbody>
											<?php
											$dt = new DateTime();
											$tz = new DateTimeZone("Asia/Jakarta"); 
											foreach($filependukung_552 as $row){
												$dt->setTimestamp($row["uploaddate"]);
												$dt->setTimezone($tz);
												$uploaddate = $dt->format("l, d F Y, H:i:s");
											?>
												<tr id="row<?php echo $row['id'];?>">
													<td id="namafile<?php echo $row['id'];?>"><?php echo $row['nama_file']; ?></td>
													<td id="uploaddate<?php echo $row['id'];?>"><?php echo $uploaddate ?></td>
													<td if="fungsi<?php echo $row['id'];?>">
													<a class="downloadFilePendukung" href="<?php echo base_url($row['filepath']) ?>"><i class="fa fa-download fa-lg" aria-hidden="true" style="margin-right: 10px;"></i></a>
											<?php
												if ($_SESSION['hak_akses'] == 'kaprodi' || $_SESSION['hak_akses'] == 'admin' || $_SESSION['hak_akses'] == 'standar 5'){
											?>
													<a class="deleteFilePendukung" data-name="<?php echo $row['nama_file']; ?>" href="../deleteFilePendukung?nav=standar5/pembelajaran/5.5pta&id=<?php echo $row['id'];?>&namafile=<?php echo $row['nama_file']; ?>"><i class="fa fa-trash fa-lg" aria-hidden="true"></i></a>
											<?php
												}
											?>		
													</td>
												</tr>
											<?php
											}
											?>
										</tbody>
									</table>		
								</div>
								<?php
								}
								if ($_SESSION['hak_akses'] == 'kaprodi' || $_SESSION['hak_akses'] == 'admin' || $_SESSION['hak_akses'] == 'standar 5'){
								?>
								<button style="display: inline;" type="button" onclick="edit_txtarea('2','1','2')" id="editTxt_btn2" class="edit_ckeditor" value="edit">EDIT</button>
								<button style="display: none;" type="button" onclick="save_txtarea('2','1','2')" id="saveTxt_btn2" class="save_ckeditor" value="save">SAVE</button>
								<?php
								}
								if($result_set552 !== "kosong" && $result_set552){
								?>
									<div class="download-pdf">
										<div class="download-pdf-single">
											<a class='download-pdf-anchor' href='../getPdf?poin=552'><i class="fa fa-file-pdf-o" aria-hidden="true" style='margin-right: 1px'></i>Download pdf data informasi poin 5.5.2</a>
										</div>
										<div class="download-pdf-all">
											<a class='download-pdf-anchor' href='../getPdf?poin=all'><i class="fa fa-file-pdf-o" aria-hidden="true" style='margin-right: 1px'></i>Download pdf data informasi semua poin</a>
										</div>
									</div>
									<?php		
									foreach($result_set552 as $row){		
									?>		
										<textarea id="editor1" name="editor1" rows="10" cols="80"><?php echo $row['waktu_penyelesaian_skripsi']?></textarea>
										<input type="hidden" id="table2" name="table1" value="std5_552ratapenyelesaianta">
										<input type="hidden" id="columns11" name="columns11" value="no">
										<input type="hidden" id="columns12" name="columns12" value="waktu_penyelesaian_skripsi">
									<?php
									}
								} else {
									?>
									<textarea id="editor1" name="editor1" rows="10" cols="80"></textarea>
									<input type="hidden" id="table2" name="table1" value="std5_552ratapenyelesaianta">
									<input type="hidden" id="columns11" name="columns11" value="no">
									<input type="hidden" id="columns12" name="columns12" value="waktu_penyelesaian_skripsi">
								<?php	
								}	
								?>
							
							</div>
						</div>
							
					</div>
				</div>

						
						
						
			</div>	
		</div>
		<script>
			$(document).ready(function(){
				var max_upload = 3;
				var x = 1;
				$('.add_more').click(function(e){
					e.preventDefault();
					if(x < max_upload){
						x++;
						$(this).before("<div><input class='browseFilePendukung' name='file_pendukung[]' type='file' size='40'/><a href='#' class='remove_field'>Remove</a></div>");
					} else {
						$(this).hide();
						$('.uploadFilePendukung').append('<p class="maksUploadFilePendukung">* Maksimal 3 upload dokumen pendukung</p>');
					}
				});
				$(this).on("click",".remove_field", function(e){ //user click on remove text
					e.preventDefault(); 
					$(this).parent('div').remove(); x--;
					$('.add_more').show();
					$('.maksUploadFilePendukung').hide();
				})
			});
		</script>
		<script>
			$(document).on('click', '.deleteFilePendukung', function(e){
				e.preventDefault();
				var nama_file = $(this).attr('data-name');
				var choice = confirm("Are you sure you want to delete file '" + nama_file + "'?");
				if (choice) {
					window.location.href =  $(this).attr('href');
				}
			});	
		</script>
		<script>
		(function (){	
			$(document).ready(function(){
			var acc = document.getElementsByClassName("accordion");
			var i;

			for (i = 0; i < acc.length; i++) {
			  acc[i].onclick = function() {
				this.classList.toggle("active");
				var panel = this.nextElementSibling;
				if (panel.style.maxHeight){
				  panel.style.maxHeight = null;
				  panel.style.paddingBottom = null;
				} else {
				  panel.style.maxHeight = panel.scrollHeight+= 5 + "px";
				  panel.style.paddingBottom = "18px";
				} 
			  }
			}
			});
		})(jQuery);	
		</script>
		
		<div class="footer">
			<?php
			include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/footer.php";
			?>
		</div>
		<script type="text/javascript">
			var editor1 = CKEDITOR.replace( 'editor1', {
				height: 400
			});
		</script>
		<script type = 'text/javascript' >
		(function (){	
			$(document).ready(function() {
				CKEDITOR.config.readOnly = true;
			});	
		})(jQuery);	
		</script>
		
		<script>
		function edit_txtarea(ocno, no, nocolumn)
		{
			document.getElementById("editTxt_btn"+ocno).style.display="none";
			document.getElementById("saveTxt_btn"+ocno).style.display="inline";

			var txtarea = document.getElementById("editor"+no);
			var tugas_data = CKEDITOR.instances['editor'+no].getData();
			CKEDITOR.instances['editor'+no].setReadOnly(false);
			
		}
		</script>
		<script>
			function save_txtarea(ocno, no, nocolumn)
			{
				var table = document.getElementById("table"+ocno).value;
				var textarea_val = CKEDITOR.instances['editor'+no].getData();
				var columns = [];
				var columns_data = {};
				
				document.getElementById("editor"+no).innerHTML = textarea_val;
				var i = 1;
				while(i<=nocolumn){
					columns[i] = document.getElementById("columns"+no+i).value;
					columns_data[i] = columns[i];
					i++;
				}
				
				$.ajax
				 ({
				  type:'post',
				  url:'../edit_ckeditor',
				  data:{
				   table:table,
				   columns_data,
				   row_no:no,
				   textarea_val:textarea_val
				  },
				  success:function(response) {
				   if(response == "success")
				   {
					document.getElementById("editTxt_btn"+ocno).style.display="inline";
					document.getElementById("saveTxt_btn"+ocno).style.display="none";
					document.getElementById("editor"+no).innerHTML=textarea_val;
					CKEDITOR.instances['editor'+no].setReadOnly(true);
				   }
				  }
				 });
			}
		</script>
		
		
				
		<script>
			function edit_row(ocid, id, txtareaamount)
			{				
				document.getElementById("edit_btn"+ocid+":"+id).style.display="none";
				document.getElementById("save_btn"+ocid+":"+id).style.display="block";
				var i = 1;
				var j = 1;
				var datatbl = [];
				for(i,j;i<=txtareaamount,j<=txtareaamount; i++,j++){
					datatbl[i] = document.getElementById("datatbl"+ocid+i+":"+id).innerHTML;
					document.getElementById("datatbl"+ocid+i+":"+id).removeAttribute('readonly');
					document.getElementById("datatbl"+ocid+i+":"+id).style.outline ="thin solid #bfbfbf";									 
					document.getElementById("datatbl"+ocid+i+":"+id).style.background ="#f2f2f2";
				}
			}
		</script>
		<script>
			function save_row(ocid, id, txtareaamount)
			{
				var table = document.getElementById("table"+ocid).value;
				var row_no = document.getElementById("row"+ocid+":"+id).value;
				var i = 1;
				var j = 1;
				var datatbl = [];
				var datatbl_data = [];
				var datatbl_val = {};
				while(i<=txtareaamount){
					datatbl[i] = document.getElementById("datatbl"+ocid+i+":"+id).value;
					datatbl_val[i] = datatbl[i];
					i++;
				}
				
				$.ajax
				({
					type:'post',
					url:'../edittabel',
					data:{
					edit_row:table,
					row_no:id,
					columns:txtareaamount,
					datatbl_val

					},
					success:function(response) {
						var i = 1;
						if(response == "success")
						{
							while(i<=txtareaamount){
								datatbl[i] = document.getElementById("datatbl"+ocid+i+":"+id).value;
								document.getElementById("edit_btn"+ocid+":"+id).style.display="block";
								document.getElementById("save_btn"+ocid+":"+id).style.display="none";
								document.getElementById("datatbl"+ocid+i+":"+id).readOnly =true;
								document.getElementById("datatbl"+ocid+i+":"+id).style.outline ="none";
								document.getElementById("datatbl"+ocid+i+":"+id).style.background ="transparent";
								i++;
							}
						}
					}
				}); 
			}
		</script>
	</body>

	
	
</html>
