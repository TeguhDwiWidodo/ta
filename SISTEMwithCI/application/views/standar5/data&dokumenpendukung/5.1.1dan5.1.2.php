<?php
if(!isset($_SESSION['nama'])){
      header("location:" . base_url());
      exit();
   }	
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Akreditasi | Standar 5 | Kurikulum | 5.1.1 dan 5.1.2</title>
		<script type = 'text/javascript' src="<?php echo base_url(); ?>js/jquery-1.12.4.js"></script>		
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/mainlayout.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/accordion.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/jquery.dataTables.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/data&dokumenstd5.css">
		<!--<link rel="stylesheet" href="<?php echo base_url(); ?>font-awesome-4.6.3/css/font-awesome.min.css">-->
		<script type = 'text/javascript' src="<?php echo base_url(); ?>js/jquery.dataTables.min.js"></script>
		<script type = 'text/javascript' src="<?php echo base_url(); ?>assets/autosize-master/dist/autosize.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/ckeditor/ckeditor.js"></script>
		<script type = 'text/javascript' >
		(function ($){	
			$(document).ready(function() {
				$('table').dataTable({
					responsive: true,
					"scrollX": true,
					"scrollY": true,
					pageResize: true,
					autoWidth: true,
					columnDefs: [
						{ width: 10, targets: 0 }
					]
					
				});
			});	
		})(jQuery);	
		</script>
		<script>
		(function(){
			$(document).ready(function(){
				autosize($('textarea'));   
			});
		})(jQuery);	
		</script>
		<script>
		(function(){
			$(document).ready(function(){
				$("#btn-messageSukses").click(function(){
					$("#messageSukses").hide(1000);  
					$("#btn-messageSukses").hide(1000);  
				});
			});
		})(jQuery);	
		</script>
		<script>
			function cekTipeFile(num) { 
				var id_value = document.getElementById('files'+num).value;
		
				if(id_value != '') { 
					var valid_extensions = /(.csv|.xls|.xlsx)$/i;   
					if(valid_extensions.test(id_value))
					{ 
						document.getElementById('btn-upload'+num).disabled = false;	
					} else {
						document.getElementById('btn-upload'+num).disabled = true;	
						alert("Tidak dapat mengunggah dokumen. Format file harus .csv/.xls/.xlsx");
					}
				} 
			}
		</script>
	</head>
	
	<body>
		<div class="header">
			<h1><a href="<?php echo base_url(); ?>dashboard">Akreditasi SI</a></h1>
			<?php
				include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/header.php";
			?>
		</div>
		
		<div class="sidebar">
			<?php
				include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/sidebar.php";
			?>
		</div>
			
		<div class="main-layout">
			<div class="main-content">
			<div class="menu-breadcrumb">
				<ul class="breadcrumb">
					<li><a href="<?php echo base_url(); ?>dashboard" id="menu-breadcrumb">Dashboard</a></li>
					<li><a href="<?php echo base_url(); ?>standar5/kurikulum" id="menu-breadcrumb">Kurikulum</a></li>
					<li>5.1 Kurikulum</li>
				</ul>
			</div>
			 <div class="menu-head">
				<ul>
					<li><a href="<?php echo base_url(); ?>standar5/kurikulum" id="menu-head" style="background:#d9d9d9;color:black;">Kurikulum</a></li>
					<li><a href="<?php echo base_url(); ?>standar5/pembelajaran" id="menu-head">Pembelajaran</a></li>
					<li><a href="<?php echo base_url(); ?>standar5/sa" id="menu-head">Suasana Akademik</a></li>
				</ul>
			</div>
			<div class="menu-subhead">
				<ul>
					<li><a href="<?php echo base_url(); ?>standar5/kurikulum/5.1.1kompetensidan5.1.2sk" id="menu-subhead" style="background:#404040;color:white;">5.1.1 Kompetensi &amp; 5.1.2 Struktur Kurikulum</a></li>
					<li><a href="<?php echo base_url(); ?>standar5/kurikulum/5.1.3dan5.1.4matakuliah" id="menu-subhead">5.1.3 Mata kuliah yang dilaksanakan &amp; 5.1.4 Substansi Praktikum yang mandiri</a></li>
					<li><a href="<?php echo base_url(); ?>standar5/kurikulum/5.2peninjauan" id="menu-subhead">5.2 Peninjauan Kurikulum dalam 5 Tahun Terakhir </a></li>
				</ul>
			</div>
			
			<div class="sub-header">
				<h2>5.1 Kurikulum</h2>
			</div>
			<div class="main-content">
			
			<?php
				if (isset($_SESSION['message'])) {
			?>		
					<div class="messageSukses" id="messageSukses">
					<button type="button" id="btn-messageSukses" class="btn-messageSukses"><?php echo $_SESSION['message']; ?></button>		
					</div>	
			<?php		
					unset($_SESSION['message']);
				}
			?>
					
				<div class="content">
				
					<h4>5.1.1 Kompetensi</h3>
					<button class="accordion">5.1.1.1 Kompetensi Utama Lulusan Program Studi Sistem Informasi</button>
					<div class="panel">
						<div class="panel-content">
						<?php
							if ($_SESSION['hak_akses'] == 'kaprodi' || $_SESSION['hak_akses'] == 'admin' || $_SESSION['hak_akses'] == 'standar 5'){
						?>
							<div class="uploadFilePendukung">
								<p>Upload dokumen pendukung (optional):</p>
								<?php //echo $error;?>
								<?php echo form_open_multipart('standar5/doUpload_FilePendukung');?>
									<input class='browseFilePendukung' name="file_pendukung[]" type="file" size="4096"/>
									<input type="hidden" name="poinmateri" value="std5_5111">
									<input type="hidden" name="nav" value="standar5/kurikulum/5.1.1kompetensidan5.1.2sk">
									<button type='button' class="add_more">Add More Files</button><br />
									<label class="custom-btnUpload-filependukung">
										<i class="fa fa-cloud-upload"></i> Upload Dokumen
										<input type="submit" name="btn-upload" value="Upload Dokumen" class="btnUpload-filependukung"/>
									</label>
								</form>
							</div>
						<?php
							}
						?>			
							<?php 
							if($filependukung_5111 !== "kosong" & $filependukung_5111){
							?>
							<div class="teks-listfileupload">3 Daftar terakhir dokumen pendukung yang sudah diunggah</div>
							<div class="listfileupload">
								<table id="DaftarDokumenPendukung" class="display" width="100%" cellspacing="0">
									<thead class="table head">
										<th>Nama Dokumen Pendukung</th>
										<th>Tanggal Upload</th>
										<th>Fungsi</th>
									</thead>
									<tbody>
										<?php
										$dt = new DateTime();
										$tz = new DateTimeZone("Asia/Jakarta"); 
										foreach($filependukung_5111 as $row){
											$dt->setTimestamp($row["uploaddate"]);
											$dt->setTimezone($tz);
											$uploaddate = $dt->format("l, d F Y, H:i:s");
										?>
											<tr id="row<?php echo $row['id'];?>">
												<td id="namafile<?php echo $row['id'];?>"><?php echo $row['nama_file']; ?></td>
												<td id="uploaddate<?php echo $row['id'];?>"><?php echo $uploaddate ?></td>
												<td if="fungsi<?php echo $row['id'];?>">
												<a class="downloadFilePendukung" href="<?php echo base_url($row['filepath']) ?>"><i class="fa fa-download fa-lg" aria-hidden="true" style="margin-right: 10px;"></i></a>
										<?php
											if ($_SESSION['hak_akses'] == 'kaprodi' || $_SESSION['hak_akses'] == 'admin' || $_SESSION['hak_akses'] == 'standar 5'){
										?>
												<a class="deleteFilePendukung" data-name="<?php echo $row['nama_file']; ?>" href="../deleteFilePendukung?nav=standar5/kurikulum/5.1.1kompetensidan5.1.2sk&id=<?php echo $row['id'];?>&namafile=<?php echo $row['nama_file']; ?>"><i class="fa fa-trash fa-lg" aria-hidden="true"></i></a>
										<?php
											}
										?>		
												</td>
											</tr>
										<?php
										}
										?>
									</tbody>
								</table>
							</div>
							<?php
							}
							if ($_SESSION['hak_akses'] == 'kaprodi' || $_SESSION['hak_akses'] == 'admin' || $_SESSION['hak_akses'] == 'standar 5'){
							?>
								<button style="display: inline;" type="button" onclick="edit_txtarea('1','1','2')" id="editTxt_btn1" class="edit_ckeditor" value="edit">EDIT</button>
								<button style="display: none;" type="button" onclick="save_txtarea('1','1','2')" id="saveTxt_btn1" class="save_ckeditor" value="save">SAVE</button>
							<?php
							}
							if($result_set511 !== "kosong" && $result_set511){
							?>
								<div class="download-pdf">
									<div class="download-pdf-single">
										<a class='download-pdf-anchor' href='../getPdf?poin=5111'><i class="fa fa-file-pdf-o" aria-hidden="true" style='margin-right: 1px'></i>Download pdf data informasi poin 5.1.1.1</a>
									</div>
									<div class="download-pdf-all">
										<a class='download-pdf-anchor' href='../getPdf?poin=all'><i class="fa fa-file-pdf-o" aria-hidden="true" style='margin-right: 1px'></i>Download pdf data informasi semua poin</a>
									</div>
								</div>
								<?php
								foreach($result_set511 as $row){		
								?>		
									<textarea id="editor1" name="editor1" rows="10" cols="80"><?php echo $row['kompetensi_utama']?></textarea>
									<input type="hidden" id="table1" name="table1" value="std5_511kompetensi">
									<input type="hidden" id="columns11" name="columns11" value="no">
									<input type="hidden" id="columns12" name="columns12" value="kompetensi_utama">
								<?php
								}
							} else {
								?>
								<textarea id="editor1" name="editor1" rows="10" cols="80"></textarea>
								<input type="hidden" id="table1" name="table1" value="std5_511kompetensi">
								<input type="hidden" id="columns11" name="columns11" value="no">
								<input type="hidden" id="columns12" name="columns12" value="kompetensi_utama">
							<?php	
							}	
							?>
							
						</div>
					</div>

					<button class="accordion">5.1.1.2 Kompetensi Pendukung Lulusan Program Studi Sistem Informasi)</button>
					<div class="panel">
						<div class="panel-content">
						<?php
							if ($_SESSION['hak_akses'] == 'kaprodi' || $_SESSION['hak_akses'] == 'admin' || $_SESSION['hak_akses'] == 'standar 5'){
						?>
							<div class="uploadFilePendukung">
								<p>Upload dokumen pendukung (optional):</p>
								<?php //echo $error;?>
								<?php echo form_open_multipart('standar5/doUpload_FilePendukung');?>
									<input class='browseFilePendukung' name="file_pendukung[]" type="file" size="4096"/>
									<input type="hidden" name="poinmateri" value="std5_5112">
									<input type="hidden" name="nav" value="standar5/kurikulum/5.1.1kompetensidan5.1.2sk">
									<button type='button' class="add_more">Add More Files</button><br />
									<label class="custom-btnUpload-filependukung">
										<i class="fa fa-cloud-upload"></i> Upload Dokumen
										<input type="submit" name="btn-upload" value="Upload Dokumen" class="btnUpload-filependukung"/>
									</label>
								</form>
							</div>
						<?php
							}
						?>			
							<?php 
							if($filependukung_5112 !== "kosong" & $filependukung_5112){
							?>
							<div class="teks-listfileupload">3 Daftar terakhir dokumen pendukung yang sudah diunggah</div>
							<div class="listfileupload">
								<table id="DaftarDokumenPendukung" class="display" width="100%" cellspacing="0">
									<thead class="table head">
										<th>Nama Dokumen Pendukung</th>
										<th>Tanggal Upload</th>
										<th>Fungsi</th>
									</thead>
									<tbody>
										<?php
										$dt = new DateTime();
										$tz = new DateTimeZone("Asia/Jakarta"); 
										foreach($filependukung_5112 as $row){
											$dt->setTimestamp($row["uploaddate"]);
											$dt->setTimezone($tz);
											$uploaddate = $dt->format("l, d F Y, H:i:s");
										?>
											<tr id="row<?php echo $row['id'];?>">
												<td id="namafile<?php echo $row['id'];?>"><?php echo $row['nama_file']; ?></td>
												<td id="uploaddate<?php echo $row['id'];?>"><?php echo $uploaddate ?></td>
												<td if="fungsi<?php echo $row['id'];?>">
												<a class="downloadFilePendukung" href="<?php echo base_url($row['filepath']) ?>"><i class="fa fa-download fa-lg" aria-hidden="true" style="margin-right: 10px;"></i></a>
										<?php
											if ($_SESSION['hak_akses'] == 'kaprodi' || $_SESSION['hak_akses'] == 'admin' || $_SESSION['hak_akses'] == 'standar 5'){
										?>
												<a class="deleteFilePendukung" data-name="<?php echo $row['nama_file']; ?>" href="../deleteFilePendukung?nav=standar5/kurikulum/5.1.1kompetensidan5.1.2sk&id=<?php echo $row['id'];?>&namafile=<?php echo $row['nama_file']; ?>"><i class="fa fa-trash fa-lg" aria-hidden="true"></i></a>
										<?php
											}
										?>		
												</td>
											</tr>
										<?php
										}
										?>
									</tbody>
								</table>
							</div>
							<?php
							}
							if ($_SESSION['hak_akses'] == 'kaprodi' || $_SESSION['hak_akses'] == 'admin' || $_SESSION['hak_akses'] == 'standar 5'){
							?>
							<button style="display: inline;" type="button" onclick="edit_txtarea('2', '1','2')" id="editTxt_btn2" class="edit_ckeditor" value="edit">EDIT</button>
							<button style="display: none;" type="button" onclick="save_txtarea('2', '1','2')" id="saveTxt_btn2" class="save_ckeditor" value="save">SAVE</button>
							<?php
							}
							if($result_set511 !== "kosong" && $result_set511){
							?>
								<div class="download-pdf">
									<div class="download-pdf-single">
										<a class='download-pdf-anchor' href='../getPdf?poin=5112'><i class="fa fa-file-pdf-o" aria-hidden="true" style='margin-right: 1px'></i>Download pdf data informasi poin 5.1.1.2</a>
									</div>
									<div class="download-pdf-all">
										<a class='download-pdf-anchor' href='../getPdf?poin=all'><i class="fa fa-file-pdf-o" aria-hidden="true" style='margin-right: 1px'></i>Download pdf data informasi semua poin</a>
									</div>
								</div>
								<?php
								foreach($result_set511 as $row){
								?>		
									<textarea id="editor2" name="editor2" rows="10" cols="80"><?php echo $row['kompetensi_pendukung']?></textarea>
									<input type="hidden" id="table2" name="table2" value="std5_511kompetensi">
									<input type="hidden" id="columns21" name="columns21" value="no">
									<input type="hidden" id="columns22" name="columns22" value="kompetensi_pendukung">
								<?php
								}
							} else {
								?>
								<textarea id="editor2" name="editor2" rows="10" cols="80"></textarea>
								<input type="hidden" id="table2" name="table2" value="std5_511kompetensi">
								<input type="hidden" id="columns21" name="columns21" value="no">
								<input type="hidden" id="columns21" name="columns22" value="kompetensi_pendukung">
							<?php	
							}	
							?>
						</div>
					</div>

					<button class="accordion">5.1.1.3 Kompetensi Lainnya Lulusan Program Studi Sistem Informasi</button>
					<div class="panel">
						<div class="panel-content">
						<?php
							if ($_SESSION['hak_akses'] == 'kaprodi' || $_SESSION['hak_akses'] == 'admin' || $_SESSION['hak_akses'] == 'standar 5'){
						?>
							<div class="uploadFilePendukung">
								<p>Upload dokumen pendukung (optional):</p>
								<?php //echo $error;?>
								<?php echo form_open_multipart('standar5/doUpload_FilePendukung');?>
									<input class='browseFilePendukung' name="file_pendukung[]" type="file" size="4096"/>
									<input type="hidden" name="poinmateri" value="std5_5113">
									<input type="hidden" name="nav" value="standar5/kurikulum/5.1.1kompetensidan5.1.2sk">
									<button type='button' class="add_more">Add More Files</button><br />
									<label class="custom-btnUpload-filependukung">
										<i class="fa fa-cloud-upload"></i> Upload Dokumen
										<input type="submit" name="btn-upload" value="Upload Dokumen" class="btnUpload-filependukung"/>
									</label>
								</form>
							</div>
						<?php
							}
						?>			
							<?php 
							if($filependukung_5113 !== "kosong" & $filependukung_5113){
							?>
							<div class="teks-listfileupload">3 Daftar terakhir dokumen pendukung yang sudah diunggah</div>
							<div class="listfileupload">
								<table id="DaftarDokumenPendukung" class="display" width="100%" cellspacing="0">
									<thead class="table head">
										<th>Nama Dokumen Pendukung</th>
										<th>Tanggal Upload</th>
										<th>Fungsi</th>
									</thead>
									<tbody>
										<?php
										$dt = new DateTime();
										$tz = new DateTimeZone("Asia/Jakarta"); 
										foreach($filependukung_5113 as $row){
											$dt->setTimestamp($row["uploaddate"]);
											$dt->setTimezone($tz);
											$uploaddate = $dt->format("l, d F Y, H:i:s");
										?>
											<tr id="row<?php echo $row['id'];?>">
												<td id="namafile<?php echo $row['id'];?>"><?php echo $row['nama_file']; ?></td>
												<td id="uploaddate<?php echo $row['id'];?>"><?php echo $uploaddate ?></td>
												<td if="fungsi<?php echo $row['id'];?>">
												<a class="downloadFilePendukung" href="<?php echo base_url($row['filepath']) ?>"><i class="fa fa-download fa-lg" aria-hidden="true" style="margin-right: 10px;"></i></a>
										<?php
											if ($_SESSION['hak_akses'] == 'kaprodi' || $_SESSION['hak_akses'] == 'admin' || $_SESSION['hak_akses'] == 'standar 5'){
										?>
												<a class="deleteFilePendukung" data-name="<?php echo $row['nama_file']; ?>" href="../deleteFilePendukung?nav=standar5/kurikulum/5.1.1kompetensidan5.1.2sk&id=<?php echo $row['id'];?>&namafile=<?php echo $row['nama_file']; ?>"><i class="fa fa-trash fa-lg" aria-hidden="true"></i></a>
										<?php
											}
										?>		
												</td>
											</tr>
										<?php
										}
										?>
									</tbody>
								</table>	
							</div>
							<?php
							}
							if ($_SESSION['hak_akses'] == 'kaprodi' || $_SESSION['hak_akses'] == 'admin' || $_SESSION['hak_akses'] == 'standar 5'){
							?>
							<button style="display: inline;" type="button" onclick="edit_txtarea('3', '1','2')" id="editTxt_btn3" class="edit_ckeditor" value="edit">EDIT</button>
							<button style="display: none;" type="button" onclick="save_txtarea('3', '1','2')" id="saveTxt_btn3" class="save_ckeditor" value="save">SAVE</button>
							<?php
							}
							if($result_set511 !== "kosong" && $result_set511){
							?>
								<div class="download-pdf">
									<div class="download-pdf-single">
										<a class='download-pdf-anchor' href='../getPdf?poin=5113'><i class="fa fa-file-pdf-o" aria-hidden="true" style='margin-right: 1px'></i>Download pdf data informasi poin 5.1.1.3</a>
									</div>
									<div class="download-pdf-all">
										<a class='download-pdf-anchor' href='../getPdf?poin=all'><i class="fa fa-file-pdf-o" aria-hidden="true" style='margin-right: 1px'></i>Download pdf data informasi semua poin</a>
									</div>
								</div>
								<?php	
								foreach($result_set511 as $row){
								?>		
									<textarea id="editor3" name="editor3" rows="10" cols="80"><?php echo $row['kompetensi_lainnya']?></textarea>
									<input type="hidden" id="table3" name="table3" value="std5_511kompetensi">
									<input type="hidden" id="columns31" name="columns31" value="no">
									<input type="hidden" id="columns32" name="columns32" value="kompetensi_lainnya">
								<?php
								}
							} else {
								?>
								<textarea id="editor3" name="editor3" rows="10" cols="80"></textarea>
								<input type="hidden" id="table3" name="table3" value="std5_511kompetensi">
								<input type="hidden" id="columns31" name="columns31" value="no">
								<input type="hidden" id="columns32" name="columns32" value="kompetensi_lainnya">
							<?php		
							}	
							?>
						</div>
					</div>
					
					<br />
					<h4>5.1.2 Struktur Kurikulum</h3>
					<button class="accordion">5.1.2.1 Jumlah SKS program studi (minimum untuk kelulusan)</button>
					<div class="panel">
						<div class="panel-content">
							<?php
							if ($_SESSION['hak_akses'] == 'kaprodi' || $_SESSION['hak_akses'] == 'admin' || $_SESSION['hak_akses'] == 'standar 5'){
							?>
							<form action="../uploaddokumen_excel" method="post" enctype="multipart/form-data">
								<label for="files1" class="huruf-upload-files">Upload Files: </label>
								<input required type="file" name="files[]" id="files1" onchange="cekTipeFile(1);" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" />
								<input type="hidden" name="table" value="std5_5121jumlahsksps">
								<input type="hidden" name="nav" value="standar5/kurikulum/5.1.1kompetensidan5.1.2sk">
								<label class="custom-btnUpload-filependukung">
									<i class="fa fa-cloud-upload"></i> UPLOAD
									<button class="upload-btn" id="btn-upload1" type="submit" name="btn-upload">UPLOAD</button>
								</label>
								<br />
								<label class="formatFileLbl" for="files1">*Format file .csv, .xls, .xlsx.</label><br/>
								<a href="../getTemplateExcel?poindata=5121" class="UploadTemplateExcel" target="_blank">*<i class="fa fa-download" aria-hidden="true"></i> Download template</a>
							</form>
							<br />
							<?php
							}
							?>
							<form action="../riwayat_uploadfile" method="post" target="_blank">
								<input type="hidden" name="table_riwayat" value="std5_5121jumlahsksps">
								<input type="hidden" name="info" value="5.1.2.1 Jumlah SKS program studi (minimum untuk kelulusan)">
								<button class="riwayat_btn" id="riwayat_btn" type="submit" name="riwayat_btn">Lihat riwayat 5 terakhir unggahan dokumen </button>
							</form>
							<br />
							<table id="table-view-data-5-1-2-1" class="display" width="100%" cellspacing="0">
								
								<thead class="table head">
									<th>No</th>
									<th>Jenis Matakuliah</th>
									<th style="width: 20px">SKS</th>
									<th>keterangan</th>
									<th>Edit</th>
								</thead>
									
								
								<tbody class="table body">
									<?php
									if($result_set5121 !== "kosong" && $result_set5121){
										$no = 0;
										foreach($result_set5121 as $row){
											$no++;
										?>	
											<tr id="row4:<?php echo $row['no'] ?>">
												<td><textarea style="font-size:14px;border: none;" id="datatbl41:<?php echo $row['no'] ?>" name="tbl4" readonly type="textarea"><?php echo $no ?></textarea></td> 	
												<td><textarea style="font-size:14px;border: none;" id="datatbl42:<?php echo $row['no'] ?>" name="tbl4" readonly type="textarea"><?php echo $row['jenis_mata_kuliah'] ?></textarea></td>
												<td><textarea style="font-size:14px;border: none;" id="datatbl43:<?php echo $row['no'] ?>" name="tbl4" readonly type="textarea"><?php echo $row['sks'] ?></textarea></td>
												<td><textarea style="font-size:14px;border: none;" id="datatbl44:<?php echo $row['no'] ?>" name="tbl4" readonly type="textarea"><?php echo $row['keterangan'] ?></textarea></td>
												<td><input style="display: block;" type="button" onclick="edit_row('4','<?php echo $row['no'] ?>', '4')" id="edit_btn4:<?php echo $row['no'] ?>" class="edit" value="edit">
												<input style="display: none;" type="button" onclick="save_row('4','<?php echo $row['no'] ?>', '4')" id="save_btn4:<?php echo $row['no'] ?>" class="save" value="save">
												<input type="hidden" id="table4" name="table4" value="std5_5121jumlahsksps">
												</td>
											</tr>
									<?php
										}
										
									}
									?>						
								</tbody>
								
								<tbody>
									<tr>
										<td colspan="2">Jumlah Total Mata Kuliah Wajib + Pilihan</td>
										<td><?php echo $jumlahMatkulWajibPilihan ?></td>
										<td colspan="2"></td>
									</tr>
								</tbody>
								<tfoot class="table head">
									<th >No</th>
									<th>Jenis Matakuliah</th>
									<th>SKS</th>
									<th>keterangan</th>
									<th>Edit</th>
								</tfoot>	
							</table>
						<?php
							if ($_SESSION['hak_akses'] == 'kaprodi' || $_SESSION['hak_akses'] == 'admin' || $_SESSION['hak_akses'] == 'standar 5'){
						?>	
							<div class="uploadFilePendukung">
								<p>Upload dokumen pendukung (optional):</p>
								<?php //echo $error;?>
								<?php echo form_open_multipart('standar5/doUpload_FilePendukung');?>
									<input class='browseFilePendukung' name="file_pendukung[]" type="file" size="4096"/>
									<input type="hidden" name="poinmateri" value="std5_5121">
									<input type="hidden" name="nav" value="standar5/kurikulum/5.1.1kompetensidan5.1.2sk">
									<button type='button' class="add_more">Add More Files</button><br />
									<label class="custom-btnUpload-filependukung">
										<i class="fa fa-cloud-upload"></i> Upload Dokumen
										<input type="submit" name="btn-upload" value="Upload Dokumen" class="btnUpload-filependukung"/>
									</label>
								</form>
							</div>
						<?php
							}
						?>			
							<?php 
							if($filependukung_5121 !== "kosong" & $filependukung_5121){
							?>
							<div class="teks-listfileupload">3 Daftar terakhir dokumen pendukung yang sudah diunggah</div>
							<div class="listfileupload">
								<table id="DaftarDokumenPendukung" class="display" width="100%" cellspacing="0">
									<thead class="table head">
										<th>Nama Dokumen Pendukung</th>
										<th>Tanggal Upload</th>
										<th>Fungsi</th>
									</thead>
									<tbody>
										<?php
										$dt = new DateTime();
										$tz = new DateTimeZone("Asia/Jakarta"); 
										foreach($filependukung_5121 as $row){
											$dt->setTimestamp($row["uploaddate"]);
											$dt->setTimezone($tz);
											$uploaddate = $dt->format("l, d F Y, H:i:s");
										?>
											<tr id="row<?php echo $row['id'];?>">
												<td id="namafile<?php echo $row['id'];?>"><?php echo $row['nama_file']; ?></td>
												<td id="uploaddate<?php echo $row['id'];?>"><?php echo $uploaddate ?></td>
												<td if="fungsi<?php echo $row['id'];?>">
												<a class="downloadFilePendukung" href="<?php echo base_url($row['filepath']) ?>"><i class="fa fa-download fa-lg" aria-hidden="true" style="margin-right: 10px;"></i></a>
										<?php
											if ($_SESSION['hak_akses'] == 'kaprodi' || $_SESSION['hak_akses'] == 'admin' || $_SESSION['hak_akses'] == 'standar 5'){
										?>
												<a class="deleteFilePendukung" data-name="<?php echo $row['nama_file']; ?>" href="../deleteFilePendukung?nav=standar5/kurikulum/5.1.1kompetensidan5.1.2sk&id=<?php echo $row['id'];?>&namafile=<?php echo $row['nama_file']; ?>"><i class="fa fa-trash fa-lg" aria-hidden="true"></i></a>
										<?php
											}
										?>		
												</td>
											</tr>
										<?php
										}
										?>
									</tbody>
								</table>	
							</div>
							<?php
							}
							?>
						</div>
					</div>
					
					<button class="accordion">5.1.2.2 Struktur kurikulum berdasarkan urutan Mata Kuliah (MK)</button>
					<div class="panel">
						<div class="panel-content">
							<?php
							if ($_SESSION['hak_akses'] == 'kaprodi' || $_SESSION['hak_akses'] == 'admin' || $_SESSION['hak_akses'] == 'standar 5'){
							?>
							<form action="../uploaddokumen_excel" method="post" enctype="multipart/form-data">
								<label for="files2" class="huruf-upload-files">Upload Files: </label>						
								<input required type="file" name="files[]" id="files2" onchange="cekTipeFile(2);" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" />
								<input type="hidden" name="table" value="std5_5122strukturkurikulummk">
								<input type="hidden" name="nav" value="standar5/kurikulum/5.1.1kompetensidan5.1.2sk">
								<label class="custom-btnUpload-filependukung">
									<i class="fa fa-cloud-upload"></i> UPLOAD
									<button class="upload-btn" id="btn-upload2" type="submit" name="btn-upload">UPLOAD</button>
								</label>
								<br />
								<label class="formatFileLbl" for="files2">*Format file .csv, .xls, .xlsx.</label><br/>
								<a href="../getTemplateExcel?poindata=5122" class="UploadTemplateExcel" target="_blank">*<i class="fa fa-download" aria-hidden="true"></i> Download template</a>
							</form>
							
							<br />
							<?php
							}
							?>
							<form action="../riwayat_uploadfile" method="post" target="_blank">
								<input type="hidden" name="table_riwayat" value="std5_5122strukturkurikulummk">
								<input type="hidden" name="info" value="5.1.2.2 Struktur kurikulum berdasarkan urutan Mata Kuliah (MK)">
								<button class="riwayat_btn" id="riwayat_btn" type="submit" name="riwayat_btn">Lihat riwayat 5 terakhir unggahan dokumen </button>
							</form>
							<br />
							<table id="table-view-data-5-1-2-2" class="display" width="100%" cellspacing="0">
								
								<thead class="table head">
									<tr>
										<th rowspan="2">No</th>
										<th rowspan="2">SMT</th>
										<th rowspan="2">Kode MK</th>
										<th rowspan="2">Nama Mata Kuliah</th>
										<th rowspan="2">Bobot SKS</th>
										<th colspan="2">SKS MK dalam Kurikulum</th>
										<th rowspan="2">Bobot Tugas</th>
										<th colspan="3">Kelengkapan</th>
										<th rowspan="2">Unit/Jur/Fak Penyelenggara</th>
										<th rowspan="2">Edit</th>	
									</tr>
									<tr>
										<th>Inti</th>
										<th>Institusional</th>
										<th>Deskripsi</th>
										<th>Silabus</th>
										<th>SAP</th>
									</tr>		
								</thead>
									
								
								<tbody class="table body">
									<?php
									if($result_set5122 !== "kosong" && $result_set5122){
										foreach($result_set5122 as $row){
										?>	
											<tr id="row5:<?php echo $row['no'] ?>">
												<td><textarea style="font-size:14px;border: none;" id="datatbl51:<?php echo $row['no'] ?>" name="tbl5" readonly type="textarea"><?php echo $row['no'] ?></textarea></td> 	
												<td><textarea style="font-size:14px;border: none;" id="datatbl52:<?php echo $row['no'] ?>" name="tbl5" readonly type="textarea"><?php echo $row['smt'] ?></textarea></td> 	
												<td><textarea style="font-size:14px;border: none;" id="datatbl53:<?php echo $row['no'] ?>" name="tbl5" readonly type="textarea"><?php echo $row['kode_mk'] ?></textarea></td>
												<td><textarea style="font-size:14px;border: none;" id="datatbl54:<?php echo $row['no'] ?>" name="tbl5" readonly type="textarea"><?php echo $row['nama_mata_kuliah'] ?></textarea></td>
												<td><textarea style="font-size:14px;border: none;" id="datatbl55:<?php echo $row['no'] ?>" name="tbl5" readonly type="textarea"><?php echo $row['bobot_sks'] ?></textarea></td> 	
												<td><textarea style="font-size:14px;border: none;" id="datatbl56:<?php echo $row['no'] ?>" name="tbl5" readonly type="textarea"><?php echo $row['sks_mk_dalam_kurikulum_inti'] ?></textarea></td>
												<td><textarea style="font-size:14px;border: none;" id="datatbl57:<?php echo $row['no'] ?>" name="tbl5" readonly type="textarea"><?php echo $row['sks_mk_dalam_kurikulum_Institusional'] ?></textarea></td>
												<td><textarea style="font-size:14px;border: none;" id="datatbl58:<?php echo $row['no'] ?>" name="tbl5" readonly type="textarea"><?php echo $row['bobot_tugas'] ?></textarea></td> 	
												<td><textarea style="font-size:14px;border: none;" id="datatbl59:<?php echo $row['no'] ?>" name="tbl5" readonly type="textarea"><?php echo $row['kelengkapan_deskripsi'] ?></textarea></td>
												<td><textarea style="font-size:14px;border: none;" id="datatbl510:<?php echo $row['no'] ?>" name="tbl5" readonly type="textarea"><?php echo $row['kelengkapan_silabus'] ?></textarea></td>
												<td><textarea style="font-size:14px;border: none;" id="datatbl511:<?php echo $row['no'] ?>" name="tbl5" readonly type="textarea"><?php echo $row['kelengkapan_sap'] ?></textarea></td> 	
												<td><textarea style="font-size:14px;border: none;" id="datatbl512:<?php echo $row['no'] ?>" name="tbl5" readonly type="textarea"><?php echo $row['unit_jur_fak_penyelenggara'] ?></textarea></td>
												<td><input style="display: block;" type="button" onclick="edit_row('5','<?php echo $row['no'] ?>', '12')" id="edit_btn5:<?php echo $row['no'] ?>" class="edit" value="edit">
												<input style="display: none;" type="button" onclick="save_row('5','<?php echo $row['no'] ?>', '12')" id="save_btn5:<?php echo $row['no'] ?>" class="save" value="save">
												<input type="hidden" id="table5" name="table5" value="std5_5122strukturkurikulummk">
												</td>
											</tr>
									<?php
										}
									}
									?>						
								</tbody>
								
								<tbody>
									<tr>
										<td colspan="4">Total SKS</td>
										<td><?php echo $jumlahBobotIntiInstitusional['bobotsks'] ?></td>
										<td><?php echo $jumlahBobotIntiInstitusional['inti'] ?></td>
										<td><?php echo $jumlahBobotIntiInstitusional['institusional'] ?></td>
										<td colspan="6"></td>
									</tr>
								</tbody>
								
								<tfoot class="table head">
									<tr>
										<th rowspan="2">No</th>
										<th rowspan="2">SMT</th>
										<th rowspan="2">Kode MK</th>
										<th rowspan="2">Nama Mata Kuliah</th>
										<th rowspan="2">Bobot SKS</th>
										<th colspan="2">SKS MK dalam Kurikulum</th>
										<th rowspan="2">Bobot Tugas</th>
										<th colspan="3">Kelengkapan</th>
										<th rowspan="2">Unit/Jur/Fak Penyelenggara</th>
										<th rowspan="2">Edit</th>	
									</tr>
									<tr>
										<th>Inti</th>
										<th>Institusional</th>
										<th>Deskripsi</th>
										<th>Silabus</th>
										<th>SAP</th>
									</tr>		
								</tfoot>	
							</table>
						<?php
							if ($_SESSION['hak_akses'] == 'kaprodi' || $_SESSION['hak_akses'] == 'admin' || $_SESSION['hak_akses'] == 'standar 5'){
						?>	
							<div class="uploadFilePendukung">
								<p>Upload dokumen pendukung (optional):</p>
								<?php //echo $error;?>
								<?php echo form_open_multipart('standar5/doUpload_FilePendukung');?>
									<input class='browseFilePendukung' name="file_pendukung[]" type="file" size="4096"/>
									<input type="hidden" name="poinmateri" value="std5_5122">
									<input type="hidden" name="nav" value="standar5/kurikulum/5.1.1kompetensidan5.1.2sk">
									<button type='button' class="add_more">Add More Files</button><br />
									<label class="custom-btnUpload-filependukung">
										<i class="fa fa-cloud-upload"></i> Upload Dokumen
										<input type="submit" name="btn-upload" value="Upload Dokumen" class="btnUpload-filependukung"/>
									</label>
								</form>
							</div>
						<?php
							}
						?>			
							<?php 
							if($filependukung_5122 !== "kosong" & $filependukung_5122){
							?>
							<div class="teks-listfileupload">3 Daftar terakhir dokumen pendukung yang sudah diunggah</div>
							<div class="listfileupload">
								<table id="DaftarDokumenPendukung" class="display" width="100%" cellspacing="0">
									<thead class="table head">
										<th>Nama Dokumen Pendukung</th>
										<th>Tanggal Upload</th>
										<th>Fungsi</th>
									</thead>
									<tbody>
										<?php
										$dt = new DateTime();
										$tz = new DateTimeZone("Asia/Jakarta"); 
										foreach($filependukung_5122 as $row){
											$dt->setTimestamp($row["uploaddate"]);
											$dt->setTimezone($tz);
											$uploaddate = $dt->format("l, d F Y, H:i:s");
										?>
											<tr id="row<?php echo $row['id'];?>">
												<td id="namafile<?php echo $row['id'];?>"><?php echo $row['nama_file']; ?></td>
												<td id="uploaddate<?php echo $row['id'];?>"><?php echo $uploaddate ?></td>
												<td if="fungsi<?php echo $row['id'];?>">
												<a class="downloadFilePendukung" href="<?php echo base_url($row['filepath']) ?>"><i class="fa fa-download fa-lg" aria-hidden="true" style="margin-right: 10px;"></i></a>
										<?php
											if ($_SESSION['hak_akses'] == 'kaprodi' || $_SESSION['hak_akses'] == 'admin' || $_SESSION['hak_akses'] == 'standar 5'){
										?>
												<a class="deleteFilePendukung" data-name="<?php echo $row['nama_file']; ?>" href="../deleteFilePendukung?nav=standar5/kurikulum/5.1.1kompetensidan5.1.2sk&id=<?php echo $row['id'];?>&namafile=<?php echo $row['nama_file']; ?>"><i class="fa fa-trash fa-lg" aria-hidden="true"></i></a>
										<?php
											}
										?>		
												</td>
											</tr>
										<?php
										}
										?>
									</tbody>
								</table>	
							</div>
							<?php
							}
							?>
						</div>
					</div>
					
				</div>	
			</div>
		</div>
		<script>
			$(document).ready(function(){
				var max_upload = 3;
				var x = 1;
				$('.add_more').click(function(e){
					e.preventDefault();
					if(x < max_upload){
						x++;
						$(this).before("<div><input class='browseFilePendukung' name='file_pendukung[]' type='file' size='40'/><a href='#' class='remove_field'>Remove</a></div>");
					} else {
						$(this).hide();
						$('.uploadFilePendukung').append('<p class="maksUploadFilePendukung">* Maksimal 3 upload dokumen pendukung</p>');
					}
				});
				$(this).on("click",".remove_field", function(e){ //user click on remove text
					e.preventDefault(); 
					$(this).parent('div').remove(); x--;
					$('.add_more').show();
					$('.maksUploadFilePendukung').hide();
				})
			});
		</script>
		<script>
		(function (){	
			$(document).ready(function(){
			var acc = document.getElementsByClassName("accordion");
			var i;

			for (i = 0; i < acc.length; i++) {
			  acc[i].onclick = function() {
				this.classList.toggle("active");
				var panel = this.nextElementSibling;
				if (panel.style.maxHeight){
				  panel.style.maxHeight = null;
				  panel.style.paddingBottom = null;
				} else {
				  panel.style.maxHeight = panel.scrollHeight += 5 + "px";
				  panel.style.paddingBottom = "18px";
				} 
			  }
			}
			});
		})(jQuery);	
		</script>
		
		<div class="footer">
			<?php
			include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/footer.php";
			?>
		</div>
		<script>
			$(document).on('click', '.deleteFilePendukung', function(e){
				e.preventDefault();
				var nama_file = $(this).attr('data-name');
				var choice = confirm("Are you sure you want to delete file '" + nama_file + "'?");
				if (choice) {
					window.location.href =  $(this).attr('href');
				}
			});	
		</script>
		
		<script type="text/javascript">
			var editor1 = CKEDITOR.replace( 'editor1', {
				height: 400
			});
			var editor2 = CKEDITOR.replace( 'editor2', {
				height: 400
			});
			var editor3 = CKEDITOR.replace( 'editor3', {
				height: 300
			});
		</script>
		<script type = 'text/javascript' >
		(function (){	
			$(document).ready(function() {
				CKEDITOR.config.readOnly = true;
			});	
		})(jQuery);	
		</script>
		<script>
		function edit_txtarea(ocno, no, nocolumn)
		{
			document.getElementById("editTxt_btn"+ocno).style.display="none";
			document.getElementById("saveTxt_btn"+ocno).style.display="inline";

			var txtarea = document.getElementById("editor"+ocno);
			var tugas_data = CKEDITOR.instances['editor'+ocno].getData();
			CKEDITOR.instances['editor'+ocno].setReadOnly(false);
			
		}
		</script>
		<script>
			function save_txtarea(ocno, no, nocolumn)
			{
				var table = document.getElementById("table"+ocno).value;
				var textarea_val = CKEDITOR.instances['editor'+ocno].getData();
				var columns = [];
				var columns_data = {};
				document.getElementById("editor"+ocno).innerHTML = textarea_val;
				var i = 1;
				while(i<=nocolumn){
					columns[i] = document.getElementById("columns"+ocno+i).value;
					columns_data[i] = columns[i];
					i++;
				}
				
				$.ajax
				 ({
				  type:'post',
				  url:'../edit_ckeditor',
				  data:{
				   table:table,
				   columns_data,
				   row_no:no,
				   textarea_val:textarea_val
				  },
				  success:function(response) {
				   if(response == "success")
				   {
					document.getElementById("editTxt_btn"+ocno).style.display="inline";
					document.getElementById("saveTxt_btn"+ocno).style.display="none";
					document.getElementById("editor"+ocno).innerHTML=textarea_val;
					CKEDITOR.instances['editor'+ocno].setReadOnly(true);
				   }
				  }
				 });
			}
		</script>
		
		
		<script>
			function edit_row(ocid, id, txtareaamount)
			{				
				document.getElementById("edit_btn"+ocid+":"+id).style.display="none";
				document.getElementById("save_btn"+ocid+":"+id).style.display="block";
				var i = 1;
				var j = 1;
				var datatbl = [];
				for(i,j;i<=txtareaamount,j<=txtareaamount; i++,j++){
					datatbl[i] = document.getElementById("datatbl"+ocid+i+":"+id).innerHTML;
					document.getElementById("datatbl"+ocid+i+":"+id).removeAttribute('readonly');
					document.getElementById("datatbl"+ocid+i+":"+id).style.outline ="thin solid #bfbfbf";									 
					document.getElementById("datatbl"+ocid+i+":"+id).style.background ="#f2f2f2";
				}
			}
		</script>
		<script>
			function save_row(ocid, id, txtareaamount)
			{
				var table = document.getElementById("table"+ocid).value;
				var row_no = document.getElementById("row"+ocid+":"+id).value;
				var i = 1;
				var j = 1;
				var datatbl = [];
				var datatbl_data = [];
				var datatbl_val = {};
				while(i<=txtareaamount){
					datatbl[i] = document.getElementById("datatbl"+ocid+i+":"+id).value;
					datatbl_val[i] = datatbl[i];
					i++;
				}
				
				$.ajax
				({
					type:'post',
					url:'../edittabel',
					data:{
					edit_row:table,
					row_no:id,
					columns:txtareaamount,
					datatbl_val

					},
					success:function(response) {
						var i = 1;
						if(response == "success")
						{
							while(i<=txtareaamount){
								datatbl[i] = document.getElementById("datatbl"+ocid+i+":"+id).value;
								document.getElementById("edit_btn"+ocid+":"+id).style.display="block";
								document.getElementById("save_btn"+ocid+":"+id).style.display="none";
								document.getElementById("datatbl"+ocid+i+":"+id).readOnly =true;
								document.getElementById("datatbl"+ocid+i+":"+id).style.outline ="none";
								document.getElementById("datatbl"+ocid+i+":"+id).style.background ="transparent";
								i++;
							}
						}
					}
				}); 
			}
		</script>
	</body>

	
	
</html>
