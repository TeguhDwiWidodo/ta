<?php

require $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/Classes/PHPExcel/IOFactory.php";

if(isset($_POST['btn-upload'])){  
	if(isset($_FILES['files'])){
		$nama = $_SESSION['nama'];
		$iduser = $_SESSION['id_user'];
		$nav = $_POST['nav'];
		foreach($_FILES['files']['tmp_name'] as $key => $tmp_name) {
			$table = $_POST['table'];
			$saveDate = time();
			$rawFileName = $_FILES['files']['name'][$key];
			$inputfilename = str_replace(' ','',$_FILES['files']['name'][$key]);
			$rawBaseName = pathinfo($inputfilename, PATHINFO_FILENAME );
			$extension = pathinfo($inputfilename, PATHINFO_EXTENSION );
			$folder = "uploads/dokumenpendukungstd5/".$table."/";
			
			$counter = 0;
			if (!file_exists($folder)) {
				mkdir($folder, 0777, true);
			}
			$inputfilename = '[Versi'.$counter.']'.$rawBaseName . '.' . $extension;
			while(file_exists($folder.$inputfilename)) {
				$inputfilename = '[Versi'.$counter.']'.$rawBaseName . '.' . $extension;
				$counter++;
			};
			$excelhtml_path = str_replace(' ','',$folder.$inputfilename.'.html');
			$excel_path = str_replace(' ','',$folder.$inputfilename);
			if(move_uploaded_file($tmp_name,$folder.$inputfilename)) {
				try {
					$inputfiletype = PHPExcel_IOFactory::identify($folder.$inputfilename);
					$objReader = PHPExcel_IOFactory::createReader($inputfiletype);
					$objPHPExcel = $objReader->load($folder.$inputfilename);
					$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'HTML');
					$objWriter->save($folder.$inputfilename.'.html');
				} catch(Exception $e) {
					die('Error loading file "'.pathinfo($folder.$inputfilename,PATHINFO_BASENAME).'": '.$e->getMessage());
				}
				$sheet = $objPHPExcel->getSheet(0); 
				$highestRow = $sheet->getHighestRow(); 
				$highestColumn = $sheet->getHighestColumn();
				$headings = $sheet->rangeToArray('A1:' . $highestColumn . 1, NULL, TRUE, FALSE);
				$lowercaseheadings = array_map('strtolower', $headings[0]);
				$updatedheadings = str_replace(' ','_', $lowercaseheadings);
				$updatedheadings = str_replace('/','_', $updatedheadings);
				$updatedheadings = str_replace('-','_', $updatedheadings);
				for ($row = 2; $row <= $highestRow; $row++){ 
					//  Read a row of data into an array
					$rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
					//  Insert row data array into your database of choice here
					if($highestColumn == 'C' ){
						$sql = "INSERT INTO $table (".$updatedheadings[0].", ".$updatedheadings[1].", ".$updatedheadings[2].") 
						VALUES ('".$rowData[0][0]."', '".$rowData[0][1]."', '".$rowData[0][2]."') 
						ON DUPLICATE KEY UPDATE ".$updatedheadings[0]."='".$rowData[0][0]."', ".$updatedheadings[1]."='".$rowData[0][1]."', ".$updatedheadings[2]."='".$rowData[0][2]."';";
					} else if($highestColumn == 'D' ){
						$sql = "INSERT INTO $table (".$updatedheadings[0].", ".$updatedheadings[1].", ".$updatedheadings[2].", ".$updatedheadings[3].") 
						VALUES ('".$rowData[0][0]."', '".$rowData[0][1]."', '".$rowData[0][2]."', '".$rowData[0][3]."') 
						ON DUPLICATE KEY UPDATE ".$updatedheadings[0]."='".$rowData[0][0]."', ".$updatedheadings[1]."='".$rowData[0][1]."', ".$updatedheadings[2]."='".$rowData[0][2]."', ".$updatedheadings[3]."='".$rowData[0][3]."';";
					} else if($highestColumn == 'E'){
						$sql = "INSERT INTO $table (".$updatedheadings[0].", ".$updatedheadings[1].", ".$updatedheadings[2].", ".$updatedheadings[3].", ".$updatedheadings[4].") 
						VALUES ('".$rowData[0][0]."', '".$rowData[0][1]."', '".$rowData[0][2]."', '".$rowData[0][3]."', '".$rowData[0][4]."') 
						ON DUPLICATE KEY UPDATE ".$updatedheadings[0]."='".$rowData[0][0]."', ".$updatedheadings[1]."='".$rowData[0][1]."', ".$updatedheadings[2]."='".$rowData[0][2]."', 
						".$updatedheadings[3]."='".$rowData[0][3]."', ".$updatedheadings[4]."='".$rowData[0][4]."';";
					} else if($highestColumn == 'F'){
						$sql = "INSERT INTO $table (".$updatedheadings[0].", ".$updatedheadings[1].", ".$updatedheadings[2].", ".$updatedheadings[3].", ".$updatedheadings[4].", ".$updatedheadings[5].") 
						VALUES ('".$rowData[0][0]."', '".$rowData[0][1]."', '".$rowData[0][2]."', '".$rowData[0][3]."', '".$rowData[0][4]."', '".$rowData[0][5]."') 
						ON DUPLICATE KEY UPDATE ".$updatedheadings[0]."='".$rowData[0][0]."', ".$updatedheadings[1]."='".$rowData[0][1]."', ".$updatedheadings[2]."='".$rowData[0][2]."', 
						".$updatedheadings[3]."='".$rowData[0][3]."', ".$updatedheadings[4]."='".$rowData[0][4]."', ".$updatedheadings[5]."='".$rowData[0][5]."';";
					} else if($highestColumn == 'H'){
						$sql = "INSERT INTO $table (".$updatedheadings[0].", ".$updatedheadings[1].", ".$updatedheadings[2].", ".$updatedheadings[3].", ".$updatedheadings[4].",
						".$updatedheadings[5].", ".$updatedheadings[6].", ".$updatedheadings[7].") 
						VALUES ('".$rowData[0][0]."', '".$rowData[0][1]."', '".$rowData[0][2]."', '".$rowData[0][3]."', '".$rowData[0][4]."', 
						'".$rowData[0][5]."', '".$rowData[0][6]."', '".$rowData[0][7]."') 
						ON DUPLICATE KEY UPDATE ".$updatedheadings[0]."='".$rowData[0][0]."', ".$updatedheadings[1]."='".$rowData[0][1]."', ".$updatedheadings[2]."='".$rowData[0][2]."', ".$updatedheadings[3]."='".$rowData[0][3]."', 
						".$updatedheadings[4]."='".$rowData[0][4]."', ".$updatedheadings[5]."='".$rowData[0][5]."', ".$updatedheadings[6]."='".$rowData[0][6]."', ".$updatedheadings[7]."='".$rowData[0][7]."';";
					} else if($highestColumn == 'I'){
						$sql = "INSERT INTO $table (".$updatedheadings[0].", ".$updatedheadings[1].", ".$updatedheadings[2].", ".$updatedheadings[3].", ".$updatedheadings[4].",
						".$updatedheadings[5].", ".$updatedheadings[6].", ".$updatedheadings[7].", ".$updatedheadings[8].") 
						VALUES ('".$rowData[0][0]."', '".$rowData[0][1]."', '".$rowData[0][2]."', '".$rowData[0][3]."', '".$rowData[0][4]."', 
						'".$rowData[0][5]."', '".$rowData[0][6]."', '".$rowData[0][7]."', '".$rowData[0][8]."') 
						ON DUPLICATE KEY UPDATE ".$updatedheadings[0]."='".$rowData[0][0]."', ".$updatedheadings[1]."='".$rowData[0][1]."', ".$updatedheadings[2]."='".$rowData[0][2]."', ".$updatedheadings[3]."='".$rowData[0][3]."', 
						".$updatedheadings[4]."='".$rowData[0][4]."', ".$updatedheadings[5]."='".$rowData[0][5]."', ".$updatedheadings[6]."='".$rowData[0][6]."', ".$updatedheadings[7]."='".$rowData[0][7]."', 
						".$updatedheadings[8]."='".$rowData[0][8]."';";	
					} else if($highestColumn == 'L'){
						$sql = "INSERT INTO $table (".$updatedheadings[0].", ".$updatedheadings[1].", ".$updatedheadings[2].", ".$updatedheadings[3].", ".$updatedheadings[4].",
						".$updatedheadings[5].", ".$updatedheadings[6].", ".$updatedheadings[7].", ".$updatedheadings[8].", ".$updatedheadings[9].", ".$updatedheadings[10].", 
						".$updatedheadings[11].") 
						VALUES ('".$rowData[0][0]."', '".$rowData[0][1]."', '".$rowData[0][2]."', '".$rowData[0][3]."', '".$rowData[0][4]."', 
						'".$rowData[0][5]."', '".$rowData[0][6]."', '".$rowData[0][7]."', '".$rowData[0][8]."', '".$rowData[0][9]."', '".$rowData[0][10]."', '".$rowData[0][11]."'') 
						ON DUPLICATE KEY UPDATE ".$updatedheadings[0]."='".$rowData[0][0]."', ".$updatedheadings[1]."='".$rowData[0][1]."', ".$updatedheadings[2]."='".$rowData[0][2]."', ".$updatedheadings[3]."='".$rowData[0][3]."', 
						".$updatedheadings[4]."='".$rowData[0][4]."', ".$updatedheadings[5]."='".$rowData[0][5]."', ".$updatedheadings[6]."='".$rowData[0][6]."', ".$updatedheadings[7]."='".$rowData[0][7]."', 
						".$updatedheadings[8]."='".$rowData[0][8]."', ".$updatedheadings[9]."='".$rowData[0][9]."', ".$updatedheadings[10]."='".$rowData[0][10]."';";
					}
					$this->db->query($sql);	
				}
				$sql2 = "INSERT INTO std5_history_uploadfile (nama_file, tabel_db, tanggal, oleh, id_user, excel_path, excelhtml_path)
				VALUES ('$inputfilename','$table','$saveDate', '$nama', '$iduser', '$excel_path', '$excelhtml_path');";
				$this->db->query($sql2);
				
				$_SESSION['message'] = 'Dokumen '.$rawFileName.' Berhasil diunggah.';
				header("Location: ../$nav?success");
			} else {
					$_SESSION['message'] = 'Dokumen '.$rawFileName.' Gagal diunggah. Cek ulang format, ukuran, dan template dokumen.';
					header("Location: ../$nav?fail");
			}
			
			
			
		}
	} else {
		$_SESSION['message'] = 'Tidak ada dokumen yang diupload.';
		header("Location: ../$nav?fail");
	}
}