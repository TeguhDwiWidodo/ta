<?php
if(!isset($_SESSION['nama'])){
      header("location:" . base_url());
      exit();
   }	
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Akreditasi | Standar 5 | Kurikulum | 5.1.3 dan 5.1.4</title>
		<script type = 'text/javascript' src="<?php echo base_url(); ?>js/jquery-1.12.4.js"></script>		
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/mainlayout.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/accordion.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/jquery.dataTables.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/data&dokumenstd5.css">
		<!--<link rel="stylesheet" href="<?php echo base_url(); ?>css/dokumenpendukung.css">-->
		<!--<link rel="stylesheet" href="<?php echo base_url(); ?>font-awesome-4.6.3/css/font-awesome.min.css">-->
		<script type = 'text/javascript' src="<?php echo base_url(); ?>js/jquery.dataTables.min.js"></script>
		<script type = 'text/javascript' src="<?php echo base_url(); ?>assets/autosize-master/dist/autosize.js"></script>
		<script type = 'text/javascript' >
		(function ($){	
			$(document).ready(function() {
				$('table').dataTable({
					responsive: true,
					"scrollX": true,
					"scrollY": true,
					pageResize: true,
					autoWidth: true,
					columnDefs: [
						{ width: 10, targets: 0 }
					]
					
				});
			});	
		})(jQuery);	
		</script>
		<script>
		(function(){
			$(document).ready(function(){
				autosize($('textarea'));   
			});
		})(jQuery);	
		</script>
		<script>
		(function(){
			$(document).ready(function(){
				$("#btn-messageSukses").click(function(){
					$("#messageSukses").hide(1000);  
					$("#btn-messageSukses").hide(1000);  
				});
			});
		})(jQuery);	
		</script>
		<script>
			function cekTipeFile(num) { 
				var id_value = document.getElementById('files'+num).value;
		
				if(id_value != '') { 
					var valid_extensions = /(.csv|.xls|.xlsx)$/i;   
					if(valid_extensions.test(id_value))
					{ 
						document.getElementById('btn-upload'+num).disabled = false;	
					} else {
						document.getElementById('btn-upload'+num).disabled = true;	
						alert("Tidak dapat mengunggah dokumen. Format file harus .csv/.xls/.xlsx");
					}
				} 
			}
		</script>
	</head>
	
	<body>
		<div class="header">
			<h1><a href="<?php echo base_url(); ?>dashboard">Akreditasi SI</a></h1>
			<?php
				include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/header.php";
			?>
		</div>
		
		<div class="sidebar">
			<?php
				include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/sidebar.php";
			?>
		</div>
			
		<div class="main-layout">
			<div class="main-content">
			<div class="menu-breadcrumb">
				<ul class="breadcrumb">
					<li><a href="<?php echo base_url(); ?>dashboard" id="menu-breadcrumb">Dashboard</a></li>
					<li><a href="<?php echo base_url(); ?>standar5/kurikulum" id="menu-breadcrumb">Kurikulum</a></li>
					<li>5.1 Kurikulum</li>
				</ul>
			</div>
			 <div class="menu-head">
				<ul>
					<li><a href="<?php echo base_url(); ?>standar5/kurikulum" id="menu-head" style="background:#d9d9d9;color:black;">Kurikulum</a></li>
					<li><a href="<?php echo base_url(); ?>standar5/pembelajaran" id="menu-head">Pembelajaran</a></li>
					<li><a href="<?php echo base_url(); ?>standar5/sa" id="menu-head">Suasana Akademik</a></li>
				</ul>
			</div>
			<div class="menu-subhead">
				<ul>
					<li><a href="<?php echo base_url(); ?>standar5/kurikulum/5.1.1kompetensidan5.1.2sk" id="menu-subhead" >5.1.1 Kompetensi &amp; 5.1.2 Struktur Kurikulum</a></li>
					<li><a href="<?php echo base_url(); ?>standar5/kurikulum/5.1.3dan5.1.4matakuliah" id="menu-subhead" style="background:#404040;color:white;">5.1.3 Mata kuliah yang dilaksanakan &amp; 5.1.4 Substansi Praktikum yang mandiri</a></li>
					<li><a href="<?php echo base_url(); ?>standar5/kurikulum/5.2peninjauan" id="menu-subhead">5.2 Peninjauan Kurikulum dalam 5 Tahun Terakhir </a></li>
				</ul>
			</div>
			
			<div class="sub-header">
				<h2>5.1 Kurikulum</h2>
			</div>
			<div class="main-content">
			
			<?php
				if (isset($_SESSION['message'])) {
			?>		
					<div class="messageSukses" id="messageSukses">
					<button type="button" id="btn-messageSukses" class="btn-messageSukses"><?php echo $_SESSION['message']; ?></button>		
					</div>	
			<?php		
					unset($_SESSION['message']);
				}
			?>
					
				<div class="content">
				
					<h4>5.1.3 Mata kuliah pilihan yang dilaksanakan dalam tiga tahun terakhir</h3>
					<button class="accordion">5.1.3 Mata kuliah pilihan yang dilaksanakan dalam tiga tahun terakhir</button>
					<div class="panel">
						<div class="panel-content">
							<?php
							if ($_SESSION['hak_akses'] == 'kaprodi' || $_SESSION['hak_akses'] == 'admin' || $_SESSION['hak_akses'] == 'standar 5'){
							?>
							<form action="../uploaddokumen_excel" method="post" enctype="multipart/form-data">
								<label for="files3" class="huruf-upload-files">Upload Files: </label>
								<input required type="file" name="files[]" id="files3" onchange="cekTipeFile(3);" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" />
								<input type="hidden" name="table" value="std5_513matkulpilihan">
								<input type="hidden" name="nav" value="standar5/kurikulum/5.1kurikulum">
								<label class="custom-btnUpload-filependukung">
									<i class="fa fa-cloud-upload"></i> UPLOAD
									<button class="upload-btn" id="btn-upload3" type="submit" name="btn-upload">UPLOAD</button>
								</label>	
								<br />
								<label class="formatFileLbl" for="files3">*Format file .csv, .xls, .xlsx.</label><br/>
								<a href="../getTemplateExcel?poindata=513" class="UploadTemplateExcel" target="_blank">*<i class="fa fa-download" aria-hidden="true"></i> Download template</a>
							</form>
							
							<br />
							<?php
							}
							?>
							<form action="../riwayat_uploadfile" method="post" target="_blank">
								<input type="hidden" name="table_riwayat" value="std5_513matkulpilihan">
								<input type="hidden" name="info" value="5.1.3 Mata kuliah pilihan yang dilaksanakan dalam tiga tahun terakhir">
								<button class="riwayat_btn" id="riwayat_btn" type="submit" name="riwayat_btn">Lihat riwayat 5 terakhir unggahan dokumen </button>
							</form>
							<br />
							<table id="table-view-data-5-1-3" class="display" width="100%" cellspacing="0">
								
								<thead class="table head">
									<th>No</th>
									<th>Semester</th>
									<th>Kode MK</th>
									<th>Nama MK (Pilihan)</th>
									<th>Bobot SKS</th>
									<th>Bobot Tugas</th>
									<th>Unit/Jur/Fak Pengelola</th>
									<th>Kelompok MK (Pilihan)</th>
									<th>Edit</th>
								</thead>
									
								
								<tbody class="table body">
									<?php
									if($result_set513 !== "kosong" && $result_set513){
										foreach($result_set513 as $row){
										?>	
											<tr id="row6:<?php echo $row['no'] ?>">
												<td><textarea style="font-size:14px;border: none;" id="datatbl61:<?php echo $row['no'] ?>" name="tbl6" readonly type="textarea"><?php echo $row['no'] ?></textarea></td> 	
												<td><textarea style="font-size:14px;border: none;" id="datatbl62:<?php echo $row['no'] ?>" name="tbl6" readonly type="textarea"><?php echo $row['semester'] ?></textarea></td>
												<td><textarea style="font-size:14px;border: none;" id="datatbl63:<?php echo $row['no'] ?>" name="tbl6" readonly type="textarea"><?php echo $row['kode_mk'] ?></textarea></td>
												<td><textarea style="font-size:14px;border: none;" id="datatbl64:<?php echo $row['no'] ?>" name="tbl6" readonly type="textarea"><?php echo $row['nama_mk_pilihan'] ?></textarea></td>
												<td><textarea style="font-size:14px;border: none;" id="datatbl65:<?php echo $row['no'] ?>" name="tbl6" readonly type="textarea"><?php echo $row['bobot_sks'] ?></textarea></td> 	
												<td><textarea style="font-size:14px;border: none;" id="datatbl66:<?php echo $row['no'] ?>" name="tbl6" readonly type="textarea"><?php echo $row['bobot_tugas'] ?></textarea></td>
												<td><textarea style="font-size:14px;border: none;" id="datatbl67:<?php echo $row['no'] ?>" name="tbl6" readonly type="textarea"><?php echo $row['unit_jur_fak_pengelola'] ?></textarea></td>
												<td><textarea style="font-size:14px;border: none;" id="datatbl68:<?php echo $row['no'] ?>" name="tbl6" readonly type="textarea"><?php echo $row['kelompok_mk_pilihan'] ?></textarea></td>
												<td><input style="display: block;" type="button" onclick="edit_row('6','<?php echo $row['no'] ?>', '8')" id="edit_btn6:<?php echo $row['no'] ?>" class="edit" value="edit">
												<input style="display: none;" type="button" onclick="save_row('6','<?php echo $row['no'] ?>', '8')" id="save_btn6:<?php echo $row['no'] ?>" class="save" value="save">
												<input type="hidden" id="table6" name="table6" value="std5_513matkulpilihan">
												</td>
											</tr>
									<?php
										}
									}
									?>						
								</tbody>
								<tbody>
									<tr>
										<td colspan="4">Total SKS</td>
										<td><?php echo $jumlahSksMkPilihan ?></td>
										<td colspan="4"></td>
									</tr>
								</tbody>
								<tfoot class="table head">
									<th>No</th>
									<th>Semester</th>
									<th>Kode MK</th>
									<th>Nama MK (Pilihan)</th>
									<th>Bobot SKS</th>
									<th>Bobot Tugas</th>
									<th>Unit/Jur/Fak Pengelola</th>
									<th>Kelompok MK (Pilihan)</th>
									<th>Edit</th>
								</tfoot>	
							</table>
						<?php
							if ($_SESSION['hak_akses'] == 'kaprodi' || $_SESSION['hak_akses'] == 'admin' || $_SESSION['hak_akses'] == 'standar 5'){
						?>	
							<div class="uploadFilePendukung">
								<p>Upload dokumen pendukung (optional):</p>
								<?php //echo $error;?>
								<?php echo form_open_multipart('standar5/doUpload_FilePendukung');?>
									<input class='browseFilePendukung' name="file_pendukung[]" type="file" size="4096"/>
									<input type="hidden" name="poinmateri" value="std5_513">
									<input type="hidden" name="nav" value="standar5/kurikulum/5.1.3dan5.1.4matakuliah">
									<button type='button' class="add_more">Add More Files</button><br />
									<label class="custom-btnUpload-filependukung">
										<i class="fa fa-cloud-upload"></i> Upload Dokumen
										<input type="submit" name="btn-upload" value="Upload Dokumen" class="btnUpload-filependukung"/>
									</label>
								</form>
							</div>
						<?php
							}
						?>			
							<?php 
							if($filependukung_513 !== "kosong" & $filependukung_513){
							?>
							<div class="teks-listfileupload">3 Daftar terakhir dokumen pendukung yang sudah diunggah</div>
							<div class="listfileupload">
								<table id="DaftarDokumenPendukung" class="display" width="100%" cellspacing="0">
									<thead class="table head">
										<th>Nama Dokumen Pendukung</th>
										<th>Tanggal Upload</th>
										<th>Fungsi</th>
									</thead>
									<tbody>
										<?php
										$dt = new DateTime();
										$tz = new DateTimeZone("Asia/Jakarta"); 
										foreach($filependukung_513 as $row){
											$dt->setTimestamp($row["uploaddate"]);
											$dt->setTimezone($tz);
											$uploaddate = $dt->format("l, d F Y, H:i:s");
										?>
											<tr id="row<?php echo $row['id'];?>">
												<td id="namafile<?php echo $row['id'];?>"><?php echo $row['nama_file']; ?></td>
												<td id="uploaddate<?php echo $row['id'];?>"><?php echo $uploaddate ?></td>
												<td if="fungsi<?php echo $row['id'];?>">
												<a class="downloadFilePendukung" href="<?php echo base_url($row['filepath']) ?>"><i class="fa fa-download fa-lg" aria-hidden="true" style="margin-right: 10px;"></i></a>
										<?php
											if ($_SESSION['hak_akses'] == 'kaprodi' || $_SESSION['hak_akses'] == 'admin' || $_SESSION['hak_akses'] == 'standar 5'){
										?>
												<a class="deleteFilePendukung" data-name="<?php echo $row['nama_file']; ?>" href="../deleteFilePendukung?nav=standar5/kurikulum/5.1.3dan5.1.4matakuliah&id=<?php echo $row['id'];?>&namafile=<?php echo $row['nama_file']; ?>"><i class="fa fa-trash fa-lg" aria-hidden="true"></i></a>
										<?php
											}
										?>		
												</td>
											</tr>
										<?php
										}
										?>
									</tbody>
								</table>	
							</div>
							<?php
							}
							?>
						</div>
					</div>
					
					<br />
					<h4>5.1.4 Substansi praktikum/praktek yang mandiri ataupun yang merupakan bagian dari mata kuliah tertentu</h3>
					<button class="accordion">5.1.4 Substansi praktikum/praktek yang mandiri ataupun yang merupakan bagian dari mata kuliah tertentu</button>
					<div class="panel">
						<div class="panel-content">
							<?php
							if ($_SESSION['hak_akses'] == 'kaprodi' || $_SESSION['hak_akses'] == 'admin' || $_SESSION['hak_akses'] == 'standar 5'){
							?>
							<form action="../uploaddokumen_excel" method="post" enctype="multipart/form-data">
								<label for="files4" class="huruf-upload-files">Upload Files: </label>
								<input required type="file" name="files[]" id="files4" onchange="cekTipeFile(4);" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" />
								<input type="hidden" name="table" value="std5_514substansipraktikum">
								<input type="hidden" name="nav" value="standar5/kurikulum/5.1kurikulum">
								<label class="custom-btnUpload-filependukung">
									<i class="fa fa-cloud-upload"></i> UPLOAD
									<button class="upload-btn" id="btn-upload4" type="submit" name="btn-upload">UPLOAD</button>
								</label>	
								<br />
								<label class="formatFileLbl" for="files4">*Format file .csv, .xls, .xlsx.</label><br/>
								<a href="../getTemplateExcel?poindata=514" class="UploadTemplateExcel" target="_blank">*<i class="fa fa-download" aria-hidden="true"></i> Download template</a>
							</form>
							
							<br />
							<?php
							}
							?>
							<form action="../riwayat_uploadfile" method="post" target="_blank">
								<input type="hidden" name="table_riwayat" value="std5_514substansipraktikum">
								<input type="hidden" name="info" value="5.1.4 Substansi praktikum/praktek yang mandiri ataupun yang merupakan bagian dari mata kuliah tertentu">
								<button class="riwayat_btn" id="riwayat_btn" type="submit" name="riwayat_btn">Lihat riwayat 5 terakhir unggahan dokumen </button>
							</form>
							<br />
							<table id="table-view-data-5-1-4" class="display" width="100%" cellspacing="0">
								
								<thead class="table head">
									<th>No</th>
									<th>Nama Praktikum/Praktek</th>
									<th>Isi Praktikum/Praktek Judul/Modul</th>
									<th>Isi Praktikum/Praktek Jam Pelaksanaan</th>
									<th>Tempat/Lokasi Praktikum/Praktek</th>
									<th>Edit</th>
								</thead>
									
								
								<tbody class="table body">
									<?php
									if($result_set514 !== "kosong" && $result_set514){
										foreach($result_set514 as $row){
										?>	
											<tr id="row7:<?php echo $row['no'] ?>">
												<td><textarea style="font-size:14px;border: none;" id="datatbl71:<?php echo $row['no'] ?>" name="tbl6" readonly type="textarea"><?php echo $row['no'] ?></textarea></td> 	
												<td><textarea style="font-size:14px;border: none;" id="datatbl72:<?php echo $row['no'] ?>" name="tbl6" readonly type="textarea"><?php echo $row['nama_praktikum_praktek'] ?></textarea></td>
												<td><textarea style="font-size:14px;border: none;" id="datatbl73:<?php echo $row['no'] ?>" name="tbl6" readonly type="textarea"><?php echo $row['isi_praktikum_praktek_judul_modul'] ?></textarea></td>
												<td><textarea style="font-size:14px;border: none;" id="datatbl74:<?php echo $row['no'] ?>" name="tbl6" readonly type="textarea"><?php echo $row['isi_praktikum_praktek_jam_pelaksanaan'] ?></textarea></td>
												<td><textarea style="font-size:14px;border: none;" id="datatbl75:<?php echo $row['no'] ?>" name="tbl6" readonly type="textarea"><?php echo $row['tempat_lokasi_praktikum_praktek'] ?></textarea></td> 
												<td><input style="display: block;" type="button" onclick="edit_row('7','<?php echo $row['no'] ?>', '5')" id="edit_btn7:<?php echo $row['no'] ?>" class="edit" value="edit">
												<input style="display: none;" type="button" onclick="save_row('7','<?php echo $row['no'] ?>', '5')" id="save_btn7:<?php echo $row['no'] ?>" class="save" value="save">
												<input type="hidden" id="table7" name="table7" value="std5_514substansipraktikum">
												</td>
											</tr>
									<?php
										}
									}
									?>						
								</tbody>
								<tfoot class="table head">
									<th>No</th>
									<th>Nama Praktikum/Praktek</th>
									<th>Isi Praktikum/Praktek Judul/Modul</th>
									<th>Isi Praktikum/Praktek Jam Pelaksanaan</th>
									<th>Tempat/Lokasi Praktikum/Praktek</th>
									<th>Edit</th>
								</tfoot>	
							</table>
						<?php
							if ($_SESSION['hak_akses'] == 'kaprodi' || $_SESSION['hak_akses'] == 'admin' || $_SESSION['hak_akses'] == 'standar 5'){
						?>	
							<div class="uploadFilePendukung">
								<p>Upload dokumen pendukung (optional):</p>
								<?php //echo $error;?>
								<?php echo form_open_multipart('standar5/doUpload_FilePendukung');?>
									<input class='browseFilePendukung' name="file_pendukung[]" type="file" size="4096"/>
									<input type="hidden" name="poinmateri" value="std5_514">
									<input type="hidden" name="nav" value="standar5/kurikulum/5.1.3dan5.1.4matakuliah">
									<button type='button' class="add_more">Add More Files</button><br />
									<label class="custom-btnUpload-filependukung">
										<i class="fa fa-cloud-upload"></i> Upload Dokumen
										<input type="submit" name="btn-upload" value="Upload Dokumen" class="btnUpload-filependukung"/>
									</label>
								</form>
								</div>
						<?php
							}
						?>			
								<?php 
								if($filependukung_514 !== "kosong" & $filependukung_514){
								?>
								<div class="teks-listfileupload">3 Daftar terakhir dokumen pendukung yang sudah diunggah</div>
								<div class="listfileupload">
									<table id="DaftarDokumenPendukung" class="display" width="100%" cellspacing="0">
										<thead class="table head">
											<th>Nama Dokumen Pendukung</th>
											<th>Tanggal Upload</th>
											<th>Fungsi</th>
										</thead>
										<tbody>
											<?php
											$dt = new DateTime();
											$tz = new DateTimeZone("Asia/Jakarta"); 
											foreach($filependukung_514 as $row){
												$dt->setTimestamp($row["uploaddate"]);
												$dt->setTimezone($tz);
												$uploaddate = $dt->format("l, d F Y, H:i:s");
											?>
												<tr id="row<?php echo $row['id'];?>">
													<td id="namafile<?php echo $row['id'];?>"><?php echo $row['nama_file']; ?></td>
													<td id="uploaddate<?php echo $row['id'];?>"><?php echo $uploaddate ?></td>
													<td if="fungsi<?php echo $row['id'];?>">
													<a class="downloadFilePendukung" href="<?php echo base_url($row['filepath']) ?>"><i class="fa fa-download fa-lg" aria-hidden="true" style="margin-right: 10px;"></i></a>
											<?php
												if ($_SESSION['hak_akses'] == 'kaprodi' || $_SESSION['hak_akses'] == 'admin' || $_SESSION['hak_akses'] == 'standar 5'){
											?>
													<a class="deleteFilePendukung" data-name="<?php echo $row['nama_file']; ?>" href="../deleteFilePendukung?nav=5.1.3dan5.1.4matakuliahk&id=<?php echo $row['id'];?>&namafile=<?php echo $row['nama_file']; ?>"><i class="fa fa-trash fa-lg" aria-hidden="true"></i></a>
											<?php
												}
											?>		
													</td>
												</tr>
											<?php
											}
											?>
										</tbody>
									</table>	
								</div>
								<?php
								}
								?>
						</div>
					</div>
					
					
				</div>	
			</div>
		</div>

		
		
		<div class="footer">
			<?php
			include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/footer.php";
			?>
		</div>
		<script>
			$(document).ready(function(){
				var max_upload = 3;
				var x = 1;
				$('.add_more').click(function(e){
					e.preventDefault();
					if(x < max_upload){
						x++;
						$(this).before("<div><input class='browseFilePendukung' name='file_pendukung[]' type='file' size='40'/><a href='#' class='remove_field'>Remove</a></div>");
					} else {
						$(this).hide();
						$('.uploadFilePendukung').append('<p class="maksUploadFilePendukung">* Maksimal 3 upload dokumen pendukung</p>');
					}
				});
				$(this).on("click",".remove_field", function(e){ //user click on remove text
					e.preventDefault(); 
					$(this).parent('div').remove(); x--;
					$('.add_more').show();
					$('.maksUploadFilePendukung').hide();
				})
			});
		</script>
		<script>
			$(document).on('click', '.deleteFilePendukung', function(e){
				e.preventDefault();
				var nama_file = $(this).attr('data-name');
				var choice = confirm("Are you sure you want to delete file '" + nama_file + "'?");
				if (choice) {
					window.location.href =  $(this).attr('href');
				}
			});	
		</script>
		<script>
		(function (){	
			$(document).ready(function(){
			var acc = document.getElementsByClassName("accordion");
			var i;

			for (i = 0; i < acc.length; i++) {
			  acc[i].onclick = function() {
				this.classList.toggle("active");
				var panel = this.nextElementSibling;
				if (panel.style.maxHeight){
				  panel.style.maxHeight = null;
				  panel.style.paddingBottom = null;
				} else {
				  panel.style.maxHeight = panel.scrollHeight += 5 + "px";
				  panel.style.paddingBottom = "18px";
				} 
			  }
			}
			});
		})(jQuery);	
		</script>
		
		<script>
			function edit_row(ocid, id, txtareaamount)
			{				
				document.getElementById("edit_btn"+ocid+":"+id).style.display="none";
				document.getElementById("save_btn"+ocid+":"+id).style.display="block";
				var i = 1;
				var j = 1;
				var datatbl = [];
				for(i,j;i<=txtareaamount,j<=txtareaamount; i++,j++){
					datatbl[i] = document.getElementById("datatbl"+ocid+i+":"+id).innerHTML;
					document.getElementById("datatbl"+ocid+i+":"+id).removeAttribute('readonly');
					document.getElementById("datatbl"+ocid+i+":"+id).style.outline ="thin solid #bfbfbf";									 
					document.getElementById("datatbl"+ocid+i+":"+id).style.background ="#f2f2f2";
				}
			}
		</script>
		<script>
			function save_row(ocid, id, txtareaamount)
			{
				var table = document.getElementById("table"+ocid).value;
				var row_no = document.getElementById("row"+ocid+":"+id).value;
				var i = 1;
				var j = 1;
				var datatbl = [];
				var datatbl_data = [];
				var datatbl_val = {};
				while(i<=txtareaamount){
					datatbl[i] = document.getElementById("datatbl"+ocid+i+":"+id).value;
					datatbl_val[i] = datatbl[i];
					i++;
				}
				
				$.ajax
				({
					type:'post',
					url:'../edittabel',
					data:{
					edit_row:table,
					row_no:id,
					columns:txtareaamount,
					datatbl_val

					},
					success:function(response) {
						var i = 1;
						if(response == "success")
						{
							while(i<=txtareaamount){
								datatbl[i] = document.getElementById("datatbl"+ocid+i+":"+id).value;
								document.getElementById("edit_btn"+ocid+":"+id).style.display="block";
								document.getElementById("save_btn"+ocid+":"+id).style.display="none";
								document.getElementById("datatbl"+ocid+i+":"+id).readOnly =true;
								document.getElementById("datatbl"+ocid+i+":"+id).style.outline ="none";
								document.getElementById("datatbl"+ocid+i+":"+id).style.background ="transparent";
								i++;
							}
						}
					}
				}); 
			}
		</script>
	</body>

	
	
</html>
