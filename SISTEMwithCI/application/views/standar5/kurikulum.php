<?php
if(!isset($_SESSION['nama'])){
      header("location:" . base_url());
      exit();
   }	
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Akreditasi | Standar 5 | Kurikulum</title>
		<script type = 'text/javascript' src="<?php echo base_url(); ?>js/jquery-1.12.4.js"></script>
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/mainlayout.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/accordion.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/jquery.dataTables.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/data&dokumenstd5.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/dokumenpendukung.css">
		<link rel="shortcut icon" href="<?php echo base_url(); ?>/favicon.ico" type="image/x-icon">
		<link rel="icon" href="<?php echo base_url(); ?>/favicon.ico" type="image/x-icon">
		<script type = 'text/javascript' src="<?php echo base_url(); ?>js/jquery.dataTables.js"></script>
		
	</head>
	
	<body>
		<div class="header">
			<h1><a href="<?php echo base_url(); ?>dashboard">Akreditasi SI</a></h1>
			<?php
				include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/header.php";
			?>
		</div>
		
		<div class="sidebar">
			<?php
				include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/sidebar.php";
			?>
		</div>
			
		<div class="main-layout">
			<div class="main-content">
			<div class="menu-breadcrumb">
				<ul class="breadcrumb">
					<li><a href="<?php echo base_url(); ?>dashboard" id="menu-breadcrumb">Dashboard</a></li>
					<li>Kurikulum</li>
				</ul>
			</div>
			 <div class="menu-head">
				<ul>
					<li><a href="<?php echo base_url(); ?>standar5/kurikulum" id="menu-head" style="background:#d9d9d9;color:black;">Kurikulum</a></li>
					<li><a href="<?php echo base_url(); ?>standar5/pembelajaran" id="menu-head">Pembelajaran</a></li>
					<li><a href="<?php echo base_url(); ?>standar5/sa" id="menu-head">Suasana Akademik</a></li>
				</ul>
			</div>
			<div class="menu-subhead">
				<ul>
					<li><a href="<?php echo base_url(); ?>standar5/kurikulum/5.1.1kompetensidan5.1.2sk" id="menu-subhead">5.1.1 Kompetensi &amp; 5.1.2 Struktur Kurikulum</a></li>
					<li><a href="<?php echo base_url(); ?>standar5/kurikulum/5.1.dan5.1.4matakuliah" id="menu-subhead">5.1.3 Mata kuliah yang dilaksanakan &amp; 5.1.2 Substansi Praktikum yang mandiri</a></li>
					<li><a href="<?php echo base_url(); ?>standar5/kurikulum/5.2peninjauan" id="menu-subhead">5.2 Peninjauan Kurikulum dalam 5 Tahun Terakhir </a></li>
				</ul>
			</div>
			
			<div class="sub-header">
				<h2>Standar 5: Kurikulum</h2>
			</div>
			<div class="main-content">
			
				
				
				
			</div>
		</div>

		<div class="footer">
			<?php
			include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/footer.php";
			?>
		</div>
		
	</body>

	
	
</html>
