<?php
if(!isset($_SESSION['nama'])){
      header("location:" . base_url());
      exit();
   }	
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Akreditasi | Standar 5 | Simulasi Penilaian | 5.1.4 Substansi praktikum dan pelaksanaan praktikum</title>
		<script type = 'text/javascript' src="<?php echo base_url(); ?>js/jquery-1.12.4.js"></script>
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/mainlayout.css">
		<!--<link rel="stylesheet" href="<?php echo base_url(); ?>css/accordion.css">-->
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/jquery.dataTables.css">
		<!--<link rel="stylesheet" href="<?php echo base_url(); ?>css/data&dokumenstd5.css">-->
		<!--<link rel="stylesheet" href="<?php echo base_url(); ?>css/simulasipenilaian.css">-->
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/submenu.simulasipenilaian.css">
		<!--<link rel="stylesheet" href="<?php echo base_url(); ?>css/dokumenpendukung.css">-->
		<script type = 'text/javascript' src="<?php echo base_url(); ?>js/jquery.dataTables.js"></script>
		<script type = 'text/javascript' src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>
		<script type = 'text/javascript' >
		(function ($){	
			$(document).ready(function() {
				$('table').dataTable({
					"order": [],
					responsive: true,
					"scrollX": true,
					"scrollY": true,
					pageResize: true,
					autoWidth: true,
					columnDefs: [
						{ width: 10, targets: 0 }
					]
					
				});
			});	
		})(jQuery);	
		</script>
		
	</head>
	
	<body>
		<div class="header">
			<h1><a href="<?php echo base_url(); ?>dashboard">Akreditasi SI</a></h1>
			<?php
				include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/header.php";
			?>
		</div>
		
		<div class="sidebar">
			<?php
				include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/sidebar.php";
			?>
		</div>
			
		<div class="main-layout">
			<div class="sub-header">
				<h2>Simulasi Penilaian Borang Program Studi Standar 5</h2>
			</div>
			<div class="sub-header">
				<h2>5.1.4 Substansi praktikum dan pelaksanaan praktikum</h2>
			</div>
			<div class="main-content">
				<button class="accordion">Substansi praktikum/praktek yang mandiri ataupun yang merupakan bagian dari mata kuliah tertentu</button>
				<div class="panel">
					<div class="panel-content">
						<table id="table-view-data-5-1-4" class="display" width="100%" cellspacing="0">
								
							<thead class="table head">
								<th>No (1)</th>
								<th>Nama Praktikum/Praktek (2)</th>
								<th>Isi Praktikum/Praktek Judul/Modul (3)</th>
								<th>Isi Praktikum/Praktek Jam Pelaksanaan (4)</th>
								<th>Tempat/Lokasi Praktikum/Praktek (5)</th>
							</thead>
								
							
							<tbody class="table body">
								<?php
								if($result_set514 !== "kosong" && $result_set514){
									foreach($result_set514 as $row){
									?>	
										<tr id="row7:<?php echo $row['no'] ?>">
											<td><?php echo $row['no'] ?></td> 	
											<td><?php echo $row['nama_praktikum_praktek'] ?></td>
											<td><?php echo $row['isi_praktikum_praktek_judul_modul'] ?></td>
											<td><?php echo $row['isi_praktikum_praktek_jam_pelaksanaan'] ?></td>
											<td><?php echo $row['tempat_lokasi_praktikum_praktek'] ?></td> 
										</tr>
								<?php
									}
								} else {
									?>
									<p>Substansi praktikum/praktek yang mandiri ataupun yang merupakan bagian dari mata kuliah tertentu tidak ditemukan, silahkan masukan Substansi praktikum/praktek yang mandiri ataupun yang merupakan bagian dari mata kuliah tertentu <a href="<?php echo base_url(); ?>standar5/kurikulum/5.1kurikulum">di sini</a>.</p>
								<?php	
								}	
								?>								
							</tbody>
						</table>
					</div>
				</div>
				
				<div class="elemen-penilaian">Elemen Penilaian</div>
				<div class="deskripsi-elemen-penilaian">
					<p>
						5.1  Kurikulum memuat matakuliah yang mendukung pencapaian kompetensi lulusan dan 
						memberikan keleluasaan pada mahasiswa untuk memperluas wawasan dan memperdalam keahlian 
						sesuai dengan minatnya, serta dilengkapi dengan deskripsi matakuliah, silabus dan rencana 
						pembelajaran.
					</p>
				</div>
				
				<div class="info-penilaian">Penilaian 5.1.4 Substansi praktikum dan pelaksanaan praktikum</div>
				<button class="tablinks" style="display: none" id="defaultOpen" onclick="nextSimulation(event, '5.1.4')">5.1.4 Substansi praktikum dan pelaksanaan praktikum</button>
				<form action="5.1.4/simpan" method="post">
					<div class="tabcontent" id="5.1.4">
						<h4>5.1.4 Substansi praktikum dan pelaksanaan praktikum</h4>
						<div class="input-penilaian">
							<p class="teks-penilaian nilai">Nilai <span class="required">*</span></p>:
							<label title="Pelaksanaan modul praktikum lebih dari cukup  (ditambah dengan demonstrasi di laboratorium ) di PT sendiri.">
								<span title="Sangat Baik (4)"><input onchange="hitungBobotxNilai('4','1')" type="radio" required class="nilai" name="nilai" id="4" value="4"/> Sangat Baik (4)
							</label></span>
							<label title="Pelaksanaan modul praktikum cukup, dilaksanakan di PT sendiri.">
								<span title="Baik (3)"><input onchange="hitungBobotxNilai('3','1')" type="radio" required class="nilai" name="nilai" id="3" value="3"/> Baik (3)
							</label></span>
							<label title="Pelaksanaan modul praktikum cukup, tetapi dilaksanakan di PT lain.">
								<span title="Cukup (2)"><input onchange="hitungBobotxNilai('2','1')" type="radio" required class="nilai" name="nilai" id="2" value="2"/> Cukup (2)
							</label></span>
							<label title="Pelaksanaan modul praktikum kurang dari minimum.">
								<span title="Kurang (1)"><input onchange="hitungBobotxNilai('1','1')" type="radio" required class="nilai" name="nilai" id="1" value="1"/> Kurang (1)
							</label></span>
							<label title="(Tidak ada skor nol)">
								<span title="Sangat Kurang (0)"><input onchange="hitungBobotxNilai('0','1')" type="radio" required class="nilai" name="nilai" id="0" value="0"/> Sangat Kurang (0)
							</label></span>
							<br />
							<p class="teks-penilaian bobot">Bobot </p>:
							<input style="background:transparent; border:none; font-size:inherit;" class="bobot-penilaian" id="bobot1"  type="text" name="bobot" readonly value="1.14"/>
							<br />
							<p class="teks-penilaian bobotxnilai">Bobot X Nilai </p>:
							<input style="background:transparent; border:none; font-size:inherit;" class="bobotxnilai-penilaian" id="bobotxnilai1" value="0" readonly/>
							<br />
							<p class="teks-penilaian catatan">Catatan </p><p style="width:8px; float:left">:</p>
							<textarea name="catatan" class="catatan-penilaian" id="catatan1"></textarea>
						</div>
						<button title="Simpan Penilaian" name="simpanBtn" type="submit" class="tablinks">Simpan</button>
					</div>
				</form>
				
			</div>
		</div>

		<div class="footer">
			<?php
			include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/footer.php";
			?>
		</div>
		<script>
		function hitungBobotxNilai(rb, no) {
			var myBox1 = rb;	
			var myBox2 = document.getElementById('bobot'+no).value;
			var result = document.getElementById('bobotxnilai'+no);	
			var myResult = myBox1 * myBox2;
			result.value = myResult;			
		}
		</script>
		
		
		<script>
		function nextSimulation(evt, simulationName) {
			// Declare all variables
			var i, tabcontent, tablinks;

			// Get all elements with class="tabcontent" and hide them
			tabcontent = document.getElementsByClassName("tabcontent");
			for (i = 0; i < tabcontent.length; i++) {
				tabcontent[i].style.display = "none";
			}

			// Get all elements with class="tablinks" and remove the class "active"
			tablinks = document.getElementsByClassName("tablinks");
			for (i = 0; i < tablinks.length; i++) {
				tablinks[i].className = tablinks[i].className.replace(" active", "");
			}

			// Show the current tab, and add an "active" class to the button that opened the tab
			document.getElementById(simulationName).style.display = "inline-table";
			evt.currentTarget.className += " active";
		}
		
		// Get the element with id="defaultOpen" and click on it
		document.getElementById("defaultOpen").click();
		</script>
		
		<script>
		(function (){	
			$(document).ready(function(){
			var acc = document.getElementsByClassName("accordion");
			var i;

			for (i = 0; i < acc.length; i++) {
			  acc[i].onclick = function() {
				this.classList.toggle("active");
				var panel = this.nextElementSibling;
				if (panel.style.maxHeight){
				  panel.style.maxHeight = null;
				  panel.style.paddingBottom = null;
				} else {
				  panel.style.maxHeight = panel.scrollHeight + "px";
				  panel.style.paddingBottom = "18px";
				} 
			  }
			}
			});
		})(jQuery);	
		</script>
		
	</body>

	
	
</html>
