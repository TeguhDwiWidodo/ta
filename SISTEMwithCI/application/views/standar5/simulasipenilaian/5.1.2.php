<?php
if(!isset($_SESSION['nama'])){
      header("location:" . base_url());
      exit();
   }	
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Akreditasi | Standar 5 | Simulasi Penilaian | 5.1.2 Struktur Kurikulum</title>
		<script type = 'text/javascript' src="<?php echo base_url(); ?>js/jquery-1.12.4.js"></script>
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/mainlayout.css">
		<!--<link rel="stylesheet" href="<?php echo base_url(); ?>css/accordion.css">-->
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/jquery.dataTables.css">
		<!--<link rel="stylesheet" href="<?php echo base_url(); ?>css/data&dokumenstd5.css">-->
		<!--<link rel="stylesheet" href="<?php echo base_url(); ?>css/simulasipenilaian.css">-->
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/submenu.simulasipenilaian.css">
		<!--<link rel="stylesheet" href="<?php echo base_url(); ?>css/dokumenpendukung.css">-->
		<script type = 'text/javascript' src="<?php echo base_url(); ?>js/jquery.dataTables.js"></script>
		<script type = 'text/javascript' src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>
		<script type = 'text/javascript' >
		(function ($){	
			$(document).ready(function() {
				$('table').dataTable({
					"order": [],
					responsive: true,
					"scrollX": true,
					"scrollY": true,
					pageResize: true,
					autoWidth: true,
					columnDefs: [
						{ width: 10, targets: 0 }
					]
					
				});
			});	
		})(jQuery);	
		</script>
		
	</head>
	
	<body>
		<div class="header">
			<h1><a href="<?php echo base_url(); ?>dashboard">Akreditasi SI</a></h1>
			<?php
				include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/header.php";
			?>
		</div>
		
		<div class="sidebar">
			<?php
				include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/sidebar.php";
			?>
		</div>
			
		<div class="main-layout">
			<div class="sub-header">
				<h2>Simulasi Penilaian Borang Program Studi Standar 5</h2>
			</div>
			<div class="sub-header">
				<h2>5.1.2 Struktur Kurikulum</h2>
			</div>
			<div class="main-content">
				<button class="accordion">Jumlah SKS program studi (minimum untuk kelulusan)</button>
				<div class="panel">
					<div class="panel-content">
						<table id="table-view-data-5-1-2-1" class="display" width="100%" cellspacing="0">
								
							<thead class="table head">
								<!--<th>No</th>-->
								<th>Jenis Matakuliah (1)</th>
								<th>SKS (2)</th>
								<th>Keterangan (3)</th>
							</thead>
							<tbody class="table body">
								<?php
								if($result_set5121 !== "kosong" && $result_set5121){
									foreach($result_set5121 as $row){
									?>	
										<tr id="row4:<?php echo $row['no'] ?>">
											<!--<td><?php //echo $row['no'] ?></td>-->
											<td><?php echo $row['jenis_mata_kuliah'] ?></td>
											<td><?php echo $row['sks'] ?></td>
											<td><?php echo $row['keterangan'] ?></td>
										</tr>
								<?php
									}
								} else {
									?>
									<p>Jumlah SKS program studi (minimum untuk kelulusan) tidak ditemukan, silahkan masukan Jumlah SKS program studi (minimum untuk kelulusan) <a href="<?php echo base_url(); ?>standar5/kurikulum/5.1kurikulum">di sini</a>.</p>
								<?php	
								}	
								?>
							</tbody>
							<tbody>
								<tr>
									<td>Jumlah Total Mata Kuliah Wajib + Pilihan</td>
									<td><?php echo $jumlahMatkulWajibPilihan ?></td>
									<td></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				
				<button id="sk" class="accordion">Struktur kurikulum berdasarkan urutan Mata Kuliah (MK)</button>
				<div class="panel">
					<div class="panel-content">
						<p>Catatan :</p>
						<ul style="padding-bottom: 5px">
							<li>Tanda "Ada" pada mata kuliah yang dalam penentuan nilai akhirnya memberikan bobot pada tugas-tugas (praktikum/praktek, PR atau makalah) ≥ 20%.</li>
							<li>Tanda "Ada" pada mata kuliah yang dilengkapi dengan deskripsi, silabus, dan atau SAP.  Sediakan dokumen pada saat asesmen lapangan.</li>
						</ul>
						<table id="table-view-data-5-1-2-2" class="display" width="100%" cellspacing="0">
							<thead class="table head">
								<!--<th>No</th>-->
								<th>SMT (1)</th>
								<th>Kode MK (2)</th>
								<th>Nama Mata Kuliah (3)</th>
								<th>Bobot SKS (4)</th>
								<th>SKS MK dalam Kurikulum Inti (5)</th>
								<th>SKS MK dalam Kurikulum Institusional (6)</th>
								<th>Bobot Tugas (7)</th>
								<th>Kelengkapan Deskripsi (8)</th>
								<th>Kelengkapan Silabus (9)</th>
								<th>Kelengkapan SAP (10)</th>
								<th>Unit/Jur/Fak Penyelenggara (11)</th>
							</thead>
							<tbody class="table body">
								<?php
								if($result_set5122 !== "kosong" && $result_set5122){
									foreach($result_set5122 as $row){
									?>	
										<tr id="row5:<?php echo $row['no'] ?>">
											<!--<td><?php //echo $row['no'] ?></td>-->
											<td><?php echo $row['smt'] ?></td> 	
											<td><?php echo $row['kode_mk'] ?></td>
											<td><?php echo $row['nama_mata_kuliah'] ?></td>
											<td><?php echo $row['bobot_sks'] ?></td> 	
											<td><?php echo $row['sks_mk_dalam_kurikulum_inti'] ?></td>
											<td><?php echo $row['sks_mk_dalam_kurikulum_Institusional'] ?></td>
											<td><?php echo $row['bobot_tugas'] ?></td> 	
											<td><?php echo $row['kelengkapan_deskripsi'] ?></td>
											<td><?php echo $row['kelengkapan_silabus'] ?></td>
											<td><?php echo $row['kelengkapan_sap'] ?></td> 	
											<td><?php echo $row['unit_jur_fak_penyelenggara'] ?></td>
										</tr>
								<?php
									}
								} else {
									?>
									<p>Jumlah SKS program studi (minimum untuk kelulusan) tidak ditemukan, silahkan masukan Jumlah SKS program studi (minimum untuk kelulusan) <a href="<?php echo base_url(); ?>standar5/kurikulum/5.1kurikulum">di sini</a>.</p>
								<?php	
								}	
								?>						
							</tbody>	
							<tbody>
								<tr>
									<td colspan="3">Total SKS</td>
									<td><?php echo $jumlahBobotIntiInstitusional['bobotsks'] ?></td>
									<td><?php echo $jumlahBobotIntiInstitusional['inti'] ?></td>
									<td><?php echo $jumlahBobotIntiInstitusional['institusional'] ?></td>
									<td colspan="5"></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				
				<div class="elemen-penilaian">Elemen Penilaian</div>
				<div class="deskripsi-elemen-penilaian">
					<p>
						5.1 Kurikulum memuat matakuliah yang mendukung pencapaian kompetensi lulusan dan memberikan 
						keleluasaan pada mahasiswa untuk memperluas wawasan dan memperdalam keahlian sesuai dengan minatnya, 
						serta dilengkapi dengan deskripsi matakuliah, silabus dan rencana pembelajaran.
					</p>
				</div>
				
				<div class="info-penilaian">Penilaian 5.1.2 Struktur Kurikulum</div>
				<button class="tablinks" style="display: none" id="defaultOpen" onclick="nextSimulation(event, '5.1.2.a')">5.1.2 Struktur Kurikulum</button>
				<form action="5.1.2/simpan" method="post">
					<div class="tabcontent" id="5.1.2.a">
						<h4>5.1.2.a Kesesuaian mata kuliah dengan standar kompetensi</h4>
						<div class="input-penilaian">
							<p class="teks-penilaian nilai">Nilai <span class="required">*</span></p>:
							<label title="Sesuai dengan standar kompetensi, sudah berorientasi  ke masa depan.">
								<span title="Sangat Baik (4)"><input onchange="hitungBobotxNilai('4','1')" type="radio" required class="nilai" name="nilai[0]" id="4" value="4"/> Sangat Baik (4)
							</label></span>
							<label title="Sesuai dengan standar kompetensi, berorientasi ke masa kini.">
								<span title="Baik (3)"><input onchange="hitungBobotxNilai('3','1')" type="radio" required class="nilai" name="nilai[0]" id="3" value="3"/> Baik (3)
							</label></span>
							<label title="Sesuai dengan standar kompetensi, tetapi masih berorientasi ke masa lalu.">
								<span title="Cukup (2)"><input onchange="hitungBobotxNilai('2','1')" type="radio" required class="nilai" name="nilai[0]" id="2" value="2"/> Cukup (2)
							</label></span>
							<label title="Tidak sesuai dengan standar kompetensi.">
								<span title="Kurang (1)"><input onchange="hitungBobotxNilai('1','1')" type="radio" required class="nilai" name="nilai[0]" id="1" value="1"/> Kurang (1)
							</label></span>
							<label title="Tidak tidak memiliki standar kompetensi.">
								<span title="Sangat Kurang (0)"><input onchange="hitungBobotxNilai('0','1')" type="radio" required class="nilai" name="nilai[0]" id="0" value="0"/> Sangat Kurang (0)
							</label></span>
							<br />
							<p class="teks-penilaian bobot">Bobot </p>:
							<input style="background:transparent; border:none; font-size:inherit;" class="bobot-penilaian" id="bobot1"  type="text" name="bobot" readonly value="0.57"/>
							<br />
							<p class="teks-penilaian bobotxnilai">Bobot X Nilai </p>:
							<input style="background:transparent; border:none; font-size:inherit;" class="bobotxnilai-penilaian" id="bobotxnilai1" value="0" readonly/>
							<br />
							<p class="teks-penilaian catatan">Catatan </p><p style="width:8px; float:left">:</p>
							<textarea name="catatan[]" class="catatan-penilaian" id="catatan1"></textarea>
						</div>
						<button type="button" title="Penilaian Selanjutnya" class="tablinks" onclick="nextSimulation(event, '5.1.2.b')">Selanjutnya</button>
					</div>
					<div class="tabcontent" id="5.1.2.b">
						<h4>5.1.2.b Persentase mata kuliah yang dalam penentuan nilai akhirnya memberikan bobot pada tugas-tugas (PR atau makalah) ≥ 20%</h4>
						<p style="width: 150px;float: left;">Cara penghitungan :</p>
						<p style="padding-left: 150px;">Jumlah mata kuliah yang diberi "Ada" pada kolom (7) <a href="#sk">Struktur kurikulum berdasarkan urutan Mata Kuliah (MK)</a> dibagi dengan jumlah total mata kuliah wajib dan pilihan.
						</p>
						<p style="width: 210px;" class="teks-penilaian informasi">Informasi </p>:
						<p style="display: inline;">PTGS = Persentase MK dengan tugas ≥ 20%</p>
						<div class="input-penilaian">
							<p style="width: 210px;padding:6px 0;" class="teks-penilaian nilai">Jumlah MK pada kolom 7 yang diberi huruf "Ada" <span class="required">*</span></p>:
							<label title="Jika PTGS ≥ 50%, maka skor = 4. Jika PTGS < 50%, maka skor = 8 x PTGS.">
								<span title=""><input readonly value="<?php echo $input1_512b ?>" style="font-size:inherit;padding: 5px;margin: 10px 0;" type="number" title="Please enter a number between 0 - 150" name="nilai[1]" min="0" max="150" required class="nilai" id="input-penilaian-number1"/>
							</label></span>

							<br />
							<p style="width: 210px;padding-top:10px;" class="teks-penilaian nilai">Jumlah MK wajib + pilihan <span class="required">*</span></p>:
							<input readonly value="<?php echo $totalMatkulWajibPilihan ?>" style="font-size:inherit;padding: 5px;margin: 5px 0;" type="number" title="Please enter a number between 0 - 150" name="nilai[2]" min="0" max="150" required class="nilai" id="input-penilaian-number2"/>
							<br />
							<p style="width: 210px;margin:10px 0;" class="teks-penilaian ptgs">PTGS </p>:
							<input style="width: 30%;background:transparent; border:none; font-size:inherit;margin: 10px 0;" class="ptgs" id="nilai-ptgs" value="0" readonly/>
							<br />
							<p style="width:210px;margin-bottom: 5px;" class="teks-penilaian nilai">Nilai </p>:
							<input style="width: 30%;background:transparent; border:none; font-size:inherit;margin-bottom: 5px;" name="nilai-number[0]" class="nilai-akhir" id="nilaiakhir1" value="0" readonly/>
							<br />
							<p style="width: 210px;" class="teks-penilaian catatan">Catatan </p><p style="width:8px; float:left">:</p>
							<textarea style="margin-bottom: 10px;" type="text" name="catatan[]" class="catatan-penilaian" id="catatan2"></textarea>
						</div>
						
						<button title="Penilaian Sebelumnya" style="margin-left:218px;margin-right: 10px" type="button" class="tablinks" onclick="nextSimulation(event, '5.1.2.a')">Sebelumnya</button>
						<button type="button" title="Penilaian Selanjutnya" style="margin-left:unset" class="tablinks" onclick="nextSimulation(event, '5.1.2.c')">Selanjutnya</button>
					</div>
					<div class="tabcontent" id="5.1.2.c">
						<h4>5.1.2.c Mata kuliah dilengkapi dengan deskripsi mata kuliah, silabus dan SAP.</h4>
						<p>Informasi : PDMK = Persentase mata kuliah yang memiliki deskripsi, silabus dan SAP.</p>
						<div class="input-penilaian">
							<p style="width: 210px;padding:6px 0;" class="teks-penilaian nilai">Jumlah MK dengan SAP <span class="required">*</span></p>:
							<label title="Jika PDMK ≥ 95%, maka skor = 4. Jika 55% < PDMK < 95%, maka skor = 10 x (PDMK – 55%). Jika PDMK ≤ 55%, maka skor = 0.">
								<span title=""><input readonly value="<?php echo $input1_512c ?>" style="font-size:inherit;padding: 5px;margin: 10px 0;" type="number" title="Please enter a number between 0 - 150" name="nilai[3]" min="0" max="150" required class="nilai" id="input-penilaian-number3"/>
							</label></span>
							<br />
							<p style="width: 210px;padding-top:10px;" class="teks-penilaian bobot">Jumlah seluruh MK <span class="required">*</span></p>:
							<input readonly value="<?php echo $totalMatkulWajibPilihan ?>" style="font-size:inherit;padding: 5px;margin: 5px 0;" type="number" title="Please enter a number between 0 - 150" name="nilai[4]" min="0" max="150" required class="nilai" id="input-penilaian-number4"/>
							<br />
							<p style="width: 210px;margin:10px 0;" class="teks-penilaian mksap">Presentase MK dengan SAP </p>:
							<input style="width: 30%;background:transparent; border:none; font-size:inherit;margin: 10px 0;" class="mksap" id="nilai-presentasi-mksap" value="0" readonly/>
							<br />
							<p style="width:210px;margin-bottom: 5px;" class="teks-penilaian nilai">Nilai </p>:
							<input style="width: 30%;background:transparent; border:none; font-size:inherit;margin-bottom: 5px;" name="nilai-number[1]" class="nilai-akhir" id="nilaiakhir2" value="0" readonly/>
							<br />
							<p style="width: 210px;" class="teks-penilaian catatan">Catatan </p><p style="width:8px; float:left">:</p>
							<textarea style="margin-bottom: 10px;" type="text" name="catatan[]" class="catatan-penilaian" id="catatan3"></textarea>
						</div>
						
						<button title="Penilaian Sebelumnya" style="margin-left:218px;margin-right: 10px" type="button" class="tablinks" onclick="nextSimulation(event, '5.1.2.b')">Sebelumnya</button>
						<button title="Simpan Penilaian" style="margin-left:unset" name="simpanBtn" type="submit" class="tablinks">Simpan</button>
					</div>
				</form>
				
			</div>
		</div>

		<div class="footer">
			<?php
			include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/footer.php";
			?>
		</div>
		<script>
		(function (){	
			$(document).ready(function(){
				var myBox1 = document.getElementById('input-penilaian-number1').value;	
				var myBox2 = document.getElementById('input-penilaian-number2').value;
				var ptgs = document.getElementById('nilai-ptgs');
				var nilaiAkhir = document.getElementById('nilaiakhir1');
				var ptgsResult = myBox1 / myBox2;
				ptgs.value = ptgsResult;
				if(ptgsResult>=0.5){
					nilaiAkhir.value = 4;
				} else {
					var nilai = 8*ptgsResult;
					nilaiAkhir.value = nilai;
				}
				if(myBox1 >= 150 || myBox2 >= 150){
					ptgs.value = "Masukan hanya nilai 0 - 150";
					nilaiAkhir.value = "Masukan hanya nilai 0 - 150";
				} else if (myBox1 == 0 || myBox2 == 0 || myBox1 == null || myBox2 == null){
					ptgs.value = 0;
					nilaiAkhir.value = 0;
				}
			});
		})(jQuery);	
		</script>
		<script>
		(function (){	
			$(document).ready(function(){
				var myBox1 = document.getElementById('input-penilaian-number3').value;	
				var myBox2 = document.getElementById('input-penilaian-number4').value;
				var presentaseMkSap = document.getElementById('nilai-presentasi-mksap');
				var nilaiAkhir = document.getElementById('nilaiakhir2');
				var presentaseMkSapResult = myBox1 / myBox2;
				presentaseMkSap.value = presentaseMkSapResult;
				if(presentaseMkSapResult <= 0.55){
					nilaiAkhir.value = 0;
				} else if (presentaseMkSapResult < 0.95){
					var nilai = 10*(presentaseMkSapResult - 0.55);
					nilaiAkhir.value = nilai;
				} else {
					nilaiAkhir.value = 4;
				}
				if(myBox1 >= 150 || myBox2 >= 150){
					presentaseMkSap.value = "Masukan hanya nilai 0 - 150";
					nilaiAkhir.value = "Masukan hanya nilai 0 - 150";
				} else if (myBox1 == 0 || myBox2 == 0 || myBox1 == null || myBox2 == null){
					presentaseMkSap.value = 0;
					nilaiAkhir.value = 0;
				}
			});
		})(jQuery);	
		</script>
		<script>
		function hitungBobotxNilai(rb, no) {
			var myBox1 = rb;	
			var myBox2 = document.getElementById('bobot'+no).value;
			var result = document.getElementById('bobotxnilai'+no);	
			var myResult = myBox1 * myBox2;
			result.value = myResult;			
		}
		</script>
		
		
		<script>
		function nextSimulation(evt, simulationName) {
			// Declare all variables
			var i, tabcontent, tablinks;

			// Get all elements with class="tabcontent" and hide them
			tabcontent = document.getElementsByClassName("tabcontent");
			for (i = 0; i < tabcontent.length; i++) {
				tabcontent[i].style.display = "none";
			}

			// Get all elements with class="tablinks" and remove the class "active"
			tablinks = document.getElementsByClassName("tablinks");
			for (i = 0; i < tablinks.length; i++) {
				tablinks[i].className = tablinks[i].className.replace(" active", "");
			}

			// Show the current tab, and add an "active" class to the button that opened the tab
			document.getElementById(simulationName).style.display = "inline-table";
			evt.currentTarget.className += " active";
		}
		
		// Get the element with id="defaultOpen" and click on it
		document.getElementById("defaultOpen").click();
		</script>
		
		<script>
		(function (){	
			$(document).ready(function(){
			var acc = document.getElementsByClassName("accordion");
			var i;

			for (i = 0; i < acc.length; i++) {
			  acc[i].onclick = function() {
				this.classList.toggle("active");
				var panel = this.nextElementSibling;
				if (panel.style.maxHeight){
				  panel.style.maxHeight = null;
				  panel.style.paddingBottom = null;
				} else {
				  panel.style.maxHeight = panel.scrollHeight+= 5 + "px";
				  panel.style.paddingBottom = "18px";
				} 
			  }
			}
			});
		})(jQuery);	
		</script>
		
	</body>

	
	
</html>
