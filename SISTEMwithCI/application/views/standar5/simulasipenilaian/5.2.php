<?php
if(!isset($_SESSION['nama'])){
      header("location:" . base_url());
      exit();
   }	
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Akreditasi | Standar 5 | Simulasi Penilaian | 5.2 Peninjauan Kurikulum dalam 5 Tahun Terakhir</title>
		<script type = 'text/javascript' src="<?php echo base_url(); ?>js/jquery-1.12.4.js"></script>
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/mainlayout.css">
		<!--<link rel="stylesheet" href="<?php echo base_url(); ?>css/accordion.css">-->
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/jquery.dataTables.css">
		<!--<link rel="stylesheet" href="<?php echo base_url(); ?>css/data&dokumenstd5.css">-->
		<!--<link rel="stylesheet" href="<?php echo base_url(); ?>css/simulasipenilaian.css">-->
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/submenu.simulasipenilaian.css">
		<!--<link rel="stylesheet" href="<?php echo base_url(); ?>css/dokumenpendukung.css">-->
		<script type = 'text/javascript' src="<?php echo base_url(); ?>js/jquery.dataTables.js"></script>
		<script type = 'text/javascript' src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>
		<script type = 'text/javascript' >
		(function ($){	
			$(document).ready(function() {
				$('table').dataTable({
					"order": [],
					responsive: true,
					"scrollX": true,
					"scrollY": true,
					pageResize: true,
					autoWidth: true,
					columnDefs: [
						{ width: 10, targets: 0, 
						 width: 250, targets: 6 }
					]
					
				});
			});	
		})(jQuery);	
		</script>
		
	</head>
	
	<body>
		<div class="header">
			<h1><a href="<?php echo base_url(); ?>dashboard">Akreditasi SI</a></h1>
			<?php
				include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/header.php";
			?>
		</div>
		
		<div class="sidebar">
			<?php
				include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/sidebar.php";
			?>
		</div>
			
		<div class="main-layout">
			<div class="sub-header">
				<h2>Simulasi Penilaian Borang Program Studi Standar 5</h2>
			</div>
			<div class="sub-header">
				<h2>5.2 Peninjauan Kurikulum dalam 5 Tahun Terakhir</h2>
			</div>
			<div class="main-content">
				<button class="accordion">Hasil Peninjauan Kurikulum 2012 menjadi Kurikulum 2016 terkait Perubahan Mata Kuliah</button>
				<div class="panel">
					<div class="panel-content">
						<table id="table-view-data-5-2" class="display" width="100%" cellspacing="0">
									
								<thead class="table head">
									<th>No (1)</th>
									<th>No MK (2)</th>
									<th>Nama MK (3)</th>
									<th>MK Baru/Lama/Hapus (4)</th>
									<th>Perubahan pada Silabus / SAP (5)</th>
									<th>Perubahan pada Buku Ajar (6)</th>
									<th>Alasan Peninjauan (7)</th>
									<th>Atas usulan/masukan dari (8)</th>
									<th>Berlaku mulai Sem/Th (9)</th>
								</thead>
									
								
								<tbody class="table body">
									<?php
									if($result_set52 !== "kosong" && $result_set52){
										foreach($result_set52 as $row){
										?>	
											<tr id="row1:<?php echo $row['no'] ?>">
												<td><?php echo $row['no'] ?></td> 	
												<td><?php echo $row['no_mk'] ?></td>
												<td><?php echo $row['nama_mk'] ?></td>
												<td><?php echo $row['mk_baru_lama_hapus'] ?></td>
												<td><?php echo $row['perubahan_pada_silabus_sap'] ?></td>
												<td><?php echo $row['perubahan_pada_buku_ajar'] ?></td>
												<td><?php echo $row['alasan_peninjauan'] ?></td>
												<td><?php echo $row['atas_usulan_masukan_dari'] ?></td>
												<td><?php echo $row['berlaku_mulai_sem_th'] ?></td>
											</tr>
									<?php
										}
									} else {
									?>
										<p>Hasil Peninjauan Kurikulum 2012 menjadi Kurikulum 2016 terkait Perubahan Mata Kuliah tidak ditemukan, silahkan masukan Hasil Peninjauan Kurikulum 2012 menjadi Kurikulum 2016 terkait Perubahan Mata Kuliah <a href="<?php echo base_url(); ?>standar5/kurikulum/5.2peninjauan">di sini</a>.</p>
									<?php	
									}	
									?>								
								</tbody>
						</table>
					</div>
				</div>
				
				<div class="elemen-penilaian">Elemen Penilaian</div>
				<div class="deskripsi-elemen-penilaian">
					<p>
						5.2  Kurikulum dan seluruh kelengkapannya harus ditinjau ulang dalam kurun waktu 
						tertentu oleh program studi bersama fihak-fihak terkait (relevansi sosial dan 
						relevansi epistemologis) untuk menyesuaikannya dengan perkembangan Ipteks dan 
						kebutuhan pemangku kepentingan (stakeholders).
					</p>
				</div>
				
				<div class="info-penilaian">Penilaian 5.2 Peninjauan Kurikulum dalam 5 Tahun Terakhir</div>
				<button class="tablinks" style="display: none" id="defaultOpen" onclick="nextSimulation(event, '5.2.a')">5.2 Peninjauan Kurikulum dalam 5 Tahun Terakhir</button>
				<form action="5.2/simpan" method="post">
					<div class="tabcontent" id="5.2.a">
						<h4>5.2.a Pelaksanaan peninjauan kurikulum selama 5 tahun terakhir</h4>
						<div class="input-penilaian">
							<p class="teks-penilaian nilai">Nilai <span class="required">*</span></p>:
							<label title="Pengembangan dilakukan secara mandiri dengan melibatkan pemangku kepentingan internal dan eksternal dan memperhatikan visi, misi, dan umpan balik program studi.">
								<span title="Sangat Baik (4)"><input onchange="hitungBobotxNilai('4','1')" type="radio" required class="nilai" name="nilai[0]" id="4" value="4"/> Sangat Baik (4)
							</label></span>
							<label title="Pengembangan dilakukan bekerjasama dengan perguruan tinggi lain tetapi tidak melibatkan pemangku kepentingan eksternal lainnya walaupun menyesuaikan dengan visi, misi, dan umpan balik.">
								<span title="Baik (3)"><input onchange="hitungBobotxNilai('3','1')" type="radio" required class="nilai" name="nilai[0]" id="3" value="3"/> Baik (3)
							</label></span>
							<label title="Pengembangan mengikuti perubahan di perguruan tinggi lain yang disesuaikan dengan visi, misi, dan umpan balik.">
								<span title="Cukup (2)"><input onchange="hitungBobotxNilai('2','1')" type="radio" required class="nilai" name="nilai[0]" id="2" value="2"/> Cukup (2)
							</label></span>
							<label title="Pengembangan mengikuti perubahan di perguruan tinggi lain tanpa penyesuaian.">
								<span title="Kurang (1)"><input onchange="hitungBobotxNilai('1','1')" type="radio" required class="nilai" name="nilai[0]" id="1" value="1"/> Kurang (1)
							</label></span>
							<label title="Dalam 5 tahun terakhir, tidak pernah melakukan peninjauan ulang.">
								<span title="Sangat Kurang (0)"><input onchange="hitungBobotxNilai('0','1')" type="radio" required class="nilai" name="nilai[0]" id="0" value="0"/> Sangat Kurang (0)
							</label></span>
							<br />
							<p class="teks-penilaian bobot">Bobot </p>:
							<input style="background:transparent; border:none; font-size:inherit;" class="bobot-penilaian" id="bobot1"  type="text" name="bobot" readonly value="0.57"/>
							<br />
							<p class="teks-penilaian bobotxnilai">Bobot X Nilai </p>:
							<input style="background:transparent; border:none; font-size:inherit;" class="bobotxnilai-penilaian" id="bobotxnilai1" value="0" readonly/>
							<br />
							<p class="teks-penilaian bobotxnilai">Catatan </p><p style="width:8px; float:left">:</p>
							<textarea name="catatan" class="catatan-penilaian" id="catatan1"></textarea>
						</div>
						<button type="button" title="Penilaian Selanjutnya" class="tablinks" onclick="nextSimulation(event, '5.2.b')">Selanjutnya</button>
					</div>
					
					<div class="tabcontent" id="5.2.b">
						<h4>5.2.b  Penyesuaian kurikulum dengan perkembangan Ipteks dan kebutuhan</h4>
						<div class="input-penilaian">
							<p class="teks-penilaian nilai">Nilai <span class="required">*</span></p>:
							<label title="Pembaharuan kurikulum dilakukan sesuai dengan perkembangan ilmu di bidangnya dan kebutuhan pemangku kepentingan.">
								<span title="Sangat Baik (4)"><input onchange="hitungBobotxNilai('4','2')" type="radio" required class="nilai" name="nilai[1]" id="4" value="4"/> Sangat Baik (4)
							</label></span>
							<label title="Pembaharuan kurikulum dilakukan sesuai dengan perkembangan ilmu di bidangnya, tetapi kurang memperhatikan kebutuhan pemangku kepentingan.">
								<span title="Baik (3)"><input onchange="hitungBobotxNilai('3','2')" type="radio" required class="nilai" name="nilai[1]" id="3" value="3"/> Baik (3)
							</label></span>
							<label title="Pembaharuan hanya menata ulang kurikulum yang sudah ada, tanpa disesuaikan dengan perkembangan.">
								<span title="Cukup (2)"><input onchange="hitungBobotxNilai('2','2')" type="radio" required class="nilai" name="nilai[1]" id="2" value="2"/> Cukup (2)
							</label></span>
							<label title="-">
								<span title="Kurang (1)"><input onchange="hitungBobotxNilai('1','2')" type="radio" required class="nilai" name="nilai[1]" id="1" value="1"/> Kurang (1)
							</label></span>
							<label title="Tidak ada pembaharuan kurikulum selama 5 tahun terakhir.">
								<span title="Sangat Kurang (0)"><input onchange="hitungBobotxNilai('0','2')" type="radio" required class="nilai" name="nilai[1]" id="0" value="0"/> Sangat Kurang (0)
							</label></span>
							<br />
							<p class="teks-penilaian bobot">Bobot </p>:
							<input style="background:transparent; border:none; font-size:inherit;" class="bobot-penilaian" id="bobot2"  type="text" name="bobot" readonly value="0.57"/>
							<br />
							<p class="teks-penilaian bobotxnilai">Bobot X Nilai </p>:
							<input style="background:transparent; border:none; font-size:inherit;" class="bobotxnilai-penilaian" id="bobotxnilai2" value="0" readonly/>
							<br />
							<p class="teks-penilaian bobotxnilai">Catatan </p><p style="width:8px; float:left">:</p>
							<textarea name="catatan" class="catatan-penilaian" id="catatan2"></textarea>
						</div>
						<button title="Penilaian Sebelumnya" style="margin-right: 10px" type="button" class="tablinks" onclick="nextSimulation(event, '5.2.a')">Sebelumnya</button>
						<button title="Simpan Penilaian" style="margin-left: unset" name="simpanBtn" type="submit" class="tablinks">Simpan</button>
					</div>
				</form>
				
			</div>
		</div>

		<div class="footer">
			<?php
			include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/footer.php";
			?>
		</div>
		<script>
		function hitungBobotxNilai(rb, no) {
			var myBox1 = rb;	
			var myBox2 = document.getElementById('bobot'+no).value;
			var result = document.getElementById('bobotxnilai'+no);	
			var myResult = myBox1 * myBox2;
			result.value = myResult;			
		}
		</script>
		
		
		<script>
		function nextSimulation(evt, simulationName) {
			// Declare all variables
			var i, tabcontent, tablinks;

			// Get all elements with class="tabcontent" and hide them
			tabcontent = document.getElementsByClassName("tabcontent");
			for (i = 0; i < tabcontent.length; i++) {
				tabcontent[i].style.display = "none";
			}

			// Get all elements with class="tablinks" and remove the class "active"
			tablinks = document.getElementsByClassName("tablinks");
			for (i = 0; i < tablinks.length; i++) {
				tablinks[i].className = tablinks[i].className.replace(" active", "");
			}

			// Show the current tab, and add an "active" class to the button that opened the tab
			document.getElementById(simulationName).style.display = "inline-table";
			evt.currentTarget.className += " active";
		}
		
		// Get the element with id="defaultOpen" and click on it
		document.getElementById("defaultOpen").click();
		</script>
		
		<script>
		(function (){	
			$(document).ready(function(){
			var acc = document.getElementsByClassName("accordion");
			var i;

			for (i = 0; i < acc.length; i++) {
			  acc[i].onclick = function() {
				this.classList.toggle("active");
				var panel = this.nextElementSibling;
				if (panel.style.maxHeight){
				  panel.style.maxHeight = null;
				  panel.style.paddingBottom = null;
				} else {
				  panel.style.maxHeight = panel.scrollHeight + "px";
				  panel.style.paddingBottom = "18px";
				} 
			  }
			}
			});
		})(jQuery);	
		</script>
		
	</body>

	
	
</html>
