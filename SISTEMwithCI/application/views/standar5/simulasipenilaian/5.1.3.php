<?php
if(!isset($_SESSION['nama'])){
      header("location:" . base_url());
      exit();
   }	
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Akreditasi | Standar 5 | Simulasi Penilaian | 5.1.3 Fleksibilitas mata kuliah pilihan</title>
		<script type = 'text/javascript' src="<?php echo base_url(); ?>js/jquery-1.12.4.js"></script>
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/mainlayout.css">
		<!--<link rel="stylesheet" href="<?php echo base_url(); ?>css/accordion.css">-->
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/jquery.dataTables.css">
		<!--<link rel="stylesheet" href="<?php echo base_url(); ?>css/data&dokumenstd5.css">-->
		<!--<link rel="stylesheet" href="<?php echo base_url(); ?>css/simulasipenilaian.css">-->
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/submenu.simulasipenilaian.css">
		<!--<link rel="stylesheet" href="<?php echo base_url(); ?>css/dokumenpendukung.css">-->
		<script type = 'text/javascript' src="<?php echo base_url(); ?>js/jquery.dataTables.js"></script>
		<script type = 'text/javascript' src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>
		<script type = 'text/javascript' >
		(function ($){	
			$(document).ready(function() {
				$('table').dataTable({
					"order": [],
					responsive: true,
					"scrollX": true,
					"scrollY": true,
					pageResize: true,
					autoWidth: true,
					columnDefs: [
						{ width: 10, targets: 0 }
					]
					
				});
			});	
		})(jQuery);	
		</script>
		
	</head>
	
	<body>
		<div class="header">
			<h1><a href="<?php echo base_url(); ?>dashboard">Akreditasi SI</a></h1>
			<?php
				include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/header.php";
			?>
		</div>
		
		<div class="sidebar">
			<?php
				include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/sidebar.php";
			?>
		</div>
			
		<div class="main-layout">
			<div class="sub-header">
				<h2>Simulasi Penilaian Borang Program Studi Standar 5</h2>
			</div>
			<div class="sub-header">
				<h2>5.1.3 Fleksibilitas mata kuliah pilihan</h2>
			</div>
			<div class="main-content">
				<button class="accordion">Mata kuliah pilihan yang dilaksanakan dalam tiga tahun terakhir</button>
				<div class="panel">
					<div class="panel-content">
						<table id="table-view-data-5-1-3" class="display" width="100%" cellspacing="0">
								
							<thead class="table head">
								<th>Semester (1)</th>
								<th>Kode MK (2)</th>
								<th>Nama MK (Pilihan) (3)</th>
								<th>Bobot SKS (4)</th>
								<th>Bobot Tugas (5)</th>
								<th>Unit/Jur/Fak Pengelola (6)</th>
								<th>Kelompok MK (Pilihan) (7)</th>
							</thead>
								
							
							<tbody class="table body">
								<?php
								if($result_set513 !== "kosong" && $result_set513){
									foreach($result_set513 as $row){
									?>	
										<tr id="row1:<?php echo $row['no'] ?>">
											<td><?php echo $row['semester'] ?></td>
											<td><?php echo $row['kode_mk'] ?></td>
											<td><?php echo $row['nama_mk_pilihan'] ?></td>
											<td><?php echo $row['bobot_sks'] ?></td> 	
											<td><?php echo $row['bobot_tugas'] ?></td>
											<td><?php echo $row['unit_jur_fak_pengelola'] ?></td>
											<td><?php echo $row['kelompok_mk_pilihan'] ?></td>
										</tr>
								<?php
									}
								} else {
									?>
									<p>Mata kuliah pilihan yang dilaksanakan dalam tiga tahun terakhir tidak ditemukan, silahkan masukan Mata kuliah pilihan yang dilaksanakan dalam tiga tahun terakhir <a href="<?php echo base_url(); ?>standar5/kurikulum/5.1kurikulum">di sini</a>.</p>
								<?php	
								}	
								?>					
							</tbody>
							<tbody>
								<tr>
									<td colspan="3">Total SKS</td>
									<td><?php echo $jumlahSksMkPilihan ?></td>
									<td colspan="3"></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				
				<div class="elemen-penilaian">Elemen Penilaian</div>
				<div class="deskripsi-elemen-penilaian">
					<p>
						5.1  Kurikulum memuat matakuliah yang mendukung pencapaian kompetensi lulusan dan 
						memberikan keleluasaan pada mahasiswa untuk memperluas wawasan dan memperdalam keahlian 
						sesuai dengan minatnya, serta dilengkapi dengan deskripsi matakuliah, silabus dan rencana 
						pembelajaran.
					</p>
				</div>
				
				<div class="info-penilaian">Penilaian 5.1.3 Fleksibilitas mata kuliah pilihan</div>
				<button class="tablinks" style="display: none" id="defaultOpen" onclick="nextSimulation(event, '5.1.3')">5.1.3 Fleksibilitas mata kuliah pilihan</button>
				<form action="5.1.3/simpan" method="post">
					<div class="tabcontent" id="5.1.3">
						<h4>5.1.3 Fleksibilitas mata kuliah pilihan</h4>
						<p style="width: 210px;" class="teks-penilaian informasi">Informasi </p>:
						<p style="display: inline;">BMKP = Bobot mata kuliah pilihan dalam sks</p>
						<br/>
						<div class="input-penilaian">
							<p style="width: 210px;padding:6px 0;" class="teks-penilaian nilai">BMKP yang harus diambil <span class="required">*</span></p>:
							<label title="Jika BMKP ≥ 9 sks dan yang disediakan/ dilaksanakan ≥ 2 kali sks mata kuliah pilihan yang harus diambil, maka skor = 4. 
							Jika BMKP ≥ 9 sks dan ≥ 1 kali sks mata kuliah yang harus diambil, maka skor = 2 x RMKP. Jika BMKP < 9 sks atau < 1 kali sks mata kuliah pilihan yang harus diambil maka skor = 2.">
								<span title=""><input style="font-size:inherit;padding: 5px;margin: 10px 0;" oninput="hitungBobotxNilaiNumber()" type="number" title="Please enter a number between 0 - 150" name="nilai[1]" min="0" max="150" required class="nilai" id="input-penilaian-number1"/>
							</label></span>

							<br />
							<p style="width: 210px;padding-top:1px;" class="teks-penilaian nilai">SKS MK pilihan yang disediakan <span class="required">*</span></p>:
							<input readonly value="<?php echo $jumlahSksMkPilihan ?>" style="font-size:inherit;padding: 5px;margin: 5px 0;" type="number" title="Please enter a number between 0 - 150" name="nilai[2]" min="0" max="150" required class="nilai" id="input-penilaian-number2"/>
							<br />
							<p style="width: 210px;margin:10px 0;" class="teks-penilaian rasio">Rasio </p>:
							<input style="width: 30%;background:transparent; border:none; font-size:inherit;margin: 10px 0;" class="ptgs" id="nilai-rasio" value="0" readonly/>
							<br />
							<p style="width:210px;margin-bottom: 5px;" class="teks-penilaian nilai">Nilai </p>:
							<input style="width: 30%;background:transparent; border:none; font-size:inherit;margin-bottom: 5px;" name="nilai-number" class="nilai-akhir" id="nilaiakhir1" value="0" readonly/>
							<br />
							<p style="width: 210px;" class="teks-penilaian catatan">Catatan </p><p style="width:8px; float:left">:</p>
							<textarea style="margin-bottom: 10px;" type="text" name="catatan" class="catatan-penilaian" id="catatan"></textarea>
						</div>
						<button style="margin-left:218px" title="Simpan Penilaian" name="simpanBtn" type="submit" class="tablinks">Simpan</button>
					</div>
				</form>
				
			</div>
		</div>

		<div class="footer">
			<?php
			include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/footer.php";
			?>
		</div>
		<script>
		function hitungBobotxNilaiNumber() {
			var myBox1 = document.getElementById('input-penilaian-number1').value;	
			var myBox2 = document.getElementById('input-penilaian-number2').value;
			var rasio = document.getElementById('nilai-rasio');
			var nilaiAkhir = document.getElementById('nilaiakhir1');
			var rasioResult = myBox2 / myBox1;
			rasio.value = rasioResult;
			if(myBox1>=9){
				if(myBox2>=2){
					nilaiAkhir.value = 4;
				} else {
					var nilai = 2*myBox2;
					nilaiAkhir.value = nilai;
				}
			} else {
				nilaiAkhir.value = 2;
			} 
			if(myBox1 >= 150 || myBox2 >= 150){
				rasio.value = "Masukan hanya nilai 0 - 150";
				nilaiAkhir.value = "Masukan hanya nilai 0 - 150";
			} else if (myBox1 == 0 || myBox2 == 0 || myBox1 == null || myBox2 == null){
				rasio.value = 0;
				nilaiAkhir.value = 0;
			}
		}
		</script>
		
		
		<script>
		function nextSimulation(evt, simulationName) {
			// Declare all variables
			var i, tabcontent, tablinks;

			// Get all elements with class="tabcontent" and hide them
			tabcontent = document.getElementsByClassName("tabcontent");
			for (i = 0; i < tabcontent.length; i++) {
				tabcontent[i].style.display = "none";
			}

			// Get all elements with class="tablinks" and remove the class "active"
			tablinks = document.getElementsByClassName("tablinks");
			for (i = 0; i < tablinks.length; i++) {
				tablinks[i].className = tablinks[i].className.replace(" active", "");
			}

			// Show the current tab, and add an "active" class to the button that opened the tab
			document.getElementById(simulationName).style.display = "inline-table";
			evt.currentTarget.className += " active";
		}
		
		// Get the element with id="defaultOpen" and click on it
		document.getElementById("defaultOpen").click();
		</script>
		
		<script>
		(function (){	
			$(document).ready(function(){
			var acc = document.getElementsByClassName("accordion");
			var i;

			for (i = 0; i < acc.length; i++) {
			  acc[i].onclick = function() {
				this.classList.toggle("active");
				var panel = this.nextElementSibling;
				if (panel.style.maxHeight){
				  panel.style.maxHeight = null;
				  panel.style.paddingBottom = null;
				} else {
				  panel.style.maxHeight = panel.scrollHeight + "px";
				  panel.style.paddingBottom = "18px";
				} 
			  }
			}
			});
		})(jQuery);	
		</script>
		
	</body>

	
	
</html>
