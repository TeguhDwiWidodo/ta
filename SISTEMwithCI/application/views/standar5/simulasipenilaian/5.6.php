<?php
if(!isset($_SESSION['nama'])){
      header("location:" . base_url());
      exit();
   }	
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Akreditasi | Standar 5 | Simulasi Penilaian | 5.6 Upaya Perbaikan Pembelajaran</title>
		<script type = 'text/javascript' src="<?php echo base_url(); ?>js/jquery-1.12.4.js"></script>
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/mainlayout.css">
		<!--<link rel="stylesheet" href="<?php echo base_url(); ?>css/accordion.css">-->
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/jquery.dataTables.css">
		<!--<link rel="stylesheet" href="<?php echo base_url(); ?>css/data&dokumenstd5.css">-->
		<!--<link rel="stylesheet" href="<?php echo base_url(); ?>css/simulasipenilaian.css">-->
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/submenu.simulasipenilaian.css">
		<!--<link rel="stylesheet" href="<?php echo base_url(); ?>css/dokumenpendukung.css">-->
		<script type = 'text/javascript' src="<?php echo base_url(); ?>js/jquery.dataTables.js"></script>
		<script type = 'text/javascript' src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>
		<script type = 'text/javascript' >
		(function ($){	
			$(document).ready(function() {
				$('table').dataTable({
					"order": [],
					responsive: true,
					"scrollX": true,
					"scrollY": true,
					pageResize: true,
					autoWidth: true,
					columnDefs: [
						{ width: 10, targets: 0}
					]
					
				});
			});	
		})(jQuery);	
		</script>
	</head>
	
	<body>
		<div class="header">
			<h1><a href="<?php echo base_url(); ?>dashboard">Akreditasi SI</a></h1>
			<?php
				include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/header.php";
			?>
		</div>
		
		<div class="sidebar">
			<?php
				include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/sidebar.php";
			?>
		</div>
			
		<div class="main-layout">
			<div class="sub-header">
				<h2>Simulasi Penilaian Borang Program Studi Standar 5</h2>
			</div>
			<div class="sub-header">
				<h2>5.6 Upaya Perbaikan Pembelajaran</h2>
			</div>
			<div class="main-content">
				<button class="accordion">Upaya Perbaikan Pembelajaran</button>
				<div class="panel">
					<div class="panel-content">
						<table id="table-view-data-5-6" class="display" width="100%" cellspacing="0">
							<thead class="table head">
								<th>No</th>
								<th>Butir</th>
								<th>Upaya Perbaikan Tindakan</th>
								<th>Upaya Perbaikan Hasil</th>
							</thead>
								
							
							<tbody class="table body">
								<?php
								if($result_set56 !== "kosong" && $result_set56){
									foreach($result_set56 as $row){
									?>	
										<tr id="row1:<?php echo $row['no'] ?>">
											<td><?php echo $row['no'] ?></td> 	
											<td><?php echo $row['butir'] ?></td>
											<td><?php echo $row['upaya_perbaikan_tindakan'] ?></td>
											<td><?php echo $row['upaya_perbaikan_hasil'] ?></td>
											</td>
										</tr>
								<?php
									}
								}
								?>						
							</tbody>
						</table>
					</div>				
				</div>
				
				<div class="elemen-penilaian">Elemen Penilaian</div>
				<div class="deskripsi-elemen-penilaian">
					<p>
						5.6  Upaya perbaikan sistem pembelajaran yang telah dilakukan selama tiga  tahun terakhir
					</p>
				</div>
				
				<div class="info-penilaian">Penilaian 5.6 Upaya Perbaikan Pembelajaran</div>
				<button class="tablinks" style="display: none" id="defaultOpen" onclick="nextSimulation(event, '5.6')">5.6 Upaya Perbaikan Pembelajaran</button>
				<form action="5.6/simpan" method="post">
					<div class="tabcontent" id="5.6">
						<h4>5.6 Upaya Perbaikan Pembelajaran</h4>
						<p>Upaya perbaikan sistem pembelajaran yang telah dilakukan selama tiga tahun terakhir berkaitan dengan: </p>
						<ol style="padding-bottom: 10px; padding-left: 10px" type="a">
							<li>Materi</li>
							<li>Metode pembelajaran</li>
							<li>Penggunaan teknologi pembelajaran</li>
							<li>Cara-cara evaluasi</li>
						</ol>						
						<div class="input-penilaian">
							<p class="teks-penilaian nilai">Nilai <span class="required">*</span></p>:
							<label title="Upaya perbaikan dilakukan untuk  semua dari yang seharusnya diperbaiki/ ditingkatkan.">
								<span title="Sangat Baik (4)"><input onchange="hitungBobotxNilai('4','1')" type="radio" required class="nilai" name="nilai" id="4" value="4"/> Sangat Baik (4)
							</label></span>
							<label title="Upaya perbaikan dilakukan untuk 3 dari 4  yang seharusnya diperbaiki/ ditingkatkan.">
								<span title="Baik (3)"><input onchange="hitungBobotxNilai('3','1')" type="radio" required class="nilai" name="nilai" id="3" value="3"/> Baik (3)
							</label></span>
							<label title="Upaya perbaikan dilakukan untuk 2 dari 4 yang seharusnya diperbaiki/ ditingkatkan.">
								<span title="Cukup (2)"><input onchange="hitungBobotxNilai('2','1')" type="radio" required class="nilai" name="nilai" id="2" value="2"/> Cukup (2)
							</label></span>
							<label title="Upaya perbaikan dilakukan untuk 1 dari yang seharusnya diperbaiki/ ditingkatkan.">
								<span title="Kurang (1)"><input onchange="hitungBobotxNilai('1','1')" type="radio" required class="nilai" name="nilai" id="1" value="1"/> Kurang (1)
							</label></span>
							<label title="Tidak ada upaya perbaikan.">
								<span title="Sangat Kurang (0)"><input onchange="hitungBobotxNilai('0','1')" type="radio" required class="nilai" name="nilai" id="0" value="0"/> Sangat Kurang (0)
							</label></span>
							<br />
							<p class="teks-penilaian bobot">Bobot </p>:
							<input style="background:transparent; border:none; font-size:inherit;" class="bobot-penilaian" id="bobot1"  type="text" name="bobot" readonly value="0.57"/>
							<br />
							<p class="teks-penilaian bobotxnilai">Bobot X Nilai </p>:
							<input style="background:transparent; border:none; font-size:inherit;" class="bobotxnilai-penilaian" id="bobotxnilai1" value="0" readonly/>
							<br />
							<p class="teks-penilaian catatan">Catatan </p><p style="width:8px; float:left">:</p>
							<textarea name="catatan" class="catatan-penilaian" id="catatan1"></textarea>
						</div>
						<button title="Simpan Penilaian" name="simpanBtn" type="submit" class="tablinks">Simpan</button>
					</div>
				</form>
				
			</div>
		</div>

		<div class="footer">
			<?php
			include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/footer.php";
			?>
		</div>
		<script>
		function hitungBobotxNilai(rb, no) {
			var myBox1 = rb;	
			var myBox2 = document.getElementById('bobot'+no).value;
			var result = document.getElementById('bobotxnilai'+no);	
			var myResult = myBox1 * myBox2;
			result.value = myResult;			
		}
		</script>
		
		
		<script>
		function nextSimulation(evt, simulationName) {
			// Declare all variables
			var i, tabcontent, tablinks;

			// Get all elements with class="tabcontent" and hide them
			tabcontent = document.getElementsByClassName("tabcontent");
			for (i = 0; i < tabcontent.length; i++) {
				tabcontent[i].style.display = "none";
			}

			// Get all elements with class="tablinks" and remove the class "active"
			tablinks = document.getElementsByClassName("tablinks");
			for (i = 0; i < tablinks.length; i++) {
				tablinks[i].className = tablinks[i].className.replace(" active", "");
			}

			// Show the current tab, and add an "active" class to the button that opened the tab
			document.getElementById(simulationName).style.display = "inline-table";
			evt.currentTarget.className += " active";
		}
		
		// Get the element with id="defaultOpen" and click on it
		document.getElementById("defaultOpen").click();
		</script>
		
		<script>
		(function (){	
			$(document).ready(function(){
			var acc = document.getElementsByClassName("accordion");
			var i;

			for (i = 0; i < acc.length; i++) {
			  acc[i].onclick = function() {
				this.classList.toggle("active");
				var panel = this.nextElementSibling;
				if (panel.style.maxHeight){
				  panel.style.maxHeight = null;
				  panel.style.paddingBottom = null;
				} else {
				  panel.style.maxHeight = panel.scrollHeight + "px";
				  panel.style.paddingBottom = "18px";
				} 
			  }
			}
			});
		})(jQuery);	
		</script>
		
	</body>

	
	
</html>
