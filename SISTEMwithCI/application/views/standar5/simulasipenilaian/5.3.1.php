<?php
if(!isset($_SESSION['nama'])){
      header("location:" . base_url());
      exit();
   }	
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Akreditasi | Standar 5 | Simulasi Penilaian | 5.3.1 Mekanisme Penyusunan Materi Kuliah dan Monitoring Perkuliahan</title>
		<script type = 'text/javascript' src="<?php echo base_url(); ?>js/jquery-1.12.4.js"></script>
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/mainlayout.css">
		<!--<link rel="stylesheet" href="<?php echo base_url(); ?>css/accordion.css">-->
		<!--<link rel="stylesheet" href="<?php echo base_url(); ?>css/jquery.dataTables.css">-->
		<!--<link rel="stylesheet" href="<?php echo base_url(); ?>css/data&dokumenstd5.css">-->
		<!--<link rel="stylesheet" href="<?php echo base_url(); ?>css/simulasipenilaian.css">-->
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/submenu.simulasipenilaian.css">
		<!--<link rel="stylesheet" href="<?php echo base_url(); ?>css/dokumenpendukung.css">-->
		<!--<script type = 'text/javascript' src="<?php echo base_url(); ?>js/jquery.dataTables.js"></script>-->
		<script type = 'text/javascript' src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>

		
	</head>
	
	<body>
		<div class="header">
			<h1><a href="<?php echo base_url(); ?>dashboard">Akreditasi SI</a></h1>
			<?php
				include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/header.php";
			?>
		</div>
		
		<div class="sidebar">
			<?php
				include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/sidebar.php";
			?>
		</div>
			
		<div class="main-layout">
			<div class="sub-header">
				<h2>Simulasi Penilaian Borang Program Studi Standar 5</h2>
			</div>
			<div class="sub-header">
				<h2>5.3.1 Mekanisme Penyusunan Materi Kuliah dan Monitoring Perkuliahan</h2>
			</div>
			<div class="main-content">
				<button class="accordion">Mekanisme Penyusunan Materi Kuliah dan Monitoring Perkuliahan</button>
				<div class="panel">
					<div class="panel-content">
					<?php
						if($result_set531 !== "kosong" && $result_set531){
							foreach($result_set531 as $row){		
							?>		
								<?php echo $row['mekanisme_penyusunan']?>
							<?php
							}
						} else {
							?>
							<p>Mekanisme Penyusunan Materi Kuliah dan Monitoring Perkuliahan tidak ditemukan, silahkan masukan Mekanisme Penyusunan Materi Kuliah dan Monitoring Perkuliahan <a href="<?php echo base_url(); ?>standar5/pembelajaran/5.3ppp">di sini</a>.</p>
						<?php	
						}
						?>
					</div>
				</div>
				
				<div class="elemen-penilaian">Elemen Penilaian</div>
				<div class="deskripsi-elemen-penilaian">
					<p>
						5.3 Pelaksanaan proses pembelajaran
					</p>
				</div>
				
				<div class="info-penilaian">Penilaian 5.3.1.a Pelaksanaan pembelajaran &amp; 5.3.1.b Mekanisme penyusunan materi perkuliahan</div>
				<button class="tablinks" style="display: none" id="defaultOpen" onclick="nextSimulation(event, '5.3.1.a')">5.3.1.a Pelaksanaan pembelajaran memiliki mekanisme untuk memonitor, mengkaji, dan memperbaiki secara periodik kegiatan perkuliahan (kehadiran dosen dan mahasiswa), penyusunan materi perkuliahan, serta penilaian hasil belajar.</button>
				<form action="5.3.1/simpan" method="post">
					<div class="tabcontent" id="5.3.1.a">
						<h4>5.3.1.a Pelaksanaan pembelajaran.</h4>
						<p >Pelaksanaan pembelajaran memiliki mekanisme untuk memonitor, mengkaji, dan memperbaiki secara periodik kegiatan perkuliahan (kehadiran dosen dan mahasiswa), penyusunan materi perkuliahan, serta penilaian hasil belajar.</p>
						<p style="float: left;width:100%">Cara penghitungan :</p>
						<p style="height:30px;width:70px;padding-top: 6px;font-size:25px;float:left;">NA  =</p>
						<p style="font-size:15px;display:inline;">Jumlah skor untuk tiga aspek</p>
						<hr class="penilaian-bagi"/>
						<p style="font-size:15px;display:inline;padding-left: 91px;">3</p>
						<p>Sedangkan penghitungan skor untuk setiap butir sebagai berikut:</p>	  
							<ol style="list-style-type:none">
								<li>1: Tidak ada monitoring</li>
								<li>2: Ada monitoring tetapi tidak ada evaluasi</li>
								<li>3: Ada monitoring, evaluasi tidak kontinu</li>
								<li>4: Ada monitoring  dan evaluasi secara kontinu</li>
							</ol>
						<p>Catatan:</p>		
						<p>Maksimal nilai 4 untuk skor setiap butir dan Maksimal nilai 12 untuk Jumlah skor untuk tiga aspek</p>		
						<div style="padding-top:10px" class="input-penilaian">
							<p class="teks-penilaian inputskor">Jumlah skor untuk tiga aspek <span class="required">*</span></p>:
							<label title="Skor = NA.">
								<span title=""><input style="font-size:inherit;padding: 5px;" oninput="hitungBobotxNilaiNumber('1')" type="number" title="Please enter a number between 3 - 12" name="nilai[0]" min="3" max="12" required class="nilai" id="input-penilaian-number1"/>
							</label></span>
							<br />
							<p style="width:260px;margin-top: 5px;" class="teks-penilaian nilai">Nilai </p>:
							<input style="width:30%;background:transparent; border:none; font-size:inherit;padding: 5px 0;" class="nilai/3-penilaian" id="bobot/nilai1" value="0" readonly/>
							<br />
							<p style="width:260px" class="teks-penilaian catatan">Catatan </p><p style="width:8px; float:left">:</p>
							<textarea name="catatan" class="catatan-penilaian" id="catatan1"></textarea>
						</div>
						<button  style="margin-left:269px" type="button" title="Penilaian Selanjutnya" class="tablinks" onclick="nextSimulation(event, '5.3.1.b')">Selanjutnya</button>
					</div>
					
					<div class="tabcontent" id="5.3.1.b">
						<h4>5.3.1.b  Mekanisme penyusunan materi perkuliahan</h4>
						<div class="input-penilaian">
							<p class="teks-penilaian nilai">Nilai <span class="required">*</span></p>:
							<label title="Materi kuliah disusun oleh kelompok dosen dalam satu bidang ilmu, dengan memperhatikan masukan dari dosen lain atau dari pengguna lulusan.">
								<span title="Sangat Baik (4)"><input onchange="hitungBobotxNilai('4','2')" type="radio" required class="nilai" name="nilai[1]" id="4" value="4"/> Sangat Baik (4)
							</label></span>
							<label title="Materi kuliah disusun oleh kelompok dosen dalam satu bidang ilmu, dengan memperhatikan masukan dari dosen lain.">
								<span title="Baik (3)"><input onchange="hitungBobotxNilai('3','2')" type="radio" required class="nilai" name="nilai[1]" id="3" value="3"/> Baik (3)
							</label></span>
							<label title="Materi kuliah disusun oleh kelompok dosen dalam satu bidang ilmu.">
								<span title="Cukup (2)"><input onchange="hitungBobotxNilai('2','2')" type="radio" required class="nilai" name="nilai[1]" id="2" value="2"/> Cukup (2)
							</label></span>
							<label title="Materi kuliah hanya disusun oleh dosen pengajar tanpa melibatkan dosen lain.">
								<span title="Kurang (1)"><input onchange="hitungBobotxNilai('1','2')" type="radio" required class="nilai" name="nilai[1]" id="1" value="1"/> Kurang (1)
							</label></span>
							<label title="Tidak ada mekanisme monitoring.">
								<span title="Sangat Kurang (0)"><input onchange="hitungBobotxNilai('0','2')" type="radio" required class="nilai" name="nilai[1]" id="0" value="0"/> Sangat Kurang (0)
							</label></span>
							<br />
							<p class="teks-penilaian bobot">Bobot </p>:
							<input style="background:transparent; border:none; font-size:inherit;" class="bobot-penilaian" id="bobot2"  type="text" name="bobot" readonly value="0.57"/>
							<br />
							<p class="teks-penilaian bobotxnilai">Bobot X Nilai </p>:
							<input style="background:transparent; border:none; font-size:inherit;" class="bobotxnilai-penilaian" id="bobotxnilai2" value="0" readonly/>
							<br />
							<p class="teks-penilaian catatan">Catatan </p><p style="width:8px; float:left">:</p>
							<textarea name="catatan" class="catatan-penilaian" id="catatan2"></textarea>
						</div>
						<button title="Penilaian Sebelumnya" style="margin-right: 10px" type="button" class="tablinks" onclick="nextSimulation(event, '5.3.1.a')">Sebelumnya</button>
						<button title="Simpan Penilaian" style="margin-left: unset" name="simpanBtn" type="submit" class="tablinks">Simpan</button>
					</div>
				</form>
				
			</div>
		</div>

		<div class="footer">
			<?php
			include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/footer.php";
			?>
		</div>
		<script>
		function hitungBobotxNilaiNumber(no) {
			var myBox1 = document.getElementById('input-penilaian-number'+no).value;	
			//var myBox2 = document.getElementById('bobot'+no).value;
			var result = document.getElementById('bobot/nilai'+no);	
			var myResult = myBox1/3;
			result.value = myResult;
			if(myBox1 >= 120 || myBox2 >= 120){
				result.value = "Masukan hanya nilai 0 - 120";
			} else if (myBox1 == 0 || myBox2 == 0 || myBox1 == null || myBox2 == null){
				result.value = 0;
			}			
		}
		</script>
		<script>
		function hitungBobotxNilai(rb, no) {
			var myBox1 = rb;	
			var myBox2 = document.getElementById('bobot'+no).value;
			var result = document.getElementById('bobotxnilai'+no);	
			var myResult = myBox1 * myBox2;
			result.value = myResult;			
		}
		</script>
		
		
		<script>
		function nextSimulation(evt, simulationName) {
			// Declare all variables
			var i, tabcontent, tablinks;

			// Get all elements with class="tabcontent" and hide them
			tabcontent = document.getElementsByClassName("tabcontent");
			for (i = 0; i < tabcontent.length; i++) {
				tabcontent[i].style.display = "none";
			}

			// Get all elements with class="tablinks" and remove the class "active"
			tablinks = document.getElementsByClassName("tablinks");
			for (i = 0; i < tablinks.length; i++) {
				tablinks[i].className = tablinks[i].className.replace(" active", "");
			}

			// Show the current tab, and add an "active" class to the button that opened the tab
			document.getElementById(simulationName).style.display = "inline-table";
			evt.currentTarget.className += " active";
		}
		
		// Get the element with id="defaultOpen" and click on it
		document.getElementById("defaultOpen").click();
		</script>
		
		<script>
		(function (){	
			$(document).ready(function(){
			var acc = document.getElementsByClassName("accordion");
			var i;

			for (i = 0; i < acc.length; i++) {
			  acc[i].onclick = function() {
				this.classList.toggle("active");
				var panel = this.nextElementSibling;
				if (panel.style.maxHeight){
				  panel.style.maxHeight = null;
				  panel.style.paddingBottom = null;
				} else {
				  panel.style.maxHeight = panel.scrollHeight + "px";
				  panel.style.paddingBottom = "18px";
				} 
			  }
			}
			});
		})(jQuery);	
		</script>
		
	</body>

	
	
</html>
