<?php
if(!isset($_SESSION['nama'])){
      header("location:" . base_url());
      exit();
   }	
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Akreditasi | Standar 5 | Simulasi Penilaian | 5.7 Upaya Peningkatan Suasana Akademik</title>
		<script type = 'text/javascript' src="<?php echo base_url(); ?>js/jquery-1.12.4.js"></script>
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/mainlayout.css">
		<!--<link rel="stylesheet" href="<?php echo base_url(); ?>css/accordion.css">-->
		<!--<link rel="stylesheet" href="<?php echo base_url(); ?>css/jquery.dataTables.css">-->
		<!--<link rel="stylesheet" href="<?php echo base_url(); ?>css/data&dokumenstd5.css">-->
		<!--<link rel="stylesheet" href="<?php echo base_url(); ?>css/simulasipenilaian.css">-->
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/submenu.simulasipenilaian.css">
		<!--<link rel="stylesheet" href="<?php echo base_url(); ?>css/dokumenpendukung.css">-->
		<!--<script type = 'text/javascript' src="<?php echo base_url(); ?>js/jquery.dataTables.js"></script>-->
		<script type = 'text/javascript' src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>
		
	</head>
	
	<body>
		<div class="header">
			<h1><a href="<?php echo base_url(); ?>dashboard">Akreditasi SI</a></h1>
			<?php
				include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/header.php";
			?>
		</div>
		
		<div class="sidebar">
			<?php
				include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/sidebar.php";
			?>
		</div>
			
		<div class="main-layout">
			<div class="sub-header">
				<h2>Simulasi Penilaian Borang Program Studi Standar 5</h2>
			</div>
			<div class="sub-header">
				<h2>5.7 Upaya Peningkatan Suasana Akademik</h2>
			</div>
			<div class="main-content">
				<button class="accordion">Kebijakan tentang suasana akademik (otonomi keilmuan, kebebasan akademik, kebebasan mimbar akademik)</button>
				<div class="panel">
					<div class="panel-content">
					<?php
						if($result_set571 !== "kosong" && $result_set571){
							foreach($result_set571 as $row){		
							?>		
								<?php echo $row['kebijakan_suasana_akademik']?>
							<?php
							}
						} else {
							?>
							<p>Kebijakan tentang suasana akademik (otonomi keilmuan, kebebasan akademik, kebebasan mimbar akademik) tidak ditemukan, silahkan masukan Kebijakan tentang suasana akademik (otonomi keilmuan, kebebasan akademik, kebebasan mimbar akademik) <a href="<?php echo base_url(); ?>standar5/sa/5.7upayapeningkatan">di sini</a>.</p>
						<?php	
						}	
						?>
					</div>
				</div>
				
				<button class="accordion">Ketersediaan dan Jenis Prasarana, Sarana dan Dana yang Memungkinkan Terciptanya Interaksi Akademik Antara Sivitas Akademika</button>
				<div class="panel">
					<div class="panel-content">
						<?php
						if($result_set572 !== "kosong" && $result_set572){
							foreach($result_set572 as $row){		
								echo $row['ketersediaan_dan_jenis_prasarana'];
							}
						} else {
							?>
							<p>Ketersediaan dan Jenis Prasarana, Sarana dan Dana yang Memungkinkan Terciptanya Interaksi Akademik Antara Sivitas Akademika tidak ditemukan, silahkan masukan Ketersediaan dan Jenis Prasarana, Sarana dan Dana yang Memungkinkan Terciptanya Interaksi Akademik Antara Sivitas Akademika <a href="<?php echo base_url(); ?>standar5/sa/5.7upayapeningkatan">di sini</a>.</p>
						<?php	
						}	
						?>
					</div>
				</div>
				
				<button class="accordion">Program dan kegiatan di dalam dan di luar proses pembelajaran, yang dilaksanakan baik di dalam maupun di luar kelas,  untuk menciptakan suasana akademik yang kondusif</button>
				<div class="panel">
					<div class="panel-content">
						<?php
						if($result_set573 !== "kosong" && $result_set573){
							foreach($result_set573 as $row){		
								echo $row['program_dan_kegiatan'];
							}
						} else {
							?>
							<p>Program dan kegiatan di dalam dan di luar proses pembelajaran, yang dilaksanakan baik di dalam maupun di luar kelas,  untuk menciptakan suasana akademik yang kondusif tidak ditemukan, silahkan masukan Program dan kegiatan di dalam dan di luar proses pembelajaran, yang dilaksanakan baik di dalam maupun di luar kelas,  untuk menciptakan suasana akademik yang kondusif <a href="<?php echo base_url(); ?>standar5/sa/5.7upayapeningkatan">di sini</a>.</p>
						<?php	
						}	
						?>
					</div>
				</div>
				
				<button class="accordion">Interaksi akademik antara dosen-mahasiswa, antar mahasiswa, serta antar dosen</button>
				<div class="panel">
					<div class="panel-content">
						<?php
						if($result_set574 !== "kosong" && $result_set574){
							foreach($result_set574 as $row){		
								echo $row['interaksi_akademik'];
							}
						} else {
							?>
							<p>Interaksi akademik antara dosen-mahasiswa, antar mahasiswa, serta antar dosen tidak ditemukan, silahkan masukan Interaksi akademik antara dosen-mahasiswa, antar mahasiswa, serta antar dosen <a href="<?php echo base_url(); ?>standar5/sa/5.7upayapeningkatan">di sini</a>.</p>
						<?php	
						}	
						?>
					</div>
				</div>
				
				<button class="accordion">Pengembangan perilaku kecendekiawanan</button>
				<div class="panel">
					<div class="panel-content">
						<?php
						if($result_set575 !== "kosong" && $result_set575){
							foreach($result_set575 as $row){			
								echo $row['pengembangan_perilaku_kecendekiawanan'];
							}
						} else {
							?>
							<p>Pengembangan perilaku kecendekiawanan tidak ditemukan, silahkan masukan Pengembangan perilaku kecendekiawanan <a href="<?php echo base_url(); ?>standar5/sa/5.7upayapeningkatan">di sini</a>.</p>
						<?php	
						}	
						?>
					</div>
				</div>
				
				<div class="elemen-penilaian">Elemen Penilaian</div>
				<div class="deskripsi-elemen-penilaian">
					<p>
						5.7 Upaya peningkatan suasana akademik: Kebijakan tentang suasana akademik, Ketersediaan dan jenis prasarana, sarana dan dana, Program dan kegiatan akademik untuk menciptakan suasana akademik, Interaksi akademik antara dosen-mahasiswa, serta pengembangan perilaku kecendekiawanan
					</p>
				</div>
				
				<div class="info-penilaian">Penilaian 5.7 Upaya Peningkatan Suasana Akademik</div>
				<button class="tablinks" style="display: none" id="defaultOpen" onclick="nextSimulation(event, '5.7.1')">5.7.1 Kebijakan tertulis tentang suasana akademik (otonomi keilmuan, kebebasan akademik, kebebasan mimbar akademik, kemitraan dosen-mahasiswa).</button>
				<form action="5.7/simpan" method="post">
					<div class="tabcontent" id="5.7.1">
						<h4>5.7.1 Kebijakan tertulis tentang suasana akademik (otonomi keilmuan, kebebasan akademik, kebebasan mimbar akademik, kemitraan dosen-mahasiswa).</h4>
						<div class="input-penilaian">
							<p class="teks-penilaian nilai">Nilai <span class="required">*</span></p>:
							<label title="Kebijakan lengkap mencakup informasi tentang otonomi keilmuan, kebebasan akademik, kebebasan mimbar akademik, dan kemitraan dosen-mahasiswa, serta dilaksanakan secara konsisten.">
								<span title="Sangat Baik (4)"><input onchange="hitungBobotxNilai('4','1')" type="radio" required class="nilai" name="nilai[0]" id="4" value="4"/> Sangat Baik (4)
							</label></span>
							<label title="Kebijakan lengkap mencakup informasi tentang otonomi keilmuan, kebebasan akademik, kebebasan mimbar akademik, dan kemitraan dosen-mahasiswa, namun tidak dilaksanakan secara konsisten.">
								<span title="Baik (3)"><input onchange="hitungBobotxNilai('3','1')" type="radio" required class="nilai" name="nilai[0]" id="3" value="3"/> Baik (3)
							</label></span>
							<label title="Kebijakan tertulis kurang lengkap.">
								<span title="Cukup (2)"><input onchange="hitungBobotxNilai('2','1')" type="radio" required class="nilai" name="nilai[0]" id="2" value="2"/> Cukup (2)
							</label></span>
							<label title="Tidak ada kebijakan tertulis tentang otonomi keilmuan, kebebasan akademik, kebebasan mimbar akademik, dan kemitraan dosen-mahasiswa.">
								<span title="Kurang (1)"><input onchange="hitungBobotxNilai('1','1')" type="radio" required class="nilai" name="nilai[0]" id="1" value="1"/> Kurang (1)
							</label></span>
							<label title="(Tidak ada skor nol)">
								<span title="Sangat Kurang (0)"><input onchange="hitungBobotxNilai('0','1')" type="radio" required class="nilai" name="nilai[0]" id="0" value="0"/> Sangat Kurang (0)
							</label></span>
							<br />
							<p class="teks-penilaian bobot">Bobot </p>:
							<input style="background:transparent; border:none; font-size:inherit;" class="bobot-penilaian" id="bobot1"  type="text" name="bobot" readonly value="0.57"/>
							<br />
							<p class="teks-penilaian bobotxnilai">Bobot X Nilai </p>:
							<input style="background:transparent; border:none; font-size:inherit;" class="bobotxnilai-penilaian" id="bobotxnilai1" value="0" readonly/>
							<br />
							<p class="teks-penilaian catatan">Catatan </p><p style="width:8px; float:left">:</p>
							<textarea name="catatan[]" class="catatan-penilaian" id="catatan1"></textarea>
						</div>
						<button type="button" title="Penilaian Selanjutnya" class="tablinks" onclick="nextSimulation(event, '5.7.2')">Selanjutnya</button>
					</div>
					<div class="tabcontent" id="5.7.2">
						<h4>5.7.2 Ketersediaan dan kelengkapan jenis prasarana, sarana serta dana yang memungkinkan terciptanya interaksi akademik antara sivitas akademika.</h4>
						<div class="input-penilaian">
							<p class="teks-penilaian nilai">Nilai <span class="required">*</span></p>:
							<label title="Tersedia, milik sendiri,  sangat lengkap dan dana yang sangat memadai.">
								<span title="Sangat Baik (4)"><input onchange="hitungBobotxNilai('4','2')" type="radio" required class="nilai" name="nilai[1]" id="4" value="4"/> Sangat Baik (4)
							</label></span>
							<label title="Tersedia, milik sendiri, lengkap, dan dana yang memadai.">
								<span title="Baik (3)"><input onchange="hitungBobotxNilai('3','2')" type="radio" required class="nilai" name="nilai[1]" id="3" value="3"/> Baik (3)
							</label></span>
							<label title="Tersedia, cukup lengkap, milik sendiri atau sewa, dan dana yang cukup memadai.">
								<span title="Cukup (2)"><input onchange="hitungBobotxNilai('2','2')" type="radio" required class="nilai" name="nilai[1]" id="2" value="2"/> Cukup (2)
							</label></span>
							<label title="Prasarana utama masih kurang, demikian pula dengan dukungan dana.">
								<span title="Kurang (1)"><input onchange="hitungBobotxNilai('1','2')" type="radio" required class="nilai" name="nilai[1]" id="1" value="1"/> Kurang (1)
							</label></span>
							<label title="(Tidak ada skor nol)">
								<span title="Sangat Kurang (0)"><input onchange="hitungBobotxNilai('0','2')" type="radio" required class="nilai" name="nilai[1]" id="0" value="0"/> Sangat Kurang (0)
							</label></span>
							<br />
							<p class="teks-penilaian bobot">Bobot </p>:
							<input style="background:transparent; border:none; font-size:inherit;" class="bobot-penilaian" id="bobot2"  type="text" name="bobot" readonly value="1.14"/>
							<br />
							<p class="teks-penilaian bobotxnilai">Bobot X Nilai </p>:
							<input style="background:transparent; border:none; font-size:inherit;" class="bobotxnilai-penilaian" id="bobotxnilai2" value="0" readonly/>
							<br />
							<p class="teks-penilaian catatan">Catatan </p><p style="width:8px; float:left">:</p>
							<textarea type="text" name="catatan[]" class="catatan-penilaian" id="catatan2"></textarea>
						</div>
						
						<button title="Penilaian Sebelumnya" style="margin-right: 10px" type="button" class="tablinks" onclick="nextSimulation(event, '5.7.1')">Sebelumnya</button>
						<button type="button" title="Penilaian Selanjutnya" style="margin-left:unset" class="tablinks" onclick="nextSimulation(event, '5.7.3')">Selanjutnya</button>
					</div>
					<div class="tabcontent" id="5.7.3">
						<h4>5.7.3 Interaksi akademik berupa program dan kegiatan akademik, selain perkuliahan dan tugas-tugas khusus, untuk menciptakan suasana akademik (seminar, simposium, lokakarya, bedah buku dll).</h4>
						<div class="input-penilaian">
							<p class="teks-penilaian nilai">Nilai <span class="required">*</span></p>:
							<label title="Kegiatan ilmiah yang terjadwal dilaksanakan setiap bulan.">
								<span title="Sangat Baik (4)"><input onchange="hitungBobotxNilai('4','3')" type="radio" required class="nilai" name="nilai[2]" id="4" value="4"/> Sangat Baik (4)
							</label></span>
							<label title="Kegiatan ilmiah yang terjadwal dilaksanakan dua s.d tiga bulan sekali.">
								<span title="Baik (3)"><input onchange="hitungBobotxNilai('3','3')" type="radio" required class="nilai" name="nilai[2]" id="3" value="3"/> Baik (3)
							</label></span>
							<label title="Kegiatan ilmiah yang terjadwal dilaksanakan empat s.d. enam bulan sekali.">
								<span title="Cukup (2)"><input onchange="hitungBobotxNilai('2','3')" type="radio" required class="nilai" name="nilai[2]" id="2" value="2"/> Cukup (2)
							</label></span>
							<label title="Kegiatan ilmiah yang terjadwal dilaksanakan lebih dari enam bulan sekali.">
								<span title="Kurang (1)"><input onchange="hitungBobotxNilai('1','3')" type="radio" required class="nilai" name="nilai[2]" id="1" value="1"/> Kurang (1)
							</label></span>
							<label title="(Tidak ada skor nol)">
								<span title="Sangat Kurang (0)"><input onchange="hitungBobotxNilai('0','3')" type="radio" required class="nilai" name="nilai[2]" id="0" value="0"/> Sangat Kurang (0)
							</label></span>
							<br />
							<p class="teks-penilaian bobot">Bobot </p>:
							<input style="background:transparent; border:none; font-size:inherit;" class="bobot-penilaian" id="bobot3"  type="text" name="bobot" readonly value="1.14"/>
							<br />
							<p class="teks-penilaian bobotxnilai">Bobot X Nilai </p>:
							<input style="background:transparent; border:none; font-size:inherit;" class="bobotxnilai-penilaian" id="bobotxnilai3" value="0" readonly/>
							<br />
							<p class="teks-penilaian catatan">Catatan </p><p style="width:8px; float:left">:</p>
							<textarea type="text" name="catatan[]" class="catatan-penilaian" id="catatan3"></textarea>
						</div>
						
						<button title="Penilaian Sebelumnya" style="margin-right: 10px" type="button" class="tablinks" onclick="nextSimulation(event, '5.7.2')">Sebelumnya</button>
						<button type="button" title="Penilaian Selanjutnya" style="margin-left:unset" class="tablinks" onclick="nextSimulation(event, '5.7.4')">Selanjutnya</button>
					</div>
					<div class="tabcontent" id="5.7.4">
						<h4>5.7.4 Interaksi akademik antara dosen-mahasiswa</h4>
						<div class="input-penilaian">
							<p class="teks-penilaian nilai">Nilai <span class="required">*</span></p>:
							<label title="Upaya baik dan hasilnya suasana kondusif untuk meningkatkan suasana akademik yang baik.">
								<span title="Sangat Baik (4)"><input onchange="hitungBobotxNilai('4','4')" type="radio" required class="nilai" name="nilai[3]" id="4" value="4"/> Sangat Baik (4)
							</label></span>
							<label title="Upaya baik, namun hasilnya baru cukup">
								<span title="Baik (3)"><input onchange="hitungBobotxNilai('3','4')" type="radio" required class="nilai" name="nilai[3]" id="3" value="3"/> Baik (3)
							</label></span>
							<label title="Cukup dalam upaya dan hasilnya.">
								<span title="Cukup (2)"><input onchange="hitungBobotxNilai('2','4')" type="radio" required class="nilai" name="nilai[3]" id="2" value="2"/> Cukup (2)
							</label></span>
							<label title="Upaya dinilai kurang dan hasilnya tidak nampak, atau tidak ada upaya.">
								<span title="Kurang (1)"><input onchange="hitungBobotxNilai('1','4')" type="radio" required class="nilai" name="nilai[3]" id="1" value="1"/> Kurang (1)
							</label></span>
							<label title="(Tidak ada skor nol)">
								<span title="Sangat Kurang (0)"><input onchange="hitungBobotxNilai('0','4')" type="radio" required class="nilai" name="nilai[3]" id="0" value="0"/> Sangat Kurang (0)
							</label></span>
							<br />
							<p class="teks-penilaian bobot">Bobot </p>:
							<input style="background:transparent; border:none; font-size:inherit;" class="bobot-penilaian" id="bobot4"  type="text" name="bobot" readonly value="0.57"/>
							<br />
							<p class="teks-penilaian bobotxnilai">Bobot X Nilai </p>:
							<input style="background:transparent; border:none; font-size:inherit;" class="bobotxnilai-penilaian" id="bobotxnilai4" value="0" readonly/>
							<br />
							<p class="teks-penilaian catatan">Catatan </p><p style="width:8px; float:left">:</p>
							<textarea type="text" name="catatan[]" class="catatan-penilaian" id="catatan4"></textarea>
						</div>
						
						<button title="Penilaian Sebelumnya" style="margin-right: 10px" type="button" class="tablinks" onclick="nextSimulation(event, '5.7.3')">Sebelumnya</button>
						<button type="button" title="Penilaian Selanjutnya" style="margin-left:unset" class="tablinks" onclick="nextSimulation(event, '5.7.5')">Selanjutnya</button>
					</div>
					<div class="tabcontent" id="5.7.5">
						<h4>5.7.5  Pengembangan perilaku kecendekiawanan</h4>
						<p>Bentuk kegiatan antara lain dapat berupa :</p>
						<ol style="padding-left: 10px;padding-bottom: 10px">
							<li>Kegiatan penanggulangan kemiskinan.</li>
							<li>Pelestarian lingkungan.</li>
							<li>Peningkatan kesejahteraan masyarakat.</li>
							<li>Kegiatan penanggulangan masalah  ekonomi, politik, sosial, budaya, dan lingkungan lainnya.</li>
						</ol>
						<div class="input-penilaian">
							<p class="teks-penilaian nilai">Nilai <span class="required">*</span></p>:
							<label title="Kegiatan yang dilakukan sangat menunjang pengembangan perilaku kecendekiawanan.">
								<span title="Sangat Baik (4)"><input onchange="hitungBobotxNilai('4','5')" type="radio" required class="nilai" name="nilai[4]" id="4" value="4"/> Sangat Baik (4)
							</label></span>
							<label title="Upaya baik, namun hasilnya baru cukup">
								<span title="Baik (3)"><input onchange="hitungBobotxNilai('3','5')" type="radio" required class="nilai" name="nilai[4]" id="3" value="3"/> Baik (3)
							</label></span>
							<label title="Cukup dalam upaya dan hasilnya.">
								<span title="Cukup (2)"><input onchange="hitungBobotxNilai('2','5')" type="radio" required class="nilai" name="nilai[4]" id="2" value="2"/> Cukup (2)
							</label></span>
							<label title="Upaya dinilai kurang dan hasilnya tidak nampak, atau tidak ada upaya.">
								<span title="Kurang (1)"><input onchange="hitungBobotxNilai('1','5')" type="radio" required class="nilai" name="nilai[4]" id="1" value="1"/> Kurang (1)
							</label></span>
							<label title="(Tidak ada skor nol)">
								<span title="Sangat Kurang (0)"><input onchange="hitungBobotxNilai('0','5')" type="radio" required class="nilai" name="nilai[4]" id="0" value="0"/> Sangat Kurang (0)
							</label></span>
							<br />
							<p class="teks-penilaian bobot">Bobot </p>:
							<input style="background:transparent; border:none; font-size:inherit;" class="bobot-penilaian" id="bobot5"  type="text" name="bobot" readonly value="0.57"/>
							<br />
							<p class="teks-penilaian bobotxnilai">Bobot X Nilai </p>:
							<input style="background:transparent; border:none; font-size:inherit;" class="bobotxnilai-penilaian" id="bobotxnilai5" value="0" readonly/>
							<br />
							<p class="teks-penilaian catatan">Catatan </p><p style="width:8px; float:left">:</p>
							<textarea type="text" name="catatan[]" class="catatan-penilaian" id="catatan5"></textarea>
						</div>
						
						<button title="Penilaian Sebelumnya" style="margin-right: 10px" type="button" class="tablinks" onclick="nextSimulation(event, '5.7.3')">Sebelumnya</button>
						<button title="Simpan Penilaian" style="margin-left:unset" name="simpanBtn" type="submit" class="tablinks">Simpan</button>
					</div>
				</form>
				
			</div>
		</div>

		<div class="footer">
			<?php
			include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/footer.php";
			?>
		</div>
		<script>
		function hitungBobotxNilai(rb, no) {
			var myBox1 = rb;	
			var myBox2 = document.getElementById('bobot'+no).value;
			var result = document.getElementById('bobotxnilai'+no);	
			var myResult = myBox1 * myBox2;
			result.value = myResult;			
		}
		</script>
		
		
		<script>
		function nextSimulation(evt, simulationName) {
			// Declare all variables
			var i, tabcontent, tablinks;

			// Get all elements with class="tabcontent" and hide them
			tabcontent = document.getElementsByClassName("tabcontent");
			for (i = 0; i < tabcontent.length; i++) {
				tabcontent[i].style.display = "none";
			}

			// Get all elements with class="tablinks" and remove the class "active"
			tablinks = document.getElementsByClassName("tablinks");
			for (i = 0; i < tablinks.length; i++) {
				tablinks[i].className = tablinks[i].className.replace(" active", "");
			}

			// Show the current tab, and add an "active" class to the button that opened the tab
			document.getElementById(simulationName).style.display = "inline-table";
			evt.currentTarget.className += " active";
		}
		
		// Get the element with id="defaultOpen" and click on it
		document.getElementById("defaultOpen").click();
		</script>
		
		<script>
		(function (){	
			$(document).ready(function(){
			var acc = document.getElementsByClassName("accordion");
			var i;

			for (i = 0; i < acc.length; i++) {
			  acc[i].onclick = function() {
				this.classList.toggle("active");
				var panel = this.nextElementSibling;
				if (panel.style.maxHeight){
				  panel.style.maxHeight = null;
				  panel.style.paddingBottom = null;
				} else {
				  panel.style.maxHeight = panel.scrollHeight + "px";
				  panel.style.paddingBottom = "18px";
				} 
			  }
			}
			});
		})(jQuery);	
		</script>
		
	</body>

	
	
</html>
