<?php
if(!isset($_SESSION['nama'])){
      header("location:" . base_url());
      exit();
   }	
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Akreditasi | Standar 5 | Simulasi Penilaian | 5.1.1 Komptensi Lulusan</title>
		<script type = 'text/javascript' src="<?php echo base_url(); ?>js/jquery-1.12.4.js"></script>
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/mainlayout.css">
		<!--<link rel="stylesheet" href="<?php echo base_url(); ?>css/accordion.css">-->
		<!--<link rel="stylesheet" href="<?php echo base_url(); ?>css/jquery.dataTables.css">-->
		<!--<link rel="stylesheet" href="<?php echo base_url(); ?>css/data&dokumenstd5.css">-->
		<!--<link rel="stylesheet" href="<?php echo base_url(); ?>css/simulasipenilaian.css">-->
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/submenu.simulasipenilaian.css">
		<!--<link rel="stylesheet" href="<?php echo base_url(); ?>css/dokumenpendukung.css">-->
		<!--<script type = 'text/javascript' src="<?php echo base_url(); ?>js/jquery.dataTables.js"></script>-->
		<script type = 'text/javascript' src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>
		
	</head>
	
	<body>
		<div class="header">
			<h1><a href="<?php echo base_url(); ?>dashboard">Akreditasi SI</a></h1>
			<?php
				include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/header.php";
			?>
		</div>
		
		<div class="sidebar">
			<?php
				include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/sidebar.php";
			?>
		</div>
			
		<div class="main-layout">
			<div class="sub-header">
				<h2>Simulasi Penilaian Borang Program Studi Standar 5</h2>
			</div>
			<div class="sub-header">
				<h2>5.1.1 Komptensi Lulusan</h2>
			</div>
			<div class="main-content">
				<button class="accordion">Uraian ringkas kompetensi utama lulusan PRODI SI</button>
				<div class="panel">
					<div class="panel-content">
						<?php
						if($result_set511 !== "kosong" && $result_set511){
							foreach($result_set511 as $row){		
							?>		
								<p><?php echo $row['kompetensi_utama']?></p>
							<?php
							}
						} else {
							?>
							<p>Uraian ringkas kompetensi utama tidak ditemukan, silahkan masukan Uraian ringkas kompetensi utama <a href="<?php echo base_url(); ?>standar5/kurikulum/5.1kurikulum">di sini</a>.</p>
						<?php	
						}	
						?>
					</div>
				</div>
				
				<button class="accordion">Uraian ringkas kompetensi pendukung lulusan PRODI SI</button>
				<div class="panel">
					<div class="panel-content">
						<?php
						if($result_set511 !== "kosong" && $result_set511){
							foreach($result_set511 as $row){		
							?>		
								<p><?php echo $row['kompetensi_pendukung']?></p>
							<?php
							}
						} else {
							?>
							<p>Uraian ringkas kompetensi utama tidak ditemukan, silahkan masukan Uraian ringkas kompetensi utama <a href="<?php echo base_url(); ?>standar5/kurikulum/5.1kurikulum">di sini</a>.</p>
						<?php	
						}	
						?>
					</div>
				</div>
				
				<button class="accordion">Uraian ringkas kompetensi lainnya lulusan PRODI SI</button>
				<div class="panel">
					<div class="panel-content">
						<?php
						if($result_set511 !== "kosong" && $result_set511){
							foreach($result_set511 as $row){		
							?>		
								<p><?php echo $row['kompetensi_lainnya']?></p>
							<?php
							}
						} else {
							?>
							<p>Uraian ringkas kompetensi utama tidak ditemukan, silahkan masukan Uraian ringkas kompetensi utama <a href="<?php echo base_url(); ?>standar5/kurikulum/5.1kurikulum">di sini</a>.</p>
						<?php	
						}	
						?>
					</div>
				</div>
				
				<div class="elemen-penilaian">Elemen Penilaian</div>
				<div class="deskripsi-elemen-penilaian">
					<p>
						5.1  Kurikulum harus memuat standar kompetensi lulusan yang terstruktur dalam kompetensi utama, 
						pendukung dan lainnya yang mendukung  tercapainya tujuan, terlaksananya misi, dan terwujudnya 
						visi program studi.
					</p>
				</div>
				
				<div class="info-penilaian">Penilaian 5.1.1 Kompetensi Lulusan</div>
				<button class="tablinks" style="display: none" id="defaultOpen" onclick="nextSimulation(event, '5.1.1.a')">5.1 Kurikulum</button>
				<form action="5.1.1/simpan" method="post">
					<div class="tabcontent" id="5.1.1.a">
						<h4>5.1.1.a Struktur kurikulum (harus memuat standar kompetensi lulusan yang terstruktur dalam kompetensi utama, pendukung dan lainnya)</h4>
						<div class="input-penilaian">
							<p class="teks-penilaian nilai">Nilai <span class="required">*</span></p>:
							<label title="Kurikulum memuat kompetensi lulusan secara lengkap (utama, pendukung, lainnya) yang terumuskan secara sangat jelas.">
								<span title="Sangat Baik (4)"><input onchange="hitungBobotxNilai('4','1')" type="radio" required class="nilai" name="nilai1" id="4" value="4"/> Sangat Baik (4)
							</label></span>
							<label title="Kurikulum memuat kompetensi lulusan secara lengkap (utama, pendukung, lainnya) yang terumuskan secara jelas.">
								<span title="Baik (3)"><input onchange="hitungBobotxNilai('3','1')" type="radio" required class="nilai" name="nilai1" id="3" value="3"/> Baik (3)
							</label></span>
							<label title="Kurikulum memuat kompetensi lulusan secara lengkap (utama, pendukung, lainnya) yang terumuskan secara cukup jelas.">
								<span title="Cukup (2)"><input onchange="hitungBobotxNilai('2','1')" type="radio" required class="nilai" name="nilai1" id="2" value="2"/> Cukup (2)
							</label></span>
							<label title="Kurikulum memuat kompetensi lulusan secara lengkap (utama, pendukung, lainnya), namun rumusannya kurang jelas.">
								<span title="Kurang (1)"><input onchange="hitungBobotxNilai('1','1')" type="radio" required class="nilai" name="nilai1" id="1" value="1"/> Kurang (1)
							</label></span>
							<label title="Kurikulum tidak memuat kompetensi lulusan secara lengkap.">
								<span title="Sangat Kurang (0)"><input onchange="hitungBobotxNilai('0','1')" type="radio" required class="nilai" name="nilai1" id="0" value="0"/> Sangat Kurang (0)
							</label></span>
							<br />
							<p class="teks-penilaian bobot">Bobot </p>:
							<input style="background:transparent; border:none; font-size:inherit;" class="bobot-penilaian" id="bobot1"  type="text" name="bobot" readonly value="0.57"/>
							<br />
							<p class="teks-penilaian bobotxnilai">Bobot X Nilai </p>:
							<input style="background:transparent; border:none; font-size:inherit;" class="bobotxnilai-penilaian" id="bobotxnilai1" value="0" readonly/>
							<br />
							<p class="teks-penilaian catatan">Catatan </p><p style="width:8px; float:left">:</p>
							<textarea name="catatan[]" class="catatan-penilaian" id="catatan1"></textarea>
						</div>
						<button type="button" title="Penilaian Selanjutnya" class="tablinks" onclick="nextSimulation(event, '5.1.1.b')">Selanjutnya</button>
					</div>
					<div class="tabcontent" id="5.1.1.b">
						<h4>5.1.1.b Orientasi dan Kesesuaian Kurikulum Dengan Visi dan Misi Program Studi</h4>
						<div class="input-penilaian">
							<p class="teks-penilaian nilai">Nilai <span class="required">*</span></p>:
							<label title="Sesuai dengan visi-misi, sudah berorientasi ke masa depan.">
								<span title="Sangat Baik (4)"><input onchange="hitungBobotxNilai('4','2')" type="radio" required class="nilai" name="nilai2" id="4" value="4"/> Sangat Baik (4)
							</label></span>
							<label title="Sesuai dengan visi-misi, berorientasi ke masa kini.">
								<span title="Baik (3)"><input onchange="hitungBobotxNilai('3','2')" type="radio" required class="nilai" name="nilai2" id="3" value="3"/> Baik (3)
							</label></span>
							<label title="Sesuai dengan visi-misi, tetapi masih berorientasi ke masa lalu.">
								<span title="Cukup (2)"><input onchange="hitungBobotxNilai('2','2')" type="radio" required class="nilai" name="nilai2" id="2" value="2"/> Cukup (2)
							</label></span>
							<label title="Tidak sesuai dengan visi-misi.">
								<span title="Kurang (1)"><input onchange="hitungBobotxNilai('1','2')" type="radio" required class="nilai" name="nilai2" id="1" value="1"/> Kurang (1)
							</label></span>
							<label title="Tidak sesuai dengan visi-misi serta tidak jelas orientasinya Atau Tidak memuat memuat standar kompetensi.">
								<span title="Sangat Kurang (0)"><input onchange="hitungBobotxNilai('0','2')" type="radio" required class="nilai" name="nilai2" id="0" value="0"/> Sangat Kurang (0)
							</label></span>
							<br />
							<p class="teks-penilaian bobot">Bobot </p>:
							<input style="background:transparent; border:none; font-size:inherit;" class="bobot-penilaian" id="bobot2"  type="text" name="bobot" readonly value="0.57"/>
							<br />
							<p class="teks-penilaian bobotxnilai">Bobot X Nilai </p>:
							<input style="background:transparent; border:none; font-size:inherit;" class="bobotxnilai-penilaian" id="bobotxnilai2" value="0" readonly/>
							<br />
							<p class="teks-penilaian catatan">Catatan </p><p style="width:8px; float:left">:</p>
							<textarea type="text" name="catatan[]" class="catatan-penilaian" id="catatan2"></textarea>
						</div>
						
						<button title="Penilaian Sebelumnya" style="margin-right: 10px" type="button" class="tablinks" onclick="nextSimulation(event, '5.1.1.a')">Sebelumnya</button>
						<button title="Simpan Penilaian" style="margin-left:unset" name="simpanBtn" type="submit" class="tablinks">Simpan</button>
					</div>
				</form>
				
			</div>
		</div>

		<div class="footer">
			<?php
			include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/footer.php";
			?>
		</div>
		<script>
		function hitungBobotxNilai(rb, no) {
			var myBox1 = rb;	
			var myBox2 = document.getElementById('bobot'+no).value;
			var result = document.getElementById('bobotxnilai'+no);	
			var myResult = myBox1 * myBox2;
			result.value = myResult;			
		}
		</script>
		
		
		<script>
		function nextSimulation(evt, simulationName) {
			// Declare all variables
			var i, tabcontent, tablinks;

			// Get all elements with class="tabcontent" and hide them
			tabcontent = document.getElementsByClassName("tabcontent");
			for (i = 0; i < tabcontent.length; i++) {
				tabcontent[i].style.display = "none";
			}

			// Get all elements with class="tablinks" and remove the class "active"
			tablinks = document.getElementsByClassName("tablinks");
			for (i = 0; i < tablinks.length; i++) {
				tablinks[i].className = tablinks[i].className.replace(" active", "");
			}

			// Show the current tab, and add an "active" class to the button that opened the tab
			document.getElementById(simulationName).style.display = "inline-table";
			evt.currentTarget.className += " active";
		}
		
		// Get the element with id="defaultOpen" and click on it
		document.getElementById("defaultOpen").click();
		</script>
		
		<script>
		(function (){	
			$(document).ready(function(){
			var acc = document.getElementsByClassName("accordion");
			var i;

			for (i = 0; i < acc.length; i++) {
			  acc[i].onclick = function() {
				this.classList.toggle("active");
				var panel = this.nextElementSibling;
				if (panel.style.maxHeight){
				  panel.style.maxHeight = null;
				  panel.style.paddingBottom = null;
				} else {
				  panel.style.maxHeight = panel.scrollHeight + "px";
				  panel.style.paddingBottom = "18px";
				} 
			  }
			}
			});
		})(jQuery);	
		</script>
		
	</body>

	
	
</html>
