<?php
if(!isset($_SESSION['nama'])){
      header("location:" . base_url());
      exit();
   }	
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Akreditasi | Standar 5 | Simulasi Penilaian | 5.5 Pembimbingan Tugas Akhir / Skripsi</title>
		<script type = 'text/javascript' src="<?php echo base_url(); ?>js/jquery-1.12.4.js"></script>
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/mainlayout.css">
		<!--<link rel="stylesheet" href="<?php echo base_url(); ?>css/accordion.css">-->
		<!--<link rel="stylesheet" href="<?php echo base_url(); ?>css/jquery.dataTables.css">-->
		<!--<link rel="stylesheet" href="<?php echo base_url(); ?>css/data&dokumenstd5.css">-->
		<!--<link rel="stylesheet" href="<?php echo base_url(); ?>css/simulasipenilaian.css">-->
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/submenu.simulasipenilaian.css">
		<!--<link rel="stylesheet" href="<?php echo base_url(); ?>css/dokumenpendukung.css">-->
		<!--<script type = 'text/javascript' src="<?php echo base_url(); ?>js/jquery.dataTables.js"></script>-->
		<script type = 'text/javascript' src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>
		
	</head>
	
	<body>
		<div class="header">
			<h1><a href="<?php echo base_url(); ?>dashboard">Akreditasi SI</a></h1>
			<?php
				include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/header.php";
			?>
		</div>
		
		<div class="sidebar">
			<?php
				include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/sidebar.php";
			?>
		</div>
			
		<div class="main-layout">
			<div class="sub-header">
				<h2>Simulasi Penilaian Borang Program Studi Standar 5</h2>
			</div>
			<div class="sub-header">
				<h2>5.5.2 Rata-Rata Lama Penyelesaian Tugas Akhir/Skripsi Pada Tiga Tahun Terakhir</h2>
			</div>
			<div class="main-content">
				<button class="accordion">Rata-Rata Lama Penyelesaian Tugas Akhir/Skripsi Pada Tiga Tahun Terakhir</button>
				<div class="panel">
					<div class="panel-content">
						<?php
						if($result_set552 !== "kosong" && $result_set552){
							foreach($result_set552 as $row){		
							?>		
								<?php echo $row['waktu_penyelesaian_skripsi']?>
							<?php
							}
						} else {
							?>
							<p>Rata-Rata Lama Penyelesaian Tugas Akhir/Skripsi Pada Tiga Tahun Terakhir tidak ditemukan, silahkan masukan Rata-Rata Lama Penyelesaian Tugas Akhir/Skripsi Pada Tiga Tahun Terakhir <a href="<?php echo base_url(); ?>standar5/kurikulum/5.1kurikulum">di sini</a>.</p>
						<?php	
						}	
						?>
					</div>
				</div>
				
				<div class="elemen-penilaian">Elemen Penilaian</div>
				<div class="deskripsi-elemen-penilaian">
					<p>
						5.5 Sistem pembimbingan tugas akhir (skripsi): ketersediaan panduan, 
						rata-rata mahasiswa per dosen pembimbing tugas akhir, 
						rata-rata jumlah pertemuan/ pembimbingan, kualifikasi akademik dosen pembimbing tugas akhir, 
						dan waktu penyelesaian penulisan.
					</p>
				</div>
				
				<div class="info-penilaian">Penilaian 5.5.2 Rata-Rata Lama Penyelesaian Tugas Akhir/Skripsi Pada Tiga Tahun Terakhir</div>
				<button class="tablinks" style="display: none" id="defaultOpen" onclick="nextSimulation(event, '5.5.2')">5.5.2 Rata-Rata Lama Penyelesaian Tugas Akhir/Skripsi Pada Tiga Tahun Terakhir</button>
				<form action="5.5.2/simpan" method="post">
					<div class="tabcontent" id="5.5.2">
						<h4>5.5.2 Rata-Rata Lama Penyelesaian Tugas Akhir/Skripsi Pada Tiga Tahun Terakhir</h4>
						<div class="input-penilaian">
							<br />
							<p style="color: red">Kolom isian yang tidak ada datanya, harus diisi dengan nol.</p>
							<p style="color: red">Untuk PS dengan waktu penyelesaian TA selama 1(satu) semester :</p>
							<p style="width: 250px;padding:5px 0;margin:10px 0" class="teks-penilaian nilai">Rata-rata waktu penyelesaian TA <span class="required">*</span></p>:
							<label title="Jika RPTA ≤ 6 bulan, maka skor = 4. Jika 6 bulan < RPTA < 14 bulan, maka skor = (14 – RPTA) / 2. Jika RPTA ≥ 14 bulan, maka skor = 0.">
								<span title=""><input oninput="hitungBobotxNilaiNumber()" style="font-size:inherit;padding: 5px;margin: 10px 0;" type="number" title="Please enter a number between 0 - 11" name="nilai[0]" min="0" max="11" required class="nilai" id="input-penilaian-number1"/>
							</label></span>
							<br />
							<p style="width: 250px;padding-top:10px;" class="teks-penilaian bobot">Nilai TA terjadwal 1 semester <span class="required">*</span></p>:
							<input style="width: 30%;background:transparent; border:none; font-size:inherit;margin: 10px 0;" name="nilai[1]" readonly class="nilai" id="input-nilai-terjadwal1"/>
							<br />
							<p style="color: red">Untuk PS dengan waktu penyelesaian TA selama 2(dua) semester :</p>
							<p style="width: 250px;padding:5px 0;margin:10px 0" class="teks-penilaian nilai">Rata-rata waktu penyelesaian TA <span class="required">*</span></p>:
							<label title="Jika RPTA ≤ 12 bulan, maka skor = 4. Jika 12 bulan < RPTA < 28 bulan, maka skor = (28 – RPTA) / 4. Jika RPTA ≥ 28 bulan, maka skor = 0.">
								<span title=""><input oninput="hitungBobotxNilaiNumber()" style="font-size:inherit;padding: 5px;margin: 10px 0;" type="number" title="Please enter a number between 0 - 21" name="nilai[2]" min="0" max="21" required class="nilai" id="input-penilaian-number2"/>
							</label></span>
							<br />
							<p style="width: 250px;padding-top:10px;" class="teks-penilaian bobot">Nilai TA terjadwal 2 semester <span class="required">*</span></p>:
							<input style="width: 30%;background:transparent; border:none; font-size:inherit;margin: 10px 0;" name="nilai[3]" readonly class="nilai" id="input-nilai-terjadwal2"/>
							<br />
							<p style="width: 250px;margin: 10px 0;" class="teks-penilaian bobot">Nilai Akhir </p>:
							<input style="width:30%;background:transparent; border:none; font-size:inherit;margin: 10px 0;" class="penilaian-nilai-akhir" id="nilai-akhir1"  type="text" name="nilai-akhir" readonly value="0"/>
							<br />
							<p style="width: 250px;" class="teks-penilaian bobotxnilai">Catatan </p><p style="width:8px; float:left">:</p>
							<textarea name="catatan" class="catatan-penilaian" id="catatan1"></textarea>
						</div>
						<button title="Simpan Penilaian" style="margin-left:258px" name="simpanBtn" type="submit" class="tablinks">Simpan</button>
					</div>
				</form>
				
			</div>
		</div>

		<div class="footer">
			<?php
			include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/footer.php";
			?>
		</div>
		
		<script>
		function hitungBobotxNilaiNumber() {
			var myBox1 = document.getElementById('input-penilaian-number1').value;	
			var myBox2 = document.getElementById('input-penilaian-number2').value;	
			var nilai_1semester = document.getElementById('input-nilai-terjadwal1');	
			var nilai_2semester = document.getElementById('input-nilai-terjadwal2');	
			var nilaiAkhir = document.getElementById('nilai-akhir1');
			if(myBox1 <= 6){
				nilai_1semester.value = 4;
			} else if(myBox1 < 14){
				nilai_1semester.value = (14 - myBox1) / 2;
			} else{
				nilai_1semester.value = 0;
			}
			if(myBox2 <= 12){
				nilai_2semester.value = 4;
			} else if(myBox2 < 28) {
				nilai_2semester.value = (28 - myBox2) / 4;
			} else {
				nilai_2semester.value = 0;
			}
			
			if(myBox1 >= 12){
				nilai_1semester.value = "Masukan hanya nilai 0 - 11";
			} else if (myBox1 == 0 || myBox1 == null){
				nilai_1semester.value = 0;
			}
			if(myBox2 >= 22){
				nilai_2semester.value = "Masukan hanya nilai 0 - 21";
			} else if (myBox2 == 0 || myBox2 == null){
				nilai_2semester.value = 0;
			}
			
			if(nilai_1semester.value > 0){
				nilaiAkhir.value = nilai_1semester.value;
			} else if(nilai_2semester.value > 0){
				nilaiAkhir.value = nilai_2semester.value;
			} else if(myBox1 == 0 || myBox1 == null || myBox2 == 0 || myBox2 == null){
				nilaiAkhir.value = 0;
				console.log(nilaiAkhir);
			}
		}
		</script>
		
		
		<script>
		function nextSimulation(evt, simulationName) {
			// Declare all variables
			var i, tabcontent, tablinks;

			// Get all elements with class="tabcontent" and hide them
			tabcontent = document.getElementsByClassName("tabcontent");
			for (i = 0; i < tabcontent.length; i++) {
				tabcontent[i].style.display = "none";
			}

			// Get all elements with class="tablinks" and remove the class "active"
			tablinks = document.getElementsByClassName("tablinks");
			for (i = 0; i < tablinks.length; i++) {
				tablinks[i].className = tablinks[i].className.replace(" active", "");
			}

			// Show the current tab, and add an "active" class to the button that opened the tab
			document.getElementById(simulationName).style.display = "inline-table";
			evt.currentTarget.className += " active";
		}
		
		// Get the element with id="defaultOpen" and click on it
		document.getElementById("defaultOpen").click();
		</script>
		
		<script>
		(function (){	
			$(document).ready(function(){
			var acc = document.getElementsByClassName("accordion");
			var i;

			for (i = 0; i < acc.length; i++) {
			  acc[i].onclick = function() {
				this.classList.toggle("active");
				var panel = this.nextElementSibling;
				if (panel.style.maxHeight){
				  panel.style.maxHeight = null;
				  panel.style.paddingBottom = null;
				} else {
				  panel.style.maxHeight = panel.scrollHeight+= 5 + "px";
				  panel.style.paddingBottom = "18px";
				} 
			  }
			}
			});
		})(jQuery);	
		</script>
		
	</body>

	
	
</html>
