<?php
if(!isset($_SESSION['nama'])){
      header("location:" . base_url());
      exit();
   }	
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Akreditasi | Standar 5 | Simulasi Penilaian | 5.5 Pembimbingan Tugas Akhir / Skripsi</title>
		<script type = 'text/javascript' src="<?php echo base_url(); ?>js/jquery-1.12.4.js"></script>
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/mainlayout.css">
		<!--<link rel="stylesheet" href="<?php echo base_url(); ?>css/accordion.css">-->
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/jquery.dataTables.css">
		<!--<link rel="stylesheet" href="<?php echo base_url(); ?>css/data&dokumenstd5.css">-->
		<!--<link rel="stylesheet" href="<?php echo base_url(); ?>css/simulasipenilaian.css">-->
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/submenu.simulasipenilaian.css">
		<!--<link rel="stylesheet" href="<?php echo base_url(); ?>css/dokumenpendukung.css">-->
		<script type = 'text/javascript' src="<?php echo base_url(); ?>js/jquery.dataTables.js"></script>
		<script type = 'text/javascript' src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>
		<script type = 'text/javascript' >
		(function ($){	
			$(document).ready(function() {
				$('table').dataTable({
					"order": [],
					responsive: true,
					"scrollX": true,
					"scrollY": true,
					pageResize: true,
					autoWidth: true,
					columnDefs: [
						{ width: 35, targets: 0}
					]
					
				});
			});	
		})(jQuery);	
		</script>
		
	</head>
	
	<body>
		<div class="header">
			<h1><a href="<?php echo base_url(); ?>dashboard">Akreditasi SI</a></h1>
			<?php
				include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/header.php";
			?>
		</div>
		
		<div class="sidebar">
			<?php
				include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/sidebar.php";
			?>
		</div>
			
		<div class="main-layout">
			<div class="sub-header">
				<h2>Simulasi Penilaian Borang Program Studi Standar 5</h2>
			</div>
			<div class="sub-header">
				<h2>5.5.1 Pelaksanaan pembimbingan Tugas Akhir atau Skripsi yang Diterapkan pada Program Studi</h2>
			</div>
			<div class="main-content">
				<button class="accordion">Pelaksanaan pembimbingan Tugas Akhir atau Skripsi yang Diterapkan pada Program Studi</button>
				<div class="panel">
					<div class="panel-content">
						<table id="table-view-data-5-5-1" class="display" width="100%" cellspacing="0">
							<thead class="table head">
								<th>No</th>
								<th>Nama Dosen Pembimbing</th>
								<th>Jumlah Mahasiswa</th>
							</thead>
							<tbody class="table body">
								<?php
								if($result_set551 !== "kosong" && $result_set551){	
									foreach($result_set551 as $row){
									?>	
										<tr id="row1:<?php echo $row['no'] ?>">
											<td><?php echo $row['no'] ?></td> 	
											<td><?php echo $row['nama_dosen_pembimbing'] ?></td>
											<td><?php echo $row['jumlah_mahasiswa'] ?></td>
										</tr>
								<?php
									}
								}
								?>						
							</tbody>
						</table>
					</div>
				</div>
				
				<div class="elemen-penilaian">Elemen Penilaian</div>
				<div class="deskripsi-elemen-penilaian">
					<p>
						5.5 Sistem pembimbingan tugas akhir (skripsi): ketersediaan panduan, 
						rata-rata mahasiswa per dosen pembimbing tugas akhir, 
						rata-rata jumlah pertemuan/ pembimbingan, kualifikasi akademik dosen pembimbing tugas akhir, 
						dan waktu penyelesaian penulisan.
					</p>
				</div>
				
				<div class="info-penilaian">Penilaian 5.5.1 Pelaksanaan pembimbingan Tugas Akhir atau Skripsi yang Diterapkan pada Program Studi</div>
				<button class="tablinks" style="display: none" id="defaultOpen" onclick="nextSimulation(event, '5.5.1.a')">5.5.1.a  Ketersediaan panduan, sosialisasi, dan penggunaan</button>
				<form action="5.5.1/simpan" method="post">
					<div class="tabcontent" id="5.5.1.a">
						<h4>5.5.1.a  Ketersediaan panduan, sosialisasi, dan penggunaan</h4>
						<div class="input-penilaian">
							<p class="teks-penilaian nilai">Nilai <span class="required">*</span></p>:
							<label title="Ada panduan tertulis yang disosialisasikan dan dilaksanakan dengan konsisten.">
								<span title="Sangat Baik (4)"><input onchange="hitungBobotxNilai('4','1')" type="radio" required class="nilai" name="nilai[0]" id="4" value="4"/> Sangat Baik (4)
							</label></span>
							<label title="Ada panduan tertulis dan disosialisasikan dengan baik, tetapi tidak dilaksanakan secara konsisten.">
								<span title="Baik (3)"><input onchange="hitungBobotxNilai('3','1')" type="radio" required class="nilai" name="nilai[0]" id="3" value="3"/> Baik (3)
							</label></span>
							<label title="Ada panduan tertulis tetapi tidak disosialisasikan dengan baik, serta tidak dilaksanakan secara konsisten.">
								<span title="Cukup (2)"><input onchange="hitungBobotxNilai('2','1')" type="radio" required class="nilai" name="nilai[0]" id="2" value="2"/> Cukup (2)
							</label></span>
							<label title="-">
								<span title="Kurang (1)"><input onchange="hitungBobotxNilai('1','1')" type="radio" required class="nilai" name="nilai[0]" id="1" value="1"/> Kurang (1)
							</label></span>
							<label title="Tidak ada panduan tertulis.">
								<span title="Sangat Kurang (0)"><input onchange="hitungBobotxNilai('0','1')" type="radio" required class="nilai" name="nilai[0]" id="0" value="0"/> Sangat Kurang (0)
							</label></span>
							<br />
							<p class="teks-penilaian bobot">Bobot </p>:
							<input style="background:transparent; border:none; font-size:inherit;" class="bobot-penilaian" id="bobot1"  type="text" name="bobot" readonly value="0.57"/>
							<br />
							<p class="teks-penilaian bobotxnilai">Bobot X Nilai </p>:
							<input style="background:transparent; border:none; font-size:inherit;" class="bobotxnilai-penilaian" id="bobotxnilai1" value="0" readonly/>
							<br />
							<p class="teks-penilaian bobotxnilai">Catatan </p><p style="width:8px; float:left">:</p>
							<textarea name="catatan[]" class="catatan-penilaian" id="catatan1"></textarea>
						</div>
						<button type="button" title="Penilaian Selanjutnya" class="tablinks" onclick="nextSimulation(event, '5.5.1.b')">Selanjutnya</button>
					</div>
					
					<div class="tabcontent" id="5.5.1.b">
						<h4>5.5.1.b  Rata-rata mahasiswa per dosen pembimbing tugas akhir (=RMTA)</h4>
						<div class="input-penilaian">
							<p style="width: 210px;padding:6px 0;" class="teks-penilaian nilai">Jumlah dosen pembimbing tugas akhir <span class="required">*</span></p>:
							<label title="Jika 0 < RMTA ≤ 4, maka skor = 4. Jika 4 < RMTA < 20, maka skor = 5 – (RMTA / 4). Jika RMTA = 0, atau RMTA ≥ 20, maka skor = 0.">
								<span title=""><input readonly value="<?php echo $jumlahDosbingTA ?>" style="font-size:inherit;padding: 5px;margin: 10px 0;" type="number" title="Please enter a number between 0 - 150" name="nilai[1]" min="0" max="150" required class="nilai" id="input-penilaian-number1"/>
							</label></span>
							<br />
							<p style="width: 210px;padding-top:1px;" class="teks-penilaian bobot">Jumlah mahasiswa tugas akhir <span class="required">*</span></p>:
							<input readonly value="<?php echo $jumlahMhsTA ?>" style="font-size:inherit;padding: 5px;margin: 5px 0;" type="number" title="Please enter a number between 0 - 2500" name="nilai[2]" min="0" max="2500" required class="nilai" id="input-penilaian-number2"/>
							<br />
							<p style="width: 210px;margin:10px 0;" class="teks-penilaian mksap">Rasio mahasiswa/dosen </p>:
							<input style="width: 30%;background:transparent; border:none; font-size:inherit;margin: 10px 0;" class="mksap" id="nilai-rasio-mhsdsn" value="0" readonly/>
							<br />
							<p style="width:210px;margin-bottom: 5px;" class="teks-penilaian nilai">Nilai </p>:
							<input style="width: 30%;background:transparent; border:none; font-size:inherit;margin-bottom: 5px;" name="nilai-number[0]" class="nilai-akhir" id="nilaiakhir1" value="0" readonly/>
							<br />
							<p style="width:210px;" class="teks-penilaian bobotxnilai">Catatan </p><p style="width:8px; float:left">:</p>
							<textarea name="catatan[]" class="catatan-penilaian" id="catatan2"></textarea>
						</div>
						<button title="Penilaian Sebelumnya" style="margin-left:218px;margin-right: 10px" type="button" class="tablinks" onclick="nextSimulation(event, '5.5.1.a')">Sebelumnya</button>
						<button type="button" title="Penilaian Selanjutnya" style="margin-left:unset" class="tablinks" onclick="nextSimulation(event, '5.5.1.c')">Selanjutnya</button>					
					</div>
				
					<div class="tabcontent" id="5.5.1.c">
						<h4>5.5.1.c Rata-rata jumlah pertemuan/pembimbingan selama penyelesaian TA (=RBTA)</h4>
						<div class="input-penilaian">
							<p style="width: 230px;padding:6px 0;" class="teks-penilaian nilai">Rata-rata jumlah pertemuan/pembimbingan TA <span class="required">*</span></p>:
								<label title="Jika RBTA ≥ 8, maka skor = 4. Jika RBTA < 8, maka skor = RBTA / 2.">
									<span title=""><input oninput="hitungBobotxNilaiNumber()" style="font-size:inherit;padding: 5px;margin: 10px 0;" type="number" title="Please enter a number between 0 - 20" name="nilai[3]" min="0" max="20" required class="nilai" id="input-penilaian-number3"/>
								</label></span>
								<br />
								<p style="width:230px;margin-bottom: 5px;" class="teks-penilaian nilai">Nilai </p>:
								<input style="width: 30%;background:transparent; border:none; font-size:inherit;margin-bottom: 5px;" name="nilai-number[1]" class="nilai-akhir" id="nilaiakhir2" value="0" readonly/>
								<br />
								<p style="width: 230px;" class="teks-penilaian catatan">Catatan </p><p style="width:8px; float:left">:</p>
								<textarea style="margin-bottom: 10px;" type="text" name="catatan[]" class="catatan-penilaian" id="catatan1"></textarea>
						</div>
						<button title="Penilaian Sebelumnya" style="margin-left: 238px;margin-right: 10px" type="button" class="tablinks" onclick="nextSimulation(event, '5.5.1.b')">Sebelumnya</button>
						<button type="button" title="Penilaian Selanjutnya" style="margin-left:unset" class="tablinks" onclick="nextSimulation(event, '5.5.1.d')">Selanjutnya</button>					
					</div>
					
					<div class="tabcontent" id="5.5.1.d">
						<h4>5.5.1.d  Kualifikasi akademik dosen pembimbing tugas akhir</h4>
						<div class="input-penilaian">
							<p class="teks-penilaian nilai">Nilai <span class="required">*</span></p>:
							<label title="Seluruh dosen pembimbing berpendidikan minimal S2 dan sesuai dengan bidang keahliannya.">
								<span title="Sangat Baik (4)"><input onchange="hitungBobotxNilai('4','4')" type="radio" required class="nilai" name="nilai[4]" id="4" value="4"/> Sangat Baik (4)
							</label></span>
							<label title="Seluruh dosen pembimbing berpendidikan minimal S2, tetapi sebagian kecil tidak sesuai dengan bidang keahliannya.">
								<span title="Baik (3)"><input onchange="hitungBobotxNilai('3','4')" type="radio" required class="nilai" name="nilai[4]" id="3" value="3"/> Baik (3)
							</label></span>
							<label title="Sebagian besar dosen pembimbing berpendidikan minimal S2, tetapi sebagian kecil tidak sesuai dengan bidang keahliannya.">
								<span title="Cukup (2)"><input onchange="hitungBobotxNilai('2','4')" type="radio" required class="nilai" name="nilai[4]" id="2" value="2"/> Cukup (2)
							</label></span>
							<label title="Sebagian besar dosen pembimbing belum berpendidikan minimal S2 dan  tidak sesuai dengan bidang keahliannya.">
								<span title="Kurang (1)"><input onchange="hitungBobotxNilai('1','4')" type="radio" required class="nilai" name="nilai[4]" id="1" value="1"/> Kurang (1)
							</label></span>
							<label title="(Tidak ada skor nol)">
								<span title="Sangat Kurang (0)"><input onchange="hitungBobotxNilai('0','4')" type="radio" required class="nilai" name="nilai[4]" id="0" value="0"/> Sangat Kurang (0)
							</label></span>
							<br />
							<p class="teks-penilaian bobot">Bobot </p>:
							<input style="background:transparent; border:none; font-size:inherit;" class="bobot-penilaian" id="bobot4"  type="text" name="bobot" readonly value="1.14"/>
							<br />
							<p class="teks-penilaian bobotxnilai">Bobot X Nilai </p>:
							<input style="background:transparent; border:none; font-size:inherit;" class="bobotxnilai-penilaian" id="bobotxnilai4" value="0" readonly/>
							<br />
							<p class="teks-penilaian bobotxnilai">Catatan </p><p style="width:8px; float:left">:</p>
							<textarea name="catatan[]" class="catatan-penilaian" id="catatan4"></textarea>
						</div>
						<button title="Penilaian Sebelumnya" style="margin-right: 10px" type="button" class="tablinks" onclick="nextSimulation(event, '5.5.1.c')">Sebelumnya</button>
						<button title="Simpan Penilaian" style="margin-left:unset" name="simpanBtn" type="submit" class="tablinks">Simpan</button>
					</div>
				</form>
				
			</div>
		</div>

		<div class="footer">
			<?php
			include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/footer.php";
			?>
		</div>
		<script>
		(function (){	
			$(document).ready(function(){
				var myBox1 = document.getElementById('input-penilaian-number1').value;	
				var myBox2 = document.getElementById('input-penilaian-number2').value;
				var rasio = document.getElementById('nilai-rasio-mhsdsn');
				var nilaiAkhir = document.getElementById('nilaiakhir1');
				var rasioResult = myBox2 / myBox1;
				rasio.value = rasioResult.toFixed(2);
				if(rasioResult == 0){
					nilaiAkhir.value = 0;
				} else if(rasioResult <= 4) {
					nilaiAkhir.value = 4;
				} else if(rasioResult < 20){
					nilaiAkhir.value = 5 - (rasioResult/4).toFixed(2);
				} else {
					nilaiAkhir.value = 0;
				}
				if(myBox1 >= 60 || myBox2 >= 500){
					rasio.value = "Masukan hanya nilai 0 - 60 pada Jumlah dosen pembimbing tugas akhir";
					nilaiAkhir.value = "Masukan hanya nilai 0 - 500 pada Jumlah mahasiswa tugas akhir";
				} else if (myBox1 == 0 || myBox2 == 0 || myBox1 == null || myBox2 == null){
					rasio.value = 0;
					nilaiAkhir.value = 0;
				}
			});
		})(jQuery);	
		</script>
		<script>
		function hitungBobotxNilaiNumber() {
			var myBox1 = document.getElementById('input-penilaian-number3').value;	
			var nilaiAkhir = document.getElementById('nilaiakhir2');
			if(myBox1 >= 8){
				nilaiAkhir.value = 4;
			} else {
				nilaiAkhir.value = myBox1 / 2;
			}
			if(myBox1 >= 20){
				nilaiAkhir.value = "Masukan hanya nilai 0 - 20";
			} else if (myBox1 == 0 || myBox1 == null){
				nilaiAkhir.value = 0;
			}
		}
		</script>
		<script>
		function hitungBobotxNilai(rb, no) {
			var myBox1 = rb;	
			var myBox2 = document.getElementById('bobot'+no).value;
			var result = document.getElementById('bobotxnilai'+no);	
			var myResult = myBox1 * myBox2;
			result.value = myResult;			
		}
		</script>
		
		
		<script>
		function nextSimulation(evt, simulationName) {
			// Declare all variables
			var i, tabcontent, tablinks;

			// Get all elements with class="tabcontent" and hide them
			tabcontent = document.getElementsByClassName("tabcontent");
			for (i = 0; i < tabcontent.length; i++) {
				tabcontent[i].style.display = "none";
			}

			// Get all elements with class="tablinks" and remove the class "active"
			tablinks = document.getElementsByClassName("tablinks");
			for (i = 0; i < tablinks.length; i++) {
				tablinks[i].className = tablinks[i].className.replace(" active", "");
			}

			// Show the current tab, and add an "active" class to the button that opened the tab
			document.getElementById(simulationName).style.display = "inline-table";
			evt.currentTarget.className += " active";
		}
		
		// Get the element with id="defaultOpen" and click on it
		document.getElementById("defaultOpen").click();
		</script>
		
		<script>
		(function (){	
			$(document).ready(function(){
			var acc = document.getElementsByClassName("accordion");
			var i;

			for (i = 0; i < acc.length; i++) {
			  acc[i].onclick = function() {
				this.classList.toggle("active");
				var panel = this.nextElementSibling;
				if (panel.style.maxHeight){
				  panel.style.maxHeight = null;
				  panel.style.paddingBottom = null;
				} else {
				  panel.style.maxHeight = panel.scrollHeight+= 5 + "px";
				  panel.style.paddingBottom = "18px";
				} 
			  }
			}
			});
		})(jQuery);	
		</script>
		
	</body>

	
	
</html>
