<?php
if(!isset($_SESSION['nama'])){
      header("location:" . base_url());
      exit();
   }	
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Akreditasi | Standar 5 | Simulasi Penilaian | 5.4 Sistem Pembimbingan Akademik</title>
		<script type = 'text/javascript' src="<?php echo base_url(); ?>js/jquery-1.12.4.js"></script>
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/mainlayout.css">
		<!--<link rel="stylesheet" href="<?php echo base_url(); ?>css/accordion.css">-->
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/jquery.dataTables.css">
		<!--<link rel="stylesheet" href="<?php echo base_url(); ?>css/data&dokumenstd5.css">-->
		<!--<link rel="stylesheet" href="<?php echo base_url(); ?>css/simulasipenilaian.css">-->
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/submenu.simulasipenilaian.css">
		<!--<link rel="stylesheet" href="<?php echo base_url(); ?>css/dokumenpendukung.css">-->
		<script type = 'text/javascript' src="<?php echo base_url(); ?>js/jquery.dataTables.js"></script>
		<script type = 'text/javascript' src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>
		<script type = 'text/javascript' >
		(function ($){	
			$(document).ready(function() {
				$('table').dataTable({
					"order": [],
					responsive: true,
					"scrollX": true,
					"scrollY": true,
					pageResize: true,
					autoWidth: true,
					columnDefs: [
						{ width: 10, targets: 0}
					]
					
				});
			});	
		})(jQuery);	
		</script>
		
	</head>
	
	<body>
		<div class="header">
			<h1><a href="<?php echo base_url(); ?>dashboard">Akreditasi SI</a></h1>
			<?php
				include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/header.php";
			?>
		</div>
		
		<div class="sidebar">
			<?php
				include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/sidebar.php";
			?>
		</div>
			
		<div class="main-layout">
			<div class="sub-header">
				<h2>Simulasi Penilaian Borang Program Studi Standar 5</h2>
			</div>
			<div class="sub-header">
				<h2>5.4 Sistem Pembimbingan Akademik</h2>
			</div>
			<div class="main-content">
				<button class="accordion">Daftar Dosen Pembimbing Akademik dan Jumlah Mahasiswa yang Dibimbing</button>
				<div class="panel">
					<div class="panel-content">
						<table id="table-view-data-5-4-1" class="display" width="100%" cellspacing="0">
									
							<thead class="table head">
								<th>No</th>
								<th>Nama Dosen Pembimbing Akademik</th>
								<th>Jumlah Mahasiswa Bimbingan</th>
								<th>Rata-rata Banyaknya Pertemuan/mhs/smt</th>
							</thead>
								
							
							<tbody class="table body">
								<?php
								if($result_set541 !== "kosong" && $result_set541){
									foreach($result_set541 as $row){
									?>	
										<tr id="row1:<?php echo $row['no'] ?>">
											<td><?php echo $row['no'] ?></td> 	
											<td><?php echo $row['nama_dosen_pembimbing_akademik'] ?></td>
											<td><?php echo $row['jumlah_mahasiswa_bimbingan'] ?></td>
											<td><?php echo $row['rata_rata_banyaknya_pertemuan_mhs_smt'] ?></td>
										</tr>
								<?php
									}
								}
								?>						
							</tbody>
							<tbody>
								<tr>
									<td colspan="2">Total Mahasiswa Bimbingan</td>
									<td><?php echo $banyaknyaMhsBimbinganPA ?></td>
									<td></td>
								</tr>
								<tr>
									<td colspan="3">Rata-rata banyaknya pertemuan per mahasiswa per semester</td>
									<td><?php echo number_format($rata2PertemuanMhsPerSmt, 1) ?></td>
									<td></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				
				<button class="accordion">Proses Pembimbingan Akademik yang Diterapkan pada Program Studi Dijelaskan Sebagai Berikut</button>
				<div class="panel">
					<div class="panel-content">
						<table id="table-view-data-5-4-2" class="display" width="100%" cellspacing="0">
									
							<thead class="table head">
								<th>No</th>
								<th>Hal</th>
								<th>Penjelasan</th>
							</thead>
								
							
							<tbody class="table body">
								<?php
								if($result_set542 !== "kosong" && $result_set542){
									foreach($result_set542 as $row){
									?>	
										<tr id="row1:<?php echo $row['no'] ?>">
											<td><?php echo $row['no'] ?></td> 	
											<td><?php echo $row['hal'] ?></td>
											<td style="text-align: left"><?php echo $row['penjelasan'] ?></td>
										</tr>
								<?php
									}
								}
								?>						
							</tbody>
						</table>
					</div>
				</div>
				
				<div class="elemen-penilaian">Elemen Penilaian</div>
				<div class="deskripsi-elemen-penilaian">
					<p>
						5.4  Sistem pembimbingan akademik: banyaknya mahasiswa per dosen PA, 
						pelaksanaan kegiatan, rata-rata pertemuan per semester, efektivitas kegiatan perwalian.
					</p>
				</div>
				
				<div class="info-penilaian">Penilaian 5.4 Sistem Pembimbingan Akademik</div>
				<button class="tablinks" style="display: none" id="defaultOpen" onclick="nextSimulation(event, '5.4.1.a')">5.4.1.a Rata-rata banyaknya mahasiswa per dosen Pembimbing Akademik (PA) per semester (=RMPA)</button>
				<form action="5.4/simpan" method="post">
					<div class="tabcontent" id="5.4.1.a">
						<h4>5.4.1.a Rata-rata banyaknya mahasiswa per dosen Pembimbing Akademik (PA) per semester (=RMPA)</h4>
						<div class="input-penilaian">
							<p style="width: 210px;padding:6px 0;" class="teks-penilaian nilai">Banyaknya dosen PA <span class="required">*</span></p>:
							<label title="Jika RMPA ≤ 20, maka skor = 4. Jika 20 < RMPA < 60, maka skor = (60 - RMPA) / 10. Jika RMPA ≥ 60 atau tidak ada perwalian, maka skor = 0.">
								<span title=""><input readonly value="<?php echo $banyaknyaDosenPA ?>" style="font-size:inherit;padding: 5px;margin: 10px 0;" type="number" title="Please enter a number between 0 - 150" name="nilai[1]" min="0" max="150" required class="nilai" id="input-penilaian-number1"/>
							</label></span>
							<br />
							<p style="width: 210px;padding-top:1px;" class="teks-penilaian bobot">Banyaknya mahasiswa bimbingan PA <span class="required">*</span></p>:
							<input readonly value="<?php echo $banyaknyaMhsBimbinganPA ?>" style="font-size:inherit;padding: 5px;margin: 5px 0;" type="number" title="Please enter a number between 0 - 2500" name="nilai[2]" min="0" max="2500" required class="nilai" id="input-penilaian-number2"/>
							<br />
							<p style="width: 210px;margin:10px 0;" class="teks-penilaian mksap">Rasio mahasiswa/dosen </p>:
							<input style="width: 30%;background:transparent; border:none; font-size:inherit;margin: 10px 0;" class="mksap" id="nilai-rasio-mhsdsn" value="0" readonly/>
							<br />
							<p style="width:210px;margin-bottom: 5px;" class="teks-penilaian nilai">Nilai </p>:
							<input style="width: 30%;background:transparent; border:none; font-size:inherit;margin-bottom: 5px;" name="nilai-number[0]" class="nilai-akhir" id="nilaiakhir1" value="0" readonly/>
							<br />
							<p style="width: 210px;" class="teks-penilaian catatan">Catatan </p><p style="width:8px; float:left">:</p>
							<textarea style="margin-bottom: 10px;" type="text" name="catatan[]" class="catatan-penilaian" id="catatan1"></textarea>
						</div>
						<button style="margin-left: 218px;" type="button" title="Penilaian Selanjutnya" class="tablinks" onclick="nextSimulation(event, '5.4.1.b')">Selanjutnya</button>
					</div>
					
					<div class="tabcontent" id="5.4.1.b">
						<h4>5.4.1.b Pelaksanaan kegiatan pembimbingan akademik</h4>
						<div class="input-penilaian">
							<p class="teks-penilaian nilai">Nilai <span class="required">*</span></p>:
							<label title="Dilakukan oleh seluruh dosen PA dengan baik sesuai panduan tertulis.">
								<span title="Sangat Baik (4)"><input onchange="hitungBobotxNilai('4','2')" type="radio" required class="nilai" name="nilai[3]" id="4" value="4"/> Sangat Baik (4)
							</label></span>
							<label title="Perwalian dilakukan oleh seluruh dosen PA tetapi tidak seluruhnya menurut panduan tertulis.">
								<span title="Baik (3)"><input onchange="hitungBobotxNilai('3','2')" type="radio" required class="nilai" name="nilai[3]" id="3" value="3"/> Baik (3)
							</label></span>
							<label title="Perwalian dilakukan oleh sebagian dosen PA dan sebagian oleh Tenaga Administrasi.">
								<span title="Cukup (2)"><input onchange="hitungBobotxNilai('2','2')" type="radio" required class="nilai" name="nilai[3]" id="2" value="2"/> Cukup (2)
							</label></span>
							<label title="Perwalian tidak dilakukan oleh dosen PA tetapi oleh Tenaga Administrasi.">
								<span title="Kurang (1)"><input onchange="hitungBobotxNilai('1','2')" type="radio" required class="nilai" name="nilai[3]" id="1" value="1"/> Kurang (1)
							</label></span>
							<label title="Tidak ada pembimbingan, hanya ada pengesahan dokumen akademik oleh pegawai administratif ">
								<span title="Sangat Kurang (0)"><input onchange="hitungBobotxNilai('0','2')" type="radio" required class="nilai" name="nilai[3]" id="0" value="0"/> Sangat Kurang (0)
							</label></span>
							<br />
							<p class="teks-penilaian bobot">Bobot </p>:
							<input style="background:transparent; border:none; font-size:inherit;" class="bobot-penilaian" id="bobot2"  type="text" name="bobot" readonly value="0.57"/>
							<br />
							<p class="teks-penilaian bobotxnilai">Bobot X Nilai </p>:
							<input style="background:transparent; border:none; font-size:inherit;" class="bobotxnilai-penilaian" id="bobotxnilai2" value="0" readonly/>
							<br />
							<p class="teks-penilaian bobotxnilai">Catatan </p><p style="width:8px; float:left">:</p>
							<textarea name="catatan[]" class="catatan-penilaian" id="catatan2"></textarea>
						</div>
						<button title="Penilaian Sebelumnya" style="margin-right: 10px" type="button" class="tablinks" onclick="nextSimulation(event, '5.4.1.a')">Sebelumnya</button>
						<button type="button" title="Penilaian Selanjutnya" style="margin-left:unset" class="tablinks" onclick="nextSimulation(event, '5.4.1.c')">Selanjutnya</button>					
					</div>
				
					<div class="tabcontent" id="5.4.1.c">
						<h4>5.4.1.c Jumlah rata-rata pertemuan pembimbingan per mahasiswa per semester (= PP)</h4>
						<div class="input-penilaian">
							<p style="width: 210px;padding:6px 0;" class="teks-penilaian nilai">Rata-rata banyak pertemuan per mhs per smt <span class="required">*</span></p>:
								<label title="Jika PP ≥ 3.0 maka skor = 4. Jika 0 < PP < 3, maka skor = PP + 1. Jika PP = 0, maka skor = 0.">
									<span title=""><input readonly value="<?php echo $rata2PertemuanMhsPerSmt ?>" style="font-size:inherit;padding: 5px;margin: 10px 0;" type="number" title="Please enter a number between 0 - 20" name="nilai[4]" min="0" max="20" required class="nilai" id="input-penilaian-number3"/>
								</label></span>
								<br />
								<p style="width:210px;margin-bottom: 5px;" class="teks-penilaian nilai">Nilai </p>:
								<input style="width: 30%;background:transparent; border:none; font-size:inherit;margin-bottom: 5px;" name="nilai-number[1]" class="nilai-akhir" id="nilaiakhir2" value="0" readonly/>
								<br />
								<p style="width: 210px;" class="teks-penilaian catatan">Catatan </p><p style="width:8px; float:left">:</p>
								<textarea style="margin-bottom: 10px;" type="text" name="catatan[]" class="catatan-penilaian" id="catatan1"></textarea>
						</div>
						<button title="Penilaian Sebelumnya" style="margin-left: 218px;margin-right: 10px" type="button" class="tablinks" onclick="nextSimulation(event, '5.4.1.b')">Sebelumnya</button>
						<button type="button" title="Penilaian Selanjutnya" style="margin-left:unset" class="tablinks" onclick="nextSimulation(event, '5.4.2')">Selanjutnya</button>					
					</div>
					
					<div class="tabcontent" id="5.4.2">
						<h4>5.4.2 Efektivitas kegiatan perwalian</h4>
						<div class="input-penilaian">
							<p class="teks-penilaian nilai">Nilai <span class="required">*</span></p>:
							<label title="Sistem bimbingan akademik sangat efektif.">
								<span title="Sangat Baik (4)"><input onchange="hitungBobotxNilai('4','4')" type="radio" required class="nilai" name="nilai[5]" id="4" value="4"/> Sangat Baik (4)
							</label></span>
							<label title="Sistem bimbingan akademik efektif.">
								<span title="Baik (3)"><input onchange="hitungBobotxNilai('3','4')" type="radio" required class="nilai" name="nilai[5]" id="3" value="3"/> Baik (3)
							</label></span>
							<label title="Sistem bantuan dan bimbingan akademik cukup efektif.">
								<span title="Cukup (2)"><input onchange="hitungBobotxNilai('2','4')" type="radio" required class="nilai" name="nilai[5]" id="2" value="2"/> Cukup (2)
							</label></span>
							<label title="Sistem bantuan dan bimbingan akademik tidak efektif.">
								<span title="Kurang (1)"><input onchange="hitungBobotxNilai('1','4')" type="radio" required class="nilai" name="nilai[5]" id="1" value="1"/> Kurang (1)
							</label></span>
							<label title="Sistem bantuan dan bimbingan akademik tidak jalan, Atau tidak ada pembimbingan">
								<span title="Sangat Kurang (0)"><input onchange="hitungBobotxNilai('0','4')" type="radio" required class="nilai" name="nilai[5]" id="0" value="0"/> Sangat Kurang (0)
							</label></span>
							<br />
							<p class="teks-penilaian bobot">Bobot </p>:
							<input style="background:transparent; border:none; font-size:inherit;" class="bobot-penilaian" id="bobot4"  type="text" name="bobot" readonly value="0.57"/>
							<br />
							<p class="teks-penilaian bobotxnilai">Bobot X Nilai </p>:
							<input style="background:transparent; border:none; font-size:inherit;" class="bobotxnilai-penilaian" id="bobotxnilai4" value="0" readonly/>
							<br />
							<p class="teks-penilaian bobotxnilai">Catatan </p><p style="width:8px; float:left">:</p>
							<textarea name="catatan[]" class="catatan-penilaian" id="catatan4"></textarea>
						</div>
						<button title="Penilaian Sebelumnya" style="margin-right: 10px" type="button" class="tablinks" onclick="nextSimulation(event, '5.4.1.c')">Sebelumnya</button>
						<button title="Simpan Penilaian" style="margin-left:unset" name="simpanBtn" type="submit" class="tablinks">Simpan</button>
					</div>
				</form>
				
			</div>
		</div>

		<div class="footer">
			<?php
			include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/footer.php";
			?>
		</div>
		<script>
		(function (){	
			$(document).ready(function(){
				var myBox1 = document.getElementById('input-penilaian-number1').value;	
				var myBox2 = document.getElementById('input-penilaian-number2').value;
				var rasio = document.getElementById('nilai-rasio-mhsdsn');
				var nilaiAkhir = document.getElementById('nilaiakhir1');
				var rasioResult = myBox2 / myBox1;
				rasio.value = rasioResult.toFixed(2);
				if(rasioResult == 0){
					nilaiAkhir.value = 0;
				} else if(rasioResult <= 20) {
					nilaiAkhir.value = 4;
				} else if(rasioResult < 60){
					nilaiAkhir.value = ((60 - rasioResult) / 10).toFixed(2);
				} else {
					nilaiAkhir.value = 0;
				}
				if(myBox1 >= 150 || myBox2 >= 2500){
					rasio.value = "Masukan hanya nilai 0 - 150 pada Banyaknya dosen PA";
					nilaiAkhir.value = "Masukan hanya nilai 0 - 2500 pada Banyaknya mahasiswa bimbingan PA";
				} else if (myBox1 == 0 || myBox2 == 0 || myBox1 == null || myBox2 == null){
					rasio.value = 0;
					nilaiAkhir.value = 0;
				}
			});
		})(jQuery);	
		</script>
		<script>
		(function (){	
			$(document).ready(function(){
				var myBox1 = document.getElementById('input-penilaian-number3').value;	
				var nilaiAkhir = document.getElementById('nilaiakhir2');
				if(myBox1 == 0){
					nilaiAkhir.value = 0;
				} else if(myBox1 <= 3) {
					nilaiAkhir.value = myBox1 + 1;
				} else {
					nilaiAkhir.value = 4;
				}
				if(myBox1 >= 20){
					nilaiAkhir.value = "Masukan hanya nilai 0 - 20";
				} else if (myBox1 == 0 || myBox1 == null){
					nilaiAkhir.value = 0;
				}
			});
		})(jQuery);	
		</script>
		<script>
		function hitungBobotxNilai(rb, no) {
			var myBox1 = rb;	
			var myBox2 = document.getElementById('bobot'+no).value;
			var result = document.getElementById('bobotxnilai'+no);	
			var myResult = myBox1 * myBox2;
			result.value = myResult;			
		}
		</script>
		
		
		<script>
		function nextSimulation(evt, simulationName) {
			// Declare all variables
			var i, tabcontent, tablinks;

			// Get all elements with class="tabcontent" and hide them
			tabcontent = document.getElementsByClassName("tabcontent");
			for (i = 0; i < tabcontent.length; i++) {
				tabcontent[i].style.display = "none";
			}

			// Get all elements with class="tablinks" and remove the class "active"
			tablinks = document.getElementsByClassName("tablinks");
			for (i = 0; i < tablinks.length; i++) {
				tablinks[i].className = tablinks[i].className.replace(" active", "");
			}

			// Show the current tab, and add an "active" class to the button that opened the tab
			document.getElementById(simulationName).style.display = "inline-table";
			evt.currentTarget.className += " active";
		}
		
		// Get the element with id="defaultOpen" and click on it
		document.getElementById("defaultOpen").click();
		</script>
		
		<script>
		(function (){	
			$(document).ready(function(){
			var acc = document.getElementsByClassName("accordion");
			var i;

			for (i = 0; i < acc.length; i++) {
			  acc[i].onclick = function() {
				this.classList.toggle("active");
				var panel = this.nextElementSibling;
				if (panel.style.maxHeight){
				  panel.style.maxHeight = null;
				  panel.style.paddingBottom = null;
				} else {
				  panel.style.maxHeight = panel.scrollHeight + "px";
				  panel.style.paddingBottom = "18px";
				} 
			  }
			}
			});
		})(jQuery);	
		</script>
		
	</body>

	
	
</html>
