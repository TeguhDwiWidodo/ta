<?php
if(!isset($_SESSION['nama'])){
      header("location:" . base_url());
      exit();
   }	
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Akreditasi | Standar 5 | Simulasi Penilaian</title>
		<script type = 'text/javascript' src="<?php echo base_url(); ?>js/jquery-1.12.4.js"></script>
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/mainlayout.css">
		<!--<link rel="stylesheet" href="<?php echo base_url(); ?>css/accordion.css">-->
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/jquery.dataTables.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/data&dokumenstd5.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/simulasipenilaian.css">
		<link rel="shortcut icon" href="<?php echo base_url(); ?>/favicon.ico" type="image/x-icon">
		<link rel="icon" href="<?php echo base_url(); ?>/favicon.ico" type="image/x-icon">
		<!--<link rel="stylesheet" href="<?php echo base_url(); ?>css/dokumenpendukung.css">-->
		<script type = 'text/javascript' src="<?php echo base_url(); ?>js/jquery.dataTables.js"></script>
		<script type = 'text/javascript' src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>
		<script>
		(function(){
			$(document).ready(function(){
				$(".messageSukses").click(function(){
					$(".messageSukses").hide(1000);  
					//$("#btn-messageSukses").hide(1000);  
				});
			});
		})(jQuery);	
		</script>
		<script type = 'text/javascript' >
		(function ($){	
			$(document).ready(function() {
				$('table').dataTable({
					responsive: true,
					"scrollX": true,
					"scrollY": true,
					pageResize: true,
					autoWidth: true,
					columnDefs: [
						{ width: 10, targets: 0}
					]
					
				});
			});	
		})(jQuery);	
		</script>
		
	</head>
	
	<body>
		<div class="header">
			<h1><a href="<?php echo base_url(); ?>dashboard">Akreditasi SI</a></h1>
			<?php
				include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/header.php";
			?>
		</div>
		
		<div class="sidebar">
			<?php
				include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/sidebar.php";
			?>
		</div>
			
		<div class="main-layout">
			<div class="sub-header">
				<h2>Simulasi Penilaian Borang Program Studi Standar 5</h2>
			</div>
			<div class="main-content">
				<div class="poin">
					<h4>Poin Penilaian Borang Program Studi Standar 5</h4>
					<?php
						if (isset($_SESSION['message'])) {
					?>		<div title="Click to dismiss" class="messageSukses messageSukses-header">Simulasi Penilaian</div>
							<div title="Click to dismiss" class="messageSukses messageSukses-content" id="messageSukses">
								<?php echo $_SESSION['message']; ?>		
							</div>	
					<?php		
							unset($_SESSION['message']);
						}
					?>
					<div class="tab">
						<button class="tablinks" onclick="openPoin(event, '5.1')">5.1 Kurikulum</button>
						<button class="tablinks" onclick="openPoin(event, '5.2')">5.2 Peninjauan Kurikulum dalam 5 Tahun Terakhir</button>
						<button class="tablinks" onclick="openPoin(event, '5.3')">5.3 Pelaksanaan Proses Pembelajaran</button>
						<button class="tablinks" onclick="openPoin(event, '5.4')">5.4 Sistem Pembimbingan Akademik</button>
						<button class="tablinks" onclick="openPoin(event, '5.5')">5.5 Pembimbingan Tugas Akhir / Skripsi</button>
						<button class="tablinks" onclick="openPoin(event, '5.6')">5.6 Upaya Perbaikan Pembelajaran</button>
						<button class="tablinks" onclick="openPoin(event, '5.7')">5.7 Upaya Peningkatan Suasana Akademik</button>
					</div>

					<div id="5.1" class="tabcontent">
						<span onclick="closePoin(event, '5.1')" class="topright">x</span>
						<h3>5.1 Kurikulum</h3>
						<ul class="main-ul">
							<a href="<?php echo base_url(); ?>standar5/simulasipenilaian/5.1.1" target="_blank">
								<li>5.1.1 Kompetensi Lulusan
									<ul class="nested-ul">
										<li>5.1.1.a Kelengkapan dan perumusan kompetensi</li>
										<li>5.1.1.b Orientasi dan kesesuaian dengan visi dan misi</li>
									</ul>
								</li>
							</a>
							<a href="<?php echo base_url(); ?>standar5/simulasipenilaian/5.1.2" target="_blank">
								<li>5.1.2 Struktur Kurikulum
									<ul class="nested-ul">
										<li>5.1.2.a Kesesuaian matakuliah dan urutannya dengan standar kompetensi</li>
										<li>5.1.2.b Persentase mata kuliah  yang dalam penentuan nilai akhirnya memberikan bobot pada tugas-tugas (PR atau makalah) ≥ 20%</li>
										<li>5.1.2.c Mata kuliah dilengkapi dengan deskripsi mata kuliah, silabus dan SAP</li>
									</ul>
								</li>
							</a>
							<a href="<?php echo base_url(); ?>standar5/simulasipenilaian/5.1.3" target="_blank">
								<li>5.1.3 Fleksibilitas mata kuliah pilihan</li>
							</a>
							<a href="<?php echo base_url(); ?>standar5/simulasipenilaian/5.1.4" target="_blank">
								<li>5.1.4 Substansi praktikum dan pelaksanaan praktikum</li>
							</a>
						</ul>
					</div>

					<div id="5.2" class="tabcontent">
						<span onclick="closePoin(event, '5.2')" class="topright">x</span>
						<h3>5.2 Peninjauan Kurikulum dalam 5 Tahun Terakhir</h3>
						<ul class="main-ul">
							<a href="<?php echo base_url(); ?>standar5/simulasipenilaian/5.2" target="_blank">
								<li>5.2.a Pelaksanaan peninjauan kurikulum selama 5 tahun terakhir</li>
								<li>5.2.b Penyesuaian kurikulum dengan perkembangan Ipteks dan kebutuhan</li>
							</a>
						</ul>	
					</div>

					<div id="5.3" class="tabcontent">
						<span onclick="closePoin(event, '5.3')" class="topright">x</span>
						<h3>5.3 Pelaksanaan Proses Pembelajaran</h3>
						<ul class="main-ul">
							<a href="<?php echo base_url(); ?>standar5/simulasipenilaian/5.3.1" target="_blank">
								<li>5.3.1.a Pelaksanaan pembelajaran memiliki mekanisme untuk memonitor, mengkaji, dan memperbaiki secara periodik kegiatan perkuliahan (kehadiran dosen dan mahasiswa), penyusunan materi perkuliahan, serta penilaian hasil belajar</li>
								<li>5.3.1.b Mekanisme penyusunan materi perkuliahan</li>
							</a>
							<a href="<?php echo base_url(); ?>standar5/simulasipenilaian/5.3.2" target="_blank">
								<li>5.3.2 Mutu soal ujian</li>
							</a>
						</ul>
					</div>
					
					<div id="5.4" class="tabcontent">
						<span onclick="closePoin(event, '5.4')" class="topright">x</span>
						<h3>5.4 Sistem Pembimbingan Akademik</h3>
						<ul class="main-ul">
							<a href="<?php echo base_url(); ?>standar5/simulasipenilaian/5.4" target="_blank">
								<li>5.4.1.a Rata-rata banyaknya mahasiswa per dosen Pembimbing</li>
								<li>5.4.1.b Pelaksanaan kegiatan pembimbingan akademik</li>
								<li>5.4.1.c Jumlah rata-rata pertemuan pembimbingan per mahasiswa per semester</li>
								<li>5.4.2 Efektivitas kegiatan perwalian</li>
							</a>	
						</ul>
					</div>
					
					<div id="5.5" class="tabcontent">
						<span onclick="closePoin(event, '5.5')" class="topright">x</span>
						<h3>5.5 Pembimbingan Tugas Akhir / Skripsi</h3>
						<ul class="main-ul">
							<a href="<?php echo base_url(); ?>standar5/simulasipenilaian/5.5.1" target="_blank">
								<li>5.5.1.a Ketersediaan panduan, sosialisasi, dan penggunaan</li>
								<li>5.5.1.b Rata-rata mahasiswa per dosen pembimbing tugas akhir </li>
								<li>5.5.1.c Rata-rata jumlah pertemuan/pembimbingan selama penyelesaian TA</li>
								<li>5.5.1.d Kualifikasi akademik dosen pembimbing tugas akhir</li>
							</a>
							<a href="<?php echo base_url(); ?>standar5/simulasipenilaian/5.5.2" target="_blank">
								<li>5.5.2 Rata-rata waktu penyelesaian penulisan tugas akhir</li>
							</a>	
						</ul>
					</div>
					
					<div id="5.6" class="tabcontent">
						<span onclick="closePoin(event, '5.6')" class="topright">x</span>
						<h3>5.6 Upaya Perbaikan Pembelajaran</h3>
						<ul class="main-ul">
							<a href="<?php echo base_url(); ?>standar5/simulasipenilaian/5.6" target="_blank">
								<li>5.6.1 Upaya perbaikan sistem pembelajaran yang telah dilakukan selama tiga tahun terakhir</li>
							</a>
						</ul>
					</div>
					
					<div id="5.7" class="tabcontent">
						<span onclick="closePoin(event, '5.7')" class="topright">x</span>
						<h3>5.7 Upaya Peningkatan Suasana Akademik</h3>
						<ul class="main-ul">
							<a href="<?php echo base_url(); ?>standar5/simulasipenilaian/5.7" target="_blank">
								<li>5.7.1 Kebijakan tertulis tentang suasana akademik (otonomi keilmuan, kebebasan akademik, kebebasan mimbar akademik, kemitraan dosen-mahasiswa)</li>
								<li>5.7.2 Ketersediaan dan kelengkapan jenis prasarana, sarana serta dana yang memungkinkan terciptanya interaksi akademik antara sivitas akademika</li>
								<li>5.7.3 Interaksi akademik berupa program dan kegiatan akademik, selain perkuliahan dan tugas-tugas khusus, untuk menciptakan suasana akademik (seminar, simposium, lokakarya, bedah buku dll)</li>
								<li>5.7.4 Interaksi akademik antara dosen-mahasiswa</li>
								<li>5.7.5 Pengembangan perilaku kecendekiawanan</li>
							</a>	
						</ul>
					</div>
					
				</div>
				
				<div class="HasilSimulasi">
					<h3 style="padding-bottom: 5px">Hasil Simulasi Penilaian Borang Program Studi Sistem Informasi Standar 5</h3>
					<p>Jumlah Hasil Bobot x Nilai Standar 5 saat ini adalah <?php echo number_format($jumlahhasil_penilaian, 2) ?> </p>
					<p>Maksimal Hasil Bobot x Nilai Standar 5 adalah 75.24 </p>
					<table id="tableHasilPenilaian" class="display" width="100%" cellspacing="0">
						<thead class="table head thead-hasilsimulasipenilaian">
							<th>No</th>
							<th>No. Butir Penilaian</th>
							<th>Aspek Penilaian</th>
							<th>Informasi dari Borang PS</th>
							<th>Bobot</th>
							<th>Nilai</th>
							<th>Nilai X Bobot</th>
							<th>Catatan</th>
							<th>Penilaian dan Catatan (oleh)</th>
						</thead>
						<tbody class="table body">
							<?php
							if($hasil_penilaian !== "kosong" && $hasil_penilaian){
								foreach($hasil_penilaian as $row){
								?>	
								<tr id="row:<?php echo $row['id'] ?>">
									<td><?php echo $row['id']?></td>
									<td><?php echo $row['butir_penilaian']?></td>
									<td><?php echo $row['deskripsi_aspeknilai']?></td>
									<td></td>
									<td><?php echo $row['bobot']?></td>
									<td><?php echo $row['nilai']?></td>
									<td><?php echo $row['bobotxnilai']?></td>
									<td><?php echo $row['catatan']?></td>
									<td><?php echo $row['nama']?></td>
								</tr>
							<?php
								}
							}
							?>	
						</tbody>
					</table>	
				</div>
				
			</div>
		</div>

		<div class="footer">
			<?php
			include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/footer.php";
			?>
		</div>
		
		<script>
		function openPoin(evt, poinName) {
			// Declare all variables
			var i, tabcontent, tablinks;

			// Get all elements with class="tabcontent" and hide them
			tabcontent = document.getElementsByClassName("tabcontent");
			for (i = 0; i < tabcontent.length; i++) {
				tabcontent[i].style.display = "none";
			}

			// Get all elements with class="tablinks" and remove the class "active"
			tablinks = document.getElementsByClassName("tablinks");
			for (i = 0; i < tablinks.length; i++) {
				tablinks[i].className = tablinks[i].className.replace(" active", "");
			}

			// Show the current tab, and add an "active" class to the button that opened the tab
			document.getElementById(poinName).style.display = "block";
			evt.currentTarget.className += " active";
		}
		</script>
		<script>
		function closePoin(evt, poinName){
			tablinks = document.getElementsByClassName("tablinks");
			for (i = 0; i < tablinks.length; i++) {
				tablinks[i].className = tablinks[i].className.replace(" active", "");
			}
			document.getElementById(poinName).style.display = "none";
		}
		</script>
		
	</body>

	
	
</html>
