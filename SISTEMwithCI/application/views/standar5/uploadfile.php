<?php
if(!isset($_SESSION['nama'])){
      header("location:" . base_url());
      exit();
   }	
?>

<!DOCTYPE html>

<html>
	<head>
		<title>Akreditasi | Upload File</title>
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/mainlayout.css">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<meta name="robots" content="upload,follow" />
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/upload.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/jquery.dataTables.css" />
		<script type = 'text/javascript' src="<?php echo base_url(); ?>js/jquery-1.12.4.js"></script>

		
		
		<!--<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>-->
		<script>	
			function makeFileList() {
				var input = document.getElementById("files");
				var ul = document.getElementById("fileList");
				while (ul.hasChildNodes()) {
					ul.removeChild(ul.firstChild);
				}
				for (var i = 0; i < input.files.length; i++) {
					var li = document.createElement("li");
					var label = document.createElement("label");
					var label2 = document.createTextNode("Deskripsi :");
					label.setAttribute("for", "input-deskripsi");
					label.setAttribute("style", "padding-right: 2%");
					label.appendChild(label2);
					var text = document.createElement("input");
					text.setAttribute("type", "text");
					text.setAttribute("id", "input-deskripsi");
					text.setAttribute("name", "input-deskripsi");
					
					
					li.innerHTML = input.files[i].name;
					ul.appendChild(li);
					$("#fileList").append(label);
					$("#fileList").append(text);
					
				}
				
				if(!ul.hasChildNodes()) {
					var li = document.createElement("li");
					li.innerHTML = "No Files Selected";
					ul.appendChild(li);
				}
			}
		</script>
		
	</head>
	
	<body>
		<div class="header">
			<h1><a href="<?php echo base_url(); ?>dashboard">Akreditasi SI</a></h1>
			<?php
				include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/header.php";
			?>
		</div>
		
		<div class="sidebar">
			<?php
				include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/sidebar.php";
			?>
		</div>
			
		<div class="main-layout">
			<div class="sub-header">
			<h2>Standar 5: Kurikulum, Pembelajaran, dan Suasana Akademik</h2>
			</div>
			<div class="main-content">
				<div class="sub-header">
				<h2 >Upload file Data Akredetasi Standar 5</h2>
				</div>
				<?php
					
				if($_SESSION['hak_akses'] == strtolower('Standar 5')){
				
				?>
				
					<div class="upload-form">
						
						<form action="uploading" method="post" enctype="multipart/form-data">
							<label for="kebutuhan"><strong>Kebutuhan</strong></label>
							<select id="kebutuhan" name="kebutuhan" required>
								<option value=""></option>
								<option value="Kurikulum">Kurikulum</option>
								<option value="Pembelajaran">Pembelajaran</option>
								<option value="Suasana Akademik">Suasana Akademik</option>
							</select>
							<p class="huruf-upload-files"><strong>Upload Files: <input required type="file" name="files[]" id="files" multiple="" onchange="makeFileList();" /></strong>
							</p>
							<p><strong>Files You Selected:</strong></p>	
							<ul id="fileList" style="padding-left: 1.3%;">
							</ul>
							<button class="upload-btn" type="submit" name="btn-upload">UPLOAD</button>
						</form>

						<?php
							if(isset($_GET['success']))	{
								?>
								<label>File: </label>
								<ul id="fileListUpload" style="padding-left: 1.3%;">
									<?php
									$sql="SELECT * FROM file_uploads_standar5 WHERE date IN (SELECT MAX(date) FROM file_uploads_standar5)";
									$result_set = $this->db->query($sql);
									foreach($result_set->result_array() as $row){
									?>								
										<li><a href="<?php echo base_url(); ?>uploads/dokumenstandar5/<?php echo $row['file_name'] ?>" target="_blank"> <?php echo $row['file_name'] ?> </a></li>	
								<?php
									}
								?>
								</ul>								
								<label>Uploaded Successfully... </label>						
								<p><strong>[<a href="uploadfile">Upload Again</a>]</strong></p>	
								<?php
							} else if(isset($_GET['fail'])) {
								?>
								<label>Problem While File Uploading ! [<a href="uploadfile">Upload Again</a>]</label>
								<?php
							}
							
								?>
					</div>
				<?php	
				} else {
				?>
					<div class="upload-form">
						<p style='color:red'><strong>Anda tidak bisa upload file, karena bukan Anggota tim pengumpul data standar 5.</strong></p>	
					</div>	
				<?php
				}
				
				?>
				
				
				<div class="view-data-uploaded">
				<hr>
					<h3>File Data Akredetasi Standar 5</h3>
					<table id="table-view-data-uploaded" class="display" width="100%" cellspacing="0">
						<thead class="thead">
							<tr>
								<th>Nama File</th>
								<th>Kebutuhan</th>
								<th>Deskripsi</th>
								<th>Tanggal Upload (Y-m-d)</th>
								<th>Ukuran (Bytes)</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
						</tbody>
						<tfoot class="thead">
							<tr>
								<th>Nama File</th>
								<th>Kebutuhan</th>
								<th>Deskripsi</th>
								<th>Tanggal Upload (Y-m-d)</th>
								<th>Ukuran (Bytes)</th>
							</tr>
						</tfoot>
						
					</table>
				</div>
				
			</div>
			
		</div>

		<div class="footer">
			<?php
			include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/footer.php";
			?>
		</div>
		<script type = 'text/javascript' src="<?php echo base_url('assets/datatables/js/jquery.dataTables.min.js');?>"></script>
		
		<script type="text/javascript">
			var table;
			$(document).ready(function() {
				table = $('#table-view-data-uploaded').DataTable({
					"processing": true, //Feature control the processing indicator.
					"serverSide": true, //Feature control DataTables' server-side processing mode.
					"order": [], //Initial no order.
			 
					// Load data for the table's content from an Ajax source
					"ajax": {
						"url": "<?php echo site_url('standar5/ajax_list')?>",
						"type": "POST"
					},
			 
					//Set column definition initialisation properties.
					"columnDefs": [
					{ 
						"targets": [ 0 ], //first column / numbering column
						"orderable": true, //set not orderable
						"render": function ( data, type, row, meta ) {
							var itemID = row[0];                   
							return '<a href="<?php echo base_url(); ?>uploads/dokumenstandar5/' + itemID + '" target="_blank">' + data + '</a>';
						}
					},
					],
				});
			} );
		</script>
	</body>

	
	
</html>
