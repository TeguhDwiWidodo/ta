<?php

require $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/Classes/PHPExcel/IOFactory.php";

if(isset($_POST['btn-upload'])){  
	if(isset($_FILES['files'])){
		$nama = $_SESSION['nama'];
		foreach($_FILES['files']['tmp_name'] as $key => $tmp_name) {
			$table = $_POST['table'];
			$inputfilename = str_replace(' ','',$_FILES['files']['name'][$key]);
			$rawBaseName = pathinfo($inputfilename, PATHINFO_FILENAME );
			$extension = pathinfo($inputfilename, PATHINFO_EXTENSION );
			$folder = "uploads/dokumenpendukungstd5/";
			$excelhtml_path = str_replace(' ','',$folder.$inputfilename.'.html');
			$excel_path = str_replace(' ','',$folder.$inputfilename);
			$counter = 0;
			while(file_exists($folder.$inputfilename)) {
				$inputfilename = '[Versi'.$counter.']'.$rawBaseName . '.' . $extension;
				$counter++;
			};
			if(move_uploaded_file($tmp_name,$folder.$inputfilename)) {
				try {
					$inputfiletype = PHPExcel_IOFactory::identify($folder.$inputfilename);
					$objReader = PHPExcel_IOFactory::createReader($inputfiletype);
					$objPHPExcel = $objReader->load($folder.$inputfilename);
					$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'HTML');
					$objWriter->save($folder.$inputfilename.'.html');
				} catch(Exception $e) {
					die('Error loading file "'.pathinfo($folder.$inputfilename,PATHINFO_BASENAME).'": '.$e->getMessage());
				}
				$sheet = $objPHPExcel->getSheet(0); 
				$highestRow = $sheet->getHighestRow(); 
				$highestColumn = $sheet->getHighestColumn();
				$headings = $sheet->rangeToArray('A1:' . $highestColumn . 1, NULL, TRUE, FALSE);
				$lowercaseheadings = array_map('strtolower', $headings[0]);
				$updatedheadings = str_replace(' ','_', $lowercaseheadings);
				for ($row = 2; $row <= $highestRow; $row++){ 
					//  Read a row of data into an array
					$rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
					if($rowData[0][2]){
					//  Insert row data array into your database of choice here
						$sql = "INSERT INTO $table VALUES ('".$rowData[0][0]."', '".$rowData[0][1]."', '".$rowData[0][2]."', '$excel_path', '$excelhtml_path') 
						ON DUPLICATE KEY UPDATE ".$updatedheadings[0]."='".$rowData[0][0]."', ".$updatedheadings[1]."='".$rowData[0][1]."', ".$updatedheadings[2]."='".$rowData[0][2]."', 
						excel_path='$excel_path', excelhtml_path='$excelhtml_path';";
					} else if ($rowData[0][3]){
						$sql = "INSERT INTO $table VALUES ('".$rowData[0][0]."', '".$rowData[0][1]."', '".$rowData[0][2]."', '".$rowData[0][3]."');";
					}
					
					$this->db->query($sql);
				?>
					<script>
						alert('successfully uploaded');
						window.location.href='../standar5/dokumenpendukung?success';
					</script>
				<?php
				}
			} else {
				?>
				<script>
					alert('error while uploading file');
					window.location.href='../standar5/dokumenpendukung?fail';
				</script>
				<?php
			}
			
			
			
		}
	} else {
		echo "file tidak ada";
	}
}