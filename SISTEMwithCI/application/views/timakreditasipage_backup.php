<?php
if(!isset($_SESSION['nama'])){
      header("location:" . base_url());
      exit();
   }	
?>

<!DOCTYPE html>
<html>
	<head>
		<title>Akreditasi | List Tim Akreditasi</title>
		<script type = 'text/javascript' src="<?php echo base_url(); ?>js/jquery-1.12.4.js"></script>
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/mainlayout.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/timakreditasi.css">
		<script type = 'text/javascript' src="<?php echo base_url(); ?>js/jquery.dataTables.js"></script>
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/jquery.dataTables.css">
		<script type = 'text/javascript' >
		(function ($){	
			$(document).ready(function() {
				$('#table-view-data-tim-akreditasi').dataTable({
					
				});
			});	
		})(jQuery);	
		</script>
	</head>
	
	<body>
		<div class="header">
			<h1><a href="<?php echo base_url(); ?>dashboard">Akreditasi SI</a></h1>
			<?php
				include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/header.php";
			?>
		</div>
		
		<div class="sidebar">
			<?php
				include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/sidebar.php";
			?>
		</div>
			
		<div class="main-layout">
			<div class="sub-header">
			<h2>Tim Pengumpul Data Akreditasi</h2>
			</div>
			<div class="main-content">
				<div class="sub-header">
				<h2 >List Data Diri Tim Akreditasi</h2>
				</div>
				<div class="tim-akreditasi" >
					<h3>Tim Anggota Akreditasi</h3>
					<table id="table-view-data-tim-akreditasi" class="display" width="100%" cellspacing="0">
						<thead class="table head">
							<th >Email</th>
							<th >Nama</th>
							<th >No Hanphone</th>
							<th >Hak Akses</th>
							<th>Tugas</th>
							<th >Edit Tugas</th>
						</thead>
							
						<tbody class="table body">
							<?php
									$sql="SELECT * FROM users";
									$result_set = $this->db->query($sql);
									foreach($result_set->result_array() as $row){
									?>	
										<tr>
											<td ><?php echo $row['email'] ?></td> 	
											<td><?php echo $row['nama'] ?></td>
											<td><?php echo $row['hp'] ?></td>
											<td><?php echo $row['hak_akses'] ?></td>
											<td id="tugas:<?php echo $row['id_user'] ?>" contenteditable="true"><?php echo $row['tugas'] ?></td>
											<td><div id="myButtonedit" style="display:block;"><button type="button" class="btn-plus"><i class="fa fa-sticky-note" aria-hidden="true"></i></i></button></div>
								<div  id="myButtonsave" style="display:none;"><button type="button" class="btn-plus"><i class="fa fa-floppy-o" aria-hidden="true"></i></button></div></td>
										</tr>
								<?php
									}
								?>
							<tr>
								
								<td><p>widodolemurian@gmail.com</p></td>
								<td><p>Teguh Dwi Widodo</p></td>
								<td><p>082217318797</p></td>
								<td><p>Standar 7</p></td>
								<td ><textarea>aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa</textarea></td>
							    <td><div id="myButtonedit" style="display:block;"><button type="button" class="btn-plus"><i class="fa fa-sticky-note" aria-hidden="true"></i></i></button></div>
								<div  id="myButtonsave" style="display:none;"><button type="button" class="btn-plus"><i class="fa fa-floppy-o" aria-hidden="true"></i></button></div>
								</td>
								<script>
								(function ($){	
								$(document).ready(function() {	
									var eb = document.getElementById('myButtonedit');
									var sb = document.getElementById('myButtonsave');
									
									eb.onclick = function(){
										document.getElementById('data-diri-tugas').removeAttribute('readonly');
										document.getElementById('data-diri-tugas').style.background ="#f2f2f2";
										document.getElementById('myButtonsave').style.display ="block";
										document.getElementById('data-diri-tugas').style.outline ="thin solid #bfbfbf";
										document.getElementById('myButtonedit').style.display ="none";
									}
									sb.onclick = function(){
										document.getElementById('data-diri-tugas').readOnly =true;
										document.getElementById('myButtonsave').style.display ="none";
										document.getElementById('myButtonedit').style.display ="block";
										document.getElementById('data-diri-tugas').style.outline ="none";
										document.getElementById('data-diri-tugas').style.background ="white";
									};
									});	
								})(jQuery);
								</script>
							</tr>
						</tbody>
						
					</table>
				</div>
				
				
			</div>
		</div>

		<div class="footer">
			<?php
			include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/footer.php";
			?>
		</div>
		<script>
			$(function(){
				//acknowledgement message
				var message_status = $("#status");
				$("td[contenteditable=true]").blur(function(){
					var field_userid = $(this).attr("id") ;
					var value = $(this).text() ;
					$.post('edittugas' , field_userid + "=" + value, function(data){
						if(data != '')
						{
							message_status.show();
							message_status.text(data);
							//hide the message
							setTimeout(function(){message_status.hide()},3000);
						}
					});
				});
			});(jQuery)

		</script>
	</body>

	
	
</html>
