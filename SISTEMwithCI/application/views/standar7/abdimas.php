
<!DOCTYPE html>
<html>
	<head>
		<title>Akreditasi | Pengabdian Masyarakat</title>
		<script type = 'text/javascript' src="<?php echo base_url(); ?>js/jquery-1.12.4.js"></script>
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/mainlayout.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/penelitian.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/accordion.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/dosen.css">
		<style>
		.tb_jmlhpengmas, th, td{
			border:1px solid gray;
			text-align: center;


		}
		</style>
		<script type = 'text/javascript' src="<?php echo base_url(); ?>js/jquery.dataTables.js"></script>
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/jquery.dataTables.css">
		<script type = 'text/javascript' >
		(function ($){
			$(document).ready(function() {
				$('#table-view-data-tim-akreditasi').dataTable({
					"order": [[ 3, 'asc' ]],
					"scrollX": true,
				});
			});
		})(jQuery);
		</script>
	</head>

	<body>
		<div class="header">
			<h1><a href="<?php echo base_url(); ?>dashboard">Akreditasi SI</a></h1>
			<?php
				include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/header.php";
			?>
		</div>

		<div class="sidebar">
			<?php
				include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/sidebar.php";
			?>
		</div>

		<div class="main-layout">

			<div class="main-content">
				<div class="menu-breadcrumb">
					<ul class="breadcrumb">
						<li><a href="<?php echo base_url(); ?>dashboard" id="menu-breadcrumb">Dashboard</a></li>
						<li>Pengmas</li>
					</ul>
				</div>
				<div class="menu-head">
					<ul>
						<li><a href="<?php echo base_url(); ?>standar7/penelitian" id="menu-head" >Penelitian</a></li>
						<li><a href="<?php echo base_url(); ?>standar7/abdimas" id="menu-head" style="background:#d9d9d9;color:black;">Pengmas</a></li>
						<li><a href="<?php echo base_url(); ?>standar7/kerjasama" id="menu-head">Kerjasama</a></li>
					</ul>
				</div>
					<div class="menu-subhead">
					<ul>
						<li><a href="<?php echo base_url(); ?>standar7/abdimasdata" id="menu-subhead">Data Pengmas</a></li>
					</ul>
				</div>
				<div class="sub-header">
				<h2> Standar 7 : Simulasi Penilaian Pengabdian Masyarakat</h2>
				</div>
			</div>



			<div class="kerjasama_layout">
				<div class="721jlmhabdimas">
					</br>
					<button class="accordion">7.2.1 Pedoman Penilaian Kegiatan Pengabdian Masyarakat</button>
					<div class="panel">
						<div class="panel-pedoman">
							<div style="">
								<div style="width:30%;float:left;padding:1%;">
								<h4>Pedoman Penilaian</h4>
								<label style="font-size:80%;text-align:justify;">Jumlah kegiatan Pengabdian kepada Masyarakat (PkM) yang dilakukan oleh dosen tetap yang bidang keahliannya sesuai dengan PS</label>
								</br>
								</br>
								<h4>Deskriptor</h4>
								<label style="font-size:80%;text-align:justify;">Jumlah kegiatan pelayanan/pengabdian kepada masyarakat (PkM) yang dilakukan oleh dosen tetap yang bidang keahliannya sama dengan PS selama tiga tahun.
								</br> Penilaian dilakukan dengan penghitungan berikut:
								</br>
										NK = Nilai kasar =
										<table style="text-align:center;border: 1px solid gray;">
												<tbody>
													<tr >
														 <td style="border:hidden;" rowspan="4">NK =</td>
													 </tr>
												<tr >
													 <td style="border:hidden;">4xn<sub>a</sub> + 2xn<sub>b</sub> + n<sub>c</sub></td>
												 </tr>
												 <tr >
													 <td style="border:hidden;"><hr></td>
												 </tr>
													<tr >
														<td style="border:hidden;">f</td>
												</tr>
											</tbody>
										</table>
									</label>
								</div>
								<div style="padding:1%;">
									<label><b>Keterangan</b></label>
										<table style="margin-left:0;font-size:80%;text-align:justify;">
												<tbody >
													<tr>
														 <td style="border:hidden;">n<sub>a</sub></td>
														 <td style="border:hidden;">=</td>
														 <td style="border:hidden;text-align:justify;"> Jumlah kegiatan PkM dengan biaya luar negeri yang sesuai bidang ilmu</sub></td>
												 </tr>
												 <tr>
														<td style="border:hidden;">n<sub>b</sub></td>
														<td style="border:hidden;">=</td>
														<td style="border:hidden;text-align:left;">Jumlah kegiatan PkM dengan biaya luar yang sesuai bidang ilmu</sub></td>
												</tr>
												<tr>
													 <td style="border:hidden;">n<sub>c</sub></td>
													 <td style="border:hidden;">=</td>
													 <td style="border:hidden;text-align:left;">Jumlah kegiatan PkM dengan biaya dari PT/sendiri yang sesuai bidang ilmu</sub></td>
											 </tr>
											 <tr>
													<td style="border:hidden;">f</td>
													<td style="border:hidden;">=</td>
													<td style="border:hidden;text-align:left;">Jumlah dosen tetap yang bidang keahliannya sesuai dengan PS</sub></td>
											</tr>
											</tbody>
										</table>
									</br>
										<label><b>Harkat dan Peringkat</b></label>
										<table class="table-pedoman"; style="position:relative;text-align:center;margin-left:0;">
											<tr>
												<th style="background-color:green;font-size:80%;padding:1%;width:25%;border:1px solid gray;">Sangat Baik</th>
												<th style="background-color:#00ff00;font-size:80%;padding:1%;width:12%;border:1px solid gray;">Baik</th>
												<th style="background-color:orange;font-size:80%;padding:1%;width:11%;border:1px solid gray;">Cukup</th>
												<th style="background-color:#ff3300;font-size:80%;padding:1%;width:12%;border:1px solid gray;">Kurang</th>
												<th style="background-color:red;font-size:80%;padding:1%;width:25%;border:1px solid gray;">Sangat Kurang</th>
											</tr>
												 <tr>
														<td style="border:1px solid gray;">4</sub></td>
														<td style="border:1px solid gray;">3</sub></td>
														<td style="border:1px solid gray;">2</sub></td>
														<td style="border:1px solid gray;">1</sub></td>
														<td style="border:1px solid gray;">0</sub></td>
												</tr>
												<tr>
													 <td style="text-align:center;font-size:80%;padding:1%;border:1px solid gray;">Jika NK ≥ 1, maka skor = 4.</td>
												 	 <td colspan="3" style="text-align:center;font-size:80%;padding:1%;border:1px solid gray;">Jika 0 <=  NK <  1, maka skor = (3 x NK) + 1</td>
													 <td style="text-align:center;font-size:80%;padding:1%;border:1px solid gray;">Jika NK = 0, maka skor = 0</td>
												</tr>
										</table>
									</br>
								</div>
							</div>
						</div>
					</div>
					<div  class="panel-simulasi">
						<div style="border:1px solid #3399ff; width:100%;background:white;position:relative;overflow:hidden;" class="smlsi-nilai-dl">
              <div style="font-size:15px;height:20px;padding:1%;padding-left:1.5%;background:#3399ff;">
                <label>7.2.1 Penilaian Pengabdian Masyarakat</label>
              </div>
							<div style="position:relative;float:left;width:40%;padding:2%;" class="panel-dtabdimas">
								<label><b>Tabel Jumlah Pengabdian Masyarakat</b></label>
									<table  class="tb_jmlhpengmas" style="font-size: 80%;">
													<tr>
															<th style="width:50%;">Sumber Pembiayaan</th>
															<th style="width:15%;background:#ffcc00;">TS-2</th>
															<th style="width:15%;background:#66ff66;">TS-1</th>
															<th style="width:15%;background:#66ccff;">TS</th>
													</tr>
													<tr>
														<td style="text-align:left;padding:0.5%;">Pembiayaan sendiri oleh peneliti</td>
														<td><?php  echo $biaya_sendiri_abdimas_ts2; ?></td>
														<td><?php  echo $biaya_sendiri_abdimas_ts1; ?></td>
														<td><?php  echo $biaya_sendiri_abdimas_ts; ?></td>
													</tr>
													<tr>
														<td style="text-align:left;padding:0.5%;">PT yang bersangkutan</td>
														<td><?php echo $pt_bersangkutan_abdimas_ts2; ?></td>
														<td><?php echo $pt_bersangkutan_abdimas_ts1; ?></td>
														<td><?php echo $pt_bersangkutan_abdimas_ts; ?></td>
													</tr>
													<tr>
														<td style="text-align:left;padding:0.5%;">Depdiknas</td>
														<td><?php echo $depdiknas_abdimas_ts2; ?></td>
														<td><?php echo $depdiknas_abdimas_ts1; ?></td>
														<td><?php echo $depdiknas_abdimas_ts; ?></td>
													</tr>
													<tr>
														<td style="text-align:left;padding:0.5%;">Institusi dalam negeri di luar Depdiknas</td>
														<td><?php echo $luar_depdiknas_abdimas_ts2; ?></td>
														<td><?php echo $luar_depdiknas_abdimas_ts2; ?></td>
														<td><?php echo $luar_depdiknas_abdimas_ts2; ?></td>
													</tr>
													<tr>
														<td style="text-align:left;padding:0.5%;">Institusi luar negeri</td>
														<td><?php echo  $biaya_luar_negeri_abdimas_hitung_ts2; ?></td>
														<td><?php echo  $biaya_luar_negeri_abdimas_hitung_ts1; ?></td>
														<td><?php echo  $biaya_luar_negeri_abdimas_hitung_ts; ?></td>
													</tr>
												</tbody>
									</table>

									<form action ="<?php echo base_url(); ?>Reportstd/rptpenelitian721" target="_blank">
						             <input class="btn" style="margin-left:0;" type="submit" value="Cetak" />
						         </form>

							</div>
							<div class="panel-penilaian" style="padding:2%;position:relative;float:left;width:50%;">
								<label><b>Simulasi Nilai</b></label>
							</br>

									<?php
										$na2 = $biaya_luar_negeri_abdimas;
										$nb2 = $depdiknas_abdimas + $luar_depdiknas_abdimas;
										$nc2 = $biaya_sendiri_abdimas + $pt_bersangkutan_abdimas;

										$na2_hitung = 4 * $na2;
										$nb2_hitung = 2 * $nb2;
										$kalkulasi_n = $na2_hitung + $nb2_hitung + $nc2 ;

										                  if(  $kalkulasi_n  == null || $jml_dosen==null){

										    $nilai_kasar =0;
										                  } else {

										   	$nilai_kasar =  $kalkulasi_n / $jml_dosen;
										                  }

																			 ?>
																			 <table style="margin-left:0;	font-size: 80%;">
																				 <tr>
																					 	<td style="border:hidden;text-align:left;">Na x 4</td>
																						<td style="border:hidden;">=</td>
																						<td style="border:hidden;"><?php echo $na2_hitung; ?></td>
																				 </tr>
																				 <tr>
																					 <td style="border:hidden;text-align:left;"><label>Nb x 2</td>
																					 <td style="border:hidden;">=</td>
																					 <td style="border:hidden;"><?php echo $nb2_hitung; ?></td>
																				</tr>
																				<tr>
																					 <td style="border:hidden;text-align:left;">Nc</td>
																					 <td style="border:hidden;">=</td>
																					 <td style="border:hidden;"><?php echo $nc2; ?></td>
																				</tr>
																				<tr>
																					 <td style="border:hidden;text-align:left;">f</td>
																					 <td style="border:hidden;">=</td>
																					 <td style="border:hidden;"><?php echo $jml_dosen; ?></td>
																				</tr>
																				<tr>
																					 <td style="border:hidden;text-align:left;">Nilai kasar</td>
																					 <td style="border:hidden;">=</td>
																					 <td style="border:hidden;"><?php echo  round($nilai_kasar, 4); ?></td>
																				</tr>
																				<tr>
																					 <td style="border:hidden;text-align:left;">Harkat dan Peringkat</td>
																					 <td style="border:hidden;">=</td>
																					 <td style="border:hidden;"><?php

											 										if($nilai_kasar >= 1  ){
											 										$skor =  4;
											 										echo  $skor;

											 										}	else if($nilai_kasar > 0 && $nilai_kasar < 1){
											 											 $skor = 3 * $nilai_kasar +1;
											 										 echo   round($skor, 4);

											 										 } else if($nilai_kasar == 0){
											 											 $skor = 0;
											 										 echo   $skor;

											 										 }

											 																								 ?>
																					 <input type="hidden"  name="skor" value="<?php echo  $skor ?>" >

																						</td>
																				</tr>
																				<tr>
																					 <td style="border:hidden;text-align:left;">Bobot</td>
																					 <td style="border:hidden;">=</td>
																					 <td style="border:hidden;">1.88</td>
																				</tr>
																				<tr>
																					 <td style="border:hidden;text-align:left;">Nilai Simulasi</td>
																					 <td style="border:hidden;">=</td>
																					 <td style="border:hidden;"><label id ="simulasinilai2"><?php echo round(($skor * 1.88), 4); ?></label></td>
																				</tr>
																			 </table>
																			 <hr>
										<button style="margin-top:4%;display: visible;left: 10;" name="btn-upload" type="button"  onclick="save_hasil9()" id="save_hasil1" class="save btn" value="save">Simpan </button>
										<script>
										function save_hasil9(){
											var nilai1 = document.getElementById("simulasinilai2").innerHTML;
											var sim_nilai = $("input[name=skor]").val();
										var id1 = "7.2.1";
										var bobot = 1.88;



										$.ajax({

												type: "POST",
												url: "<?php echo base_url(); ?>Standar7/simulasinilai",
											data: { nilai1 : nilai1, sim_nilai:sim_nilai, id1:id1, bobot:bobot},


												success: function(data){
										console.log(data);
										alert("Sukses input data nilai, hasil "+nilai1);
													},

												error: function(xhr, status, errorThrown){
												alert( xhr.status);
												alert( xhr.errorThrown);
												alert( xhr.responseText);
												console.log(xhr.responseText);
												}
										});

										}
										</script>
							</div>
						</div>
					</div>

					</div>




					<div class="722">
					</br>
						<button class="accordion">7.2.2 Keterlibatan Mahasiswa</button>
						<div class="panel">
							<div class="panel-pedoman">
								<div style="padding:2%;width:30%;float:left;">
									<h4>Pedoman Penilaian</h4>
									<label  style="font-size:80%;">Keterlibatan mahasiswa dalam kegiatan pengabdian kepada masyarakat.</label>
									</br>
									</br>
									<h4>Deskriptor</h4>
									<label style="font-size:80%;">Keterlibatan mahasiswa dalam kegiatan pelayanan/pengabdian kepada masyarakat</label>
								</div>
								<div style="padding:2%;">
									<label><b>Harkat dan Peringkat</b></label>
										<table style="text-align:center;">
												<tbody>
													<tr >
														<th style="background-color:green;font-size:80%;padding:1%;width:20%;border:1px solid gray;">Sangat Baik</th>
													 <th style="background-color:#00ff00;font-size:80%;padding:1%;width:20%;border:1px solid gray;">Baik</th>
													 <th style="background-color:orange;font-size:80%;padding:1%;width:20%;border:1px solid gray;">Cukup</th>
													 <th style="background-color:#ff3300;font-size:80%;padding:1%;width:20%;border:1px solid gray;">Kurang</th>
													 <th style="background-color:red;font-size:80%;padding:1%;width:20%;border:1px solid gray;">Sangat Kurang</th>
												 </tr>
												 <tr >
														<td style="font-size:80;">4</sub></td>
														<td style="font-size:80;">3</sub></td>
														<td style="font-size:80;">2</sub></td>
														<td style="font-size:80;">1</sub></td>
														<td style="font-size:80;">0</sub></td>
												</tr>
												 <tr >
													 <td style="text-align:left;font-size:80%;padding:1%;">Mahasiswa terlibat penuh dan diberi tanggung jawab.</td>
													<td style="text-align:left;font-size:80%;padding:1%;">Mahasiswa terlibat penuh, namun tanggung jawab ada pada dosen Pembina.</td>
														<td style="text-align:left;font-size:80%;padding:1%;"> Mahasiswa hanya diminta sebagai tenaga pembantu.</td>
														<td style="text-align:left;font-size:80%;padding:1%;"> Mahasiswa hanya diminta sebagai tenaga pembantu.</td>
														<td style="text-align:left;font-size:80%;padding:1%;"> Mahasiswa hanya diminta sebagai tenaga pembantu.</td>
												</tr>

											</tbody>
										</table>
									</div>
							</div>
						</div>
						<div class="panel-simulasi">
							<div style="border:1px solid #3399ff; width:100%;background:white;position:relative;overflow:hidden;" class="smlsi-nilai-dl">
								<div style="font-size:15px;height:20px;padding:1%;padding-left:1.5%;background:#3399ff;">
									<label>7.2.2 Penilaian keterlibatan mahasiswa Pengabdian Masyarakat</label>
								</div>
								<div class="panel-dtpengmas1">

									<div style="padding:2%;text-align:justify;">
										<label><b>Deskripsi</b></label>
										<div style="border:1px solid gray;padding:2%;">
											<p >
											<?php
									 					echo	 $view722->text;
											 ?>
								 	 		</p>
									</div>
									<form action ="<?php echo base_url(); ?>Reportstd/rptpenelitian722" target="_blank">
						             <input class="btn" style="margin-left:0;" type="submit" value="Cetak" />
						         </form>
								</div>
								<div class="panel-penilaian">
									<div style="padding:1%;">
										<hr>
										</br>
										<label><b>Simulasi Penilaian</b></label>
										</br>
										<table style="margin-left:0;">
											<tr>
												<td style="width:20%;border:hidden;text-align:left;"><label>Input Nilai</label></td>
												<td style="width:1%;border:hidden;"><label>:</label>
												</td>
												<td style="width:20%;border:hidden;text-align:left;">
													<label title="Ada kerjasama dengan institusi di dalam negeri, banyak dalam jumlah.  Semuanya  relevan dengan bidang keahlian PS.">
														<span><input style="padding-left:1%;" id="hasil_option1" type="radio" onchange="calculasi1()"   name="nilai_option" value="4" />4 : Sangat Baik</span>
													</label>
												</td>
												<td style="width:14%;border:hidden;text-align:left;">
													<label title="Ada kerjasama dengan institusi di dalam negeri, cukup dalam jumlah.  Sebagian besar relevan dengan bidang keahlian PS" >
														<span><input style="padding-left:1%;" id="hasil_option2" type="radio" onchange="calculasi1()" name="nilai_option" value="3"/>3 : Baik</span></label>

												</td>
												<td style="width:14%;border:hidden;text-align:left;"><label>
													<label title="Ada kerjasama dengan institusi di dalam negeri, kurang dalam jumlah. Sebagian besar relevan dengan bidang keahlian PS" >
														<span><input style="padding-left:1%;" id="hasil_option3" type="radio" onchange="calculasi1()" name="nilai_option" value="2"/>2 : Cukup</span></label>

												</td>
												<td style="width:14%;border:hidden;text-align:left;" ><label>
													<label title="Belum ada atau tidak ada kerjasama" >
														<span><input style="padding-left:1%;" id="hasil_option4"  type="radio" onchange="calculasi1()" name="nilai_option" value="1"/>1 : Kurang</span></label>

												</td>
												<td style="width:20%;border:hidden;text-align:left;"><label><label title="(Tadak ada skor nol)" >
													<label title="(Tadak ada skor nol)" >
														<span><input style="padding-left:1%;" id="hasil_option5" type="radio" onchange="calculasi1()" name="nilai_option"   value="0"/>0 : Sangat Kurang</span></label>

												</td>
											</tr>
											<tr>
												<td style="width:20%;border:hidden;text-align:left;"><label>Bobot</label></td>
												<td style="width:1%;border:hidden;"><label>:</label></td>
												<td style="border:hidden;text-align:left;"  colspan="6"><label>1.88</label></td>
											</tr>
											<tr>
												<td style="width:20%;border:hidden;text-align:left;"><label>Hasil</label></td>
												<td style="width:1%;border:hidden;"><label>:</label></td>
												<td style="border:hidden;text-align:left;" colspan="6"><label id="result"></label></td>
											</tr>

										</table>



							<!--   <select id = "nilai1"  multiple = "multiple" onchange="calculasi1()" size = "2" width:"15%">
											<option value = "0" >0</option>
										<option value = "1" >1</option>
										<option value = "2">2</option>
										<option value = "3">3</option>
										<option value = "4" selected>4</option> -->
												 <?php
											//	 foreach($nilai_dalam as $nd){

													?>
												<!--  <label>hasil sebelumnya:</label> : <label><?php//  echo $nd->nilai; ?></label> </br> -->
												 <?php
										//		 }
													?>
													</br>
													<hr>
												 <button style="display: visible;left: 10;" name="btn-upload" type="button"  onclick="save_hasil1()" id="save_nilai" class="save btn" value="save" disabled="disabled">Simpan </button>
											 </br>
											 <script>
													$("#hasil_option1").change(function () {$("#save_nilai").prop("disabled", false);});
													$("#hasil_option2").change(function () {$("#save_nilai").prop("disabled", false);});
													$("#hasil_option3").change(function () {$("#save_nilai").prop("disabled", false);});
													$("#hasil_option4").change(function () {$("#save_nilai").prop("disabled", false);});
													$("#hasil_option5").change(function () {$("#save_nilai").prop("disabled", false);});
												 </script>
												 <script>
												 function calculasi1(){


																			 if( document.getElementById("hasil_option1").checked == true){
																					var nilai1 = document.getElementById("hasil_option1").value * 1.88;
																					document.getElementById("result").innerHTML= nilai1;
																					document.getElementById("save_nilai").style.visibility = 'visible';


																			 } else if( document.getElementById("hasil_option2").checked == true){
																					var nilai1 = document.getElementById("hasil_option2").value * 1.88;
																					document.getElementById("result").innerHTML= nilai1;
																					document.getElementById("save_nilai").style.visibility = 'visible';


																			 }else if( document.getElementById("hasil_option3").checked == true){
																					var nilai1 = document.getElementById("hasil_option3").value * 1.88;
																					document.getElementById("result").innerHTML= nilai1;
																					document.getElementById("save_nilai").style.visibility = 'visible';


																			 }else if( document.getElementById("hasil_option4").checked == true){
																					var nilai1 = document.getElementById("hasil_option4").value * 1.88;
																					document.getElementById("result").innerHTML= nilai1;
																					document.getElementById("save_nilai").style.visibility = 'visible';


																			 }else if( document.getElementById("hasil_option5").checked == true){
																					var nilai1 = document.getElementById("hasil_option5").value * 1.88;
																					document.getElementById("result").innerHTML= nilai1;
																					document.getElementById("save_nilai").style.visibility = 'visible';
																 }

															 }

															 function save_hasil1(){
																 var nilai1 = document.getElementById("result").innerHTML;
																 var sim_nilai = $("input[name=nilai_option]:checked").val();
															 var id1 = "7.2.2";

															 	var bobot = 1.88;
															 $.ajax({

																	 type: "POST",
																	 url: "<?php echo base_url(); ?>Standar7/simulasinilai",
																	   data: { nilai1 : nilai1, sim_nilai:sim_nilai, id1:id1, bobot:bobot},

																	 success: function(data){
															 console.log(data);
														 alert("Sukses input data nilai, hasil "+nilai1);
																		 },

																	 error: function(xhr, status, errorThrown){
																	 alert( xhr.status);
																	 alert( xhr.errorThrown);
																	 alert( xhr.responseText);
																	 console.log(xhr.responseText);
																	 }
															 });

															 }
												 </script>
									</div>
								</div>
							</div>
						</div>
					</div>
					<script>
								var acc = document.getElementsByClassName("accordion");
								var i;

								for (i = 0; i < acc.length; i++) {
									acc[i].onclick = function() {
									this.classList.toggle("active");
									var panel = this.nextElementSibling;
									if (panel.style.maxHeight){
										panel.style.maxHeight = null;
									} else {
										panel.style.maxHeight = panel.scrollHeight + "px";
									}
									}
								}
					</script>
				</div>

		</div>

		<div class="footer">
			<?php
			include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/footer.php";
			?>
		</div>

	</body>



</html>
