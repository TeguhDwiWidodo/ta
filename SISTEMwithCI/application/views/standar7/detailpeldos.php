<?php
if(!isset($_SESSION['nama'])){
      header("location:" . base_url());
      exit();
   }
?>

<!DOCTYPE html>
<html>
	<head>
		<title>Akreditasi | Detail</title>
		<script type = 'text/javascript' src="<?php echo base_url(); ?>js/jquery-1.12.4.js"></script>
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/mainlayout.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/penelitian.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/dosen.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/accordion.css">
		<script type = 'text/javascript' src="<?php echo base_url(); ?>js/jquery.dataTables.js"></script>
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/jquery.dataTables.css">
		<script type = 'text/javascript' >
			(function ($){
				$(document).ready(function() {
					$('#detail-peldos').dataTable({
						"order": [[ 0, 'asc' ]],

					});
				});
			})(jQuery);
		</script>

	</head>

	<body>

		<div class="header">
			<h1><a href="<?php echo base_url(); ?>dashboard">Akreditasi SI</a></h1>
			<?php
				include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/header.php";
			?>
		</div>

		<div class="sidebar">
			<?php
				include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/sidebar.php";
			?>
		</div>

		<div class="main-layout">

			<div class="main-content">
			<div class="menu-breadcrumb">
				<ul class="breadcrumb">
					<li><a href="<?php echo base_url(); ?>dashboard" id="menu-breadcrumb">Dashboard</a></li>
					<li>Penilaian</li>
				</ul>
			</div>
			<div class="menu-head">
				<ul>
					<li><a href="<?php echo base_url(); ?>standar7/penelitian" id="menu-head" >Penelitian</a></li>
					<li><a href="<?php echo base_url(); ?>standar7/abdimas" id="menu-head" >Abdimas</a></li>
					<li><a href="<?php echo base_url(); ?>standar7/kerjasama" id="menu-head" >Kerjasama</a></li>
				</ul>
			</div>

			<div class="kerjasama_layout" style="padding:2%;width:95%;">
        <h3 >Penelitian Dosen yang dijadikan TA Mahasiswa</h3>
        </br>
            <div>
        							<?php

        									foreach($result->result() as $row){


        									?>
                          <table>
      											<tr >
                              <td><label>Program Studi</label></td>
                              <td><label>:</label></td>
                              <td><label class="detail" title="<?php echo $row->program_studi ?>" id="programstudi<?php echo $row->idpeldos ?>"  type="text" name="no_ts" placeholder="" readonly ><?php echo $row->program_studi ?></label></td>
                              </tr>

      											<tr>
                              <td><label>No TS</label></td>
                              <td><label>:</label></td>
                              <td><label class="detail" title="<?php echo $row->no_ts ?>" id="no_ts<?php echo $row->idpeldos?>"  type="text" name="no_ts" placeholder="" readonly ><?php echo $row->no_ts ?></label></td>
                              </tr>
      										   <tr>
                                <td><label>Judul Penelitian</label></td>
                                <td><label>:</label></td>
                                <td><label class="detail"  title="<?php echo $row->judul ?>" id="judul<?php echo $row->idpeldos ?>"  type="text" name="no_ts" placeholder="" readonly ><?php echo $row->judul ?></label></td>
                              </tr>
                            <tr>
                              <td><label>Dosen Ketua</label></td>
                              <td><label>:</label></td>
                              <td><label class="detail" title="<?php echo $row->nama_dosenketua ?>" id="tipe<?php echo $row->idpeldos ?>"  type="text" name="no_ts" placeholder="" readonly ><?php echo $row->nama_dosenketua ?></label></td>
                            </tr>
                            <tr>
                              <td><label>Dosen Anggota</label></td>
                              <td><label>:</label></td>
                              <td><label class="detail" title="<?php echo $row->nama_dosenanggota_1 ?>" id="tipe<?php echo $row->idpeldos ?>"  type="text" name="no_ts" placeholder="" readonly ><?php echo $row->nama_dosenanggota_1 ?></label></td>
                            </tr>
                          </table>

                          <?php
                          }
                          ?>
              </div>
              <br/>
              <div>
                <table id="detail-peldos">
                <thead class="table head-peldosen">
                  <th >Program Studi</th>
                  <th >No Ts</th>
                  <th >Judul Tugas Akhir</th>
                  <th>Nama Mahasiswa </th>
                  </thead>
                <tbody>
            							<?php

            									foreach($result1->result() as $rows){


            									?>

                              <tr id="row:<?php echo $rows->id_peldos ?>">

          											<td ><label class="detail" title="<?php echo $rows->prodi ?>" id="programstudi<?php echo $rows->id_peldos ?>"  type="text" name="prodi" placeholder="" readonly ><?php echo $rows->prodi ?></label></td>
                                <td><label class="detail" title="<?php echo $rows->no_ts ?>" id="no_ts<?php echo $rows->id_peldos ?>"  type="text" name="no_ts" placeholder="" readonly ><?php echo $rows->no_ts ?></label></td>
          										  <td><label class="detail" title="<?php echo $rows->judul_ta_mahasiswa ?>" id="judulta<?php echo $rows->id_peldos ?>"  type="text" name="judul_ta_mahasiswa" placeholder="" readonly ><?php echo $rows->judul_ta_mahasiswa?></label></td>
                                <td><label class="detail" title="<?php echo $rows->nama_mahasiswa ?>" id="nama_mahasiswa<?php echo $rows->id_peldos ?>"  type="text" name="nama_mahasiswas" placeholder="" readonly ><?php echo $rows->nama_mahasiswa ?></label></td>

                              </tr>
                            </tbody>

                                                          <?php
                                                            }
                                                          ?>

                  </table>

              </div>



</div>
		<div class="footer">
			<?php
			include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/footer.php";
			?>
		</div>

	</body>



</html>
