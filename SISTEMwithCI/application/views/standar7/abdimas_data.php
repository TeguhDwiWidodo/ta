
<!DOCTYPE html>
<html>
	<head>
		<title>Akreditasi | Pengmas Data</title>
		<script type = 'text/javascript' src="<?php echo base_url(); ?>js/jquery-1.12.4.js"></script>
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/mainlayout.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/penelitian.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/dosen.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/accordion.css">
		<script type = 'text/javascript' src="<?php echo base_url(); ?>js/jquery.dataTables.js"></script>
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/jquery.dataTables.css">
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/ckeditor/ckeditor.js"></script>
		<script type = 'text/javascript' >
			(function ($){
				$(document).ready(function() {
					$('#table-view-data-kerjasama').dataTable({
						"order": [[ 0, 'asc' ]],
            "aLengthMenu": [5, 10, 25],
						iDisplayLength: 5,
						"scrollX": true,
					});
				});
			})(jQuery);
		</script>
		<script type='text/javascript'>
		$(function() {
						$( "#tags" ).autocomplete({
							 source: "<?php echo base_url(); ?>standar7/dosenautocomplete",
							 dataType:'json',
							 type:'POST',
							 minLength: 4

						});
				 });
		</script>
		<script type='text/javascript'>
		$(function() {
						$( "#tags2" ).autocomplete({
							 source: "<?php echo base_url(); ?>standar7/dosenautocomplete",
							 dataType:'json',
							 type:'POST',
							 minLength: 4

						});
				 });
		</script>
		<script type='text/javascript'>
		$(function() {
						$( "#tags3" ).autocomplete({
							 source: "<?php echo base_url(); ?>standar7/dosenautocomplete",
							 dataType:'json',
							 type:'POST',
							 minLength: 4

						});
				 });
		</script>
		<script type='text/javascript'>
		$(function() {
						$( "#tags4" ).autocomplete({
							 source: "<?php echo base_url(); ?>standar7/dosenautocomplete",
							 dataType:'json',
							 type:'POST',
							 minLength: 4

						});
				 });
		</script>
		<script type='text/javascript'>
		$(function() {
						$( "#tags5" ).autocomplete({
							 source: "<?php echo base_url(); ?>standar7/dosenautocomplete",
							 dataType:'json',
							 type:'POST',
							 minLength: 4

						});
				 });
		</script>
		<script type='text/javascript'>
			$(function() {
	    function split( val ) {
	        return val.split( /,\s*/ );
	    }
	    function extractLast( term ) {
	        return split( term ).pop();
	    }

	    $( "#tags1" ).bind( "keydown", function( event ) {
	        if ( event.keyCode === $.ui.keyCode.TAB &&
	            $( this ).autocomplete( "instance" ).menu.active ) {
	            event.preventDefault();
	        }
	    })
	    .autocomplete({
	        minLength: 1,
	        source: function( request, response ) {
	            // delegate back to autocomplete, but extract the last term
	            $.getJSON("<?php echo base_url(); ?>standar7/dosenautocomplete", { term : extractLast( request.term )},response);
	        },
	        focus: function() {
	            // prevent value inserted on focus
	            return false;
	        },
	        select: function( event, ui ) {
	            var terms = split( this.value );
	            // remove the current input
	            terms.pop();
	            // add the selected item
	            terms.push( ui.item.value );
	            // add placeholder to get the comma-and-space at the end
	            terms.push( "" );
	            this.value = terms.join( ", " );
	            return false;
	        }
	    });
	});
		</script>

	</head>

	<body>
		<div class="header">
			<h1><a href="<?php echo base_url(); ?>dashboard">Akreditasi SI</a></h1>
			<?php
				include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/header.php";
			?>
		</div>

		<div class="sidebar">
			<?php
				include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/sidebar.php";
			?>
		</div>

		<div class="main-layout">
		<?php
			if ( $this->session->userdata('hak_akses') == 'admin' ?: $this->session->userdata('hak_akses') == 'standar 7' ?: $this->session->userdata('hak_akses') == 'kaprodi'){
			?>

			<div class="main-content">
				<div class="menu-breadcrumb">
					<ul class="breadcrumb">
						<li><a href="<?php echo base_url(); ?>dashboard" id="menu-breadcrumb">Dashboard</a></li>
						<li><a href="<?php echo base_url(); ?>standar7/abdimas" id="menu-breadcrumb">Pengmas</a></li>
						<li>Pengabdian Masyarakat Data</li>
					</ul>
				</div>
				<div class="menu-head">
					<ul>
						<li><a href="<?php echo base_url(); ?>standar7/penelitian" id="menu-head" >Penelitian</a></li>
						<li><a href="<?php echo base_url(); ?>standar7/abdimas" id="menu-head" style="background:#d9d9d9;color:black;" >Pengmas</a></li>
						<li><a href="<?php echo base_url(); ?>standar7/kerjasama" id="menu-head" >Kerjasama</a></li>
					</ul>
				</div>

				<div class="menu-subhead">
					<ul>
						<li><a href="<?php echo base_url(); ?>standar7/abdimasdata" id="menu-subhead" style="background:#404040;color:white;">Data Pengmas</a></li>
					</ul>
				</div>
				<div class="sub-header">
				<h2> Standar 7 : Data Pengabdian Masyarakat</h2>
				</div>
					<div class="kerjasama dosen">
						<div class="krjsama-header dosen-header">
							<h2>Pilih Cara : </h2>
						</div>
						<button class="accordion">Import Pengabdian Masyarakat</button>
						<div class="panel">
							<div class="panel-input">
							<br/>
								<h3 class="head-panel">IMPORT DATA PENGABDIAN MASYARAKAT</h3>
							<hr>
							<br>
							<!-- 	<label >Pengupdate Terakhir : <?php

								if(empty($pengupdate->pengupdate)){

										echo "";


								} else {

									echo $pengupdate->pengupdate;
								} ?></label></br> -->
							<label >Download Template Data Pengabdian Masyarakat : </label>
							<label style="color:red;font-size:12px;"><a href="<?php echo base_url(); ?>uploads/format_import_std7/format_data_abdimas.xlsx" target="_blank">Download File</a></label>
							<br>
						  <br>
						   <form action="<?php echo base_url();  ?>Standar7/abdimasimport" onsubmit="return Validate(this);" method="post" enctype="multipart/form-data">
								<p class="huruf-upload-files">Files: <input required type="file" name="files" id="files" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" /></p>
								<label style="color:red;font-size:12px;">*File yang dapat di import csv, xls, dan xlxs</label>
								<input type="hidden" name="kerjasama" value="tb_kerjasama">
								<br>
								<br>
								<hr>
								<button class="upload-btn btn" type="submit" name="btn-upload">Upload</button>
							</form>
							<?php
								if($this->session->flashdata('msg')){
									echo "<script> alert('".$this->session->flashdata('msg')."'); </script>";
								}
							?>
						  <br>
						  <script type="text/javascript" language="javascript">
								var _validFileExtensions = [".xlsx", ".xls", ".csv"];
									function Validate(oForm) {
										var arrInputs = oForm.getElementsByTagName("input");
										for (var i = 0; i < arrInputs.length; i++) {
											var oInput = arrInputs[i];
											if (oInput.type == "file") {
												var sFileName = oInput.value;
												if (sFileName.length > 0) {
													var blnValid = false;
													for (var j = 0; j < _validFileExtensions.length; j++) {
														var sCurExtension = _validFileExtensions[j];
														if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
															blnValid = true;
															break;
														}
													}

													if (!blnValid) {
														alert("Sorry, " + sFileName + " is invalid, allowed extensions are: " + _validFileExtensions.join(", "));
														return false;
													}
												}
											}
										}

										return true;
									}
							</script>
							</div>
						</div>
						<button class="accordion">Input Data Pengabdian Masyarakat</button>
						<div class="panel">

 						   <table class="display" width="100%" cellspacing="0">
 						   <form  class="data-diri-form" name="data-diri-form"  method="post" action ="<?php echo base_url(); ?>Standar7/abdimasinsert" enctype="multipart/form-data">

 						   <tbody>
 						       <tr>
 						     <td colspan="3"><h3 class="head-panel">INPUT DATA PENGABDIAN MASYARAKAT</h3><hr/><br/></td>
 						     </tr>
 						     <tr>
 						      <td><p class="label" >Program studi</p></td>
 						      <td><p> : </p></td>
 						      <td><input class="data-input-kerjasama" name="program_studi"   type="text"   placeholder="Program studi" required ></td>
 						    </tr>
								<tr>
									<td><p class="label">Tahun</p></td>
									<td><p> : </p></td>
									<td><input class="data-input-kerjasama" id="tahun" name="tahun"   type="date" onchange="coba()" placeholder="Tahun" required ></td>
									 </tr>
									 <tr>
										 <td><p class="label">NO TS</p></td>
										 <td><p> : </p></td>
										 <td>
											 <input class="data-input-kerjasama" id="no_ts" name="no_ts" style="text-transform:uppercase;" type="text"  placeholder="NO_TS" readonly required >

										 </td>
									 </tr>
									 <script type="text/javascript">

									 function coba(no){

										 $.ajax({
														 url:"<?php echo base_url();?>/Standar7/number_ts",
														 dataType:'json',
														 type:"POST",
														 success: function(result){
																 var obj = result;
																 console.log(obj);
																// $.each(result.results,function(item){
																//     $('ul').append('<li>' + item + '</li>')
																// })


																	////////////   TS validasi ////////////////////
																	var dateFrom_ts = obj.number_ts[0].date_from;

																	var dateCheck_ts = document.getElementById('tahun').value;

																	var d1_ts = dateFrom_ts.split("-");

																	var c_ts = dateCheck_ts.split("-");

																	var from_ts = new Date(d1_ts[0],d1_ts[1]-1, d1_ts[2]);  // -1 because months are from 0 to 11
																 //  var to_ts   = new Date(d2_ts[0], d2_ts[1]-1, d2_ts[2]);
									 var get_time = new Date(c_ts[0], c_ts[1], c_ts[2]);

																	var check_ts = new Date(d1_ts[0], d1_ts[1], d1_ts[2]);
																		var check_ts1 = new Date(d1_ts[0]-1, d1_ts[1], d1_ts[2]);
																			var check_ts2 = new Date(d1_ts[0]-2, d1_ts[1], d1_ts[2]);
																					var check_ts3 = new Date(d1_ts[0]-3, d1_ts[1], d1_ts[2]);
									 // console.log(to_ts);
									 // console.log(dateCheck_ts);
									 // console.log(dateTo_ts);

									 console.log(get_time);
									 console.log(check_ts);
									 console.log(check_ts1);
									 console.log(check_ts2);

									 if(get_time <= check_ts && get_time >= check_ts1 ){
									 document.getElementById('no_ts').value = "TS";

									 } else if(get_time <= check_ts1 && get_time >= check_ts2 ){
									 document.getElementById('no_ts').value = "TS_1";

									 }  else if(get_time <= check_ts2 && get_time >= check_ts3 ){
									 document.getElementById('no_ts').value = "TS_2";

									 }



														 }
												 });
									 // console.log(to+from+check)
									 }
									 </script>

 						     <tr>
 						      <td><p class="label" id="label1">Tipe</p></td>
 						      <td><p> : </p></td>
 						      <td><textarea class="data-input-kerjasama" name="tipe" style="height:60px;"  type="text"   placeholder="Tipe" required ></textarea></td>
 						    </tr>

 						    <tr>
 						       <td><p class="label" id="label1">Judul</p></td>
 						       <td><p> : </p></td>
 						       <td><textarea class="data-input-kerjasama"  name="judul" style="height:60px;"  type="text"   placeholder="Judul" required ></textarea></td>

 						     </tr>

 						    <tr>
 						         <td><p class="label">Lokasi</p></td>
 						         <td><p> : </p></td>
 						         <td><input class="data-input-kerjasama" name="lokasi"  type="text"  placeholder="Lokasi" required ></td>
 						       </tr>
							 <tr>
 						     <td><p class="label">Mitra</p></td>
 						     <td><p> : </p></td>
 						     <td><input class="data-input-kerjasama" name="mitra"  type="text"  placeholder="Mitra" required ></td>
 						   </tr>

							<tr>
 						     <td><p class="label">Nama Dosen Ketua</p></td>
 						     <td><p> : </p></td>
 						     <td><input style="font-size:80%;" class="data-input-kerjasama ui-widget" id="tags" name="nama_dosen_ketua"  type="text"  placeholder="Nama Dosen Ketua" required ></td>

 						  </tr>
 							<tr>
 						    <td><p class="label" >Nama dosen anggota 1</p></td>
 						    <td><p > : </p></td>
 						    <td><input class="data-input-kerjasama ui-widget" name="nama_dosen_anggota" id="tags1"  type="text"  placeholder="Nama dosen anggota"  ></td>
								<label for="tags1"></label>
						 </tr>
						 <tr>
							 <td><p class="label" >Nama dosen anggota 2</p></td>
							 <td><p > : </p></td>
							 <td><input class="data-input-kerjasama ui-widget" name="nama_dosen_anggota2" id="tags2"  type="text"  placeholder="Nama dosen anggota"  ></td>
							 <label for="tags2"></label>
						</tr>
						<tr>
							<td><p class="label" >Nama dosen anggota 3</p></td>
							<td><p > : </p></td>
							<td><input class="data-input-kerjasama ui-widget" name="nama_dosen_anggota3" id="tags3"  type="text"  placeholder="Nama dosen anggota"  ></td>
							<label for="tags3"></label>
					 </tr>
					 <tr>
						 <td><p class="label" >Nama dosen anggota 4</p></td>
						 <td><p > : </p></td>
						 <td><input class="data-input-kerjasama ui-widget" name="nama_dosen_anggota4" id="tags4"  type="text"  placeholder="Nama dosen anggota"  ></td>
						 <label for="tags4"></label>
					</tr>
					<tr>
						<td><p class="label" >Nama dosen anggota 5</p></td>
						<td><p > : </p></td>
						<td><input class="data-input-kerjasama ui-widget" name="nama_dosen_anggota5" id="tags5"  type="text"  placeholder="Nama dosen anggota"  ></td>
						<label for="tags5"></label>
				 </tr>
						 <tr>
 						    <td><p class="label" id="label1">Nama mahasiswa</p></td>
 						    <td><p id="label1"> : </p></td>
 						    <td><textarea class="data-input-kerjasama" name="nama_mahasiswa" style="height:60px;" type="text"  placeholder="Nama mahasiswa" required ></textarea></td>
 						 </tr>
						 <tr>
 						   <td><p class="label">Anggaran</p></td>
 						   <td><p> : </p></td>
 						   <td><input id="anggaran" class="data-input-kerjasama" name="anggaran"  type="number"  min="0" required ></td>
							 <script>
							 $('#anggaran').on('change keyup', function() {
								var sanitized = $(this).val().replace(/[^0-9]/g, '');
								$(this).val(sanitized);
								});
							 </script>
 						 </tr>
 					 	 <tr>
 						   <td><p class="label">Bukti laporan akhir</p></td>
 						   <td><p> : </p></td>
 						   <td><input class="data-input-kerjasama" name="bukti_laporan_akhir"  type="text"  placeholder="Bukti laporan akhir" required ></td>
 						 </tr>
						 <tr>
 						     <td><p class="label">Keterangan</p></td>
 						     <td><p> : </p></td>
 						     <td><input class="data-input-kerjasama" name="keterangan"  type="text"  placeholder="Keterangan" required ></td>
 						  </tr>

 						     <tr>
 						      <td><p class="label">Sumber pembiayaan</p></td>
 						      <td><p> : </p></td>
 						      <td><select name="sumber_pembiayaan"  class="data-input-kerjasama">
 						          <option value="Pembiayaan sendiri oleh peneliti">Pembiayaan sendiri oleh peneliti</option>
 						          <option value="PT yang bersangkutan">PT yang bersangkutan</option>
 						          <option value="Depdiknas">Depdiknas</option>]
 						           <option value="Institusi dalam negeri di luar Depdiknas">Institusi dalam negeri di luar Depdiknas</option>
 						           <option value="Institusi luar negeri">Institusi luar negeri</option>
 						     </select>
 						     </td>
 						     </tr>


 						     <tr>
 						     <td colspan="3"><br/><hr/><input class="data-diri-button btn" id="data-diri-button" name="simpanbtn" title="simpan" type="submit" value="Simpan" ><br/></td>
 						     </tr>
 						   </tbody>

 						   </form>
 						 </table>
						</div>
						<button class="accordion">Input Deskripsi Mahasiswa yang terlibat dalam pengabdian masyarakat </button>
						<div class="panel" >
							</br>
							<label> Deskripsi Keterlibatan Mahasiswa dalam Pengabdian Masyarakat
								</br>
								</br>
							<form  method="post" action ="<?php echo base_url(); ?>Standar7/tb_7_2_2_abdimas_insert">
								<textarea  name = "data" class="ckeditor" id="ckedtor" > <?php echo	 $view722->text; ?></textarea>
								<button style="display: block;" class="save btn" name="btn-upload" type="submit"  onclick="" id="save_btn" value="save">Simpan</button>
								</form>
							</br>
						</div>
					</div>
					<script>
						var acc = document.getElementsByClassName("accordion");
						var i;

						for (i = 0; i < acc.length; i++) {
						  acc[i].onclick = function() {
							this.classList.toggle("active");
							var panel = this.nextElementSibling;
							if (panel.style.maxHeight){
							  panel.style.maxHeight = null;
							} else {
							  panel.style.maxHeight = panel.scrollHeight + "px";
							}
						  }
						}
					</script>
			</div>
			<div class="data-kerjasama">
			<table id="table-view-data-kerjasama" class="display" width="100%" cellspacing="0">

						<thead class="table head-peldosen">
							<th>#</th>
							<th>No</th>
							<th >Program studi</th>
							<th >no ts  </th>
							<th >type</th>
							<th>judul</th>
							<th>lokasi</th>
							<th>mitra</th>
							<th>tahun</th>
							<th>nama dosen ketua</th>
							<th>nama dosen anggota 1</th>
							<th>nama dosen anggota 2</th>
							<th>nama dosen anggota 3</th>
							<th>nama dosen anggota 4</th>
							<th>nama dosen anggota 5</th>
							<th>nama mahasiswa</th>
							<th >anggaran</th>
							<th>Bukti laporan akhir</th>
							<th>keterangan</th>
							<th>Sumber Pembiayaan</th>
							<th>DocPendukung</th>
							<th>Status Valid</th>
							<th>Upload</th>
							<th><i class="fa fa-pencil-square-o" aria-hidden="true"></th>
								<?php if($this->session->userdata('hak_akses') == 'admin' ?: $this->session->userdata('hak_akses') == 'kaprodi') { ?>
										<th >Validasi </th>
								<?php } ?>
							<th><i class="fa fa-trash-o" aria-hidden="true"></i></th>
						</thead>

						<tbody class="table body-peldosen" role="alert" aria-live="polite" aria-relevant="all">

							<?php
									$no = 1;
									foreach($result as $row){

									?>
										<tr id="row:<?php echo $row->no ?>">
										<td ><input type="checkbox" name="pilih[]" class="check" value="<?php echo $row->no ?>" ></td>
											<td ><?php echo $no++ ?></td>
											<td ><textarea class="data-diri-input" title="<?php echo $row->program_studi ?>" id="abdimas_programstudi<?php echo $row->no ?>"  type="text" name="program_studi" placeholder="" readonly /><?php echo $row->program_studi ?></textarea></td>
											<td><textarea class="data-diri-input" title="<?php echo $row->no_ts ?>" id="abdimas_no_ts<?php echo $row->no ?>"  type="text" name="no_ts" placeholder="" readonly /><?php echo $row->no_ts ?></textarea></td>
											<td><textarea class="data-diri-input" title="<?php echo $row->type ?>" id="abdimas_type<?php echo $row->no ?>"  type="text" name="type" placeholder="" readonly /><?php echo $row->type ?></textarea></td>
											<td><textarea class="data-diri-input" title="<?php echo $row->judul ?>" id="abdimas_judul<?php echo $row->no ?>"  type="text" name="judul" placeholder="" readonly /><?php echo $row->judul ?></textarea></td>
											<td><textarea class="data-diri-input" title="<?php echo $row->lokasi ?>" id="abdimas_lokasi<?php echo $row->no ?>"  type="text" name="lokasi" placeholder="" readonly /><?php echo $row->lokasi ?></textarea></td>
											<td ><textarea class="data-diri-input" title="<?php echo $row->mitra ?>" id="abdimas_mitra<?php echo $row->no ?>"  type="text" name="mitra" placeholder="" readonly /><?php echo $row->mitra ?></textarea></td>
											<td><textarea class="data-diri-input" title="<?php echo $row->tahun ?>" id="abdimas_tahun<?php echo $row->no ?>"  type="text" name="tahun" placeholder="" readonly /><?php echo $row->tahun ?></textarea></td>
											<td><textarea class="data-diri-input" title="<?php echo $row->nama_dosen_ketua ?>" id="abdimas_nama_dosen_ketua<?php echo $row->no ?>"  type="text" name="nama_dosen_ketua" placeholder="" readonly /><?php echo $row->nama_dosen_ketua ?></textarea></td>
											<td ><textarea class="data-diri-input" title="<?php echo $row->nama_dosen_anggota ?>" id="abdimas_nama_dosen_anggota<?php echo $row->no ?>"  type="text" name="nama_dosen_anggota" placeholder="" readonly /><?php echo $row->nama_dosen_anggota ?></textarea></td>
											<td ><textarea class="data-diri-input" title="<?php echo $row->nama_anggotadosen2 ?>" id="abdimas_nama_dosen_anggota2<?php echo $row->no ?>"  type="text" name="nama_dosen_anggota2" placeholder="" readonly /><?php echo $row->nama_anggotadosen2 ?></textarea></td>
											<td ><textarea class="data-diri-input" title="<?php echo $row->nama_anggotadosen3 ?>" id="abdimas_nama_dosen_anggota3<?php echo $row->no ?>"  type="text" name="nama_dosen_anggota3" placeholder="" readonly /><?php echo $row->nama_anggotadosen3 ?></textarea></td>
											<td ><textarea class="data-diri-input" title="<?php echo $row->nama_anggotadosen4 ?>" id="abdimas_nama_dosen_anggota4<?php echo $row->no ?>"  type="text" name="nama_dosen_anggota4" placeholder="" readonly /><?php echo $row->nama_anggotadosen4 ?></textarea></td>
											<td ><textarea class="data-diri-input" title="<?php echo $row->nama_anggotadosen5 ?>" id="abdimas_nama_dosen_anggota5<?php echo $row->no ?>"  type="text" name="nama_dosen_anggota5" placeholder="" readonly /><?php echo $row->nama_anggotadosen5 ?></textarea></td>
											<td><textarea class="data-diri-input" title="<?php echo $row->nama_mahasiswa ?>" id="abdimas_nama_mahasiswa<?php echo $row->no ?>"  type="text" name="nama_mahasiswa" placeholder="" readonly /><?php echo $row->nama_mahasiswa ?></textarea></td>
											<td><span style="font-size:80%;float:left;">Rp. </span><textarea  style="width:70%;"  class="data-diri-input" title="Rp. <?php echo $row->anggaran ?>" id="abdimas_anggaran<?php echo $row->no ?>"  type="text" name="anggaran" placeholder="" readonly /><?php echo $row->anggaran ?></textarea></td>
											<td ><textarea class="data-diri-input" title="<?php echo $row->bukti_laporan_akhir ?>" id="abdimas_bukti_laporan_akhir<?php echo $row->no ?>"  type="text" name="bukti_laporan_akhir" placeholder="" readonly /><?php echo $row->bukti_laporan_akhir ?></textarea></td>
											<td><textarea class="data-diri-input" title="<?php echo $row->keterangan ?>" id="abdimas_keterangan<?php echo $row->no ?>"  type="text" name="keterangan" placeholder="" readonly /><?php echo $row->keterangan ?></textarea></td>
											<td><textarea class="data-diri-input" title="<?php echo $row->sumber_pembiayaan ?>" id="abdimas_sumber_pembiayaan<?php echo $row->no ?>"  type="text" name="sumber_pembiayaan" placeholder="" readonly /><?php echo $row->sumber_pembiayaan ?></textarea></td>
												<td ><div class="lots">
																<p><a target="_blank" title="Uploaded : [<?php echo $row->file_date ?>], <?php echo $row->file_name ?> " href="<?php echo base_url(); ?>uploads/dokumen_abdimas_std7/<?php echo $row->file_name?>"><?php echo $row->file_name?> </a>...</p>

											</div></td>
											<td >
												<?php
												if ($row->sts_valid == 'valid') {
													?>
														<label>Y</label>
													<?php
												} else {
													?>

													<label id="n:<?php echo $row->no ?>">N</label>
													<?php
												}?>
											</td>
											<td >

												<form onsubmit="return Validate1(this);" action="<?php echo base_url(); ?>Standar7/abdimasuploadpdf/<?php echo $row->no ?>" method="post" enctype="multipart/form-data">
													<input class="aa" style="font-size:9px;display: none;" id="file<?php echo $row->no ?>" required type="file" name="files" onchange="javascript:updateList('<?php echo $row->no?>')" accept="application/pdf" />
													<input type="hidden" value="<?php echo $row->no ?>" name="data-id" >
													<input type="hidden" value="<?php echo $row->file_name?>" name="data-filename" >
													<div class="lots" id="nama-file<?php echo $row->no ?>"></div>
													<br>
													<button class="tombol-kelola" title="Pilih Doc Pendukung : <?php echo $row->judul ?>, <?php echo $row->mitra ?>" style="display: block;" type="button" onclick="edit_row('<?php echo $row->no ?>')" id="edit_btn<?php echo $row->no ?>" class="edit" value="edit"><i class="fa fa-file" aria-hidden="true"></i></button>
													<br>
													<button class="tombol-kelola" title="Upload : <?php echo $row->judul ?>, <?php echo $row->mitra ?>" style="display: none;" name="btn-upload" type="submit"  onclick="save_row('<?php echo $row->no ?>')" id="save_btn<?php echo $row->no ?>" class="save" value="save"><i class="fa fa-floppy-o" aria-hidden="true"></i></button>
													</form>
													<script type="text/javascript" language="javascript">
														var _validFileExtensions1 = [".pdf"];
															function Validate1(oForm) {
																var arrInputs = oForm.getElementsByTagName("input");
																for (var i = 0; i < arrInputs.length; i++) {
																	var oInput = arrInputs[i];
																	if (oInput.type == "file") {
																		var sFileName = oInput.value;
																		if (sFileName.length > 0) {
																			var blnValid = false;
																			for (var j = 0; j < _validFileExtensions1.length; j++) {
																				var sCurExtension = _validFileExtensions1[j];
																				if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
																					blnValid = true;
																					break;
																				}
																			}

																			if (!blnValid) {
																				alert("Sorry, " + sFileName + " is invalid, allowed extensions are: " + _validFileExtensions1.join(", "));
																				return false;
																			}
																		}
																	}
																}

																return true;
															}
													</script>
												</td>
												<td>
												<button class="tombol-kelola" title="Ubah : <?php echo $row->judul ?>, <?php echo $row->mitra ?>" onclick="edit_row1('<?php echo $row->no ?>')"  id="edit<?php echo $row->no ?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
												<button class="tombol-kelola" title="Simpan : <?php echo $row->judul ?>, <?php echo $row->mitra ?>" onclick="save_row1('<?php echo $row->no?>')"  id="save<?php echo $row->no ?>" style="display:none;" /><i class="fa fa-floppy-o" aria-hidden="true"></i></button>
												</td>
												<?php if($this->session->userdata('hak_akses') == 'admin' ?: $this->session->userdata('hak_akses') == 'kaprodi') { ?>
														<td >
															<input type="hidden" id="idnum<?php echo $row->no ?>" value="<?php echo $row->no?>" name="data-id" >
															<input type="hidden" id="stsvalid<?php echo $row->no ?>" value="valid" name="data-id" >
															<button name="validasi" onclick="validasi(<?php echo $row->no ?> )" <?php if ($row->sts_valid == 'valid'){ ?> disabled <?php   } ?> title="Validasi : <?php echo $row->judul ?>" id="validasi<?php echo $row->no ?>" type="submit"><i class="fa fa-check-square" aria-hidden="true"></i></button>
															<script>
																function validasi(no){
																	var id=  document.getElementById("idnum"+no).value;
																		var validasi=  document.getElementById("stsvalid"+no).value;

																		$.ajax({

																				type: "POST",
																				url: "<?php echo base_url(); ?>Standar7/abdimas_valid",
																				data: {id:id, validasi:validasi
																			 },

																				success: function(data){
																				console.log(data);

																					},

																				error: function(xhr, status, errorThrown){
																				alert( xhr.status);
																				alert( xhr.errorThrown);
																				alert( xhr.responseText);
																				console.log(xhr.responseText);
																				}
																		});
																		document.getElementById('validasi'+no).disabled = true;
																		document.getElementById('n:'+no).innerHTML = 'Y';
																}
															</script>
															</td>
												<?php } ?>

												<td>
												<form action="<?php echo base_url(); ?>standar7/abdimashapus/<?php echo $row->no ?>" method="post" enctype="multipart/form-data">
												<input type="hidden" value="<?php echo $row->no?>" name="data-id" >
												<input type="hidden" value="<?php echo $row->file_name?>" name="data-filename" >

												<button class="tombol-kelola" title="Hapus : <?php echo $row->judul ?>, <?php echo $row->mitra ?>" name="btn-delete" onclick="return ConfirmDelete(<?php echo $row->no ?>)"  id="delete-btn<?php echo $row->no ?>" type="submit"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
												</form>
													<script type="text/javascript">
															var baseUrl='<?php echo base_url(); ?>standar7/kerjasamahapus/<?php echo $row->no ?>';
															var msgg = 'Apakah anda yakin untuk menghapus data ini ?';
															function ConfirmDelete(no)
															{
															if (confirm(msgg)) {
																location.href=baseUrl;
															}
															else {
																 return false;
															}
															}
													</script>
												</td>


															<script>
											var no;
												function edit_row1(no)
												{
														document.getElementById('edit'+no).style.display = 'none';
														document.getElementById('save'+no).style.display = 'block';
														document.getElementById("abdimas_type"+no).removeAttribute('readonly');
													 document.getElementById("abdimas_judul"+no).removeAttribute('readonly');
													 document.getElementById("abdimas_programstudi"+no).removeAttribute('readonly');
													document.getElementById("abdimas_no_ts"+no).removeAttribute('readonly');
													 document.getElementById("abdimas_lokasi"+no).removeAttribute('readonly');
														document.getElementById("abdimas_mitra"+no).removeAttribute('readonly');
														document.getElementById("abdimas_tahun"+no).removeAttribute('readonly');
														 document.getElementById("abdimas_nama_dosen_ketua"+no).removeAttribute('readonly');
														document.getElementById("abdimas_nama_dosen_anggota"+no).removeAttribute('readonly');
														document.getElementById("abdimas_nama_dosen_anggota2"+no).removeAttribute('readonly');
														document.getElementById("abdimas_nama_dosen_anggota3"+no).removeAttribute('readonly');
														document.getElementById("abdimas_nama_dosen_anggota4"+no).removeAttribute('readonly');
														document.getElementById("abdimas_nama_dosen_anggota5"+no).removeAttribute('readonly');
														document.getElementById("abdimas_nama_mahasiswa"+no).removeAttribute('readonly');
														document.getElementById("abdimas_anggaran"+no).removeAttribute('readonly');
														 document.getElementById("abdimas_bukti_laporan_akhir"+no).removeAttribute('readonly');
															document.getElementById("abdimas_keterangan"+no).removeAttribute('readonly');


																document.getElementById("abdimas_programstudi"+no).style.background = "#ccffff";
																document.getElementById("abdimas_no_ts"+no).style.background = "#ccffff";
															document.getElementById("abdimas_type"+no).style.background = "#ccffff";
														 document.getElementById("abdimas_judul"+no).style.background = "#ccffff";
														 document.getElementById("abdimas_lokasi"+no).style.background = "#ccffff";
															document.getElementById("abdimas_mitra"+no).style.background = "#ccffff";
															document.getElementById("abdimas_tahun"+no).style.background = "#ccffff";
															 document.getElementById("abdimas_nama_dosen_ketua"+no).style.background = "#ccffff";
															document.getElementById("abdimas_nama_dosen_anggota"+no).style.background = "#ccffff";
															document.getElementById("abdimas_nama_dosen_anggota2"+no).style.background = "#ccffff";
															document.getElementById("abdimas_nama_dosen_anggota3"+no).style.background = "#ccffff";
															document.getElementById("abdimas_nama_dosen_anggota4"+no).style.background = "#ccffff";
															document.getElementById("abdimas_nama_dosen_anggota5"+no).style.background = "#ccffff";
															document.getElementById("abdimas_nama_mahasiswa"+no).style.background = "#ccffff";
																			document.getElementById("abdimas_anggaran"+no).style.background = "#ccffff";
															 document.getElementById("abdimas_bukti_laporan_akhir"+no).style.background = "#ccffff";
																document.getElementById("abdimas_keterangan"+no).style.background = "#ccffff";

												}



											</script>
											<script>

												function save_row1(no){
													var abdimas_typel=  document.getElementById("abdimas_type"+no).value;
													var abdimas_judull=  document.getElementById("abdimas_judul"+no).value;
													var abdimas_lokasi= document.getElementById("abdimas_lokasi"+no).value;
													var abdimas_mitra=  document.getElementById("abdimas_mitra"+no).value;
													var abdimas_tahun= document.getElementById("abdimas_tahun"+no).value;
													var abdimas_nama_dosen_ketua= document.getElementById("abdimas_nama_dosen_ketua"+no).value;
													var abdimas_nama_dosen_anggota= document.getElementById("abdimas_nama_dosen_anggota"+no).value;
													var abdimas_nama_dosen_anggota2= document.getElementById("abdimas_nama_dosen_anggota2"+no).value;
													var abdimas_nama_dosen_anggota3= document.getElementById("abdimas_nama_dosen_anggota3"+no).value;
													var abdimas_nama_dosen_anggota4= document.getElementById("abdimas_nama_dosen_anggota4"+no).value;
													var abdimas_nama_dosen_anggota5= document.getElementById("abdimas_nama_dosen_anggota5"+no).value;
													var abdimas_nama_mahasiswa= document.getElementById("abdimas_nama_mahasiswa"+no).value;
													var abdimas_bukti_laporan_akhir= document.getElementById("abdimas_bukti_laporan_akhir"+no).value;
													var abdimas_keterangan= document.getElementById("abdimas_keterangan"+no).value
													var abdimas_anggaran= document.getElementById("abdimas_anggaran"+no).value
													var no = no;


																$.ajax({

																		type: "POST",
																		url: "<?php echo base_url(); ?>Standar7/abdimasupdate",
																		data: {no:no, abdimas_typel:abdimas_typel, abdimas_judull:abdimas_judull, abdimas_lokasi:abdimas_lokasi,
																			abdimas_mitra:abdimas_mitra, abdimas_tahun:abdimas_tahun, abdimas_nama_dosen_ketua:abdimas_nama_dosen_ketua,
																			  abdimas_nama_dosen_anggota:abdimas_nama_dosen_anggota,
																				 abdimas_nama_dosen_anggota2:abdimas_nama_dosen_anggota2,
																				  abdimas_nama_dosen_anggota3:abdimas_nama_dosen_anggota3,
																					 abdimas_nama_dosen_anggota4:abdimas_nama_dosen_anggota4,
																					  abdimas_nama_dosen_anggota5:abdimas_nama_dosen_anggota5,
																				abdimas_nama_mahasiswa:abdimas_nama_mahasiswa,
																				 abdimas_bukti_laporan_akhir:abdimas_bukti_laporan_akhir, abdimas_keterangan:abdimas_keterangan, abdimas_anggaran:abdimas_anggaran },

																		success: function(data){
																		console.log(data);

																			},

																		error: function(xhr, status, errorThrown){
																		alert( xhr.status);
																		alert( xhr.errorThrown);
																		alert( xhr.responseText);
																		console.log(xhr.responseText);
																		}
																});

																						document.getElementById('save'+no).style.display = 'none';
																						document.getElementById('edit'+no).style.display = 'block';
																						document.getElementById("abdimas_programstudi"+no).readOnly =true;
																					document.getElementById("abdimas_no_ts"+no).readOnly =true;
																						document.getElementById("abdimas_type"+no).readOnly =true;
																					document.getElementById("abdimas_judul"+no).readOnly =true;
																					document.getElementById("abdimas_lokasi"+no).readOnly =true;
																					 document.getElementById("abdimas_mitra"+no).readOnly =true;
																					 document.getElementById("abdimas_tahun"+no).readOnly =true;
																						document.getElementById("abdimas_nama_dosen_ketua"+no).readOnly =true;
																					 document.getElementById("abdimas_nama_dosen_anggota"+no).readOnly =true;
																					 document.getElementById("abdimas_nama_dosen_anggota2"+no).readOnly =true;
																					 document.getElementById("abdimas_nama_dosen_anggota3"+no).readOnly =true;
																					 document.getElementById("abdimas_nama_dosen_anggota4"+no).readOnly =true;
																					 document.getElementById("abdimas_nama_dosen_anggota5"+no).readOnly =true;
																					 document.getElementById("abdimas_nama_mahasiswa"+no).readOnly =true;
																					 document.getElementById("abdimas_anggaran"+no).readOnly =true;
																						document.getElementById("abdimas_bukti_laporan_akhir"+no).readOnly =true;
																						 document.getElementById("abdimas_keterangan"+no).readOnly =true;


																						document.getElementById("abdimas_programstudi"+no).style.background = "#f3f3f3";
																					document.getElementById("abdimas_no_ts"+no).style.background = "#f3f3f3";
 																						document.getElementById("abdimas_type"+no).style.background = "#f3f3f3";
 																					document.getElementById("abdimas_judul"+no).style.background = "#f3f3f3";
 																					document.getElementById("abdimas_lokasi"+no).style.background = "#f3f3f3";
 																					 document.getElementById("abdimas_mitra"+no).style.background = "#f3f3f3";
 																					 document.getElementById("abdimas_tahun"+no).style.background = "#f3f3f3";
 																						document.getElementById("abdimas_nama_dosen_ketua"+no).style.background = "#f3f3f3";
 																					 document.getElementById("abdimas_nama_dosen_anggota"+no).style.background = "#f3f3f3";
																					 document.getElementById("abdimas_nama_dosen_anggota2"+no).style.background = "#f3f3f3";
																					 document.getElementById("abdimas_nama_dosen_anggota3"+no).style.background = "#f3f3f3";
																					 document.getElementById("abdimas_nama_dosen_anggota4"+no).style.background = "#f3f3f3";
																					 document.getElementById("abdimas_nama_dosen_anggota5"+no).style.background = "#f3f3f3";
 																					 document.getElementById("abdimas_nama_mahasiswa"+no).style.background = "#f3f3f3";
																					 document.getElementById("abdimas_anggaran"+no).style.background = "#f3f3f3";
 																						document.getElementById("abdimas_bukti_laporan_akhir"+no).style.background = "#f3f3f3";
 																						 document.getElementById("abdimas_keterangan"+no).style.background = "#f3f3f3";

																					}
											</script>


												<script>
													function edit_row(no) {
														document.getElementById('file'+no).click();
														document.getElementById("save_btn"+no).style.display="block";
													}

													function save_row(no) {
														document.getElementById("edit_btn"+no).style.display="block";
														document.getElementById("save_btn"+no).style.display="none";
													}

													updateList = function(no) {
														var input = document.getElementById('file'+no);
														var output = document.getElementById('nama-file'+no);

														output.innerHTML = '<label>';
														for (var i = 0; i < input.files.length; ++i) {
														output.innerHTML += input.files.item(i).name ;
														}
														output.innerHTML += '</label>';
											}
												</script>
										</tr>
								<?php
									}
								?>
						</tbody>
						<tfoot class="table head-peldosen">
							<th><i class="fa fa-check" aria-hidden="true"></i></th>
							<th>No</th>
							<th >Program studi</th>
							<th >no ts  </th>
							<th >type</th>
							<th>judul</th>
							<th>lokasi</th>
							<th>mitra</th>
							<th>tahun</th>
							<th>nama dosen ketua</th>
							<th>nama dosen anggota 1</th>
							<th>nama dosen anggota 2</th>
							<th>nama dosen anggota 3</th>
							<th>nama dosen anggota 4</th>
							<th>nama dosen anggota 5</th>
							<th>nama mahasiswa</th>
							<th>anggaran</th>
							<th>Bukti laporan akhir</th>
							<th>keterangan</th>
							<th>Sumber Pembiayaan</th>
							<th>DocPendukung</th>
							<th>Status Valid</th>
								<th>	Upload</th>
									<th><i class="fa fa-pencil-square-o" aria-hidden="true"></th>
										<?php if($this->session->userdata('hak_akses') == 'admin' ?: $this->session->userdata('hak_akses') == 'kaprodi') { ?>
												<th >Validasi </th>
										<?php } ?>
							<th><i class="fa fa-trash-o" aria-hidden="true"></i></th>
						</tfoot>

				</table>
			</div>
			<div class="delete all-data">
				<button class="btn-delete-list" name="btn-delete-check" type="button" onclick="hapus_checklist()" >Hapus Terpilih</button>
				<?php
					if($this->session->flashdata('msgg')){
						echo "<script> alert('".$this->session->flashdata('msgg')."'); </script>";
					}
				?>
				<script type="text/javascript" language="javascript">
					function hapus_checklist(){

						var selected_value = new Array(); // initialize empty array
						var confirmation = confirm("Apakah anda yakin menghapus data yang anda pilih ?");
						 if(confirmation){
							$(".check:checked").each(function(){
									selected_value.push($(this).val());
								});
								console.log(selected_value);
							$.ajax({
									type: "POST",
									url: "<?php echo base_url(); ?>Standar7/abdimashapuschecklist",
									data: {result:JSON.stringify(selected_value)},

									success: function(data){
									console.log(data);
									window.location.href='<?php echo base_url(); ?>Standar7/abdimasdata?success';
									},

									error: function(xhr, status, errorThrown){
									alert( xhr.status);
									alert( xhr.errorThrown);
									alert( xhr.responseText);
									console.log(xhr.responseText);
									}
							});
						 }
					}
				</script>

				<button class="btn-delete-all" name="btn-delete-all" onclick="ConfirmDeleteall()"  id="delete-btn" type="submit">Hapus Semua Data</i></button>

				<script type="text/javascript">
						var baseUrl='<?php echo base_url(); ?>standar7/abdimashapussemua';
						function ConfirmDeleteall()
							{
									var msg = 'Apakah anda yakin menghapus semua data Pengabdian Masyarakat ?';
									if (confirm(msg)){
									   location.href=baseUrl;
									}
							}
				</script>

			</div>
			<br>



<!-- ----------------------------------SELAIN STANDAR 7--------------------------------------- -->





		<?php
			} else {
		?>


					<div class="main-content">
						<div class="menu-breadcrumb">
							<ul class="breadcrumb">
								<li><a href="<?php echo base_url(); ?>dashboard" id="menu-breadcrumb">Dashboard</a></li>
								<li><a href="<?php echo base_url(); ?>standar7/kerjasama" id="menu-breadcrumb">Kerjasama</a></li>
								<li>Kerjasama Data</li>
							</ul>
						</div>
						<div class="menu-head">
							<ul>
								<li><a href="<?php echo base_url(); ?>standar7/penelitian" id="menu-head" >Penelitian</a></li>
								<li><a href="<?php echo base_url(); ?>standar7/abdimas" id="menu-head" style="background:#d9d9d9;color:black;" >Abdimas</a></li>
								<li><a href="<?php echo base_url(); ?>standar7/kerjasama" id="menu-head" >Kerjasama</a></li>
							</ul>
						</div>

						<div class="menu-subhead">
							<ul>
								<li><a href="<?php echo base_url(); ?>standar7/abdimasdata" id="menu-subhead" style="background:#404040;color:white;">Data Abdimas</a></li>
							</ul>
						</div>
						<div class="sub-header">
						<h2> Standar 7 : Data Kerjasama</h2>
						</div>
							<div class="kerjasama dosen">
								<div class="krjsama-header dosen-header">
									<h2>Pilih Cara : </h2>
								</div>
								<button class="accordion">Import Data Kerjasama</button>
								<div class="panel">
									</br>
										<label style="font-size:80%; color:red;">Maaf tidak bisa melakukan import data, karena bukan Anggota tim pengumpul data standar 7</label>
									</br>
									</br>
								</div>
								<button class="accordion">Input Data Kerjasama</button>
								<div class="panel">
								</br>
									<label style="font-size:80%; color:red;">Maaf tidak bisa melakukan import data, karena bukan Anggota tim pengumpul data standar 7</label>
								</br>
								</br>
								</div>
								<button class="accordion">Input Mahasiswa yang terlibat dalam pengabdian masyarakat </button>
								<div class="panel">
								</br>
									<label style="font-size:80%; color:red;">Maaf tidak bisa melakukan import data, karena bukan Anggota tim pengumpul data standar 7</label>
								</br>
								</br>
								</div>
							</div>
							<script>
								var acc = document.getElementsByClassName("accordion");
								var i;

								for (i = 0; i < acc.length; i++) {
									acc[i].onclick = function() {
									this.classList.toggle("active");
									var panel = this.nextElementSibling;
									if (panel.style.maxHeight){
										panel.style.maxHeight = null;
									} else {
										panel.style.maxHeight = panel.scrollHeight + "px";
									}
									}
								}
							</script>
					</div>
					<div class="data-kerjasama">
					<table id="table-view-data-kerjasama" class="display" width="100%" cellspacing="0">

								<thead class="table head-peldosen">
									<th>No</th>
									<th >Program studi</th>
									<th >no ts</th>
									<th >type</th>
									<th>judul</th>
									<th>lokasi</th>
									<th>mitra</th>
									<th>tahun</th>
									<th>nama dosen ketua</th>
									<th>nama dosen anggota</th>
									<th>nama mahasiswa</th>
									<th>anggaran</th>
									<th>bukti_laporan_akhir</th>
									<th>keterangan</th>
									<th>sumber_pembiayaan</th>
									<th>DocPendukung</th>
								</thead>

								<tbody class="table body-peldosen" role="alert" aria-live="polite" aria-relevant="all">

									<?php
											$no = 1;
											foreach($result as $row){

											?>
												<tr id="row:<?php echo $row->no ?>">
													<td ><?php echo $no++ ?></td>
													<td ><textarea class="data-diri-input" id="abdimas_programstudi<?php echo $row->no ?>"  type="text" name="program_studi" placeholder="" readonly /><?php echo $row->program_studi ?></textarea></td>
													<td><textarea class="data-diri-input" id="abdimas_no_ts<?php echo $row->no ?>"  type="text" name="no_ts" placeholder="" readonly /><?php echo $row->no_ts ?></textarea></td>
													<td><textarea class="data-diri-input" id="abdimas_type<?php echo $row->no ?>"  type="text" name="type" placeholder="" readonly /><?php echo $row->type ?></textarea></td>
													<td><textarea class="data-diri-input" id="abdimas_judul<?php echo $row->no ?>"  type="text" name="judul" placeholder="" readonly /><?php echo $row->judul ?></textarea></td>
													<td><textarea class="data-diri-input" id="abdimas_lokasi<?php echo $row->no ?>"  type="text" name="lokasi" placeholder="" readonly /><?php echo $row->lokasi ?></textarea></td>
													<td ><textarea class="data-diri-input" id="abdimas_mitra<?php echo $row->no ?>"  type="text" name="mitra" placeholder="" readonly /><?php echo $row->mitra ?></textarea></td>
													<td><textarea class="data-diri-input" id="abdimas_tahun<?php echo $row->no ?>"  type="text" name="tahun" placeholder="" readonly /><?php echo $row->tahun ?></textarea></td>
													<td><textarea class="data-diri-input" id="abdimas_nama_dosen_ketua<?php echo $row->no ?>"  type="text" name="nama_dosen_ketua" placeholder="" readonly /><?php echo $row->nama_dosen_ketua ?></textarea></td>
													<td ><textarea class="data-diri-input" id="abdimas_nama_dosen_anggota<?php echo $row->no ?>"  type="text" name="nama_dosen_anggota" placeholder="" readonly /><?php echo $row->nama_dosen_anggota ?></textarea></td>
													<td><textarea class="data-diri-input" id="abdimas_nama_mahasiswa<?php echo $row->no ?>"  type="text" name="nama_mahasiswa" placeholder="" readonly /><?php echo $row->nama_mahasiswa ?></textarea></td>
													<td><textarea class="data-diri-input" id="abdimas_anggaran<?php echo $row->no ?>"  type="text" name="anggaran" placeholder="" readonly /><?php echo $row->anggaran ?></textarea></td>
													<td ><textarea class="data-diri-input" id="abdimas_bukti_laporan_akhir<?php echo $row->no ?>"  type="text" name="bukti_laporan_akhir" placeholder="" readonly /><?php echo $row->bukti_laporan_akhir ?></textarea></td>
													<td><textarea class="data-diri-input" id="abdimas_keterangan<?php echo $row->no ?>"  type="text" name="keterangan" placeholder="" readonly /><?php echo $row->keterangan ?></textarea></td>
													<td><textarea class="data-diri-input" id="abdimas_sumber_pembiayaan<?php echo $row->no ?>"  type="text" name="sumber_pembiayaan" placeholder="" readonly /><?php echo $row->sumber_pembiayaan ?></textarea></td>
														<td ><div class="lots">
																		<p><a target="_blank" href="<?php echo base_url(); ?>uploads/dokumen_kerjasama_std7/<?php echo $row->file_name?>"><?php echo $row->file_name?> </a></p>

													</div></td>

												</tr>
										<?php
											}
										?>
								</tbody>
								<tfoot class="table head-peldosen">
									<th>No</th>
									<th >Program studi</th>
									<th >no ts  </th>
									<th >type</th>
									<th>judul</th>
									<th>lokasi</th>
									<th>mitra</th>
									<th>tahun</th>
									<th>nama dosen ketua</th>
									<th>nama dosen anggota</th>
									<th>nama mahasiswa</th>
									<th>anggaran</th>
									<th>bukti_laporan_akhir</th>
									<th>keterangan</th>
									<th>sumber_pembiayaan</th>
									<th>DocPendukung</th>
								</tfoot>

						</table>
					</div>
					<br>
		<?php
			}
		?>

		</div>

		<div class="footer">
			<?php
			include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/footer.php";
			?>
		</div>

	</body>



</html>
