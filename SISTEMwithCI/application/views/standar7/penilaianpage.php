<?php
if(!isset($_SESSION['nama'])){
      header("location:" . base_url());
      exit();
   }
?>

<!DOCTYPE html>
<html>
	<head>
		<title>Akreditasi | Penilaian</title>
		<script type = 'text/javascript' src="<?php echo base_url(); ?>js/jquery-1.12.4.js"></script>
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/mainlayout.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/penelitian.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/dosen.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/accordion.css">
		<script type = 'text/javascript' src="<?php echo base_url(); ?>js/jquery.dataTables.js"></script>
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/jquery.dataTables.css">
		<script type = 'text/javascript' >
			(function ($){
				$(document).ready(function() {
					$('#view-nilai').dataTable({
						"order": [[ 0, 'asc' ]],

					});
				});
			})(jQuery);
		</script>
		<script type = 'text/javascript' >
			(function ($){
				$(document).ready(function() {
					$('#view-record-penilai').dataTable({

					});
				});
			})(jQuery);
		</script>
	</head>

	<body>

		<div class="header">
			<h1><a href="<?php echo base_url(); ?>dashboard">Akreditasi SI</a></h1>
			<?php
				include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/header.php";
			?>
		</div>

		<div class="sidebar">
			<?php
				include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/sidebar.php";
			?>
		</div>

		<div class="main-layout">

			<div class="main-content">
			<div class="menu-breadcrumb">
				<ul class="breadcrumb">
					<li><a href="<?php echo base_url(); ?>dashboard" id="menu-breadcrumb">Dashboard</a></li>
					<li>Penilaian</li>
				</ul>
			</div>
			<div class="menu-head">
				<ul>
					<li><a href="<?php echo base_url(); ?>standar7/penelitian" id="menu-head" >Penelitian</a></li>
					<li><a href="<?php echo base_url(); ?>standar7/abdimas" id="menu-head" >Abdimas</a></li>
					<li><a href="<?php echo base_url(); ?>standar7/kerjasama" id="menu-head" >Kerjasama</a></li>
				</ul>
			</div>

			<div class="kerjasama_layout" style="padding:2%;width:95%;">
        <h3 >Hasil Simulasi Penilaian Standar 7</h3>
        </br>
        <table id="view-nilai" style="padding:1%;">
          <thead class="table head-peldosen" >
            <tr >
              <th >ID </th>
              <th >Aspek penilaian</th>
              <th >Harkat & Peringkat</th>
              <th >Bobot</th>
              <th >Harkat & Peringkat x Bobot</th>
              <th >Penilai Terakhir</th>
              <th >Tanggal Terakhir</th>

              </tr>
          </thead>

                    <tbody style="font-size:75%;" >
             <?php

                       foreach($nilai as $nl) { ?>
                  <tr>
                      <td><?php echo $nl->id ?></td>
                      <td><?php echo $nl->aspek_penilaian ?></td>
                      <td><?php echo round($nl->simulasi_nilai, 4) ?></td>
                      <td><?php echo $nl->bobot ?></td>
                      <td style="text-align:center;"><?php echo round($nl->nilai, 2) ?></td>
                      <td><?php echo $nl->penilai_terakhir ?></td>
                      <td><?php echo $nl->date ?></td>
                  </tr>
                  <?php
                       }
                       ?>
                 </tbody>
                  <tfoot class="table head-peldosen">
                    <th colspan='2'>Total </th>
                    <th ><?php echo round($nilaihrkt_count, 2) ?></th>
                    <th > </th>
                    <th><?php echo round($nilai_count, 2) ?></th>
                    <th colspan='2'></th>
                  </tr>
                  <tfoot>

        </table>
     <form action ="<?php echo base_url(); ?>Reportstd/rptpenelitian" target="_blank">
            <input class="btn" style="margin-left:0;" type="submit" value="Report" />
        </form>
      </br>
        <div>
          <label>Report Version</lebel>
          <?php

                    foreach($results as $row) { ?>
            <table style="margin-left:0px;">
          <tr <?php echo $row->no ?>>
          <td ><a href="<?php echo base_url(); ?>uploads/report/<?php echo $row->file_names ?>" target="_blank"><?php echo $row->file_names ?></a></td>
          <td><label><?php echo $row->file_date ?></label></td>
          </tr >
          <table>
          <?php
               }
               ?>
        </div>
    </div>

    <hr>


    			<div class="kerjasama_layout" style="padding:2%;width:95%;">
            <h3 >Daftar Penilai Simulasi Penilaian Standar 7</h3>
            </br>
            <table id="view-record-penilai" style="padding:1%;text-align:center;">

                  <thead class="table head-peldosen">
                     <th >No</th>
                      <th >Id Standar 7</th>
                       <th >nama penilai</td>
                        <th >Bobot</th>
                        <th >Nilai</th>
                        <th >Tanggal</th>


                 </thead>
                   <tbody style="font-size:75%;">
                 <?php
                          $no = 1;
                           foreach($record_penilai_view as $nla){
                             ?>
                             <tr>
                               <td><?php echo $no++ ?></td>
                               <td><?php echo $nla->id_standar7 ?></td>
                               <td><?php echo $nla->nama_penilai ?></td>
                               <td><?php echo $nla->bobot ?></td>
                               <td><?php echo $nla->nilai ?></td>
                               <td><?php echo $nla->date ?></td>
                             </tr>



                        <?php
                           }

                           ?>
              </tbody>
              <tfoot class="table head-peldosen">
                <th >No</th>
                 <th >Id Standar 7</th>
                  <th >nama penilai</td>
                   <th >Bobot</th>
                   <th >Nilai</th>
                   <th >Tanggal</th>
              </tfoot>
            </table>
        </div>
		<div class="footer">
			<?php
			include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/footer.php";
			?>
		</div>

	</body>



</html>
