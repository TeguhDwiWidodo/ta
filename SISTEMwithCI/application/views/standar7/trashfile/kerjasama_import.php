<?php

require $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/Classes/PHPExcel/IOFactory.php";

if(isset($_POST['btn-upload'])){  
	if(isset($_FILES['files'])){
		foreach($_FILES['files']['tmp_name'] as $key => $tmp_name) {
			$table = $_POST['kerjasama'];
			$inputfilename = $_FILES['files']['name'][$key];
			$exceldata = array();
			$folder= "uploads/dokumenstandar7/"; 
			if(move_uploaded_file($tmp_name,$folder.$inputfilename)) {
				try {
					$inputfiletype = PHPExcel_IOFactory::identify($folder.$inputfilename);
					$objReader = PHPExcel_IOFactory::createReader($inputfiletype);
					$objPHPExcel = $objReader->load($folder.$inputfilename);
				} catch(Exception $e) {
					die('Error loading file "'.pathinfo($folder.$inputfilename,PATHINFO_BASENAME).'": '.$e->getMessage());
				}
				$sheet = $objPHPExcel->getSheet(0); 
				$highestRow = $sheet->getHighestRow(); 
				$highestColumn = $sheet->getHighestColumn();
				$headings = $sheet->rangeToArray('A1:' . $highestColumn . 1, NULL, TRUE, FALSE);
				for ($row = 2; $row <= $highestRow; $row++){ 
					//  Read a row of data into an array
					$rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);			
						$sql = "INSERT INTO $table (`nama_instansi`, `jenis_kegiatan`, `mulai_kerjasama`, `akhir_kerjasama`, `instasi_negeri`, `keterangan`) VALUES ('".$rowData[0][0]."','".$rowData[0][1]."','".$rowData[0][2]."','".$rowData[0][3]."','".$rowData[0][4]."','".$rowData[0][5]."') 
						ON DUPLICATE KEY UPDATE nama_instansi='".$rowData[0][0]."', jenis_kegiatan='".$rowData[0][1]."',mulai_kerjasama='".$rowData[0][2]."',akhir_kerjasama='".$rowData[0][3]."',instasi_negeri='".$rowData[0][4]."',keterangan='".$rowData[0][5]."';";
					
					$this->db->query($sql);
				?>
					<script>
						alert('successfully uploaded');
						window.location.href='../standar7/kerjasamadata?success';
					</script>
				<?php
				}
			} else {
				?>
				<script>
					alert('error while uploading file');
					window.location.href='../standar7/kerjasamadata?fail';
				</script>
				<?php
			}
			
			
			
		}
	} else {
		echo "file tidak ada";
	}
}