<?php
if(!isset($_SESSION['nama'])){
      header("location:" . base_url());
      exit();
   }
?>

<!DOCTYPE html>
<html>
	<head>
		<title>Akreditasi | Kerjasama</title>
		<script type = 'text/javascript' src="<?php echo base_url(); ?>js/jquery-1.12.4.js"></script>
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/mainlayout.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/penelitian.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/dosen.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/accordion.css">
		<script type = 'text/javascript' src="<?php echo base_url(); ?>js/jquery.dataTables.js"></script>
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/jquery.dataTables.css">
		<script type = 'text/javascript' >
			(function ($){
				$(document).ready(function() {
					$('#table-view-data-kerjasama-dalam-negeri').dataTable({
						"order": [[ 0, 'asc' ]],
					});
				});
			})(jQuery);
		</script>
		<script type = 'text/javascript' >
			(function ($){
				$(document).ready(function() {
					$('#table-view-data-kerjasama-luar-negeri').dataTable({
						"order": [[ 0, 'asc' ]],
					});
				});
			})(jQuery);
		</script>
	</head>

	<body>
		<div class="header">
			<h1><a href="<?php echo base_url(); ?>dashboard">Akreditasi SI</a></h1>
			<?php
				include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/header.php";
			?>
		</div>

		<div class="sidebar">
			<?php
				include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/sidebar.php";
			?>
		</div>

		<div class="main-layout">

      <div class="main-content">

			     <div class="menu-breadcrumb">
				     <ul class="breadcrumb">
					     <li><a href="<?php echo base_url(); ?>dashboard" id="menu-breadcrumb">Dashboard</a></li>
					     <li>Kerjasama</li>
				     </ul>
			     </div>

    			<div class="menu-head">
    				<ul>
    					<li><a href="<?php echo base_url(); ?>standar7/penelitian" id="menu-head" >Penelitian</a></li>
    					<li><a href="<?php echo base_url(); ?>standar7/abdimas" id="menu-head" >Pengmas</a></li>
    					<li><a href="<?php echo base_url(); ?>standar7/kerjasama" id="menu-head" style="background:#d9d9d9;color:black;">Kerjasama</a></li>
    				</ul>
    			</div>

				  <div class="menu-subhead">
				    <ul>
					    <li><a href="<?php echo base_url(); ?>standar7/kerjasamadata" id="menu-subhead">Data Kerjasama</a></li>
				    </ul>
			    </div>

    			<div class="sub-header">
    			    <h2> Standar 7 : Simulasi Penilaian Kerjasama</h2>
    			</div>
			</div>
    </br>
      <div class="kerjasama_layout">

        <div class="731kerjasama">

          <button class="accordion" >7.3.1 Pedoman Penilaian Kerjasama Dalam Negeri</button>
          <div class="panel">
            <div class="panel-pedoman" style="padding:2%;width:25%;float:left;">
              <h4>Pedoman Penilaian</h4>
                <label style="font-size:80%;">Kegiatan kerjasama dengan instansi di dalam negeri dalam tiga tahun terakhir.</label>
                </br>
                </br>
              <h4>Deskriptor</h4>
                <label style="font-size:80%;">Kegiatan kerjasama dengan instansi di dalam negeri dalam tiga tahun terakhir</label>
                </br>
              </div>
              <div style="padding:2%;width:65%;float:left;">
                  <label><b>Harkat dan Peringkat</b></label>

                    <table class="table-pedoman"; style="text-align:center;">
                      <tr>
                        <th style="background-color:green;font-size:80%;padding:1%;width:20%;border:1px solid gray;">Sangat Baik</th>
                        <th style="background-color:#00ff00;font-size:80%;padding:1%;width:20%;border:1px solid gray;">Baik</th>
                        <th style="background-color:orange;font-size:80%;padding:1%;width:20%;border:1px solid gray;">Cukup</th>
                        <th style="background-color:#ff3300;font-size:80%;padding:1%;width:20%;border:1px solid gray;">Kurang</th>
                        <th style="background-color:red;font-size:80%;padding:1%;width:20%;border:1px solid gray;">Sangat Kurang</th>
                      </tr>
                         <tr>
                            <td style="border:1px solid gray;">4</sub></td>
                            <td style="border:1px solid gray;">3</sub></td>
                            <td style="border:1px solid gray;">2</sub></td>
                            <td style="border:1px solid gray;">1</sub></td>
                            <td style="border:1px solid gray;">0</sub></td>
                        </tr>
                        <tr>
                          <td style="border:1px solid gray;text-align:left;font-size:80%;padding:1%;">Ada kerjasama dengan institusi di dalam negeri, banyak dalam jumlah.  Semuanya  relevan dengan bidang keahlian PS.</td>
                          <td style="border:1px solid gray;text-align:left;font-size:80%;padding:1%;">Ada kerjasama dengan institusi di dalam negeri, cukup dalam jumlah.  Sebagian besar relevan dengan bidang keahlian PS.</td>
                          <td style="border:1px solid gray;text-align:left;font-size:80%;padding:1%;">Ada kerjasama dengan institusi di dalam negeri, kurang dalam jumlah. Sebagian besar relevan dengan bidang keahlian PS. </td>
                          <td style="border:1px solid gray;text-align:left;font-size:80%;padding:1%;">Belum ada atau tidak ada kerjasama.</td>
                          <td style="border:1px solid gray;text-align:left;font-size:80%;padding:1%;">Tadak ada skor nol</td>
                        </tr>
                    </table>

            </div>

          </div>
          <div class="panel-simulasi">
            <div style="border:1px solid #3399ff; width:100%;background:white;position:relative;overflow:hidden;" class="smlsi-nilai-dl">
                <div style="font-size:15px;height:20px;padding:1%;padding-left:1.5%;background:#3399ff;">
                  <label>7.3.1 Penilaian Kerjasama Dalam Negeri</label>
                </div>
                <button style="background:#333333;padding-left:5%;color:white;" class="accordion" >Data Kerjasama Dalam Negeri</button>
                <div class="panel">
                <div class="panel-dtkerjasama" style="padding:2%;margin-bottom:3%;">
                  <table id="table-view-data-kerjasama-dalam-negeri" class="display" cellspacing="0">
                    <thead class="table head-peldosen">
                      <th >No</th>
                      <th >Nama Instansi</th>
                      <th>Jenis Kegiatan</th>
                      <th>Mulai Kerjasama</th>
                      <th>Akhir Kerjasama</th>
                      <th>Instansi Negeri</th>
                      <th>Manfaat</th>
                      <th >Ket</th>
                    </thead>

                    <tbody class="table body-peldosen" role="alert" aria-live="polite" aria-relevant="all">
                      <?php
                          $no = 1;
                          foreach($result as $row){
                      ?>
                      <tr id="row:<?php echo $row->id ?>">
                        <td > <?php echo $no++ ?></td>
                        <td  > <?php echo $row->nama_instansi ?></td>
                        <td  > <?php echo $row->jenis_kegiatan ?></td>
                        <td  > <?php echo $row->mulai_kerjasama ?></td>
                        <td > <?php echo $row->akhir_kerjasama ?></td>
                        <td > <?php echo $row->instasi_negeri ?></td>
                          <td > <?php echo $row->manfaat ?></td>
                        <td > <?php echo $row->keterangan ?></td>
                      </tr>
                      <?php
                        }
                      ?>
                    </tbody>
                    <tfoot class="table head-peldosen">
                    <th colspan="5">Total Instansi Dalam Negeri</th>
                    <th><?php echo $count_dn; ?></th>
                    <th></th>
                    <th></th>
                    </tfoot>
                  </table>
                  <form action ="<?php echo base_url(); ?>Reportstd/rptpenelitian731" target="_blank">
                         <input class="btn" style="margin-left:0;" type="submit" value="Cetak" />
                     </form>
                </div>
              </div>
              <hr>
              </br>
                  <div style="padding:2%;">
                    <label style="margin-left:5%;"><b>Simulasi Penilaian</b></label>
                  </br>
                  <hr>
                  </br>
                  <table>
                    <tr>
                      <td style="width:20%;"><label>Input Nilai</label></td>
                      <td style="width:1%;"><label>:</label>
                      </td>
                      <td style="width:20%;">
                        <label title="Ada kerjasama dengan institusi di dalam negeri, banyak dalam jumlah.  Semuanya  relevan dengan bidang keahlian PS.">
                        <span><input style="padding-left:1%;" id="hasil_option1" type="radio" onchange="calculasi1()"   name="nilai_option" value="4" />4 : Sangat Baik</span>
                      </label>
                      </td>
                      <td style="width:14%;">
                        <label><label title="Ada kerjasama dengan institusi di dalam negeri, cukup dalam jumlah.  Sebagian besar relevan dengan bidang keahlian PS" >
                        <span><input style="padding-left:1%;" id="hasil_option2" type="radio" onchange="calculasi1()" name="nilai_option" value="3"/>3 : Baik</span>
                      </label></label>
                      </td>
                      <td style="width:14%;"><label>
                        <label title="Ada kerjasama dengan institusi di dalam negeri, kurang dalam jumlah. Sebagian besar relevan dengan bidang keahlian PS" >
                        <span><input style="padding-left:1%;" id="hasil_option3" type="radio" onchange="calculasi1()" name="nilai_option" value="2"/>2 : Cukup</span>
                      </label></label>
                      </td>
                      <td style="width:14%;" ><label>
                        <label title="Belum ada atau tidak ada kerjasama" >
                        <span><input style="padding-left:1%;" id="hasil_option4"  type="radio" onchange="calculasi1()" name="nilai_option" value="1"/>1 : Kurang</span>
                      </label></label>
                      </td>
                      <td style="width:20%;"><label><label title="(Tadak ada skor nol)" >
                        <span><input style="padding-left:1%;" id="hasil_option5" type="radio" onchange="calculasi1()" name="nilai_option"  value="0" >0 : Sangat Kurang</span>
                      </label></label>
                      </td>
                    </tr>
                    <tr>
                      <td style="width:20%;"><label>Bobot</label></td>
                      <td style="width:1%;"><label>:</label></td>
                      <td colspan="6"><label>1.88</label></td>
                    </tr>
                    <tr>
                      <td style="width:20%;"><label>Hasil</label></td>
                      <td style="width:1%;"><label>:</label></td>
                      <td colspan="6"><label id="result"></label></td>
                    </tr>

                  </table>
                    </br>
                    <hr>
                   <button style="display: visible;left: 10;" name="btn-upload" type="button"  onclick="save_hasil1()" id="save_hasil1" class="save btn" value="save" disabled="disabled">Simpan </button>
                 </br>

                   <script>
                      $("#hasil_option1").change(function () {$("#save_hasil1").prop("disabled", false);});
                      $("#hasil_option2").change(function () {$("#save_hasil1").prop("disabled", false);});
                      $("#hasil_option3").change(function () {$("#save_hasil1").prop("disabled", false);});
                      $("#hasil_option4").change(function () {$("#save_hasil1").prop("disabled", false);});
                      $("#hasil_option5").change(function () {$("#save_hasil1").prop("disabled", false);});
                  </script>

                  <script>
                      function calculasi1(){
                               if( document.getElementById("hasil_option1").checked == true){
                                  var nilai1 = document.getElementById("hasil_option1").value * 1.88;
                                  document.getElementById("result").innerHTML= nilai1;
                                  document.getElementById("save_hasil1").style.visibility = 'visible';


                               } else if( document.getElementById("hasil_option2").checked == true){
                                  var nilai1 = document.getElementById("hasil_option2").value * 1.88;
                                  document.getElementById("result").innerHTML= nilai1;
                                  document.getElementById("save_hasil1").style.visibility = 'visible';


                               }else if( document.getElementById("hasil_option3").checked == true){
                                  var nilai1 = document.getElementById("hasil_option3").value * 1.88;
                                  document.getElementById("result").innerHTML= nilai1;
                                  document.getElementById("save_hasil1").style.visibility = 'visible';


                               }else if( document.getElementById("hasil_option4").checked == true){
                                  var nilai1 = document.getElementById("hasil_option4").value * 1.88;
                                  document.getElementById("result").innerHTML= nilai1;
                                  document.getElementById("save_hasil1").style.visibility = 'visible';


                               }else if( document.getElementById("hasil_option5").checked == true){
                                  var nilai1 = document.getElementById("hasil_option5").value * 1.88;
                                  document.getElementById("result").innerHTML= nilai1;
                                  document.getElementById("save_hasil1").style.visibility = 'visible';
                   }
                 }

                 function save_hasil1(){
                   var nilai1 = document.getElementById("result").innerHTML;
                   var sim_nilai = $("input[name=nilai_option]:checked").val();
                 var id1 = "7.3.1";
                          var bobot = 1.88;
                 $.ajax({
                     type: "POST",
                     url: "<?php echo base_url(); ?>Standar7/simulasinilai",
                  data: { nilai1 : nilai1, sim_nilai:sim_nilai, id1:id1, bobot:bobot},

                     success: function(data){
                 console.log(data);
               alert("Sukses input data nilai, hasil "+nilai1);
                       },
                     error: function(xhr, status, errorThrown){
                     alert( xhr.status);
                     alert( xhr.errorThrown);
                     alert( xhr.responseText);
                     console.log(xhr.responseText);
                     }
                 });

                 }
                 </script>
                   </div>
            </div>

          </div>

        </div>






    </br>





        <div class="732kerjasama">

                    <button class="accordion" >7.3.2 Pedoman Penilaian Kerjasama Luar Negeri</button>
                    <div class="panel">
                      <div class="panel-pedoman" style="padding:2%;width:25%;float:left;">
                        <h4>Pedoman Penilaian</h4>
                          <label style="font-size:80%;">Kegiatan kerjasama dengan instansi di Luar negeri dalam tiga tahun terakhir.</label>
                          </br>
                          </br>
                        <h4>Deskriptor</h4>
                          <label style="font-size:80%;">Kegiatan kerjasama dengan instansi di luar negeri dalam tiga tahun terakhir</label>
                          </br>
                          </br>
                        </div>
                          <div style="padding:2%;width:65%;float:left;">
                            <label><b>Harkat dan Peringkat</b></label>
                              <table class="table-pedoman"; style="text-align:center;">
                                <tr>
                                  <th style="background-color:green;font-size:80%;padding:1%;width:20%;border:1px solid gray;">Sangat Baik</th>
                                  <th style="background-color:#00ff00;font-size:80%;padding:1%;width:20%;border:1px solid gray;">Baik</th>
                                  <th style="background-color:orange;font-size:80%;padding:1%;width:20%;border:1px solid gray;">Cukup</th>
                                  <th style="background-color:#ff3300;font-size:80%;padding:1%;width:20%;border:1px solid gray;">Kurang</th>
                                  <th style="background-color:red;font-size:80%;padding:1%;width:20%;border:1px solid gray;">Sangat Kurang</th>
                                </tr>
                                   <tr>
                                      <td style="border:1px solid gray;">4</sub></td>
                                      <td style="border:1px solid gray;">3</sub></td>
                                      <td style="border:1px solid gray;">2</sub></td>
                                      <td style="border:1px solid gray;">1</sub></td>
                                      <td style="border:1px solid gray;">0</sub></td>
                                  </tr>
                                  <tr>
                                    <td style="text-align:left;font-size:80%;padding:1%;border:1px solid gray;">Ada kerjasama dengan institusi di luar negeri, banyak dalam jumlah.  Semuanya  relevan dengan bidang keahlian PS.</td>
                                    <td style="text-align:left;font-size:80%;padding:1%;border:1px solid gray;">Ada kerjasama dengan institusi di luar negeri, cukup dalam jumlah.  Sebagian besar relevan dengan bidang keahlian PS.</td>
                                    <td style="text-align:left;font-size:80%;padding:1%;border:1px solid gray;">Ada kerjasama dengan institusi di luar negeri, kurang dalam jumlah. Sebagian besar relevan dengan bidang keahlian PS. </td>
                                    <td style="text-align:left;font-size:80%;padding:1%;border:1px solid gray;">Belum ada atau tidak ada kerjasama.</td>
                                    <td style="text-align:left;font-size:80%;padding:1%;border:1px solid gray;">Tadak ada skor nol</td>
                                  </tr>
                              </table>
                      </div>
                    </div>
                    <div class="panel-simulasi">
                      <div style="border:1px solid #3399ff; width:100%;background:white;position:relative;overflow:hidden;" class="smlsi-nilai-dl">
                      <div style="font-size:15px;height:20px;padding:1%;padding-left:1.5%;background:#3399ff;">
                        <label>7.3.2 Penilaian Kerjasama Luar Negeri</label>
                      </div>
                      <button class="accordion" style="background:#333333;padding-left:5%;color:white;" >Data Kerjasama Luar Negeri</button>
                      <div class="panel">
                      <div class="" style="padding:2%;">
                        <table id="table-view-data-kerjasama-luar-negeri" class="display" width="100%" cellspacing="0">

                          <thead class="table head-peldosen">
                            <th>no</th>
                            <th >Nama Instansi</th>
                            <th >Jenis Kegiatan</th>
                            <th >Mulai Kerjasama</th>
                            <th>Akhir Kerjasama</th>
                            <th>Instansi Negeri</th>
                            <th>Manfaat</th>
                            <th>Ket</th>
                          </thead>

                          <tbody class="table body-peldosen" role="alert" aria-live="polite" aria-relevant="all">

                            <?php
                                $no = 1;
                                foreach($result1 as $row){

                                ?>
                                  <tr id="row:<?php echo $row->id ?>">
                                    <td ><?php echo $no++ ?></td>
                                    <td ><?php echo $row->nama_instansi ?></td>
                                    <td><?php echo $row->jenis_kegiatan ?></td>
                                    <td><?php echo $row->mulai_kerjasama ?></td>
                                    <td><?php echo $row->akhir_kerjasama ?></td>
                                    <td><?php echo $row->instasi_negeri ?></td>
                                    <td><?php echo $row->manfaat ?></td>
                                    <td ><?php echo $row->keterangan ?></td>
                                  </tr>
                              <?php
                                }
                              ?>
                          </tbody>
                          <tfoot class="table head-peldosen">
                            <th colspan="5">Total Instansi Luar Negeri</th>
                            <th><?php echo $count_ln; ?></th>
                            <th></th>
                            <th></th>
                          </tfoot>
                        </table>
                        <form action ="<?php echo base_url(); ?>Reportstd/rptpenelitian732" target="_blank">
                               <input class="btn" style="margin-left:0;" type="submit" value="Cetak" />
                           </form>
                      </div>
                    </div>
                      </br>
                        <hr>
                        </br>
                            <div style="padding:2%;">
                              <label style="margin-left:5%;"><b>Simulasi Penilaian</b></label>
                            </br>
                            <hr>
                            </br>
                            <table>
                              <tr>
                                <td style="width:20%;"><label>Input Nilai</label></td>
                                <td style="width:1%;"><label>:</label>
                                </td>
                                <td style="width:20%;">
                                  <label title="Ada kerjasama dengan institusi di Luar negeri, banyak dalam jumlah.  Semuanya  relevan dengan bidang keahlian PS.">
                                  <span><input style="padding-left:1%;" id="hasil_option_luar1" type="radio" onchange="calculasi2()"   name="nilai_option" value="4" />4 : Sangat Baik</span>
                                </label>
                                </td>
                                <td style="width:14%;">
                                  <label><label title="Ada kerjasama dengan institusi di Luar negeri, cukup dalam jumlah.  Sebagian besar relevan dengan bidang keahlian PS" >
                                  <span><input style="padding-left:1%;" id="hasil_option_luar2" type="radio" onchange="calculasi2()" name="nilai_option" value="3"/>3 : Baik</span>
                                </label></label>
                                </td>
                                <td style="width:14%;"><label>
                                  <label title="Ada kerjasama dengan institusi di Luar negeri, kurang dalam jumlah. Sebagian besar relevan dengan bidang keahlian PS" >
                                  <span><input style="padding-left:1%;" id="hasil_option_luar3" type="radio" onchange="calculasi2()" name="nilai_option" value="2"/>2 : Cukup</span>
                                </label></label>
                                </td>
                                <td style="width:14%;" ><label>
                                  <label title="Belum ada atau tidak ada kerjasama" >
                                  <span><input style="padding-left:1%;" id="hasil_option_luar4"  type="radio" onchange="calculasi2()" name="nilai_option" value="1"/>1 : Kurang</span>
                                </label></label>
                                </td>
                                <td style="width:20%;"><label><label title="(Tadak ada skor nol)" >
                                  <span><input style="padding-left:1%;" id="hasil_option_luar5" type="radio" onchange="calculasi2()" name="nilai_option"  value="0" >0 : Sangat Kurang</span>
                                </label></label>
                                </td>
                              </tr>
                              <tr>
                                <td style="width:20%;"><label>Bobot</label></td>
                                <td style="width:1%;"><label>:</label></td>
                                <td colspan="6"><label>1.88</label></td>
                              </tr>
                              <tr>
                                <td style="width:20%;"><label>Hasil</label></td>
                                <td style="width:1%;"><label>:</label></td>
                                <td colspan="6"><label id="result1"></label></td>
                              </tr>

                            </table>
                              </br>
                              <hr>
                             <button style="display: visible;left: 10;" name="btn-upload" type="button"  onclick="save_hasil()" id="save_hasil" class="save btn" value="save" disabled="disabled">Simpan </button>
                           </br>

                                               <script>
                                                  $("#hasil_option_luar1").change(function () {$("#save_hasil").prop("disabled", false);});
                                                  $("#hasil_option_luar2").change(function () {$("#save_hasil").prop("disabled", false);});
                                                  $("#hasil_option_luar3").change(function () {$("#save_hasil").prop("disabled", false);});
                                                  $("#hasil_option_luar4").change(function () {$("#save_hasil").prop("disabled", false);});
                                                  $("#hasil_option_luar5").change(function () {$("#save_hasil").prop("disabled", false);});
                                                 </script>

                                               <script>
                                               function calculasi2(){
                                                 if( document.getElementById("hasil_option_luar1").checked == true){
                                                    var nilai1 = document.getElementById("hasil_option_luar1").value * 1.88;
                                                    document.getElementById("result1").innerHTML= nilai1;
                                                    document.getElementById("save_hasil").style.visibility = 'visible';


                                                 } else if( document.getElementById("hasil_option_luar2").checked == true){
                                                    var nilai1 = document.getElementById("hasil_option_luar2").value * 1.88;
                                                    document.getElementById("result1").innerHTML= nilai1;
                                                    document.getElementById("save_hasil").style.visibility = 'visible';


                                                 }else if( document.getElementById("hasil_option_luar3").checked == true){
                                                    var nilai1 = document.getElementById("hasil_option_luar3").value * 1.88;
                                                    document.getElementById("result1").innerHTML= nilai1;
                                                    document.getElementById("save_hasil").style.visibility = 'visible';


                                                 }else if( document.getElementById("hasil_option_luar4").checked == true){
                                                    var nilai1 = document.getElementById("hasil_option_luar4").value * 1.88;
                                                    document.getElementById("result1").innerHTML= nilai1;
                                                    document.getElementById("save_hasil").style.visibility = 'visible';


                                                 }else if( document.getElementById("hasil_option_luar5").checked == true){
                                                    var nilai1 = document.getElementById("hasil_option_luar5").value * 1.88;
                                                    document.getElementById("result1").innerHTML= nilai1;
                                                    document.getElementById("save_hasil").style.visibility = 'visible';

                                     }

                                               }

                                               function save_hasil(){

                                                 var nilai1 = document.getElementById("result1").innerHTML;
                                                 //var sim_nilai = $("input[value=nilai_option]").val();
                                                 var sim_nilai = $("input[name=nilai_option]:checked").val();
                                               var id1 = "7.3.2";
                                                        var bobot = 1.88;
                                               $.ajax({


                                                   type: "POST",
                                                   url: "<?php echo base_url(); ?>Standar7/simulasinilai",
                                                data: { nilai1 : nilai1, sim_nilai:sim_nilai, id1:id1, bobot:bobot},


                                               success: function(data){
                                               console.log(data);
                                               alert("Sukses input data nilai, hasil "+nilai1);
                                               },

                                               error: function(xhr, status, errorThrown){
                                               alert( xhr.status);
                                               alert( xhr.errorThrown);
                                               alert( xhr.responseText);
                                               console.log(xhr.responseText);
                                               }
                                               });

                                               }
                                               </script>
                             </div>
                      </div>

                    </div>
        </div>

      </div>

      <!-- ################### Script-Accordion ###################### -->
      <script>
						var acc = document.getElementsByClassName("accordion");
						var i;

						for (i = 0; i < acc.length; i++) {
						  acc[i].onclick = function() {
							this.classList.toggle("active");
							var panel = this.nextElementSibling;
							if (panel.style.maxHeight){
							  panel.style.maxHeight = null;
							} else {
							  panel.style.maxHeight = panel.scrollHeight + "px";
							}
						  }
						}
			</script>
      <!-- ################### Script-Accordion-End ###################### -->

    </div>

		<div class="footer">
			<?php
			include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/footer.php";
			?>
		</div>
	</body>

</html>
