
<!DOCTYPE html>
<html>
	<head>
		<title>Akreditasi | Penelitian Dosen SI (TA)</title>
		<script type = 'text/javascript' src="<?php echo base_url(); ?>js/jquery-1.12.4.js"></script>
		<script type = 'text/javascript' src="<?php echo base_url(); ?>js/jquery-ui-1.12.1/jquery-ui.js"></script>
		<link rel="stylesheet" href="<?php echo base_url(); ?>js/jquery-ui-1.12.1/jquery-ui.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/mainlayout.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/penelitian.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/dosen.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/accordion.css">
		<script type = 'text/javascript' src="<?php echo base_url(); ?>assets/autosize-master/dist/autosize.js"></script>
		<script type = 'text/javascript' src="<?php echo base_url(); ?>js/jquery.dataTables.js"></script>
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/jquery.dataTables.css">
		<script type = 'text/javascript' >
		(function ($){
			$(document).ready(function() {
				$('#table-view-data-pelta').dataTable({
					"order": [[ 1, 'asc' ]],
					"aLengthMenu": [5, 10, 25],
					iDisplayLength: 5,
						"scrollX": true,
  					//"deferRender": true,
				});
			});
		})(jQuery);
		</script>
		<script type = 'text/javascript' >
		(function ($){
			$(document).ready(function() {

				$('#table-view-data-tim-akreditasi').dataTable({
					"order": [[ 1, 'asc' ]],
					"aLengthMenu": [5, 10, 25],
					iDisplayLength: 5,
						"scrollX": true,
  					//"deferRender": true,
				});

			});
		})(jQuery);
		</script>
		<script type='text/javascript'>
		$(function() {
						$( "#tags" ).autocomplete({
							 source: "<?php echo base_url(); ?>standar7/dosenautocomplete",
							 dataType:'json',
							 type:'POST',
							 minLength: 4

						});
				 });
		</script>
		<script type='text/javascript'>
		$(function() {
						$( "#ketuaa" ).autocomplete({
							 source: "<?php echo base_url(); ?>standar7/dosenautocomplete",
							 dataType:'json',
							 type:'POST',
							 minLength: 4

						});
				 });
		</script>
		<script type='text/javascript'>
		$(function() {

				//random json values
				var projects = [
					{id:1,value:"Thomas",cp:134},
				    {id:65,value:"Richard",cp:1743},
				    {id:235,value:"Harold",cp:7342},
				    {id:78,value:"Santa Maria",cp:787},
					{id:75,value:"Gunner",cp:788},
					{id:124,value:"Shad",cp:124},
					{id:1233,value:"Aziz",cp:3544},
					{id:244,value:"Buet",cp:7847}
				];
				$( "#tags1" ).autocomplete({
					minLength: 4,
					source: '<?php echo base_url(); ?>standar7/dosenautocomplete1',

					focus: function( event, ui ) {
						console.log(ui.item.value);
				        $( "#tags1" ).val( ui.item.value );
								$( "#prodi" ).val( ui.item.prodi );
								$( "#nots" ).val( ui.item.nots );
						$( "#judul" ).val( ui.item.judul );
						$( "#ketua" ).val( ui.item.ketua );
						$( "#anggota" ).val( ui.item.anggota );
						$( "#tahun" ).val( ui.item.tahun );
						return false;
					}//you can write for select too
				    /*select:*/
				})
				});
		</script>
	</head>

	<body>
		<div class="header">
			<h1><a href="<?php echo base_url(); ?>dashboard">Akreditasi SI</a></h1>
			<?php
				include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/header.php";
			?>
		</div>

		<div class="sidebar">
			<?php
				include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/sidebar.php";
			?>
		</div>

		<div class="main-layout">
			<?php

				if( $this->session->userdata('hak_akses') == 'admin' ?: $this->session->userdata('hak_akses') == 'standar 7' ?: $this->session->userdata('hak_akses') == 'kaprodi'){

				?>
			<div class="main-content">
			<div class="-menu-breadcrumb">
				<ul class="breadcrumb">
					<li><a href="<?php echo base_url(); ?>dashboard" id="menu-breadcrumb">Dashboard</a></li>
					<li><a href="<?php echo base_url(); ?>standar7/penelitian" id="menu-breadcrumb">Penelitian</a></li>
					<li>Penelitian Dosen SI (TA)</li>
				</ul>
			</div>
			<div class="menu-head">
				<ul>
					<li><a href="<?php echo base_url(); ?>standar7/penelitian" id="menu-head" style="background:#d9d9d9;color:black;">Penelitian</a></li>
					<li><a href="<?php echo base_url(); ?>standar7/abdimas" id="menu-head">Abdimas</a></li>
					<li><a href="<?php echo base_url(); ?>standar7/kerjasama" id="menu-head">Kerjasama</a></li>
				</ul>
			</div>

				<div class="menu-subhead">
				<ul>
					<li><a href="<?php echo base_url(); ?>standar7/penelitiandosen" id="menu-subhead" >Penelitian Dosen SI</a></li>
					<li><a href="<?php echo base_url(); ?>standar7/penelitiandosenta" id="menu-subhead" style="background:#404040;color:white;">Penelitian (TA)</a></li>
					<li><a href="<?php echo base_url(); ?>standar7/publikasi" id="menu-subhead">Publikasi</a></li>
					<li><a href="<?php echo base_url(); ?>standar7/haki" id="menu-subhead">HaKI</a></li>
					<li><a href="<?php echo base_url(); ?>standar7/dosen" id="menu-subhead">Dosen</a></li>
				</ul>
			</div>

			<div class="sub-header">
			<h2> Standar 7 : Penelitian Dosen Sistem Informasi (Tugas Akhir)</h2>
			</div>
			<div class="penelitian_dosen_ta dosen">


				<div class="peldosta-header dosen-header">
			    <h2>Pilih Cara : </h2>
			  </div>
			  <button class="accordion">Import Data TA Mahasiswa</button>
			  <div class="panel">
			    <div class="panel-input">
			    <br>
			    <h3 class="head-panel">IMPORT DATA TA MAHASISWA</h3>
			    <hr>
			    <br>
			  <!--	<label >Pengupdate Terakhir : <?php

			    if(empty($pengupdate->pengupdate)){

			        echo "";


			    } else {

			      echo $pengupdate->pengupdate;
			    } ?></label></br> -->
			    <label >Download Template Data Ta Mahasiswa : </label>
			    <label style="color:red;font-size:12px;"><a href="<?php echo base_url(); ?>uploads/Format_import_std7/format_data_tamahasiswa.xlsx" target="_blank">Download File</a></label>
			    <br>
			    <br>
			     <form action="<?php echo base_url(); ?>Standar7/importpenelitiansita" method="post" enctype="multipart/form-data">
			      <p class="huruf-upload-files">Files TA Mahasiswa : <input required type="file" name="files1" id="files1" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" /></p>
			      <label style="color:red;font-size:12px;">*File yang dapat di import csv, xls, dan xlxs</label>
			      <input type="hidden" name="datadosen" value="tb_penelitian_dosen_ta">
			      <br>
			      <br>
			      <hr>
			      <button class="upload-btn btn" type="submit" name="btn-upload1">Upload</button>
			    </form>
			    <br>
			  </div>
			  </div>
			  <button class="accordion">Input Data TA Mahasiswa  </button>
			  <div class="panel">
			    <table class="display" width="100%" cellspacing="0">
			    <form  class="data-diri-form" name="data-diri-form-ta"  method="post" action ="<?php echo base_url(); ?>Standar7/penelitiantainsert" enctype="multipart/form-data">

			    <tbody>
			        <tr>
			      <td colspan="3"><h3 class="head-panel">INPUT DATA TA MAHASISWA</h3><hr/><br/></td>
			      </tr>
			      <tr>
			        <td><p class="label">NO TS</p></td>
			        <td><p> : </p></td>
			        <td>
			          <select   name="no_ts" class="data-input-kerjasama">
			           <option value="ts">TS</option>
			           <option value="ts-1">TS-1</option>
			           <option value="ts-2">TS-2</option>
			         </select>

			        </td>
			      </tr>
			      <tr>
			       <td><p class="label" >Tahun Ajaran</p></td>
			       <td><p> : </p></td>
			       <td><input class="data-input-kerjasama" name="tahun_ajrn"   type="text"   placeholder="Tahun Ajaran" required ></td>
			     </tr>	<tr>
			        <td><p class="label">NIM</p></td>
			        <td><p> : </p></td>
			        <td><input class="data-input-kerjasama" name="nim"  type="text"  placeholder="NIM" required ></td>
			      </tr><tr>
			        <td><p class="label">Nama Mahasiswa</p></td>
			        <td><p> : </p></td>
			        <td><input class="data-input-kerjasama" name="nama_mahasiswa"  type="text"  placeholder="Nama Mahasiswa" required ></td>
			      </tr>
			     <tr>
			        <td><p class="label" id="label1">Kelas</p></td>
			        <td><p> : </p></td>
			        <td><input class="data-input-kerjasama" name="kelas"   type="text"   placeholder="Kelas" required ></td>
			      </tr>

			      <tr>
			        <td><p class="label">Status</p></td>
			        <td><p> : </p></td>
			        <td><input class="data-input-kerjasama" name="status"  type="text"  placeholder="Status" required ></td>
			      </tr>
			      <tr>
			        <td><p class="label">Tanggal Input</p></td>
			        <td><p> : </p></td>
			        <td><input class="data-input-kerjasama" name="input_date"  type="date"  placeholder="Tanggal Input" required ></td>
			      </tr>

			      <tr>
			        <td><p class="label">Pembimbing Akademik</p></td>
			        <td><p> : </p></td>
			        <td><input style="font-size:80%;" class="data-input-kerjasama ui-widget" id="tags" name="pembimbing_akdmk"  type="text"  placeholder="Pembimbing Akademik" required ></td>
							<label for="tags">Tags : </label>
			      </tr>

						<tr>
			        <td><p class="label">Kode Pengajar</p></td>
			        <td><p> : </p></td>
			        <td><input class="data-input-kerjasama" name="kode_pngjr"  type="text"  placeholder="Kode Pengajar" required ></td>
			      </tr>

			      <tr>
			      <td colspan="3"><br/><hr/><input class="data-diri-button btn" id="data-diri-button1" name="simpanbtn1" title="simpan" type="submit" value="Simpan" ><br/></td>
			      </tr>
			    </tbody>

			    </form>


			  </table>
			  </div>
				<button class="accordion">Data TA Mahasiswa  </button>
			  <div class="panel">
				</br>
				<div class="data-kerjasama">

				<table id="table-view-data-pelta" class="display" width="100%" cellspacing="0">

							<thead style="font-size: 60%;" class="table head-peldosen">
								<th>#</th>
								<th>no</th>
								<th >	No TS</th>
								<th >Tahun Ajaran</th>
								<th>NIM</th>
								<th>Nama mahasiswa</th>
								<th>Kelas</th>
								<th>Status</th>
								<th>Tanggal Input</th>
								<th>Pembimbing Akademik</th>
								<th>Kode Pengajar</th>

								<th><i class="fa fa-pencil-square-o" aria-hidden="true"></i></th>
								<th><i class="fa fa-trash-o" aria-hidden="true"></i></th>
							</thead>

							<tbody class="table body-peldosen" role="alert" aria-live="polite" aria-relevant="all">

								<?php
										$no = 1;

										foreach($result1 as $row){

										?>
											<tr id="row:<?php echo $row->no ?>">
											<td ><input type="checkbox" name="pilih[]" class="check" value="<?php echo $row->no ?>" ></td>
												<td ><?php echo $no++ ?></td>
												<td ><textarea class="data-diri-input" title="<?php echo $row->no_ts ?>" id="no_ts<?php echo $row->no ?>"  type="text"  name="no_ts"   placeholder="" readonly /><?php echo $row->no_ts ?></textarea></td>
												<td><textarea class="data-diri-input" title="<?php echo $row->tahun_ajrn ?>" id="tahun_ajrn<?php echo $row->no ?>"  type="text" name="tahun_ajrn"  placeholder="" readonly /><?php echo $row->tahun_ajrn ?></textarea></td>
												<td><textarea class="data-diri-input" title="<?php echo $row->nim ?>" id="nim<?php echo $row->no ?>" name="nim"  type="text"  placeholder="" readonly /><?php echo $row->nim ?></textarea></td>
												<td><textarea class="data-diri-input" title="<?php echo $row->nama_mahasiswa ?>" id="nama_mahasiswa<?php echo $row->no ?>"  type="text" name="nama_mahasiswa" placeholder="" readonly /><?php echo $row->nama_mahasiswa?></textarea></td>
												<td><textarea class="data-diri-input" title="<?php echo $row->kelas ?>" id="kelas<?php echo $row->no ?>"  type="text" name="kelas"  placeholder="" readonly /><?php echo $row->kelas ?></textarea></td>
												<td ><textarea class="data-diri-input" title="<?php echo $row->status ?>" id="status<?php echo $row->no ?>" name="status"   type="text"  placeholder="" readonly /><?php echo $row->status ?></textarea></td>
												<td ><textarea class="data-diri-input" title="<?php echo $row->input_date ?>" id="input_date<?php echo $row->no ?>" name="input_date"  type="text"  placeholder="" readonly /><?php echo $row->input_date ?></textarea></td>
												<td ><textarea class="data-diri-input" title="<?php echo $row->pembimbing_akdmk ?>" id="pembimbing_akdmk<?php echo $row->no ?>"  type="text" name="pembimbing_akdmk"  placeholder="" readonly /><?php echo $row->pembimbing_akdmk ?></textarea></td>
												<td ><textarea class="data-diri-input" title="<?php echo $row->kode_pngjr ?>" id="kode_pngjr<?php echo $row->no ?>"  type="text" name="kode_pngjr"  placeholder="" readonly /><?php echo $row->kode_pngjr ?></textarea></td>


												<td><button onclick="edit_row2('<?php echo $row->no ?>')" title="Ubah : <?php echo $row->nama_mahasiswa ?>" id="editt<?php echo $row->no ?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
												<button  onclick="save_row2('<?php echo $row->no ?>')" title="Simpan : <?php echo $row->nama_mahasiswa ?>" id="savee<?php echo $row->no ?>" style="display:none;" /><i class="fa fa-floppy-o" aria-hidden="true"></i></button>
												</td>
												<td>
												<form action="<?php echo base_url(); ?>standar7/penelitiantahapus/<?php echo $row->no ?>" method="post" enctype="multipart/form-data">
												<input type="hidden" value="<?php echo $row->no?>" name="data-id" >
												<button name="btn-delete" onclick="return ConfirmDelete(<?php echo $row->no ?>)" title="Hapus : <?php echo $row->nama_mahasiswa ?>" id="delete-btnn<?php echo $row->no ?>" type="submit"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
												</form>
													<script type="text/javascript">
															var baseUrl='<?php echo base_url(); ?>standar7/penelitiantahapus/<?php echo $row->no ?>';
															var msgg = 'Apakah anda yakin untuk menghapus data ini ?';
															function ConfirmDelete(no)
															{
															if (confirm(msgg)) {
																location.href=baseUrl;
															}
															else {
																 return false;
															}
															}
													</script>
												</td>


															<script>
											var no;
												function edit_row2(no)
												{
														document.getElementById('editt'+no).style.display = 'none';
														document.getElementById('savee'+no).style.display = 'block';

													 document.getElementById("no_ts"+no).removeAttribute('readonly');
														document.getElementById("tahun_ajrn"+no).removeAttribute('readonly');
													 document.getElementById("nim"+no).removeAttribute('readonly');
													 document.getElementById("nama_mahasiswa"+no).removeAttribute('readonly');
														document.getElementById("kelas"+no).removeAttribute('readonly');
														 document.getElementById("status"+no).removeAttribute('readonly');
														 document.getElementById("input_date"+no).removeAttribute('readonly');
														 document.getElementById("pembimbing_akdmk"+no).removeAttribute('readonly');
														 document.getElementById("kode_pngjr"+no).removeAttribute('readonly');


													 document.getElementById("no_ts"+no).style.background = "#ccffff";
															document.getElementById("tahun_ajrn"+no).style.background = "#ccffff";
													 document.getElementById("nim"+no).style.background = "#ccffff";
												 document.getElementById("nama_mahasiswa"+no).style.background = "#ccffff";
													document.getElementById("kelas"+no).style.background = "#ccffff";
												 document.getElementById("status"+no).style.background = "#ccffff";
												 document.getElementById("input_date"+no).style.background = "#ccffff";
													 document.getElementById("pembimbing_akdmk"+no).style.background = "#ccffff";
													 document.getElementById("kode_pngjr"+no).style.background = "#ccffff";

												}



											</script>
											<script>

												function save_row2(no){

													var no_ts=  document.getElementById("no_ts"+no).value;
													var tahun_ajrn= document.getElementById("tahun_ajrn"+no).value;
													var nim= document.getElementById("nim"+no).value;
													var nama_mahasiswa=  document.getElementById("nama_mahasiswa"+no).value;
													var kelas =  document.getElementById("kelas"+no).value;
													var status= document.getElementById("status"+no).value;
													var input_date=  document.getElementById("input_date"+no).value;
													var pembimbing_akdmk=  document.getElementById("pembimbing_akdmk"+no).value;

													var kode_pngjr=  document.getElementById("kode_pngjr"+no).value;
													var id = no;

																$.ajax({

																		type: "POST",
																		url: "<?php echo base_url(); ?>Standar7/penelitiantasiupdate",
																		data: {id:id, no_ts:no_ts, tahun_ajrn:tahun_ajrn, nim:nim, nama_mahasiswa:nama_mahasiswa,  kelas:kelas, status,
																		input_date:input_date, pembimbing_akdmk:pembimbing_akdmk, kode_pngjr:kode_pngjr },

																		success: function(data){
																		console.log(data);

																			},

																		error: function(xhr, status, errorThrown){
																		alert( xhr.status);
																		alert( xhr.errorThrown);
																		alert( xhr.responseText);
																		console.log(xhr.responseText);
																		}
																});

																						document.getElementById('editt'+no).style.display = 'block';
																						document.getElementById('savee'+no).style.display = 'none';
																						 document.getElementById("no_ts"+no).readOnly =true;
																							document.getElementById("tahun_ajrn"+no).readOnly =true;
																						 document.getElementById("nim"+no).readOnly =true;
																						 document.getElementById("nama_mahasiswa"+no).readOnly =true;
																							document.getElementById("kelas"+no).readOnly =true;
																							 document.getElementById("status"+no).readOnly =true;
																							 document.getElementById("input_date"+no).readOnly =true;
																							 document.getElementById("pembimbing_akdmk"+no).readOnly =true;
																							 document.getElementById("kode_pngjr"+no).readOnly =true;


																							 													 document.getElementById("no_ts"+no).style.background = "#f3f3f3";
																							 															document.getElementById("tahun_ajrn"+no).style.background = "#f3f3f3";
																							 													 document.getElementById("nim"+no).style.background = "#f3f3f3";
																							 												 document.getElementById("nama_mahasiswa"+no).style.background = "#f3f3f3";
																							 													document.getElementById("kelas"+no).style.background = "#f3f3f3";
																							 												 document.getElementById("status"+no).style.background = "#f3f3f3";
																							 												 document.getElementById("input_date"+no).style.background = "#f3f3f3";
																							 													 document.getElementById("pembimbing_akdmk"+no).style.background = "#f3f3f3";
																							 													 document.getElementById("kode_pngjr"+no).style.background = "#f3f3f3";


																					}
											</script>



											</tr>
									<?php
										}
									?>
							</tbody>

							<tfoot style="font-size: 60%;" class="table head-peldosen">
								<th><i class="fa fa-check" aria-hidden="true"></i></th>
								<th>no</th>
								<th >	No TS</th>
								<th >Tahun Ajaran</th>
								<th>NIM</th>
								<th>Nama mahasiswa</th>
								<th>Kelas</th>
								<th>Status</th>
								<th>Tanggal Input</th>
								<th>Pembimbing Akademik</th>
								<th>Kode Pengajar</th>
								<th><i class="fa fa-pencil-square-o" aria-hidden="true"></i></th>
								<th><i class="fa fa-trash-o" aria-hidden="true"></i></th>
							</tfoot>

					</table>
				</div>
				<div class="delete all-data">
					<button class="btn-delete-list" name="btn-delete-check1" type="button" onclick="hapus_checklist1()" >Hapus Terpilih</button>
					<?php
						if($this->session->flashdata('msgg')){
							echo "<script> alert('".$this->session->flashdata('msgg')."'); </script>";
						}
					?>
					<script type="text/javascript" language="javascript">
						function hapus_checklist1(){

							var selected_value = new Array(); // initialize empty array
							var confirmation = confirm("Apakah anda yakin menghapus data yang anda pilih ?");
							 if(confirmation){
								$(".check:checked").each(function(){
										selected_value.push($(this).val());
									});
									console.log(selected_value);
								$.ajax({
										type: "POST",
										url: "<?php echo base_url(); ?>Standar7/penelitiantahapuschecklist",
										data: {result:JSON.stringify(selected_value)},

										success: function(data){
										console.log(data);
										window.location.href='<?php echo base_url(); ?>Standar7/penelitiandosenta';
										},

										error: function(xhr, status, errorThrown){
										alert( xhr.status);
										alert( xhr.errorThrown);
										alert( xhr.responseText);
										console.log(xhr.responseText);
										}
								});
							 }
						}
					</script>

					<button class="btn-delete-all" name="btn-delete-all1" onclick="ConfirmDeleteall1()"  id="delete-btn1" type="submit">Hapus Semua Data</i></button>

							<script type="text/javascript">
									function ConfirmDeleteall1()

										{
												var msg = 'Apakah anda yakin menghapus semua data TA Mahasiswa ?';
												if (confirm(msg)){
													 location.href='<?php echo base_url(); ?>standar7/peltahapussemua';
												}
										}
							</script>
						</div>
					</br>
				</div>




				<div class="peldosta-header dosen-header">
					<h2>Pilih Cara : </h2>
				</div>
				<button class="accordion">Import Data Penelitian Dosen (TA Mahasiswa)</button>
				<div class="panel">
					<div class="panel-input">
					<br>
					<h3 class="head-panel">IMPORT DATA PENELITIAN DOSEN (TA MAHASISWA)</h3>
					<hr>
					<br>
				<!--	<label >Pengupdate Terakhir : <?php

					if(empty($pengupdate->pengupdate)){

							echo "";


					} else {

						echo $pengupdate->pengupdate;
					} ?></label></br> -->
					<label >Download Template Data Penelitian Dosen (TA Mahasiswa) : </label>
					<label style="color:red;font-size:12px;"><a href="<?php echo base_url(); ?>uploads/Format_import_std7/format_data_penelitiandosenta.xlsx" target="_blank">Download File</a></label>
					<br>
					<br>
				   <form action="<?php echo base_url(); ?>Standar7/importpenelitiandosensita" method="post" enctype="multipart/form-data">
						<p class="huruf-upload-files">Files Penelitian Dosen (TA Mahasiswa) : <input required type="file" name="files" id="files" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" /></p>
						<label style="color:red;font-size:12px;">*File yang dapat di import csv, xls, dan xlxs</label>
						<input type="hidden" name="datadosen" value="tb_penelitian_dosen_ta">
						<br>
						<br>
						<hr>
						<button class="upload-btn btn" type="submit" name="btn-upload">Upload</button>
					</form>
					<br>
				</div>
				</div>
				<button class="accordion">Input Data Penelitian Dosen (TA Mahasiswa)</button>
				<div class="panel">
					<table class="display" width="100%" cellspacing="0">
					<form  class="data-diri-form" name="data-diri-form"  method="post" action ="<?php echo base_url(); ?>Standar7/penelitiandosentainsert" enctype="multipart/form-data">

					<tbody>
							<tr>
						<td colspan="3"><h3 class="head-panel">INPUT DATA PENELITIAN DOSEN (TA MAHASISWA)</h3><hr/><br/></td>
						</tr>
						<tr>
							<td><p class="label">ID Peldos</p></td>
							<td><p> : </p></td>
							<td><input class="data-input-kerjasama ui-widget" id="tags1" name="idpeldos"  type="text"  placeholder="ID Peldos" required ></td>
							<label for="tags1"></label>
						</tr>
						<tr>
						 <td><p class="label" >Program studi</p></td>
						 <td><p> : </p></td>
						 <td><input class="data-input-kerjasama" id="prodi" name="program_studi"  type="text"   placeholder="Program Studi" readonly required ></td>
						 <label for="prodi"></label>
					 </tr>
					 <tr>
						<td><p class="label">NO TS </p></td>
						<td><p> : </p></td>
						<td><input class="data-input-kerjasama ui-widget" id="nots" name="no_ts"  type="text"  placeholder="NO TS" readonly  required ></td>
						<label for="nots"></label>
					</tr>

						<tr>
						 <td><p class="label" id="label1">judul penelitian dosen</p></td>
						 <td><p id="label1"> : </p></td>
						 <td><textarea style="font-size:80%;height:80px;" class="data-input-kerjasama ui-widget" id="judul" name="judul_penelitian_dosen"  type="text"   placeholder="Judul Penelitian Dosen" readonly required ></textarea></td>
						 <label for="judul"></label>
					 </tr>
					 <tr>
						 <td><p class="label">Nama Dosen Ketua  </p></td>
						 <td><p> : </p></td>
						 <td><input class="data-input-kerjasama ui-widget" id="ketua" name="nama_dosenketua"  type="text"  placeholder="Nama Dosen Ketua" readonly  required ></td>
						 <label for="ketua"></label>
					 </tr>
					 <tr>
						 <td><p class="label">Nama Dosen Anggota  </p></td>
						 <td><p> : </p></td>
						 <td><input class="data-input-kerjasama ui-widget" id="anggota" name="nama_dosenanggota"  type="text"  placeholder="Nama Dosen Anggota" readonly  required ></td>
						 <label for="anggota"></label>
					 </tr>	<tr>
 							<td><p class="label">Tahun</p></td>
 							<td><p> : </p></td>
 							<td><input class="data-input-kerjasama ui-widget" id="tahun" name="tahun"  type="text"  placeholder="Tahun"  readonly required ></td>
							<label for="tahun"></label>
 						</tr><tr>
							<td><p class="label">Nama Mahasiswa  </p></td>
							<td><p> : </p></td>
							<td><input class="data-input-kerjasama" name="nama_mahasiswa"  type="text"  placeholder="Nama Mahasiswa" required ></td>
						</tr>
					 <tr>
							<td><p class="label" id="label1">Judul TA Mahasiswa</p></td>
							<td><p> : </p></td>
							<td><textarea class="data-input-kerjasama" name="judul_ta_mahasiswa" style="height:80px;"  type="text"   placeholder="Judul TA Mahasiswa" required ></textarea></td>
						</tr>

						<tr>
					 		<td><p class="label" >Bukti laporan akhir</p></td>
					 		<td><p> : </p></td>
					 		<td><input class="data-input-kerjasama" name="bukti_laporan_akhir"   type="text"   placeholder="Bukti laporan akhir" required ></td>
					 	</tr>


						<tr>
							<td><p class="label">Keterangan</p></td>
							<td><p> : </p></td>
							<td><input class="data-input-kerjasama" name="keterangan"  type="text"  placeholder="Keterangan" required ></td>
						</tr>



												<!--		<tr>
														 <td><p class="label">Sumber pembiayaan</p></td>
														 <td><p> : </p></td>
														 <td><select name="sumber_pembiayaan"  class="data-input-kerjasama">
																 <option value="Pembiayaan sendiri oleh peneliti">Pembiayaan sendiri oleh peneliti</option>
																 <option value="PT yang bersangkutan">PT yang bersangkutan</option>
																 <option value="Depdiknas">Depdiknas</option>]
																  <option value="Institusi dalam negeri di luar Depdiknas">Institusi dalam negeri di luar Depdiknas</option>
																	<option value="Institusi luar negeri">Institusi luar negeri</option>
														</select>
												 </td>
											 </tr> -->

						<tr>
						<td colspan="3"><br/><hr/><input class="data-diri-button btn" id="data-diri-button" name="simpanbtn" title="simpan" type="submit" value="Simpan" ><br/></td>
						</tr>
					</tbody>

					</form>


				</table>
				</div>
				<?php
					if($this->session->flashdata('msgta')){
						echo "<script> alert('".$this->session->flashdata('msgta')."'); </script>";
					}
				?>
			</div>

			<div class="data-kerjasama">
			<table id="table-view-data-tim-akreditasi" class="display" width="100%" cellspacing="0">

						<thead style="font-size: 60%;" class="table head-peldosen">
							<th>#</th>
							<th>no</th>
							<th >Program studi</th>
							<th >	No ts</th>
							<th >ID Peldos</th>
							<th >judul penelitian dosen	</th>
							<th>Judul ta mahasiswa</th>
							<th>Nama mahasiswa</th>
							<th >Nama dosen ketua</th>
							<th >Nama dosen anggota</th>
							<th>tahun</th>
							<th>Bukti laporan akhir</th>
							<th>keterangan</th>
							<th>Doc Pendukung</th>
							<th>Status Valid</th>
							<th>Upload(Bukti)</th>
							<th><i class="fa fa-pencil-square-o" aria-hidden="true"></i></th>
								<?php if($this->session->userdata('hak_akses') == 'admin' ?: $this->session->userdata('hak_akses') == 'kaprodi') { ?>
										<th >Validasi </th>
								<?php } ?>
							<th><i class="fa fa-trash-o" aria-hidden="true"></i></th>
						</thead>

						<tbody class="table body-peldosen" role="alert" aria-live="polite" aria-relevant="all">

							<?php
									$no = 1;
									foreach($result as $row){

									?>
										<tr id="row:<?php echo $row->no ?>">
										<td ><input type="checkbox" name="pilih[]" class="check" value="<?php echo $row->no ?>" ></td>
											<td ><?php echo $no++ ?></td>
											<td ><textarea class="data-diri-input"  title=" <?php echo $row->prodi ?>" id="program_studi<?php echo $row->no ?>"  type="text" name="prodi" placeholder="" readonly /><?php echo $row->prodi ?></textarea></td>
											<td><textarea class="data-diri-input" title=" <?php echo $row->no_ts ?>" id="n_ts<?php echo $row->no ?>"  type="text" name="no_ts" placeholder="" readonly /><?php echo $row->no_ts ?></textarea></td>
											<td><textarea class="data-diri-input" title=" <?php echo $row->id_peldos ?>" id="n_ts<?php echo $row->no ?>"  type="text" name="id_peldos" placeholder="" readonly /><?php echo $row->id_peldos ?></textarea></td>
											<td><textarea class="data-diri-input"  title=" <?php echo $row->judul_penelitian_dosen ?>" id="judul_penelitian_dosen<?php echo $row->no ?>"  type="text" name="judul_penelitian_dosen" placeholder="" readonly /><?php echo $row->judul_penelitian_dosen ?>
											</textarea></td>
											<td ><a id="link<?php echo $row->no ?>" href="<?php echo base_url(); ?>standar7/peldos_details/<?php echo $row->id_peldos ?>" target="_blank"><textarea class="data-diri-input" title=" <?php echo $row->judul_ta_mahasiswa ?>" id="judul_ta_mahasiswa<?php echo $row->no ?>"  type="text" name="judul_ta_mahasiswa" placeholder="" readonly /><?php echo $row->judul_ta_mahasiswa ?>
											</textarea></a></td>
											<td><textarea class="data-diri-input" title=" <?php echo $row->nama_mahasiswa?>" id="nm_mahasiswa<?php echo $row->no ?>"  type="text" name="nama_mahasiswa" placeholder="" readonly /><?php echo $row->nama_mahasiswa ?></textarea></td>

											<td><textarea class="data-diri-input" title=" <?php echo $row->nama_dosenketua ?>" id="nama_dosenketua<?php echo $row->no ?>"  type="text" name="nama_dosenketua" placeholder="" readonly /><?php echo $row->nama_dosenketua ?></textarea></td>
											<td><textarea class="data-diri-input" title=" <?php echo $row->nama_dosenanggota ?>" id="nama_dosenanggota<?php echo $row->no ?>"  type="text" name="nama_dosenanggota" placeholder="" readonly /><?php echo $row->nama_dosenanggota ?></textarea></td>
											<td><textarea class="data-diri-input" title=" <?php echo $row->tahun ?>" id="tahun<?php echo $row->no ?>"  type="text" name="tahun" placeholder="" readonly /><?php echo $row->tahun?></textarea></td>

											<td ><textarea class="data-diri-input" title=" <?php echo $row->bukti_laporan_akhir ?>" id="bukti_laporan_akhir<?php echo $row->no ?>"  type="text" name="bukti_laporan_akhir" placeholder="" readonly /><?php echo $row->bukti_laporan_akhir ?> </textarea></td>
											<td ><textarea class="data-diri-input" title=" <?php echo $row->keterangan ?>" id="keterangan<?php echo $row->no ?>"  type="text" name="keterangan" placeholder="" readonly /><?php echo $row->keterangan ?></textarea></td>

											<td ><div class="lots">
																<p><a target="_blank" title="Uploaded : [<?php echo $row->file_date ?>], <?php echo $row->file_name ?> " href="<?php echo base_url(); ?>uploads/dokumen_peldosTA_std7/<?php echo $row->file_name ?>"><?php echo $row->file_name ?> </a>...</p>

											</div></td>
											<td >
												<?php
												if ($row->sts_valid == 'valid') {
													?>
														<label>Y</label>
													<?php
												} else {
													?>

													<label id="n:<?php echo $row->no ?>">N</label>
													<?php
												}?>
											</td>
											<td >


											<form onsubmit="return Validate1(this);" action="<?php echo base_url(); ?>Standar7/penelitiandosentauploadpdf/<?php echo $row->no ?>" method="post" enctype="multipart/form-data">
												<input class="aa" style="font-size:9px;display: none;" id="file<?php echo $row->no ?>" required type="file" name="files" onchange="javascript:updateList('<?php echo $row->no ?>')" accept="application/pdf" />
												<input type="hidden" value="<?php echo $row->no ?>" name="data-id" >
												<input type="hidden" value="<?php echo $row->file_name?>" name="data-filename" >
												<div class="lots" id="nama-file<?php echo $row->no ?>"></div>
												<br>
												<button style="display: block;" title="Pilih Doc Pendukung : <?php echo $row->judul_ta_mahasiswa ?>" type="button" onclick="edit_row('<?php echo $row->no ?>')" id="edit_btn<?php echo $row->no ?>" class="edit" value="edit"><i class="fa fa-file" aria-hidden="true"></i></button>
												<br>
												<button style="display: none;" name="btn-upload" title="Upload : <?php echo $row->judul_ta_mahasiswa ?>" type="submit"  onclick="save_row('<?php echo $row->no ?>')" id="save_btn<?php echo $row->no ?>" class="save" value="save"><i class="fa fa-floppy-o" aria-hidden="true"></i></button>
												</form>
												<script type="text/javascript" language="javascript">
													var _validFileExtensions1 = [".pdf"];
														function Validate1(oForm) {
															var arrInputs = oForm.getElementsByTagName("input");
															for (var i = 0; i < arrInputs.length; i++) {
																var oInput = arrInputs[i];
																if (oInput.type == "file") {
																	var sFileName = oInput.value;
																	if (sFileName.length > 0) {
																		var blnValid = false;
																		for (var j = 0; j < _validFileExtensions1.length; j++) {
																			var sCurExtension = _validFileExtensions1[j];
																			if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
																				blnValid = true;
																				break;
																			}
																		}

																		if (!blnValid) {
																			alert("Sorry, " + sFileName + " is invalid, allowed extensions are: " + _validFileExtensions1.join(", "));
																			return false;
																		}
																	}
																}
															}

															return true;
														}
												</script>
											</td>
											<td><button onclick="edit_row1('<?php echo $row->no ?>')" title="Ubah : <?php echo $row->judul_ta_mahasiswa ?>"  id="edit<?php echo $row->no ?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
											<button  onclick="save_row1('<?php echo $row->no ?>')" title="Simpan : <?php echo $row->judul_ta_mahasiswa ?>"  id="save<?php echo $row->no ?>" style="display:none;" /><i class="fa fa-floppy-o" aria-hidden="true"></i></button>
											</td>

											<?php if($this->session->userdata('hak_akses') == 'admin' ?: $this->session->userdata('hak_akses') == 'kaprodi') { ?>
													<td >
														<input type="hidden" id="idnum<?php echo $row->no ?>" value="<?php echo $row->no?>" name="data-id" >
														<input type="hidden" id="stsvalid<?php echo $row->no ?>" value="valid" name="data-id" >
														<button name="validasi" onclick="validasi(<?php echo $row->no ?> )" <?php if ($row->sts_valid == 'valid'){ ?> disabled <?php   } ?> title="Validasi : <?php echo $row->judul_ta_mahasiswa ?>" id="validasi<?php echo $row->no ?>" type="submit"><i class="fa fa-check-square" aria-hidden="true"></i></button>
														<script>
															function validasi(no){
																var id=  document.getElementById("idnum"+no).value;
																	var validasi=  document.getElementById("stsvalid"+no).value;

																	$.ajax({

																			type: "POST",
																			url: "<?php echo base_url(); ?>Standar7/peldosta_valid",
																			data: {id:id, validasi:validasi
																		 },

																			success: function(data){
																			console.log(data);

																				},

																			error: function(xhr, status, errorThrown){
																			alert( xhr.status);
																			alert( xhr.errorThrown);
																			alert( xhr.responseText);
																			console.log(xhr.responseText);
																			}
																	});
																	document.getElementById('validasi'+no).disabled = true;
																	document.getElementById('n:'+no).innerHTML = 'Y';
															}
														</script>
														</td>
											<?php } ?>


											<td>
											<form action="<?php echo base_url(); ?>standar7/penelitiandosentahapus/<?php echo $row->no ?>" method="post" enctype="multipart/form-data">
											<input type="hidden" value="<?php echo $row->no?>" name="data-id" >
											<input type="hidden" value="<?php echo $row->file_name?>" name="data-filename" >
											<button name="btn-delete" onclick="return ConfirmDelete(<?php echo $row->no ?>)" title="Hapus : <?php echo $row->judul_ta_mahasiswa ?>" id="delete-btn<?php echo $row->no ?>" type="submit"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
											</form>
												<script type="text/javascript">
														var baseUrl='<?php echo base_url(); ?>standar7/penelitiandosentahapus/<?php echo $row->no ?>';
														var msgg = 'Apakah anda yakin untuk menghapus data ini ?';
														function ConfirmDelete(no)
														{
														if (confirm(msgg)) {
															location.href=baseUrl;
														}
														else {
															 return false;
														}
														}
												</script>
											</td>


														<script>
										var no;
											function edit_row1(no)
											{
													document.getElementById('edit'+no).style.display = 'none';
													document.getElementById('save'+no).style.display = 'block';
												 document.getElementById("program_studi"+no).removeAttribute('readonly');
												document.getElementById("n_ts"+no).removeAttribute('readonly');
													links = document.getElementById("link"+no).href;
													document.getElementById("link"+no).removeAttribute("href");
													document.getElementById("judul_penelitian_dosen"+no).removeAttribute('readonly');
													document.getElementById("nama_dosenketua"+no).removeAttribute('readonly');
													document.getElementById("nama_dosenanggota"+no).removeAttribute('readonly');
												 document.getElementById("tahun"+no).removeAttribute('readonly');
												 document.getElementById("nm_mahasiswa"+no).removeAttribute('readonly');
													document.getElementById("judul_ta_mahasiswa"+no).removeAttribute('readonly');
													document.getElementById("bukti_laporan_akhir"+no).removeAttribute('readonly');
												  document.getElementById("keterangan"+no).removeAttribute('readonly');

												 document.getElementById("program_studi"+no).style.background = "#ccffff";
												 document.getElementById("n_ts"+no).style.background = "#ccffff";
													document.getElementById("judul_penelitian_dosen"+no).style.background = "#ccffff";
													document.getElementById("nama_dosenketua"+no).style.background = "#ccffff";
													document.getElementById("nama_dosenanggota"+no).style.background = "#ccffff";
												 document.getElementById("tahun"+no).style.background = "#ccffff";
												 document.getElementById("nm_mahasiswa"+no).style.background = "#ccffff";
													document.getElementById("judul_ta_mahasiswa"+no).style.background = "#ccffff";
													document.getElementById("bukti_laporan_akhir"+no).style.background = "#ccffff";
												  document.getElementById("keterangan"+no).style.background = "#ccffff";

											}



										</script>
										<script>

											function save_row1(no){
												var program_studi=  document.getElementById("program_studi"+no).value;
												var no_ts=  document.getElementById("n_ts"+no).value;
												var judul_penelitian_dosen= document.getElementById("judul_penelitian_dosen"+no).value;
												var nama_dosenketua= document.getElementById("nama_dosenketua"+no).value;
												var nama_dosenanggota= document.getElementById("nama_dosenanggota"+no).value;
												var nama_mahasiswa=  document.getElementById("nm_mahasiswa"+no).value;
												var judul_ta_mahasiswa= document.getElementById("judul_ta_mahasiswa"+no).value;
												var tahun =  document.getElementById("tahun"+no).value;
												var bukti_laporan_akhir= document.getElementById("bukti_laporan_akhir"+no).value;
													var keterangan= document.getElementById("keterangan"+no).value;
												var id = no;

															$.ajax({

																	type: "POST",
																	url: "<?php echo base_url(); ?>Standar7/penelitiandosentasiupdate",
																	data: {id:id, program_studi:program_studi, no_ts:no_ts, judul_penelitian_dosen:judul_penelitian_dosen, nama_dosenketua:nama_dosenketua, nama_dosenanggota:nama_dosenanggota, nama_mahasiswa:nama_mahasiswa,  judul_ta_mahasiswa:judul_ta_mahasiswa, tahun:tahun, bukti_laporan_akhir:bukti_laporan_akhir,
																	keterangan:keterangan },

																	success: function(data){
																	console.log(data);

																		},

																	error: function(xhr, status, errorThrown){
																	alert( xhr.status);
																	alert( xhr.errorThrown);
																	alert( xhr.responseText);
																	console.log(xhr.responseText);
																	}
															});

																					document.getElementById('edit'+no).style.display = 'block';
																					document.getElementById('save'+no).style.display = 'none';
																					 document.getElementById("program_studi"+no).readOnly =true;
																					 document.getElementById("n_ts"+no).readOnly =true;
																					 links1 = document.getElementById("link"+no).href = links;
						 															document.getElementById("link"+no).setAttribute("href",links1);
																					 document.getElementById("judul_penelitian_dosen"+no).readOnly =true;
																					 document.getElementById("nama_dosenketua"+no).readOnly =true;
																					 document.getElementById("nama_dosenanggota"+no).readOnly =true;
																					 document.getElementById("nm_mahasiswa"+no).readOnly =true;
																					 document.getElementById("tahun"+no).readOnly =true;
																					 document.getElementById("judul_ta_mahasiswa"+no).readOnly =true;
																					 document.getElementById("bukti_laporan_akhir"+no).readOnly =true;
																					 document.getElementById("keterangan"+no).readOnly =true;

																					 document.getElementById("program_studi"+no).style.background = "#f3f3f3";
																					 document.getElementById("n_ts"+no).style.background = "#f3f3f3";
																						document.getElementById("judul_penelitian_dosen"+no).style.background = "#f3f3f3";
																						document.getElementById("nama_dosenketua"+no).style.background = "#f3f3f3";
																						document.getElementById("nama_dosenanggota"+no).style.background = "#f3f3f3";
																					 document.getElementById("tahun"+no).style.background = "#f3f3f3";
																					 document.getElementById("nm_mahasiswa"+no).style.background = "#f3f3f3";
																						document.getElementById("judul_ta_mahasiswa"+no).style.background = "#f3f3f3";
																						document.getElementById("bukti_laporan_akhir"+no).style.background = "#f3f3f3";
																					  document.getElementById("keterangan"+no).style.background = "#f3f3f3";

																				}
										</script>


											<script>
												function edit_row(no) {
													document.getElementById('file'+no).click();
													document.getElementById("save_btn"+no).style.display="block";
												}

												function save_row(no) {
													document.getElementById("edit_btn"+no).style.display="block";
													document.getElementById("save_btn"+no).style.display="none";
												}

												updateList = function(no) {
													var input = document.getElementById('file'+no);
													var output = document.getElementById('nama-file'+no);

													output.innerHTML = '<label>';
													for (var i = 0; i < input.files.length; ++i) {
													output.innerHTML += input.files.item(i).name ;
													}
													output.innerHTML += '</label>';
			}
											</script>
										</tr>
								<?php
									}
								?>
						</tbody>

						<tfoot style="font-size: 60%;" class="table head-peldosen">
							<th><i class="fa fa-check" aria-hidden="true"></i></th>
							<th>no</th>
							<th >Program studi</th>
							<th >	No ts</th>
							<th >ID Peldos</th>
							<th >judul penelitian dosen	</th>
							<th>Judul ta mahasiswa</th>
							<th>Nama mahasiswa</th>
							<th>Nama dosen ketua</th>
							<th>Nama dosen anggota</th>
							<th>tahun</th>
							<th>Bukti laporan akhir</th>
							<th>keterangan</th>
							<th>Doc Pendukung</th>
							<th>Status Valid</th>
							<th>Upload(Bukti)</th>
							<th><i class="fa fa-pencil-square-o" aria-hidden="true"></i></th>
							<?php if($this->session->userdata('hak_akses') == 'admin' ?: $this->session->userdata('hak_akses') == 'kaprodi') { ?>
									<th >Validasi </th>
							<?php } ?>
							<th><i class="fa fa-trash-o" aria-hidden="true"></i></th>
						</tfoot>

				</table>
			</div>
			<div class="delete all-data">
				<button class="btn-delete-list" name="btn-delete-check" type="button" onclick="hapus_checklist()" >Hapus Terpilih</button>
				<?php
					if($this->session->flashdata('msgg')){
						echo "<script> alert('".$this->session->flashdata('msgg')."'); </script>";
					}
				?>
				<script type="text/javascript" language="javascript">
					function hapus_checklist(){

						var selected_value = new Array(); // initialize empty array
						var confirmation = confirm("Apakah anda yakin menghapus data yang anda pilih ?");
						 if(confirmation){
							$(".check:checked").each(function(){
									selected_value.push($(this).val());
								});
								console.log(selected_value);
							$.ajax({
									type: "POST",
									url: "<?php echo base_url(); ?>Standar7/penelitiandosentahapuschecklist",
									data: {result:JSON.stringify(selected_value)},

									success: function(data){
									console.log(data);
									window.location.href='<?php echo base_url(); ?>Standar7/penelitiandosenta';
									},

									error: function(xhr, status, errorThrown){
									alert( xhr.status);
									alert( xhr.errorThrown);
									alert( xhr.responseText);
									console.log(xhr.responseText);
									}
							});
						 }
					}
				</script>

				<button class="btn-delete-all" name="btn-delete-all" onclick="ConfirmDeleteall()"  id="delete-btn" type="submit">Hapus Semua Data</i></button>

				<script type="text/javascript">
						var baseUrl='<?php echo base_url(); ?>standar7/peldostahapussemua';
						function ConfirmDeleteall()
							{
									var msg = 'Apakah anda yakin menghapus semua data Penelitian Dosen (TA Mahasiswa) ?';
									if (confirm(msg)){
										 location.href=baseUrl;
									}
							}
				</script>

			</div>
			<script>
		 	 var acc = document.getElementsByClassName("accordion");
		 	 var i;

		 	 for (i = 0; i < acc.length; i++) {
		 		 acc[i].onclick = function() {
		 		 this.classList.toggle("active");
		 		 var panel = this.nextElementSibling;
		 		 if (panel.style.maxHeight){
		 			 panel.style.maxHeight = null;
		 		 } else {
		 			 panel.style.maxHeight = panel.scrollHeight + "px";
		 		 }
		 		 }
		 	 }
		  </script>
			</div>
		<?php
	} else {

		 ?>
		 <div class="main-content">
		 <div class="-menu-breadcrumb">
			 <ul class="breadcrumb">
				 <li><a href="<?php echo base_url(); ?>dashboard" id="menu-breadcrumb">Dashboard</a></li>
				 <li><a href="<?php echo base_url(); ?>standar7/penelitian" id="menu-breadcrumb">Penelitian</a></li>
				 <li>Penelitian Dosen SI (TA)</li>
			 </ul>
		 </div>
		 <div class="menu-head">
			 <ul>
				 <li><a href="<?php echo base_url(); ?>standar7/penelitian" id="menu-head" style="background:#d9d9d9;color:black;">Penelitian</a></li>
				 <li><a href="<?php echo base_url(); ?>standar7/abdimas" id="menu-head">Abdimas</a></li>
				 <li><a href="<?php echo base_url(); ?>standar7/kerjasama" id="menu-head">Kerjasama</a></li>
			 </ul>
		 </div>

			 <div class="menu-subhead">
			 <ul>
				 <li><a href="<?php echo base_url(); ?>standar7/penelitiandosen" id="menu-subhead" >Penelitian Dosen SI</a></li>
				 <li><a href="<?php echo base_url(); ?>standar7/penelitiandosenta" id="menu-subhead" style="background:#404040;color:white;">Penelitian (TA)</a></li>
				 <li><a href="<?php echo base_url(); ?>standar7/publikasi" id="menu-subhead">Publikasi</a></li>
				 <li><a href="<?php echo base_url(); ?>standar7/haki" id="menu-subhead">HaKI</a></li>
				 <li><a href="<?php echo base_url(); ?>standar7/dosen" id="menu-subhead">Dosen</a></li>
			 </ul>
		 </div>

		 <div class="sub-header">
		 <h2> Standar 7 : Penelitian Dosen Sistem Informasi (Tugas Akhir)</h2>
		 </div>
		 <div class="penelitian_dosen_ta dosen">
			 <div class="peldosta-header dosen-header">
				 <h2>Pilih Cara : </h2>
			 </div>
			 <button class="accordion">Import Data TA Mahasiswa</button>
			 <div class="panel">
				 <div class="panel-input">
				 <br>
				 <h3 class="head-panel">IMPORT DATA TA MAHASISWA</h3>
				 <hr>
				 <br>
			 <!--	<label >Pengupdate Terakhir : <?php

				 if(empty($pengupdate->pengupdate)){

						 echo "";


				 } else {

					 echo $pengupdate->pengupdate;
				 } ?></label></br> -->
				 <label >Download Template Data Ta Mahasiswa : </label>
				 <label style="color:red;font-size:12px;"><a href="<?php echo base_url(); ?>uploads/Format_import_std7/format_data_penelitiandosenta.xlsx" target="_blank">Download File</a></label>
				 <br>
				 <br>
					<form action="<?php echo base_url(); ?>Standar7/importpenelitiansita" method="post" enctype="multipart/form-data">
					 <p class="huruf-upload-files">Files TA Mahasiswa : <input required type="file" name="files1" id="files1" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" /></p>
					 <label style="color:red;font-size:12px;">*File yang dapat di import csv, xls, dan xlxs</label>
					 <input type="hidden" name="datadosen" value="tb_penelitian_dosen_ta">
					 <br>
					 <br>
					 <hr>
					 <button class="upload-btn btn" type="submit" name="btn-upload1">Upload</button>
				 </form>
				 <br>
			 </div>
			 </div>
			 <button class="accordion">Input Data TA Mahasiswa  </button>
			 <div class="panel">
				 <table class="display" width="100%" cellspacing="0">
				 <form  class="data-diri-form" name="data-diri-form-ta"  method="post" action ="<?php echo base_url(); ?>Standar7/penelitiantainsert" enctype="multipart/form-data">

				 <tbody>
						 <tr>
					 <td colspan="3"><h3 class="head-panel">INPUT DATA TA MAHASISWA</h3><hr/><br/></td>
					 </tr>
					 <tr>
						 <td><p class="label">NO TS</p></td>
						 <td><p> : </p></td>
						 <td>
							 <select   name="no_ts" class="data-input-kerjasama">
								<option value="ts">TS</option>
								<option value="ts-1">TS-1</option>
								<option value="ts-2">TS-2</option>
							</select>

						 </td>
					 </tr>
					 <tr>
						<td><p class="label" id="label1">Tahun Ajaran</p></td>
						<td><p> : </p></td>
						<td><input class="data-input-kerjasama" name="tahun_ajrn"   type="text"   placeholder="Judul Penelitian Dosen" required ></td>
					</tr>	<tr>
						 <td><p class="label">NIM</p></td>
						 <td><p> : </p></td>
						 <td><input class="data-input-kerjasama" name="nim"  type="text"  placeholder="NIM" required ></td>
					 </tr><tr>
						 <td><p class="label">Nama Mahasiswa</p></td>
						 <td><p> : </p></td>
						 <td><input class="data-input-kerjasama" name="nama_mahasiswa"  type="text"  placeholder="Nama Mahasiswa" required ></td>
					 </tr>
					<tr>
						 <td><p class="label" id="label1">Kelas</p></td>
						 <td><p> : </p></td>
						 <td><input class="data-input-kerjasama" name="kelas"   type="text"   placeholder="Kelas" required ></td>
					 </tr>

					 <tr>
						 <td><p class="label">Status</p></td>
						 <td><p> : </p></td>
						 <td><input class="data-input-kerjasama" name="status"  type="text"  placeholder="Status" required ></td>
					 </tr>
					 <tr>
						 <td><p class="label">Tanggal Input</p></td>
						 <td><p> : </p></td>
						 <td><input class="data-input-kerjasama" name="input_date"  type="text"  placeholder="Tanggal Input" required ></td>
					 </tr>

					 <tr>
						 <td><p class="label">Pembimbing Akademik</p></td>
						 <td><p> : </p></td>
						 <td><input class="data-input-kerjasama" name="pembimbing_akdmk"  type="text"  placeholder="Pembimbing Akademik" required ></td>
					 </tr>


						 <tr>
							 <td><p class="label">Kode Dosen</p></td>
							 <td><p> : </p></td>
							 <td><input class="data-input-kerjasama" name="kode_dosen"  type="text"  placeholder="Kode Dosen" required ></td>
						 </tr>


					 <tr>
					 <td colspan="3"><br/><hr/><input class="data-diri-button btn" id="data-diri-button1" name="simpanbtn1" title="simpan" type="submit" value="Simpan" ><br/></td>
					 </tr>
				 </tbody>

				 </form>


			 </table>
			 </div>
			 <button class="accordion">Data TA Mahasiswa  </button>
			 <div class="panel">
			 </br>
			 <div class="data-kerjasama">

			 <table id="table-view-data-pelta" class="display" width="100%" cellspacing="0">

						 <thead style="font-size: 60%;" class="table head-peldosen">
							 <th>#</th>
							 <th>no</th>
							 <th >	No TS</th>
							 <th >Tahun Ajaran</th>
							 <th>NIM</th>
							 <th>Nama mahasiswa</th>
							 <th>Kelas</th>
							 <th>Status</th>
							 <th>Tanggal Input</th>
							 <th>Pembimbing Akademik</th>
							 <th><i class="fa fa-pencil-square-o" aria-hidden="true"></i></th>
							 <th><i class="fa fa-trash-o" aria-hidden="true"></i></th>
						 </thead>

						 <tbody class="table body-peldosen" role="alert" aria-live="polite" aria-relevant="all">

							 <?php
									 $no = 1;

									 foreach($result1 as $row){

									 ?>
										 <tr id="row:<?php echo $row->no ?>">
										 <td ><input type="checkbox" name="pilih[]" class="check" value="<?php echo $row->no ?>" ></td>
											 <td ><?php echo $no++ ?></td>
											 <td ><textarea class="data-diri-input" id="no_ts<?php echo $row->no ?>"  type="text"  name="no_ts"   placeholder="" readonly /><?php echo $row->no_ts ?></textarea></td>
											 <td><textarea class="data-diri-input" id="tahun_ajrn<?php echo $row->no ?>"  type="text" name="tahun_ajrn"  placeholder="" readonly /><?php echo $row->tahun_ajrn ?></textarea></td>
											 <td><textarea class="data-diri-input" id="nim<?php echo $row->no ?>" name="nim"  type="text"  placeholder="" readonly /><?php echo $row->nim ?></textarea></td>
											 <td><textarea class="data-diri-input" id="nama_mahasiswa<?php echo $row->no ?>"  type="text" name="nama_mahasiswa" placeholder="" readonly /><?php echo $row->nama_mahasiswa?></textarea></td>
											 <td><textarea class="data-diri-input" id="kelas<?php echo $row->no ?>"  type="text" name="kelas"  placeholder="" readonly /><?php echo $row->kelas ?></textarea></td>
											 <td ><textarea class="data-diri-input" id="status<?php echo $row->no ?>" name="status"   type="text"  placeholder="" readonly /><?php echo $row->status ?></textarea></td>
											 <td ><textarea class="data-diri-input" id="input_date<?php echo $row->no ?>" name="input_date"  type="text"  placeholder="" readonly /><?php echo $row->input_date ?></textarea></td>
											 <td ><textarea class="data-diri-input" id="pembimbing_akdmk<?php echo $row->no ?>"  type="text" name="pembimbing_akdmk"  placeholder="" readonly /><?php echo $row->pembimbing_akdmk ?></textarea></td>

											 <td><button onclick="edit_row2('<?php echo $row->no ?>')"  id="editt<?php echo $row->no ?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
											 <button  onclick="save_row2('<?php echo $row->no ?>')"  id="savee<?php echo $row->no ?>" style=" visibility: hidden;" /><i class="fa fa-floppy-o" aria-hidden="true"></i></button>
											 </td>
											 <td>
											 <form action="<?php echo base_url(); ?>standar7/penelitiantahapus/<?php echo $row->no ?>" method="post" enctype="multipart/form-data">
											 <input type="hidden" value="<?php echo $row->no?>" name="data-id" >
											 <button name="btn-delete" onclick="return ConfirmDelete(<?php echo $row->no ?>)"  id="delete-btnn<?php echo $row->no ?>" type="submit"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
											 </form>
												 <script type="text/javascript">
														 var baseUrl='<?php echo base_url(); ?>standar7/penelitiantahapus/<?php echo $row->no ?>';
														 var msgg = 'Apakah anda yakin untuk menghapus data ini ?';
														 function ConfirmDelete(no)
														 {
														 if (confirm(msgg)) {
															 location.href=baseUrl;
														 }
														 else {
																return false;
														 }
														 }
												 </script>
											 </td>


														 <script>
										 var no;
											 function edit_row2(no)
											 {
													 document.getElementById('editt'+no).style.visibility = 'hidden';
													 document.getElementById('savee'+no).style.visibility = 'visible';

													document.getElementById("no_ts"+no).removeAttribute('readonly');
													 document.getElementById("tahun_ajrn"+no).removeAttribute('readonly');
													document.getElementById("nim"+no).removeAttribute('readonly');
													document.getElementById("nama_mahasiswa"+no).removeAttribute('readonly');
													 document.getElementById("kelas"+no).removeAttribute('readonly');
														document.getElementById("status"+no).removeAttribute('readonly');
														document.getElementById("input_date"+no).removeAttribute('readonly');
														document.getElementById("pembimbing_akdmk"+no).removeAttribute('readonly');
														document.getElementById("kode_dosen"+no).removeAttribute('readonly');

											 }



										 </script>
										 <script>

											 function save_row2(no){

												 var no_ts=  document.getElementById("no_ts"+no).value;
												 var tahun_ajrn= document.getElementById("tahun_ajrn"+no).value;
												 var nim= document.getElementById("nim"+no).value;
												 var nama_mahasiswa=  document.getElementById("nama_mahasiswa"+no).value;
												 var kelas =  document.getElementById("kelas"+no).value;
												 var status= document.getElementById("status"+no).value;
												 var input_date=  document.getElementById("input_date"+no).value;
												 var pembimbing_akdmk=  document.getElementById("pembimbing_akdmk"+no).value;
												 var kode_dosen=  document.getElementById("kode_dosen"+no).value;
												 var id = no;

															 $.ajax({

																	 type: "POST",
																	 url: "<?php echo base_url(); ?>Standar7/penelitiantasiupdate",
																	 data: {id:id, no_ts:no_ts, tahun_ajrn:tahun_ajrn, nim:nim, nama_mahasiswa:nama_mahasiswa,  kelas:kelas, status,
																	 input_date:input_date, pembimbing_akdmk:pembimbing_akdmk, kode_dosen:kode_dosen },

																	 success: function(data){
																	 console.log(data);

																		 },

																	 error: function(xhr, status, errorThrown){
																	 alert( xhr.status);
																	 alert( xhr.errorThrown);
																	 alert( xhr.responseText);
																	 console.log(xhr.responseText);
																	 }
															 });

																					 document.getElementById('editt'+no).style.visibility = 'visible';
																					 document.getElementById('savee'+no).style.visibility = 'hidden';
																						document.getElementById("no_ts"+no).readOnly =true;
																						 document.getElementById("tahun_ajrn"+no).readOnly =true;
																						document.getElementById("nim"+no).readOnly =true;
																						document.getElementById("nama_mahasiswa"+no).readOnly =true;
																						 document.getElementById("kelas"+no).readOnly =true;
																							document.getElementById("status"+no).readOnly =true;
																							document.getElementById("input_date"+no).readOnly =true;
																							document.getElementById("pembimbing_akdmk"+no).readOnly =true;
																							document.getElementById("kode_dosen"+no).readOnly =true;

																				 }
										 </script>



										 </tr>
								 <?php
									 }
								 ?>
						 </tbody>

						 <tfoot style="font-size: 60%;" class="table head-peldosen">
							 <th><i class="fa fa-check" aria-hidden="true"></i></th>
							 <th>no</th>
							 <th >	No TS</th>
							 <th >Tahun Ajaran</th>
							 <th>NIM</th>
							 <th>Nama mahasiswa</th>
							 <th>Kelas</th>
							 <th>Status</th>
							 <th>Tanggal Input</th>
							 <th>Pembimbing Akademik</th>
							 <th><i class="fa fa-pencil-square-o" aria-hidden="true"></i></th>
							 <th><i class="fa fa-trash-o" aria-hidden="true"></i></th>
						 </tfoot>

				 </table>
			 </div>
			 <div class="delete all-data">
				 <button class="btn-delete-list" name="btn-delete-check1" type="button" onclick="hapus_checklist1()" >Hapus Terpilih</button>
				 <?php
					 if($this->session->flashdata('msgg')){
						 echo "<script> alert('".$this->session->flashdata('msgg')."'); </script>";
					 }
				 ?>
				 <script type="text/javascript" language="javascript">
					 function hapus_checklist1(){

						 var selected_value = new Array(); // initialize empty array
						 var confirmation = confirm("Apakah anda yakin menghapus data yang anda pilih ?");
							if(confirmation){
							 $(".check:checked").each(function(){
									 selected_value.push($(this).val());
								 });
								 console.log(selected_value);
							 $.ajax({
									 type: "POST",
									 url: "<?php echo base_url(); ?>Standar7/penelitiantahapuschecklist",
									 data: {result:JSON.stringify(selected_value)},

									 success: function(data){
									 console.log(data);
									 window.location.href='<?php echo base_url(); ?>Standar7/penelitiandosenta';
									 },

									 error: function(xhr, status, errorThrown){
									 alert( xhr.status);
									 alert( xhr.errorThrown);
									 alert( xhr.responseText);
									 console.log(xhr.responseText);
									 }
							 });
							}
					 }
				 </script>

				 <button class="btn-delete-all" name="btn-delete-all1" onclick="ConfirmDeleteall1()"  id="delete-btn1" type="submit">Hapus Semua Data</i></button>

						 <script type="text/javascript">
								 var baseUrl='<?php echo base_url(); ?>standar7/peltahapussemua';
								 function ConfirmDeleteall1()
									 {
											 var msg = 'Apakah anda yakin menghapus semua data TA Mahasiswa ?';
											 if (confirm(msg)){
													location.href=baseUrl;
											 }
									 }
						 </script>
					 </div>
				 </br>
			 </div>

			 <div class="peldosta-header dosen-header">
				 <h2>Pilih Cara : </h2>
			 </div>
			 <button class="accordion">Import Data Penelitian Dosen (TA Mahasiswa)</button>
			 <div class="panel">
			 </br>
						 <label style="font-size:80%; color:red;">Maaf tidak bisa melakukan import data, karena bukan Anggota tim pengumpul data standar 7</label>
			 </br>
			 </br>
			 </div>
			 <button class="accordion">Input Data Penelitian Dosen (TA Mahasiswa)</button>
			 <div class="panel">
			 </br>
						 <label style="font-size:80%; color:red;">Maaf tidak bisa melakukan input data, karena bukan Anggota tim pengumpul data standar 7</label>
			 </br>
			 </br>
			 </div>
		 </div>

		 <div class="data-kerjasama">
		 <table id="table-view-data-tim-akreditasi" class="display" width="100%" cellspacing="0">

					 <thead style="font-size: 60%;" class="table head-peldosen">
						 <th>No</th>
						 <th >Program studi</th>
						 <th >No ts</th>
						 <th >judul penelitian dosen	</th>
						 <th>tahun</th>
						 <th>Nama mahasiswa</th>
						 <th>Judul ta mahasiswa</th>
						 <th>Bukti laporan akhir</th>
						 <th>keterangan</th>
						 <th>Doc Pendukung</th>
					 </thead>

					 <tbody class="table body-peldosen" role="alert" aria-live="polite" aria-relevant="all">

						 <?php
								 $no = 1;
								 foreach($result as $row){

								 ?>
									 <tr id="row:<?php echo $row->no ?>">
										 <td ><?php echo $no++ ?></td>
										 <td ><textarea class="data-diri-input" id="program_studi<?php echo $row->no ?>"  type="text" name="nama_instansi" placeholder="" readonly /><?php echo $row->prodi ?></textarea></td>
										 <td><textarea class="data-diri-input" id="no_ts<?php echo $row->no ?>"  type="text" name="jenis_kegiatan" placeholder="" readonly /><?php echo $row->no_ts ?></textarea></td>
										 <td><textarea class="data-diri-input" id="judul_penelitian_dosen<?php echo $row->no ?>"  type="text" name="mulai-kerjasama" placeholder="" readonly /><?php echo $row->judul_penelitian_dosen ?></textarea></td>
										 <td><textarea class="data-diri-input" id="tahun<?php echo $row->no ?>"  type="text" name="akhir-kerjasama" placeholder="" readonly /><?php echo $row->tahun?></textarea></td>
										 <td><textarea class="data-diri-input" id="nama_mahasiswa<?php echo $row->no ?>"  type="text" name="instansi-negeri" placeholder="" readonly /><?php echo $row->nama_mahasiswa ?></textarea></td>
										 <td ><textarea class="data-diri-input" id="judul_ta_mahasiswa<?php echo $row->no ?>"  type="text" name="keterangan" placeholder="" readonly /><?php echo $row->judul_ta_mahasiswa ?></textarea></td>
										 <td ><textarea class="data-diri-input" id="bukti_laporan_akhir<?php echo $row->no ?>"  type="text" name="keterangan" placeholder="" readonly /><?php echo $row->bukti_laporan_akhir ?> </textarea></td>
										 <td ><textarea class="data-diri-input" id="keterangan<?php echo $row->no ?>"  type="text" name="keterangan" placeholder="" readonly /><?php echo $row->keterangan ?></textarea></td>

										 <td ><div class="lots">
															 <p><a target="_blank" href="<?php echo base_url(); ?>uploads/dokumen_kerjasama_std7/<?php echo $row->file_name ?>"><?php echo $row->file_name ?> </a>...</p>

										 </div></td>


									 </tr>
							 <?php
								 }
							 ?>
					 </tbody>

					 <tfoot style="font-size: 60%;" class="table head-peldosen">
						 <th>No</th>
						 <th >Program studi</th>
						 <th >No ts</th>
						 <th >judul penelitian dosen	</th>
						 <th>tahun</th>
						 <th>Nama mahasiswa</th>
						 <th>Judul ta mahasiswa</th>
						 <th>Bukti laporan akhir</th>
						 <th>keterangan</th>
						 <th>Doc Pendukung</th>
					 </tfoot>

			 </table>
		 </div>


		 </div>

		 <script>
			 var acc = document.getElementsByClassName("accordion");
			 var i;

			 for (i = 0; i < acc.length; i++) {
				 acc[i].onclick = function() {
				 this.classList.toggle("active");
				 var panel = this.nextElementSibling;
				 if (panel.style.maxHeight){
					 panel.style.maxHeight = null;
				 } else {
					 panel.style.maxHeight = panel.scrollHeight + "px";
				 }
				 }
			 }
		 </script>
		 </div>
		 <?php
	 }
		 ?>

		</div>

		<div class="footer">
			<?php
			include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/footer.php";
			?>
		</div>

	</body>



</html>
