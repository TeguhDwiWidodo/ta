<?php

require $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/Classes/PHPExcel/IOFactory.php";

if(isset($_POST['btn-upload'])){  
	if(isset($_FILES['files'])){
		foreach($_FILES['files']['tmp_name'] as $key => $tmp_name) {
			$table = $_POST['peldosen'];
			$inputfilename = $_FILES['files']['name'][$key];
			$exceldata = array();
			$folder= "uploads/dokumenstandar7/"; 
			if(move_uploaded_file($tmp_name,$folder.$inputfilename)) {
				try {
					$inputfiletype = PHPExcel_IOFactory::identify($folder.$inputfilename);
					$objReader = PHPExcel_IOFactory::createReader($inputfiletype);
					$objPHPExcel = $objReader->load($folder.$inputfilename);
				} catch(Exception $e) {
					die('Error loading file "'.pathinfo($folder.$inputfilename,PATHINFO_BASENAME).'": '.$e->getMessage());
				}
				$sheet = $objPHPExcel->getSheet(0); 
				$highestRow = $sheet->getHighestRow(); 
				$highestColumn = $sheet->getHighestColumn();
				$headings = $sheet->rangeToArray('A1:' . $highestColumn . 1, NULL, TRUE, FALSE);
				for ($row = 2; $row <= $highestRow; $row++){ 
					//  Read a row of data into an array
					$rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);			
						$sql = "INSERT INTO $table (`no`, `program_studi`, `no_ts`, `tipe`, `judul`, `tahun`, `nip`, `nama_dosenketua`, `nama_dosenanggota_1`, `nama_dosenanggota_2`, `nama_dosenanggota_3`, `nama_dosenanggota_4`, `nama_dosenanggota_5`, `nama_mahasiswa_1`, `nama_mahasiswa_2`, `nama_mahasiswa_3`, `nama_mahasiswa_4`, `nama_mahasiswa_5`, `anggaran`, `keterangan`) VALUES('".$rowData[0][0]."', '".$rowData[0][1]."', '".$rowData[0][2]."', '".$rowData[0][3]."' , '".$rowData[0][4]."', '".$rowData[0][5]."', '".$rowData[0][6]."' , '".$rowData[0][7]."', '".$rowData[0][8]."', '".$rowData[0][9]."' , '".$rowData[0][10]."', '".$rowData[0][11]."', '".$rowData[0][12]."' , '".$rowData[0][13]."' , '".$rowData[0][14]."' , '".$rowData[0][15]."', '".$rowData[0][16]."' , '".$rowData[0][17]."' , '".$rowData[0][18]."' , '".$rowData[0][19]."' )
						ON DUPLICATE KEY UPDATE no='".$rowData[0][0]."', program_studi='".$rowData[0][1]."',no_ts='".$rowData[0][2]."',tipe='".$rowData[0][3]."',judul='".$rowData[0][4]."',tahun='".$rowData[0][5]."',nip='".$rowData[0][6]."',nama_dosenketua='".$rowData[0][7]."',nama_dosenanggota_1='".$rowData[0][8]."',nama_dosenanggota_2='".$rowData[0][9]."',nama_dosenanggota_3='".$rowData[0][10]."',nama_dosenanggota_4='".$rowData[0][11]."',nama_dosenanggota_5='".$rowData[0][12]."',nama_mahasiswa_1='".$rowData[0][13]."',nama_mahasiswa_2='".$rowData[0][14]."',nama_mahasiswa_3='".$rowData[0][15]."',nama_mahasiswa_4='".$rowData[0][16]."',nama_mahasiswa_5='".$rowData[0][17]."',anggaran='".$rowData[0][18]."',keterangan='".$rowData[0][19]."';";
					
					$this->db->query($sql);
				?>
					<script>
						alert('successfully uploaded');
						window.location.href='../standar7/penelitiandosen?success';
					</script>
				<?php
				}
			} else {
				?>
				<script>
					alert('error while uploading file');
					window.location.href='../standar7/penelitiandosen?fail';
				</script>
				<?php
			}
			
			
			
		}
	} else {
		echo "file tidak ada";
	}
}