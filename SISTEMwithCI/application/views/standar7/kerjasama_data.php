<?php
if(!isset($_SESSION['nama'])){
      header("location:" . base_url());
      exit();
   }
?>

<!DOCTYPE html>
<html>
	<head>
		<title>Akreditasi | Kerjasama Data</title>
		<script type = 'text/javascript' src="<?php echo base_url(); ?>js/jquery-1.12.4.js"></script>

		<link rel="stylesheet" href="<?php echo base_url(); ?>css/mainlayout.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/penelitian.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/dosen.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/accordion.css">
		<script type = 'text/javascript' src="<?php echo base_url(); ?>js/jquery.dataTables.js"></script>
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/jquery.dataTables.css">
		<script type = 'text/javascript' >
			(function ($){
				$(document).ready(function() {
					$('#table-view-data-kerjasama').dataTable({
						"order": [[ 1, 'asc' ]],
            "aLengthMenu": [5, 10, 25],
						iDisplayLength: 5,
              "scrollX": true
					});
				});
			})(jQuery);
		</script>
    <script type = 'text/javascript' >
			(function ($){
				$(document).ready(function() {
					$('#table-view-data-kerjasama1').dataTable({
						"order": [[ 0, 'asc' ]],
            "aLengthMenu": [5, 10, 25],
						iDisplayLength: 5,
              "scrollX": true
					});
				});
			})(jQuery);
		</script>

	</head>

	<body>
		<div class="header">
			<h1><a href="<?php echo base_url(); ?>dashboard">Akreditasi SI</a></h1>
			<?php
				include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/header.php";
			?>
		</div>

		<div class="sidebar">
			<?php
				include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/sidebar.php";
			?>
		</div>

		<div class="main-layout">
		<?php
			if ( $this->session->userdata('hak_akses') == 'admin' ?: $this->session->userdata('hak_akses') == 'standar 7' ?: $this->session->userdata('hak_akses') == 'kaprodi'){
			?>

			<div class="main-content">
				<div class="menu-breadcrumb">
					<ul class="breadcrumb">
						<li><a href="<?php echo base_url(); ?>dashboard" id="menu-breadcrumb">Dashboard</a></li>
						<li><a href="<?php echo base_url(); ?>standar7/kerjasama" id="menu-breadcrumb">Kerjasama</a></li>
						<li>Kerjasama Data</li>
					</ul>
				</div>
				<div class="menu-head">
					<ul>
						<li><a href="<?php echo base_url(); ?>standar7/penelitian" id="menu-head" >Penelitian</a></li>
						<li><a href="<?php echo base_url(); ?>standar7/abdimas" id="menu-head" >Pengmas</a></li>
						<li><a href="<?php echo base_url(); ?>standar7/kerjasama" id="menu-head" style="background:#d9d9d9;color:black;">Kerjasama</a></li>
					</ul>
				</div>

					<div class="menu-subhead">
					<ul>
						<li><a href="<?php echo base_url(); ?>standar7/kerjasamadata" id="menu-subhead" style="background:#404040;color:white;">Data Kerjasama</a></li>
					</ul>
				</div>
				<div class="sub-header">
				<h2> Standar 7 : Data Kerjasama</h2>
				</div>
					<div class="kerjasama dosen">
						<div class="krjsama-header dosen-header">
							<h2>Pilih Cara : </h2>
						</div>
						<button class="accordion">Import Data Kerjasama</button>
						<div class="panel">
						 <div class="panel-input">
							<br/>
							<h3 class="head-panel">IMPORT DATA KERJASAMA</h3>
							<hr/>
							<br>
            <!--  <label >Pengupdate terakhir : <?php if(empty($pengupdate->pengupdate)){

                  echo "";


							} else {

								echo $pengupdate->pengupdate;
							} ?> </label></br> -->
							<label >Download Template Data Kerjasama : </label>
							<label style="color:red;font-size:12px;"><a href="<?php echo base_url(); ?>uploads/format_import_std7/format_data_kerjasama.xlsx" target="_blank">Download File</a></label>
							<br>
						  <br>
						   <form action="<?php echo base_url();  ?>Standar7/kerjasamaimport" onsubmit="return Validate(this);" method="post" enctype="multipart/form-data">
								<p class="huruf-upload-files">Files: <input required type="file" name="files" id="files" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" /></p>
								<label style="color:red;font-size:12px;">*File yang dapat di import csv, xls, dan xlxs</label>
								<input type="hidden" name="kerjasama" value="tb_kerjasama">
								<br/>
								<br/>
								<hr/>
								<button class="btn" type="submit" name="btn-upload">Upload</button>
							</form>
							<?php
								if($this->session->flashdata('msg')){
									echo "<script> alert('".$this->session->flashdata('msg')."'); </script>";
								}
							?>
						  <br>
						  <script type="text/javascript" language="javascript">
								var _validFileExtensions = [".xlsx", ".xls", ".csv"];
									function Validate(oForm) {
										var arrInputs = oForm.getElementsByTagName("input");
										for (var i = 0; i < arrInputs.length; i++) {
											var oInput = arrInputs[i];
											if (oInput.type == "file") {
												var sFileName = oInput.value;
												if (sFileName.length > 0) {
													var blnValid = false;
													for (var j = 0; j < _validFileExtensions.length; j++) {
														var sCurExtension = _validFileExtensions[j];
														if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
															blnValid = true;
															break;
														}
													}

													if (!blnValid) {
														alert("Sorry, " + sFileName + " is invalid, allowed extensions are: " + _validFileExtensions.join(", "));
														return false;
													}
												}
											}
										}

										return true;
									}
							</script>
							</div>
						</div>
						<button class="accordion">Input Data Kerjasama</button>
						<div class="panel">

						<?php
						if ($this->session->flashdata('msga')){

						echo "		<script>

						alert('". $this->session->flashdata('msga')."');

						</script>";

						}

						?>

							<table class="display" width="100%" cellspacing="0">
								<form class="data-diri-form" name="data-diri-form" action="<?php echo base_url(); ?>Standar7/kerjasamainsert" method="post" enctype="multipart/form-data">
								<tbody>
								    <tr>
									<td colspan="3"><h3 class="head-panel">INPUT DATA KERJASAMA</h3><hr/><br/></td>
									</tr>
									<tr>
										<td><p class="label">Nama Instansi</p></td>
										<td><p> : </p></td>
										<td><input class="data-input-kerjasama" id="data-diri-nama-instantsi"  type="text"  name="nama-instansi" placeholder="Nama Instansi" required ></td>
									</tr>
									<tr>
										<td><p class="label" id="label1">Jenis Kegiatan</p></td>
										<td><p id="label1"> : </p></td>
										<td><textarea style="height:60px;" class="data-input-kerjasama" id="data-jenis-kegiatan"  type="textarea"  name="jenis-kegiatan" placeholder="Jenis Kegiatan" required></textarea></td>
									</tr>
									<tr>
										<td><p class="label">Mulai Kegiatan</p></td>
										<td><p> : </p></td>
										<td><input class="data-input-kerjasama" id="datepicker1"  type="date"  name="mulaikegiatan" placeholder="DD-M-YYYY" ></td>
									</tr>
                                    <tr>
										<td><p class="label">Akhir Kegiatan</p></td>
										<td><p> : </p></td>
										<td><input class="data-input-kerjasama" id="datepicker"  type="date"  name="akhirkegiatan" placeholder="DD-M-YYYY" ></td>
									</tr>

									<tr>

										<td ><br/><p class="label">Instansi Negeri</p><br/></td>
										<td ><br/><p> : </p><br/></td>
										<td>
												<span  class="data-radio-label"><input class="data-radio-kerjasama" id="data-diri-instansi-negeri"  type="radio"  name="instansinegeri" value="Dalam Negeri" required></input>Dalam Negeri</span>
                      </br>
                        <span  class="data-radio-label"><input class="data-radio-kerjasama" id="data-diri-instansi-negeri"  type="radio"  name="instansinegeri" value="Luar Negeri" required></input>Luar Negeri</span>

                    </td>
									</tr>
									 <tr>
										<td><p class="label">Keterangan</p></td>
										<td><p> : </p></td>
										<td><input class="data-input-kerjasama" id="data-diri-ket"  type="text"  name="ket" placeholder="Keterangan" ></td>
									</tr>
                  <tr>
                   <td><p class="label" id="label1">Manfaat yang Diperoleh</p></td>
                   <td><p id="label1"> : </p></td>
                   <td><textarea class="data-input-kerjasama" id="data-diri-manfaat" style="height:60px;"  type="text"  name="manfaat" placeholder="manfaat" ></textarea></td>
                 </tr>
									<tr>
									<td colspan="3"><br/><hr/><input class="data-diri-button btn" id="data-diri-button" name="simpanbtn" title="simpan" type="submit" value="Simpan" ><br/></td>
									</tr>
								</tbody>

								</form>
							</table>
						</div>
					</div>
					<script>
						var acc = document.getElementsByClassName("accordion");
						var i;

						for (i = 0; i < acc.length; i++) {
						  acc[i].onclick = function() {
							this.classList.toggle("active");
							var panel = this.nextElementSibling;
							if (panel.style.maxHeight){
							  panel.style.maxHeight = null;
							} else {
							  panel.style.maxHeight = panel.scrollHeight + "px";
							}
						  }
						}
					</script>
			</div>
			<div class="data-kerjasama">
			<table id="table-view-data-kerjasama" class="display" width="100%" cellspacing="0">

						<thead style="font-size: 60%;" class="table head-peldosen">
							<th>#</th>
							<th>no</th>
							<th >Nama Instansi</th>
							<th >Jenis Kegiatan</th>
							<th >Mulai Kerjasama</th>
							<th>Akhir Kerjasama</th>
							<th>Instansi Negeri</th>
							<th>Ket</th>
              <th>Manfaat yang Diperoleh</th>
							<th>Doc Pendukung(Bukti)</th>
							<th>Upload(Bukti)</th>
              <th><i class="fa fa-pencil-square-o" aria-hidden="true"></i></th>
							<th><i class="fa fa-trash-o" aria-hidden="true"></i></th>
						</thead>

						<tbody class="table body-peldosen" role="alert" aria-live="polite" aria-relevant="all">

							<?php
									$no = 1;
									foreach($result as $row){

									?>
										<tr id="row:<?php echo $row->id ?>">
										<td ><input type="checkbox" name="pilih[]" class="check" value="<?php echo $row->id ?>" ></td>
											<td ><?php echo $no++ ?></td>
											<td ><textarea class="data-diri-input" title="<?php echo $row->nama_instansi ?>" id="data-diri-nama-instansi<?php echo $row->id ?>"  type="text" name="nama_instansi" placeholder="" readonly /><?php echo $row->nama_instansi ?></textarea></td>
											<td><textarea class="data-diri-input" title="<?php echo $row->jenis_kegiatan ?>" id="data-diri-jenis-kegiatan<?php echo $row->id ?>"  type="text" name="jenis_kegiatan" placeholder="" readonly /><?php echo $row->jenis_kegiatan ?></textarea></td>
											<td><textarea class="data-diri-input" title="<?php echo $row->mulai_kerjasama ?>" id="data-diri-mulai-kerjasama<?php echo $row->id ?>"  type="text" name="mulai-kerjasama" placeholder="" readonly /><?php echo $row->mulai_kerjasama ?></textarea></td>
											<td><textarea class="data-diri-input" title="<?php echo $row->akhir_kerjasama ?>" id="data-diri-akhir-kerjasama<?php echo $row->id ?>"  type="text" name="akhir-kerjasama" placeholder="" readonly /><?php echo $row->akhir_kerjasama ?></textarea></td>
											<td><textarea class="data-diri-input" title="<?php echo $row->instasi_negeri ?>" id="data-diri-instansi-negeri<?php echo $row->id ?>"  type="text" name="instansi-negeri" placeholder="" readonly /><?php echo $row->instasi_negeri ?></textarea></td>
											<td ><textarea class="data-diri-input" title="<?php echo $row->keterangan?>" id="data-diri-keterangan<?php echo $row->id ?>"  type="text" name="keterangan" placeholder="" readonly /><?php echo $row->keterangan ?></textarea></td>
                      <td ><textarea class="data-diri-input" title="<?php echo $row->manfaat ?>" id="data-diri-manfaat<?php echo $row->id ?>"  type="text" name="manfaat" placeholder="" readonly /><?php echo $row->manfaat ?></textarea></td>

                      <td ><div class="lots">
																<p><a target="_blank" title="Uploaded : [<?php echo $row->file_date ?>], <?php echo $row->nama_instansi ?>,<?php echo $row->file_name ?>" href="<?php echo base_url(); ?>uploads/dokumen_kerjasama_std7/<?php echo $row->file_name ?>"><?php echo $row->file_name ?> </a>...</p>

											</div></td>

											<td >


											<form onsubmit="return Validate1(this);" action="<?php echo base_url(); ?>Standar7/kerjasamauploadpdf/<?php echo $row->id ?>" method="post" enctype="multipart/form-data">
												<input class="aa" style="font-size:9px;display: none;" id="file<?php echo $row->id ?>" required type="file" name="files" onchange="javascript:updateList('<?php echo $row->id ?>')" accept="application/pdf" />
												<input type="hidden" value="<?php echo $row->id ?>" name="data-id" >
                        <input type="hidden" value="<?php echo $row->file_name ?>" name="data-filename" >
												<div class="lots" id="nama-file<?php echo $row->id ?>"></div>
												<br>
												<button  class="tombol-kelola" style="display: block;" title="Pilih Doc Pendukung : <?php echo $row->nama_instansi ?>, <?php echo $row->jenis_kegiatan ?>" type="button" onclick="edit_row('<?php echo $row->id ?>')" id="edit_btn<?php echo $row->id ?>" class="edit" value="edit"><i class="fa fa-file" aria-hidden="true"></i></button>
												<br>
												<button  class="tombol-kelola" style="display: none;" title="Upload : <?php echo $row->nama_instansi ?>, <?php echo $row->jenis_kegiatan ?>" name="btn-upload" type="submit"  onclick="save_row('<?php echo $row->id ?>')" id="save_btn<?php echo $row->id ?>" class="save" value="save"><i class="fa fa-floppy-o" aria-hidden="true"></i></button>
												</form>
												<script type="text/javascript" language="javascript">
													var _validFileExtensions1 = [".pdf"];
														function Validate1(oForm) {
															var arrInputs = oForm.getElementsByTagName("input");
															for (var i = 0; i < arrInputs.length; i++) {
																var oInput = arrInputs[i];
																if (oInput.type == "file") {
																	var sFileName = oInput.value;
																	if (sFileName.length > 0) {
																		var blnValid = false;
																		for (var j = 0; j < _validFileExtensions1.length; j++) {
																			var sCurExtension = _validFileExtensions1[j];
																			if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
																				blnValid = true;
																				break;
																			}
																		}

																		if (!blnValid) {
																			alert("Sorry, " + sFileName + " is invalid, allowed extensions are: " + _validFileExtensions1.join(", "));
																			return false;
																		}
																	}
																}
															}

															return true;
														}
												</script>
											</td>
											<td>
                      <button  class="tombol-kelola" title="Ubah : <?php echo $row->nama_instansi ?>, <?php echo $row->jenis_kegiatan ?>" onclick="edit_row1('<?php echo $row->id ?>')"  id="edit<?php echo $row->id ?>" ><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
                      <button  class="tombol-kelola"  title="simpan : <?php echo $row->nama_instansi ?>, <?php echo $row->jenis_kegiatan ?>" onclick="save_row1('<?php echo $row->id ?>')"  id="save<?php echo $row->id ?>" style="display:none;" >
                        <i class="fa fa-floppy-o" aria-hidden="true"></i></button>
                      </td>
											<td>
											<form action="<?php echo base_url(); ?>standar7/kerjasamahapus/<?php echo $row->id ?>" method="post" enctype="multipart/form-data">
											<input type="hidden" value="<?php echo $row->id?>" name="data-id" >
                      	<input type="hidden" value="<?php echo $row->file_name ?>" name="data-filename" >

											<button class="tombol-kelola"  title="Hapus : <?php echo $row->nama_instansi ?>, <?php echo $row->jenis_kegiatan ?>" name="btn-delete" onclick="return ConfirmDelete(<?php echo $row->id ?>)"  id="delete-btn<?php echo $row->id ?>" type="submit"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
											</form>
												<script type="text/javascript">
													  var baseUrl='<?php echo base_url(); ?>standar7/kerjasamahapus/<?php echo $row->id ?>';

													  function ConfirmDelete(no)
													  {
                              	  var msgg = 'Apakah anda yakin untuk menghapus data ini ? '+no;
														if (confirm(msgg)) {
															location.href=baseUrl;
														}
														else {
															 return false;
														}
													  }
												</script>
											</td>


														<script>
										var no;
											function edit_row1(no)
											{
												  document.getElementById('edit'+no).style.display='none';
												  document.getElementById('save'+no).style.display='block';
												 document.getElementById("data-diri-nama-instansi"+no).removeAttribute('readonly');
												 document.getElementById("data-diri-jenis-kegiatan"+no).removeAttribute('readonly');
												  document.getElementById("data-diri-mulai-kerjasama"+no).removeAttribute('readonly');
												 document.getElementById("data-diri-akhir-kerjasama"+no).removeAttribute('readonly');
												 document.getElementById("data-diri-instansi-negeri"+no).removeAttribute('readonly');
												  document.getElementById("data-diri-keterangan"+no).removeAttribute('readonly');
                          document.getElementById("data-diri-manfaat"+no).removeAttribute('readonly');

                          document.getElementById("data-diri-nama-instansi"+no).style.background = "#ccffff";
 												 document.getElementById("data-diri-jenis-kegiatan"+no).style.background = "#ccffff";
 												  document.getElementById("data-diri-mulai-kerjasama"+no).style.background = "#ccffff";
 												 document.getElementById("data-diri-akhir-kerjasama"+no).style.background = "#ccffff";
 												 document.getElementById("data-diri-instansi-negeri"+no).style.background = "#ccffff";
 												  document.getElementById("data-diri-keterangan"+no).style.background = "#ccffff";
                           document.getElementById("data-diri-manfaat"+no).style.background = "#ccffff";

											}



										</script>
										<script>

											function save_row1(no){

												var nama_instansi=  document.getElementById("data-diri-nama-instansi"+no).value;
												var jenis_kegiatan=  document.getElementById("data-diri-jenis-kegiatan"+no).value;
												var mulai_kerjasama= document.getElementById("data-diri-mulai-kerjasama"+no).value;
												var akhir_kerjasama=  document.getElementById("data-diri-akhir-kerjasama"+no).value;
												var instansi_negeri= document.getElementById("data-diri-instansi-negeri"+no).value;
												var keterangan= document.getElementById("data-diri-keterangan"+no).value;
                        var manfaat= document.getElementById("data-diri-manfaat"+no).value;
												var id = no;


															$.ajax({

																	type: "POST",
																	url: "<?php echo base_url(); ?>Standar7/kerjasamaupdate",
																	data: {id:id, nama_instansi :nama_instansi, jenis_kegiatan:jenis_kegiatan, mulai_kerjasama:mulai_kerjasama, akhir_kerjasama:akhir_kerjasama,  instansi_negeri:instansi_negeri, keterangan:keterangan, manfaat:manfaat },

																	success: function(data){
																	console.log(data);

																		},

																	error: function(xhr, status, errorThrown){
																	alert( xhr.status);
																	alert( xhr.errorThrown);
																	alert( xhr.responseText);
																	console.log(xhr.responseText);
																	}
															});

																					document.getElementById('edit'+no).style.display='block';
																				  document.getElementById('save'+no).style.display='none';
																					 document.getElementById("data-diri-nama-instansi"+no).readOnly =true;
																					 document.getElementById("data-diri-jenis-kegiatan"+no).readOnly =true;
																					 document.getElementById("data-diri-mulai-kerjasama"+no).readOnly =true;
																					 document.getElementById("data-diri-akhir-kerjasama"+no).readOnly =true;
																					 document.getElementById("data-diri-instansi-negeri"+no).readOnly =true;
																					 document.getElementById("data-diri-keterangan"+no).readOnly =true;
                                           document.getElementById("data-diri-manfaat"+no).readOnly =true;


                                          document.getElementById("data-diri-nama-instansi"+no).style.background = "#f3f3f3";
                                           document.getElementById("data-diri-jenis-kegiatan"+no).style.background = "#f3f3f3";
                                           document.getElementById("data-diri-mulai-kerjasama"+no).style.background = "#f3f3f3";
                                          document.getElementById("data-diri-akhir-kerjasama"+no).style.background = "#f3f3f3";
                                          document.getElementById("data-diri-instansi-negeri"+no).style.background = "#f3f3f3";
                                           document.getElementById("data-diri-keterangan"+no).style.background = "#f3f3f3";
                                            document.getElementById("data-diri-manfaat"+no).style.background = "#f3f3f3";
																				}
										</script>


											<script>
												function edit_row(no) {
												  document.getElementById('file'+no).click();
												  document.getElementById("save_btn"+no).style.display="block";
												}

												function save_row(no) {
												  document.getElementById("edit_btn"+no).style.display="block";
												  document.getElementById("save_btn"+no).style.display="none";
												}

												updateList = function(no) {
												  var input = document.getElementById('file'+no);
												  var output = document.getElementById('nama-file'+no);

												  output.innerHTML = '<label>';
												  for (var i = 0; i < input.files.length; ++i) {
													output.innerHTML += input.files.item(i).name ;
												  }
												  output.innerHTML += '</label>';
                            }
											</script>
										</tr>
								<?php
									}
								?>
						</tbody>

						<tfoot style="font-size: 60%;" class="table head-peldosen">
							<th><i class="fa fa-check" aria-hidden="true"></i></th>
							<th>no</th>
							<th >Nama Instansi</th>
							<th >Jenis Kegiatan</th>
							<th >Mulai Kerjasama</th>
							<th>Akhir Kerjasama</th>
							<th>Instansi Negeri</th>
							<th>Ket</th>
              <th>Manfaat yang Diperoleh</th>
							<th>Doc Pendukung(Bukti)</th>
							<th >Upload(Bukti)</th>
              <th><i class="fa fa-pencil-square-o" aria-hidden="true"></i></th>
							<th><i class="fa fa-trash-o" aria-hidden="true"></i></th>
						</tfoot>

				</table>
			</div>
			<div class="delete all-data">
				<button class="btn-delete-list" name="btn-delete-check" type="button" onclick="hapus_checklist()" >Hapus Terpilih</button>
				<?php
					if($this->session->flashdata('msgg')){
						echo "<script> alert('".$this->session->flashdata('msgg')."'); </script>";
					}
				?>
				<script type="text/javascript" language="javascript">
					function hapus_checklist(){

						var selected_value = new Array(); // initialize empty array
						var confirmation = confirm("Apakah anda yakin menghapus data yang anda pilih ?");
						 if(confirmation){
							$(".check:checked").each(function(){
									selected_value.push($(this).val());
								});
								console.log(selected_value);
							$.ajax({
									type: "POST",
									url: "<?php echo base_url(); ?>Standar7/kerjasamahapuschecklist",
									data: {result:JSON.stringify(selected_value)},

									success: function(data){
									console.log(data);
									window.location.href='<?php echo base_url(); ?>Standar7/kerjasamadata?success';
									},

									error: function(xhr, status, errorThrown){
									alert( xhr.status);
									alert( xhr.errorThrown);
									alert( xhr.responseText);
									console.log(xhr.responseText);
									}
							});
						 }
					}
				</script>

				<button class="btn-delete-all" name="btn-delete-all" onclick="ConfirmDeleteall()"  id="delete-btn" type="submit">Hapus Semua Data</i></button>

				<script type="text/javascript">
						var baseUrl='<?php echo base_url(); ?>standar7/kerjasamahapussemua';
						function ConfirmDeleteall()
							{
									var msg = 'Apakah anda yakin menghapus semua data Kerjasama ?';
									if (confirm(msg)){
									   location.href=baseUrl;
									}
							}
				</script>

			</div>
			<br>



<!-- ######################### SELAIN STANDAR 7 ############################# -->





		<?php
			} else {
		?>

    			<div class="main-content">
    				<div class="menu-breadcrumb">
    					<ul class="breadcrumb">
    						<li><a href="<?php echo base_url(); ?>dashboard" id="menu-breadcrumb">Dashboard</a></li>
    						<li><a href="<?php echo base_url(); ?>standar7/kerjasama" id="menu-breadcrumb">Kerjasama</a></li>
    						<li>Kerjasama Data</li>
    					</ul>
    				</div>
    				<div class="menu-head">
    					<ul>
    						<li><a href="<?php echo base_url(); ?>standar7/penelitian" id="menu-head" >Penelitian</a></li>
    						<li><a href="<?php echo base_url(); ?>standar7/abdimas" id="menu-head" >Abdimas</a></li>
    						<li><a href="<?php echo base_url(); ?>standar7/kerjasama" id="menu-head" style="background:#d9d9d9;color:black;">Kerjasama</a></li>
    					</ul>
    				</div>

    					<div class="menu-subhead">
    					<ul>
    						<li><a href="<?php echo base_url(); ?>standar7/kerjasamadata" id="menu-subhead" style="background:#404040;color:white;">Data Kerjasama</a></li>
    					</ul>
    				</div>
    				<div class="sub-header">
    				<h2> Standar 7 : Data Kerjasama</h2>
    				</div>
    					<div class="kerjasama dosen">
    						<div class="krjsama-header dosen-header">
    							<h2>Pilih Cara : </h2>
    						</div>
    						<button class="accordion">Import Data Kerjasama</button>
    						<div class="panel">
                  </br>
    						        <label style="font-size:80%; color:red;">Maaf tidak bisa melakukan import data, karena bukan Anggota tim pengumpul data standar 7</label>
                  </br>
                  </br>
                </div>
    						<button class="accordion">Input Data Kerjasama</button>
                <div class="panel">
                  </br>
                        <label style="font-size:80%; color:red;">Maaf tidak bisa melakukan input data, karena bukan Anggota tim pengumpul data standar 7</label>
                  </br>
                  </br>
                </div>

    					</div>
    					<script>
    						var acc = document.getElementsByClassName("accordion");
    						var i;

    						for (i = 0; i < acc.length; i++) {
    						  acc[i].onclick = function() {
    							this.classList.toggle("active");
    							var panel = this.nextElementSibling;
    							if (panel.style.maxHeight){
    							  panel.style.maxHeight = null;
    							} else {
    							  panel.style.maxHeight = panel.scrollHeight + "px";
    							}
    						  }
    						}
    					</script>
    			</div>
    			<div class="data-kerjasama">
    			<table id="table-view-data-kerjasama1" class="display" width="100%" cellspacing="0">

    						<thead style="font-size: 60%;" class="table head-peldosen">
    							<th>No</th>
    							<th >Nama Instansi</th>
    							<th >Jenis Kegiatan</th>
    							<th >Mulai Kerjasama</th>
    							<th>Akhir Kerjasama</th>
    							<th>Instansi Negeri</th>
    							<th>Ket</th>
                  <th>Manfaat yang Diperoleh</th>
    							<th>Doc Pendukung(Bukti)</th>
    						</thead>

    						<tbody class="table body-peldosen" role="alert" aria-live="polite" aria-relevant="all">

    							<?php
    									$no = 1;
    									foreach($result as $row){

    									?>
    										<tr id="row:<?php echo $row->id ?>">
    											<td ><?php echo $no++ ?></td>
    											<td ><textarea class="data-diri-input" id="data-diri-nama-instansi<?php echo $row->id ?>"  type="text" name="nama_instansi" placeholder="" readonly /><?php echo $row->nama_instansi ?></textarea></td>
    											<td><textarea class="data-diri-input" id="data-diri-jenis-kegiatan<?php echo $row->id ?>"  type="text" name="jenis_kegiatan" placeholder="" readonly /><?php echo $row->jenis_kegiatan ?></textarea></td>
    											<td><textarea class="data-diri-input" id="data-diri-mulai-kerjasama<?php echo $row->id ?>"  type="text" name="mulai-kerjasama" placeholder="" readonly /><?php echo $row->mulai_kerjasama ?></textarea></td>
    											<td><textarea class="data-diri-input" id="data-diri-akhir-kerjasama<?php echo $row->id ?>"  type="text" name="akhir-kerjasama" placeholder="" readonly /><?php echo $row->akhir_kerjasama ?></textarea></td>
    											<td><textarea class="data-diri-input" id="data-diri-instansi-negeri<?php echo $row->id ?>"  type="text" name="instansi-negeri" placeholder="" readonly /><?php echo $row->instasi_negeri ?></textarea></td>
    											<td ><textarea class="data-diri-input" id="data-diri-keterangan<?php echo $row->id ?>"  type="text" name="keterangan" placeholder="" readonly /><?php echo $row->keterangan ?></textarea></td>
                          <td ><textarea class="data-diri-input" title="<?php echo $row->manfaat ?>" id="data-diri-manfaat<?php echo $row->id ?>"  type="text" name="manfaat" placeholder="" readonly /><?php echo $row->manfaat ?></textarea></td>

                          <td ><div class="lots">
    																<p><a target="_blank" href="<?php echo base_url(); ?>uploads/dokumen_kerjasama_std7/<?php echo $row->file_name ?>"><?php echo $row->file_name ?> </a>...</p>

    											</div></td>
    										</tr>
    								<?php
    									}
    								?>
    						</tbody>

    						<tfoot style="font-size: 60%;" class="table head-peldosen">
    							<th>No</th>
    							<th >Nama Instansi</th>
    							<th >Jenis Kegiatan</th>
    							<th >Mulai Kerjasama</th>
    							<th>Akhir Kerjasama</th>
    							<th>Instansi Negeri</th>
    							<th>Ket</th>
                  <th>Manfaat yang Diperoleh</th>
    							<th>Doc Pendukung(Bukti)</th>
    						</tfoot>

    				</table>

    			</div>
    			<br>



		<?php
			}
		?>

		</div>

		<div class="footer">
			<?php
			include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/footer.php";
			?>
		</div>

	</body>



</html>
