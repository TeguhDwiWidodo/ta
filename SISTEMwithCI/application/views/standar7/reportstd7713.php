<?php
if(!isset($_SESSION['nama'])){
      header("location:" . base_url());
      exit();
   }
?>

<!DOCTYPE html>
<html>
	<head>
		<title>Report</title>
		<script type = 'text/javascript' src="<?php echo base_url(); ?>js/jquery-1.12.4.js"></script>
		<script type = 'text/javascript' src="<?php echo base_url(); ?>js/jquery.dataTables.js"></script>

    <link rel="stylesheet" href="<?php echo base_url(); ?>css/pdf.css">

 <!--    <style>
      .std711{
        background: red;
      }
    </style> -->
	</head>

	<body>

    <h2 >STANDAR 7 : PENELITIAN, PELAYANAN/PENGABDIAN MASYARAKAT, DAN KERJASAMA</h2>



        <div class="7_1_3">
          <label>7.1</label>  <label>Penelitian Dosen Tetap yang Bidang Keahliannya Sesuai dengan PS</label><br>
           <label>7.1.3 Tuliskan judul artikel ilmiah/karya ilmiah/karya seni/buku yang dihasilkan selama tiga tahun terakhir oleh dosen tetap yang bidang keahliannya sesuai dengan PS dengan mengikuti format tabel berikut: </label>
           <br>
           <table align="center"   class="display1"  >


                 <thead>
                   <tr>
                   <th id="display-th" class="display" rowspan="2" >No</th>

                   <th class="display"  rowspan="2" >Judul</th>

                   <th class="display" rowspan="2" >Penulis Utama</th>
                   <th class="display" rowspan="2">Penulis Tambahan</th>
                   <th class="display" rowspan="2">Tanggal</th>

                   <th class="display" colspan="3" >Tingkat* </th>

                 </tr>

                 <tr>
                   <th class="display" >Lokal   </th>

                   <th class="display" >Nasional   </th>

                   <th class="display" >Internasional   </th>
                 </tr>
                 <thead>

                 <tbody  role="alert" aria-live="polite" aria-relevant="all">


                   <?php
                       $no = 0;

                       foreach($publikasi as $row){
                          $no++;
                       ?>
                         <tr >
                           <td class="display2"><?php echo $no;?></td>
                                 <td class="display2"><?php echo $row->judul ?></td>
                             <td class="display2"><?php echo $row->penulis_utm ?></td>
                           <td class="display2"><?php echo $row->penulis_tmbh ?></td>
                           <td class="display2"><?php echo $row->tanggal ?> </td>
                               <td class="display2"><?php

                               if($row->skl_publikasi == "Lokal"){
                                 echo $row->jlm_dosen_si;

                               }

                                 ?></td>
                                 <td class="display2"><?php

                                 if($row->skl_publikasi == "Nasional"){
                                   echo $row->jlm_dosen_si;

                                 }
                                   ?></td>


                                   <td class="display2"><?php

                                   if($row->skl_publikasi == "Internasional"){
                                     echo $row->jlm_dosen_si;

                                   }

                                     ?></td>


                                     <?php

                                   } ?>


                                 <tr  >
                                   <td class="display2" colspan="5">Jumlah</td>
                                   <td class="display2">Nc : <?php echo $data_dosen_lokal ?></td>
                                   <td class="display2">Nb : <?php echo $data_dosen_nasional ?></td>
                                   <td class="display2">Na : <?php echo $data_dosen_internasional ?></td>
                                 </tr>
                                 </tbody>
                     </table>
                     <label>Catatan: * = Tuliskan banyaknya dosen pada sel yang sesuai</label>
         </div>
	    </body>
</html>
