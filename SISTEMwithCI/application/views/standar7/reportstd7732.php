<?php
if(!isset($_SESSION['nama'])){
      header("location:" . base_url());
      exit();
   }
?>

<!DOCTYPE html>
<html>
	<head>
		<title>Report</title>
		<script type = 'text/javascript' src="<?php echo base_url(); ?>js/jquery-1.12.4.js"></script>
		<script type = 'text/javascript' src="<?php echo base_url(); ?>js/jquery.dataTables.js"></script>

    <link rel="stylesheet" href="<?php echo base_url(); ?>css/pdf.css">

 <!--    <style>
      .std711{
        background: red;
      }
    </style> -->
	</head>

	<body>

    <h2 >STANDAR 7 : PENELITIAN, PELAYANAN/PENGABDIAN MASYARAKAT, DAN KERJASAMA</h2>

            <div class="7-3-2">
              <label>7.3   Kegiatan Kerjasama dengan Instansi Lain</label><br>
              <label>7.3.2  Tuliskan instansi Luar negeri yang menjalin kerjasama* yang terkait dengan program studi/jurusan dalam tiga tahun terakhir.</label>
              <table  class="display1" width="100%" >

                <thead >
                  <tr>
                  <th class="display">no</th>
                  <th class="display">Nama Instansi</th>
                  <th class="display">Jenis Kegiatan</th>
                  <th class="display">Mulai Kerjasama</th>
                  <th class="display">Akhir Kerjasama</th>
                  <th class="display">Manfaat</th>
                </tr>
                </thead>

                <tbody class="table body-peldosen" role="alert" aria-live="polite" aria-relevant="all">

                  <?php
                      $no = 1;
                      foreach($result1 as $row){

                      ?>
                        <tr id="row:<?php echo $row->id ?>">
                          <td class="display2"><?php echo $no++ ?></td>
                          <td class="display2"><?php echo $row->nama_instansi ?></td>
                          <td class="display2"><?php echo $row->jenis_kegiatan ?></td>
                          <td class="display2"><?php echo $row->mulai_kerjasama ?></td>
                          <td class="display2"><?php echo $row->akhir_kerjasama ?></td>
                          <td class="display2"><?php echo $row->manfaat ?></td>
                        </tr>
                    <?php
                      }
                    ?>
                </tbody>
                <tfoot >
                  <tr>
                  <th class="display" colspan="5">Total Instansi Luar Negeri</th>
                  <th class="display"><?php echo $count_ln; ?></th>
                  <th class="display"></th>
                </tr>
                </tfoot>
              </table>
              <label>Catatan : (*) dokumen pendukung disediakan pada saat asesmen lapangan</label>

            </div>


	    </body>
</html>
