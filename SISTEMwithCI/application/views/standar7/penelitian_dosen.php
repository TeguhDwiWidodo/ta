
<!DOCTYPE html>
<html>
	<head>
		<title>Akreditasi | Penelitian Dosen SI</title>
		<script type = 'text/javascript' src="<?php echo base_url(); ?>js/jquery-1.12.4.js"></script>
		<script type = 'text/javascript' src="<?php echo base_url(); ?>js/jquery-ui-1.12.1/jquery-ui.js"></script>
		<link rel="stylesheet" href="<?php echo base_url(); ?>js/jquery-ui-1.12.1/jquery-ui.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/mainlayout.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/penelitian.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/dosen.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/accordion.css">
		<script type = 'text/javascript' src="<?php echo base_url(); ?>assets/autosize-master/dist/autosize.js"></script>
		<script type = 'text/javascript' src="<?php echo base_url(); ?>js/jquery.dataTables.js"></script>
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/jquery.dataTables.css">
		<script type = 'text/javascript' >
		(function ($){
			$(document).ready(function() {
				$('#table-view-data-peldosen').dataTable({
					"order": [[ 2, 'asc' ]],
					"aLengthMenu": [5, 10, 25],
					iDisplayLength: 5,
						"scrollX": true,
				});
			});
		})(jQuery);
		</script>
		<script type = 'text/javascript' >
		(function ($){
			$(document).ready(function() {
				$('#table-view-data-peldosen-ex').dataTable({
					"order": [[ 2, 'asc' ]],
						"scrollX": true,
				});
			});
		})(jQuery);
		</script>
		<script type='text/javascript'>
		$(function() {
            $( "#tags" ).autocomplete({
               source: "<?php echo base_url(); ?>standar7/dosenautocomplete",
							 dataType:'json',
							 type:'POST',
               minLength: 4

            });
         });
		</script>
		<script type='text/javascript'>
		$(function() {
            $( "#tags" ).autocomplete({
               source: "<?php echo base_url(); ?>standar7/dosenautocomplete",
							 dataType:'json',
							 type:'POST',
               minLength: 2

            });
         });
		</script>
		<script type='text/javascript'>
		$(function() {
            $( "#tags2" ).autocomplete({
               source: "<?php echo base_url(); ?>standar7/dosenautocomplete",
							 dataType:'json',
							 type:'POST',
               minLength: 2

            });
         });
		</script>
		<script type='text/javascript'>
		$(function() {
            $( "#tags3" ).autocomplete({
               source: "<?php echo base_url(); ?>standar7/dosenautocomplete",
							 dataType:'json',
							 type:'POST',
               minLength: 2

            });
         });
		</script>
		<script type='text/javascript'>
		$(function() {
            $( "#tags4" ).autocomplete({
               source: "<?php echo base_url(); ?>standar7/dosenautocomplete",
							 dataType:'json',
							 type:'POST',
               minLength: 2

            });
         });
		</script>
		<script type='text/javascript'>
		$(function() {
            $( "#tags5" ).autocomplete({
               source: "<?php echo base_url(); ?>standar7/dosenautocomplete",
							 dataType:'json',
							 type:'POST',
               minLength: 2

            });
         });
		</script>
		<script type='text/javascript'>
			$(function() {
	    function split( val ) {
	        return val.split( /,\s*/ );
	    }
	    function extractLast( term ) {
	        return split( term ).pop();
	    }

	    $( "#tags1" ).bind( "keydown", function( event ) {
	        if ( event.keyCode === $.ui.keyCode.TAB &&
	            $( this ).autocomplete( "instance" ).menu.active ) {
	            event.preventDefault();
	        }
	    })
	    .autocomplete({
	        minLength: 1,
	        source: function( request, response ) {
	            // delegate back to autocomplete, but extract the last term
	            $.getJSON("<?php echo base_url(); ?>standar7/dosenautocomplete", { term : extractLast( request.term )},response);
	        },
	        focus: function() {
	            // prevent value inserted on focus
	            return false;
	        },
	        select: function( event, ui ) {
	            var terms = split( this.value );
	            // remove the current input
	            terms.pop();
	            // add the selected item
	            terms.push( ui.item.value );
	            // add placeholder to get the comma-and-space at the end
	            terms.push( "" );
	            this.value = terms.join( ", " );
	            return false;
	        }
	    });
	});
		</script>

	</head>

	<body>
		<div class="header">
			<h1><a href="<?php echo base_url(); ?>dashboard">Akreditasi SI</a></h1>
			<?php
				include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/header.php";
			?>
		</div>

		<div class="sidebar">
			<?php
				include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/sidebar.php";
			?>
		</div>

		<div class="main-layout">
			<?php

				if( $this->session->userdata('hak_akses') == 'admin' ?: $this->session->userdata('hak_akses') == 'standar 7' ?: $this->session->userdata('hak_akses') == 'kaprodi'){

				?>
			<div class="main-content">
					<div class="-menu-breadcrumb">
						<ul class="breadcrumb">
							<li><a href="<?php echo base_url(); ?>dashboard" id="menu-breadcrumb">Dashboard</a></li>
							<li><a href="<?php echo base_url(); ?>standar7/penelitian" id="menu-breadcrumb">Penelitian</a></li>
							<li>Penelitian Dosen SI</li>
						</ul>
					</div>
					<div class="menu-head">
						<ul>
							<li><a href="<?php echo base_url(); ?>standar7/penelitian" id="menu-head" style="background:#d9d9d9;color:black;">Penelitian</a></li>
							<li><a href="<?php echo base_url(); ?>standar7/abdimas" id="menu-head">Pengmas</a></li>
							<li><a href="<?php echo base_url(); ?>standar7/kerjasama" id="menu-head">Kerjasama</a></li>
						</ul>
					</div>

						<div class="menu-subhead">
						<ul>
							<li><a href="<?php echo base_url(); ?>standar7/penelitiandosen" id="menu-subhead" style="background:#404040;color:white;">Penelitian Dosen SI</a></li>
							<li><a href="<?php echo base_url(); ?>standar7/penelitiandosenta" id="menu-subhead">Penelitian (TA)</a></li>
							<li><a href="<?php echo base_url(); ?>standar7/publikasi" id="menu-subhead">Publikasi</a></li>
							<li><a href="<?php echo base_url(); ?>standar7/haki" id="menu-subhead">HaKI</a></li>
							<li><a href="<?php echo base_url(); ?>standar7/dosen" id="menu-subhead">Dosen</a></li>
						</ul>
					</div>

					<div class="sub-header">
					<h2> Standar 7 : Penelitian Dosen Sistem Informasi</h2>
					</div>

					<script type="text/javascript" language="javascript">
						var _validFileExtensions = [".xlsx", ".xls", ".csv"];
							function Validate(oForm) {
								var arrInputs = oForm.getElementsByTagName("input");
								for (var i = 0; i < arrInputs.length; i++) {
									var oInput = arrInputs[i];
									if (oInput.type == "file") {
										var sFileName = oInput.value;
										if (sFileName.length > 0) {
											var blnValid = false;
											for (var j = 0; j < _validFileExtensions.length; j++) {
												var sCurExtension = _validFileExtensions[j];
												if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
													blnValid = true;
													break;
												}
											}

											if (!blnValid) {
												alert("Sorry, " + sFileName + " is invalid, allowed extensions are: " + _validFileExtensions.join(", "));
												return false;
											}
										}
									}
								}

								return true;
							}
					</script>

					<div class="penelitian_dosen dosen">
						<div class="peldos-header dosen-header">
							<h2>Pilih Cara : </h2>
						</div>
						<button class="accordion">Import Data Penelitian Dosen</button>
						<div class="panel">
							<div class="panel-input">
							<br>
							<h3 class="head-panel">IMPORT DATA PENELITIAN DOSEN</h3>
							<hr>
							<br>
						<!--	<label >Pengupdate Terakhir : <?php

							if(empty($pengupdate->pengupdate)){

									echo "";


							} else {

								echo $pengupdate->pengupdate;
							} ?></label></br> -->
							<label >Download Template Data Penelitian : </label>
							<label style="color:red;font-size:12px;"><a href="<?php echo base_url(); ?>uploads/Format_import_std7/format_data_penelitiandosen.xlsx" target="_blank">Download File</a></label>
							<br>
						  <br>
						   <form action="<?php echo base_url(); ?>/Standar7/importpenelitiandosenproses" onsubmit="return Validate(this);" method="post" enctype="multipart/form-data">
								<p class="huruf-upload-files">Files: <input required type="file" name="files" id="files" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" /></p>
								<label style="color:red;font-size:12px;">*File yang dapat di import csv, xls, dan xlxs</label>
								<input type="hidden" name="peldosen" value="tb_penelitian_dosen">
								<br>
								<br>
								<hr>
								<button class="upload-btn btn" type="submit" name="btn-upload">Upload</button>
							</form>
						  <br>
						</div>
						</div>
						<button class="accordion">Input Data Penelitian Dosen</button>
						<div class="panel">
							<table class="display" width="100%" cellspacing="0">
							<form  class="data-diri-form" name="data-diri-form"  method="post" action ="<?php echo base_url(); ?>Standar7/penelitiandoseninsert" enctype="multipart/form-data">

							<tbody>
									<tr>
								<td colspan="3"><h3 class="head-panel">INPUT DATA PENELITIAN DOSEN</h3><hr/><br/></td>
								</tr>
								<tr>
 								 <td><!-- <p class="label" id="label1"> --><p class="label">ID Penelitian</p></td>
 								 <td><p> : </p></td>
 								 <td><input class="data-input-kerjasama" name="idpeldos"  type="text"   placeholder="ID Penelitian" required /></td>
 							 </tr>
								<tr>
 								 <td><!-- <p class="label" id="label1"> --><p class="label">Program studi</p></td>
 								 <td><p> : </p></td>
 								 <td><input class="data-input-kerjasama" name="program_studi"  type="text"   placeholder="Program Studi" required /></td>
 							 </tr>
							 <tr>
								 <td><p class="label">Tahun</p></td>
								 <td><p> : </p></td>

								 <td><input class="data-input-kerjasama" id="tahun" name="tahun"   type="date" onchange="coba()" placeholder="Tahun"  required > </td>

								  </tr>
									<tr>
										<td><p class="label">NO TS</p></td>
										<td><p> : </p></td>
										<td>
											<input class="data-input-kerjasama" id="no_ts" name="no_ts"  type="text"  placeholder="NO_TS" readonly required >

										</td>
									</tr>
									<script type="text/javascript">

									function coba(no){

									  $.ajax({
									          url:"<?php echo base_url();?>/Standar7/number_ts",
									          dataType:'json',
									          type:"POST",
									          success: function(result){
									              var obj = result;
									              console.log(obj);
									             // $.each(result.results,function(item){
									             //     $('ul').append('<li>' + item + '</li>')
									             // })


									               ////////////   TS validasi ////////////////////
									               var dateFrom_ts = obj.number_ts[0].date_from;

									               var dateCheck_ts = document.getElementById('tahun').value;

									               var d1_ts = dateFrom_ts.split("-");

									               var c_ts = dateCheck_ts.split("-");

									               var from_ts = new Date(d1_ts[0],d1_ts[1]-1, d1_ts[2]);  // -1 because months are from 0 to 11
									              //  var to_ts   = new Date(d2_ts[0], d2_ts[1]-1, d2_ts[2]);
									var get_time = new Date(c_ts[0], c_ts[1], c_ts[2]);

									               var check_ts = new Date(d1_ts[0], d1_ts[1], d1_ts[2]);
									                 var check_ts1 = new Date(d1_ts[0]-1, d1_ts[1], d1_ts[2]);
									                   var check_ts2 = new Date(d1_ts[0]-2, d1_ts[1], d1_ts[2]);
									                       var check_ts3 = new Date(d1_ts[0]-3, d1_ts[1], d1_ts[2]);
									// console.log(to_ts);
									// console.log(dateCheck_ts);
									// console.log(dateTo_ts);

									console.log(get_time);
									console.log(check_ts);
									console.log(check_ts1);
									console.log(check_ts2);

									if(get_time <= check_ts && get_time >= check_ts1 ){
									document.getElementById('no_ts').value = "TS";

									} else if(get_time <= check_ts1 && get_time >= check_ts2 ){
									document.getElementById('no_ts').value = "TS_1";

									}  else if(get_time <= check_ts2 && get_time >= check_ts3 ){
									document.getElementById('no_ts').value = "TS_2";

									}



									          }
									      });
									// console.log(to+from+check)
									}
									</script>


								<tr>
 								 <td><p class="label" >Tipe</p></td>
 								 <td><p> : </p></td>
 								 <td><input class="data-input-kerjasama" name="tipe"   type="text"   placeholder="Tipe" required ></td>
 							 </tr>
							 <tr>
									<td><p class="label" id="label1">Judul</p></td>
									<td><p> : </p></td>
									<td><textarea class="data-input-kerjasama" name="judul" style="height:80px;"  type="text"   placeholder="Judul" required ></textarea></td>
								</tr>



								<tr>

									<td><p class="label">Nama Ketua Dosen</p></td>
									<td><p> : </p></td>
									<td><input style="font-size:80%;" class="data-input-kerjasama ui-widget" id="tags" name="nama_ketua_dosen"  type="text"  placeholder="Nama Ketua Dosen" required ></td>

								</tr>
								<tr>
									<td><p class="label" >Nama anggota Dosen 1</p></td>
									<td><p> : </p></td>
									<td><input  class="data-input-kerjasama ui-widget"  id="tags1" name="nama_dosen_anggota" style="font-size:80%;"  type="text"  placeholder="Nama Anggota Dosen"  ></td>
									<label for="tags1"></label>
								</tr>
								<tr>
									<td><p class="label" >Nama anggota Dosen 2</p></td>
									<td><p> : </p></td>
									<td><input  class="data-input-kerjasama ui-widget"  id="tags2" name="nama_dosen_anggota2" style="font-size:80%;"  type="text"  placeholder="Nama Anggota Dosen"  ></td>
									<label for="tags1"></label>
								</tr>
								<tr>
									<td><p class="label">Nama anggota Dosen 3</p></td>
									<td><p> : </p></td>
									<td><input  class="data-input-kerjasama ui-widget"  id="tags3" name="nama_dosen_anggota3" style="font-size:80%;"  type="text"  placeholder="Nama Anggota Dosen"  ></td>
									<label for="tags1"></label>
								</tr>
								<tr>
									<td><p class="label" >Nama anggota Dosen 4</p></td>
									<td><p> : </p></td>
									<td><input  class="data-input-kerjasama ui-widget"  id="tags4" name="nama_dosen_anggota4" style="font-size:80%;"  type="text"  placeholder="Nama Anggota Dosen"  ></td>
									<label for="tags1"></label>
								</tr>
								<tr>
									<td><p class="label" >Nama anggota Dosen 5</p></td>
									<td><p> : </p></td>
									<td><input  class="data-input-kerjasama ui-widget"  id="tags5" name="nama_dosen_anggota5" style="font-size:80%;"  type="text"  placeholder="Nama Anggota Dosen"  ></td>
									<label for="tags1"></label>
								</tr>
								<tr>
									<td><p class="label" id="label1">Nama Mahasiswa  </p></td>
									<td><p> : </p></td>
									<td><textarea class="data-input-kerjasama" name="nama_mahasiswa" style="height:80px;"  type="text"  placeholder="Nama Mahasiswa" required ></textarea></td>
								</tr>
								<tr>
									<td><p class="label">Status</p></td>
									<td><p> : </p></td>
									<td><input class="data-input-kerjasama" name="status"  type="text"  placeholder="Status" required ></td>
								</tr>
								<tr>
									<td><p class="label">Anggaran</p></td>
									<td><p> : </p></td>
									<td><input id="anggaran" class="data-input-kerjasama" name="anggaran"  type="number"   required ></td>
									<script>
								 $('#anggaran').on('change keyup', function() {
									var sanitized = $(this).val().replace(/[^0-9]/g, '');
									$(this).val(sanitized);
									});
								 </script>
								</tr>
								<tr>
									<td><p class="label">Keterangan</p></td>
									<td><p> : </p></td>
									<td><input class="data-input-kerjasama" name="keterangan"  type="text"  placeholder="Keterangan" required ></td>
								</tr>

								<tr>
								 <td><p class="label">Sumber Pembiayaan</p></td>
								 <td><p> : </p></td>
								 <td><select name="sumber_pembiayaan"  class="data-input-kerjasama">
										 <option value="Pembiayaan sendiri oleh peneliti">Pembiayaan sendiri oleh peneliti</option>
										 <option value="PT yang bersangkutan">PT yang bersangkutan</option>
										 <option value="Depdiknas">Depdiknas</option>]
										  <option value="Institusi dalam negeri di luar Depdiknas">Institusi dalam negeri di luar Depdiknas</option>
											<option value="Institusi luar negeri">Institusi luar negeri</option>
								</select>
						 </td>
							 </tr>
							 <tr>
							  <td><p class="label">	Jenis Penelitian</p></td>
							  <td><p> : </p></td>
							  <td><select name="jenis_penelitian"  class="data-input-kerjasama">
							 		 <option value="Penelitian Dosen">Penelitian Dosen</option>
							 		 <option value="Penelitian TA">Penelitian TA</option>

							 </select>
							 </td>
							 </tr>


								<tr>
								<td colspan="3"><br/><hr/><input class="data-diri-button btn" id="data-diri-button" name="simpanbtn" title="simpan" type="submit" value="Simpan" ><br/></td>
								</tr>
							</tbody>

							</form>
							<?php
								if($this->session->flashdata('msga')){
									echo "<script> alert('".$this->session->flashdata('msga')."'); </script>";
								}
							?>

						</table>
						</div>
					</div>
					<script>
						var acc = document.getElementsByClassName("accordion");
						var i;

						for (i = 0; i < acc.length; i++) {
						  acc[i].onclick = function() {
							this.classList.toggle("active");
							var panel = this.nextElementSibling;
							if (panel.style.maxHeight){
							  panel.style.maxHeight = null;
							} else {
							  panel.style.maxHeight = panel.scrollHeight + "px";
							}
						  }
						}
					</script>
			</div>
			<div class="data-dosen">
			<table id="table-view-data-peldosen" class="display" width="100%" cellspacing="0">

						<thead class="table head-peldosen">
							<th>#</th>
							<th>no</th>
							<th >ID Peldos</th>
							<th >Program Studi</th>
							<th >No Ts</th>
							<th >Tipe</th>
							<th>Judul</th>
							<th >Tahun</th>
							<th >Nama dosen ketua</th>
							<th >Nama dosen anggota1</th>
							<th >Nama dosen anggota2</th>
							<th >Nama dosen anggota3</th>
							<th >Nama dosen anggota4</th>
							<th >Nama dosen anggota5</th>
							<th >nama_mahasiswa</th>
							<th >status</th>
							<th >Anggaran(Rp)</th>
							<th>Keterangan</th>
							<th>Sumber pembiayaan</th>
							<th>Jenis Penelitian</th>
							<th>Doc Pendukung (Bukti)</th>
							<th>Status Valid</th>
							<th >Upload (Bukti)</th>
							<th><i class="fa fa-pencil-square-o" aria-hidden="true"></th>
								<?php if($this->session->userdata('hak_akses') == 'admin' ?: $this->session->userdata('hak_akses') == 'kaprodi') { ?>
										<th >Validasi </th>
								<?php } ?>
							<th><i class="fa fa-trash-o" aria-hidden="true"></i></th>
								</thead>

						<tbody class="table body-peldosen" role="alert" aria-live="polite" aria-relevant="all">

							<?php
									$num = 1;
									foreach($result as $row){
									?>
										<tr id="row:<?php echo $row->peldos_no ?>">
											<td ><input type="checkbox" name="pilih[]" class="check" value="<?php echo $row->peldos_no ?>" ></td>

											<td class="number"><?php echo $num++ ?></td>
											<td ><textarea class="data-diri-input" title="<?php echo $row->idpeldos ?>" id="idpeldos<?php echo $row->peldos_no ?>"  type="text" name="idpeldos" placeholder="" readonly ><?php echo $row->idpeldos ?></textarea></td>

											<td ><textarea class="data-diri-input" title="<?php echo $row->program_studi ?>" id="programstudi<?php echo $row->peldos_no ?>"  type="text" name="no_ts" placeholder="" readonly ><?php echo $row->program_studi ?></textarea></td>
											<td><textarea class="data-diri-input" title="<?php echo $row->no_ts ?>" id="no_ts<?php echo $row->peldos_no ?>"  type="text" name="no_ts" placeholder="" readonly ><?php echo $row->no_ts ?></textarea></td>
											<td><textarea class="data-diri-input" title="<?php echo $row->tipe ?>" id="tipe<?php echo $row->peldos_no ?>"  type="text" name="no_ts" placeholder="" readonly ><?php echo $row->tipe ?></textarea></td>

										<!--	<td><textarea class="data-diri-input"  title="<?php echo $row->judul ?>" id="judul<?php echo $row->peldos_no ?>"  type="text" name="no_ts" placeholder="" readonly ><?php echo $row->judul ?></textarea></td>
										--><td><a id="link<?php echo $row->peldos_no ?>" href="<?php echo base_url(); ?>standar7/peldos_details/<?php echo $row->idpeldos ?>" target="_blank"><textarea class="data-diri-input" title="<?php echo $row->judul ?>"  id="judul<?php echo $row->peldos_no ?>"  type="text" name="no_ts" placeholder="" readonly ><?php echo $row->judul ?></textarea></a></td>

											<td><textarea class="data-diri-input" title="<?php echo $row->tahun ?>" id="tahun<?php echo $row->peldos_no ?>"  type="text" name="no_ts" placeholder="" readonly ><?php echo $row->tahun ?></textarea></td>

											<td><textarea class="data-diri-input" title="<?php echo $row->nama_dosenketua ?>" id="nama_dosen_ketua<?php echo $row->peldos_no ?>"  type="text" name="no_ts" placeholder="" readonly ><?php echo $row->nama_dosenketua ?></textarea></td>
											<td ><textarea class="data-diri-input" title="<?php echo $row->nama_dosenanggota_1 ?>" id="nama_dosenanggota_1<?php echo $row->peldos_no ?>"  type="text" name="no_ts" placeholder="" readonly ><?php echo $row->nama_dosenanggota_1 ?></textarea></td>
											<td ><textarea class="data-diri-input" title="<?php echo $row->nama_dosenanggota_2 ?>" id="nama_dosenanggota_2<?php echo $row->peldos_no ?>"  type="text" name="no_ts" placeholder="" readonly ><?php echo $row->nama_dosenanggota_2 ?></textarea></td>
											<td ><textarea class="data-diri-input" title="<?php echo $row->nama_dosenanggota_3 ?>" id="nama_dosenanggota_3<?php echo $row->peldos_no ?>"  type="text" name="no_ts" placeholder="" readonly ><?php echo $row->nama_dosenanggota_3 ?></textarea></td>
											<td ><textarea class="data-diri-input" title="<?php echo $row->nama_dosenanggota_4 ?>" id="nama_dosenanggota_4<?php echo $row->peldos_no ?>"  type="text" name="no_ts" placeholder="" readonly ><?php echo $row->nama_dosenanggota_4 ?></textarea></td>
											<td ><textarea class="data-diri-input" title="<?php echo $row->nama_dosenanggota_5 ?>" id="nama_dosenanggota_5<?php echo $row->peldos_no ?>"  type="text" name="no_ts" placeholder="" readonly ><?php echo $row->nama_dosenanggota_5 ?></textarea></td>
											<td ><textarea class="data-diri-input" title="<?php echo $row->nama_mahasiswa_1 ?>" id="nama_mahasiswa_1<?php echo $row->peldos_no ?>"  type="text" name="no_ts" placeholder="" readonly ><?php echo $row->nama_mahasiswa_1 ?></textarea></td>
											<td ><textarea class="data-diri-input" title="<?php echo $row->status ?>" id="status<?php echo $row->peldos_no ?>"  type="text" name="no_ts" placeholder="" readonly ><?php echo $row->status ?></textarea></td>


											<td ><span style="font-size:80%;float:left;">Rp. </span><textarea  style="width:70%;"  class="data-diri-input" title="<?php echo $row->anggaran ?>" id="anggaran<?php echo $row->peldos_no ?>"  type="text" name="no_ts" placeholder="" readonly ><?php echo $row->anggaran ?></textarea></td>
											<td ><textarea class="data-diri-input" title="<?php echo $row->keterangan ?>" id="keterangan<?php echo $row->peldos_no ?>"  type="text" name="no_ts" placeholder="" readonly ><?php echo $row->keterangan ?></textarea></td>
											<td ><textarea class="data-diri-input" title="<?php echo $row->sumber_pembiayaan ?>" id="sumber_pembiayaan<?php echo $row->peldos_no ?>"  type="text" name="no_ts" placeholder="" readonly ><?php echo $row->sumber_pembiayaan ?></textarea></td>
											<td ><textarea class="data-diri-input" title="<?php echo $row->jenis_penelitian ?>" id="jenis_penelitian<?php echo $row->peldos_no ?>"  type="text" name="no_ts" placeholder="" readonly ><?php echo $row->jenis_penelitian ?></textarea></td>

											<td ><div class="lots">
																<p><a target="_blank" title="Uploaded : [<?php echo $row->file_date ?>], <?php echo $row->file_name ?> " href="<?php echo base_url(); ?>uploads/dokumen_peldos_std7/<?php echo $row->file_name ?>"><?php echo $row->file_name ?></a>...</p>

											</div></td>

											<td >
												<?php
												if ($row->sts_valid == 'valid') {
													?>
														<label>Y</label>
													<?php
												} else {
													?>

													<label id="n:<?php echo $row->peldos_no ?>">N</label>
													<?php
												}?>
											</td>

											<td >
											<form action="<?php echo base_url(); ?>Standar7/penelitian_dosenuploadpdf/<?php echo $row->peldos_no ?>" method="post" enctype="multipart/form-data">
												<input class="aa" style="font-size:9px;display: none;" id="file<?php echo $row->peldos_no ?>" required type="file" name="files" onchange="javascript:updateList('<?php echo $row->peldos_no ?>')" accept="application/pdf" />
												<input type="hidden" value="<?php echo $row->peldos_no?>" name="data-id" >
												<input type="hidden" value="<?php echo $row->file_name?>" name="data-filename" >
												<div class="lots" id="nama-file<?php echo $row->peldos_no ?>"></div>
												<br>
												<button title="Pilih Doc Pendukung : <?php echo $row->judul ?>" style="display: block;" type="button" onclick="edit_row('<?php echo $row->peldos_no ?>')" id="edit_btn<?php echo $row->peldos_no ?>" class="edit" value="edit"><i class="fa fa-file" aria-hidden="true"></i></button>
												<br>
												<button title="Upload : <?php echo $row->judul ?>" style="display: none;" name="btn-upload" type="submit" onclick="save_row('<?php echo $row->peldos_no ?>')" id="save_btn<?php echo $row->peldos_no ?>" class="save" value="save"><i class="fa fa-upload" aria-hidden="true"></i></button>

												</form>


											</td>


											<script>
												function edit_row(no) {
												  document.getElementById('file'+no).click();
												  document.getElementById("save_btn"+no).style.display="block";
												}

												function save_row(no) {
												  document.getElementById("edit_btn"+no).style.display="block";
												  document.getElementById("save_btn"+no).style.display="none";
												}

												updateList = function(no) {
												  var input = document.getElementById('file'+no);
												  var output = document.getElementById('nama-file'+no);

												  output.innerHTML = '<label>';
												  for (var i = 0; i < input.files.length; ++i) {
													output.innerHTML += input.files.item(i).name ;
												  }
												  output.innerHTML += '</label>';
}
											</script>
											<td >
											<button onclick="edit_row1('<?php echo $row->peldos_no ?>')" title="Ubah : <?php echo $row->judul ?>" id="edit<?php echo $row->peldos_no ?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
											<button  onclick="save_row1('<?php echo $row->peldos_no ?>')" title="Simpan : <?php echo $row->judul ?>" id="save<?php echo $row->peldos_no ?>" style="display:none;" /><i class="fa fa-floppy-o" aria-hidden="true"></i></button>
										</td>



										<?php if($this->session->userdata('hak_akses') == 'admin' ?: $this->session->userdata('hak_akses') == 'kaprodi') { ?>
												<td >
													<input type="hidden" id="idnum<?php echo $row->peldos_no ?>" value="<?php echo $row->peldos_no?>" name="data-id" >
													<input type="hidden" id="stsvalid<?php echo $row->peldos_no ?>" value="valid" name="data-id" >
													<button name="validasi" onclick="validasi(<?php echo $row->peldos_no ?> )" <?php if ($row->sts_valid == 'valid'){ ?> disabled <?php   } ?> title="Validasi : <?php echo $row->judul ?>" id="validasi<?php echo $row->peldos_no ?>" type="submit"><i class="fa fa-check-square" aria-hidden="true"></i></button>
													<script>
														function validasi(no){
															var id=  document.getElementById("idnum"+no).value;
																var validasi=  document.getElementById("stsvalid"+no).value;

																$.ajax({

																		type: "POST",
																		url: "<?php echo base_url(); ?>Standar7/penelitiandosenvalid",
																		data: {id:id, validasi:validasi
																	 },

																		success: function(data){
																		console.log(data);

																			},

																		error: function(xhr, status, errorThrown){
																		alert( xhr.status);
																		alert( xhr.errorThrown);
																		alert( xhr.responseText);
																		console.log(xhr.responseText);
																		}
																});
																document.getElementById('validasi'+no).disabled = true;
																document.getElementById('n:'+no).innerHTML = 'Y';
														}
													</script>
													</td>
										<?php } ?>




										<td >
											<form action="<?php echo base_url(); ?>standar7/penelitian_dosenhapus/<?php echo $row->peldos_no ?> " method="post" enctype="multipart/form-data">
											<input type="hidden" value="<?php echo$row->peldos_no ?>?>" name="data-id" >
											<input type="hidden" value="<?php echo $row->file_name?>" name="data-filename" >

											<button name="btn-delete" onclick="return ConfirmDelete(<?php echo $row->peldos_no ?> )" title="Hapus : <?php echo $row->judul ?>" id="delete-btn<?php echo $row->peldos_no ?>" type="submit"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
											</form>
											<script type="text/javascript">
													var baseUrl='<?php echo base_url(); ?>standar7/publikasi/<?php echo $row->peldos_no ?>';
													var msgg = 'Apakah anda yakin untuk menghapus data ini ?';
													function ConfirmDelete(no)
													{
													if (confirm(msgg)) {
														location.href=baseUrl;
													}
													else {
														 return false;
													}
													}
											</script>
											</td>

										</tr>
								<?php
									}
								?>

								<script>
				var no;
					function edit_row1(no)
					{

							document.getElementById('edit'+no).style.display ='none';
							document.getElementById('save'+no).style.display ='block';
							document.getElementById("idpeldos"+no).removeAttribute('readonly');
							document.getElementById("programstudi"+no).removeAttribute('readonly');
						 document.getElementById("no_ts"+no).removeAttribute('readonly');
						 document.getElementById("tipe"+no).removeAttribute('readonly');
							document.getElementById("judul"+no).removeAttribute('readonly');
							document.getElementById("tahun"+no).removeAttribute('readonly');
						 document.getElementById("nama_dosen_ketua"+no).removeAttribute('readonly');
						 document.getElementById("nama_dosenanggota_1"+no).removeAttribute('readonly');
						 document.getElementById("nama_dosenanggota_2"+no).removeAttribute('readonly');
						 document.getElementById("nama_dosenanggota_3"+no).removeAttribute('readonly');
						 document.getElementById("nama_dosenanggota_4"+no).removeAttribute('readonly');
						 document.getElementById("nama_dosenanggota_5"+no).removeAttribute('readonly');
							document.getElementById("nama_mahasiswa_1"+no).removeAttribute('readonly');
							document.getElementById("status"+no).removeAttribute('readonly');
							document.getElementById("anggaran"+no).removeAttribute('readonly');
							document.getElementById("keterangan"+no).removeAttribute('readonly');
							links = document.getElementById("link"+no).href;
							document.getElementById("link"+no).removeAttribute("href");
						document.getElementById("sumber_pembiayaan"+no).removeAttribute('readonly');
							document.getElementById("jenis_penelitian"+no).removeAttribute('readonly');
							document.getElementById("idpeldos"+no).style.background = "#ccffff";
							document.getElementById("programstudi"+no).style.background = "#ccffff";
						 document.getElementById("no_ts"+no).style.background = "#ccffff";
						 document.getElementById("tipe"+no).style.background = "#ccffff";
							document.getElementById("judul"+no).style.background = "#ccffff";
							document.getElementById("tahun"+no).style.background = "#ccffff";
						 document.getElementById("nama_dosen_ketua"+no).style.background = "#ccffff";
						 document.getElementById("nama_dosenanggota_1"+no).style.background = "#ccffff";
						 document.getElementById("nama_dosenanggota_2"+no).style.background = "#ccffff";
						 document.getElementById("nama_dosenanggota_3"+no).style.background = "#ccffff";
						 document.getElementById("nama_dosenanggota_4"+no).style.background = "#ccffff";
						 document.getElementById("nama_dosenanggota_5"+no).style.background = "#ccffff";
							document.getElementById("nama_mahasiswa_1"+no).style.background = "#ccffff";
							document.getElementById("status"+no).style.background = "#ccffff";
							document.getElementById("anggaran"+no).style.background = "#ccffff";
							document.getElementById("keterangan"+no).style.background = "#ccffff";
							document.getElementById("sumber_pembiayaan"+no).style.background = "#ccffff";
							document.getElementById("jenis_penelitian"+no).style.background = "#ccffff";
					}



				</script>
				<script>

					function save_row1(no){

						var idpeldos=  document.getElementById("idpeldos"+no).value;
						var program_studi=  document.getElementById("programstudi"+no).value;
						var no_ts=  document.getElementById("no_ts"+no).value;
						var tipe= document.getElementById("tipe"+no).value;
						var judul =  document.getElementById("judul"+no).value;
						var tahun= document.getElementById("tahun"+no).value;
						var nama_dosen_ketua= document.getElementById("nama_dosen_ketua"+no).value;
						var nama_dosenanggota_1=  document.getElementById("nama_dosenanggota_1"+no).value;
						var nama_dosenanggota_2=  document.getElementById("nama_dosenanggota_2"+no).value;
						var nama_dosenanggota_3=  document.getElementById("nama_dosenanggota_3"+no).value;
						var nama_dosenanggota_4=  document.getElementById("nama_dosenanggota_4"+no).value;
						var nama_dosenanggota_5=  document.getElementById("nama_dosenanggota_5"+no).value;
						var nama_mahasiswa_1=  document.getElementById("nama_mahasiswa_1"+no).value;
						var status= document.getElementById("status"+no).value;
						var anggaran=  document.getElementById("anggaran"+no).value;
							var keterangan=  document.getElementById("keterangan"+no).value;
										var sumber_pembiayaan=  document.getElementById("sumber_pembiayaan"+no).value;
										var jenis_penelitian=  document.getElementById("jenis_penelitian"+no).value;
							var id = no;


									$.ajax({

											type: "POST",
											url: "<?php echo base_url(); ?>Standar7/penelitian_dosenupdate",
											data: {id:id, idpeldos:idpeldos, program_studi:program_studi, no_ts :no_ts, tipe:tipe, judul:judul, tahun:tahun,  nama_dosen_ketua:nama_dosen_ketua,
												nama_dosenanggota_1:nama_dosenanggota_1,
												nama_dosenanggota_2:nama_dosenanggota_2,
												nama_dosenanggota_3:nama_dosenanggota_3,
												nama_dosenanggota_4:nama_dosenanggota_4,
												nama_dosenanggota_5:nama_dosenanggota_5,
												nama_mahasiswa_1:nama_mahasiswa_1,
												status:status,
												tahun:tahun, anggaran:anggaran,
											keterangan:keterangan,
												sumber_pembiayaan:sumber_pembiayaan,
												jenis_penelitian:jenis_penelitian,


										 },

											success: function(data){
											console.log(data);

												},

											error: function(xhr, status, errorThrown){
											alert( xhr.status);
											alert( xhr.errorThrown);
											alert( xhr.responseText);
											console.log(xhr.responseText);
											}
									});

																document.getElementById('edit'+no).style.display ='block';
															document.getElementById('save'+no).style.display ='none';

															document.getElementById("idpeldos"+no).readOnly =true;
															document.getElementById("programstudi"+no).readOnly =true;
															 document.getElementById("no_ts"+no).readOnly =true;
													document.getElementById("tipe"+no).readOnly =true;
														document.getElementById("judul"+no).readOnly =true;
														document.getElementById("tahun"+no).readOnly =true;
														document.getElementById("nama_dosen_ketua"+no).readOnly =true;
													document.getElementById("nama_dosenanggota_1"+no).readOnly =true;
													document.getElementById("nama_dosenanggota_2"+no).readOnly =true;
													document.getElementById("nama_dosenanggota_3"+no).readOnly =true;
													document.getElementById("nama_dosenanggota_4"+no).readOnly =true;
													document.getElementById("nama_dosenanggota_5"+no).readOnly =true;
															document.getElementById("nama_mahasiswa_1"+no).readOnly =true;
														document.getElementById("status"+no).readOnly =true;
															document.getElementById("anggaran"+no).readOnly =true;
															document.getElementById("keterangan"+no).readOnly =true;

															links1 = document.getElementById("link"+no).href = links;
															document.getElementById("link"+no).setAttribute("href",links1);
												document.getElementById("sumber_pembiayaan"+no).readOnly =true;
												document.getElementById("jenis_penelitian"+no).readOnly =true;

																			document.getElementById("idpeldos"+no).style.background = "#f3f3f3";
																			document.getElementById("programstudi"+no).style.background = "#f3f3f3";
																		 document.getElementById("no_ts"+no).style.background = "#f3f3f3";
																		 document.getElementById("tipe"+no).style.background = "#f3f3f3";
																			document.getElementById("judul"+no).style.background = "#f3f3f3";
																			document.getElementById("tahun"+no).style.background = "#f3f3f3";
																		 document.getElementById("nama_dosen_ketua"+no).style.background = "#f3f3f3";
																		 document.getElementById("nama_dosenanggota_1"+no).style.background = "#f3f3f3";
																		 document.getElementById("nama_dosenanggota_2"+no).style.background = "#f3f3f3";
																		 document.getElementById("nama_dosenanggota_3"+no).style.background = "#f3f3f3";
																		 document.getElementById("nama_dosenanggota_4"+no).style.background = "#f3f3f3";
																		 document.getElementById("nama_dosenanggota_5"+no).style.background = "#f3f3f3";
																			document.getElementById("nama_mahasiswa_1"+no).style.background = "#f3f3f3";
																			document.getElementById("status"+no).style.background = "#f3f3f3";
																			document.getElementById("anggaran"+no).style.background = "#f3f3f3";
																			document.getElementById("keterangan"+no).style.background = "#f3f3f3";
																document.getElementById("sumber_pembiayaan"+no).style.background = "#f3f3f3";
																document.getElementById("jenis_penelitian"+no).style.background = "#f3f3f3";

														}
				</script>
						</tbody>
						<tfoot class="table head-peldosen">
							<th>#</th>
							<th>no</th>
							<th >ID Peldos</th>
							<th >Program Studi</th>
							<th >No Ts</th>
							<th >Tipe</th>
							<th>Judul</th>
							<th >Tahun</th>
							<th >Nama dosen ketua</th>
							<th >Nama dosen anggota1</th>
							<th >Nama dosen anggota2</th>
							<th >Nama dosen anggota3</th>
							<th >Nama dosen anggota4</th>
							<th >Nama dosen anggota5</th>
							<th >nama_mahasiswa</th>
							<th >status</th>
							<th >Anggaran(Rp)</th>
							<th>Keterangan</th>
							<th>Sumber pembiayaan</th>
							<th>Jenis Penelitian</th>
							<th>Doc Pendukung (Bukti)</th>
							<th>Status Valid</th>
							<th >Upload (Bukti)</th>
							<th><i class="fa fa-pencil-square-o" aria-hidden="true"></i></th>
							<?php if($this->session->userdata('hak_akses') == 'admin' ?: $this->session->userdata('hak_akses') == 'kaprodi') { ?>
									<th >Validasi</th>
							<?php } ?>
							<th><i class="fa fa-trash-o" aria-hidden="true"></i></th>
						</tfoot>

				</table>
			</div>
			<div class="delete all-data">
				<button class="btn-delete-list" name="btn-delete-check" type="button" onclick="hapus_checklist()" >Hapus Terpilih</button>
				<?php
					if($this->session->flashdata('msgg')){
						echo "<script> alert('".$this->session->flashdata('msgg')."'); </script>";
					}
				?>
				<script type="text/javascript" language="javascript">
					function hapus_checklist(){

						var selected_value = new Array(); // initialize empty array
						var confirmation = confirm("Apakah anda yakin menghapus data yang anda pilih ?");
						 if(confirmation){
							$(".check:checked").each(function(){
									selected_value.push($(this).val());
								});
								console.log(selected_value);
							$.ajax({
									type: "POST",
									url: "<?php echo base_url(); ?>Standar7/peldoshapuschecklist1",
									data: {result:JSON.stringify(selected_value)},

									success: function(data){
									console.log(data);
									window.location.href='<?php echo base_url(); ?>Standar7/penelitiandosen?success';
									},

									error: function(xhr, status, errorThrown){
									alert( xhr.status);
									alert( xhr.errorThrown);
									alert( xhr.responseText);
									console.log(xhr.responseText);
									}
							});
						 }
					}
				</script>

				<button class="btn-delete-all" name="btn-delete-all" onclick="ConfirmDeleteall()"  id="delete-btn" type="submit">Hapus Semua Data</i></button>

				<script type="text/javascript">
						var baseUrl='<?php echo base_url(); ?>standar7/penelitianhapussemua';
						function ConfirmDeleteall()
							{
									var msg = 'Apakah anda yakin menghapus semua data Penelitian Dosen ?';
									if (confirm(msg)){
									   location.href=baseUrl;
									}
							}
				</script>

			</div>
			<br>

			<?php
				}else{
					?>
					<div class="main-content">
							<div class="-menu-breadcrumb">
								<ul class="breadcrumb">
									<li><a href="<?php echo base_url(); ?>dashboard" id="menu-breadcrumb">Dashboard</a></li>
									<li><a href="<?php echo base_url(); ?>standar7/penelitian" id="menu-breadcrumb">Penelitian</a></li>
									<li>Penelitian Dosen SI</li>
								</ul>
							</div>
							<div class="menu-head">
								<ul>
									<li><a href="<?php echo base_url(); ?>standar7/penelitian" id="menu-head" style="background:#d9d9d9;color:black;">Penelitian</a></li>
									<li><a href="<?php echo base_url(); ?>standar7/abdimas" id="menu-head">Abdimas</a></li>
									<li><a href="<?php echo base_url(); ?>standar7/kerjasama" id="menu-head">Kerjasama</a></li>
								</ul>
							</div>

								<div class="menu-subhead">
								<ul>
									<li><a href="<?php echo base_url(); ?>standar7/penelitiandosen" id="menu-subhead" style="background:#404040;color:white;">Penelitian Dosen SI</a></li>
									<li><a href="<?php echo base_url(); ?>standar7/penelitiandosenta" id="menu-subhead">Penelitian (TA)</a></li>
									<li><a href="<?php echo base_url(); ?>standar7/publikasi" id="menu-subhead">Publikasi</a></li>
									<li><a href="<?php echo base_url(); ?>standar7/haki" id="menu-subhead">HaKI</a></li>
									<li><a href="<?php echo base_url(); ?>standar7/dosen" id="menu-subhead">Dosen</a></li>
								</ul>
							</div>

							<div class="sub-header">
							<h2> Standar 7 : Penelitian Dosen Sistem Informasi</h2>
							</div>

							<div class="penelitian_dosen dosen">
								<div class="peldos-header dosen-header">
									<h2>Pilih Cara : </h2>
								</div>
								<button class="accordion">Import Data Penelitian Dosen</button>
								<div class="panel">
								</br>
											<label style="font-size:80%; color:red;">Maaf tidak bisa melakukan import data, karena bukan Anggota tim pengumpul data standar 7</label>
								</br>
								</br>
								</div>
								<button class="accordion">Input Data Penelitian Dosen</button>
								<div class="panel">
								</br>
											<label style="font-size:80%; color:red;">Maaf tidak bisa melakukan input data, karena bukan Anggota tim pengumpul data standar 7</label>
								</br>
								</br>
								</div>
							</div>
							<script>
								var acc = document.getElementsByClassName("accordion");
								var i;

								for (i = 0; i < acc.length; i++) {
								  acc[i].onclick = function() {
									this.classList.toggle("active");
									var panel = this.nextElementSibling;
									if (panel.style.maxHeight){
									  panel.style.maxHeight = null;
									} else {
									  panel.style.maxHeight = panel.scrollHeight + "px";
									}
								  }
								}
							</script>
					</div>
					<div class="data-dosen">
					<table id="table-view-data-peldosen-ex" class="display" width="100%" cellspacing="0">

								<thead class="table head-peldosen">
									<th>No</th>
									<th >Program Studi</th>
									<th >No Ts</th>
									<th >Tipe</th>
									<th>Judul</th>
									<th >Tahun</th>
									<th >Nama dosen ketua</th>
									<th >Nama dosen anggota</th>
									<th >nama_mahasiswa</th>
									<th >status</th>
									<th >Anggaran(Rp)</th>
									<th>Keterangan</th>
									<th>Sumber pembiayaan</th>
									<th>Jenis Penelitian</th>
									<th>Doc Pendukung (Bukti)</th>
								</thead>

								<tbody class="table body-peldosen" role="alert" aria-live="polite" aria-relevant="all">

									<?php

											foreach($result as $row){
											?>
												<tr id="row:<?php echo $row->peldos_no ?>">

													<td class="number"></td>

													<td ><textarea class="data-diri-input" id="programstudi<?php echo $row->peldos_no ?>"  type="text" name="no_ts" placeholder="" readonly ><?php echo $row->program_studi ?></textarea></td>
													<td><textarea class="data-diri-input" id="no_ts<?php echo $row->peldos_no ?>"  type="text" name="no_ts" placeholder="" readonly ><?php echo $row->no_ts ?></textarea></td>
													<td><textarea class="data-diri-input" id="tipe<?php echo $row->peldos_no ?>"  type="text" name="no_ts" placeholder="" readonly ><?php echo $row->tipe ?></textarea></td>

													<td><textarea class="data-diri-input" id="judul<?php echo $row->peldos_no ?>"  type="text" name="no_ts" placeholder="" readonly ><?php echo $row->judul ?></textarea></td>
													<td><textarea class="data-diri-input" id="tahun<?php echo $row->peldos_no ?>"  type="text" name="no_ts" placeholder="" readonly ><?php echo $row->tahun ?></textarea></td>

													<td><textarea class="data-diri-input" id="nama_dosen_ketua<?php echo $row->peldos_no ?>"  type="text" name="no_ts" placeholder="" readonly ><?php echo $row->nama_dosenketua ?></textarea></td>
													<td ><textarea class="data-diri-input" id="nama_dosenanggota_1<?php echo $row->peldos_no ?>"  type="text" name="no_ts" placeholder="" readonly ><?php echo $row->nama_dosenanggota_1 ?></textarea></td>
													<td ><textarea class="data-diri-input" id="nama_mahasiswa_1<?php echo $row->peldos_no ?>"  type="text" name="no_ts" placeholder="" readonly ><?php echo $row->nama_mahasiswa_1 ?></textarea></td>
													<td ><textarea class="data-diri-input" id="status<?php echo $row->peldos_no ?>"  type="text" name="no_ts" placeholder="" readonly ><?php echo $row->status ?></textarea></td>


													<td ><textarea class="data-diri-input" id="anggaran<?php echo $row->peldos_no ?>"  type="text" name="no_ts" placeholder="" readonly ><?php echo $row->anggaran ?></textarea></td>
													<td ><textarea class="data-diri-input" id="keterangan<?php echo $row->peldos_no ?>"  type="text" name="no_ts" placeholder="" readonly ><?php echo $row->keterangan ?></textarea></td>
													<td ><textarea class="data-diri-input" id="sumber_pembiayaan<?php echo $row->peldos_no ?>"  type="text" name="no_ts" placeholder="" readonly ><?php echo $row->sumber_pembiayaan ?></textarea></td>
													<td ><textarea class="data-diri-input" id="jenis_penelitian<?php echo $row->peldos_no ?>"  type="text" name="no_ts" placeholder="" readonly ><?php echo $row->jenis_penelitian ?></textarea></td>

													<td ><div class="lots">
																		<p><a target="_blank" href="<?php echo base_url(); ?>uploads/dokumen_peldos_std7/<?php echo $row->file_name ?>"><?php echo $row->file_name ?></a>...</p>

													</div></td>

												</tr>
										<?php
											}
										?>

								</tbody>
								<tfoot class="table head-peldosen">
									<th>No</th>
									<th >Program Studi</th>
									<th >No Ts</th>
									<th >Tipe</th>
									<th>Judul</th>
									<th >Tahun</th>
									<th >Nama dosen ketua</th>
									<th >Nama dosen anggota</th>
									<th >nama_mahasiswa</th>
									<th >status</th>
									<th >Anggaran(Rp)</th>
									<th>Keterangan</th>
										<th>Sumber pembiayaan</th>
										<th>Jenis Penelitian</th>
									<th>Doc Pendukung (Bukti)</th>
								</tfoot>

						</table>
					</div>


			<?php
				}
			?>
		</div>

		<div class="footer">
			<?php
			include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/footer.php";
			?>
		</div>

	</body>



</html>
