<?php
if(!isset($_SESSION['nama'])){
      header("location:" . base_url());
      exit();
   }
?>

<!DOCTYPE html>
<html>
	<head>
		<title>Report</title>
		<script type = 'text/javascript' src="<?php echo base_url(); ?>js/jquery-1.12.4.js"></script>
		<script type = 'text/javascript' src="<?php echo base_url(); ?>js/jquery.dataTables.js"></script>

    <link rel="stylesheet" href="<?php echo base_url(); ?>css/pdf.css">

 <!--    <style>
      .std711{
        background: red;
      }
    </style> -->
	</head>

	<body>

    <h2 >STANDAR 7 : PENELITIAN, PELAYANAN/PENGABDIAN MASYARAKAT, DAN KERJASAMA</h2>


          <div class="7_1_4">
              <label>7.1</label>  <label>Penelitian Dosen Tetap yang Bidang Keahliannya Sesuai dengan PS</label><br>
            <label>7.1.4  Sebutkan karya dosen dan atau mahasiswa Program Studi yang telah memperoleh/sedang memproses perlindungan Hak atas Kekayaan Intelektual (HaKI) selama tiga tahun terakhir.</label>
                    <br>
                    <br>
                      <table  align="center" style="witdh:100%;" class="display1">
                            <thead >
                              <tr>
                                <th class="display">No</th>
                                <th class="display">Karya*</th>
                              </tr>
                                      </thead>

                              <tbody >
                                <tr >

                                  <?php
                                    $no = 0;
                                  foreach($result_haki as $row){
                                      $no++;
                                  ?>
                                  <tr>
                                <td class="display2">
                                  <?php  echo $no; ?>
                                </td>
                                <td class="display2">
                                    <?php  echo $row->karya; ?>
                                </td>
                              </tr>
                        <?php
                        } ?>
                    </tbody>
              </table>
              <label>* Lampirkan surat paten HaKI atau keterangan sejenis.</label>
            </div>
	    </body>
</html>
