
<!DOCTYPE html>
<html>
	<head>
		<title>Akreditasi | Publikasi SI</title>
		<script type = 'text/javascript' src="<?php echo base_url(); ?>js/jquery-1.12.4.js"></script>
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/mainlayout.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/penelitian.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/dosen.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/accordion.css">
		<script type = 'text/javascript' src="<?php echo base_url(); ?>assets/autosize-master/dist/autosize.js"></script>
		<script type = 'text/javascript' src="<?php echo base_url(); ?>js/jquery.dataTables.js"></script>
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/jquery.dataTables.css">
		<script type = 'text/javascript' >
		(function ($){
			$(document).ready(function() {
				$('#table-view-data-peldosen').dataTable({
					"order": [[ 2, 'asc' ]],
					"aLengthMenu": [5, 10, 25],
					iDisplayLength: 5,
						"scrollX": true,
						//"deferRender": true,
				});
			});
		})(jQuery);
		</script>
		<script type='text/javascript'>
		$(function() {
            $( "#tags" ).autocomplete({
               source: "<?php echo base_url(); ?>standar7/dosenautocomplete",
							 dataType:'json',
							 type:'POST',
               minLength: 2

            });
         });
		</script>
		<script type='text/javascript'>
		$(function() {
            $( "#tags2" ).autocomplete({
               source: "<?php echo base_url(); ?>standar7/dosenautocomplete",
							 dataType:'json',
							 type:'POST',
               minLength: 2

            });
         });
		</script>
		<script type='text/javascript'>
		$(function() {
            $( "#tags3" ).autocomplete({
               source: "<?php echo base_url(); ?>standar7/dosenautocomplete",
							 dataType:'json',
							 type:'POST',
               minLength: 2

            });
         });
		</script>
		<script type='text/javascript'>
		$(function() {
            $( "#tags4" ).autocomplete({
               source: "<?php echo base_url(); ?>standar7/dosenautocomplete",
							 dataType:'json',
							 type:'POST',
               minLength: 2

            });
         });
		</script>
		<script type='text/javascript'>
		$(function() {
            $( "#tags5" ).autocomplete({
               source: "<?php echo base_url(); ?>standar7/dosenautocomplete",
							 dataType:'json',
							 type:'POST',
               minLength: 2

            });
         });
		</script>
		<script type='text/javascript'>
			$(function() {
	    function split( val ) {
	        return val.split( /,\s*/ );
	    }
	    function extractLast( term ) {
	        return split( term ).pop();
	    }

	    $( "#tags1" ).bind( "keydown", function( event ) {
	        if ( event.keyCode === $.ui.keyCode.TAB &&
	            $( this ).autocomplete( "instance" ).menu.active ) {
	            event.preventDefault();
	        }
	    })
	    .autocomplete({
	        minLength: 1,
	        source: function( request, response ) {
	            // delegate back to autocomplete, but extract the last term
	            $.getJSON("<?php echo base_url(); ?>standar7/dosenautocomplete", { term : extractLast( request.term )},response);
	        },
	        focus: function() {
	            // prevent value inserted on focus
	            return false;
	        },
	        select: function( event, ui ) {
	            var terms = split( this.value );
	            // remove the current input
	            terms.pop();
	            // add the selected item
	            terms.push( ui.item.value );
	            // add placeholder to get the comma-and-space at the end
	            terms.push( "" );
	            this.value = terms.join( ", " );
	            return false;
	        }
	    });
	});
		</script>
	</head>

	<body>
		<div class="header">
			<h1><a href="<?php echo base_url(); ?>dashboard">Akreditasi SI</a></h1>
			<?php
				include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/header.php";
			?>
		</div>

		<div class="sidebar">
			<?php
				include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/sidebar.php";
			?>
		</div>

		<div class="main-layout">
			<?php

				if( $this->session->userdata('hak_akses') == 'admin' ?: $this->session->userdata('hak_akses') == 'standar 7' ?: $this->session->userdata('hak_akses') == 'kaprodi'){

				?>
			<div class="main-content">
					<div class="-menu-breadcrumb">
						<ul class="breadcrumb">
							<li><a href="<?php echo base_url(); ?>dashboard" id="menu-breadcrumb">Dashboard</a></li>
							<li><a href="<?php echo base_url(); ?>standar7/penelitian" id="menu-breadcrumb">Penelitian</a></li>
							<li>Publikasi</li>
						</ul>
					</div>
					<div class="menu-head">
						<ul>
							<li><a href="<?php echo base_url(); ?>standar7/penelitian" id="menu-head" style="background:#d9d9d9;color:black;">Penelitian</a></li>
							<li><a href="<?php echo base_url(); ?>standar7/abdimas" id="menu-head">Pengmas</a></li>
							<li><a href="<?php echo base_url(); ?>standar7/kerjasama" id="menu-head">Kerjasama</a></li>
						</ul>
					</div>

						<div class="menu-subhead">
						<ul>
							<li><a href="<?php echo base_url(); ?>standar7/penelitiandosen" id="menu-subhead" >Penelitian Dosen SI</a></li>
							<li><a href="<?php echo base_url(); ?>standar7/penelitiandosenta" id="menu-subhead">Penelitian (TA)</a></li>
							<li><a href="<?php echo base_url(); ?>standar7/publikasi" id="menu-subhead" style="background:#404040;color:white;">Publikasi</a></li>
							<li><a href="<?php echo base_url(); ?>standar7/haki" id="menu-subhead">HaKI</a></li>
							<li><a href="<?php echo base_url(); ?>standar7/dosen" id="menu-subhead">Dosen</a></li>
						</ul>
					</div>

					<div class="sub-header">
					<h2> Standar 7 : Publikasi</h2>
					</div>

					<script type="text/javascript" language="javascript">
						var _validFileExtensions = [".xlsx", ".xls", ".csv"];
							function Validate(oForm) {
								var arrInputs = oForm.getElementsByTagName("input");
								for (var i = 0; i < arrInputs.length; i++) {
									var oInput = arrInputs[i];
									if (oInput.type == "file") {
										var sFileName = oInput.value;
										if (sFileName.length > 0) {
											var blnValid = false;
											for (var j = 0; j < _validFileExtensions.length; j++) {
												var sCurExtension = _validFileExtensions[j];
												if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
													blnValid = true;
													break;
												}
											}

											if (!blnValid) {
												alert("Sorry, " + sFileName + " is invalid, allowed extensions are: " + _validFileExtensions.join(", "));
												return false;
											}
										}
									}
								}

								return true;
							}
					</script>

					<div class="penelitian_dosen dosen">
						<div class="peldos-header dosen-header">
							<h2>Pilih Cara : </h2>
						</div>
						<button class="accordion">Import Data Publikasi</button>
						<div class="panel">
							<div class="panel-input">
							<br/>
							<h3 class="head-panel">IMPORT DATA PUBLIKASI</h3>
							<hr/>
							<br>
					<!--		<label >pengupdate : <?php

							if(empty($pengupdate->pengupdate)){

echo "";


							} else {

								echo $pengupdate->pengupdate;
							} ?></label></br> -->
							<label >Download Template Data Publikasi : </label>
							<label style="color:red;font-size:12px;"><a href="<?php echo base_url(); ?>uploads/Format_import_std7/format_data_publikasi.xlsx" target="_blank">Download File</a></label>
							<br>
						  <br>
						   <form action="<?php echo base_url(); ?>/Standar7/publikasiimport" onsubmit="return Validate(this);" method="post" enctype="multipart/form-data">
								<p class="huruf-upload-files">Files: <input   type="file" name="files" id="files" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" /></p>
								<label style="color:red;font-size:12px;">*File yang dapat di import csv, xls, dan xlxs</label>
								<br/>
								<br/>
								<hr/>
								<button class="btn" type="submit" name="btn-upload">Upload</button>
							</form>
						  <br>
						</div>
						</div>
						<button class="accordion">Input Data Publikasi</button>
						<div class="panel">
							<table class="display" width="100%" cellspacing="0">
							<form  class="data-diri-form" name="data-diri-form"  method="post" action ="<?php echo base_url(); ?>Standar7/publikasiinsert" enctype="multipart/form-data">

							<tbody>
									<tr>
								<td colspan="3"><h3 class="head-panel">INPUT DATA PUBLIKASI</h3><hr/><br/></td>
								</tr>
								<tr>
 								 <td><p class="label" >program Studi</p></td>
 								 <td><p> : </p></td>
 								 <td><input class="data-input-kerjasama" name="prodi"   type="text"   placeholder="Program Studi"   ></td>
 							 </tr>
							 <tr>
								<td><p class="label">Tahun</p></td>
								<td><p> : </p></td>
								<td><input class="data-input-kerjasama" id="tahun" name="tanggal"   type="date" onchange="coba()" placeholder="Tanggal" required ></td>
								 </tr>
								 <tr>
									 <td><p class="label">NO TS</p></td>
									 <td><p> : </p></td>
									 <td>
										 <input class="data-input-kerjasama" id="no_ts" name="no_ts"  type="text"  placeholder="NO_TS" readonly required >

									 </td>
								 </tr>
								 <script type="text/javascript">

								 function coba(no){

									 $.ajax({
													 url:"<?php echo base_url();?>/Standar7/number_ts",
													 dataType:'json',
													 type:"POST",
													 success: function(result){
															 var obj = result;
															 console.log(obj);
															// $.each(result.results,function(item){
															//     $('ul').append('<li>' + item + '</li>')
															// })


																////////////   TS validasi ////////////////////
																var dateFrom_ts = obj.number_ts[0].date_from;

																var dateCheck_ts = document.getElementById('tahun').value;

																var d1_ts = dateFrom_ts.split("-");

																var c_ts = dateCheck_ts.split("-");

																var from_ts = new Date(d1_ts[0],d1_ts[1]-1, d1_ts[2]);  // -1 because months are from 0 to 11
															 //  var to_ts   = new Date(d2_ts[0], d2_ts[1]-1, d2_ts[2]);
								 var get_time = new Date(c_ts[0], c_ts[1], c_ts[2]);

																var check_ts = new Date(d1_ts[0], d1_ts[1], d1_ts[2]);
																	var check_ts1 = new Date(d1_ts[0]-1, d1_ts[1], d1_ts[2]);
																		var check_ts2 = new Date(d1_ts[0]-2, d1_ts[1], d1_ts[2]);
																				var check_ts3 = new Date(d1_ts[0]-3, d1_ts[1], d1_ts[2]);
								 // console.log(to_ts);
								 // console.log(dateCheck_ts);
								 // console.log(dateTo_ts);

								 console.log(get_time);
								 console.log(check_ts);
								 console.log(check_ts1);
								 console.log(check_ts2);

								 if(get_time <= check_ts && get_time >= check_ts1 ){
								 document.getElementById('no_ts').value = "TS";

								 } else if(get_time <= check_ts1 && get_time >= check_ts2 ){
								 document.getElementById('no_ts').value = "TS_1";

								 }  else if(get_time <= check_ts2 && get_time >= check_ts3 ){
								 document.getElementById('no_ts').value = "TS_2";

								 }



													 }
											 });
								 // console.log(to+from+check)
								 }
								 </script>

								<tr>
								 <td><p class="label">Skala Publikasi</p></td>
								 <td><p> : </p></td>
								 <td><select name="skl_publikasi"  class="data-input-kerjasama">
										 <option value="Lokal">Lokal</option>
										 <option value="Nasional">Nasional</option>
										 <option value="Internasional">Internasional</option>
							 	</select>
						 </td>
							 </tr><tr>
								 <td><p class="label" id="label1">Judul</p></td>
								 <td><p> : </p></td>
								 <td><textarea class="data-input-kerjasama" name="judul" style="height:80px;"  type="text"   placeholder="Judul"   ></textarea></td>
							 </tr><tr>
								 <td><p class="label">Jenis Publikasi</p></td>
								 <td><p> : </p></td>
								 <td><select class="data-input-kerjasama" name="jns_publikasi" type="text"  placeholder="Jenis Publikasi"   >
									 <option value="Jurnal (Terindeks Scopus)">Jurnal (Terindeks Scopus)</option>
									 <option value="Jurnal (Tidak Terindeks Scopus)l">Jurnal (Tidak Terindeks Scopus)</option>
									 <option value="Prosiding (Terindeks Scopus)">Prosiding (Terindeks Scopus)</option>
									 <option value="Prosiding (Tidak Terindeks Scopus)">Prosiding (Tidak Terindeks Scopus)</option>
								 </select></td>
							 </tr><tr>
								 <td><p class="label">Nama Seminar</p></td>
								 <td><p> : </p></td>
								 <td><input class="data-input-kerjasama" name="nm_seminar"  type="text"   placeholder="Nama Seminar"   ></td>
							 </tr><tr>
								 <td><p class="label">Penulis Utama</p></td>
								 <td><p> : </p></td>
								 <td><input style="font-size:80%;" class="data-input-kerjasama ui-widget" id="tags" name="penulis_utm"  type="text"   placeholder="Penulis Utama"   ></td>
								 <label for="tags"> </label>
							 </tr><tr>
								 <td><p class="label" >Penulis Tambahan 1</p></td>
								 <td><p > : </p></td>
								 <td><input class="data-input-kerjasama ui-widget" name="penulis_tmbh" id="tags1"   type="text"   placeholder="Penulis Tambahan"   ></td>
								 <label for="tags"> </label>
							 </tr><tr>
								 <td><p class="label" >Penulis Tambahan 2</p></td>
								 <td><p > : </p></td>
								 <td><input class="data-input-kerjasama ui-widget" name="penulis_tmbh2" id="tags2"   type="text"   placeholder="Penulis Tambahan"   ></td>
								 <label for="tags"> </label>
							 </tr><tr>
								 <td><p class="label" >Penulis Tambahan 3</p></td>
								 <td><p > : </p></td>
								 <td><input class="data-input-kerjasama ui-widget" name="penulis_tmbh3" id="tags3"   type="text"   placeholder="Penulis Tambahan"   ></td>
								 <label for="tags"> </label>
							 </tr><tr>
								 <td><p class="label" >Penulis Tambahan 4</p></td>
								 <td><p > : </p></td>
								 <td><input class="data-input-kerjasama ui-widget" name="penulis_tmbh4" id="tags4"   type="text"   placeholder="Penulis Tambahan"   ></td>
								 <label for="tags"> </label>
							 </tr><tr>
								 <td><p class="label" >Penulis Tambahan 5</p></td>
								 <td><p > : </p></td>
								 <td><input class="data-input-kerjasama ui-widget" name="penulis_tmbh5" id="tags5"   type="text"   placeholder="Penulis Tambahan"   ></td>
								 <label for="tags"> </label>
							 </tr><tr>
								 <td><p class="label">Keterangan</p></td>
								 <td><p> : </p></td>
								 <td><textarea class="data-input-kerjasama" name="ket" style="height:60px;"  type="text"  placeholder="Keterangan"   ></textarea></td>
							 </tr><tr>
								 <td><p class="label">Jumlah Dosen SI di Publikasi</p></td>
								 <td><p> : </p></td>
								 <td><input id="jlmdosen" class="data-input-kerjasama" name="jlm_dosen_si"  type="number"  min="1" max="10"     ></td>
								 <script>
								$('#jlmdosen').on('change keyup', function() {
								 var sanitized = $(this).val().replace(/[^0-9]/g, '');
								 $(this).val(sanitized);
								 });
								</script>
							 </tr>
								<tr>
								<td colspan="3"><br/><hr/><input class="data-diri-button btn" id="data-diri-button" name="simpanbtn" title="simpan" type="submit" value="Simpan" ><br/></td>
								</tr>
							</tbody>

							</form>


						</table>
						</div>
						<?php
							if($this->session->flashdata('msgp')){
								echo "<script> alert('".$this->session->flashdata('msgp')."'); </script>";
							}
						?>
					</div>
					<script>
						var acc = document.getElementsByClassName("accordion");
						var i;

						for (i = 0; i < acc.length; i++) {
						  acc[i].onclick = function() {
							this.classList.toggle("active");
							var panel = this.nextElementSibling;
							if (panel.style.maxHeight){
							  panel.style.maxHeight = null;
							} else {
							  panel.style.maxHeight = panel.scrollHeight + "px";
							}
						  }
						}
					</script>
			</div>
			<div class="data-dosen">
			<table  id="table-view-data-peldosen" class="display"  width="100%" cellspacing="0">

						<thead style="font-size: 60%;" class="table head-peldosen">
							<th>#</th>
							<th>No</th>
							<th >Prodi</th>
							<th >no TS</th>
							<th >Skala Publikasi</th>
							<th >Judul</th>
							<th >Jenis Publikasi</th>
							<th >Nama Seminar</th>
							<th>Penulis Utama</th>
							<th >Penulis Tambahan 1</th>
							<th >Penulis Tambahan 2</th>
							<th >Penulis Tambahan 3</th>
							<th >Penulis Tambahan 4</th>
							<th >Penulis Tambahan 5</th>
							<th >Tanggal</th>
							<th >Keterangan</th>
							<th >jumlah Dosen SI</th>
							<th >Bukti LoA</th>
							<th >Bukti Jurnal</th>
							<th>Status Valid</th>
							<th >Upload (Bukti LoA)</th>
							<th>upload (Bukti Jurnal)</th>
							<th><i class="fa fa-trash-o" aria-hidden="true"></i></th>
							<?php if($this->session->userdata('hak_akses') == 'admin' ?: $this->session->userdata('hak_akses') == 'kaprodi') { ?>
									<th >Validasi </th>
							<?php } ?>
							<th><i class="fa fa-pencil-square-o" aria-hidden="true"></i></th>
						</thead>

						<tbody style="font-size: 60%;" class="table body-peldosen" role="alert" aria-live="polite" aria-relevant="all">

							<?php
									$no = 0;

									foreach($publikasi as $row){
										 $no++;
									?>
										<tr id="row:<?php echo $row->id ?>">
											<td ><input type="checkbox" name="pilih[]" class="check" value="<?php echo $row->id ?>" ></td>
											<td ><?php echo $no;?></td>
											<td ><textarea class="data-diri-input" title="<?php echo $row->prodi ?>" id="data-diri-nama-instansi0-<?php echo $row->id ?>"  type="text" name="prodi" placeholder="" readonly ><?php echo $row->prodi ?></textarea> </td>
											<td ><textarea class="data-diri-input" title="<?php echo $row->no_ts ?>" id="data-diri-nama-instansi1-<?php echo $row->id ?>"  type="text" name="no_ts" placeholder="" readonly ><?php echo $row->no_ts ?></textarea> </td>
											<td ><textarea class="data-diri-input" title="<?php echo $row->skl_publikasi ?>" id="data-diri-nama-instansi2-<?php echo $row->id ?>"  type="text" name="skl_publikasi" placeholder="" readonly ><?php echo $row->skl_publikasi ?> </textarea></td>
											<td ><textarea class="data-diri-input" title="<?php echo $row->judul ?>" id="data-diri-nama-instansi3-<?php echo $row->id ?>"  type="text" name="judul" placeholder="" readonly ><?php echo $row->judul ?> </textarea> </td>
											<td ><textarea class="data-diri-input" title="<?php echo $row->jns_publikasi ?>" id="data-diri-nama-instansi4-<?php echo $row->id ?>"  type="text" name="jns_publikasi" placeholder="" readonly ><?php echo $row->jns_publikasi ?> </textarea></td>
											<td ><textarea class="data-diri-input" title="<?php echo $row->nm_seminar ?>" id="data-diri-nama-instansi5-<?php echo $row->id ?>"  type="text" name="nm_seminar" placeholder="" readonly ><?php echo $row->nm_seminar ?> </textarea></td>
											<td ><textarea class="data-diri-input" title="<?php echo $row->penulis_utm ?>" id="data-diri-nama-instansi6-<?php echo $row->id ?>"  type="text" name="penulis_utm" placeholder="" readonly ><?php echo $row->penulis_utm ?> </textarea></td>
											<td ><textarea class="data-diri-input" title="<?php echo $row->penulis_tmbh ?>" id="data-diri-nama-instansi7-<?php echo $row->id ?>"  type="text" name="penulis_tmbh" placeholder="" readonly ><?php echo $row->penulis_tmbh ?> </textarea></td>
											<td ><textarea class="data-diri-input" title="<?php echo $row->penulis_tmbh2 ?>" id="data-diri-nama-instansi12-<?php echo $row->id ?>"  type="text" name="penulis_tmbh2" placeholder="" readonly ><?php echo $row->penulis_tmbh2 ?> </textarea></td>
											<td ><textarea class="data-diri-input" title="<?php echo $row->penulis_tmbh3 ?>" id="data-diri-nama-instansi13-<?php echo $row->id ?>"  type="text" name="penulis_tmbh3" placeholder="" readonly ><?php echo $row->penulis_tmbh3 ?> </textarea></td>
											<td ><textarea class="data-diri-input" title="<?php echo $row->penulis_tmbh4 ?>" id="data-diri-nama-instansi14-<?php echo $row->id ?>"  type="text" name="penulis_tmbh4" placeholder="" readonly ><?php echo $row->penulis_tmbh4 ?> </textarea></td>
											<td ><textarea class="data-diri-input" title="<?php echo $row->penulis_tmbh5 ?>" id="data-diri-nama-instansi15-<?php echo $row->id ?>"  type="text" name="penulis_tmbh5" placeholder="" readonly ><?php echo $row->penulis_tmbh5 ?> </textarea></td>
											<td ><textarea class="data-diri-input" title="<?php echo $row->tanggal ?>" id="data-diri-nama-instansi8-<?php echo $row->id ?>"  type="text" name="tanggal" placeholder="" readonly ><?php echo $row->tanggal ?> </textarea></td>
											<td ><textarea class="data-diri-input" title="<?php echo $row->ket ?>" id="data-diri-nama-instansi10-<?php echo $row->id ?>"  type="text" name="ket" placeholder="" readonly ><?php echo $row->ket?> </textarea></td>
											<td ><textarea class="data-diri-input" title="<?php echo $row->jlm_dosen_si ?>" id="data-diri-nama-instansi11-<?php echo $row->id ?>"  type="text" name="jlm_dosen_si" placeholder="" readonly ><?php echo $row->jlm_dosen_si ?></textarea></td>
											<td ><div class="lots">
																<p><a target="_blank" title="Uploaded : [<?php echo $row->file_dates ?>], <?php echo $row->file_names ?> " href="<?php echo base_url(); ?>uploads/dokumen_publikasiloa_std7/<?php echo $row->file_names ?>"><?php echo  $row->file_names ?></a>...</p>

											</div>
											<td ><div class="lots">
																<p><a target="_blank" title="Uploaded : [<?php echo $row->file_date ?>], <?php echo $row->file_name ?> " href="<?php echo base_url(); ?>uploads/dokumen_publikasi_std7/<?php echo $row->file_name ?>"><?php echo  $row->file_name ?></a>...</p>

											</div>
										</td>
										<td >
											<?php
											if ($row->sts_valid == 'valid') {
												?>
													<label>Y</label>
												<?php
											} else {
												?>

												<label id="n:<?php echo $row->id ?>">N</label>
												<?php
											}?>
										</td>
										<td>
										<form onsubmit="return Validate2(this);" action="<?php echo base_url(); ?>Standar7/publikasiuploadloapdf/<?php echo $row->id ?> " method="post" enctype="multipart/form-data">
											<input class="aa" style="font-size:9px;display: none;" id="files<?php echo $row->id ?>" required type="file" name="files" onchange="javascript:updateListt('<?php echo $row->id ?>')" accept="application/pdf" />
											<input type="hidden" value="<?php echo $row->id ?>" name="data-id1" >
											<input type="hidden" value="<?php echo $row->file_names ?>" name="data-filename_" >
											<div class="lots" id="nama-files<?php echo $row->id ?>"></div>
											<br>
											<button style="display: block;" title="Pilih Doc LoA : <?php echo $row->judul ?>"  type="button" onclick="edit_roww('<?php echo $row->id ?>')" id="edit_btnn<?php echo $row->id ?>" class="edit" value="edit"><i class="fa fa-file" aria-hidden="true"></i></button>
											<br>
											<button style="display: none;" name="btn-upload" title="Upload LoA : <?php echo $row->judul ?>"  type="submit"  onclick="save_roww('<?php echo $row->id ?>')" id="save_btnn<?php echo $row->id ?>" class="save" value="save"><i class="fa fa-floppy-o" aria-hidden="true"></i></button>
											</form>
											<script type="text/javascript" language="javascript">
												var _validFileExtensions1 = [".pdf"];
													function Validate2(oForm) {
														var arrInputs = oForm.getElementsByTagName("input");
														for (var i = 0; i < arrInputs.length; i++) {
															var oInput = arrInputs[i];
															if (oInput.type == "file") {
																var sFileName = oInput.value;
																if (sFileName.length > 0) {
																	var blnValid = false;
																	for (var j = 0; j < _validFileExtensions1.length; j++) {
																		var sCurExtension = _validFileExtensions1[j];
																		if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
																			blnValid = true;
																			break;
																		}
																	}

																	if (!blnValid) {
																		alert("Sorry, " + sFileName + " is invalid, allowed extensions are: " + _validFileExtensions1.join(", "));
																		return false;
																	}
																}
															}
														}

														return true;
													}
											</script>
										</td>
										<td >

											<form onsubmit="return Validate1(this);" action="<?php echo base_url(); ?>Standar7/publikasiuploadpdf/<?php echo $row->id ?> " method="post" enctype="multipart/form-data">
												<input class="aa" style="font-size:9px;display: none;" id="file<?php echo $row->id ?>"   type="file" name="files" onchange="javascript:updateList('<?php echo $row->id ?>')" accept="application/pdf" />
												<input type="hidden" value="<?php echo $row->id ?>" name="data-id" >
												<input type="hidden" value="<?php echo $row->file_name?>" name="data-filename" >
												<div class="lots"  id="nama-file<?php echo $row->id ?>"></div>
												<br>
												<button style="display: block;" title="Pilih Doc Pendukung : <?php echo $row->judul ?>" type="button" onclick="edit_row('<?php echo $row->id ?>')" id="edit_btn<?php echo $row->id ?>" class="edit" value="edit"><i class="fa fa-file" aria-hidden="true"></i></button>
												<br>
												<button style="display: none;" name="btn-upload" title="Upload : <?php echo $row->judul ?>" type="submit"  onclick="save_row('<?php echo $row->id ?> ')" id="save_btn<?php echo $row->id ?>" class="save" value="save"><i class="fa fa-floppy-o" aria-hidden="true"></i></button>
												</form>
												<script type="text/javascript" language="javascript">
													var _validFileExtensions1 = [".pdf"];
														function Validate1(oForm) {
															var arrInputs = oForm.getElementsByTagName("input");
															for (var i = 0; i < arrInputs.length; i++) {
																var oInput = arrInputs[i];
																if (oInput.type == "file") {
																	var sFileName = oInput.value;
																	if (sFileName.length > 0) {
																		var blnValid = false;
																		for (var j = 0; j < _validFileExtensions1.length; j++) {
																			var sCurExtension = _validFileExtensions1[j];
																			if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
																				blnValid = true;
																				break;
																			}
																		}

																		if (!blnValid) {
																			alert("Sorry, " + sFileName + " is invalid, allowed extensions are: " + _validFileExtensions1.join(", "));
																			return false;
																		}
																	}
																}
															}

															return true;
														}
												</script>

											</td>
											<td >
											<button onclick="edit_row1('<?php echo $row->id ?>')" title="Ubah : <?php echo $row->judul ?>" id="edit<?php echo $row->id ?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
											<button  onclick="save_row1('<?php echo $row->id ?>')" title="Simpan : <?php echo $row->judul ?>" id="save<?php echo $row->id ?>" style="display:none;" /><i class="fa fa-floppy-o" aria-hidden="true"></i></button>
										</td>
										<?php if($this->session->userdata('hak_akses') == 'admin' ?: $this->session->userdata('hak_akses') == 'kaprodi') { ?>
												<td >
													<input type="hidden" id="idnum<?php echo $row->id ?>" value="<?php echo $row->id?>" name="data-id" >
													<input type="hidden" id="stsvalid<?php echo $row->id ?>" value="valid" name="data-id" >
													<button name="validasi" onclick="validasi(<?php echo $row->id ?> )" <?php if ($row->sts_valid == 'valid'){ ?> disabled <?php   } ?> title="Validasi : <?php echo $row->judul ?>" id="validasi<?php echo $row->id ?>" type="submit"><i class="fa fa-check-square" aria-hidden="true"></i></button>
													<script>
														function validasi(no){
															var id=  document.getElementById("idnum"+no).value;
																var validasi=  document.getElementById("stsvalid"+no).value;

																$.ajax({

																		type: "POST",
																		url: "<?php echo base_url(); ?>Standar7/publikasi_valid",
																		data: {id:id, validasi:validasi
																	 },

																		success: function(data){
																		console.log(data);

																			},

																		error: function(xhr, status, errorThrown){
																		alert( xhr.status);
																		alert( xhr.errorThrown);
																		alert( xhr.responseText);
																		console.log(xhr.responseText);
																		}
																});
																document.getElementById('validasi'+no).disabled = true;
																document.getElementById('n:'+no).innerHTML = 'Y';
														}
													</script>
													</td>
										<?php } ?>

										<td >
											<form action="<?php echo base_url(); ?>standar7/publikasihapus/<?php echo $row->id ?> " method="post" enctype="multipart/form-data">
											<input type="hidden" value="<?php echo$row->id ?>?>" name="data-id" >
											<input type="hidden" value="<?php echo $row->file_name?>" name="data-filename" >
											<input type="hidden" value="<?php echo $row->file_names?>" name="data-filename_" >
											<button name="btn-delete" title="Hapus : <?php echo $row->judul ?>" onclick="return ConfirmDelete(<?php echo $row->id ?> )"  id="delete-btn<?php echo $row->id ?>" type="submit"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
											</form>
											<script type="text/javascript">
													var baseUrl='<?php echo base_url(); ?>standar7/publikasi/<?php echo $row->id ?>';
													var msgg = 'Apakah anda yakin untuk menghapus data ini ?';
													function ConfirmDelete(no)
													{
													if (confirm(msgg)) {
														location.href=baseUrl;
													}
													else {
														 return false;
													}
													}
											</script>
											</td>


														<script>
										var no;
											function edit_row1(no)
											{

												  document.getElementById('edit'+no).style.display='none';
												  document.getElementById('save'+no).style.display='block';
													document.getElementById("data-diri-nama-instansi0-"+no).removeAttribute('readonly');
												  document.getElementById("data-diri-nama-instansi1-"+no).removeAttribute('readonly');
												 document.getElementById("data-diri-nama-instansi2-"+no).removeAttribute('readonly');
												 document.getElementById("data-diri-nama-instansi3-"+no).removeAttribute('readonly');
												  document.getElementById("data-diri-nama-instansi4-"+no).removeAttribute('readonly');
													document.getElementById("data-diri-nama-instansi5-"+no).removeAttribute('readonly');
												 document.getElementById("data-diri-nama-instansi6-"+no).removeAttribute('readonly');
												 document.getElementById("data-diri-nama-instansi7-"+no).removeAttribute('readonly');
													document.getElementById("data-diri-nama-instansi8-"+no).removeAttribute('readonly');
													document.getElementById("data-diri-nama-instansi10-"+no).removeAttribute('readonly');
													document.getElementById("data-diri-nama-instansi11-"+no).removeAttribute('readonly');
													document.getElementById("data-diri-nama-instansi12-"+no).removeAttribute('readonly');
													document.getElementById("data-diri-nama-instansi13-"+no).removeAttribute('readonly');
													document.getElementById("data-diri-nama-instansi14-"+no).removeAttribute('readonly');
													document.getElementById("data-diri-nama-instansi15-"+no).removeAttribute('readonly');


													document.getElementById("data-diri-nama-instansi0-"+no).style.background = "#ccffff";
												  document.getElementById("data-diri-nama-instansi1-"+no).style.background = "#ccffff";
												 document.getElementById("data-diri-nama-instansi2-"+no).style.background = "#ccffff";
												 document.getElementById("data-diri-nama-instansi3-"+no).style.background = "#ccffff";
												  document.getElementById("data-diri-nama-instansi4-"+no).style.background = "#ccffff";
													document.getElementById("data-diri-nama-instansi5-"+no).style.background = "#ccffff";
												 document.getElementById("data-diri-nama-instansi6-"+no).style.background = "#ccffff";
												 document.getElementById("data-diri-nama-instansi7-"+no).style.background = "#ccffff";
													document.getElementById("data-diri-nama-instansi8-"+no).style.background = "#ccffff";
													document.getElementById("data-diri-nama-instansi10-"+no).style.background = "#ccffff";
													document.getElementById("data-diri-nama-instansi11-"+no).style.background = "#ccffff";
													document.getElementById("data-diri-nama-instansi12-"+no).style.background = "#ccffff";
													document.getElementById("data-diri-nama-instansi13-"+no).style.background = "#ccffff";
													document.getElementById("data-diri-nama-instansi14-"+no).style.background = "#ccffff";
													document.getElementById("data-diri-nama-instansi15-"+no).style.background = "#ccffff";
											}



										</script>
										<script>

											function save_row1(no){

												var prodi=  document.getElementById("data-diri-nama-instansi0-"+no).value;
												var no_ts=  document.getElementById("data-diri-nama-instansi1-"+no).value;
												var skl_publikasi=  document.getElementById("data-diri-nama-instansi2-"+no).value;
												var judul= document.getElementById("data-diri-nama-instansi3-"+no).value;
												var jns_publikasi=  document.getElementById("data-diri-nama-instansi4-"+no).value;
												var nm_seminar= document.getElementById("data-diri-nama-instansi5-"+no).value;
												var penulis_utm= document.getElementById("data-diri-nama-instansi6-"+no).value;
												var penulis_tmbh=  document.getElementById("data-diri-nama-instansi7-"+no).value;
												var tanggal=  document.getElementById("data-diri-nama-instansi8-"+no).value;

												var ket=  document.getElementById("data-diri-nama-instansi10-"+no).value;
												var jlm_dosen_si= document.getElementById("data-diri-nama-instansi11-"+no).value;
												var penulis_tmbh2= document.getElementById("data-diri-nama-instansi12-"+no).value;
												var penulis_tmbh3= document.getElementById("data-diri-nama-instansi13-"+no).value;
												var penulis_tmbh4= document.getElementById("data-diri-nama-instansi14-"+no).value;
												var penulis_tmbh5= document.getElementById("data-diri-nama-instansi15-"+no).value;
												var id = no;


															$.ajax({

																	type: "POST",
																	url: "<?php echo base_url(); ?>Standar7/publikasiupdate",
																	data: {id:id, prodi:prodi, no_ts :no_ts, skl_publikasi:skl_publikasi, judul:judul, jns_publikasi:jns_publikasi,
																		nm_seminar:nm_seminar, penulis_utm:penulis_utm, penulis_tmbh:penulis_tmbh,
																		penulis_tmbh2:penulis_tmbh2, penulis_tmbh3:penulis_tmbh3,
																		penulis_tmbh4:penulis_tmbh4, penulis_tmbh5:penulis_tmbh5,
																		 tanggal:tanggal,  ket:ket, jlm_dosen_si:jlm_dosen_si },

																	success: function(data){
																	console.log(data);

																		},

																	error: function(xhr, status, errorThrown){
																	alert( xhr.status);
																	alert( xhr.errorThrown);
																	alert( xhr.responseText);
																	console.log(xhr.responseText);
																	}
															});

																					document.getElementById('edit'+no).style.display='block';
																				  document.getElementById('save'+no).style.display='none';
																					document.getElementById("data-diri-nama-instansi0-"+no).readOnly =true;
																					 document.getElementById("data-diri-nama-instansi1-"+no).readOnly =true;
 																				 document.getElementById("data-diri-nama-instansi2-"+no).readOnly =true;
 																				 document.getElementById("data-diri-nama-instansi3-"+no).readOnly =true;
 																				  document.getElementById("data-diri-nama-instansi4-"+no).readOnly =true;
 																					document.getElementById("data-diri-nama-instansi5-"+no).readOnly =true;
 																				 document.getElementById("data-diri-nama-instansi6-"+no).readOnly =true;
 																				 document.getElementById("data-diri-nama-instansi7-"+no).readOnly =true;
 																					document.getElementById("data-diri-nama-instansi8-"+no).readOnly =true;
 																					document.getElementById("data-diri-nama-instansi10-"+no).readOnly =true;
 																					document.getElementById("data-diri-nama-instansi11-"+no).readOnly =true;
																					document.getElementById("data-diri-nama-instansi12-"+no).readOnly =true;
																					document.getElementById("data-diri-nama-instansi13-"+no).readOnly =true;
																					document.getElementById("data-diri-nama-instansi14-"+no).readOnly =true;
																					document.getElementById("data-diri-nama-instansi15-"+no).readOnly =true;


																					document.getElementById("data-diri-nama-instansi0-"+no).style.background = "#f3f3f3";
																				  document.getElementById("data-diri-nama-instansi1-"+no).style.background = "#f3f3f3";
																				 document.getElementById("data-diri-nama-instansi2-"+no).style.background = "#f3f3f3";
																				 document.getElementById("data-diri-nama-instansi3-"+no).style.background = "#f3f3f3";
																				  document.getElementById("data-diri-nama-instansi4-"+no).style.background = "#f3f3f3";
																					document.getElementById("data-diri-nama-instansi5-"+no).style.background = "#f3f3f3";
																				 document.getElementById("data-diri-nama-instansi6-"+no).style.background = "#f3f3f3";
																				 document.getElementById("data-diri-nama-instansi7-"+no).style.background = "#f3f3f3";
																					document.getElementById("data-diri-nama-instansi8-"+no).style.background = "#f3f3f3";
																					document.getElementById("data-diri-nama-instansi10-"+no).style.background = "#f3f3f3";
																					document.getElementById("data-diri-nama-instansi11-"+no).style.background = "#f3f3f3";
																					document.getElementById("data-diri-nama-instansi12-"+no).style.background = "#f3f3f3";
																					document.getElementById("data-diri-nama-instansi13-"+no).style.background = "#f3f3f3";
																					document.getElementById("data-diri-nama-instansi14-"+no).style.background = "#f3f3f3";
																					document.getElementById("data-diri-nama-instansi15-"+no).style.background = "#f3f3f3";
																				}
										</script>

										<script>
										function edit_roww(no) {
											document.getElementById('files'+no).click();
											document.getElementById("save_btnn"+no).style.display="block";
										}

										function save_roww(no) {
											document.getElementById("edit_btnn"+no).style.display="block";
											document.getElementById("save_btnn"+no).style.display="none";
										}

										updateListt = function(no) {
											 var input = document.getElementById('files'+no);
											var output = document.getElementById('nama-files'+no);

											output.innerHTML = '<label>';
											for (var i = 0; i < input.files.length; ++i) {
											output.innerHTML += input.files.item(i).name ;
											}
											output.innerHTML += '</label>';
									}


										</script>

											<script>
												function edit_row(no) {
												  document.getElementById('file'+no).click();
												  document.getElementById("save_btn"+no).style.display="block";
												}

												function save_row(no) {
												  document.getElementById("edit_btn"+no).style.display="block";
												  document.getElementById("save_btn"+no).style.display="none";
												}


																								updateList = function(no) {
																								  var input = document.getElementById('file'+no);
																								  var output = document.getElementById('nama-file'+no);

																								  output.innerHTML = '<label>';
																								  for (var i = 0; i < input.files.length; ++i) {
																									output.innerHTML += input.files.item(i).name ;
																								  }
																								  output.innerHTML += '</label>';
												                            }

											</script>
										</tr>
								<?php
									}
								?>
						</tbody>
						<tfoot style="font-size: 60%;" class="table head-peldosen">
							<th>#</th>
							<th>No</th>
							<th >Prodi</th>
							<th >no TS</th>
							<th >Skala Publikasi</th>
							<th >Judul</th>
							<th >Jenis Publikasi</th>
							<th >Nama Seminar</th>
							<th>Penulis Utama</th>
							<th >Penulis Tambahan 1</th>
							<th >Penulis Tambahan 2</th>
							<th >Penulis Tambahan 3</th>
							<th >Penulis Tambahan 4</th>
							<th >Penulis Tambahan 5</th>
							<th >Tanggal</th>
							<th >Keterangan</th>
							<th >Jumlah Dosen SI</th>
							<th >Bukti LoA</th>
							<th >Bukti Jurnal</th>
							<th>Status Valid</th>
							<th >Upload (Bukti LoA)</th>
							<th >Upload (Bukti Jurnal)</th>
							<th><i class="fa fa-trash-o" aria-hidden="true"></i></th>
							<?php if($this->session->userdata('hak_akses') == 'admin' ?: $this->session->userdata('hak_akses') == 'kaprodi') { ?>
									<th >Validasi </th>
							<?php } ?>
							<th><i class="fa fa-pencil-square-o" aria-hidden="true"></i></th>
						</tfoot>

				</table>
			</div>
			<div class="delete all-data">
				<button class="btn-delete-list" name="btn-delete-check" type="button" onclick="hapus_checklist()" >Hapus Terpilih</button>
				<?php
					if($this->session->flashdata('msgg')){
						echo "<script> alert('".$this->session->flashdata('msgg')."'); </script>";
					}
				?>
				<script type="text/javascript" language="javascript">
					function hapus_checklist(){

						var selected_value = new Array(); // initialize empty array
						var confirmation = confirm("Apakah anda yakin menghapus data yang anda pilih ?");
						 if(confirmation){
							$(".check:checked").each(function(){
									selected_value.push($(this).val());
								});
								console.log(selected_value);
							$.ajax({
									type: "POST",
									url: "<?php echo base_url(); ?>Standar7/publikasihapuschecklist",
									data: {result:JSON.stringify(selected_value)},

									success: function(data){
									console.log(data);
									window.location.href='<?php echo base_url(); ?>Standar7/publikasi?success';
									},

									error: function(xhr, status, errorThrown){
									alert( xhr.status);
									alert( xhr.errorThrown);
									alert( xhr.responseText);
									console.log(xhr.responseText);
									}
							});
						 }
					}
				</script>

				<button class="btn-delete-all" name="btn-delete-all" onclick="ConfirmDeleteall()"  id="delete-btn" type="submit">Hapus Semua Data</i></button>

				<script type="text/javascript">
						var baseUrl='<?php echo base_url(); ?>standar7/publikasihapussemua';
						function ConfirmDeleteall()
							{
									var msg = 'Apakah anda yakin menghapus semua data Publikasi ?';
									if (confirm(msg)){
									   location.href=baseUrl;
									}
							}
				</script>

			</div>
			<?php




				}else{
					?>
					<div class="main-content">
							<div class="-menu-breadcrumb">
								<ul class="breadcrumb">
									<li><a href="<?php echo base_url(); ?>dashboard" id="menu-breadcrumb">Dashboard</a></li>
									<li><a href="<?php echo base_url(); ?>standar7/penelitian" id="menu-breadcrumb">Penelitian</a></li>
									<li>Publikasi</li>
								</ul>
							</div>
							<div class="menu-head">
								<ul>
									<li><a href="<?php echo base_url(); ?>standar7/penelitian" id="menu-head" style="background:#d9d9d9;color:black;">Penelitian</a></li>
									<li><a href="<?php echo base_url(); ?>standar7/abdimas" id="menu-head">Abdimas</a></li>
									<li><a href="<?php echo base_url(); ?>standar7/kerjasama" id="menu-head">Kerjasama</a></li>
								</ul>
							</div>

								<div class="menu-subhead">
								<ul>
									<li><a href="<?php echo base_url(); ?>standar7/penelitiandosen" id="menu-subhead" >Penelitian Dosen SI</a></li>
									<li><a href="<?php echo base_url(); ?>standar7/penelitiandosenta" id="menu-subhead">Penelitian (TA)</a></li>
									<li><a href="<?php echo base_url(); ?>standar7/publikasi" id="menu-subhead" style="background:#404040;color:white;">Publikasi</a></li>
									<li><a href="<?php echo base_url(); ?>standar7/haki" id="menu-subhead">HaKI</a></li>
									<li><a href="<?php echo base_url(); ?>standar7/dosen" id="menu-subhead">Dosen</a></li>
								</ul>
							</div>

							<div class="sub-header">
							<h2> Standar 7 : Publikasi</h2>
							</div>

							<script type="text/javascript" language="javascript">
								var _validFileExtensions = [".xlsx", ".xls", ".csv"];
									function Validate(oForm) {
										var arrInputs = oForm.getElementsByTagName("input");
										for (var i = 0; i < arrInputs.length; i++) {
											var oInput = arrInputs[i];
											if (oInput.type == "file") {
												var sFileName = oInput.value;
												if (sFileName.length > 0) {
													var blnValid = false;
													for (var j = 0; j < _validFileExtensions.length; j++) {
														var sCurExtension = _validFileExtensions[j];
														if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
															blnValid = true;
															break;
														}
													}

													if (!blnValid) {
														alert("Sorry, " + sFileName + " is invalid, allowed extensions are: " + _validFileExtensions.join(", "));
														return false;
													}
												}
											}
										}

										return true;
									}
							</script>

							<div class="penelitian_dosen dosen">
								<div class="peldos-header dosen-header">
									<h2>Pilih Cara : </h2>
								</div>
								<button class="accordion">Import Data Publikasi</button>
								<div class="panel">
									</br>
										<label style="font-size:80%; color:red;">Maaf tidak bisa melakukan import data, karena bukan Anggota tim pengumpul data standar 7</label>
									</br>
									</br>
								</div>
								<button class="accordion">Input Data Publikasi</button>
								<div class="panel">
								</br>
											<label style="font-size:80%; color:red;">Maaf tidak bisa melakukan input data, karena bukan Anggota tim pengumpul data standar 7</label>
								</br>
								</br>
							</div>
							<script>
								var acc = document.getElementsByClassName("accordion");
								var i;

								for (i = 0; i < acc.length; i++) {
								  acc[i].onclick = function() {
									this.classList.toggle("active");
									var panel = this.nextElementSibling;
									if (panel.style.maxHeight){
									  panel.style.maxHeight = null;
									} else {
									  panel.style.maxHeight = panel.scrollHeight + "px";
									}
								  }
								}
							</script>
					</div>
					<div class="data-dosen">
					<table  id="table-view-data-peldosen" class="display"  width="100%" cellspacing="0">

								<thead style="font-size: 60%;" class="table head-peldosen">
									<th>No</th>
									<th >Prodi</th>
									<th >no TS</th>
									<th >Skala Publikasi</th>
									<th >Judul</th>
									<th >Jenis Publikasi</th>
									<th >Nama Seminar</th>
									<th>Penulis Utama</th>
									<th >Penulis Tambahan</th>
									<th >Tanggal</th>
									<th >Tahun</th>
									<th >Keterangan</th>
									<th >jumlah Dosen SI</th>
									<th >Doc pendukung</th>

								</thead>

								<tbody style="font-size: 60%;" class="table body-peldosen" role="alert" aria-live="polite" aria-relevant="all">

									<?php
											$no = 0;

											foreach($publikasi as $row){
												 $no++;
											?>
												<tr id="row:<?php echo $row->id ?>">
													<td ><?php echo $no;?></td>
													<td ><textarea class="data-diri-input" id="data-diri-nama-instansi0-<?php echo $row->id ?>"  type="text" name="prodi" placeholder="" readonly ><?php echo $row->prodi ?></textarea> </td>
													<td ><textarea class="data-diri-input" id="data-diri-nama-instansi1-<?php echo $row->id ?>"  type="text" name="no_ts" placeholder="" readonly ><?php echo $row->no_ts ?></textarea> </td>
													<td ><textarea class="data-diri-input" id="data-diri-nama-instansi2-<?php echo $row->id ?>"  type="text" name="skl_publikasi" placeholder="" readonly ><?php echo $row->skl_publikasi ?> </textarea></td>
													<td ><textarea class="data-diri-input" id="data-diri-nama-instansi3-<?php echo $row->id ?>"  type="text" name="judul" placeholder="" readonly ><?php echo $row->judul ?> </textarea> </td>
													<td ><textarea class="data-diri-input" id="data-diri-nama-instansi4-<?php echo $row->id ?>"  type="text" name="jns_publikasi" placeholder="" readonly ><?php echo $row->jns_publikasi ?> </textarea></td>
													<td ><textarea class="data-diri-input" id="data-diri-nama-instansi5-<?php echo $row->id ?>"  type="text" name="nm_seminar" placeholder="" readonly ><?php echo $row->nm_seminar ?> </textarea></td>
													<td ><textarea class="data-diri-input" id="data-diri-nama-instansi6-<?php echo $row->id ?>"  type="text" name="penulis_utm" placeholder="" readonly ><?php echo $row->penulis_utm ?> </textarea></td>
													<td ><textarea class="data-diri-input" id="data-diri-nama-instansi7-<?php echo $row->id ?>"  type="text" name="penulis_tmbh" placeholder="" readonly ><?php echo $row->penulis_tmbh ?> </textarea></td>
													<td ><textarea class="data-diri-input" id="data-diri-nama-instansi8-<?php echo $row->id ?>"  type="text" name="tanggal" placeholder="" readonly ><?php echo $row->tanggal ?> </textarea></td>
													<td ><textarea class="data-diri-input" id="data-diri-nama-instansi9-<?php echo $row->id ?>"  type="text" name="tahun" placeholder="" readonly ><?php echo $row->tahun ?> </textarea></td>
													<td ><textarea class="data-diri-input" id="data-diri-nama-instansi10-<?php echo $row->id ?>"  type="text" name="ket" placeholder="" readonly ><?php echo $row->ket?> </textarea></td>
													<td ><textarea class="data-diri-input" id="data-diri-nama-instansi11-<?php echo $row->id ?>"  type="text" name="jlm_dosen_si" placeholder="" readonly ><?php echo $row->jlm_dosen_si ?></textarea></td>
													<td ><div class="lots">
																		<p><a target="_blank" href="<?php echo base_url(); ?>uploads/dokumen_peldos_std7/<?php echo $row->file_name ?>"><?php echo  $row->file_name ?></a>...</p>

													</div>
												</td>

												</tr>
										<?php
											}
										?>
								</tbody>
								<tfoot style="font-size: 60%;" class="table head-peldosen">
									<th>No</th>
									<th >Prodi</th>
									<th >no TS</th>
									<th >Skala Publikasi</th>
									<th >Judul</th>
									<th >Jenis Publikasi</th>
									<th >Nama Seminar</th>
									<th>Penulis Utama</th>
									<th >Penulis Tambahan</th>
									<th >Tanggal</th>
									<th >Tahun</th>
									<th >Keterangan</th>
									<th >Jumlah Dosen SI</th>
									<th >Doc pendukung</th>
								</tfoot>

						</table>
					</div>

			<?php
				}
			?>
		</div>

		<div class="footer">
			<?php
			include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/footer.php";
			?>
		</div>

	</body>



</html>
