
<!DOCTYPE html>
<html>
	<head>
		<title>Akreditasi | Dosen</title>
		<script type = 'text/javascript' src="<?php echo base_url(); ?>js/jquery-1.12.4.js"></script>
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/mainlayout.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/accordion.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/penelitian.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/dosen.css">
		<script type = 'text/javascript' src="<?php echo base_url(); ?>assets/autosize-master/dist/autosize.js"></script>
		<script type = 'text/javascript' src="<?php echo base_url(); ?>js/jquery.dataTables.js"></script>
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/jquery.dataTables.css">
		<script type = 'text/javascript' >
		(function ($){
			$(document).ready(function() {
				$('#table-view-data-dosen').dataTable({
					"order": [[ 0, 'asc' ]],
					"aLengthMenu": [5, 10, 25],
					iDisplayLength: 5,
					"scrollX": true,
				});
			});
		})(jQuery);
		</script>
	</head>

	<body>
		<div class="header">
			<h1><a href="<?php echo base_url(); ?>dashboard">Akreditasi SI</a></h1>
			<?php
				include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/header.php";
			?>
		</div>

		<div class="sidebar">
			<?php
				include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/sidebar.php";
			?>
		</div>

		<div class="main-layout">

			<div class="main-content">
			<div class="-menu-breadcrumb">
				<ul class="breadcrumb">
					<li><a href="<?php echo base_url(); ?>dashboard" id="menu-breadcrumb">Dashboard</a></li>
					<li><a href="<?php echo base_url(); ?>standar7/penelitian" id="menu-breadcrumb">Penelitian</a></li>
					<li>Dosen</li>
				</ul>
			</div>
			<div class="menu-head">
				<ul>
					<li><a href="<?php echo base_url(); ?>standar7/penelitian" id="menu-head" style="background:#d9d9d9;color:black;">Penelitian</a></li>
					<li><a href="<?php echo base_url(); ?>standar7/abdimas" id="menu-head">Pengmas</a></li>
					<li><a href="<?php echo base_url(); ?>standar7/kerjasama" id="menu-head">Kerjasama</a></li>
				</ul>
			</div>

				<div class="menu-subhead">
				<ul>
					<li><a href="<?php echo base_url(); ?>standar7/penelitiandosen" id="menu-subhead" >Penelitian Dosen SI</a></li>
					<li><a href="<?php echo base_url(); ?>standar7/penelitiandosenta" id="menu-subhead" >Penelitian (TA)</a></li>
					<li><a href="<?php echo base_url(); ?>standar7/publikasi" id="menu-subhead">Publikasi</a></li>
					<li><a href="<?php echo base_url(); ?>standar7/haki" id="menu-subhead">HaKI</a></li>
					<li><a href="<?php echo base_url(); ?>standar7/dosen" id="menu-subhead" style="background:#404040;color:white;">Dosen</a></li>
				</ul>
			</div>

			<div class="sub-header">
			<h2> Standar 7 : Data Dosen Sistem Informasi</h2>
			</div>
			<script type="text/javascript" language="javascript">
				var _validFileExtensions = [".xlsx", ".xls", ".csv"];
					function Validate(oForm) {
						var arrInputs = oForm.getElementsByTagName("input");
						for (var i = 0; i < arrInputs.length; i++) {
							var oInput = arrInputs[i];
							if (oInput.type == "file") {
								var sFileName = oInput.value;
								if (sFileName.length > 0) {
									var blnValid = false;
									for (var j = 0; j < _validFileExtensions.length; j++) {
										var sCurExtension = _validFileExtensions[j];
										if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
											blnValid = true;
											break;
										}
									}

									if (!blnValid) {
										alert("Sorry, " + sFileName + " is invalid, allowed extensions are: " + _validFileExtensions.join(", "));
										return false;
									}
								}
							}
						}

						return true;
					}
			</script>

			<div class="pilihdata_dosen dosen">
				<div class="dosen-header">
					<h2>Pilih Cara : </h2>
				</div>
				<button class="accordion">Import Data Dosen</button>
				<div class="panel">
					<div class="panel-input">
						<br/>
						<h3 class="head-panel">IMPORT DATA DOSEN</h3>
						<hr/>
						<br>
				   <form action="importdosenproses" onsubmit="return Validate(this);" method="post" enctype="multipart/form-data">
						<!-- 	<label >pengupdate : <?php

							if(empty($pengupdate->pengupdate)){

echo "";


							} else {

								echo $pengupdate->pengupdate;
							} ?></label></br> -->
						<label >Download Template Data Dosen : </label>
						<label style="color:red;font-size:12px;"><a href="<?php echo base_url(); ?>uploads/Format_import_std7/format_data_dosen.xlsx" target="_blank">Download File</a></label>
						</br>
						</br>
						<p class="huruf-upload-files">Files: <input required type="file" name="files" id="files" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" /></p>
						<label style="color:red;font-size:12px;">*File yang dapat di import csv, xls, dan xlxs</label>
						<input type="hidden" name="datadosen" value="tb_dosen">
						<br/>
						<br/>
						<hr/>
						<button class="btn" type="submit" name="btn-upload">Upload</button>
					</form>
				  <br>
					<script type="text/javascript" language="javascript">
						var _validFileExtensions = [".xlsx", ".xls", ".csv"];
							function Validate(oForm) {
								var arrInputs = oForm.getElementsByTagName("input");
								for (var i = 0; i < arrInputs.length; i++) {
									var oInput = arrInputs[i];
									if (oInput.type == "file") {
										var sFileName = oInput.value;
										if (sFileName.length > 0) {
											var blnValid = false;
											for (var j = 0; j < _validFileExtensions.length; j++) {
												var sCurExtension = _validFileExtensions[j];
												if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
													blnValid = true;
													break;
												}
											}

											if (!blnValid) {
												alert("Sorry, " + sFileName + " is invalid, allowed extensions are: " + _validFileExtensions.join(", "));
												return false;
											}
										}
									}
								}

								return true;
							}
					</script>
					</div>
				</div>
				<button class="accordion">Input Data Dosen</button>
				<div class="panel">
					<table class="display" width="100%" cellspacing="0">
						<form  class="data-diri-form" name="data-diri-form" action="<?php echo base_url(); ?>/Standar7/insert_dosen" method="post" enctype="multipart/form-data">
						<tbody>
								<tr>
							<td colspan="3"><h3 class="head-panel">INPUT DATA DOSEN</h3><hr/><br/></td>
							</tr>
							<tr>
								<td><p class="label">nip</p></td>
								<td><p> : </p></td>
								<td><input class="data-input-kerjasama" id="nip"  type="text"  name="nip" placeholder="NIP" required ></td>
							</tr>
							<tr>
								<td><p class="label">nama dosen</p></td>
								<td><p> : </p></td>
								<td><input class="data-input-kerjasama" id="data-diri-nama-instantsi"  type="text"  name="nama_dosen" placeholder="Nama Dosen" required ></td>
							</tr>
							<tr>
								<td><p class="label" >Tempat Lahir</p></td>
								<td><p id="label1"> : </p></td>
								<td><input class="data-input-kerjasama" id="data-diri-nama-instantsi"  type="text"  name="tempat_lahir" placeholder="Tempat Lahir" required ></td>
								</tr>
							<tr>
								<td><p class="label">	Tanggal lahir</p></td>
								<td><p> : </p></td>
								<td><input class="data-input-kerjasama" id="datepicker1"  type="date"  name="tanggal_lahir" placeholder="Tanggal Lahir"  ></td>
							</tr>
																<tr>
								<td><p class="label">Pendidikan terakhir</p></td>
								<td><p> : </p></td>
								<td><input class="data-input-kerjasama" id="datepicker1"  type="text"  name="pendidikan_terakhir" placeholder="Pendidikan Terakhir"  ></td>

								</tr>

								<tr>
									<td><p class="label">Jabatan akademik</p></td>
									<td><p> : </p></td>
									<td><input class="data-input-kerjasama" id="datepicker1"  type="text"  name="jabatan_akademik" placeholder="Jabatan akademik"  ></td>

									</tr>
							 <tr>
								<td><p class="label" >NIDN</p></td>
								<td><p> : </p></td>
								<td><input  class="data-input-kerjasama" id="data-jenis-kegiatan"  type="textarea"  name="keterangan" placeholder="Keterangan" required></td>
							</tr>
							<tr>
							 <td><p class="label">	program_studi</p></td>
							 <td><p> : </p></td>
							 <td><input class="data-input-kerjasama" id="data-diri-ket"  type="text"  name="program_studi" placeholder="Program studi" ></td>
						 </tr>

						<tr>
						 <td><p class="label">	Bidang keahlian pdd</p></td>
						 <td><p> : </p></td>
						 <td><input class="data-input-kerjasama" id="data-diri-ket"  type="text"  name="bidang_keahlian_pdd" placeholder="Bidang keahlian pdd" ></td>
					 </tr>
					 <tr>
						<td><p class="label">		Status dosen</p></td>
						<td><p> : </p></td>
						<td><select   name="status_dosen" class="data-input-kerjasama" id="data-diri-ket">
							 <option value="Dosen_PS">Dosen_PS</option>
							 <option value="Dosen_Non_PS">Dosen_Non_PS</option>
						 </select>
						</td>
					</tr>
							<tr>
							<td colspan="3"><br/><hr/><input class="data-diri-button btn" id="data-diri-button" name="simpanbtn" title="simpan" type="submit" value="Simpan" ><br/></td>
							</tr>
						</tbody>

						</form>



<?php
if($this->session->userdata('nip_sama') != null){
echo "<script>
alert('".$this->session->userdata('nip_sama')."');

</script>";
}

 ?>
					</table>
				</div>
			</div>

			<script>
				var acc = document.getElementsByClassName("accordion");
				var i;

				for (i = 0; i < acc.length; i++) {
				  acc[i].onclick = function() {
					this.classList.toggle("active");
					var panel = this.nextElementSibling;
					if (panel.style.maxHeight){
					  panel.style.maxHeight = null;
					} else {
					  panel.style.maxHeight = panel.scrollHeight + "px";
					}
				  }
				}
			</script>

			<div class="data-dosen">
			<table id="table-view-data-dosen" class="display" width="100%" cellspacing="0">

						<thead style="font-size: 60%;" class="table head-dosen">
							<th>#</th>
							<th>No</th>
							<th >NIP</th>
							<th >Nama Dosen</th>
							<th >Tempat Lahir</th>
							<th>Tanggal Lahir</th>
							<th >Pendidikan Terakhir</th>
							<th >Jabatan Akademik</th>
							<th >NIDN</th>
							<th style="width:10%;" >Program Studi</th>
							<th>Bidang Keahlian Pendidikan</th>
							<th >Status Dosen</th>
							<th><i class="fa fa-pencil-square-o" aria-hidden="true"></i></th>
							<th><i class="fa fa-trash-o" aria-hidden="true"></i></th>
						</thead>


						<tbody style="font-size: 60%;" class="table body-dosen">
							<?php
							    $nomor = 0;

									foreach($data as $row){
										$nomor++;
									?>
										<tr id="row:<?php echo $row->no; ?>">
											<td ><input type="checkbox" name="pilih[]" class="check" value="<?php echo $row->no ?>" ></td>
											<td><?php echo $nomor;?></td>
											<td ><label  title="<?php echo $row->nip ?>"><?php echo $row->nip ?></label></td>
											<td><textarea class="data-diri-input" title="<?php echo $row->nama_dosen ?>" id="data-diri-nama-dosen<?php echo $row->no ?>"  type="text" name="nama_instansi" placeholder="" readonly><?php echo $row->nama_dosen ?></textarea></td>
											<td><textarea class="data-diri-input" title="<?php echo $row->tempat_lahir ?>" id="data-diri-tempat-lahir<?php echo $row->no ?>"  type="text" name="nama_instansi" placeholder="" readonly><?php echo $row->tempat_lahir ?></textarea></td>
											<td><textarea class="data-diri-input" title="<?php echo $row->tanggal_lahir ?>" id="data-diri-tanggal-lahir<?php echo $row->no ?>"  type="text" name="nama_instansi" placeholder="" readonly><?php echo $row->tanggal_lahir ?></textarea></td>
											<td ><textarea class="data-diri-input" title="<?php echo $row->pendidikan_terakhir ?>" id="data-diri-pendidikan-terakhir<?php echo $row->no ?>"  type="text" name="nama_instansi" placeholder="" readonly><?php echo $row->pendidikan_terakhir ?></textarea></td>
											<td ><textarea class="data-diri-input" title="<?php echo $row->jabatan_akademik ?>" id="data-diri-jabatan-akademik<?php echo $row->no ?>"  type="text" name="nama_instansi" placeholder="" readonly><?php echo $row->jabatan_akademik ?></textarea></td>
											<td><textarea class="data-diri-input" title="<?php echo $row->keterangan ?>" id="data-diri-keterangan<?php echo $row->no ?>"  type="text" name="nama_instansi" placeholder="" readonly><?php echo $row->keterangan ?></textarea></td>
											<td><textarea class="data-diri-input" title="<?php echo $row->program_studi ?>" id="data-diri-program-studi<?php echo $row->no ?>"  type="text" name="nama_instansi" placeholder="" readonly><?php echo $row->program_studi ?></textarea></td>
											<td><textarea class="data-diri-input" title="<?php echo $row->bidang_keahlian_pdd ?>" id="data-diri-bidang-keahlian-pdd<?php echo $row->no ?>"  type="text" name="nama_instansi" placeholder="" readonly><?php echo $row->bidang_keahlian_pdd ?></textarea></td>
											<td><textarea class="data-diri-input" title="<?php echo $row->status_dosen ?>" id="data-diri-status-dosen<?php echo $row->no ?>"  type="text" name="nama_instansi" placeholder="" readonly><?php echo $row->status_dosen ?></textarea></td>
											<td><button onclick="edit_row1('<?php echo $row->no ?>')"  title="Ubah : <?php echo $row->nama_dosen ?>" id="edit<?php echo $row->no ?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
											<button  onclick="save_row1('<?php echo $row->no ?>')" title="Simpan : <?php echo $row->nama_dosen ?>"  id="save<?php echo $row->no ?>" style=" display:none;" /><i class="fa fa-floppy-o" aria-hidden="true"></i></button>
											</td>
											<td>
											<button name="btn-delete" title="Hapus : <?php echo $row->nama_dosen ?>" onclick="hapus_row('<?php echo $row->no ?>')"  id="delete-btn<?php echo $row->no ?>" type="button"><i class="fa fa-trash-o" aria-hidden="true"></i></button>

											</td>


														<script>
										var no;
										function hapus_checklist(){

											var selected_value = new Array(); // initialize empty array
											var confirmation = confirm("Apakah anda yakin menghapus data yang anda pilih ?");
											 if(confirmation){
												$(".check:checked").each(function(){
														selected_value.push($(this).val());
													});
													console.log(selected_value);
												$.ajax({
														type: "POST",
														url: "<?php echo base_url(); ?>Standar7/kerjasamahapuschecklist",
														data: {result:JSON.stringify(selected_value)},

														success: function(data){
														console.log(data);
														window.location.href='<?php echo base_url(); ?>Standar7/kerjasamadata?success';
														},

														error: function(xhr, status, errorThrown){
														alert( xhr.status);
														alert( xhr.errorThrown);
														alert( xhr.responseText);
														console.log(xhr.responseText);
														}
												});
											 }
										}
										function hapus_row(no){

											var msgg = 'Apakah anda yakin untuk menghapus data ini ? '   +no;

												var id=no;
											if (confirm(msgg)) {
												$.ajax({

														type: "POST",
														url: "<?php echo base_url(); ?>Standar7/dosenhapus",
														data: {id:id  },

														success: function(data){
														console.log(data);
										alert('data dengan '+id+' terhapus');
										window.location.href='<?php echo base_url(); ?>Standar7/dosen';

															},

														error: function(xhr, status, errorThrown){
														alert( xhr.status);
														alert( xhr.errorThrown);
														alert( xhr.responseText);
														console.log(xhr.responseText);
														}
												});
											}
											else {
												 return false;
											}

										}
											function edit_row1(no)
											{
												  document.getElementById('edit'+no).style.display= 'none';
												  document.getElementById('save'+no).style.display='block';
												document.getElementById("data-diri-nama-dosen"+no).removeAttribute('readonly');
												 document.getElementById("data-diri-tempat-lahir"+no).removeAttribute('readonly');
												 document.getElementById("data-diri-tanggal-lahir"+no).removeAttribute('readonly');
												 document.getElementById("data-diri-pendidikan-terakhir"+no).removeAttribute('readonly');
												document.getElementById("data-diri-jabatan-akademik"+no).removeAttribute('readonly');
												 document.getElementById("data-diri-keterangan"+no).removeAttribute('readonly');
												 document.getElementById("data-diri-program-studi"+no).removeAttribute('readonly');
												document.getElementById("data-diri-bidang-keahlian-pdd"+no).removeAttribute('readonly');
												document.getElementById("data-diri-status-dosen"+no).removeAttribute('readonly');

												document.getElementById("data-diri-nama-dosen"+no).style.background = "#ccffff";
												 document.getElementById("data-diri-tempat-lahir"+no).style.background = "#ccffff";
												 document.getElementById("data-diri-tanggal-lahir"+no).style.background = "#ccffff";
												 document.getElementById("data-diri-pendidikan-terakhir"+no).style.background = "#ccffff";
												document.getElementById("data-diri-jabatan-akademik"+no).style.background = "#ccffff";
												 document.getElementById("data-diri-keterangan"+no).style.background = "#ccffff";
												 document.getElementById("data-diri-program-studi"+no).style.background = "#ccffff";
												document.getElementById("data-diri-bidang-keahlian-pdd"+no).style.background = "#ccffff";
												document.getElementById("data-diri-status-dosen"+no).style.background = "#ccffff";
											}



										</script>
										<script>

											function save_row1(no){
												var nama_dosen=  document.getElementById("data-diri-nama-dosen"+no).value;
												var tempat_lahir=  document.getElementById("data-diri-tempat-lahir"+no).value;
												var tanggal_lahir= document.getElementById("data-diri-tanggal-lahir"+no).value;
												var pendidikan_terakhir=  document.getElementById("data-diri-pendidikan-terakhir"+no).value;
												var jabatan_akademik= document.getElementById("data-diri-jabatan-akademik"+no).value;
												var keterangan= document.getElementById("data-diri-keterangan"+no).value;
												var program_studi=  document.getElementById("data-diri-program-studi"+no).value;
												var bidang_keahlian_pdd= document.getElementById("data-diri-bidang-keahlian-pdd"+no).value;
												var status_dosen= document.getElementById("data-diri-status-dosen"+no).value;
															var id = no;


															$.ajax({

																	type: "POST",
																	url: "<?php echo base_url(); ?>Standar7/dosenupdate",
																	data: {id:id, nama_dosen :nama_dosen, tempat_lahir:tempat_lahir, tanggal_lahir:tanggal_lahir,  pendidikan_terakhir: pendidikan_terakhir,  jabatan_akademik:jabatan_akademik, keterangan:keterangan
																	, program_studi :program_studi, bidang_keahlian_pdd:bidang_keahlian_pdd,status_dosen:status_dosen  },

																	success: function(data){
																	console.log(data);

																		},

																	error: function(xhr, status, errorThrown){
																	alert( xhr.status);
																	alert( xhr.errorThrown);
																	alert( xhr.responseText);
																	console.log(xhr.responseText);
																	}
															});
																					document.getElementById('edit'+no).style.display= 'block';
																				  document.getElementById('save'+no).style.display= 'none';
																					document.getElementById("data-diri-nama-dosen"+no).readOnly =true;
																					document.getElementById("data-diri-tempat-lahir"+no).readOnly =true;
																					document.getElementById("data-diri-tanggal-lahir"+no).readOnly =true;
																				 document.getElementById("data-diri-pendidikan-terakhir"+no).readOnly =true;
																				document.getElementById("data-diri-jabatan-akademik"+no).readOnly =true;
																					 document.getElementById("data-diri-keterangan"+no).readOnly =true;
																					 document.getElementById("data-diri-program-studi"+no).readOnly =true;
																				document.getElementById("data-diri-bidang-keahlian-pdd"+no).readOnly =true;
																						document.getElementById("data-diri-status-dosen"+no).readOnly =true;

																						document.getElementById("data-diri-nama-dosen"+no).style.background = "#f3f3f3";
																						document.getElementById("data-diri-tempat-lahir"+no).style.background = "#f3f3f3";
																						document.getElementById("data-diri-tanggal-lahir"+no).style.background = "#f3f3f3";
																					 document.getElementById("data-diri-pendidikan-terakhir"+no).style.background = "#f3f3f3";
																					document.getElementById("data-diri-jabatan-akademik"+no).style.background = "#f3f3f3";
																						 document.getElementById("data-diri-keterangan"+no).style.background = "#f3f3f3";
																						 document.getElementById("data-diri-program-studi"+no).style.background = "#f3f3f3";
																					document.getElementById("data-diri-bidang-keahlian-pdd"+no).style.background = "#f3f3f3";
																							document.getElementById("data-diri-status-dosen"+no).style.background = "#f3f3f3";
																				}
										</script>


											<script>
												function edit_row(no) {
												  document.getElementById('file'+no).click();
												  document.getElementById("save_btn"+no).style.display="block";
												}

												function save_row(no) {
												  document.getElementById("edit_btn"+no).style.display="block";
												  document.getElementById("save_btn"+no).style.display="none";
												}

												updateList = function(no) {
												  var input = document.getElementById('file'+no);
												  var output = document.getElementById('nama-file'+no);

												  output.innerHTML = '<label>';
												  for (var i = 0; i < input.files.length; ++i) {
													output.innerHTML += input.files.item(i).name ;
												  }
												  output.innerHTML += '</label>';
}
											</script>
										</tr>
								<?php
									}
								?>
						</tbody>
						<tfoot style="font-size: 60%;" class="table head-dosen">
							<th>#</th>
							<th>No</th>
							<th >NIP</th>
							<th >Nama Dosen</th>
							<th >Tempat Lahir</th>
							<th>Tanggal Lahir</th>
							<th >Pendidikan Terakhir</th>
							<th >Jabatan Akademik</th>
							<th >NIDN</th>
							<th style="width:10%;" >Program Studi</th>
							<th>Bidang Keahlian Pendidikan</th>
							<th >Status Dosen</th>
							<th><i class="fa fa-pencil-square-o" aria-hidden="true"></i></th>
							<th><i class="fa fa-trash-o" aria-hidden="true"></i></th>
						</tfoot>


				</table>
				</div>
				<div class="delete all-data">
					<button class="btn-delete-list" name="btn-delete-check" type="button" onclick="hapus_checklist()" >Hapus Terpilih</button>
					<?php
						if($this->session->flashdata('msgg')){
							echo "<script> alert('".$this->session->flashdata('msgg')."'); </script>";
						}
					?>
					<script type="text/javascript" language="javascript">
						function hapus_checklist(){

							var selected_value = new Array(); // initialize empty array
							var confirmation = confirm("Apakah anda yakin menghapus data yang anda pilih ?");
							 if(confirmation){
								$(".check:checked").each(function(){
										selected_value.push($(this).val());
									});
									console.log(selected_value);
								$.ajax({
										type: "POST",
										url: "<?php echo base_url(); ?>Standar7/dosenhapuschecklist",
										data: {result:JSON.stringify(selected_value)},

										success: function(data){
										console.log(data);
										window.location.href='<?php echo base_url(); ?>Standar7/dosen';
										},

										error: function(xhr, status, errorThrown){
										alert( xhr.status);
										alert( xhr.errorThrown);
										alert( xhr.responseText);
										console.log(xhr.responseText);
										}
								});
							 }
						}
					</script>

					<button class="btn-delete-all" name="btn-delete-all" onclick="ConfirmDeleteall()"  id="delete-btn" type="submit">Hapus Semua Data</i></button>

					<script type="text/javascript">
							var baseUrl='<?php echo base_url(); ?>standar7/dosenhapussemua';
							function ConfirmDeleteall()
								{
										var msg = 'Apakah anda yakin menghapus semua data Dosen ?';
										if (confirm(msg)){
										   location.href=baseUrl;
										}
								}
					</script>

				</div>
			</div>
		</div>

		<div class="footer">
			<?php
			include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/footer.php";
			?>
		</div>

	</body>



</html>
