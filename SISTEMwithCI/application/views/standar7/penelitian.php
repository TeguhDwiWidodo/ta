<?php
if(!isset($_SESSION['nama'])){
      header("location:" . base_url());
      exit();
   }
?>

<!DOCTYPE html>
<html>
	<head>
		<title>Akreditasi | Penelitian</title>
		<script type = 'text/javascript' src="<?php echo base_url(); ?>js/jquery-1.12.4.js"></script>
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/mainlayout.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/penelitian.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/dosen.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/accordion.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/header.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/sidebar.css">

		<script type = 'text/javascript' src="<?php echo base_url(); ?>js/jquery.dataTables.js"></script>
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/jquery.dataTables.css">
		<script type = 'text/javascript' >
		(function ($){
			$(document).ready(function() {
				$('#table-view-data-tim-akreditasi').dataTable({
					"order": [[ 0, 'asc' ]],

				});
			});
		})(jQuery);
		</script>
    <script type = 'text/javascript' >
		(function ($){
			$(document).ready(function() {
				$('#table-view-data-peldosen').dataTable({
					"order": [[ 0, 'asc' ]],

				});
			});
		})(jQuery);
		</script>

    <script type = 'text/javascript' >
		(function ($){
			$(document).ready(function() {
				$('#view-HaKI').dataTable({
					"order": [[ 0, 'asc' ]],

				});
			});
		})(jQuery);
		</script>
	</head>

	<body>

		<div class="header">
			<h1><a href="<?php echo base_url(); ?>dashboard">Akreditasi SI</a></h1>
			<?php
			include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/header.php";
			?>
		</div>





		<div class="sidebar">
				<?php
			include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/sidebar.php";
			?>
		</div>





		<div class="main-layout">


			<div class="main-content">
			<div class="menu-breadcrumb">
				<ul class="breadcrumb">
					<li><a href="<?php echo base_url(); ?>dashboard" id="menu-breadcrumb">Dashboard</a></li>
					<li>Penelitian</li>
				</ul>
			</div>
			 <div class="menu-head">
				<ul>
					<li><a href="<?php echo base_url(); ?>standar7/penelitian" id="menu-head" style="background:#d9d9d9;color:black;">Penelitian</a></li>
					<li><a href="<?php echo base_url(); ?>standar7/abdimas" id="menu-head">Pengmas</a></li>
					<li><a href="<?php echo base_url(); ?>standar7/kerjasama" id="menu-head">Kerjasama</a></li>

				</ul>
			</div>
			<div class="menu-subhead">
				<ul>
					<li><a href="<?php echo base_url(); ?>standar7/penelitiandosen" id="menu-subhead">Penelitian Dosen SI</a></li>
					<li><a href="<?php echo base_url(); ?>standar7/penelitiandosenta" id="menu-subhead">Penelitian (TA)</a></li>
					<li><a href="<?php echo base_url(); ?>standar7/publikasi" id="menu-subhead">Publikasi</a></li>
					<li><a href="<?php echo base_url(); ?>standar7/haki" id="menu-subhead">HaKI</a></li>
					<li><a href="<?php echo base_url(); ?>standar7/dosen" id="menu-subhead">Dosen</a></li>

				</ul>
			</div>

			<div class="sub-header">
			<h2> Standar 7 : Penelitian</h2>
			</div>

			</div>
      <div class="kerjasama_layout">


          <div class="711penelitian">
            <button class="accordion">7.1.1 Pedoman Penilaian Penelitian Dosen</button>
            <div class="panel">
              <div class="panel-pedoman">
                <div style="">
                  <div style="width:30%;float:left;padding:1%;">
                  <h4>Pedoman Penilaian</h4>
                  <label style="font-size:80%;text-align:justify;">Jumlah kegiatan Pengabdian kepada Masyarakat (PkM) yang dilakukan oleh dosen tetap yang bidang keahliannya sesuai dengan PS</label>
                  </br>
                  </br>
                  <h4>Deskriptor</h4>
                  <label style="font-size:80%;text-align:justify;">Jumlah penelitian yang sesuai dengan bidang keilmuan PS, yang dilakukan oleh dosen tetap yang bidang keahliannya sama dengan PS, selama 3 tahun.
                  </br> Penilaian dilakukan dengan penghitungan berikut:
                      NK = Nilai kasar =
                      <table style="text-align:center;border: 1px solid gray;">
                          <tbody>
                            <tr >
                               <td style="border:hidden;" rowspan="4">NK =</td>
                             </tr>
                          <tr >
                             <td style="border:hidden;">4xn<sub>a</sub> + 2xn<sub>b</sub> + n<sub>c</sub></td>
                           </tr>
                           <tr >
                             <td style="border:hidden;"><hr></td>
                           </tr>
                            <tr >
                              <td style="border:hidden;">f</td>
                          </tr>
                        </tbody>
                      </table>
                    </label>
                  </div>
                  <div style="padding:1%;">
                    <label><b>Keterangan</b></label>
                      <table style="margin-left:0;font-size:80%;text-align:justify;">
                          <tbody >
                            <tr>
                               <td style="border:hidden;">n<sub>a</sub></td>
                               <td style="border:hidden;">=</td>
                               <td style="border:hidden;text-align:justify;"> Jumlah penelitian dengan biaya luar negeri yang sesuai bidang ilmu</sub></td>
                           </tr>
                           <tr>
                              <td style="border:hidden;">n<sub>b</sub></td>
                              <td style="border:hidden;">=</td>
                              <td style="border:hidden;text-align:left;">Jumlah penelitian dengan biaya luar yang sesuai bidang ilmu</sub></td>
                          </tr>
                          <tr>
                             <td style="border:hidden;">n<sub>c</sub></td>
                             <td style="border:hidden;">=</td>
                             <td style="border:hidden;text-align:left;">Jumlah penelitian dengan biaya dari PT/sendiri yang sesuai bidang ilmu</sub></td>
                         </tr>
                         <tr>
                            <td style="border:hidden;">f</td>
                            <td style="border:hidden;">=</td>
                            <td style="border:hidden;text-align:left;">Jumlah dosen tetap yang bidang keahliannya sesuai dengan PS</sub></td>
                        </tr>
                        </tbody>
                      </table>
                    </br>
                      <label><b>Harkat dan Peringkat</b></label>
                      <table class="table-pedoman"; style="position:relative;text-align:center;margin-left:0;">
                        <tr>
                          <th style="background-color:green;font-size:80%;padding:1%;width:25%;border:1px solid gray;">Sangat Baik</th>
                          <th style="background-color:#00ff00;font-size:80%;padding:1%;width:12%;border:1px solid gray;">Baik</th>
                          <th style="background-color:orange;font-size:80%;padding:1%;width:11%;border:1px solid gray;">Cukup</th>
                          <th style="background-color:#ff3300;font-size:80%;padding:1%;width:12%;border:1px solid gray;">Kurang</th>
                          <th style="background-color:red;font-size:80%;padding:1%;width:25%;border:1px solid gray;">Sangat Kurang</th>
                        </tr>
                           <tr>
                              <td style="border:1px solid gray;">4</sub></td>
                              <td style="border:1px solid gray;">3</sub></td>
                              <td style="border:1px solid gray;">2</sub></td>
                              <td style="border:1px solid gray;">1</sub></td>
                              <td style="border:1px solid gray;">0</sub></td>
                          </tr>
                          <tr>
                             <td style="text-align:center;font-size:80%;padding:1%;border:1px solid gray;">Jika NK ≥ 2, maka skor = 4</td>
                             <td colspan="3" style="text-align:center;font-size:80%;padding:1%;border:1px solid gray;">Jika 0 < NK < 2, maka skor = (1.5 x NK) + 1</td>
                             <td style="text-align:center;font-size:80%;padding:1%;border:1px solid gray;">Jika NK = 0, maka skor = 0</td>
                          </tr>
                      </table>
                    </br>
                  </div>
                </div>
              </div>

            </div>

            <div style="border:1px solid #3399ff; width:100%;background:white;overflow:hidden; " class="smlsi-nilai-dl">
              <div style=" height:20px;padding:1%;padding-left:1.5%;background:#3399ff;">
                <label >7.3.1 Penilaian Jumlah Penelitian Dosen </label>
              </div>
              <div style="position:relative;float:left;width:40%;padding:2%;text-:center;" class="panel-dtabdimas">
              <label><b>Tabel Jumlah Penelitian Dosen</b></label>
              <table class="jlmh-peldos" style="font-size: 80%;border:1px solid gray;"  class="display">
                  <tr>
                      <th style="width:50%;text-align:center;border:1px solid gray;">Sumber Pembiayaan</th>
                      <th style="width:15%;background:#ffcc00;text-align:center;border:1px solid gray;">TS-2</th>
                      <th style="width:15%;background:#66ff66;text-align:center;border:1px solid gray;">TS-1</th>
                      <th style="width:15%;background:#66ccff;text-align:center;border:1px solid gray;">TS</th>
                  </tr>

                    <tbody style="text-align:center;">
                      <tr >
                        <td style="text-align:left;padding:0.5%;border:1px solid gray;">Pembiayaan sendiri oleh peneliti  </td>
                        <td style="border:1px solid gray;"><?php  echo $biaya_sendiri_ts2; ?></td>
                        <td style="border:1px solid gray;"><?php  echo $biaya_sendiri_ts1; ?></td>
                        <td style="border:1px solid gray;"><?php  echo $biaya_sendiri_ts; ?></td>
                      </tr>
                      <tr>
                        <td style="text-align:left;padding:0.5%;border:1px solid gray;">PT yang bersangkutan</td>
                        <td style="border:1px solid gray;"><?php echo $pt_bersangkutan_ts2; ?></td>
                        <td style="border:1px solid gray;"><?php echo $pt_bersangkutan_ts1; ?></td>
                        <td style="border:1px solid gray;"><?php echo $pt_bersangkutan_ts; ?></td>
                      </tr>
                      <tr>
                        <td style="text-align:left;padding:0.5%;border:1px solid gray;">Depdiknas</td>
                        <td style="border:1px solid gray;"><?php echo $depdiknas_ts2; ?></td>
                        <td style="border:1px solid gray;"><?php echo $depdiknas_ts1; ?></td>
                        <td style="border:1px solid gray;"><?php echo $depdiknas_ts; ?></td>
                      </tr>
                      <tr>
                        <td style="text-align:left;padding:0.5%;border:1px solid gray;">Institusi dalam negeri di luar Depdiknas</td>
                        <td style="border:1px solid gray;"><?php echo $luar_depdiknas_ts2; ?></td>
                        <td style="border:1px solid gray;"><?php echo $luar_depdiknas_ts1; ?></td>
                        <td style="border:1px solid gray;"><?php echo $luar_depdiknas_ts; ?></td>
                      </tr>
                      <tr>
                        <td style="text-align:left;padding:0.5%;border:1px solid gray;">Institusi luar negeri</td>
                        <td style="border:1px solid gray;"><?php echo $biaya_luar_negeri_ts2; ?></td>
                        <td style="border:1px solid gray;"><?php echo $biaya_luar_negeri_ts1; ?></td>
                        <td style="border:1px solid gray;"><?php echo $biaya_luar_negeri_ts; ?></td>
                      </tr>


                    </tbody>
              </table>
              <form action ="<?php echo base_url(); ?>Reportstd/rptpenelitian711" target="_blank">
                     <input class="btn" style="margin-left:0;" type="submit" value="Cetak" />
                 </form>
            </div>

                <div style="padding:2%;padding-left:5.5%;">
                  <label><b>Simulasi Nilai</b></label>
                  <?php

                  $na1= $biaya_luar_negeri * 4;
                  $pnb = $depdiknas + $luar_depdiknas;
                  $nb1= $pnb * 2;
                  $nc1= $biaya_sendiri + $pt_bersangkutan ;
                  $lk1 = $na1 + $nb1 + $nc1;



                  if(  $lk1  == null || $jml_dosen==null){

                    $nilai_total2 =0;
                  } else {

                    $nilai_total2 = $lk1 / $jml_dosen;
                  }
                  ?>

                  <table style="margin-left:0;font-size:80%;">
                    <tr>
                      <td>Na x 4</td>
                      <td>:</td>
                      <td><?php echo   $na1; ?></td>
                    </tr>
                    <tr>
                      <td>Nb x 2</td>
                      <td>:</td>
                      <td><?php echo   $nb1 ; ?></td>
                    </tr>
                    <tr>
                      <td>Nc</td>
                      <td>:</td>
                      <td><?php echo   $nc1 ; ?></td>
                    </tr>
                    <tr>
                      <td>Total dosen</td>
                      <td>:</td>
                      <td><?php echo $jml_dosen ; ?></td>
                    </tr>
                    <tr>
                      <td>Nilai kasar</td>
                      <td>:</td>
                      <td><?php echo round($nilai_total2, 4)  ; ?></td>
                    </tr>
                    <tr>
                      <td>Harkat dan Peringkat</td>
                      <td>:</td>
                      <td><?php

                      $satu = 1;

                      if($nilai_total2 > 2){
                      $harkat1 = 4;
                      echo $harkat1 ;

                    } else if($nilai_total2 < 2 && $nilai_total2 > 0){

                        $harkat1 = 1.5 * $nilai_total2  + $satu;
                       echo  round($harkat1, 4);

                     } else if($nilai_total2 == 0){
                        $harkat1 = 0;
                      echo   $harkat1;

                      }
                        ?>
                           <input type="hidden"  name="skor1" value="<?php echo  $harkat1 ?>" >
                      </td>
                    </tr>
                    <tr>
                      <td>Bobot</td>
                      <td>:</td>
                      <td><label id="bobot12">  3.75 </label></td>
                    </tr>
                    <tr>
                      <td>Nilai simulasi</td>
                      <td>:</td>
                      <td><label id ="simulasinilai2"><?php echo round(($harkat1 * 3.75), 4); ?></label></td>
                    </tr>
                  </table>
                  <button style="margin-top:2%;display: visible;left: 10;" name="btn-upload" type="button"  onclick="save_hasil5()" id="save_hasil1" class="save btn" value="save">Simpan </button>
                  <script>
                  function save_hasil5(){
                    var nilai1 = document.getElementById("simulasinilai2").innerHTML;
                    var sim_nilai = $("input[name=skor1]").val();
                    var bobot = document.getElementById("bobot12").innerHTML;

                  var id1 = "7.1.1";
                  $.ajax({

                      type: "POST",
                      url: "<?php echo base_url(); ?>Standar7/simulasinilai",
                  data: { nilai1 : nilai1, sim_nilai:sim_nilai, id1:id1, bobot:bobot},

                      success: function(data){
                  console.log(data);
                  alert("Sukses input data nilai, hasil "+nilai1);
                        },

                      error: function(xhr, status, errorThrown){
                      alert( xhr.status);
                      alert( xhr.errorThrown);
                      alert( xhr.responseText);
                      console.log(xhr.responseText);
                      }
                  });

                  }
                  </script>
                </div>
              </div>

              </br>
              </br>
          </div>




          <div class="712penelitian">
            <button class="accordion">7.1.2 Tugas Akhir Mahasiswa dalam Penelitian Dosen</button>
            <div class="panel">
              <div class="panel-pedoman">
                <div style="">
                  <div style="width:30%;float:left;padding:1%;">
                  <h4>Pedoman Penilaian</h4>
                  <label style="font-size:80%;text-align:justify;">Keterlibatan mahasiswa yang melakukan tugas akhir dalam penelitian dosen</label>
                  </br>
                  </br>
                  <h4>Deskriptor</h4>
                  <label style="font-size:80%;text-align:justify;">Keterlibatan mahasiswa yang melakukan tugas akhir dalam penelitian dosen</label>
                  </div>
                  <div style="padding:1%;">
                    <label><b>Keterangan</b></label>
                    <br>
                      <label style="font-size:80%;text-align:justify;" >PD =  Persentase mahasiswa yang melakukan tugas akhir dalam penelitian dosen</label>
                    </br>
                    </br>
                      <label><b>Harkat dan Peringkat</b></label>
                      <table class="table-pedoman"; style="position:relative;text-align:center;margin-left:0;">
                        <tr>
                          <th style="background-color:green;font-size:80%;padding:1%;width:25%;border:1px solid gray;">Sangat Baik</th>
                          <th style="background-color:#00ff00;font-size:80%;padding:1%;width:12%;border:1px solid gray;">Baik</th>
                          <th style="background-color:orange;font-size:80%;padding:1%;width:11%;border:1px solid gray;">Cukup</th>
                          <th style="background-color:#ff3300;font-size:80%;padding:1%;width:12%;border:1px solid gray;">Kurang</th>
                          <th style="background-color:red;font-size:80%;padding:1%;width:25%;border:1px solid gray;">Sangat Kurang</th>
                        </tr>
                           <tr>
                              <td style="border:1px solid gray;">4</sub></td>
                              <td style="border:1px solid gray;">3</sub></td>
                              <td style="border:1px solid gray;">2</sub></td>
                              <td style="border:1px solid gray;">1</sub></td>
                              <td style="border:1px solid gray;">0</sub></td>
                          </tr>
                          <tr>
                             <td style="text-align:center;font-size:80%;padding:1%;border:1px solid gray;">Jika PD ≥ 25%, maka skor = 4.</td>
                             <td colspan="3" style="text-align:center;font-size:80%;padding:1%;border:1px solid gray;">Jika 0% < PD < 25%, maka skor = 1 + (12 x PD).</td>
                             <td style="text-align:center;font-size:80%;padding:1%;border:1px solid gray;">Jika PD = 0%, maka skor = 0.</td>
                          </tr>
                      </table>
                    </br>
                  </div>
                </div>
              </div>

            </div>

            <div style="border:1px solid #3399ff; width:100%;background:white; " class="smlsi-nilai-dl">
              <div style=" height:20px;padding:1%;padding-left:1.5%;background:#3399ff;">
                <label >7.1.2 penilaian Tugas Akhir Mahasiswa dalam Penelitian Dosen </label>
              </div>
                <div style="padding:1%;padding-left:5.5%;">
                  <label><b>Simulasi Nilai</b></label></br></br>
                    <table style="margin-left:0;font-size:80%;">
                      <tr>
                        <td>Jumlah penelitian dosen yang dijadikan Ta oleh mahasiswa  </td>
                        <td>:</td>
                        <td><?php echo $jumlah_mahasiswa_peldosta; ?></td>
                      </tr>
                      <tr>
                        <td>Jumlah TA Mahasiswa  </td>
                        <td>:</td>
                        <td><?php echo $jumlah_mahasiswa_pelta; ?></td>
                      </tr>
                      <tr>
                        <td>Hitung rumus</td>
                        <td>:</td>
                        <td><?php


                        if ($jumlah_mahasiswa_pelta == null || $jumlah_mahasiswa_peldosta == null){
                          $hitung = 0;

                        } else {

                          $hitung = $jumlah_mahasiswa_peldosta/$jumlah_mahasiswa_pelta * 100;
                        }

                        echo round($hitung, 4); ?> %</td>
                      </tr>
                      <tr>
                        <td>Bobot</td>
                        <td>:</td>
                        <td><?php $bobot = 1.88; echo $bobot; ?></td>
                      </tr>
                      <tr>
                        <td>Harkat dan peringkat</td>
                        <td>:</td>
                        <td><?php if($hitung >= 25){

                          echo $skor=4 ;

                        } else if ($hitung < 25 && $hitung > 0) {
                        $skor = (12*($hitung/100)) + 1;
                        echo round($skor, 4);

                      } else if($hitung == 0) {
                        $skor = 0;
                        echo $skor;
                        }


                      ?>
                        <input type="hidden"  name="skor2" value="<?php echo  $skor ?>" >
                        </td>
                      </tr>
                      <tr>
                        <td>Simulasi nilai</td>
                        <td>:</td>
                        <td><label id="hasil7"><?php echo $skor * $bobot; ?></label></td>
                      </tr>
                    </table>
                    <form action ="<?php echo base_url(); ?>Reportstd/rptpenelitian712" target="_blank">
                           <input class="btn" style="margin-left:0;" type="submit" value="Cetak" />
                       </form>
                      <button style="display: visible;left: 10;" name="btn-upload" type="button"  onclick="save_hasil8()" id="save_hasil1" class="save btn" value="save">Simpan </button>
                              <script>
                                        function save_hasil8(){
                                          var nilai1 = document.getElementById("hasil7").innerHTML;
                                          var sim_nilai = $("input[name=skor2]").val();
                                          var bobot = 1.88;
                                        var id1 = "7.1.2";
                                        $.ajax({

                                            type: "POST",
                                            url: "<?php echo base_url(); ?>Standar7/simulasinilai",
                                            data: { nilai1 : nilai1, sim_nilai:sim_nilai, id1:id1, bobot:bobot},

                                            success: function(data){
                                        console.log(data);
                                        alert("Sukses input data nilai, hasil "+nilai1);
                                              },

                                            error: function(xhr, status, errorThrown){
                                            alert( xhr.status);
                                            alert( xhr.errorThrown);
                                            alert( xhr.responseText);
                                            console.log(xhr.responseText);
                                            }
                                        });

                                        }
                                        </script>
                </div>
              </div>

              </br>
              </br>
          </div>





          <div class="713penelitian">
            <button class="accordion">7.1.3 Pedoman Penilaian Publikasi</button>
            <div class="panel">
              <div class="panel-pedoman">
                <div style="">
                  <div style="width:30%;float:left;padding:1%;">
                  <h4>Pedoman Penilaian</h4>
                  <label style="font-size:80%;text-align:justify;">Jumlah artikel ilmiah yang dihasilkan oleh dosen tetap yang bidang keahliannya sesuai dengan PS per tahun, selama tiga tahun.</label>
                  </br>
                  </br>
                  <h4>Deskriptor</h4>
                  <label style="font-size:80%;text-align:justify;">Jumlah artikel ilmiah yang dihasilkan oleh dosen tetap yang bidang keahliannya sama dengan PS, selama 3 tahun.
                  </br> Penilaian dilakukan dengan penghitungan berikut:
                      NK = Nilai kasar =
                      <table style="text-align:center;border: 1px solid gray;">
                          <tbody>
                            <tr >
                               <td style="border:hidden;" rowspan="4">NK =</td>
                             </tr>
                          <tr >
                             <td style="border:hidden;">4xn<sub>a</sub> + 2xn<sub>b</sub> + n<sub>c</sub></td>
                           </tr>
                           <tr >
                             <td style="border:hidden;"><hr></td>
                           </tr>
                            <tr >
                              <td style="border:hidden;">f</td>
                          </tr>
                        </tbody>
                      </table>
                    </label>
                  </div>
                  <div style="padding:1%;">
                    <label><b>Keterangan</b></label>
                      <table style="margin-left:0;font-size:80%;text-align:justify;">
                          <tbody >
                            <tr>
                               <td style="border:hidden;">n<sub>a</sub></td>
                               <td style="border:hidden;">=</td>
                               <td style="border:hidden;text-align:justify;">Internasional</sub></td>
                           </tr>
                           <tr>
                              <td style="border:hidden;">n<sub>b</sub></td>
                              <td style="border:hidden;">=</td>
                              <td style="border:hidden;text-align:left;">Nasional</sub></td>
                          </tr>
                          <tr>
                             <td style="border:hidden;">n<sub>c</sub></td>
                             <td style="border:hidden;">=</td>
                             <td style="border:hidden;text-align:left;">Lokal</sub></td>
                         </tr>
                         <tr>
                            <td style="border:hidden;">f</td>
                            <td style="border:hidden;">=</td>
                            <td style="border:hidden;text-align:left;">Jumlah dosen tetap yang bidang keahliannya sesuai dengan PS</sub></td>
                        </tr>
                        </tbody>
                      </table>
                    </br>
                      <label><b>Harkat dan Peringkat</b></label>
                      <table class="table-pedoman"; style="position:relative;text-align:center;margin-left:0;">
                        <tr>
                          <th style="background-color:green;font-size:80%;padding:1%;width:25%;border:1px solid gray;">Sangat Baik</th>
                          <th style="background-color:#00ff00;font-size:80%;padding:1%;width:12%;border:1px solid gray;">Baik</th>
                          <th style="background-color:orange;font-size:80%;padding:1%;width:11%;border:1px solid gray;">Cukup</th>
                          <th style="background-color:#ff3300;font-size:80%;padding:1%;width:12%;border:1px solid gray;">Kurang</th>
                          <th style="background-color:red;font-size:80%;padding:1%;width:25%;border:1px solid gray;">Sangat Kurang</th>
                        </tr>
                           <tr>
                              <td style="border:1px solid gray;">4</sub></td>
                              <td style="border:1px solid gray;">3</sub></td>
                              <td style="border:1px solid gray;">2</sub></td>
                              <td style="border:1px solid gray;">1</sub></td>
                              <td style="border:1px solid gray;">0</sub></td>
                          </tr>
                          <tr>
                             <td style="text-align:center;font-size:80%;padding:1%;border:1px solid gray;">Jika NK ≥ 6, maka skor = 4.</td>
                             <td colspan="3" style="text-align:center;font-size:80%;padding:1%;border:1px solid gray;">Jika 0 < NK <  6, maka skor = 1 + (NK / 2).</td>
                             <td style="text-align:center;font-size:80%;padding:1%;border:1px solid gray;">Jika NK = 0, maka skor = 0.</td>
                          </tr>
                      </table>
                    </br>
                  </div>
                </div>
              </div>

            </div>

            <div style="border:1px solid #3399ff; width:100%;background:white; " class="smlsi-nilai-dl">
              <div style=" height:20px;padding:1%;padding-left:1.5%;background:#3399ff;">
                <label >7.1.3 Penilaian Publikasi </label>
              </div>


              <button style="background:#333333;padding-left:5%;color:white;" class="accordion">Data Publikasi</button>
              <div class="panel">
                <div class="tb-view-publikasi" style="padding:2%;">

                              <table  id="table-view-data-peldosen" class="display"  width="100%" cellspacing="0">

                                    <thead style="font-size: 70%;padding:0.5%;" class="table head-peldosen">
                                      <tr>
                                      <th rowspan="2">No</th>

                                      <th rowspan="2">Judul</th>

                                      <th rowspan="2">Penulis Utama</th>
                                      <th rowspan="2" >Penulis Tambahan</th>
                                      <th rowspan="2">Tanggal</th>

                                      <th colspan="4" >jumlah Dosen SI </th>

                                    </tr>
                                    <tr>


                                      <th  >Lokal   </th>

                                      <th >Nasional   </th>

                                      <th >Internasional   </th>
                                    </tr>
                                    </thead>

                                    <tbody style="font-size: 60%;" class="table body-peldosen" role="alert" aria-live="polite" aria-relevant="all">

                                      <?php
                                          $no = 0;

                                          foreach($publikasi as $row){
                                             $no++;
                                             if($row->sts_valid == "valid"){
                                          ?>
                                            <tr >
                                              
                                              <td ><?php echo $no;?></td>
                                                    <td ><?php echo $row->judul ?>  </td>
                                                    <td ><?php echo $row->penulis_utm ?> </td>
                                              <td ><?php echo $row->penulis_tmbh ?> </td>
                                              <td ><?php echo $row->tanggal ?> </td>

                                                  <td ><?php

                                                  if($row->skl_publikasi == "Lokal" ){
                                                    echo $row->jlm_dosen_si;

                                                  }

                                                    ?></td>
                                                    <td ><?php

                                                    if($row->skl_publikasi == "Nasional"){
                                                      echo $row->jlm_dosen_si;

                                                    }
                                                      ?></td>


                                                      <td ><?php

                                                      if($row->skl_publikasi == "Internasional" ){
                                                        echo $row->jlm_dosen_si;

                                                      }

                                                        ?></td>


                                                        <?php
                                                       }
                                                      } ?>

                                                    </tbody>
                                                    <tfoot style="font-size: 70%;" class="table head-peldosen">
                                                      <td colspan="5">Jumlah</td>
                                                      <td >Nc : <?php echo $data_dosen_lokal ?></td>
                                                      <td >Nb : <?php echo $data_dosen_nasional ?></td>
                                                      <td >Na : <?php echo $data_dosen_internasional ?></td>
                                                    </tfoot>
                                        </table>
                                        <form action ="<?php echo base_url(); ?>Reportstd/rptpenelitian713" target="_blank">
                                               <input class="btn" style="margin-left:0;" type="submit" value="Cetak" />
                                           </form>

              </div>
            </div>

                <div style="padding:1%;padding-left:5.5%;">
                  <label><b>Simulasi Nilai</b></label></br>
                <?php
                $na= $data_dosen_internasional * 4;
                $nb= $data_dosen_nasional * 2;
                $nc= $data_dosen_lokal;
                $lk = $na + $nb + $nc;
                ?>

                <table style="margin-left:0;font-size:80%;">
                  <tr>
                    <td>Na x 4</br></td>
                    <td>:</td>
                    <td><?php echo $na ; ?></td>
                  </tr>
                  <tr>
                    <td>Nb x 2</td>
                    <td>:</td>
                    <td><?php echo $nb ; ?></td>
                  </tr>
                  <tr>
                    <td>Nc</td>
                    <td>:</td>
                    <td><?php echo $nc ; ?></td>
                  </tr>
                  <tr>
                    <td>Total dosen</td>
                    <td>:</td>
                    <td><?php echo $jml_dosen ; ?></td>
                  </tr>
                  <tr>
                    <td>Nilai Kasar</td>
                    <td>:</td>
                    <td><?php

                                                if ($lk == null || $jml_dosen  ==null){
                       echo "0"  ;

                                                } else {
                     echo round(($lk / $jml_dosen), 4)  ;

                                                }

                                               ?>
                    </td>
                  </tr>
                  <tr>
                    <td>Harkat dan Peringkat</td>
                    <td>:</td>
                    <td><?php



                    if ($lk == null || $jml_dosen  ==null){
                    $nilai_total1 =  "0"  ;

                    } else {
                    $nilai_total1 = $lk / $jml_dosen;

                    }


                    $satu = 1;

                    if($nilai_total1 > 6){
                    $harkat = 4;
                    echo $harkat ;

                    } else if($nilai_total1 < 6 && $nilai_total1 > 0 ){

                      $harkat = $nilai_total1 / 2 + $satu;
                     echo  round($harkat, 4);

                    }  else if($nilai_total1 == 0){
                      $harkat = 0;
                    echo   $harkat;

                    }
                    ?>

                  <input type="hidden"  name="skor3" value="<?php echo  $harkat ?>" ></td>
                  </tr>
                  <tr>
                    <td>Bobot</td>
                    <td>:</td>
                    <td><label id="bobot14"> 3.75 </label></td>
                  </tr>
                  <tr>
                    <td>Nilai Simulasi</td>
                    <td>:</td>
                    <td><label id="simulasinilai"><?php echo round(($harkat * 3.75),4); ?></label></td>
                  </tr>
                </table>

                <button style="display: visible;left: 10;" name="btn-upload" type="button"  onclick="save_hasil3()" id="save_hasil1" class="save btn" value="save">Simpan </button>
                <script>
                function save_hasil3(){
                  var nilai1 = document.getElementById("simulasinilai").innerHTML;
                  var sim_nilai = $("input[name=skor3]").val();
                var id1 = "7.1.3";

                var bobot = document.getElementById("bobot14").innerHTML;


                $.ajax({

                    type: "POST",
                    url: "<?php echo base_url(); ?>Standar7/simulasinilai",
                data: { nilai1 : nilai1, sim_nilai:sim_nilai, id1:id1, bobot:bobot},

                    success: function(data){
                console.log(data);
                alert("Sukses input data nilai, hasil "+nilai1);
                      },

                    error: function(xhr, status, errorThrown){
                    alert( xhr.status);
                    alert( xhr.errorThrown);
                    alert( xhr.responseText);
                    console.log(xhr.responseText);
                    }
                });

                }
                </script>

                  </div>
              </div>

              </br>
              </br>
          </div>
          <div class="714penelitian">
            <button class="accordion">7.1.4 Pedoman Penilaian HaKI</button>
            <div class="panel">
              <div class="panel-pedoman" style="padding:2%;width:25%;float:left;">
                <h4>Pedoman Penilaian</h4>
                  <label style="font-size:80%;text-align:justify;">Karya-karya PS/institusi yang telah memperoleh perlindungan Hak atas Kekayaan Intelektual (HaKI) dalam tiga tahun terakhir.</label>
                  </br>
                  </br>
                <h4>Deskriptor</h4>
                  <label style="font-size:80%;text-align:justify;">Karya-karya PS/institusi yang telah memperoleh perlindungan Hak atas Kekayaan Intelektual (HaKI) dalam tiga tahun terakhir.</label>
                  </br>
                </div>
                <div style="padding:2%;width:65%;float:left;">
                    <label><b>Harkat dan Peringkat</b></label>

                      <table class="table-pedoman"; style="text-align:center;">
                        <tr>
                          <th style="background-color:green;font-size:80%;padding:1%;width:20%;border:1px solid gray;">Sangat Baik</th>
                          <th style="background-color:#00ff00;font-size:80%;padding:1%;width:20%;border:1px solid gray;">Baik</th>
                          <th style="background-color:orange;font-size:80%;padding:1%;width:20%;border:1px solid gray;">Cukup</th>
                          <th style="background-color:#ff3300;font-size:80%;padding:1%;width:20%;border:1px solid gray;">Kurang</th>
                          <th style="background-color:red;font-size:80%;padding:1%;width:20%;border:1px solid gray;">Sangat Kurang</th>
                        </tr>
                           <tr>
                              <td style="border:1px solid gray;">4</sub></td>
                              <td style="border:1px solid gray;">3</sub></td>
                              <td style="border:1px solid gray;">2</sub></td>
                              <td style="border:1px solid gray;">1</sub></td>
                              <td style="border:1px solid gray;">0</sub></td>
                          </tr>
                          <tr>
                            <td style="border:1px solid gray;text-align:left;font-size:80%;padding:1%;">Dua atau lebih karya yang memperoleh HaKI</td>
                            <td style="border:1px solid gray;text-align:left;font-size:80%;padding:1%;">Satu yang memperoleh HaKI</td>
                            <td style="border:1px solid gray;text-align:left;font-size:80%;padding:1%;">Tidak ada karya dosen tetap yang memperoleh HaKI</td>
                            <td style="border:1px solid gray;text-align:left;font-size:80%;padding:1%;">Tidak ada skor satu</td>
                            <td style="border:1px solid gray;text-align:left;font-size:80%;padding:1%;">Tidak ada skor nol</td>
                          </tr>
                      </table>

              </div>

            </div>

            <div style="border:1px solid #3399ff; width:100%;background:white; " class="smlsi-nilai-dl">
              <div style=" height:20px;padding:1%;padding-left:1.5%;background:#3399ff;">
                <label >7.1.4 Penilaian HaKI</label>
              </div>
              <button style="background:#333333;padding-left:5%;color:white;" class="accordion">Data HaKI</button>
              <div class="panel">
                <div class="data-haki" style="width:80%;padding:1%;">
                  <label><b>Data HaKI</b></label>
                  <table id="view-HaKI"  class="display">
                    <thead style="font-size: 80%;" class="table head-peldosen">
                    <th>No</th>
                    <th>Karya</th>
                    <th>lampiran</th>

                    </thead>

                        <tbody style="font-size: 80%;">

                          <?php
                          foreach($result_haki as $row){

                           ?>
                          <tr>
                            <td>
                              <?php  echo $row->id; ?>
                            </td>
                            <td>
                              <?php  echo $row->karya; ?>
                            </td>
                            <td>

                            <p><a target="_blank" href="<?php echo base_url(); ?>uploads/dokumen_kerjasama_std7/<?php echo $row->file_name ?>"><?php echo $row->file_name?> </a></p>
                          </td>
                          </tr>

                          <?php

                          } ?>

                        </tbody>
                  </table>
                  <form action ="<?php echo base_url(); ?>Reportstd/rptpenelitian714" target="_blank">
                         <input class="btn" style="margin-left:0;" type="submit" value="Cetak" />
                     </form>
                </div>
                </div>
                <div style="padding:2%;">
                  <label style="margin-left:5%;"><b>Simulasi Penilaian</b></label>
                </br>
                <hr>
                </br>
                <table>
                  <tr>
                    <td style="width:20%;"><label>Input Nilai</label></td>
                    <td style="width:1%;"><label>:</label>
                    </td>
                    <td style="width:20%;">
                      <label><span><input style="padding-left:1%;" id="hasil_option_luar1" type="radio" onchange="calculasi()"   name="nilai_option" value="4" />4 : Sangat Baik</span>
                    </label>
                    </td>
                    <td style="width:14%;">
                      <label title="Ada kerjasama dengan institusi di dalam negeri, cukup dalam jumlah.  Sebagian besar relevan dengan bidang keahlian PS" >
                        <span><input style="padding-left:1%;" id="hasil_option_luar2" type="radio" onchange="calculasi()" name="nilai_option" value="3"/>3 : Baik</span></label>

                    </td>
                    <td style="width:14%;">
                      <label title="Ada kerjasama dengan institusi di dalam negeri, kurang dalam jumlah. Sebagian besar relevan dengan bidang keahlian PS" >
                        <span><input style="padding-left:1%;" id="hasil_option_luar3" type="radio" onchange="calculasi()" name="nilai_option" value="2"/>2 : Cukup</span></label>

                    </td>
                    <td style="width:14%;" >
                      <label title="Belum ada atau tidak ada kerjasama" >
                        <span><input style="padding-left:1%;" id="hasil_option_luar4"  type="radio" onchange="calculasi()" name="nilai_option" value="1"/>1 : Kurang</span></label>

                    </td>
                    <td style="width:20%;">  <label title="(Tadak ada skor nol)" >
                        <span><input style="padding-left:1%;" id="hasil_option_luar5" type="radio" onchange="calculasi()" name="nilai_option"   value="0"/>0 : Sangat Kurang</span></label>

                    </td>
                  </tr>
                  <tr>
                    <td style="width:20%;"><label>Bobot</label></td>
                    <td style="width:1%;"><label>:</label></td>
                    <td colspan="6"><label>1.88</label></td>
                  </tr>
                  <tr>
                    <td style="width:20%;"><label>Hasil</label></td>
                    <td style="width:1%;"><label>:</label></td>
                    <td colspan="6"><label id="result1"></label></td>
                  </tr>

                </table>
                  </br>
                  <hr>



               <!-- <select id = "nilai1"  multiple = "multiple" onchange="calculasi1()" size = "2" width:"15%">
                    <option value = "0" >0</option>
                  <option value = "1" >1</option>
                  <option value = "2">2</option>
                  <option value = "3">3</option>
                  <option value = "4" selected>4</option> -->


                      <!-- <?php
                       foreach($nilai_dalam as $nd){

                        ?>
                       <label>hasil sebelumnya:</label> : <label><?php  echo $nd->nilai; ?></label> </br>
                       <?php
                       }
                        ?> -->
                       <button style="display: visible;left: 10;" name="btn-upload" type="button"  onclick="save_hasil1()" id="save_nilai" class="save btn" value="save" disabled="disabled">Simpan </button>
                     </br>
                     <script>
                        $("#hasil_option_luar1").change(function () {$("#save_nilai").prop("disabled", false);});
                        $("#hasil_option_luar2").change(function () {$("#save_nilai").prop("disabled", false);});
                        $("#hasil_option_luar3").change(function () {$("#save_nilai").prop("disabled", false);});
                        $("#hasil_option_luar4").change(function () {$("#save_nilai").prop("disabled", false);});
                        $("#hasil_option_luar5").change(function () {$("#save_nilai").prop("disabled", false);});
                       </script>

                       <script>
                       function calculasi(){
                         if( document.getElementById("hasil_option_luar1").checked == true){
                            var nilai1 = document.getElementById("hasil_option_luar1").value * 1.88;
                            document.getElementById("result1").innerHTML= nilai1;
                            document.getElementById("save_nilai").style.visibility = 'visible';


                         } else if( document.getElementById("hasil_option_luar2").checked == true){
                            var nilai1 = document.getElementById("hasil_option_luar2").value * 1.88;
                            document.getElementById("result1").innerHTML= nilai1;
                            document.getElementById("save_nilai").style.visibility = 'visible';


                         }else if( document.getElementById("hasil_option_luar3").checked == true){
                            var nilai1 = document.getElementById("hasil_option_luar3").value * 1.88;
                            document.getElementById("result1").innerHTML= nilai1;
                            document.getElementById("save_nilai").style.visibility = 'visible';


                         }else if( document.getElementById("hasil_option_luar4").checked == true){
                            var nilai1 = document.getElementById("hasil_option_luar4").value * 1.88;
                            document.getElementById("result1").innerHTML= nilai1;
                            document.getElementById("save_nilai").style.visibility = 'visible';


                         }else if( document.getElementById("hasil_option_luar5").checked == true){
                            var nilai1 = document.getElementById("hasil_option_luar5").value * 1.88;
                            document.getElementById("result1").innerHTML= nilai1;
                            document.getElementById("save_nilai").style.visibility = 'visible';

             }

                       }

                       function save_hasil1(){
                         var nilai1 = document.getElementById("result1").innerHTML;
                         var sim_nilai = $("input[name=nilai_option]:checked").val();
                       var id1 = "7.1.4";

                       var bobot = 1.88;

                       $.ajax({

                           type: "POST",
                           url: "<?php echo base_url(); ?>Standar7/simulasinilai",
                        data: { nilai1 : nilai1, sim_nilai:sim_nilai, id1:id1, bobot:bobot},
                           success: function(data){
                       console.log(data);
                     alert("Sukses input data nilai, hasil "+nilai1);
                             },

                           error: function(xhr, status, errorThrown){
                           alert( xhr.status);
                           alert( xhr.errorThrown);
                           alert( xhr.responseText);
                           console.log(xhr.responseText);
                           }
                       });

                       }
                       </script>


                </div>
              </div>

              </br>
              </br>
          </div>
			<script>
						var acc = document.getElementsByClassName("accordion");
						var i;

						for (i = 0; i < acc.length; i++) {
						  acc[i].onclick = function() {
							this.classList.toggle("active");
							var panel = this.nextElementSibling;
							if (panel.style.maxHeight){
							  panel.style.maxHeight = null;
							} else {
							  panel.style.maxHeight = panel.scrollHeight + "px";
							}
						  }
						}
			</script>
		</div>



		</div>

		<div class="footer">
			<?php
			include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/footer.php";
			?>
		</div>

	</body>



</html>
