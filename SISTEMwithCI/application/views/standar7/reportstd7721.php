<?php
if(!isset($_SESSION['nama'])){
      header("location:" . base_url());
      exit();
   }
?>

<!DOCTYPE html>
<html>
	<head>
		<title>Report</title>
		<script type = 'text/javascript' src="<?php echo base_url(); ?>js/jquery-1.12.4.js"></script>
		<script type = 'text/javascript' src="<?php echo base_url(); ?>js/jquery.dataTables.js"></script>

    <link rel="stylesheet" href="<?php echo base_url(); ?>css/pdf.css">

 <!--    <style>
      .std711{
        background: red;
      }
    </style> -->
	</head>

	<body>

    <h2 >STANDAR 7 : PENELITIAN, PELAYANAN/PENGABDIAN MASYARAKAT, DAN KERJASAMA</h2>

    <div class="7-2-1">
      <label>7.2   Kegiatan Pelayanan/Pengabdian kepada Masyarakat (PkM)</label><br>
      <label>7.2.1  Tuliskan jumlah kegiatan Pelayanan/Pengabdian kepada Masyarakat (*) yang sesuai dengan bidang keilmuan PS selama tiga tahun terakhir yang dilakukan oleh dosen tetap yang bidang keahliannya sesuai dengan PS dengan mengikuti format tabel berikut:</label>
      <br>
      <br>
      <table align="center" style="width:70%;" class="display1">
        <tbody >
        <tr class="display">
        <th class="display">Sumber Pembiayaan</th>
        <th class="display">TS-2</th>
        <th class="display">TS-1</th>
        <th class="display">TS</th>
      </tr>
      <tr >
      <th class="display">(1)</th>
      <th class="display">(2)</th>
      <th class="display">(3)</th>
      <th class="display">(4)</th>
      </tr>
              <tr>
                <td class="display">
                  Pembiayaan sendiri oleh peneliti
                </td >
                <td class="display2"><?php  echo $biaya_sendiri_abdimas_ts2; ?></td>
                <td class="display2"><?php  echo $biaya_sendiri_abdimas_ts1; ?></td>
                <td class="display2"><?php  echo $biaya_sendiri_abdimas_ts; ?></td>
                </td>
              </tr>
              <tr>
                <td class="display">
                  PT yang bersangkutan
                </td>
                <td class="display2"><?php echo $pt_bersangkutan_abdimas_ts2; ?></td>
                <td class="display2"><?php echo $pt_bersangkutan_abdimas_ts1; ?></td>
                <td class="display2"><?php echo $pt_bersangkutan_abdimas_ts; ?></td>
              </tr>
              <tr>
                <td class="display">
                  Depdiknas
                </td >
                <td class="display2"><?php echo $depdiknas_abdimas_ts2; ?></td>
                <td class="display2"><?php echo $depdiknas_abdimas_ts1; ?></td>
                <td class="display2"><?php echo $depdiknas_abdimas_ts; ?></td>
              </tr>
              <tr>
                <td class="display">
                  Institusi dalam negeri di luar Depdiknas
                </td>
                <td class="display2"><?php echo $luar_depdiknas_abdimas_ts2; ?></td>
                <td class="display2"><?php echo $luar_depdiknas_abdimas_ts2; ?></td>
                <td class="display2"><?php echo $luar_depdiknas_abdimas_ts2; ?></td>
              </tr>
              <tr>
                <td class="display">
                  Institusi luar negeri
                </td>
                <td class="display2"><?php echo  $biaya_luar_negeri_abdimas_hitung_ts2; ?></td>
                <td class="display2"><?php echo  $biaya_luar_negeri_abdimas_hitung_ts1; ?></td>
                <td class="display2"><?php echo  $biaya_luar_negeri_abdimas_hitung_ts; ?></td>
              </tr>


            </tbody>
      </table>
      <labe>Catatan: (*) Pelayanan/Pengabdian kepada Masyarakat adalah penerapan bidang ilmu untuk menyelesaikan masalah di masyarakat (termasuk masyarakat industri, pemerintah, dsb.)</label>

    </div>
	    </body>
</html>
