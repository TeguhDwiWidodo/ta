<?php
if(!isset($_SESSION['nama'])){
      header("location:" . base_url());
      exit();
   }
?>

<!DOCTYPE html>
<html>
	<head>
		<title>Report</title>
		<script type = 'text/javascript' src="<?php echo base_url(); ?>js/jquery-1.12.4.js"></script>
		<script type = 'text/javascript' src="<?php echo base_url(); ?>js/jquery.dataTables.js"></script>

    <link rel="stylesheet" href="<?php echo base_url(); ?>css/pdf.css">

 <!--    <style>
      .std711{
        background: red;
      }
    </style> -->
	</head>

	<body>

    <h2 >STANDAR 7 : PENELITIAN, PELAYANAN/PENGABDIAN MASYARAKAT, DAN KERJASAMA</h2>

      <div class="7-2-2">
        <label>7.2   Kegiatan Pelayanan/Pengabdian kepada Masyarakat (PkM)</label><br>
        <label>7.1.2 Adakah mahasiswa tugas akhir yang dilibatkan dalam penelitian dosen dalam tiga tahun terakhir? </label>
        <br>
        <?php if( $view722  == null){
          ?>
                <div style="margin-left:5%;"><input  type="checkbox" name="tidakada" value="Tidak Ada" checked="checked">Tidak</div>
                <div style="margin-left:5%;"><input  type="checkbox" name="ada" value="Ada" >Ya</div>
        <?php } else { ?>
               <div style="margin-left:5%;"><input  type="checkbox" name="tidakada" value="Tidak Ada">Tidak</div>
               <div style="margin-left:5%;"><input  type="checkbox" name="ada" value="Ada" checked="checked">Ya</div>
        <?php } ?>


        <div style="text-align:justify;">
          <label><b>Deskripsi</b></label>
          <div style="border:1px solid gray;padding:1%;">
            <p >
            <?php
                  echo	 $view722->text;
             ?>
            </p>
        </div>
      </div>
      </div>
	    </body>
</html>
