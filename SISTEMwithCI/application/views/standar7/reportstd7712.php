<?php
if(!isset($_SESSION['nama'])){
      header("location:" . base_url());
      exit();
   }
?>

<!DOCTYPE html>
<html>
	<head>
		<title>Report</title>
		<script type = 'text/javascript' src="<?php echo base_url(); ?>js/jquery-1.12.4.js"></script>
		<script type = 'text/javascript' src="<?php echo base_url(); ?>js/jquery.dataTables.js"></script>

    <link rel="stylesheet" href="<?php echo base_url(); ?>css/pdf.css">

 <!--    <style>
      .std711{
        background: red;
      }
    </style> -->
	</head>

	<body>

    <h2 >STANDAR 7 : PENELITIAN, PELAYANAN/PENGABDIAN MASYARAKAT, DAN KERJASAMA</h2>


        <div class="7_1_2">
          <div>
            <label>7.1</label>  <label>Penelitian Dosen Tetap yang Bidang Keahliannya Sesuai dengan PS</label>
            </div>
            <label>7.1.2 Adakah mahasiswa tugas akhir yang dilibatkan dalam penelitian dosen dalam tiga tahun terakhir? </label>
            <br>
            <?php if($jumlah_mahasiswa_peldosta  == null && $jumlah_mahasiswa_pelta != null){
              ?>
                    <div style="margin-left:5%;"><input  type="checkbox" name="tidakada" value="Tidak Ada" checked="checked">Tidak Ada</div>
                    <div style="margin-left:5%;"><input  type="checkbox" name="ada" value="Ada" >Ada</div>
            <?php } else { ?>
                   <div style="margin-left:5%;"><input  type="checkbox" name="tidakada" value="Tidak Ada">Tidak Ada</div>
                   <div style="margin-left:5%;"><input  type="checkbox" name="ada" value="Ada" checked="checked">Ada</div>
            <?php } ?>


            <div style="margin-left:5%;">Jika ada, banyaknya mahasiswa PS yang ikut serta dalam penelitian dosen adalah  <?php echo $jumlah_mahasiswa_peldosta; ?> orang, dari <?php echo $jumlah_mahasiswa_pelta; ?> mahasiswa yang melakukan tugas akhir melalui skripsi.</div>
        </div>
	    </body>
</html>
