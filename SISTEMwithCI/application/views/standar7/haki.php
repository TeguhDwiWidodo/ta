
<!DOCTYPE html>
<html>
	<head>
		<title>Akreditasi | HaKI</title>
		<script type = 'text/javascript' src="<?php echo base_url(); ?>js/jquery-1.12.4.js"></script>
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/mainlayout.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/penelitian.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/dosen.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/accordion.css">
		<script type = 'text/javascript' src="<?php echo base_url(); ?>assets/autosize-master/dist/autosize.js"></script>
		<script type = 'text/javascript' src="<?php echo base_url(); ?>js/jquery.dataTables.js"></script>
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/jquery.dataTables.css">
		<script type = 'text/javascript' >
		(function ($){
			$(document).ready(function() {
				$('#table-view-data-peldosen').dataTable({
					"order": [[ 0, 'asc' ]],
					"aLengthMenu": [5, 10, 25],
					iDisplayLength: 5,

				});
			});
		})(jQuery);
		</script>
	</head>

	<body>
		<div class="header">
			<h1><a href="<?php echo base_url(); ?>dashboard">Akreditasi SI</a></h1>
			<?php
				include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/header.php";
			?>
		</div>

		<div class="sidebar">
			<?php
				include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/sidebar.php";
			?>
		</div>

		<div class="main-layout">
			<?php

			if ( $this->session->userdata('hak_akses') == 'admin' ?: $this->session->userdata('hak_akses') == 'standar 7' ?: $this->session->userdata('hak_akses') == 'kaprodi'){

				?>
			<div class="main-content">
					<div class="-menu-breadcrumb">
						<ul class="breadcrumb">
							<li><a href="<?php echo base_url(); ?>dashboard" id="menu-breadcrumb">Dashboard</a></li>
							<li><a href="<?php echo base_url(); ?>standar7/penelitian" id="menu-breadcrumb">Penelitian</a></li>
							<li>HaKI</li>
						</ul>
					</div>
					<div class="menu-head">
						<ul>
							<li><a href="<?php echo base_url(); ?>standar7/penelitian" id="menu-head" style="background:#d9d9d9;color:black;">Penelitian</a></li>
							<li><a href="<?php echo base_url(); ?>standar7/abdimas" id="menu-head">Pengmas</a></li>
							<li><a href="<?php echo base_url(); ?>standar7/kerjasama" id="menu-head">Kerjasama</a></li>
						</ul>
					</div>

						<div class="menu-subhead">
						<ul>
							<li><a href="<?php echo base_url(); ?>standar7/penelitiandosen" id="menu-subhead">Penelitian Dosen SI</a></li>
							<li><a href="<?php echo base_url(); ?>standar7/penelitiandosenta" id="menu-subhead">Penelitian (TA)</a></li>
							<li><a href="<?php echo base_url(); ?>standar7/publikasi" id="menu-subhead">Publikasi</a></li>
							<li><a href="<?php echo base_url(); ?>standar7/haki" id="menu-subhead" style="background:#404040;color:white;">HaKI</a></li>
							<li><a href="<?php echo base_url(); ?>standar7/dosen" id="menu-subhead">Dosen</a></li>
						</ul>
					</div>

					<div class="sub-header">
					<h2> Standar 7 : HaKI</h2>
					</div>

					<script type="text/javascript" language="javascript">
						var _validFileExtensions = [".xlsx", ".xls", ".csv"];
							function Validate(oForm) {
								var arrInputs = oForm.getElementsByTagName("input");
								for (var i = 0; i < arrInputs.length; i++) {
									var oInput = arrInputs[i];
									if (oInput.type == "file") {
										var sFileName = oInput.value;
										if (sFileName.length > 0) {
											var blnValid = false;
											for (var j = 0; j < _validFileExtensions.length; j++) {
												var sCurExtension = _validFileExtensions[j];
												if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
													blnValid = true;
													break;
												}
											}

											if (!blnValid) {
												alert("Sorry, " + sFileName + " is invalid, allowed extensions are: " + _validFileExtensions.join(", "));
												return false;
											}
										}
									}
								}

								return true;
							}
					</script>

					<div class="penelitian_dosen dosen">
						<div class="peldos-header dosen-header">
							<h2>Pilih Cara : </h2>
						</div>
						<button class="accordion">Import Data HaKI</button>
						<div class="panel">
							<div class="panel-input">
							<br/>
							<h3 class="head-panel">IMPORT DATA HaKI</h3>
							<hr/>
							<br>
						<!--	<label >pengupdate : <?php

							if(empty($pengupdate->pengupdate)){

echo "";


							} else {

								echo $pengupdate->pengupdate;
							} ?></label></br> -->
							<label >Download Template HaKI : </label>
							<label style="color:red;font-size:12px;"><a href="<?php echo base_url(); ?>uploads/Format_import_std7/format_data_haki.xlsx" target="_blank">Download File</a></label>
							<br>
						  <br>
						   <form action="<?php echo base_url(); ?>Standar7/importhakiproses" onsubmit="return Validate(this);" method="post" enctype="multipart/form-data">
								<p class="huruf-upload-files">Files: <input required type="file" name="files" id="files" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" /></p>
								<label style="color:red;font-size:12px;">*File yang dapat di import csv, xls, dan xlxs</label>

								<br/>
								<br/>
								<hr/>
								<button class="btn" type="submit" name="btn-upload">Upload</button>
							</form>
						  <br>
						</div>
						</div>
						<button class="accordion">Input Data HaKI</button>
						<div class="panel">
							<table class="display" width="100%" cellspacing="0">
							<form  class="data-diri-form" name="data-diri-form" method="post"   action ="<?php echo base_url(); ?>Standar7/hakiiinsert" enctype="multipart/form-data">

							<tbody>
									<tr>
								<td colspan="3"><h3 class="head-panel">INPUT DATA HaKI</h3><hr/><br/></td>
								</tr>
								<tr>
									<td><p class="label">karya</p></td>
									<td><p> : </p></td>
									<td><input class="data-input-kerjasama" id="karya"  type="text"  name="karya" placeholder="karya" required ></td>

								</tr>
								<tr>
									<td><p class="label">Jenis</p></td>
									<td><p> : </p></td>
									<td><select   name="jenishaki" class="data-input-kerjasama">
									 <option value="Hak Paten">Hak Paten</option>
									 <option value="Hak Cipta">Hak Cipta</option>
									 <option value="Hak Merk">Hak Merk</option>
								 </select>
							 </td>

								</tr>
								<tr>
									<td><p class="label">Tahun</p></td>
									<td><p> : </p></td>
									<td><input class="data-input-kerjasama" id="tahun"  type="date"  name="tahun" placeholder="tahun" required ></td>

								</tr>

								<tr>
								<td colspan="3"><br/><hr/><input class="data-diri-button btn" id="data-diri-button" name="simpanbtn" title="simpan" type="submit" value="Simpan" ><br/></td>
								</tr>
							</tbody>

							</form>

						</table>
						</div>
					</div>
					<script>
						var acc = document.getElementsByClassName("accordion");
						var i;

						for (i = 0; i < acc.length; i++) {
						  acc[i].onclick = function() {
							this.classList.toggle("active");
							var panel = this.nextElementSibling;
							if (panel.style.maxHeight){
							  panel.style.maxHeight = null;
							} else {
							  panel.style.maxHeight = panel.scrollHeight + "px";
							}
						  }
						}
					</script>
			</div>
			<div class="data-dosen">
			<table id="table-view-data-peldosen" class="display" width="100%" cellspacing="0">

						<thead class="table head-peldosen">
							<th>#</th>
							<th>no</th>
							<th >Karya</th>
							<th >Jenis</th>
							<th >Tahun</th>
							<th>lampiran</th>
							<th >Upload (Bukti)</th>
							<th><i class="fa fa-pencil-square-o" aria-hidden="true"></i></th>
							<th><i class="fa fa-trash-o" aria-hidden="true"></i></th>
						</thead>

						<tbody class="table body-peldosen" role="alert" aria-live="polite" aria-relevant="all">


														<?php
																$no = 1;
																foreach($result as $row){

																?>
																	<tr id="row:<?php echo $row->id ?>">
																	<td ><input type="checkbox" name="pilih[]" class="check" value="<?php echo $row->id ?>" ></td>
																		<td ><?php echo $no++ ?></td>
																		<td ><textarea class="data-diri-input" title="<?php echo $row->karya ?>" id="karya<?php echo $row->id ?>"  type="text" name="karya" placeholder="" readonly /><?php echo $row->karya ?></textarea></td>
																		<td ><textarea class="data-diri-input" title="<?php echo $row->jenis_haki?>" id="jenis<?php echo $row->id ?>"  type="text" name="karya" placeholder="" readonly /><?php echo $row->jenis_haki ?></textarea></td>

																		<td ><textarea class="data-diri-input" title="<?php echo $row->tahun ?>" id="tahun<?php echo $row->id ?>"  type="text" name="tahun" placeholder="" readonly /><?php echo $row->tahun ?></textarea></td>

																				<td ><div class="lots">
																							<p><a target="_blank" title="Uploaded : [<?php echo $row->file_date ?>], <?php echo $row->file_name ?> " href="<?php echo base_url(); ?>uploads/dokumen_haki_std7/<?php echo $row->file_name ?>"><?php echo $row->file_name?> </a>...</p>

																		</div></td>

																		<td >
																			<form onsubmit="return Validate1(this);" action="<?php echo base_url(); ?>Standar7/hakiuploadpdf/<?php echo $row->id ?>" method="post" enctype="multipart/form-data">
																				<input class="aa" style="font-size:9px;display: none;" id="file<?php echo $row->id ?>" required type="file" name="files" onchange="javascript:updateList('<?php echo $row->id ?>')" accept="application/pdf" />
																				<input type="hidden" value="<?php echo $row->id ?>" name="data-id" >
																				<input type="hidden" value="<?php echo $row->file_name?>" name="data-filename" >
																				<div class="lots" id="nama-file<?php echo $row->id ?>"></div>
																				<br>
																				<button style="display: block;" title="Pilih Doc Pendukung : <?php echo $row->karya ?>" type="button" onclick="edit_row('<?php echo $row->id ?>')" id="edit_btn<?php echo $row->id ?>" class="edit" value="edit"><i class="fa fa-file" aria-hidden="true"></i></button>
																				<br>
																				<button style="display: none;" title="Upload : <?php echo $row->karya ?>" name="btn-upload" type="submit"  onclick="save_row('<?php echo $row->id ?>')" id="save_btn<?php echo $row->id ?>" class="save" value="save"><i class="fa fa-floppy-o" aria-hidden="true"></i></button>
																				</form>
																				<script type="text/javascript" language="javascript">
																					var _validFileExtensions1 = [".pdf"];
																						function Validate1(oForm) {
																							var arrInputs = oForm.getElementsByTagName("input");
																							for (var i = 0; i < arrInputs.length; i++) {
																								var oInput = arrInputs[i];
																								if (oInput.type == "file") {
																									var sFileName = oInput.value;
																									if (sFileName.length > 0) {
																										var blnValid = false;
																										for (var j = 0; j < _validFileExtensions1.length; j++) {
																											var sCurExtension = _validFileExtensions1[j];
																											if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
																												blnValid = true;
																												break;
																											}
																										}

																										if (!blnValid) {
																											alert("Sorry, " + sFileName + " is invalid, allowed extensions are: " + _validFileExtensions1.join(", "));
																											return false;
																										}
																									}
																								}
																							}

																							return true;
																						}
																				</script>
																			</td>
																			<td><button onclick="edit_row1('<?php echo $row->id ?>')" title="Ubah : <?php echo $row->karya ?>" id="edit<?php echo $row->id ?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
																			<button  onclick="save_row1('<?php echo $row->id ?>')" title="Simpan : <?php echo $row->karya ?>" id="save<?php echo $row->id ?>" style="display:none;" /><i class="fa fa-floppy-o" aria-hidden="true"></i></button>
																			</td>
																			<td>
																			<form action="<?php echo base_url(); ?>standar7/hakihapus/<?php echo $row->id ?>" method="post" enctype="multipart/form-data">
																			<input type="hidden" value="<?php echo $row->id?>" name="data-id" >
																			<input type="hidden" value="<?php echo $row->file_name?>" name="data-filename" >

																			<button name="btn-delete" title="Hapus : <?php echo $row->karya ?>" onclick="return ConfirmDelete(<?php echo $row->id ?>)"  id="delete-btn<?php echo $row->id ?>" type="submit"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
																			</form>
																				<script type="text/javascript">
																						var baseUrl='<?php echo base_url(); ?>standar7/kerjasamahapus/<?php echo $row->id ?>';
																						var msgg = 'Apakah anda yakin untuk menghapus data ini ?';
																						function ConfirmDelete(no)
																						{
																						if (confirm(msgg)) {
																							location.href=baseUrl;
																						}
																						else {
																							 return false;
																						}
																						}
																				</script>
																			</td>


																						<script>
																			var no;
																			function edit_row1(no)
																			{
																					document.getElementById('edit'+no).style.display= 'none';
																					document.getElementById('save'+no).style.display= 'block';
																				 document.getElementById("karya"+no).removeAttribute('readonly');
																				document.getElementById("jenis"+no).removeAttribute('readonly');
																				 document.getElementById("tahun"+no).removeAttribute('readonly');
																				 document.getElementById('tahun'+no).style.background = "#ccffff";
																				 document.getElementById('karya'+no).style.background = "#ccffff";
																				 document.getElementById("jenis"+no).style.background = "#ccffff";


																			}



																			</script>
																			<script>

																			function save_row1(no){
																				var karya=  document.getElementById("karya"+no).value;
																				var jenis=  document.getElementById("jenis"+no).value;
																				var tahun=  document.getElementById("tahun"+no).value;

																				var id = no;


																							$.ajax({

																									type: "POST",
																									url: "<?php echo base_url(); ?>Standar7/hakiupdate",
																									data: {id:id, karya:karya, jenis:jenis, tahun:tahun},

																									success: function(data){
																									console.log(data);

																										},

																									error: function(xhr, status, errorThrown){
																									alert( xhr.status);
																									alert( xhr.errorThrown);
																									alert( xhr.responseText);
																									console.log(xhr.responseText);
																									}
																							});

																													document.getElementById('edit'+no).style.display= 'block';
																													document.getElementById('save'+no).style.display= 'none';
																													document.getElementById("karya"+no).readOnly =true;
																													document.getElementById("tahun"+no).readOnly =true;
																													document.getElementById("jenis"+no).readOnly =true;

								 																				 document.getElementById('tahun'+no).style.background = "#f3f3f3";
																												 document.getElementById("jenis"+no).style.background = "#f3f3f3";
								 																				 document.getElementById('karya'+no).style.background = "#f3f3f3";

																												}
																			</script>


																			<script>
																				function edit_row(no) {
																					document.getElementById('file'+no).click();
																					document.getElementById("save_btn"+no).style.display="block";
																				}

																				function save_row(no) {
																					document.getElementById("edit_btn"+no).style.display="block";
																					document.getElementById("save_btn"+no).style.display="none";
																				}

																				updateList = function(no) {
																					var input = document.getElementById('file'+no);
																					var output = document.getElementById('nama-file'+no);

																					output.innerHTML = '<label>';
																					for (var i = 0; i < input.files.length; ++i) {
																					output.innerHTML += input.files.item(i).name ;
																					}
																					output.innerHTML += '</label>';
																			}
																			</script>
																	</tr>
															<?php
																}
															?>
															</tbody>
															<tfoot class="table head-peldosen">
																<th>#</th>
																<th>no</th>
																<th >Karya</th>
																<th >Jenis</th>
																<th >Tahun</th>
																<th>lampiran</th>
																<th >Upload (Bukti)</th>
																<th><i class="fa fa-pencil-square-o" aria-hidden="true"></i></th>
																<th><i class="fa fa-trash-o" aria-hidden="true"></i></th>
															</tfoot>

													</table>
												</div>
												<div class="delete all-data">
													<button class="btn-delete-list" name="btn-delete-check" type="button" onclick="hapus_checklist()" >Hapus Terpilih</button>
													<?php
														if($this->session->flashdata('msgg')){
															echo "<script> alert('".$this->session->flashdata('msgg')."'); </script>";
														}
													?>
													<script type="text/javascript" language="javascript">
														function hapus_checklist(){

															var selected_value = new Array(); // initialize empty array
															var confirmation = confirm("Apakah anda yakin menghapus data yang anda pilih ?");
															 if(confirmation){
																$(".check:checked").each(function(){
																		selected_value.push($(this).val());
																	});
																	console.log(selected_value);
																$.ajax({
																		type: "POST",
																		url: "<?php echo base_url(); ?>Standar7/hakihapuschecklist",
																		data: {result:JSON.stringify(selected_value)},

																		success: function(data){
																		console.log(data);
																		window.location.href='<?php echo base_url(); ?>Standar7/haki?success';
																		},

																		error: function(xhr, status, errorThrown){
																		alert( xhr.status);
																		alert( xhr.errorThrown);
																		alert( xhr.responseText);
																		console.log(xhr.responseText);
																		}
																});
															 }
														}
													</script>

													<button class="btn-delete-all" name="btn-delete-all" onclick="ConfirmDeleteall()"  id="delete-btn" type="submit">Hapus Semua Data</i></button>

													<script type="text/javascript">
															var baseUrl='<?php echo base_url(); ?>standar7/hakihapussemua';
															function ConfirmDeleteall()
																{
																		var msg = 'Apakah anda yakin menghapus semua data HaKI ?';
																		if (confirm(msg)){
																		   location.href=baseUrl;
																		}
																}
													</script>

			</div>
			<?php
				}else{
					?>
					<div class="main-content">
							<div class="-menu-breadcrumb">
								<ul class="breadcrumb">
									<li><a href="<?php echo base_url(); ?>dashboard" id="menu-breadcrumb">Dashboard</a></li>
									<li><a href="<?php echo base_url(); ?>standar7/penelitian" id="menu-breadcrumb">Penelitian</a></li>
									<li>HaKI</li>
								</ul>
							</div>
							<div class="menu-head">
								<ul>
									<li><a href="<?php echo base_url(); ?>standar7/penelitian" id="menu-head" style="background:#d9d9d9;color:black;">Penelitian</a></li>
									<li><a href="<?php echo base_url(); ?>standar7/abdimas" id="menu-head">Abdimas</a></li>
									<li><a href="<?php echo base_url(); ?>standar7/kerjasama" id="menu-head">Kerjasama</a></li>
								</ul>
							</div>

								<div class="menu-subhead">
								<ul>
									<li><a href="<?php echo base_url(); ?>standar7/penelitiandosen" id="menu-subhead">Penelitian Dosen SI</a></li>
									<li><a href="<?php echo base_url(); ?>standar7/penelitiandosenta" id="menu-subhead">Penelitian Dosen SI (TA)</a></li>
									<li><a href="<?php echo base_url(); ?>standar7/publikasi" id="menu-subhead">Publikasi</a></li>
									<li><a href="<?php echo base_url(); ?>standar7/haki" id="menu-subhead" style="background:#404040;color:white;">HaKI</a></li>
									<li><a href="<?php echo base_url(); ?>standar7/dosen" id="menu-subhead">Dosen</a></li>
								</ul>
							</div>

							<div class="sub-header">
							<h2> Standar 7 : HaKI</h2>
							</div>

							<div class="penelitian_dosen dosen">
								<div class="peldos-header dosen-header">
									<h2>Pilih Cara : </h2>
								</div>
								<button class="accordion">Import Data HaKI</button>
								<div class="panel">
									</br>
												<label style="font-size:80%; color:red;">Maaf tidak bisa melakukan import data, karena bukan Anggota tim pengumpul data standar 7</label>
									</br>
									</br>

								</div>
								<button class="accordion">Input Data HaKI</button>
								<div class="panel">
									</br>
										<label style="font-size:80%; color:red;">Maaf tidak bisa melakukan input data, karena bukan Anggota tim pengumpul data standar 7</label>
									</br>
									</br>
							</div>
							<script>
								var acc = document.getElementsByClassName("accordion");
								var i;

								for (i = 0; i < acc.length; i++) {
								  acc[i].onclick = function() {
									this.classList.toggle("active");
									var panel = this.nextElementSibling;
									if (panel.style.maxHeight){
									  panel.style.maxHeight = null;
									} else {
									  panel.style.maxHeight = panel.scrollHeight + "px";
									}
								  }
								}
							</script>
					</div>
					<div class="data-dosen">
					<table id="table-view-data-peldosen" class="display" width="100%" cellspacing="0">

								<thead class="table head-peldosen">

									<th>No</th>
									<th >Karya</th>
									<th >Tahun</th>
									<th>lampiran</th>
								</thead>

								<tbody class="table body-peldosen" role="alert" aria-live="polite" aria-relevant="all">


																<?php
																		$no = 1;
																		foreach($result as $row){

																		?>
																			<tr id="row:<?php echo $row->id ?>">
																				<td ><?php echo $no++ ?></td>
																				<td ><textarea class="data-diri-input" id="karya<?php echo $row->id ?>"  type="text" name="karya" placeholder="" readonly /><?php echo $row->karya ?></textarea></td>
																				<td ><textarea class="data-diri-input" id="tahun<?php echo $row->id ?>"  type="text" name="tahun" placeholder="" readonly /><?php echo $row->tahun ?></textarea></td>

																						<td ><div class="lots">
																									<p><a target="_blank" href="<?php echo base_url(); ?>uploads/dokumen_kerjasama_std7/<?php echo $row->file_name ?>"><?php echo $row->file_name?> </a></p>

																				</div></td>

																			</tr>
																	<?php
																		}
																	?>
																	</tbody>
																	<tfoot class="table head-peldosen">
																		<th>no</th>
																		<th >Karya</th>
																		<th >Tahun</th>
																		<th>lampiran</th>
																	</tfoot>

															</table>
														</div>

			<?php
				}
			?>
		</div>

		<div class="footer">
			<?php
			include $_SERVER['DOCUMENT_ROOT']."/ta/sistemwithci/assets/footer.php";
			?>
		</div>

	</body>



</html>
