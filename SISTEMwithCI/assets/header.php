<?php
$sql="SELECT nama,profilpic_path,hak_akses FROM users WHERE id_user = '".$_SESSION['id_user']."' ";
$result_set = $this->db->query($sql);
foreach($result_set->result_array() as $row){
	$profilpicpath_header = $row['profilpic_path'];
	$namauser_header = $row['nama'];
}

if(!isset($_SESSION['nama'])){
      header("location:" . base_url());
      exit();
   }
?>

<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/header.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>font-awesome-4.6.3/css/font-awesome.min.css">
	 <script type = 'text/javascript' src="<?php echo base_url(); ?>js/jquery-1.12.4.js"></script>

	 <script type = 'text/javascript' src="<?php echo base_url(); ?>js/jquery-ui-1.12.1/jquery-ui.js"></script>
	 <link rel="stylesheet" href="<?php echo base_url(); ?>js/jquery-ui-1.12.1/jquery-ui.css">
	</head>

	<body>
		<div class="profile">
		  <div class="profile-back">
			<div class="name-profile">
				<p><?php echo $namauser_header ?>, Hak akses: <?php echo strtoupper($_SESSION['hak_akses']) ?></p>
			</div>
				<div class="image-profile" id="showbox">
					<img class="image" src="<?php echo base_url($profilpicpath_header); ?>"  alt="Image Profile" style="width:35px;height:35px;">
					<i id="header-arrow" class="fa fa-caret-down" aria-hidden="true"></i>
				</div>
				</div>

				<div class="setting" id="bigbox">
					<div id="triangle-up"></div>

					<div class="setting-account">
						<p><a href="<?php echo base_url(); ?>profile/">Ubah Profil</a></p>
					</div>
					<!--
					<div class="setting-account">
						<p><a href="<?php // echo base_url(); ?>timakreditasi/">Tim Akreditasi</a></p>
					</div>
					-->
					<hr size="1" width="98%">
					<form id="logout"action="<?php echo base_url("logout/") ?>" method="post">
						<input class="logout-button" id="logout-button" name="logoutbtn" title="Logout" type="submit" value="Logout" >
					</form>
				</div>




					<script type="text/javascript">
					$(document).ready(function(){
						$('.image-profile').click(function(event){
							event.stopPropagation();
							 $(".setting").slideToggle("fast");
						});
						$(".setting").on("click", function (event) {
							event.stopPropagation();
						});
					});

					$(document).on("click", function () {
						$(".setting").hide();
					});

					</script>
		</div>


	</body>

</html>
