<!DOCTYPE html>
<html>
	<head>

		<link rel="stylesheet" href="<?php echo base_url(); ?>css/sidebar.css">
		<script type = 'text/javascript' src="<?php echo base_url(); ?>js/jquery-1.12.4.js"></script>

		<script type = 'text/javascript' src="<?php echo base_url(); ?>js/jquery-ui-1.12.1/jquery-ui.js"></script>
		<link rel="stylesheet" href="<?php echo base_url(); ?>js/jquery-ui-1.12.1/jquery-ui.css">
	</head>
		<div class="menu">
			<ul>
				<li><a class="menu-bar" id="menu-link" onclick="toggle_visibility('hide')">Standar 5 <i id="arroww" class="fa fa-caret-down" aria-hidden="true"></i></a>
					<ul id="hide" style="display:none;">
						<li><a class="menu-standar" id="menu-link" href="<?php echo base_url(); ?>standar5/kurikulum"><i id="arrow1" class="fa fa-caret-right" aria-hidden="true"></i>Data dan Dokumen Standar 5</a></li>
						<li><a class="menu-standar" id="menu-link" href="<?php echo base_url(); ?>standar5/simulasipenilaian"><i id="arrow1" class="fa fa-caret-right" aria-hidden="true"></i>Penilaian Borang PRODI Standar 5</a></li>
					<!--	<li><a class="menu-standar" id="menu-link" href="<?php echo base_url(); ?>standar5/uploadfile"><i id="arrow1" class="fa fa-caret-right" aria-hidden="true"></i>Upload file Standar 5</a></li>
					-->
					</ul>
				</li>
				<li><a class="menu-bar" id="menu-link" onclick="toggle_visibility('hide1')">Standar 7<i id="arrow" class="fa fa-caret-down" aria-hidden="true"></i></a>
					<ul id="hide1" style="display:none;">
						<li><a class="menu-standar" id="menu-link" href="<?php echo base_url(); ?>standar7/penelitian"><i id="arrow1" class="fa fa-caret-right" aria-hidden="true"></i>Data dan Dokumen Standar 7</a></li>
						<li><a class="menu-standar" id="menu-link" href="<?php echo base_url(); ?>standar7/penilaian"><i id="arrow1" class="fa fa-caret-right" aria-hidden="true"></i>Penilaian Borang PRODI Standar 7</a></li>
						<!-- <li><a class="menu-standar" id="menu-link" href="<?php echo base_url(); ?>standar7/uploadfile"><i id="arrow1" class="fa fa-caret-right" aria-hidden="true"></i>Upload file Standar 7</a></li>
						-->
					</ul>
				</li>
				<?php
				if($_SESSION['hak_akses'] == 'admin'){
				?>
					<li><a class="menu-bar" id="menu-link" onclick="toggle_visibility('hide2')">Admin<i id="arrow2" class="fa fa-caret-down" aria-hidden="true"></i></a>
					<ul id="hide2" style="display:none;">
						<li><a class="menu-standar" id="menu-link" href="<?php echo base_url(); ?>admin/kelolapengguna"><i id="arrow1" class="fa fa-caret-right" aria-hidden="true"></i>Kelola Pengguna</a></li>
						<li><a class="menu-standar" id="menu-link" href="<?php echo base_url(); ?>admin/tambahtsview"><i id="arrow1" class="fa fa-caret-right" aria-hidden="true"></i>Tambah TS</a></li>
					</ul>

				</li>
				<?php
				} else {

				}
				?>
				<li><a class="menu-bar" id="menu-link" href="<?php echo base_url(); ?>timakreditasi">Lihat Tim Akreditasi</a></li>
			</ul>
			<script type="text/javascript">
					function toggle_visibility(id) {
					var e = document.getElementById(id);
					if (e.style.display == 'block' || e.style.display=='') e.style.display = 'none';
					else e.style.display = 'block';
					}
			</script>
		</div>
	<body>

	</body>

</html>
